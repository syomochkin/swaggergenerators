declare namespace Definitions {
    /**
     * AddPlaceTypeCommonCharacteristicDto
     */
    export interface AddPlaceTypeCommonCharacteristicDto {
        characteristic?: string;
        defaultValue?: /* DefaultValue */ DefaultValue;
        settings?: /* CommonCharacteristicSettings */ CommonCharacteristicSettings;
    }
    /**
     * AddStockToCarrierCmd
     */
    export interface AddStockToCarrierCmd {
        baseAmount?: number;
        baseUnit?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        stockType?: string;
        tabNumber?: string;
        unit?: string;
        userName?: string;
    }
    /**
     * AllocationSettingsDto
     */
    export interface AllocationSettingsDto {
        maxMixedProducts?: number; // int32
        placeTypeCode?: string;
        zoneCode?: string;
    }
    /**
     * AllocationZoneTransferReq
     */
    export interface AllocationZoneTransferReq {
        carrierId?: string; // uuid
        destinationPlaceId?: string; // uuid
        operationId?: string;
        startPlaceId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * AssignProductBatchCmd
     */
    export interface AssignProductBatchCmd {
        expirationTime?: string; // date-time
        inboundId?: string; // uuid
        inboundTime?: string; // date-time
        manufactureTime?: string; // date-time
        mercuryExtId?: string;
        number?: string;
        productId?: string;
        productName?: string;
        siteCode?: string;
        type?: "REGULAR" | "RETURN";
        unit?: string;
        vendorCode?: string;
        vendorName?: string;
        weightProduct?: boolean;
    }
    /**
     * BoxRoute
     */
    export interface BoxRoute {
        operationId?: string;
        plannedTotalDuration?: number; // int32
        rejectedItems?: /* ProductAmountDto */ ProductAmountDto[];
        steps?: /* RouteStep */ RouteStep[];
        transferId?: string; // uuid
    }
    /**
     * BuildBoxRouteCmd
     */
    export interface BuildBoxRouteCmd {
        items?: /* ProductAmountDto */ ProductAmountDto[];
        operationId?: string;
        siteCode?: string;
        transferId?: string; // uuid
    }
    /**
     * BulkOperationDto
     */
    export interface BulkOperationDto {
        operationId?: string;
        status?: string;
    }
    /**
     * BulkPlaceUpdateStatusCmd
     */
    export interface BulkPlaceUpdateStatusCmd {
        operationId?: string;
        placeIds?: string /* uuid */[];
        statusCode?: string;
    }
    /**
     * BulkPlaceUpdateTypeCmd
     */
    export interface BulkPlaceUpdateTypeCmd {
        operationId?: string;
        placeIds?: string /* uuid */[];
        targetPlaceTypeId?: string; // uuid
    }
    /**
     * BulkPlaceUpdateZoneCmd
     */
    export interface BulkPlaceUpdateZoneCmd {
        operationId?: string;
        placeIds?: string /* uuid */[];
        zoneId?: string; // uuid
    }
    /**
     * CancelStockReservationCmd
     */
    export interface CancelStockReservationCmd {
        incomingQuantity?: number; // int32
        operationId?: string;
        outgoingQuantity?: number; // int32
        siteCode?: string;
        stockId?: string; // uuid
        transferId?: string; // uuid
        unit?: string;
    }
    /**
     * CarrierDto
     */
    export interface CarrierDto {
        author?: string;
        barcode?: string;
        carriersTypeId?: string; // uuid
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        nestedCarriers?: /* CarrierDto */ CarrierDto[];
        parentId?: string; // uuid
        parentNumber?: string;
        placeAddress?: string;
        placeId?: string; // uuid
        processId?: string;
        type?: /* CarrierTypeDto */ CarrierTypeDto;
        updatedTime?: number; // int32
    }
    /**
     * CarrierGridView
     */
    export interface CarrierGridView {
        barcode?: string;
        id?: string; // uuid
        nestedCarriers?: /* CarrierGridView */ CarrierGridView[];
        status?: "incoming" | "outgoing" | "actual";
        type?: /* CarrierTypeGridView */ CarrierTypeGridView;
    }
    /**
     * CarrierShippedResponse
     */
    export interface CarrierShippedResponse {
        carrierId?: string; // uuid
        carrierMoveResponses?: /* MoveCarrierToPlaceResponse */ MoveCarrierToPlaceResponse[];
        operationId?: string;
        placeId?: string; // uuid
    }
    /**
     * CarrierTypeCodeExistsQuery
     */
    export interface CarrierTypeCodeExistsQuery {
        code?: string;
    }
    /**
     * CarrierTypeDto
     */
    export interface CarrierTypeDto {
        archived?: boolean;
        author?: string;
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        height?: number; // int32
        id?: string; // uuid
        length?: number; // int32
        maxVolume?: number;
        maxWeight?: number;
        name?: string;
        updatedTime?: number; // int32
        width?: number; // int32
    }
    /**
     * CarrierTypeGridView
     */
    export interface CarrierTypeGridView {
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * ChangeRawStock
     */
    export interface ChangeRawStock {
        position?: number; // int64
        stockId?: string; // uuid
    }
    /**
     * ChangeRawStockTypeCmd
     */
    export interface ChangeRawStockTypeCmd {
        operationId?: string;
        orderVersion?: string;
        outboundDeliveryNumber?: string;
        outboundTime?: string; // date-time
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        stocks?: /* ChangeRawStock */ ChangeRawStock[];
        tabNumber?: string;
        userName?: string;
    }
    /**
     * CharacteristicDto
     */
    export interface CharacteristicDto {
        type?: string;
        value?: /* ValueDto */ ValueDto;
    }
    /**
     * CodeExists
     */
    export interface CodeExists {
        exists?: boolean;
    }
    /**
     * CommonCharacteristicSettings
     */
    export interface CommonCharacteristicSettings {
        editable?: boolean;
        mandatory?: boolean;
        parentInheritance?: "check" | "ignore";
        unit?: string;
    }
    /**
     * ConfirmProductBatchCmd
     */
    export interface ConfirmProductBatchCmd {
        batchId?: string; // uuid
        targetStockType?: string;
    }
    /**
     * Coordinates3D
     */
    export interface Coordinates3D {
        x?: number;
        y?: number;
        z?: number;
    }
    /**
     * CoordinatesDto
     */
    export interface CoordinatesDto {
        x?: number;
        y?: number;
        z?: number;
    }
    /**
     * CreateCarrierCmd
     */
    export interface CreateCarrierCmd {
        carrierBarcode?: string;
        carrierId?: string; // uuid
        carrierTypeCode?: string;
        placeBarcode?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * CreateCarrierTypeCmd
     */
    export interface CreateCarrierTypeCmd {
        code?: string;
        description?: string;
        height?: number; // int32
        id?: string; // uuid
        length?: number; // int32
        maxVolume?: number;
        maxWeight?: number;
        name?: string;
        width?: number; // int32
    }
    /**
     * CreatePlaceCmd
     */
    export interface CreatePlaceCmd {
        address?: string;
        addressTypeId?: string; // uuid
        coordinates?: /* Coordinates3D */ Coordinates3D;
        description?: string;
        id?: string; // uuid
        number?: number; // int32
        parentId?: string; // uuid
        siteId?: string; // uuid
        status?: /* PlaceStatusDto */ PlaceStatusDto;
        statusReason?: string;
        typeId?: string; // uuid
        zoneIds?: string /* uuid */[];
    }
    /**
     * CreatePlaceTypeCmd
     */
    export interface CreatePlaceTypeCmd {
        code?: string;
        coordinatesRequired?: boolean;
        description?: string;
        maxMixBatches?: number; // int32
        name?: string;
        numerationRule?: "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP";
        placeTypeId?: string; // uuid
        storagePlace?: boolean;
    }
    /**
     * CreatePlanRequest
     */
    export interface CreatePlanRequest {
        carrierId?: string; // uuid
        startPlaceId?: string; // uuid
        targetZoneId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * CreateSiteCmd
     */
    export interface CreateSiteCmd {
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * CreateZoneCmd
     */
    export interface CreateZoneCmd {
        boundedWarehouseProductGroups?: /* WarehouseProductGroupDto */ WarehouseProductGroupDto[];
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
        siteId?: string; // uuid
        typeId?: string; // uuid
    }
    /**
     * CreateZoneTypeCmd
     */
    export interface CreateZoneTypeCmd {
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * Data
     */
    export interface Data {
        address?: string;
        author?: string;
        childrenAmount?: number; // int32
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        status?: /* PlaceStatusDto */ PlaceStatusDto;
        typeName?: string;
        updatedTime?: number; // int32
        zoneNames?: string[];
    }
    /**
     * DefaultValue
     */
    export interface DefaultValue {
        decValue?: number;
        intValue?: number; // int32
        maxIntValue?: number; // int32
        minIntValue?: number; // int32
    }
    /**
     * DetailedCarriersSearchQuery
     */
    export interface DetailedCarriersSearchQuery {
        barcodes?: string[];
    }
    /**
     * DetailedStockSearchQuery
     */
    export interface DetailedStockSearchQuery {
        carrierId?: string; // uuid
        carrierNumber?: string;
        inCarrier?: "IN_ONLY" | "OUT_ONLY" | "ALL";
        maxResponseSize?: number; // int32
        placeAddress?: string;
        placeId?: string; // uuid
        productBatchId?: string; // uuid
        productBatchNumber?: string;
        productId?: string;
        siteCode?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
    }
    /**
     * DetailedStocksSearchResult
     */
    export interface DetailedStocksSearchResult {
        stocks?: /* StockDto */ StockDto[];
        totalCount?: number; // int64
    }
    /**
     * DropAllProcessReservationsCmd
     */
    export interface DropAllProcessReservationsCmd {
        operationId?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * DropCarrierReservationForProcessCmd
     */
    export interface DropCarrierReservationForProcessCmd {
        carrierNumber?: string;
        dropForNestedCarriers?: boolean;
        operationId?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * DropPlaceReservationForProcessCmd
     */
    export interface DropPlaceReservationForProcessCmd {
        operationId?: string;
        placeBarcode?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * DropPlaceReservationsForTransferCmd
     */
    export interface DropPlaceReservationsForTransferCmd {
        operationId?: string;
        processId?: string;
        siteCode?: string;
        transferId?: string; // uuid
    }
    /**
     * DropTransferCmd
     */
    export interface DropTransferCmd {
        transferId?: string; // uuid
    }
    /**
     * Error
     */
    export interface Error {
        code?: string;
        message: string;
    }
    /**
     * FullAmount
     */
    export interface FullAmount {
        quantity?: number;
        unit?: string;
        volume?: number;
        weight?: number; // int64
    }
    /**
     * ModifyStockTypeCmd
     */
    export interface ModifyStockTypeCmd {
        code?: string;
        name?: string;
    }
    /**
     * MonoPalletPlacementRequest
     */
    export interface MonoPalletPlacementRequest {
        incomingCarrierId?: string; // uuid
        openCarriers?: string /* uuid */[];
        operationId?: string;
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        siteCode?: string;
        stockTypeCode?: string;
    }
    /**
     * MoveAllStocksToCarrierCmd
     */
    export interface MoveAllStocksToCarrierCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        sourcePlaceId?: string; // uuid
        tabNumber?: string;
        targetCarrierId?: string; // uuid
        transferId?: string; // uuid
        userName?: string;
    }
    /**
     * MoveAllStocksToCarrierResponse
     */
    export interface MoveAllStocksToCarrierResponse {
        operationId?: string;
        stockDto?: /* StockDto */ StockDto[];
        targetCarrierId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * MoveCarrierToPlaceResponse
     */
    export interface MoveCarrierToPlaceResponse {
        carrierId?: string; // uuid
        operationId?: string;
        rejected?: boolean;
        transferComplete?: boolean;
    }
    /**
     * MoveStockCmd
     */
    export interface MoveStockCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        tabNumber?: string;
        targetCarrierId?: string; // uuid
        targetPlaceId?: string; // uuid
        userName?: string;
    }
    /**
     * MoveStockCmdResponse
     */
    export interface MoveStockCmdResponse {
        movedQuantity?: number; // int32
        operationId?: string;
        originalStockId?: string; // uuid
        targetStockActualQuantity?: number;
        targetStockId?: string; // uuid
    }
    /**
     * MoveStockToCarrierCmd
     */
    export interface MoveStockToCarrierCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        sourcePlaceId?: string; // uuid
        sourceStockId?: string; // uuid
        tabNumber?: string;
        targetCarrierId?: string; // uuid
        transferId?: string; // uuid
        unit?: string;
        userName?: string;
    }
    /**
     * NamedId
     */
    export interface NamedId {
        label?: string;
        value?: string; // uuid
    }
    /**
     * OrderBoxDto
     */
    export interface OrderBoxDto {
        carrierId?: string; // uuid
        orderItems?: /* OutboundOrderItemDto */ OutboundOrderItemDto[];
    }
    /**
     * OrderByCriteria
     */
    export interface OrderByCriteria {
        field?: string;
        order?: "ASC" | "DESC";
    }
    /**
     * OutboundOrderCmd
     */
    export interface OutboundOrderCmd {
        operationId?: string;
        orderBoxes?: /* OrderBoxDto */ OrderBoxDto[];
        orderVersion?: string;
        outboundDeliveryNumber?: string;
        outboundTime?: string; // date-time
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        tabNumber?: string;
        unshippedOrderItems?: /* UnshippedOrderItemDto */ UnshippedOrderItemDto[];
        userName?: string;
    }
    /**
     * OutboundOrderItemDto
     */
    export interface OutboundOrderItemDto {
        baseAmount?: number;
        baseUnit?: string;
        position?: number; // int64
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number; // int32
        unit?: string;
    }
    /**
     * OverridePlaceCommonCharacteristicCmd
     */
    export interface OverridePlaceCommonCharacteristicCmd {
        characteristic?: string;
        siteId?: string; // uuid
        value?: /* ValueCmd */ ValueCmd;
    }
    /**
     * PageRequestInfo
     */
    export interface PageRequestInfo {
        limit?: number; // int32
        offset?: number; // int32
    }
    /**
     * PickerRouteReq
     */
    export interface PickerRouteReq {
        pickingZoneId?: string; // uuid
        siteCode?: string;
        startPlaceId?: string; // uuid
        transferIds?: string /* uuid */[];
    }
    /**
     * PickingZoneTransferReq
     */
    export interface PickingZoneTransferReq {
        carrierId?: string; // uuid
        destinationPlaceId?: string; // uuid
        operationId?: string;
        startPlaceId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * PlaceCharacteristicDto
     */
    export interface PlaceCharacteristicDto {
        author?: string;
        characteristicId?: string; // uuid
        checkParents?: string;
        createdTime?: number; // int32
        editable?: boolean;
        editor?: string;
        mandatory?: boolean;
        placeId?: string; // uuid
        siteId?: string; // uuid
        type?: string;
        typeCharacteristicId?: string; // uuid
        updatedTime?: number; // int32
        value?: /* ValueDto */ ValueDto;
    }
    /**
     * PlaceDto
     */
    export interface PlaceDto {
        address?: string;
        addressTypeId?: string; // uuid
        author?: string;
        childPlaces?: /* PlaceDto */ PlaceDto[];
        coordinates?: /* CoordinatesDto */ CoordinatesDto;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        number?: number; // int32
        parentId?: string; // uuid
        parentPlaces?: /* PlaceDto */ PlaceDto[];
        siteId?: string; // uuid
        status?: /* PlaceStatusDto */ PlaceStatusDto;
        statusReason?: string;
        storagePlace?: boolean;
        typeCode?: string;
        typeId?: string; // uuid
        typeName?: string;
        updatedTime?: number; // int32
        zones?: /* ZoneInfo */ ZoneInfo[];
    }
    /**
     * PlaceHierarchyFilter
     */
    export interface PlaceHierarchyFilter {
        hierarchyElements?: /* PlaceHierarchyFilterCondition */ PlaceHierarchyFilterCondition[];
        siteId?: string; // uuid
    }
    /**
     * PlaceHierarchyFilterCondition
     */
    export interface PlaceHierarchyFilterCondition {
        criteria?: string;
        placeTypeId?: string; // uuid
    }
    /**
     * PlaceHierarchyTreeDto
     */
    export interface PlaceHierarchyTreeDto {
        address?: string;
        characteristics?: /* CharacteristicDto */ CharacteristicDto[];
        children?: /* PlaceHierarchyTreeDto */ PlaceHierarchyTreeDto[];
        id?: string; // uuid
        number?: number; // int32
        status?: string;
        type?: /* Type */ Type;
        zones?: /* Zone */ Zone[];
    }
    /**
     * PlaceInfo
     */
    export interface PlaceInfo {
        address?: string;
        author?: string;
        childrenAmount?: number; // int32
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        placeZones?: /* PlaceZoneInfo */ PlaceZoneInfo[];
        siteId?: string; // uuid
        siteName?: string;
        typeName?: string;
        updatedTime?: number; // int32
        zoneNames?: string[];
    }
    /**
     * PlaceNodeWithPosition
     */
    export interface PlaceNodeWithPosition {
        childPlaces?: /* Data */ Data[];
        placeNodeChainFromTop?: /* NamedId */ NamedId[];
    }
    /**
     * PlaceSearchFilter
     */
    export interface PlaceSearchFilter {
        hierarchy?: /* PlaceHierarchyFilter */ PlaceHierarchyFilter;
        placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
        placeTypes?: string /* uuid */[];
        zonesType?: /* PlaceZonesFilter */ PlaceZonesFilter[];
    }
    /**
     * PlaceShortView
     */
    export interface PlaceShortView {
        barcode?: string;
        description?: string;
        id?: string; // uuid
        siteCode?: string;
        typeCode?: string;
        typeName?: string;
    }
    /**
     * PlaceStatusDto
     */
    export interface PlaceStatusDto {
        code?: string;
        default?: boolean;
        title?: string;
    }
    /**
     * PlaceTypeCharacteristicDTO
     */
    export interface PlaceTypeCharacteristicDTO {
        author?: string;
        characteristicId?: string; // uuid
        checkParents?: string;
        createdTime?: number; // int32
        defaultValue?: /* DefaultValue */ DefaultValue;
        editable?: boolean;
        editor?: string;
        mandatory?: boolean;
        placeTypeId?: string; // uuid
        type?: string;
        updatedTime?: number; // int32
    }
    /**
     * PlaceTypeCodeExistsQuery
     */
    export interface PlaceTypeCodeExistsQuery {
        code?: string;
    }
    /**
     * PlaceTypeDto
     */
    export interface PlaceTypeDto {
        author?: string;
        code?: string;
        coordinatesRequired?: boolean;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        maxMixBatches?: number; // int32
        name?: string;
        numberOfImplementation?: number; // int64
        numerationRule?: string;
        placeTypeId?: string; // uuid
        siteId?: string; // uuid
        storagePlace?: boolean;
        updatedTime?: number; // int32
    }
    /**
     * PlaceWithPosition
     */
    export interface PlaceWithPosition {
        place?: /* PlaceDto */ PlaceDto;
        placeNodeChainFromTop?: /* NamedId */ NamedId[];
    }
    /**
     * PlaceWithType
     */
    export interface PlaceWithType {
        address?: string;
        id?: string; // uuid
        number?: number; // int32
        type?: /* Type */ Type;
    }
    /**
     * PlaceZoneInfo
     */
    export interface PlaceZoneInfo {
        zoneCode?: string;
        zoneName?: string;
        zoneTypeCode?: string;
        zoneTypeName?: string;
    }
    /**
     * PlaceZonesFilter
     */
    export interface PlaceZonesFilter {
        zoneTypeId?: string; // uuid
        zonesId?: string /* uuid */[];
    }
    /**
     * PlacesSearchQuery
     */
    export interface PlacesSearchQuery {
        filter?: /* PlaceSearchFilter */ PlaceSearchFilter;
        orderInfo?: /* OrderByCriteria */ OrderByCriteria[];
        pageInfo?: /* PageRequestInfo */ PageRequestInfo;
    }
    /**
     * PlacesSearchResult
     */
    export interface PlacesSearchResult {
        places?: /* PlaceInfo */ PlaceInfo[];
        totalCount?: number; // int32
    }
    /**
     * ProductAmountDto
     */
    export interface ProductAmountDto {
        productId?: string;
        quantity?: number;
        rejectionReason?: "NO_STOCK" | "NO_ROUTE";
        unit?: string;
    }
    /**
     * ProductBatchBalanceByStockType
     */
    export interface ProductBatchBalanceByStockType {
        baseAmountERP?: number;
        baseAmountWMS?: number;
        quantityERP?: number; // int32
        quantityWMS?: number;
        stockTypeCode?: string;
    }
    /**
     * ProductBatchBalanceDifferenceDto
     */
    export interface ProductBatchBalanceDifferenceDto {
        canCorrect?: boolean;
        needCorrect?: boolean;
        stockTypes?: /* ProductBatchBalanceByStockType */ ProductBatchBalanceByStockType[];
    }
    /**
     * ProductBatchDto
     */
    export interface ProductBatchDto {
        expirationTime?: number; // int32
        id?: string; // uuid
        inboundId?: string; // uuid
        inboundTime?: number; // int32
        manufactureTime?: number; // int32
        mercuryExtId?: string;
        number?: string;
        productId?: string;
        productName?: string;
        siteId?: string; // uuid
        type?: "REGULAR" | "RETURN";
        vendorCode?: string;
        vendorName?: string;
        weightProduct?: boolean;
    }
    /**
     * ProductBatchSearchDto
     */
    export interface ProductBatchSearchDto {
        confirmed?: boolean;
        expirationTime?: number; // int32
        id?: string; // uuid
        inboundTime?: number; // int32
        manufactureTime?: number; // int32
        productBatchNumber?: string;
        productId?: string;
        productName?: string;
        siteCode?: string;
        siteId?: string; // uuid
        type?: "REGULAR" | "RETURN";
        vendorCode?: string;
        vendorName?: string;
    }
    /**
     * ProductBatchSearchFilter
     */
    export interface ProductBatchSearchFilter {
        confirmed?: boolean;
        expirationDateFrom?: string; // date-time
        expirationDateTo?: string; // date-time
        manufactureTimeFrom?: string; // date-time
        manufactureTimeTo?: string; // date-time
        number?: string;
        productId?: string;
        siteCode?: string;
        siteId?: string; // uuid
        types?: ("REGULAR" | "RETURN")[];
    }
    /**
     * ProductBatchesSearchQuery
     */
    export interface ProductBatchesSearchQuery {
        filter?: /* ProductBatchSearchFilter */ ProductBatchSearchFilter;
        orderInfo?: /* OrderByCriteria */ OrderByCriteria[];
        pageInfo?: /* PageRequestInfo */ PageRequestInfo;
    }
    /**
     * ProductBatchesSearchResult
     */
    export interface ProductBatchesSearchResult {
        productBatches?: /* ProductBatchSearchDto */ ProductBatchSearchDto[];
        totalCount?: number; // int64
    }
    /**
     * ProductPlacementRequest
     */
    export interface ProductPlacementRequest {
        openCarriers?: string /* uuid */[];
        operationId?: string;
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        siteCode?: string;
        stockTypeCode?: string;
    }
    /**
     * ProductPlacementResponse
     */
    export interface ProductPlacementResponse {
        clientError?: /* Error */ Error;
        operationId?: string;
        productId?: string;
        rejectedInfo?: /* RejectedInfo */ RejectedInfo;
        reservedCarriers?: /* ReservedCarrier */ ReservedCarrier[];
    }
    /**
     * RawPlacementRequest
     */
    export interface RawPlacementRequest {
        incomingCarrierId?: string; // uuid
        openCarriers?: string /* uuid */[];
        operationId?: string;
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        siteCode?: string;
        stockTypeCode?: string;
    }
    /**
     * RawStock
     */
    export interface RawStock {
        position?: number; // int64
        quantity?: number;
        stockId?: string; // uuid
    }
    /**
     * ReduceIncomingQuantityCmd
     */
    export interface ReduceIncomingQuantityCmd {
        operationId?: string;
        productBatchId?: string; // uuid
        quantity?: number; // int32
        stockTypeCode?: string;
        transferId?: string; // uuid
    }
    /**
     * ReducePlaceReservationForTransferCmd
     */
    export interface ReducePlaceReservationForTransferCmd {
        carrierTypeCode?: string;
        operationId?: string;
        placeId?: string; // uuid
        quantity?: number; // int32
        siteCode?: string;
        transferId?: string; // uuid
    }
    /**
     * RejectedInfo
     */
    export interface RejectedInfo {
        clientError?: /* Error */ Error;
        quantity?: number;
    }
    /**
     * ReplenishCmd
     */
    export interface ReplenishCmd {
        destPlaceBarcode?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number; // int32
        siteCode?: string;
        srcCarrierId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        targetStockTypeCode?: string;
        userName?: string;
    }
    /**
     * ReservationCancelledResponse
     */
    export interface ReservationCancelledResponse {
        incomingQuantity?: number;
        operationId?: string;
        outgoingQuantity?: number;
        siteCode?: string;
        stockId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * ReservationDto
     */
    export interface ReservationDto {
        batchId?: string; // uuid
        placeAddress?: string;
        placeId?: string; // uuid
        productId?: string;
        quantityIncoming?: number;
        quantityOutgoing?: number;
        stockId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * ReservationInfoDto
     */
    export interface ReservationInfoDto {
        batchId?: string; // uuid
        incomingQuantity?: number;
        outgoingQuantity?: number;
        productId?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
        unit?: string;
    }
    /**
     * ReserveCarrierForProcessCmd
     */
    export interface ReserveCarrierForProcessCmd {
        carrierNumber?: string;
        operationId?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * ReserveCarrierForProcessResponse
     */
    export interface ReserveCarrierForProcessResponse {
        clientError?: /* Error */ Error;
        operationId?: string;
    }
    /**
     * ReserveInfo
     */
    export interface ReserveInfo {
        placeAddress?: string;
        placeId?: string; // uuid
        transfer?: /* TransferDto */ TransferDto;
    }
    /**
     * ReservePlaceForProcessCmd
     */
    export interface ReservePlaceForProcessCmd {
        operationId?: string;
        placeBarcode?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * ReservePlacesForCarriersCmd
     */
    export interface ReservePlacesForCarriersCmd {
        carrierPlaceTypeCode?: string;
        carriers?: /* TransferDto */ TransferDto[];
        operationId?: string;
        processId?: string;
        processName?: string;
        processPlaceTypeCode?: string;
        reservationMode?: "FULL" | "PARTIAL";
        siteCode?: string;
        zones?: /* ZoneReference */ ZoneReference[];
    }
    /**
     * ReserveStockOutgoingQuantityResponse
     */
    export interface ReserveStockOutgoingQuantityResponse {
        operationId?: string;
        reservation?: /* ReservationDto */ ReservationDto;
    }
    /**
     * ReservedCarrier
     */
    export interface ReservedCarrier {
        openNewCarrier?: boolean;
        openedCarrierId?: string; // uuid
        quantity?: number;
    }
    /**
     * ReservedPlacesForCarriersResponse
     */
    export interface ReservedPlacesForCarriersResponse {
        clientError?: /* Error */ Error;
        operationId?: string;
        processId?: string;
        processName?: string;
        rejectedTransfers?: /* TransferDto */ TransferDto[];
        reserves?: /* ReserveInfo */ ReserveInfo[];
        siteCode?: string;
    }
    /**
     * Route
     */
    export interface Route {
        steps?: /* RouteStep */ RouteStep[];
    }
    /**
     * RouteStep
     */
    export interface RouteStep {
        allocationSector?: /* ZoneWithType */ ZoneWithType;
        allocationZone?: /* ZoneWithType */ ZoneWithType;
        characteristics?: /* CharacteristicDto */ CharacteristicDto[];
        floor?: /* PlaceWithType */ PlaceWithType;
        hierarchy?: /* PlaceWithType */ PlaceWithType[];
        item?: /* StockAmountDto */ StockAmountDto;
        pickingSector?: /* ZoneWithType */ ZoneWithType;
        pickingZone?: /* ZoneWithType */ ZoneWithType;
        place?: /* PlaceWithType */ PlaceWithType;
        plannedDuration?: number; // int32
        transferId?: string; // uuid
        zones?: /* ZoneWithType */ ZoneWithType[];
    }
    /**
     * ShipCarrierCmd
     */
    export interface ShipCarrierCmd {
        carrierId?: string; // uuid
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        tabNumber?: string;
        userName?: string;
    }
    /**
     * SiteCodeExistsQuery
     */
    export interface SiteCodeExistsQuery {
        code?: string;
    }
    /**
     * SiteDto
     */
    export interface SiteDto {
        archived?: boolean;
        author?: string;
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        name?: string;
        updatedTime?: number; // int32
    }
    /**
     * SiteSettingDto
     */
    export interface SiteSettingDto {
        bigDecimalValue?: number;
        code?: "MAX_MIX_PRODUCTS";
        intValue?: number; // int32
        stringValue?: string;
        type?: "INTEGER" | "STRING" | "BIG_DECIMAL";
    }
    /**
     * SlottingRoute
     */
    export interface SlottingRoute {
        plannedDuration?: number; // int32
        steps?: /* SlottingRouteStep */ SlottingRouteStep[];
        transferId?: string; // uuid
    }
    /**
     * SlottingRouteStep
     */
    export interface SlottingRouteStep {
        characteristics?: /* CharacteristicDto */ CharacteristicDto[];
        hierarchy?: /* PlaceWithType */ PlaceWithType[];
        place?: /* PlaceWithType */ PlaceWithType;
        plannedDuration?: number; // int32
        reservation?: /* ReservationInfoDto */ ReservationInfoDto;
        transferId?: string; // uuid
        zones?: /* ZoneWithType */ ZoneWithType[];
    }
    /**
     * StockAddressesAndProductIdsSearchFilter
     */
    export interface StockAddressesAndProductIdsSearchFilter {
        addresses?: string[];
        productIds?: string[];
        siteId?: string; // uuid
    }
    /**
     * StockAddressesAndProductIdsSearchQuery
     */
    export interface StockAddressesAndProductIdsSearchQuery {
        filter?: /* StockAddressesAndProductIdsSearchFilter */ StockAddressesAndProductIdsSearchFilter;
        orderInfo?: /* OrderByCriteria */ OrderByCriteria[];
        pageInfo?: /* PageRequestInfo */ PageRequestInfo;
    }
    /**
     * StockAmountDto
     */
    export interface StockAmountDto {
        productId?: string;
        quantity?: number;
        stockId?: string; // uuid
        unit?: string;
    }
    /**
     * StockDto
     */
    export interface StockDto {
        actualQuantity?: number;
        actualVolume?: number; // int32
        actualWeight?: number; // int32
        availableQuantity?: number;
        availableVolume?: number; // int32
        availableWeight?: number; // int32
        baseUnit?: string;
        batchConfirmed?: boolean;
        batchId?: string; // uuid
        batchNumber?: string;
        carrierId?: string; // uuid
        incomingQuantity?: number;
        incomingVolume?: number; // int32
        incomingWeight?: number; // int32
        outgoingQuantity?: number;
        outgoingVolume?: number; // int32
        outgoingWeight?: number; // int32
        placeId?: string; // uuid
        productId?: string;
        productName?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
        unit?: string;
    }
    /**
     * StockHierarchySearchFilter
     */
    export interface StockHierarchySearchFilter {
        hierarchy?: /* PlaceHierarchyFilter */ PlaceHierarchyFilter;
        placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
        placeTypes?: string /* uuid */[];
        productIds?: string[];
        zonesType?: /* PlaceZonesFilter */ PlaceZonesFilter[];
    }
    /**
     * StockHierarchySearchQuery
     */
    export interface StockHierarchySearchQuery {
        filter?: /* StockHierarchySearchFilter */ StockHierarchySearchFilter;
        orderInfo?: /* OrderByCriteria */ OrderByCriteria[];
        pageInfo?: /* PageRequestInfo */ PageRequestInfo;
    }
    /**
     * StockInCmd
     */
    export interface StockInCmd {
        baseAmount?: number;
        baseUnit?: string;
        carrierId?: string; // uuid
        documentDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        placeId?: string; // uuid
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        productBatchId?: string; // uuid
        quantity?: number; // int32
        stockTypeCode?: string;
        tabNumber?: string;
        unit?: string;
        userName?: string;
    }
    /**
     * StockInResponse
     */
    export interface StockInResponse {
        operationId?: string;
        stockId?: string; // uuid
    }
    /**
     * StockInfo
     */
    export interface StockInfo {
        actual?: /* FullAmount */ FullAmount;
        available?: /* FullAmount */ FullAmount;
        batchConfirmed?: boolean;
        batchId?: string; // uuid
        batchNumber?: string;
        carrier?: boolean;
        expirationTime?: number; // int32
        inbound?: /* FullAmount */ FullAmount;
        inboundTime?: number; // int32
        manufactureTime?: number; // int32
        outbound?: /* FullAmount */ FullAmount;
        placeAddress?: string;
        placeId?: string; // uuid
        productId?: string;
        productName?: string;
        sellByTime?: number; // int32
        stockTypeCode?: string;
        stockTypeId?: string; // uuid
    }
    /**
     * StockTypeDto
     */
    export interface StockTypeDto {
        author?: string;
        code?: string;
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        name?: string;
        updatedTime?: number; // int32
    }
    /**
     * StocksSearchResult
     */
    export interface StocksSearchResult {
        stocks?: /* StockInfo */ StockInfo[];
        totalCount?: number; // int32
    }
    /**
     * TransferDroppedResponse
     */
    export interface TransferDroppedResponse {
        canceledReservations?: /* ReservationDto */ ReservationDto[];
        transferId?: string; // uuid
    }
    /**
     * TransferDto
     */
    export interface TransferDto {
        carrierTypeCode?: string;
        quantity?: number; // int32
        transferId?: string; // uuid
    }
    /**
     * TryAllocateSpaceForReplenishmentCmd
     */
    export interface TryAllocateSpaceForReplenishmentCmd {
        destPlaceBarcode?: string;
        operationId?: string;
        processId?: string;
        productBatchId?: string; // uuid
        quantity?: number; // int32
        siteCode?: string;
        srcCarrierId?: string; // uuid
        stockTypeCode?: string;
    }
    /**
     * TryAllocateSpaceForReplenishmentResponse
     */
    export interface TryAllocateSpaceForReplenishmentResponse {
        approvedQuantity?: number;
        operationId?: string;
        rejectedInfo?: /* RejectedInfo */ RejectedInfo;
    }
    /**
     * TryTakeRequest
     */
    export interface TryTakeRequest {
        operationId?: string;
        placeId?: string; // uuid
        quantity?: number; // int32
        stockId?: string; // uuid
        transferId?: string; // uuid
        unit?: string;
    }
    /**
     * TryTakeResponse
     */
    export interface TryTakeResponse {
        acceptedQuantity?: number;
        operationId?: string;
        rejectReason?: string;
        rejected?: boolean;
        stockId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * Type
     */
    export interface Type {
        code?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * UnshippedOrderItemDto
     */
    export interface UnshippedOrderItemDto {
        baseAmount?: number;
        baseUnit?: string;
        position?: number; // int64
        productId?: string;
        quantity?: number; // int32
        unit?: string;
    }
    /**
     * UpdateAllocationSettingsRequest
     */
    export interface UpdateAllocationSettingsRequest {
        maxMixedProducts?: number; // int32
        placeTypeCode?: string;
        siteCode?: string;
        zoneCodes?: string[];
    }
    /**
     * UpdateCarrierTypeCmd
     */
    export interface UpdateCarrierTypeCmd {
        description?: string;
        height?: number; // int32
        length?: number; // int32
        maxVolume?: number;
        maxWeight?: number;
        name?: string;
        width?: number; // int32
    }
    /**
     * UpdatePlaceCmd
     */
    export interface UpdatePlaceCmd {
        address?: string;
        addressTypeId?: string; // uuid
        coordinates?: /* Coordinates3D */ Coordinates3D;
        description?: string;
        number?: number; // int32
        status?: /* PlaceStatusDto */ PlaceStatusDto;
        statusReason?: string;
        typeId?: string; // uuid
        zoneIds?: string /* uuid */[];
    }
    /**
     * UpdatePlaceTypeCmd
     */
    export interface UpdatePlaceTypeCmd {
        description?: string;
        maxMixBatches?: number; // int32
        name?: string;
        numerationRule?: "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP";
        storagePlace?: boolean;
    }
    /**
     * UpdatePlaceTypeCommonCharacteristicDto
     */
    export interface UpdatePlaceTypeCommonCharacteristicDto {
        characteristic?: string;
        defaultValue?: /* DefaultValue */ DefaultValue;
    }
    /**
     * UpdateSiteCmd
     */
    export interface UpdateSiteCmd {
        description?: string;
        name?: string;
    }
    /**
     * UpdateStockTypeByInventoryCmd
     */
    export interface UpdateStockTypeByInventoryCmd {
        baseAmount?: number;
        docDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        operationType?: "MOVE";
        orderItem?: number; // int32
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        saleAmount?: number; // int32
        salesOrder?: string;
        specStock?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        transferId?: string; // uuid
        updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
        userName?: string;
    }
    /**
     * UpdateStockTypeByPickingCmd
     */
    export interface UpdateStockTypeByPickingCmd {
        docDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        operationType?: "MOVE";
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        stockId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
        userName?: string;
    }
    /**
     * UpdateStockTypeEvent
     */
    export interface UpdateStockTypeEvent {
        actualQuantity?: number;
        batchId?: string; // uuid
        batchNumber?: string;
        beiQuantity?: number;
        beiUnit?: string;
        carrierId?: string; // uuid
        documentDate?: string; // date-time
        documentNumber?: string;
        expirationDate?: string; // date-time
        manufactureDate?: string; // date-time
        newStockId?: string; // uuid
        newStockTypeCode?: string;
        oldStockId?: string; // uuid
        oldStockTypeCode?: string;
        operationId?: string;
        operationType?: "MOVE";
        orderItemIndex?: number; // int32
        placeWithType?: /* PlaceWithType */ PlaceWithType;
        productId?: string;
        productName?: string;
        salesOrder?: string;
        shouldNotifyERP?: boolean;
        siteCode?: string;
        specialStockCode?: string;
        storageDate?: string; // date-time
        tabNumber?: string;
        unit?: string;
        updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
    }
    /**
     * UpdateStockTypeForBatchCmd
     */
    export interface UpdateStockTypeForBatchCmd {
        batchId?: string; // uuid
        newStockTypeCode?: string;
        oldStockTypeCode?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        tabNumber?: string;
        userName?: string;
    }
    /**
     * UpdateStockTypeInStockByUICmd
     */
    export interface UpdateStockTypeInStockByUICmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        userName?: string;
    }
    /**
     * UpdateZoneCmd
     */
    export interface UpdateZoneCmd {
        boundedWarehouseProductGroups?: /* WarehouseProductGroupDto */ WarehouseProductGroupDto[];
        code?: string;
        description?: string;
        name?: string;
    }
    /**
     * UpdateZoneTypeCmd
     */
    export interface UpdateZoneTypeCmd {
        description?: string;
        name?: string;
    }
    /**
     * ValueCmd
     */
    export interface ValueCmd {
        decValue?: number;
        intValue?: number; // int32
        maxIntValue?: number; // int32
        minIntValue?: number; // int32
    }
    /**
     * ValueDto
     */
    export interface ValueDto {
        decValue?: number;
        intValue?: number; // int32
        maxDecValue?: number;
        maxIntValue?: number; // int32
        minDecValue?: number;
        minIntValue?: number; // int32
    }
    /**
     * WarehouseProductGroupDto
     */
    export interface WarehouseProductGroupDto {
        code?: string;
    }
    /**
     * WriteOffLostStockCmd
     */
    export interface WriteOffLostStockCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        tabNumber?: string;
        userName?: string;
    }
    /**
     * WriteOffRawStocksCmd
     */
    export interface WriteOffRawStocksCmd {
        operationId?: string;
        orderVersion?: string;
        outboundDeliveryNumber?: string;
        outboundTime?: string; // date-time
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        stocks?: /* RawStock */ RawStock[];
        tabNumber?: string;
        userName?: string;
    }
    /**
     * WriteOffStockCmd
     */
    export interface WriteOffStockCmd {
        baseAmount?: number;
        baseUnit?: string;
        documentDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        tabNumber?: string;
        unit?: string;
        userName?: string;
    }
    /**
     * WriteOffStockResponse
     */
    export interface WriteOffStockResponse {
        clientError?: /* Error */ Error;
        operationId?: string;
        stockId?: string; // uuid
    }
    /**
     * Zone
     */
    export interface Zone {
        code?: string;
        id?: string; // uuid
        name?: string;
        type?: /* Type */ Type;
    }
    /**
     * ZoneCodeExistsQuery
     */
    export interface ZoneCodeExistsQuery {
        code?: string;
        siteId?: string; // uuid
        typeId?: string; // uuid
    }
    /**
     * ZoneDto
     */
    export interface ZoneDto {
        code?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * ZoneInfo
     */
    export interface ZoneInfo {
        code?: string;
        id?: string; // uuid
        name?: string;
        typeCode?: string;
        typeId?: string; // uuid
        typeName?: string;
    }
    /**
     * ZoneProjection
     */
    export interface ZoneProjection {
        author?: string;
        boundedWarehouseProductGroups?: /* WarehouseProductGroupDto */ WarehouseProductGroupDto[];
        childAmount?: number; // int32
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        name?: string;
        siteId?: string; // uuid
        typeId?: string; // uuid
        typeName?: string;
        updatedTime?: number; // int32
    }
    /**
     * ZoneReference
     */
    export interface ZoneReference {
        zoneCode?: string;
        zoneTypeCode?: string;
    }
    /**
     * ZoneTypeCodeExistsQuery
     */
    export interface ZoneTypeCodeExistsQuery {
        code?: string;
    }
    /**
     * ZoneTypeDto
     */
    export interface ZoneTypeDto {
        author?: string;
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        name?: string;
        numberOfImplementation?: number; // int64
        updatedTime?: number; // int32
        warehouseProductGroupCanBeBounded?: boolean;
    }
    /**
     * ZoneTypeWithZones
     */
    export interface ZoneTypeWithZones {
        code?: string;
        id?: string; // uuid
        name?: string;
        zones?: /* ZoneDto */ ZoneDto[];
    }
    /**
     * ZoneTypeWithZonesDto
     */
    export interface ZoneTypeWithZonesDto {
        code?: string;
        id?: string; // uuid
        name?: string;
        zones?: /* ZoneDto */ ZoneDto[];
    }
    /**
     * ZoneWithType
     */
    export interface ZoneWithType {
        code?: string;
        id?: string; // uuid
        name?: string;
        type?: /* Type */ Type;
    }
}
declare namespace Paths {
    namespace AddStockToCarrierUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* AddStockToCarrierCmd */ Definitions.AddStockToCarrierCmd;
        }
        namespace Responses {
            export type $200 = /* StockDto */ Definitions.StockDto;
        }
    }
    namespace AddUsingPOST {
        export interface BodyParameters {
            req: Parameters.Req;
        }
        namespace Parameters {
            export type Req = /* AddPlaceTypeCommonCharacteristicDto */ Definitions.AddPlaceTypeCommonCharacteristicDto;
        }
        namespace Responses {
            export type $200 = /* PlaceTypeCharacteristicDTO */ Definitions.PlaceTypeCharacteristicDTO;
        }
    }
    namespace AssignBatchUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* AssignProductBatchCmd */ Definitions.AssignProductBatchCmd;
        }
        namespace Responses {
            export type $200 = /* ProductBatchDto */ Definitions.ProductBatchDto;
        }
    }
    namespace BindToRouteStrategyUsingPOST {
        export interface FormDataParameters {
            siteCode: Parameters.SiteCode;
        }
        namespace Parameters {
            export type SiteCode = string;
        }
    }
    namespace BoxRouteUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* BuildBoxRouteCmd */ Definitions.BuildBoxRouteCmd;
        }
        namespace Responses {
            export type $200 = /* BoxRoute */ Definitions.BoxRoute;
        }
    }
    namespace CancelStockReservationUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CancelStockReservationCmd */ Definitions.CancelStockReservationCmd;
        }
        namespace Responses {
            export type $200 = /* ReservationCancelledResponse */ Definitions.ReservationCancelledResponse;
        }
    }
    namespace CarriersUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace CodeExistsUsingGET {
        namespace Responses {
            export type $200 = /* CodeExists */ Definitions.CodeExists;
        }
    }
    namespace CodeExistsUsingPOST {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* CarrierTypeCodeExistsQuery */ Definitions.CarrierTypeCodeExistsQuery;
        }
        namespace Responses {
            export type $200 = /* CodeExists */ Definitions.CodeExists;
        }
    }
    namespace CodeExistsUsingPOST1 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* PlaceTypeCodeExistsQuery */ Definitions.PlaceTypeCodeExistsQuery;
        }
        namespace Responses {
            export type $200 = /* CodeExists */ Definitions.CodeExists;
        }
    }
    namespace CodeExistsUsingPOST2 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* SiteCodeExistsQuery */ Definitions.SiteCodeExistsQuery;
        }
        namespace Responses {
            export type $200 = /* CodeExists */ Definitions.CodeExists;
        }
    }
    namespace CodeExistsUsingPOST3 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* ZoneCodeExistsQuery */ Definitions.ZoneCodeExistsQuery;
        }
        namespace Responses {
            export type $200 = /* CodeExists */ Definitions.CodeExists;
        }
    }
    namespace CodeExistsUsingPOST4 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* ZoneTypeCodeExistsQuery */ Definitions.ZoneTypeCodeExistsQuery;
        }
        namespace Responses {
            export type $200 = /* CodeExists */ Definitions.CodeExists;
        }
    }
    namespace ConfirmBatchUsingPOST {
        export interface BodyParameters {
            confirmProductBatchCmd: Parameters.ConfirmProductBatchCmd;
        }
        namespace Parameters {
            export type ConfirmProductBatchCmd = /* ConfirmProductBatchCmd */ Definitions.ConfirmProductBatchCmd;
        }
    }
    namespace ConfirmStocksUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ChangeRawStockTypeCmd */ Definitions.ChangeRawStockTypeCmd;
        }
    }
    namespace CreatePlaceTypeUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreatePlaceTypeCmd */ Definitions.CreatePlaceTypeCmd;
        }
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto;
        }
    }
    namespace CreateSiteUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreateSiteCmd */ Definitions.CreateSiteCmd;
        }
        namespace Responses {
            export type $200 = /* SiteDto */ Definitions.SiteDto;
        }
    }
    namespace CreateStockTypeUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ModifyStockTypeCmd */ Definitions.ModifyStockTypeCmd;
        }
        namespace Responses {
            export type $200 = /* StockTypeDto */ Definitions.StockTypeDto;
        }
    }
    namespace CreateTransferPlanAllocationUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreatePlanRequest */ Definitions.CreatePlanRequest;
        }
    }
    namespace CreateTransferPlanPickingUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreatePlanRequest */ Definitions.CreatePlanRequest;
        }
    }
    namespace CreateUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreateCarrierCmd */ Definitions.CreateCarrierCmd;
        }
        namespace Responses {
            export type $200 = /* CarrierDto */ Definitions.CarrierDto;
        }
    }
    namespace CreateUsingPOST1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreateCarrierTypeCmd */ Definitions.CreateCarrierTypeCmd;
        }
        namespace Responses {
            export type $200 = /* CarrierTypeDto */ Definitions.CarrierTypeDto;
        }
    }
    namespace CreateUsingPOST2 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreatePlaceCmd */ Definitions.CreatePlaceCmd;
        }
        namespace Responses {
            export type $200 = /* PlaceDto */ Definitions.PlaceDto;
        }
    }
    namespace CreateUsingPOST3 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreateZoneTypeCmd */ Definitions.CreateZoneTypeCmd;
        }
        namespace Responses {
            export type $200 = /* ZoneTypeDto */ Definitions.ZoneTypeDto;
        }
    }
    namespace CreateZoneUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* CreateZoneCmd */ Definitions.CreateZoneCmd;
        }
        namespace Responses {
            export type $200 = /* ZoneDto */ Definitions.ZoneDto;
        }
    }
    namespace DropAllReservationsForProcessUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* DropAllProcessReservationsCmd */ Definitions.DropAllProcessReservationsCmd;
        }
    }
    namespace DropReservationForProcessUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* DropCarrierReservationForProcessCmd */ Definitions.DropCarrierReservationForProcessCmd;
        }
    }
    namespace DropReservationForProcessUsingPOST1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* DropPlaceReservationForProcessCmd */ Definitions.DropPlaceReservationForProcessCmd;
        }
    }
    namespace DropReservationUsingDELETE {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* DropPlaceReservationsForTransferCmd */ Definitions.DropPlaceReservationsForTransferCmd;
        }
    }
    namespace DropTransferUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* DropTransferCmd */ Definitions.DropTransferCmd;
        }
        namespace Responses {
            export type $200 = /* TransferDroppedResponse */ Definitions.TransferDroppedResponse;
        }
    }
    namespace FindAllUsingPOST {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* DetailedStockSearchQuery */ Definitions.DetailedStockSearchQuery;
        }
        namespace Responses {
            export type $200 = /* DetailedStocksSearchResult */ Definitions.DetailedStocksSearchResult;
        }
    }
    namespace FindPlacesByProcessIdUsingGET {
        namespace Responses {
            export type $200 = /* PlaceDto */ Definitions.PlaceDto[];
        }
    }
    namespace FindTransferUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* PickingZoneTransferReq */ Definitions.PickingZoneTransferReq;
        }
        namespace Responses {
            export type $200 = /* Route */ Definitions.Route;
        }
    }
    namespace FindTransferUsingPOST1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* AllocationZoneTransferReq */ Definitions.AllocationZoneTransferReq;
        }
        namespace Responses {
            export type $200 = /* Route */ Definitions.Route;
        }
    }
    namespace GetAllUsingGET {
        namespace Responses {
            export type $200 = /* CarrierDto */ Definitions.CarrierDto[];
        }
    }
    namespace GetAllUsingGET1 {
        namespace Responses {
            export type $200 = /* ProductBatchDto */ Definitions.ProductBatchDto[];
        }
    }
    namespace GetAllUsingGET2 {
        namespace Responses {
            export type $200 = /* ReservationDto */ Definitions.ReservationDto[];
        }
    }
    namespace GetAllUsingGET3 {
        namespace Responses {
            export type $200 = /* StockDto */ Definitions.StockDto[];
        }
    }
    namespace GetAllUsingPOST {
        export interface BodyParameters {
            detailedCarriersSearchQuery: Parameters.DetailedCarriersSearchQuery;
        }
        namespace Parameters {
            export type DetailedCarriersSearchQuery = /* DetailedCarriersSearchQuery */ Definitions.DetailedCarriersSearchQuery;
        }
        namespace Responses {
            export type $200 = /* CarrierDto */ Definitions.CarrierDto[];
        }
    }
    namespace GetAvailablePlaceTypesByParentPlaceIdUsingGET {
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto[];
        }
    }
    namespace GetBatchBalanceDifferenceUsingGET {
        namespace Responses {
            export type $200 = /* ProductBatchBalanceDifferenceDto */ Definitions.ProductBatchBalanceDifferenceDto;
        }
    }
    namespace GetBulkUpdateOperationDataUsingGET {
        namespace Responses {
            export type $200 = /* BulkOperationDto */ Definitions.BulkOperationDto;
        }
    }
    namespace GetByIdUsingGET {
        namespace Responses {
            export type $200 = /* CarrierDto */ Definitions.CarrierDto;
        }
    }
    namespace GetByIdUsingGET1 {
        namespace Responses {
            export type $200 = /* ProductBatchDto */ Definitions.ProductBatchDto;
        }
    }
    namespace GetByIdUsingGET2 {
        namespace Responses {
            export type $200 = /* StockDto */ Definitions.StockDto;
        }
    }
    namespace GetByPlaceIdUsingGET {
        namespace Responses {
            export type $200 = /* CarrierGridView */ Definitions.CarrierGridView[];
        }
    }
    namespace GetCarrierTypeUsingGET {
        namespace Responses {
            export type $200 = /* CarrierTypeDto */ Definitions.CarrierTypeDto;
        }
    }
    namespace GetCarrierTypesUsingGET {
        namespace Responses {
            export type $200 = /* CarrierTypeDto */ Definitions.CarrierTypeDto[];
        }
    }
    namespace GetConsolidationZoneUsingGET {
        namespace Responses {
            export type $200 = /* ZoneDto */ Definitions.ZoneDto;
        }
    }
    namespace GetHierarchyByTypeCodeUsingGET {
        namespace Responses {
            export type $200 = /* PlaceHierarchyTreeDto */ Definitions.PlaceHierarchyTreeDto[];
        }
    }
    namespace GetHierarchyForParentIdUsingGET {
        namespace Responses {
            export type $200 = /* PlaceNodeWithPosition */ Definitions.PlaceNodeWithPosition;
        }
    }
    namespace GetHierarchyUsingGET {
        namespace Responses {
            export type $200 = /* PlaceNodeWithPosition */ Definitions.PlaceNodeWithPosition;
        }
    }
    namespace GetPlaceByCodeUsingGET {
        namespace Responses {
            export type $200 = /* PlaceShortView */ Definitions.PlaceShortView[];
        }
    }
    namespace GetPlaceByIdUsingGET {
        namespace Responses {
            export type $200 = /* PlaceWithPosition */ Definitions.PlaceWithPosition;
        }
    }
    namespace GetPlaceCharacteristicUsingGET {
        namespace Responses {
            export type $200 = /* PlaceCharacteristicDto */ Definitions.PlaceCharacteristicDto;
        }
    }
    namespace GetPlaceCharacteristicsUsingGET {
        namespace Responses {
            export type $200 = /* PlaceCharacteristicDto */ Definitions.PlaceCharacteristicDto[];
        }
    }
    namespace GetPlaceStatusesUsingGET {
        namespace Responses {
            export type $200 = /* PlaceStatusDto */ Definitions.PlaceStatusDto[];
        }
    }
    namespace GetPlaceTypeCharacteristicUsingGET {
        namespace Responses {
            export type $200 = /* PlaceTypeCharacteristicDTO */ Definitions.PlaceTypeCharacteristicDTO;
        }
    }
    namespace GetPlaceTypeCharacteristicsUsingGET {
        namespace Responses {
            export type $200 = /* PlaceTypeCharacteristicDTO */ Definitions.PlaceTypeCharacteristicDTO[];
        }
    }
    namespace GetPlaceTypeUsingGET {
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto;
        }
    }
    namespace GetPlaceTypesBySiteUsingGET {
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto[];
        }
    }
    namespace GetPlaceTypesBySiteUsingGET1 {
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto[];
        }
    }
    namespace GetPlaceUsingGET {
        namespace Responses {
            export type $200 = /* PlaceDto */ Definitions.PlaceDto;
        }
    }
    namespace GetRouteUsingPOST {
        namespace Responses {
            export type $200 = /* PlaceShortView */ Definitions.PlaceShortView[];
        }
    }
    namespace GetSettingsForSiteAndPlaceTypeUsingGET {
        namespace Responses {
            export type $200 = /* AllocationSettingsDto */ Definitions.AllocationSettingsDto[];
        }
    }
    namespace GetSettingsForSiteUsingGET {
        namespace Responses {
            export type $200 = /* SiteSettingDto */ Definitions.SiteSettingDto[];
        }
    }
    namespace GetSiteByCodeUsingGET {
        namespace Responses {
            export type $200 = /* SiteDto */ Definitions.SiteDto;
        }
    }
    namespace GetSiteUsingGET {
        namespace Responses {
            export type $200 = /* SiteDto */ Definitions.SiteDto;
        }
    }
    namespace GetSitesUsingGET {
        namespace Responses {
            export type $200 = /* SiteDto */ Definitions.SiteDto[];
        }
    }
    namespace GetStockTypeByCodeUsingGET {
        namespace Responses {
            export type $200 = /* StockTypeDto */ Definitions.StockTypeDto;
        }
    }
    namespace GetStockTypeByIdUsingGET {
        namespace Responses {
            export type $200 = /* StockTypeDto */ Definitions.StockTypeDto;
        }
    }
    namespace GetStockTypesUsingGET {
        namespace Responses {
            export type $200 = /* StockTypeDto */ Definitions.StockTypeDto[];
        }
    }
    namespace GetTransferPlaceUsingGET {
        namespace Responses {
            export type $200 = /* PlaceShortView */ Definitions.PlaceShortView;
        }
    }
    namespace GetTransferUsingGET {
        namespace Responses {
            export type $200 = /* Route */ Definitions.Route;
        }
    }
    namespace GetZoneByIdUsingGET {
        namespace Responses {
            export type $200 = /* ZoneDto */ Definitions.ZoneDto;
        }
    }
    namespace GetZoneTypeUsingGET {
        namespace Responses {
            export type $200 = /* ZoneTypeDto */ Definitions.ZoneTypeDto;
        }
    }
    namespace GetZoneTypesUsingGET {
        namespace Responses {
            export type $200 = /* ZoneDto */ Definitions.ZoneDto[];
        }
    }
    namespace GetZoneTypesUsingGET1 {
        namespace Responses {
            export type $200 = /* ZoneTypeDto */ Definitions.ZoneTypeDto[];
        }
    }
    namespace GetZoneTypesUsingGET2 {
        namespace Responses {
            export type $200 = /* ZoneTypeDto */ Definitions.ZoneTypeDto[];
        }
    }
    namespace GetZoneTypesWithZonesUsingGET {
        namespace Responses {
            export type $200 = /* ZoneTypeWithZones */ Definitions.ZoneTypeWithZones[];
        }
    }
    namespace GetZoneTypesWithZonesUsingGET1 {
        namespace Responses {
            export type $200 = /* ZoneTypeWithZonesDto */ Definitions.ZoneTypeWithZonesDto[];
        }
    }
    namespace GetZonesBySiteIdAndTypeIdUsingGET {
        namespace Responses {
            export type $200 = /* ZoneProjection */ Definitions.ZoneProjection[];
        }
    }
    namespace GetZonesByTypeUsingGET {
        namespace Responses {
            export type $200 = /* ZoneDto */ Definitions.ZoneDto[];
        }
    }
    namespace MonoPalletProductPlacementUsingPOST {
        export interface BodyParameters {
            request: Parameters.Request;
        }
        namespace Parameters {
            export type Request = /* MonoPalletPlacementRequest */ Definitions.MonoPalletPlacementRequest;
        }
        namespace Responses {
            export type $200 = /* ProductPlacementResponse */ Definitions.ProductPlacementResponse;
        }
    }
    namespace MoveCarrierToPlaceUsingPUT {
        namespace Responses {
            export type $200 = /* MoveCarrierToPlaceResponse */ Definitions.MoveCarrierToPlaceResponse;
        }
    }
    namespace MoveStockToCarrierUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* MoveAllStocksToCarrierCmd */ Definitions.MoveAllStocksToCarrierCmd;
        }
        namespace Responses {
            export type $200 = /* MoveAllStocksToCarrierResponse */ Definitions.MoveAllStocksToCarrierResponse;
        }
    }
    namespace MoveStockToCarrierUsingPUT1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* MoveStockToCarrierCmd */ Definitions.MoveStockToCarrierCmd;
        }
        namespace Responses {
            export type $200 = /* StockDto */ Definitions.StockDto;
        }
    }
    namespace MoveStockUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* MoveStockCmd */ Definitions.MoveStockCmd;
        }
        namespace Responses {
            export type $200 = /* MoveStockCmdResponse */ Definitions.MoveStockCmdResponse;
        }
    }
    namespace OutboundOrderUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* OutboundOrderCmd */ Definitions.OutboundOrderCmd;
        }
        namespace Responses {
            export type $200 = /* StockDto */ Definitions.StockDto[];
        }
    }
    namespace OutboundOrderUsingPOST1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* OutboundOrderCmd */ Definitions.OutboundOrderCmd;
        }
        namespace Responses {
            export type $200 = /* StockDto */ Definitions.StockDto[];
        }
    }
    namespace OverrideUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* OverridePlaceCommonCharacteristicCmd */ Definitions.OverridePlaceCommonCharacteristicCmd;
        }
        namespace Responses {
            export type $200 = /* PlaceCharacteristicDto */ Definitions.PlaceCharacteristicDto;
        }
    }
    namespace PickerRoutesUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* PickerRouteReq */ Definitions.PickerRouteReq;
        }
        namespace Responses {
            export type $200 = /* Route */ Definitions.Route;
        }
    }
    namespace PickingZonesUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace PlaceTypesUsingGET {
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto[];
        }
    }
    namespace PlacesUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace ProductPlacementUsingPOST {
        export interface BodyParameters {
            request: Parameters.Request;
        }
        namespace Parameters {
            export type Request = /* ProductPlacementRequest */ Definitions.ProductPlacementRequest;
        }
        namespace Responses {
            export type $200 = /* ProductPlacementResponse */ Definitions.ProductPlacementResponse;
        }
    }
    namespace RawPlacementUsingPOST {
        export interface BodyParameters {
            request: Parameters.Request;
        }
        namespace Parameters {
            export type Request = /* RawPlacementRequest */ Definitions.RawPlacementRequest;
        }
        namespace Responses {
            export type $200 = /* ProductPlacementResponse */ Definitions.ProductPlacementResponse;
        }
    }
    namespace ReduceIncomingQuantityUsingPOST {
        export interface BodyParameters {
            request: Parameters.Request;
        }
        namespace Parameters {
            export type Request = /* ReduceIncomingQuantityCmd */ Definitions.ReduceIncomingQuantityCmd;
        }
    }
    namespace ReduceReservationForTransferUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ReducePlaceReservationForTransferCmd */ Definitions.ReducePlaceReservationForTransferCmd;
        }
    }
    namespace ReplenishUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ReplenishCmd */ Definitions.ReplenishCmd;
        }
    }
    namespace ReserveForProcessUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ReserveCarrierForProcessCmd */ Definitions.ReserveCarrierForProcessCmd;
        }
        namespace Responses {
            export type $200 = /* ReserveCarrierForProcessResponse */ Definitions.ReserveCarrierForProcessResponse;
        }
    }
    namespace ReserveForProcessUsingPOST1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ReservePlaceForProcessCmd */ Definitions.ReservePlaceForProcessCmd;
        }
    }
    namespace ReserveOutgoingUsingPOST {
        namespace Responses {
            export type $200 = /* ReserveStockOutgoingQuantityResponse */ Definitions.ReserveStockOutgoingQuantityResponse;
        }
    }
    namespace ReserveUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ReservePlacesForCarriersCmd */ Definitions.ReservePlacesForCarriersCmd;
        }
        namespace Responses {
            export type $200 = /* ReservedPlacesForCarriersResponse */ Definitions.ReservedPlacesForCarriersResponse;
        }
    }
    namespace RouteStartRulesUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace SearchUsingPOST {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* PlacesSearchQuery */ Definitions.PlacesSearchQuery;
        }
        namespace Responses {
            export type $200 = /* PlacesSearchResult */ Definitions.PlacesSearchResult;
        }
    }
    namespace SearchUsingPOST1 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* ProductBatchesSearchQuery */ Definitions.ProductBatchesSearchQuery;
        }
        namespace Responses {
            export type $200 = /* ProductBatchesSearchResult */ Definitions.ProductBatchesSearchResult;
        }
    }
    namespace SearchUsingPOST2 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* StockAddressesAndProductIdsSearchQuery */ Definitions.StockAddressesAndProductIdsSearchQuery;
        }
        namespace Responses {
            export type $200 = /* StocksSearchResult */ Definitions.StocksSearchResult;
        }
    }
    namespace SearchUsingPOST3 {
        export interface BodyParameters {
            query: Parameters.Query;
        }
        namespace Parameters {
            export type Query = /* StockHierarchySearchQuery */ Definitions.StockHierarchySearchQuery;
        }
        namespace Responses {
            export type $200 = /* StocksSearchResult */ Definitions.StocksSearchResult;
        }
    }
    namespace ShipUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ShipCarrierCmd */ Definitions.ShipCarrierCmd;
        }
        namespace Responses {
            export type $200 = /* CarrierShippedResponse */ Definitions.CarrierShippedResponse;
        }
    }
    namespace SlottingRouteUsingGET {
        namespace Responses {
            export type $200 = /* SlottingRoute */ Definitions.SlottingRoute;
        }
    }
    namespace StartBulkUpdatePlaceStatusUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* BulkPlaceUpdateStatusCmd */ Definitions.BulkPlaceUpdateStatusCmd;
        }
        namespace Responses {
            export type $200 = /* BulkOperationDto */ Definitions.BulkOperationDto;
        }
    }
    namespace StartBulkUpdatePlaceTypeUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* BulkPlaceUpdateTypeCmd */ Definitions.BulkPlaceUpdateTypeCmd;
        }
        namespace Responses {
            export type $200 = /* BulkOperationDto */ Definitions.BulkOperationDto;
        }
    }
    namespace StartBulkUpdateZoneUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* BulkPlaceUpdateZoneCmd */ Definitions.BulkPlaceUpdateZoneCmd;
        }
        namespace Responses {
            export type $200 = /* BulkOperationDto */ Definitions.BulkOperationDto;
        }
    }
    namespace StockInUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* StockInCmd */ Definitions.StockInCmd;
        }
        namespace Responses {
            export type $200 = /* StockInResponse */ Definitions.StockInResponse;
        }
    }
    namespace StocksUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace StorageZonesUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace TryAllocateSpaceManualUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* TryAllocateSpaceForReplenishmentCmd */ Definitions.TryAllocateSpaceForReplenishmentCmd;
        }
        namespace Responses {
            export type $200 = /* TryAllocateSpaceForReplenishmentResponse */ Definitions.TryAllocateSpaceForReplenishmentResponse;
        }
    }
    namespace TryAllocateSpaceUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* TryAllocateSpaceForReplenishmentCmd */ Definitions.TryAllocateSpaceForReplenishmentCmd;
        }
        namespace Responses {
            export type $200 = /* TryAllocateSpaceForReplenishmentResponse */ Definitions.TryAllocateSpaceForReplenishmentResponse;
        }
    }
    namespace TryTakeUsingPOST {
        export interface BodyParameters {
            tryTakeRequest: Parameters.TryTakeRequest;
        }
        namespace Parameters {
            export type TryTakeRequest = /* TryTakeRequest */ Definitions.TryTakeRequest;
        }
        namespace Responses {
            export type $200 = /* TryTakeResponse */ Definitions.TryTakeResponse;
        }
    }
    namespace UpdateAllocationSettingsUsingPOST {
        export interface BodyParameters {
            request: Parameters.Request;
        }
        namespace Parameters {
            export type Request = /* UpdateAllocationSettingsRequest */ Definitions.UpdateAllocationSettingsRequest;
        }
        namespace Responses {
            export type $200 = /* AllocationSettingsDto */ Definitions.AllocationSettingsDto[];
        }
    }
    namespace UpdatePlaceTypeUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdatePlaceTypeCmd */ Definitions.UpdatePlaceTypeCmd;
        }
        namespace Responses {
            export type $200 = /* PlaceTypeDto */ Definitions.PlaceTypeDto;
        }
    }
    namespace UpdateSettingsForSiteUsingPUT {
        export interface BodyParameters {
            siteSettingDtos: Parameters.SiteSettingDtos;
        }
        namespace Parameters {
            export type SiteSettingDtos = /* SiteSettingDto */ Definitions.SiteSettingDto[];
        }
        namespace Responses {
            export type $200 = /* SiteSettingDto */ Definitions.SiteSettingDto[];
        }
    }
    namespace UpdateSiteUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdateSiteCmd */ Definitions.UpdateSiteCmd;
        }
        namespace Responses {
            export type $200 = /* SiteDto */ Definitions.SiteDto;
        }
    }
    namespace UpdateStockTypeForBatchUsingPOST {
        export interface BodyParameters {
            updateStockTypeForBatchCmd: Parameters.UpdateStockTypeForBatchCmd;
        }
        namespace Parameters {
            export type UpdateStockTypeForBatchCmd = /* UpdateStockTypeForBatchCmd */ Definitions.UpdateStockTypeForBatchCmd;
        }
    }
    namespace UpdateStockTypeInStockByInventoryUsingPOST {
        export interface BodyParameters {
            updateStockTypeCmd: Parameters.UpdateStockTypeCmd;
        }
        namespace Parameters {
            export type UpdateStockTypeCmd = /* UpdateStockTypeByInventoryCmd */ Definitions.UpdateStockTypeByInventoryCmd;
        }
        namespace Responses {
            export type $200 = /* UpdateStockTypeEvent */ Definitions.UpdateStockTypeEvent;
        }
    }
    namespace UpdateStockTypeInStockByPickingUsingPOST {
        export interface BodyParameters {
            updateStockTypeCmd: Parameters.UpdateStockTypeCmd;
        }
        namespace Parameters {
            export type UpdateStockTypeCmd = /* UpdateStockTypeByPickingCmd */ Definitions.UpdateStockTypeByPickingCmd;
        }
        namespace Responses {
            export type $200 = /* UpdateStockTypeEvent */ Definitions.UpdateStockTypeEvent;
        }
    }
    namespace UpdateStockTypeInStockByUIUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdateStockTypeInStockByUICmd */ Definitions.UpdateStockTypeInStockByUICmd;
        }
    }
    namespace UpdateStockTypeUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* ModifyStockTypeCmd */ Definitions.ModifyStockTypeCmd;
        }
        namespace Responses {
            export type $200 = /* StockTypeDto */ Definitions.StockTypeDto;
        }
    }
    namespace UpdateUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdateCarrierTypeCmd */ Definitions.UpdateCarrierTypeCmd;
        }
        namespace Responses {
            export type $200 = /* CarrierTypeDto */ Definitions.CarrierTypeDto;
        }
    }
    namespace UpdateUsingPUT1 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdatePlaceCmd */ Definitions.UpdatePlaceCmd;
        }
        namespace Responses {
            export type $200 = /* PlaceDto */ Definitions.PlaceDto;
        }
    }
    namespace UpdateUsingPUT2 {
        export interface BodyParameters {
            req: Parameters.Req;
        }
        namespace Parameters {
            export type Req = /* UpdatePlaceTypeCommonCharacteristicDto */ Definitions.UpdatePlaceTypeCommonCharacteristicDto;
        }
        namespace Responses {
            export type $200 = /* PlaceTypeCharacteristicDTO */ Definitions.PlaceTypeCharacteristicDTO;
        }
    }
    namespace UpdateUsingPUT3 {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdateZoneTypeCmd */ Definitions.UpdateZoneTypeCmd;
        }
        namespace Responses {
            export type $200 = /* ZoneTypeDto */ Definitions.ZoneTypeDto;
        }
    }
    namespace UpdateZoneUsingPUT {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* UpdateZoneCmd */ Definitions.UpdateZoneCmd;
        }
        namespace Responses {
            export type $200 = /* ZoneDto */ Definitions.ZoneDto;
        }
    }
    namespace UploadAllocationSettingsUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace UploadProductBatchesUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace UploadRouteStrategyUsingPOST {
        export interface FormDataParameters {
            siteCode: Parameters.SiteCode;
        }
        namespace Parameters {
            export type SiteCode = string;
        }
    }
    namespace UploadWriteOffSettingsUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace UploadZoneCharacteristicsUsingPOST {
        export interface FormDataParameters {
            siteCode: Parameters.SiteCode;
        }
        namespace Parameters {
            export type SiteCode = string;
        }
    }
    namespace WriteOffLostStockUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* WriteOffLostStockCmd */ Definitions.WriteOffLostStockCmd;
        }
        namespace Responses {
            export type $200 = /* WriteOffStockResponse */ Definitions.WriteOffStockResponse;
        }
    }
    namespace WriteOffRawStockUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* WriteOffRawStocksCmd */ Definitions.WriteOffRawStocksCmd;
        }
    }
    namespace WriteOffStockUsingPOST {
        export interface BodyParameters {
            cmd: Parameters.Cmd;
        }
        namespace Parameters {
            export type Cmd = /* WriteOffStockCmd */ Definitions.WriteOffStockCmd;
        }
        namespace Responses {
            export type $200 = /* WriteOffStockResponse */ Definitions.WriteOffStockResponse;
        }
    }
    namespace ZoneTransferRulesUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
    namespace ZonesUploadUsingPOST {
        export interface FormDataParameters {
            siteId: Parameters.SiteId /* uuid */;
        }
        namespace Parameters {
            export type SiteId = string; // uuid
        }
    }
}
