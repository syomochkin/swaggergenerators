/* tslint:disable */
export class MarkDTO {
  code: string;
  createdAt: number;
  inboundDeliveryId?: string;
  inboundDeliveryPosition?: number;
  level: 'TRANSPORT' | 'GROUP' | 'INDIVIDUAL';
  markId?: string;
  markStatus: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  orderNumber?: string;
  orderPosition?: number;
  parentCode?: string;
  productBatchId?: string;
  productBatchNumber?: string;
  productId: string;
  productName?: string;
  type: 'EGAIS' | 'CHZ';
  unit?: string;
}

/** my fixes */
