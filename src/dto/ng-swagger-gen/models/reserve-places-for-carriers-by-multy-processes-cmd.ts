/* tslint:disable */
import { ProcessTransferInfo } from './process-transfer-info';
import { ZoneReference } from './zone-reference';
export class ReservePlacesForCarriersByMultyProcessesCmd {
  carrierPlaceTypeCode?: string;
  carriers?: Array<ProcessTransferInfo>;
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  processPlaceTypeCode?: string;
  siteCode?: string;
  zones?: Array<ZoneReference>;
}

/** my fixes */
