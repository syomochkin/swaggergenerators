/* tslint:disable */
export class UIStockSearchQuery {

  /**
   * Номер носителя
   */
  carrierBarcodes?: Array<string>;

  /**
   * Тип носителя
   */
  carrierTypeCodes?: Array<string>;

  /**
   * Признак "Принято к учету"
   */
  confirmed?: boolean;

  /**
   * Типы зон, исключенные из поиска
   */
  excludedZoneTypeIds?: Array<string>;

  /**
   * Годен до (от)
   */
  expirationDateFrom?: string;

  /**
   * Годен до (до)
   */
  expirationDateTo?: string;

  /**
   * Принят (от)
   */
  inboundTimeFrom?: string;

  /**
   * Принят (до)
   */
  inboundTimeTo?: string;

  /**
   * Произведен (от)
   */
  manufactureTimeFrom?: string;

  /**
   * Произведен (до)
   */
  manufactureTimeTo?: string;

  /**
   * Тип маркировки
   */
  markAccountingTypes?: Array<'EGAIS' | 'CHZ'>;
  maxResponseSize?: number;

  /**
   * Адреса мест
   */
  placeAddresses?: Array<string>;

  /**
   * Статус места
   */
  placeStatusCodes?: Array<string>;

  /**
   * Тип места
   */
  placeTypeCodes?: Array<string>;

  /**
   * Номер партии
   */
  productBatchNumbers?: Array<string>;

  /**
   * Тип партии
   */
  productBatchTypes?: Array<'REGULAR' | 'RETURN'>;

  /**
   * Артикулы товара
   */
  productIds?: Array<string>;

  /**
   * Срок реализации (от)
   */
  sellByTimeFrom?: string;

  /**
   * Срок реализации (до)
   */
  sellByTimeTo?: string;
  siteCode: string;

  /**
   * Вид запаса
   */
  stockTypeCodes?: Array<string>;

  /**
   * Признак "Виртуальное место"
   */
  virtualPlace?: boolean;

  /**
   * Без маркировки
   */
  withoutMarkAccountingType?: boolean;

  /**
   * Зоны
   */
  zoneIds?: Array<string>;
}

/** my fixes */
