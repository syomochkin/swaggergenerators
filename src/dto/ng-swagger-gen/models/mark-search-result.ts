/* tslint:disable */
import { MarkDTO } from './mark-dto';
export class MarkSearchResult {
  marks?: Array<MarkDTO>;
  totalCount?: number;
}

/** my fixes */
