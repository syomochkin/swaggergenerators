/* tslint:disable */
export class TryTakeRequest {
  operationId?: string;
  placeId?: string;
  quantity?: number;
  stockId?: string;
  transferId?: string;
  unit?: string;
}

/** my fixes */
