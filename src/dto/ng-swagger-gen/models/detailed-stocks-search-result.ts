/* tslint:disable */
import { StockDto } from './stock-dto';
export class DetailedStocksSearchResult {
  stocks?: Array<StockDto>;
  totalCount?: number;
}

/** my fixes */
