/* tslint:disable */
export class ProductAmountDto {
  productId?: string;
  quantity?: number;
  rejectionReason?: 'NO_STOCK' | 'NO_ROUTE';
  unit?: string;
}

/** my fixes */
