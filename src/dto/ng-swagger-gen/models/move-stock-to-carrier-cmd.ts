/* tslint:disable */
export class MoveStockToCarrierCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  quantity?: number;
  sourcePlaceId?: string;
  sourceStockId?: string;
  tabNumber?: string;
  targetCarrierId?: string;
  transferId?: string;
  unit?: string;
  userName?: string;
}

/** my fixes */
