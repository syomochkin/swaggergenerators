/* tslint:disable */
import { AuditCarrierRecordDto } from './audit-carrier-record-dto';
export class AuditCarrierRecordSearchResponse {
  records?: Array<AuditCarrierRecordDto>;
  recordsCount?: number;
}

/** my fixes */
