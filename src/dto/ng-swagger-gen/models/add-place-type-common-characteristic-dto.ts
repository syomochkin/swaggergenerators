/* tslint:disable */
import { DefaultValue } from './default-value';
import { CommonCharacteristicSettings } from './common-characteristic-settings';
export class AddPlaceTypeCommonCharacteristicDto {
  characteristic?: string;
  defaultValue?: DefaultValue;
  settings?: CommonCharacteristicSettings;
}

/** my fixes */
