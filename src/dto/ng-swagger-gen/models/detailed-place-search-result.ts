/* tslint:disable */
import { PlaceDto } from './place-dto';
export class DetailedPlaceSearchResult {
  places?: Array<PlaceDto>;
  totalCount?: number;
}

/** my fixes */
