/* tslint:disable */
import { PlaceHierarchyFilterCondition } from './place-hierarchy-filter-condition';
export class PlaceHierarchyFilter {
  hierarchyElements?: Array<PlaceHierarchyFilterCondition>;
  siteId?: string;
}

/** my fixes */
