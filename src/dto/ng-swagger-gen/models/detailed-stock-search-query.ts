/* tslint:disable */
export class DetailedStockSearchQuery {
  carrierId?: string;
  carrierNumber?: string;
  inCarrier?: 'IN_ONLY' | 'OUT_ONLY' | 'ALL';
  inVirtualPlace?: 'VIRTUAL' | 'NOT_VIRTUAL' | 'ALL';
  maxResponseSize?: number;
  placeAddress?: string;
  placeId?: string;
  productBatchId?: string;
  productBatchNumber?: string;
  productId?: string;
  productIds?: Array<string>;
  siteCode?: string;
  stockId?: string;
  stockTypeCode?: string;
}

/** my fixes */
