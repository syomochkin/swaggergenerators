/* tslint:disable */
import { RejectedInfo } from './rejected-info';
import { ReservedCarrier } from './reserved-carrier';
export class ProductPlacementResponse {
  operationId?: string;
  productBatchId?: string;
  productId?: string;
  rejectedInfo?: RejectedInfo;
  reservedCarriers?: Array<ReservedCarrier>;
}

/** my fixes */
