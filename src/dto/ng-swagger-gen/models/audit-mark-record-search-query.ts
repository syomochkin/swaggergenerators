/* tslint:disable */
export class AuditMarkRecordSearchQuery {
  createdAtFrom: string;
  createdAtTo: string;
  inboundDeliveryPosition?: number;
  level?: 'TRANSPORT' | 'GROUP' | 'INDIVIDUAL';
  login?: string;
  markCode?: string;
  operation?: string;
  process?: string;
  productId?: string;
  siteCode: string;
  sourceProductBatchNumber?: string;
  sourceStatus?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  tabNumber?: string;
  targetProductBatchNumber?: string;
  targetStatus?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  type?: 'EGAIS' | 'CHZ';
  unit?: string;
  userName?: string;
}

/** my fixes */
