/* tslint:disable */
export class ReservationCancelledResponse {
  incomingQuantity?: number;
  operationId?: string;
  outgoingQuantity?: number;
  siteCode?: string;
  stockId?: string;
  transferId?: string;
}

/** my fixes */
