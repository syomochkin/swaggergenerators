/* tslint:disable */
import { StockDto } from './stock-dto';
export class MoveAllStocksToCarrierResponse {
  operationId?: string;
  stockDto?: Array<StockDto>;
  targetCarrierId?: string;
  transferId?: string;
}

/** my fixes */
