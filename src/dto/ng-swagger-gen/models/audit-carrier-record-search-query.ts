/* tslint:disable */
export class AuditCarrierRecordSearchQuery {
  carrierId?: string;
  carrierNumber?: string;
  createdAtFrom: string;
  createdAtTo: string;
  login?: string;
  maxResponseSize?: number;
  operation?: string;
  operationId?: string;
  processType?: string;
  sourceCarrierReservationKey?: string;
  sourceParentId?: string;
  sourceParentNumber?: string;
  sourceParentTypeCode?: string;
  sourcePlaceAddress?: string;
  sourceSiteCode?: string;
  sourceTypeCode?: string;
  tabNumber?: string;
  targetCarrierReservationKey?: string;
  targetParentId?: string;
  targetParentNumber?: string;
  targetParentTypeCode?: string;
  targetPlaceAddress?: string;
  targetSiteCode?: string;
  targetTypeCode?: string;
  userName?: string;
}

/** my fixes */
