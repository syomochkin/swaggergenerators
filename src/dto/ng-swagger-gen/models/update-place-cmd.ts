/* tslint:disable */
import { Coordinates3D } from './coordinates-3d';
import { PlaceStatusDto } from './place-status-dto';
export class UpdatePlaceCmd {
  address: string;
  addressTypeId?: string;
  coordinates?: Coordinates3D;
  description?: string;
  number: number;
  status?: PlaceStatusDto;
  statusReason?: string;
  typeId: string;
  zoneIds?: Array<string>;
}

/** my fixes */
