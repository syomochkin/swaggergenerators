/* tslint:disable */
export class AuditStockRecordDto {
  additionalInfo?: {[key: string]: string};
  baseAmount: number;
  baseUnit: string;
  calculatedBaseAmount: boolean;
  createdTime: number;
  id?: number;
  operation: string;
  process: string;
  quantity: number;
  siteCode: string;
  sourceCarrierNumber?: string;
  sourcePlaceAddress?: string;
  sourceProductBatchId?: string;
  sourceProductBatchNumber?: string;
  sourceProductId?: string;
  sourceProductName?: string;
  sourceStockTypeCode?: string;
  tabNumber?: string;
  targetCarrierNumber?: string;
  targetPlaceAddress?: string;
  targetProductBatchId?: string;
  targetProductBatchNumber?: string;
  targetProductId?: string;
  targetProductName?: string;
  targetStockTypeCode?: string;
  unit: string;
  userName?: string;
}

/** my fixes */
