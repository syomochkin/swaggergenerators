/* tslint:disable */
export class ProductBatchDto {
  createMarksOnPicking?: boolean;
  expirationTime?: number;
  id?: string;
  inboundId?: string;
  inboundTime?: number;
  manufactureTime?: number;
  marksAccountingType?: 'EGAIS' | 'CHZ';
  mercuryExtId?: string;
  number?: string;
  productId?: string;
  productName?: string;
  referenceBNumber?: string;
  siteId?: string;
  type?: 'REGULAR' | 'RETURN';
  vendorCode?: string;
  vendorName?: string;
  weightProduct?: boolean;
}

/** my fixes */
