/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
import { ResponseMarkDTO } from './response-mark-dto';
export class ChangeMarksStatusResponse {
  error?: AbstractWarehouseError;
  marks?: Array<ResponseMarkDTO>;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

/** my fixes */
