/* tslint:disable */
export class RouteStrategyDTO {

  /**
   * Идентификатор стратегии
   */
  id: string;

  /**
   * Название стратегии (русскоязычное понятное навание стратегии)
   */
  name: string;

  /**
   * Код площадки
   */
  siteCode: string;

  /**
   * коды секторов, к которым будет привязана данная стратегия
   */
  zoneCodes: Array<string>;
}

/** my fixes */
