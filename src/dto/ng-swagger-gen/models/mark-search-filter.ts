/* tslint:disable */
import { MarkSearchDTO } from './mark-search-dto';
export class MarkSearchFilter {
  createdAtFrom?: string;
  createdAtTo?: string;
  inboundDeliveryId?: string;
  inboundDeliveryPosition?: number;
  level?: 'TRANSPORT' | 'GROUP' | 'INDIVIDUAL';
  marks?: Array<MarkSearchDTO>;
  parentCode?: string;
  productBatchId?: string;
  productBatchNumber?: string;
  productId?: string;
  siteCode: string;
  statuses?: Array<'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED'>;
  type?: 'EGAIS' | 'CHZ';
  unit?: string;
}

/** my fixes */
