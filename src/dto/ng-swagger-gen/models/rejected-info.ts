/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class RejectedInfo {
  clientError?: AbstractWarehouseError;
  quantity?: number;
}

/** my fixes */
