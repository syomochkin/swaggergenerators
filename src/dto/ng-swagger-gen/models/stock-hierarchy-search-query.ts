/* tslint:disable */
import { StockHierarchySearchFilter } from './stock-hierarchy-search-filter';
import { OrderByCriteria } from './order-by-criteria';
import { PageRequestInfo } from './page-request-info';
export class StockHierarchySearchQuery {
  filter?: StockHierarchySearchFilter;
  orderInfo?: Array<OrderByCriteria>;
  pageInfo?: PageRequestInfo;
}

/** my fixes */
