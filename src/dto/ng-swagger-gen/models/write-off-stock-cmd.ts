/* tslint:disable */
export class WriteOffStockCmd {
  baseAmount?: number;
  baseUnit?: string;
  documentDate?: string;
  documentNumber?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  quantity?: number;
  stockId?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

/** my fixes */
