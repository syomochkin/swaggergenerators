/* tslint:disable */
import { SlottingRouteStep } from './slotting-route-step';
export class SlottingRoute {
  plannedDuration?: number;
  steps?: Array<SlottingRouteStep>;
  transferId?: string;
}

/** my fixes */
