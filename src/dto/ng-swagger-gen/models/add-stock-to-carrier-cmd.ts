/* tslint:disable */
export class AddStockToCarrierCmd {
  baseAmount?: number;
  baseUnit?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  stockType?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

/** my fixes */
