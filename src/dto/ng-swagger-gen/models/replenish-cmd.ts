/* tslint:disable */
export class ReplenishCmd {
  destPlaceBarcode?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  siteCode?: string;
  srcCarrierId?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  targetStockTypeCode?: string;
  userName?: string;
}

/** my fixes */
