/* tslint:disable */
import { OutboundOrderItemDto } from './outbound-order-item-dto';
export class OrderBoxDto {
  carrierId?: string;
  orderItems?: Array<OutboundOrderItemDto>;
}

/** my fixes */
