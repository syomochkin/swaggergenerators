/* tslint:disable */
export class ExchangeChuteReservationCmd {
  carrierTypeCode?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  sourceTransferId?: string;
  targetTransferId?: string;
  zoneCodes?: Array<string>;
}

/** my fixes */
