/* tslint:disable */
import { AuditMarkRecordDto } from './audit-mark-record-dto';
export class AuditMarkRecordSearchResponse {
  moreRecordsLeft?: boolean;
  records?: Array<AuditMarkRecordDto>;
}

/** my fixes */
