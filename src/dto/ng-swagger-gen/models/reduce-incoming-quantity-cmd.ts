/* tslint:disable */
export class ReduceIncomingQuantityCmd {
  operationId?: string;
  productBatchId?: string;
  quantity?: number;
  stockTypeCode?: string;
  transferId?: string;
}

/** my fixes */
