/* tslint:disable */
import { ValueCmd } from './value-cmd';
export class OverridePlaceCommonCharacteristicCmd {
  characteristic?: string;
  siteId?: string;
  value?: ValueCmd;
}

/** my fixes */
