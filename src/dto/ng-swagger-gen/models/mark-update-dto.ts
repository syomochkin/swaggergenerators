/* tslint:disable */
export class MarkUpdateDTO {
  code?: string;
  orderNumber?: string;
  orderPosition?: number;
  productBatchId?: string;
  type?: 'EGAIS' | 'CHZ';
}

/** my fixes */
