/* tslint:disable */
import { ProductBatchBalanceByStockType } from './product-batch-balance-by-stock-type';
export class ProductBatchBalanceDifferenceDto {
  canCorrect?: boolean;
  needCorrect?: boolean;
  stockTypes?: Array<ProductBatchBalanceByStockType>;
}

/** my fixes */
