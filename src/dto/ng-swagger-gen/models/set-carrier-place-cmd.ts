/* tslint:disable */
export class SetCarrierPlaceCmd {
  actualPlaceAddress?: string;
  carrierId?: string;
  operationId?: string;
  planPlaceAddress?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  tabNumber?: string;
  transferId?: string;
  userName?: string;
}

/** my fixes */
