/* tslint:disable */
import { ProcessTransferInfo } from './process-transfer-info';
import { ZoneReference } from './zone-reference';
export class ReservePlacesForCarriersByClusterDeliveryCmd {
  carrierPlaceTypeCode?: string;
  carriers?: Array<ProcessTransferInfo>;
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  processPlaceTypeCode?: string;
  reservationMode?: 'FULL' | 'PARTIAL' | 'SUSTAINED';
  siteCode?: string;
  zones?: Array<ZoneReference>;
}

/** my fixes */
