/* tslint:disable */
export class ReducePlaceReservationForTransferCmd {
  carrierTypeCode?: string;
  operationId?: string;
  placeId?: string;
  quantity?: number;
  siteCode?: string;
  transferId?: string;
}

/** my fixes */
