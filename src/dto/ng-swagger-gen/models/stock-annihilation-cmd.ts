/* tslint:disable */
export class StockAnnihilationCmd {
  baseAmount?: number;
  baseUnit?: string;
  carrierId?: string;
  carrierReservationKey?: string;
  operationId?: string;
  placeId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  productBatchId?: string;
  quantity?: number;
  siteCode?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

/** my fixes */
