/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class PlaceReservationForCarriersNotFoundError extends AbstractWarehouseError{
  code?: string;
  message?: string;
}

/** my fixes */
