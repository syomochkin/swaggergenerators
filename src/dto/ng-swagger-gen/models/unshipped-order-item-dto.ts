/* tslint:disable */
export class UnshippedOrderItemDto {
  baseAmount?: number;
  baseUnit?: string;
  position?: number;
  productId?: string;
  quantity?: number;
  unit?: string;
}

/** my fixes */
