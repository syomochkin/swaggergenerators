/* tslint:disable */
export class CreateCarrierTypeCmd {
  code?: string;
  description?: string;
  height?: number;
  id?: string;
  length?: number;
  maxVolume?: number;
  maxWeight?: number;
  name?: string;
  width?: number;
}

/** my fixes */
