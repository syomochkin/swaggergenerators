/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class AllocationError extends AbstractWarehouseError{
  code?: string;
  message?: string;
}

/** my fixes */
