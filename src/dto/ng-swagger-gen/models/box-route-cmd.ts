/* tslint:disable */
import { ProductAmountItem } from './product-amount-item';
export class BoxRouteCmd {
  items?: Array<ProductAmountItem>;
  joinedItemByProductId?: {[key: string]: ProductAmountItem};
  operationId?: string;
  processBusinessKey?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  useRoute?: 'NO_ROUTE';
  useStartPlace?: 'NO_START_PLACE';
}

/** my fixes */
