/* tslint:disable */
import { UIStockDto } from './uistock-dto';
export class UIStockSearchResult {
  stocks: Array<UIStockDto>;
  totalCount: number;
}

/** my fixes */
