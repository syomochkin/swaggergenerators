/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class CreateScannedChzMarkResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

/** my fixes */
