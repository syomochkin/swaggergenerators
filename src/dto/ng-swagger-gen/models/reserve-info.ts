/* tslint:disable */
import { TransferDto } from './transfer-dto';
export class ReserveInfo {
  placeAddress?: string;
  placeId?: string;
  placeTypeCode?: string;
  transfer?: TransferDto;
}

/** my fixes */
