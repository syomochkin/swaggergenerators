/* tslint:disable */
import { WarehouseProductGroupDto } from './warehouse-product-group-dto';
export class UpdateZoneCmd {
  boundedWarehouseProductGroups?: Array<WarehouseProductGroupDto>;
  code?: string;
  description?: string;
  name?: string;
}

/** my fixes */
