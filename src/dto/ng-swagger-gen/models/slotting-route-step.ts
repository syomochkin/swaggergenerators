/* tslint:disable */
import { CharacteristicDto } from './characteristic-dto';
import { PlaceWithType } from './place-with-type';
import { ReservationInfoDto } from './reservation-info-dto';
import { ZoneWithType } from './zone-with-type';
export class SlottingRouteStep {
  characteristics?: Array<CharacteristicDto>;
  hierarchy?: Array<PlaceWithType>;
  place?: PlaceWithType;
  plannedDuration?: number;
  reservation?: ReservationInfoDto;
  transferId?: string;
  zones?: Array<ZoneWithType>;
}

/** my fixes */
