/* tslint:disable */
export class SiteSettingDto {
  bigDecimalValue?: number;
  code?: 'MAX_MIX_PRODUCTS';
  intValue?: number;
  stringValue?: string;
  type?: 'INTEGER' | 'STRING' | 'BIG_DECIMAL';
}

/** my fixes */
