/* tslint:disable */
export class MoveCarrierToPlaceResponse {
  carrierId?: string;
  processBusinessKey?: string;
  rejected?: boolean;
  rejectedReason?: string;
  transferComplete?: boolean;
}

/** my fixes */
