/* tslint:disable */
export class ReservationDto {
  batchId?: string;
  placeAddress?: string;
  placeId?: string;
  productId?: string;
  quantityIncoming?: number;
  quantityOutgoing?: number;
  stockId?: string;
  transferId?: string;
}

/** my fixes */
