/* tslint:disable */
import { CarrierTypeDto } from './carrier-type-dto';
export class CarrierDto {
  author?: string;
  barcode?: string;
  carriersTypeId?: string;
  createdTime?: number;
  editor?: string;
  id?: string;
  nestedCarriers?: Array<CarrierDto>;
  parentId?: string;
  parentNumber?: string;
  placeAddress?: string;
  placeId?: string;
  processId?: string;
  type?: CarrierTypeDto;
  updatedTime?: number;
}

/** my fixes */
