/* tslint:disable */
import { CoordinatesDto } from './coordinates-dto';
import { PlaceStatusDto } from './place-status-dto';
import { ZoneInfo } from './zone-info';
export class PlaceDto {
  address?: string;
  addressTypeId?: string;
  author?: string;
  childPlaces?: Array<PlaceDto>;
  coordinates?: CoordinatesDto;
  createdTime?: number;
  description?: string;
  editor?: string;
  id?: string;
  number?: number;
  parentId?: string;
  parentPlaces?: Array<PlaceDto>;
  siteId?: string;
  status?: PlaceStatusDto;
  statusReason?: string;
  storagePlace?: boolean;
  typeCode?: string;
  typeId?: string;
  typeName?: string;
  updatedTime?: number;
  virtualPlace?: boolean;
  zones?: Array<ZoneInfo>;
}

/** my fixes */
