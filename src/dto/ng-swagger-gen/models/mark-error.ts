/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class MarkError extends AbstractWarehouseError{
  alreadyExistingCodes?: Array<string>;
  code?: string;
  deleteMarkHasChildren?: Array<string>;
  message?: string;
  notFoundCodes?: Array<string>;
  wrongParent?: Array<string>;
  wrongProduct?: Array<string>;
  wrongProductBatches?: Array<string>;
  wrongStatusCodes?: Array<string>;
  wrongUnit?: Array<string>;
}

/** my fixes */
