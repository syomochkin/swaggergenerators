/* tslint:disable */
import { DetailedPlaceSearchFilter } from './detailed-place-search-filter';
export class DetailedPlaceSearchQuery {
  filter: DetailedPlaceSearchFilter;
  maxResponseSize?: number;
  pageNumber?: number;
}

/** my fixes */
