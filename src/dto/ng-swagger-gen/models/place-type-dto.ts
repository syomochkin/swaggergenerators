/* tslint:disable */
export class PlaceTypeDto {
  author?: string;
  code?: string;
  coordinatesRequired?: boolean;
  createdTime?: number;
  description?: string;
  editor?: string;
  maxMixBatches?: number;
  maxMixProducts?: number;
  name?: string;
  numberOfImplementation?: number;
  numerationRule?: string;
  placeTypeId?: string;
  siteId?: string;
  storagePlace?: boolean;
  updatedTime?: number;
  virtualPlace?: boolean;
}

/** my fixes */
