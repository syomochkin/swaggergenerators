/* tslint:disable */
import { OrderBoxDto } from './order-box-dto';
import { UnshippedOrderItemDto } from './unshipped-order-item-dto';
export class OutboundOrderCmd {
  operationId?: string;
  orderBoxes?: Array<OrderBoxDto>;
  orderVersion?: string;
  outboundDeliveryNumber?: string;
  outboundTime?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  tabNumber?: string;
  unshippedOrderItems?: Array<UnshippedOrderItemDto>;
  userName?: string;
}

/** my fixes */
