/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class ReserveCarrierForProcessResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
}

/** my fixes */
