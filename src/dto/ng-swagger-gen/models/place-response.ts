/* tslint:disable */
import { CarrierTransferReservationInfo } from './carrier-transfer-reservation-info';
import { CarrierInfo } from './carrier-info';
export class PlaceResponse {
  carrierTransferReservations?: Array<CarrierTransferReservationInfo>;
  carriers?: Array<CarrierInfo>;
  placeAddress?: string;
  placeId?: string;
  processId?: string;
}

/** my fixes */
