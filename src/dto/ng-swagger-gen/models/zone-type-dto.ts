/* tslint:disable */
export class ZoneTypeDto {
  author?: string;
  code?: string;
  createdTime?: number;
  description?: string;
  editor?: string;
  id?: string;
  name?: string;
  numberOfImplementation?: number;
  updatedTime?: number;
  warehouseProductGroupCanBeBounded?: boolean;
}

/** my fixes */
