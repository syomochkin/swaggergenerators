/* tslint:disable */
export class PickingZoneTransferReq {
  carrierId?: string;
  destinationPlaceId?: string;
  operationId?: string;
  processBusinessKey?: string;
  startPlaceId?: string;
  transferId?: string;
}

/** my fixes */
