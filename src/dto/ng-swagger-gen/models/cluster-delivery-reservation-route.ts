/* tslint:disable */
import { RouteStep } from './route-step';
export class ClusterDeliveryReservationRoute {
  author?: string;
  createdTime: number;
  editor?: string;
  steps: Array<RouteStep>;
  updatedTime: number;
}

/** my fixes */
