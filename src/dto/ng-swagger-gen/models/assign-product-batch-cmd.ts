/* tslint:disable */
export class AssignProductBatchCmd {
  createMarksOnPicking?: boolean;
  expirationTime?: string;
  inboundId?: string;
  inboundTime?: string;
  manufactureTime?: string;
  marksAccountingType?: 'EGAIS' | 'CHZ';
  mercuryExtId?: string;
  number?: string;
  productId?: string;
  productName?: string;
  referenceBNumber?: string;
  siteCode?: string;
  type?: 'REGULAR' | 'RETURN';
  unit?: string;
  vendorCode?: string;
  vendorName?: string;
  weightProduct?: boolean;
}

/** my fixes */
