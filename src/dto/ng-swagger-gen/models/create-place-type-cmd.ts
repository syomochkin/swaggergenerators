/* tslint:disable */
export class CreatePlaceTypeCmd {
  code?: string;
  coordinatesRequired?: boolean;
  description?: string;
  maxMixBatches?: number;
  maxMixProducts?: number;
  name?: string;
  numerationRule?: 'LEFT_TO_RIGHT_TOP_TO_BOTTOM' | 'LEFT_TO_RIGHT_BOTTOM_UP' | 'RIGHT_TO_LEFT_TOP_TO_BOTTOM' | 'RIGHT_TO_LEFT_BOTTOM_UP';
  placeTypeId?: string;
  storagePlace?: boolean;
  virtualPlace?: boolean;
}

/** my fixes */
