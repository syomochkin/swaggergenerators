/* tslint:disable */
export class AuditStockRecordSearchQuery {
  createdAtFrom: string;
  createdAtTo: string;
  operation?: string;
  process?: string;
  siteCode: string;
  sourceCarrierNumber?: string;
  sourcePlaceAddress?: string;
  sourceProductBatchNumber?: string;
  sourceProductId?: string;
  sourceStockTypeCode?: string;
  tabNumber?: string;
  targetCarrierNumber?: string;
  targetPlaceAddress?: string;
  targetProductBatchNumber?: string;
  targetProductId?: string;
  targetStockTypeCode?: string;
}

/** my fixes */
