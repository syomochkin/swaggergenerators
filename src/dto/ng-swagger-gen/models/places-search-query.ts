/* tslint:disable */
import { PlaceSearchFilter } from './place-search-filter';
import { OrderByCriteria } from './order-by-criteria';
import { PageRequestInfo } from './page-request-info';
export class PlacesSearchQuery {
  filter?: PlaceSearchFilter;
  orderInfo?: Array<OrderByCriteria>;
  pageInfo?: PageRequestInfo;
}

/** my fixes */
