/* tslint:disable */
export class UpdateStockTypeByInventoryCmd {
  baseAmount?: number;
  docDate?: string;
  documentNumber?: string;
  operationId?: string;
  operationType?: 'MOVE';
  orderItem?: number;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  saleAmount?: number;
  salesOrder?: string;
  specStock?: string;
  stockId?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  transferId?: string;
  updateReason?: 'UNKNOWN' | 'PRODUCT_BATCH' | 'CORRUPTED_BARCODE' | 'DEFECT' | 'NO_PRODUCT' | 'EXPIRED';
  userName?: string;
}

/** my fixes */
