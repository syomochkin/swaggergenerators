/* tslint:disable */
import { MarkSearchFilter } from './mark-search-filter';
import { OrderByCriteria } from './order-by-criteria';
export class MarkSearchQuery {
  filter?: MarkSearchFilter;
  maxResponseSize?: number;
  orderInfo?: Array<OrderByCriteria>;
  pageNumber?: number;
}

/** my fixes */
