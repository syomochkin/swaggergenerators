/* tslint:disable */
export class RejectedProductItemAmount {
  productId?: string;
  quantity?: number;
  rejectionReason?: 'NO_STOCK';
  unit?: string;
}

/** my fixes */
