/* tslint:disable */
export class AuditMarkRecordDto {
  createdTime: number;
  id?: number;
  inboundDeliveryId?: string;
  inboundDeliveryPosition?: number;
  level: 'TRANSPORT' | 'GROUP' | 'INDIVIDUAL';
  login?: string;
  markCode: string;
  operation: string;
  parentCode?: string;
  process: string;
  productId: string;
  productName?: string;
  siteCode: string;
  sourceOrderNumber?: string;
  sourceOrderPosition?: number;
  sourceProductBatchNumber?: string;
  sourceStatus?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  tabNumber?: string;
  targetOrderNumber?: string;
  targetOrderPosition?: number;
  targetProductBatchNumber?: string;
  targetStatus?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  type: 'EGAIS' | 'CHZ';
  unit?: string;
  userName?: string;
}

/** my fixes */
