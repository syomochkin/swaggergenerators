/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class WriteOffRawResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  outboundDeliveryNumber?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

/** my fixes */
