/* tslint:disable */
import { DefaultValue } from './default-value';
export class PlaceTypeCharacteristicDTO {
  author?: string;
  characteristicId?: string;
  checkParents?: string;
  createdTime?: number;
  defaultValue?: DefaultValue;
  editable?: boolean;
  editor?: string;
  mandatory?: boolean;
  placeTypeId?: string;
  type?: string;
  updatedTime?: number;
}

/** my fixes */
