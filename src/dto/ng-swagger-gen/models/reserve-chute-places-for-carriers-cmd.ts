/* tslint:disable */
import { TransferDto } from './transfer-dto';
import { ZoneReference } from './zone-reference';
export class ReserveChutePlacesForCarriersCmd {
  carrierPlaceTypeCodes?: Array<string>;
  carriers?: Array<TransferDto>;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  processPlaceTypeCodes?: Array<string>;
  siteCode?: string;
  zones?: Array<ZoneReference>;
}

/** my fixes */
