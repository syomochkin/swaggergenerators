/* tslint:disable */
import { AuditStockRecordDto } from './audit-stock-record-dto';
export class AuditStockRecordSearchResponse {
  moreRecordsLeft?: boolean;
  records?: Array<AuditStockRecordDto>;
}

/** my fixes */
