/* tslint:disable */
import { FullAmount } from './full-amount';
export class StockInfo {
  actual?: FullAmount;
  available?: FullAmount;
  batchConfirmed?: boolean;
  batchId?: string;
  batchNumber?: string;
  carrier?: boolean;
  expirationTime?: number;
  inbound?: FullAmount;
  inboundTime?: number;
  manufactureTime?: number;
  outbound?: FullAmount;
  placeAddress?: string;
  placeId?: string;
  productId?: string;
  productName?: string;
  sellByTime?: number;
  stockTypeCode?: string;
  stockTypeId?: string;
}

/** my fixes */
