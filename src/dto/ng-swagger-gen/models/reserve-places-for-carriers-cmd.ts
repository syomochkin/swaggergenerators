/* tslint:disable */
import { TransferDto } from './transfer-dto';
import { ZoneReference } from './zone-reference';
export class ReservePlacesForCarriersCmd {
  carrierPlaceTypeCode?: string;
  carriers?: Array<TransferDto>;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  processPlaceTypeCode?: string;
  reservationMode?: 'FULL' | 'PARTIAL' | 'SUSTAINED';
  siteCode?: string;
  zones?: Array<ZoneReference>;
}

/** my fixes */
