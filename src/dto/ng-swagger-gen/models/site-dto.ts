/* tslint:disable */
export class SiteDto {
  archived?: boolean;
  author?: string;
  code?: string;
  createdTime?: number;
  description?: string;
  editor?: string;
  id?: string;
  name?: string;
  updatedTime?: number;
}

/** my fixes */
