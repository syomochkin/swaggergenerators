/* tslint:disable */
export class PickerRouteReq {
  pickingZoneId?: string;
  siteCode?: string;
  startPlaceId?: string;
  transferIds?: Array<string>;
}

/** my fixes */
