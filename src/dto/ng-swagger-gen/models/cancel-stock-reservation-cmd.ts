/* tslint:disable */
export class CancelStockReservationCmd {
  incomingQuantity?: number;
  operationId?: string;
  outgoingQuantity?: number;
  siteCode?: string;
  stockId?: string;
  transferId?: string;
  unit?: string;
}

/** my fixes */
