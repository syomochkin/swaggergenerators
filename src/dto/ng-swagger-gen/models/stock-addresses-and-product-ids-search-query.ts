/* tslint:disable */
import { StockAddressesAndProductIdsSearchFilter } from './stock-addresses-and-product-ids-search-filter';
import { OrderByCriteria } from './order-by-criteria';
import { PageRequestInfo } from './page-request-info';
export class StockAddressesAndProductIdsSearchQuery {
  filter?: StockAddressesAndProductIdsSearchFilter;
  orderInfo?: Array<OrderByCriteria>;
  pageInfo?: PageRequestInfo;
}

/** my fixes */
