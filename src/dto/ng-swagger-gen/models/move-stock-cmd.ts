/* tslint:disable */
export class MoveStockCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  quantity?: number;
  stockId?: string;
  tabNumber?: string;
  targetCarrierId?: string;
  targetPlaceId?: string;
  userName?: string;
}

/** my fixes */
