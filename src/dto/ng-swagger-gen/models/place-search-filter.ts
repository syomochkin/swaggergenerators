/* tslint:disable */
import { PlaceHierarchyFilter } from './place-hierarchy-filter';
import { PlaceZonesFilter } from './place-zones-filter';
export class PlaceSearchFilter {
  hierarchy?: PlaceHierarchyFilter;
  placeCategory?: 'STORAGE_PLACE' | 'NOT_STORAGE_PLACE' | 'ALL';
  placeTypes?: Array<string>;
  zonesType?: Array<PlaceZonesFilter>;
}

/** my fixes */
