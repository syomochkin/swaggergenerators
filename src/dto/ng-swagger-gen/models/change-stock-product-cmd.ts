/* tslint:disable */
export class ChangeStockProductCmd {
  operationId: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  productId: string;
  quantity: number;
  stockId: string;
  tabNumber?: string;
  userName?: string;
}

/** my fixes */
