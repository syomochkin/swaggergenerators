/* tslint:disable */
export class StockTypeDto {
  author?: string;
  code?: string;
  createdTime?: number;
  editor?: string;
  id?: string;
  name?: string;
  updatedTime?: number;
  writeOffAllowed?: boolean;
}

/** my fixes */
