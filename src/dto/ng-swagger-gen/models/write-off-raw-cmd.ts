/* tslint:disable */
import { RawStock } from './raw-stock';
export class WriteOffRawCmd {
  operationId?: string;
  orderVersion?: string;
  outboundDeliveryNumber?: string;
  outboundTime?: string;
  placeAddress?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  stocks?: Array<RawStock>;
  tabNumber?: string;
  userName?: string;
}

/** my fixes */
