/* tslint:disable */
import { CharacteristicDto } from './characteristic-dto';
import { Type } from './type';
import { Zone } from './zone';
export class PlaceHierarchyTreeDto {
  address?: string;
  characteristics?: Array<CharacteristicDto>;
  children?: Array<PlaceHierarchyTreeDto>;
  id?: string;
  number?: number;
  status?: string;
  type?: Type;
  zones?: Array<Zone>;
}

/** my fixes */
