/* tslint:disable */
import { ProductBatchSearchDto } from './product-batch-search-dto';
export class ProductBatchesSearchResult {
  productBatches?: Array<ProductBatchSearchDto>;
  totalCount?: number;
}

/** my fixes */
