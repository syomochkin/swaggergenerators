/* tslint:disable */
export class CarrierTypeDto {
  archived?: boolean;
  author?: string;
  code?: string;
  createdTime?: number;
  description?: string;
  editor?: string;
  height?: number;
  id?: string;
  length?: number;
  maxVolume?: number;
  maxWeight?: number;
  name?: string;
  updatedTime?: number;
  width?: number;
}

/** my fixes */
