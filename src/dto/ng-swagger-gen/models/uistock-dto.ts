/* tslint:disable */
export class UIStockDto {

  /**
   * Фактический остаток в ПЕИ
   */
  actualQuantity: number;

  /**
   * Фактический остаток в мл
   */
  actualVolume: number;

  /**
   * Фактический остаток в граммах
   */
  actualWeight: number;

  /**
   * Доступный остаток в ПЕИ
   */
  availableQuantity: number;

  /**
   * Доступный остаток в мл
   */
  availableVolume: number;

  /**
   * Доступный остаток в граммах
   */
  availableWeight: number;

  /**
   * Носитель
   */
  carrierBarcode?: string;

  /**
   * Тип носителя
   */
  carrierTypeCode?: string;

  /**
   * Годен до
   */
  expirationTime: number;

  /**
   * Дата и время приемки партии запаса
   */
  inboundTime: number;

  /**
   * Входящий остаток в ПЕИ
   */
  incomingQuantity: number;

  /**
   * Входящий остаток в мл
   */
  incomingVolume: number;

  /**
   * Входящий остаток в граммах
   */
  incomingWeight: number;

  /**
   * Дата и время производства партии запаса
   */
  manufactureTime: number;

  /**
   * Тип помарочного учета
   */
  marksAccountingType?: 'EGAIS' | 'CHZ';

  /**
   * Исходящий остаток в ПЕИ
   */
  outgoingQuantity: number;

  /**
   * Исходящий остаток в мл
   */
  outgoingVolume: number;

  /**
   * Исходящий остаток в граммах
   */
  outgoingWeight: number;

  /**
   * Родительский носитель
   */
  parentCarrierBarcode?: string;

  /**
   * Тип родительского носителя
   */
  parentCarrierTypeCode?: string;

  /**
   * Адрес места
   */
  placeAddress: string;

  /**
   * Статус места
   */
  placeStatus: string;

  /**
   * Тип места
   */
  placeTypeCode: string;

  /**
   * Партия принята к учету
   */
  productBatchConfirmed: boolean;

  /**
   * Номер партии
   */
  productBatchNumber: string;

  /**
   * Тип партии
   */
  productBatchType: 'REGULAR' | 'RETURN';

  /**
   * Артикул
   */
  productId: string;

  /**
   * Название товара
   */
  productName: string;

  /**
   * Срок реализации
   */
  sellByTime: number;

  /**
   * id запаса
   */
  stockId: string;

  /**
   * Код вида запаса
   */
  stockTypeCode: string;

  /**
   * ПЕИ
   */
  unit: string;
}

/** my fixes */
