/* tslint:disable */
import { RejectedInfo } from './rejected-info';
export class TryAllocateSpaceForReplenishmentResponse {
  approvedQuantity?: number;
  operationId?: string;
  rejectedInfo?: RejectedInfo;
}

/** my fixes */
