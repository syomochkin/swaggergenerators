/* tslint:disable */
import { ProductAmountDto } from './product-amount-dto';
import { RouteStep } from './route-step';
export class BoxRoute {
  operationId?: string;
  plannedTotalDuration?: number;
  processBusinessKey?: string;
  rejectedItems?: Array<ProductAmountDto>;
  steps?: Array<RouteStep>;
  transferId?: string;
}

/** my fixes */
