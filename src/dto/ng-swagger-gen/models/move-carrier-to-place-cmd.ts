/* tslint:disable */
export class MoveCarrierToPlaceCmd {
  carrierId?: string;
  carrierReservationKey?: string;
  operationId?: string;
  placeId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  tabNumber?: string;
  transferId?: string;
  userName?: string;
}

/** my fixes */
