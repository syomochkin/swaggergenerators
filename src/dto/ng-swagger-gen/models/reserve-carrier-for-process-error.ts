/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class ReserveCarrierForProcessError extends AbstractWarehouseError{
  code?: string;
  message?: string;
}

/** my fixes */
