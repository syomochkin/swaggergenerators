/* tslint:disable */
import { ChangeRawStock } from './change-raw-stock';
export class ChangeRawStockTypeCmd {
  operationId?: string;
  orderVersion?: string;
  outboundDeliveryNumber?: string;
  outboundTime?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  stocks?: Array<ChangeRawStock>;
  tabNumber?: string;
  userName?: string;
}

/** my fixes */
