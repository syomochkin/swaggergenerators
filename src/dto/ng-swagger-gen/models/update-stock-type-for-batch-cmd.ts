/* tslint:disable */
export class UpdateStockTypeForBatchCmd {
  batchId?: string;
  newStockTypeCode?: string;
  oldStockTypeCode?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  tabNumber?: string;
  userName?: string;
}

/** my fixes */
