/* tslint:disable */
export class ProductBatchBalanceByStockType {
  baseAmountERP?: number;
  baseAmountWMS?: number;
  quantityERP?: number;
  quantityWMS?: number;
  stockTypeCode?: string;
}

/** my fixes */
