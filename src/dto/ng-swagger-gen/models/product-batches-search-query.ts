/* tslint:disable */
import { ProductBatchSearchFilter } from './product-batch-search-filter';
import { OrderByCriteria } from './order-by-criteria';
import { PageRequestInfo } from './page-request-info';
export class ProductBatchesSearchQuery {
  filter?: ProductBatchSearchFilter;
  orderInfo?: Array<OrderByCriteria>;
  pageInfo?: PageRequestInfo;
}

/** my fixes */
