/* tslint:disable */
export class MarkCreateDTO {
  code?: string;
  inboundDeliveryId?: string;
  inboundDeliveryPosition?: number;
  level?: 'TRANSPORT' | 'GROUP' | 'INDIVIDUAL';
  parentCode?: string;
  productBatchId?: string;
  productId?: string;
  status?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  type?: 'EGAIS' | 'CHZ';
  unit?: string;
}

/** my fixes */
