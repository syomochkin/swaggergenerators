/* tslint:disable */
export class MoveStockCmdResponse {
  movedQuantity?: number;
  operationId?: string;
  originalStockId?: string;
  targetStockActualQuantity?: number;
  targetStockId?: string;
}

/** my fixes */
