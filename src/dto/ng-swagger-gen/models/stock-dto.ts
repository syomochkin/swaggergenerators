/* tslint:disable */
export class StockDto {
  actualQuantity?: number;
  actualVolume?: number;
  actualWeight?: number;
  availableQuantity?: number;
  availableVolume?: number;
  availableWeight?: number;
  baseUnit?: string;
  batchConfirmed?: boolean;
  batchId?: string;
  batchNumber?: string;
  carrierId?: string;
  incomingQuantity?: number;
  incomingVolume?: number;
  incomingWeight?: number;
  outgoingQuantity?: number;
  outgoingVolume?: number;
  outgoingWeight?: number;
  placeId?: string;
  productId?: string;
  productName?: string;
  stockId?: string;
  stockTypeCode?: string;
  unit?: string;
}

/** my fixes */
