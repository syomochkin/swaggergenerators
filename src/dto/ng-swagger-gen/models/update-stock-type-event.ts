/* tslint:disable */
import { PlaceWithType } from './place-with-type';
export class UpdateStockTypeEvent {
  actualQuantity?: number;
  batchId?: string;
  batchNumber?: string;
  beiQuantity?: number;
  beiUnit?: string;
  carrierId?: string;
  documentDate?: string;
  documentNumber?: string;
  expirationDate?: string;
  manufactureDate?: string;
  marksAccountingType?: 'EGAIS' | 'CHZ';
  newStockId?: string;
  newStockTypeCode?: string;
  oldStockId?: string;
  oldStockTypeCode?: string;
  operationId?: string;
  operationType?: 'MOVE';
  orderItemIndex?: number;
  placeWithType?: PlaceWithType;
  productId?: string;
  productName?: string;
  salesOrder?: string;
  shouldNotifyERP?: boolean;
  siteCode?: string;
  specialStockCode?: string;
  storageDate?: string;
  tabNumber?: string;
  unit?: string;
  updateReason?: 'UNKNOWN' | 'PRODUCT_BATCH' | 'CORRUPTED_BARCODE' | 'DEFECT' | 'NO_PRODUCT' | 'EXPIRED';
}

/** my fixes */
