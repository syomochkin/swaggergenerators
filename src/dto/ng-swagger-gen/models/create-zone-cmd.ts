/* tslint:disable */
import { WarehouseProductGroupDto } from './warehouse-product-group-dto';
export class CreateZoneCmd {
  boundedWarehouseProductGroups?: Array<WarehouseProductGroupDto>;
  code?: string;
  description?: string;
  id?: string;
  name?: string;
  siteId?: string;
  typeId?: string;
}

/** my fixes */
