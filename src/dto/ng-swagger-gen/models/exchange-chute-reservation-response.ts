/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
import { ReserveInfo } from './reserve-info';
export class ExchangeChuteReservationResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
  sourceTransferId?: string;
  targetReserveInfo?: ReserveInfo;
}

/** my fixes */
