/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class UpdatePlaceStatusResponse {
  error?: AbstractWarehouseError;
  placeAddress?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

/** my fixes */
