/* tslint:disable */
import { Data } from './data';
import { NamedId } from './named-id';
export class PlaceNodeWithPosition {
  childPlaces?: Array<Data>;
  placeNodeChainFromTop?: Array<NamedId>;
}

/** my fixes */
