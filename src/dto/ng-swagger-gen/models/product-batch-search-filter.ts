/* tslint:disable */
export class ProductBatchSearchFilter {
  confirmed?: boolean;
  expirationDateFrom?: string;
  expirationDateTo?: string;
  inboundTimeFrom?: string;
  inboundTimeTo?: string;
  manufactureTimeFrom?: string;
  manufactureTimeTo?: string;
  marksAccountingType?: 'EGAIS' | 'CHZ';
  mercuryExtId?: string;
  number?: string;
  productId?: string;
  sellByTimeFrom?: string;
  sellByTimeTo?: string;
  siteCode?: string;
  siteId?: string;
  types?: Array<'REGULAR' | 'RETURN'>;
}

/** my fixes */
