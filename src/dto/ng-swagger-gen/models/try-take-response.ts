/* tslint:disable */
export class TryTakeResponse {
  acceptedQuantity?: number;
  operationId?: string;
  rejectReason?: string;
  rejected?: boolean;
  stockId?: string;
  transferId?: string;
}

/** my fixes */
