/* tslint:disable */
export class UpdateCarrierTypeCmd {
  description?: string;
  height?: number;
  length?: number;
  maxVolume?: number;
  maxWeight?: number;
  name?: string;
  width?: number;
}

/** my fixes */
