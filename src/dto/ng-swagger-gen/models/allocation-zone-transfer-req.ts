/* tslint:disable */
export class AllocationZoneTransferReq {
  carrierId?: string;
  destinationPlaceId?: string;
  operationId?: string;
  processBusinessKey?: string;
  startPlaceId?: string;
  transferId?: string;
}

/** my fixes */
