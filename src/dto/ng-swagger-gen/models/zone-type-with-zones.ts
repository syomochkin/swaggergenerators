/* tslint:disable */
import { ZoneDto } from './zone-dto';
export class ZoneTypeWithZones {
  code?: string;
  id?: string;
  name?: string;
  zones?: Array<ZoneDto>;
}

/** my fixes */
