/* tslint:disable */
import { ReservationDto } from './reservation-dto';
export class TransferDroppedResponse {
  canceledReservations?: Array<ReservationDto>;
  processBusinessKey?: string;
  transferId?: string;
}

/** my fixes */
