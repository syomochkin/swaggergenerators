/* tslint:disable */
export class UpdateRouteStrategyCmd {

  /**
   * Название стратегии (русскоязычное понятное навание стратегии)
   */
  name: string;

  /**
   * Код площадки
   */
  siteCode: string;

  /**
   * коды секторов, к которым будет привязана данная стратегия.
   */
  zoneCodes?: Array<string>;
}

/** my fixes */
