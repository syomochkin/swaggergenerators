/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class GeneralWarehouseError extends AbstractWarehouseError{
  code?: string;
  message?: string;
}

/** my fixes */
