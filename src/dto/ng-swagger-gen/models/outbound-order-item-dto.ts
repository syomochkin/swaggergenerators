/* tslint:disable */
export class OutboundOrderItemDto {
  baseAmount?: number;
  baseUnit?: string;
  position?: number;
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  unit?: string;
}

/** my fixes */
