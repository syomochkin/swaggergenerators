/* tslint:disable */
export class ProductBatchSearchDto {
  beiUnit?: string;
  beiUnitDenominator?: number;
  beiUnitNumerator?: number;
  confirmed?: boolean;
  createdTime?: number;
  expirationTime?: number;
  height?: number;
  id?: string;
  inboundTime?: number;
  length?: number;
  manufactureTime?: number;
  marksAccountingType?: 'EGAIS' | 'CHZ';
  mercuryExtId?: string;
  peiUnit?: string;
  productBatchNumber?: string;
  productId?: string;
  productName?: string;
  sellByTime?: number;
  siteCode?: string;
  siteId?: string;
  totalBaseAmountRemainder?: number;
  totalQuantityRemainder?: number;
  type?: 'REGULAR' | 'RETURN';
  vendorCode?: string;
  vendorName?: string;
  volume?: number;
  weightGross?: number;
  weightNet?: number;
  width?: number;
}

/** my fixes */
