/* tslint:disable */
export class MoveStockBetweenReplenishmentCarriersCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  quantity?: number;
  siteCode?: string;
  sourceCarrierReservationKey?: string;
  stockId?: string;
  tabNumber?: string;
  targetCarrierId?: string;
  targetCarrierReservationKey?: string;
  userName?: string;
}

/** my fixes */
