/* tslint:disable */
import { PlaceZoneInfo } from './place-zone-info';
export class PlaceInfo {
  address?: string;
  author?: string;
  childrenAmount?: number;
  createdTime?: number;
  editor?: string;
  id?: string;
  placeZones?: Array<PlaceZoneInfo>;
  siteId?: string;
  siteName?: string;
  typeName?: string;
  updatedTime?: number;
  zoneNames?: Array<string>;
}

/** my fixes */
