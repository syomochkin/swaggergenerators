/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class RouteNotVacantError extends AbstractWarehouseError{
  code?: string;
  message?: string;
}

/** my fixes */
