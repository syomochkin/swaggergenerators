/* tslint:disable */
import { Coordinates3D } from './coordinates-3d';
import { PlaceStatusDto } from './place-status-dto';
export class CreatePlaceCmd {
  address?: string;
  addressTypeId?: string;
  coordinates?: Coordinates3D;
  description?: string;
  id?: string;
  number?: number;
  parentId?: string;
  siteId?: string;
  status?: PlaceStatusDto;
  statusReason?: string;
  typeId?: string;
  zoneIds?: Array<string>;
}

/** my fixes */
