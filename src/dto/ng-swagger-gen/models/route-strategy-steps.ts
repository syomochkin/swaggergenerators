/* tslint:disable */
import { RouteStrategyStep } from './route-strategy-step';
export class RouteStrategySteps {

  /**
   * Последенее время редактирования стратегии
   */
  lastUpdateTime: number;

  /**
   * Пользователь, который последний раз редактировал стратегию
   */
  lastUpdateUser: string;

  /**
   * Идентификатор стратегии
   */
  routeStrategyId: string;

  /**
   * Название стратегии
   */
  routeStrategyName: string;

  /**
   * Последовательность секций для обхода
   */
  steps: Array<RouteStrategyStep>;
}

/** my fixes */
