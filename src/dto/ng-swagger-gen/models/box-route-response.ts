/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
import { RejectedProductItemAmount } from './rejected-product-item-amount';
import { TransferRoute } from './transfer-route';
export class BoxRouteResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  rejectedItems?: Array<RejectedProductItemAmount>;
  transferRoutes?: Array<TransferRoute>;
}

/** my fixes */
