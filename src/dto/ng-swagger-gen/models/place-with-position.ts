/* tslint:disable */
import { PlaceDto } from './place-dto';
import { NamedId } from './named-id';
export class PlaceWithPosition {
  place?: PlaceDto;
  placeNodeChainFromTop?: Array<NamedId>;
}

/** my fixes */
