/* tslint:disable */
export class RouteStep {
  aisleNumber?: number;
  cameraNumber?: number;
  floorNumber?: number;
  orderNumber: number;
  placeAddress: string;
  regionNumber?: number;
  rowNumber?: number;
  sectionNumber?: number;
}

/** my fixes */
