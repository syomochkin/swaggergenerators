/* tslint:disable */
import { ValueDto } from './value-dto';
export class PlaceCharacteristicDto {
  author?: string;
  characteristicId?: string;
  checkParents?: string;
  createdTime?: number;
  editable?: boolean;
  editor?: string;
  mandatory?: boolean;
  placeId?: string;
  siteId?: string;
  type?: string;
  typeCharacteristicId?: string;
  updatedTime?: number;
  value?: ValueDto;
}

/** my fixes */
