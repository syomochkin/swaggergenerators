/* tslint:disable */
import { PlaceInfo } from './place-info';
export class PlacesSearchResult {
  places?: Array<PlaceInfo>;
  totalCount?: number;
}

/** my fixes */
