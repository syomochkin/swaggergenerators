/* tslint:disable */
import { RejectedInfo } from './rejected-info';
export class CheckPlaceForStockResponse {
  approvedQuantity?: number;
  processBusinessKey?: string;
  rejectedInfo?: RejectedInfo;
  siteCode?: string;
}

/** my fixes */
