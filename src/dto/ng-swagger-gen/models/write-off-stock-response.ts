/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class WriteOffStockResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  stockId?: string;
}

/** my fixes */
