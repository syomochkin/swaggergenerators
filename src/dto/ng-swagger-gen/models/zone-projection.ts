/* tslint:disable */
import { WarehouseProductGroupDto } from './warehouse-product-group-dto';
export class ZoneProjection {
  author?: string;
  boundedWarehouseProductGroups?: Array<WarehouseProductGroupDto>;
  childAmount?: number;
  code?: string;
  createdTime?: number;
  description?: string;
  editor?: string;
  id?: string;
  name?: string;
  siteId?: string;
  typeId?: string;
  typeName?: string;
  updatedTime?: number;
}

/** my fixes */
