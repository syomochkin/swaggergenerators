/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
import { ProcessTransferInfo } from './process-transfer-info';
import { ReserveInfo } from './reserve-info';
export class ReservedPlacesForCarriersByMultiProcessesResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  rejectedTransfers?: Array<ProcessTransferInfo>;
  reserves?: Array<ReserveInfo>;
  siteCode?: string;
}

/** my fixes */
