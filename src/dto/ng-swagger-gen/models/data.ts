/* tslint:disable */
import { PlaceStatusDto } from './place-status-dto';
export class Data {
  address?: string;
  author?: string;
  childrenAmount?: number;
  createdTime?: number;
  editor?: string;
  id?: string;
  status?: PlaceStatusDto;
  typeName?: string;
  updatedTime?: number;
  zoneNames?: Array<string>;
}

/** my fixes */
