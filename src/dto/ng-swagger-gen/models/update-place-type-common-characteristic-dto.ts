/* tslint:disable */
import { DefaultValue } from './default-value';
export class UpdatePlaceTypeCommonCharacteristicDto {
  characteristic?: string;
  defaultValue?: DefaultValue;
}

/** my fixes */
