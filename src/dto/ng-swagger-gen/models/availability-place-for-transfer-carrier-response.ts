/* tslint:disable */
import { AvailableCarrierTypeDto } from './available-carrier-type-dto';
export class AvailabilityPlaceForTransferCarrierResponse {
  availableCarrierTypes: Array<AvailableCarrierTypeDto>;
}

/** my fixes */
