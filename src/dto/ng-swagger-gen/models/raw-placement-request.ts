/* tslint:disable */
export class RawPlacementRequest {
  incomingCarrierId?: string;
  openCarriers?: Array<string>;
  operationId?: string;
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  siteCode?: string;
  stockTypeCode?: string;
}

/** my fixes */
