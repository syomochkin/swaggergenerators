/* tslint:disable */
export class UpdateStockTypeByPickingCmd {
  docDate?: string;
  documentNumber?: string;
  operationId?: string;
  operationType?: 'MOVE';
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  stockId?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  updateReason?: 'UNKNOWN' | 'PRODUCT_BATCH' | 'CORRUPTED_BARCODE' | 'DEFECT' | 'NO_PRODUCT' | 'EXPIRED';
  userName?: string;
}

/** my fixes */
