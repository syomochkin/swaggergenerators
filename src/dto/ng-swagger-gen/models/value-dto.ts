/* tslint:disable */
export class ValueDto {
  decValue?: number;
  intValue?: number;
  maxDecValue?: number;
  maxIntValue?: number;
  minDecValue?: number;
  minIntValue?: number;
}

/** my fixes */
