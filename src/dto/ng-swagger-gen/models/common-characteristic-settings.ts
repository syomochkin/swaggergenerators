/* tslint:disable */
export class CommonCharacteristicSettings {
  editable?: boolean;
  mandatory?: boolean;
  parentInheritance?: 'check' | 'ignore';
  unit?: string;
}

/** my fixes */
