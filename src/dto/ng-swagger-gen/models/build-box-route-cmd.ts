/* tslint:disable */
import { ProductAmountDto } from './product-amount-dto';
export class BuildBoxRouteCmd {
  items?: Array<ProductAmountDto>;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
  transferId?: string;
}

/** my fixes */
