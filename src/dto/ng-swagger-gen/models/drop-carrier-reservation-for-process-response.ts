/* tslint:disable */
export class DropCarrierReservationForProcessResponse {
  carrierIds?: Array<string>;
  processBusinessKey?: string;
  siteCode?: string;
}

/** my fixes */
