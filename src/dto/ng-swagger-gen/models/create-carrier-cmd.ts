/* tslint:disable */
export class CreateCarrierCmd {
  carrierBarcode?: string;
  carrierId?: string;
  carrierReservationKey?: string;
  carrierTypeCode?: string;
  operationId?: string;
  placeBarcode?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

/** my fixes */
