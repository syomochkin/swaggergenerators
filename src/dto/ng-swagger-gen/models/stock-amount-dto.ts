/* tslint:disable */
export class StockAmountDto {
  createMarksOnPicking?: boolean;
  marksAccountingType?: 'EGAIS' | 'CHZ';
  productId?: string;
  quantity?: number;
  stockId?: string;
  unit?: string;
}

/** my fixes */
