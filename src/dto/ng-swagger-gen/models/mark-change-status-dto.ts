/* tslint:disable */
export class MarkChangeStatusDTO {
  code?: string;
  productBatchId?: string;
  productId?: string;
  sourceStatus?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  targetStatus?: 'INBOUND_DELIVERY' | 'REJECTED' | 'INBOUND_SCANNED' | 'INBOUND_EGAIS_CHECK' | 'FOR_SALE' | 'PICKED' | 'SOLD' | 'CLIENT_RETURN' | 'RETURN_EGAIS_CHECK' | 'WRITTEN_OFF' | 'FOUND' | 'CANCELLED';
  type?: 'EGAIS' | 'CHZ';
  unit?: string;
}

/** my fixes */
