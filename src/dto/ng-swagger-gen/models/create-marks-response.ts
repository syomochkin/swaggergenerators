/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
export class CreateMarksResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

/** my fixes */
