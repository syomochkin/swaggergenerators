/* tslint:disable */
export class TryAllocateSpaceForReplenishmentCmd {
  destPlaceBarcode?: string;
  operationId?: string;
  processId?: string;
  productBatchId?: string;
  quantity?: number;
  siteCode?: string;
  srcCarrierId?: string;
  stockTypeCode?: string;
}

/** my fixes */
