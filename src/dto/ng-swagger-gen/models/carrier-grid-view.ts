/* tslint:disable */
import { CarrierTypeGridView } from './carrier-type-grid-view';
export class CarrierGridView {
  barcode?: string;
  id?: string;
  nestedCarriers?: Array<CarrierGridView>;
  status?: 'incoming' | 'outgoing' | 'actual';
  type?: CarrierTypeGridView;
}

/** my fixes */
