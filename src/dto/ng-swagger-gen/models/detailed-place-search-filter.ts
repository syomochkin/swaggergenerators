/* tslint:disable */
export class DetailedPlaceSearchFilter {
  placeCategory?: 'STORAGE_PLACE' | 'NOT_STORAGE_PLACE' | 'ALL';
  placeIds?: Array<string>;
  placeStatus?: string;
  placeTypeCode?: string;
  siteCode: string;
  withHierarchy?: boolean;
  zoneCode?: string;
  zoneId?: string;
  zoneTypeCode?: string;
}

/** my fixes */
