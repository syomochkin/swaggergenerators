/* tslint:disable */
export class RouteStrategyStep {

  /**
   * Адрес места/секции
   */
  sectionAddress: string;

  /**
   * Признак, является ли данное место - местом старта
   */
  startPlace: boolean;

  /**
   * Порядковый номер секции в стратегии обхода
   */
  step: number;
}

/** my fixes */
