/* tslint:disable */
import { ReservationDto } from './reservation-dto';
export class ReserveStockOutgoingQuantityResponse {
  operationId?: string;
  reservation?: ReservationDto;
}

/** my fixes */
