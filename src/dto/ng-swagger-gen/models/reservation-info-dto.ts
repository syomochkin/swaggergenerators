/* tslint:disable */
export class ReservationInfoDto {
  batchId?: string;
  incomingQuantity?: number;
  outgoingQuantity?: number;
  productId?: string;
  stockId?: string;
  stockTypeCode?: string;
  unit?: string;
}

/** my fixes */
