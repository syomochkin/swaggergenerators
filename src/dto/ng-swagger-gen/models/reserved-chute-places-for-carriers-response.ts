/* tslint:disable */
import { AbstractWarehouseError } from './abstract-warehouse-error';
import { TransferDto } from './transfer-dto';
import { ReserveInfo } from './reserve-info';
export class ReservedChutePlacesForCarriersResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  rejectedTransfers?: Array<TransferDto>;
  reserves?: Array<ReserveInfo>;
  siteCode?: string;
}

/** my fixes */
