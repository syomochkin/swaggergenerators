/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceTypeDto } from '../models/place-type-dto';
import { CreatePlaceTypeCmd } from '../models/create-place-type-cmd';
import { CodeExists } from '../models/code-exists';
import { PlaceTypeCodeExistsQuery } from '../models/place-type-code-exists-query';
import { UpdatePlaceTypeCmd } from '../models/update-place-type-cmd';

/**
 * Place Type Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceTypeGatewayService extends __BaseService {
  static readonly getPlaceTypesBySiteUsingGET1Path = '/v1/topology/sites/{siteId}/placeTypes';
  static readonly createPlaceTypeUsingPOSTPath = '/v1/topology/sites/{siteId}/placeTypes';
  static readonly getAvailablePlaceTypesByParentPlaceIdUsingGETPath = '/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId';
  static readonly codeExistsUsingPOST1Path = '/v1/topology/sites/{siteId}/placeTypes/codeExists';
  static readonly getPlaceTypeUsingGETPath = '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';
  static readonly updatePlaceTypeUsingPUTPath = '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос на получение типов мест для площадки с количеством их экземпляров
   *
   * siteId - обязательное поле
   * @param siteId siteId
   * @return OK
   */
  getPlaceTypesBySiteUsingGET1Response(siteId: string): __Observable<__StrictHttpResponse<Array<PlaceTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(siteId))}/placeTypes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceTypeDto>>;
      })
    );
  }
  /**
   * Запрос на получение типов мест для площадки с количеством их экземпляров
   *
   * siteId - обязательное поле
   * @param siteId siteId
   * @return OK
   */
  getPlaceTypesBySiteUsingGET1(siteId: string): __Observable<Array<PlaceTypeDto>> {
    return this.getPlaceTypesBySiteUsingGET1Response(siteId).pipe(
      __map(_r => _r.body as Array<PlaceTypeDto>)
    );
  }

  /**
   * createPlaceType
   * @param params The `PlaceTypeGatewayService.CreatePlaceTypeUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  createPlaceTypeUsingPOSTResponse(params: PlaceTypeGatewayService.CreatePlaceTypeUsingPOSTParams): __Observable<__StrictHttpResponse<PlaceTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}/placeTypes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceTypeDto>;
      })
    );
  }
  /**
   * createPlaceType
   * @param params The `PlaceTypeGatewayService.CreatePlaceTypeUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  createPlaceTypeUsingPOST(params: PlaceTypeGatewayService.CreatePlaceTypeUsingPOSTParams): __Observable<PlaceTypeDto> {
    return this.createPlaceTypeUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as PlaceTypeDto)
    );
  }

  /**
   * getAvailablePlaceTypesByParentPlaceId
   * @param params The `PlaceTypeGatewayService.GetAvailablePlaceTypesByParentPlaceIdUsingGETParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `parentPlaceId`: parentPlaceId
   *
   * @return OK
   */
  getAvailablePlaceTypesByParentPlaceIdUsingGETResponse(params: PlaceTypeGatewayService.GetAvailablePlaceTypesByParentPlaceIdUsingGETParams): __Observable<__StrictHttpResponse<Array<PlaceTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.parentPlaceId != null) __params = __params.set('parentPlaceId', params.parentPlaceId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}/placeTypes/availableByParentPlaceId`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceTypeDto>>;
      })
    );
  }
  /**
   * getAvailablePlaceTypesByParentPlaceId
   * @param params The `PlaceTypeGatewayService.GetAvailablePlaceTypesByParentPlaceIdUsingGETParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `parentPlaceId`: parentPlaceId
   *
   * @return OK
   */
  getAvailablePlaceTypesByParentPlaceIdUsingGET(params: PlaceTypeGatewayService.GetAvailablePlaceTypesByParentPlaceIdUsingGETParams): __Observable<Array<PlaceTypeDto>> {
    return this.getAvailablePlaceTypesByParentPlaceIdUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<PlaceTypeDto>)
    );
  }

  /**
   * codeExists
   * @param params The `PlaceTypeGatewayService.CodeExistsUsingPOST1Params` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `query`: query
   *
   * @return OK
   */
  codeExistsUsingPOST1Response(params: PlaceTypeGatewayService.CodeExistsUsingPOST1Params): __Observable<__StrictHttpResponse<CodeExists>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}/placeTypes/codeExists`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CodeExists>;
      })
    );
  }
  /**
   * codeExists
   * @param params The `PlaceTypeGatewayService.CodeExistsUsingPOST1Params` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `query`: query
   *
   * @return OK
   */
  codeExistsUsingPOST1(params: PlaceTypeGatewayService.CodeExistsUsingPOST1Params): __Observable<CodeExists> {
    return this.codeExistsUsingPOST1Response(params).pipe(
      __map(_r => _r.body as CodeExists)
    );
  }

  /**
   * getPlaceType
   * @param params The `PlaceTypeGatewayService.GetPlaceTypeUsingGETParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placeTypeId`: placeTypeId
   *
   * @return OK
   */
  getPlaceTypeUsingGETResponse(params: PlaceTypeGatewayService.GetPlaceTypeUsingGETParams): __Observable<__StrictHttpResponse<PlaceTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}/placeTypes/${encodeURIComponent(String(params.placeTypeId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceTypeDto>;
      })
    );
  }
  /**
   * getPlaceType
   * @param params The `PlaceTypeGatewayService.GetPlaceTypeUsingGETParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placeTypeId`: placeTypeId
   *
   * @return OK
   */
  getPlaceTypeUsingGET(params: PlaceTypeGatewayService.GetPlaceTypeUsingGETParams): __Observable<PlaceTypeDto> {
    return this.getPlaceTypeUsingGETResponse(params).pipe(
      __map(_r => _r.body as PlaceTypeDto)
    );
  }

  /**
   * updatePlaceType
   * @param params The `PlaceTypeGatewayService.UpdatePlaceTypeUsingPUTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updatePlaceTypeUsingPUTResponse(params: PlaceTypeGatewayService.UpdatePlaceTypeUsingPUTParams): __Observable<__StrictHttpResponse<PlaceTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}/placeTypes/${encodeURIComponent(String(params.placeTypeId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceTypeDto>;
      })
    );
  }
  /**
   * updatePlaceType
   * @param params The `PlaceTypeGatewayService.UpdatePlaceTypeUsingPUTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updatePlaceTypeUsingPUT(params: PlaceTypeGatewayService.UpdatePlaceTypeUsingPUTParams): __Observable<PlaceTypeDto> {
    return this.updatePlaceTypeUsingPUTResponse(params).pipe(
      __map(_r => _r.body as PlaceTypeDto)
    );
  }
}

module PlaceTypeGatewayService {

  /**
   * Parameters for createPlaceTypeUsingPOST
   */
  export interface CreatePlaceTypeUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * cmd
     */
    cmd: CreatePlaceTypeCmd;
  }

  /**
   * Parameters for getAvailablePlaceTypesByParentPlaceIdUsingGET
   */
  export interface GetAvailablePlaceTypesByParentPlaceIdUsingGETParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * parentPlaceId
     */
    parentPlaceId?: string;
  }

  /**
   * Parameters for codeExistsUsingPOST1
   */
  export interface CodeExistsUsingPOST1Params {

    /**
     * siteId
     */
    siteId: string;

    /**
     * query
     */
    query: PlaceTypeCodeExistsQuery;
  }

  /**
   * Parameters for getPlaceTypeUsingGET
   */
  export interface GetPlaceTypeUsingGETParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * placeTypeId
     */
    placeTypeId: string;
  }

  /**
   * Parameters for updatePlaceTypeUsingPUT
   */
  export interface UpdatePlaceTypeUsingPUTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * placeTypeId
     */
    placeTypeId: string;

    /**
     * cmd
     */
    cmd: UpdatePlaceTypeCmd;
  }
}

export { PlaceTypeGatewayService }
