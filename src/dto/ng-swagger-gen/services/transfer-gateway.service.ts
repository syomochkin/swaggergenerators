/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { AvailabilityPlaceForTransferCarrierResponse } from '../models/availability-place-for-transfer-carrier-response';
import { AvailabilityPlaceForTransferCarrierCmd } from '../models/availability-place-for-transfer-carrier-cmd';
import { PlaceShortView } from '../models/place-short-view';
import { MoveCarrierToPlaceResponse } from '../models/move-carrier-to-place-response';
import { MoveCarrierToPlaceCmd } from '../models/move-carrier-to-place-cmd';
import { SetCarrierPlaceCmd } from '../models/set-carrier-place-cmd';

/**
 * Transfer Gateway
 */
@Injectable({
  providedIn: 'root',
})
class TransferGatewayService extends __BaseService {
  static readonly availabilityAisleUsingPUTPath = '/v1/transfer/availabilityPlaceForTransferCarrier';
  static readonly getRouteUsingPOSTPath = '/v1/transfer/getRoute/{carrierId}';
  static readonly moveCarrierToPlaceUsingPOSTPath = '/v1/transfer/moveCarrierToPlace';
  static readonly setCarrierPlaceUsingPOSTPath = '/v1/transfer/setCarrierPlace';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Проверка доступности места для трансфера носителя
   * @param cmd cmd
   * @return OK
   */
  availabilityAisleUsingPUTResponse(cmd: AvailabilityPlaceForTransferCarrierCmd): __Observable<__StrictHttpResponse<AvailabilityPlaceForTransferCarrierResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/transfer/availabilityPlaceForTransferCarrier`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<AvailabilityPlaceForTransferCarrierResponse>;
      })
    );
  }
  /**
   * Проверка доступности места для трансфера носителя
   * @param cmd cmd
   * @return OK
   */
  availabilityAisleUsingPUT(cmd: AvailabilityPlaceForTransferCarrierCmd): __Observable<AvailabilityPlaceForTransferCarrierResponse> {
    return this.availabilityAisleUsingPUTResponse(cmd).pipe(
      __map(_r => _r.body as AvailabilityPlaceForTransferCarrierResponse)
    );
  }

  /**
   * getRoute
   * @param carrierId carrierId
   * @return OK
   */
  getRouteUsingPOSTResponse(carrierId: string): __Observable<__StrictHttpResponse<Array<PlaceShortView>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/transfer/getRoute/${encodeURIComponent(String(carrierId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceShortView>>;
      })
    );
  }
  /**
   * getRoute
   * @param carrierId carrierId
   * @return OK
   */
  getRouteUsingPOST(carrierId: string): __Observable<Array<PlaceShortView>> {
    return this.getRouteUsingPOSTResponse(carrierId).pipe(
      __map(_r => _r.body as Array<PlaceShortView>)
    );
  }

  /**
   * Перенос носителя в место
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  moveCarrierToPlaceUsingPOSTResponse(cmd: MoveCarrierToPlaceCmd): __Observable<__StrictHttpResponse<MoveCarrierToPlaceResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/transfer/moveCarrierToPlace`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<MoveCarrierToPlaceResponse>;
      })
    );
  }
  /**
   * Перенос носителя в место
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  moveCarrierToPlaceUsingPOST(cmd: MoveCarrierToPlaceCmd): __Observable<MoveCarrierToPlaceResponse> {
    return this.moveCarrierToPlaceUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as MoveCarrierToPlaceResponse)
    );
  }

  /**
   * Размещение носителя в месте без проверок
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  setCarrierPlaceUsingPOSTResponse(cmd: SetCarrierPlaceCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/transfer/setCarrierPlace`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Размещение носителя в месте без проверок
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  setCarrierPlaceUsingPOST(cmd: SetCarrierPlaceCmd): __Observable<null> {
    return this.setCarrierPlaceUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module TransferGatewayService {
}

export { TransferGatewayService }
