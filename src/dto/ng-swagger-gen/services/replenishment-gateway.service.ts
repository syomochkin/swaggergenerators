/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { MoveStockBetweenReplenishmentCarriersResponse } from '../models/move-stock-between-replenishment-carriers-response';
import { MoveStockBetweenReplenishmentCarriersCmd } from '../models/move-stock-between-replenishment-carriers-cmd';
import { ReplenishCmd } from '../models/replenish-cmd';
import { Route } from '../models/route';
import { AllocationZoneTransferReq } from '../models/allocation-zone-transfer-req';
import { TryAllocateSpaceForReplenishmentResponse } from '../models/try-allocate-space-for-replenishment-response';
import { TryAllocateSpaceForReplenishmentCmd } from '../models/try-allocate-space-for-replenishment-cmd';

/**
 * Replenishment Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ReplenishmentGatewayService extends __BaseService {
  static readonly moveStockBetweenCarriersUsingPOSTPath = '/v1/replenishment/moveStockBetweenCarriers';
  static readonly replenishUsingPOSTPath = '/v1/replenishment/replenish';
  static readonly findTransferUsingPOST1Path = '/v1/replenishment/transfer/build';
  static readonly tryAllocateSpaceRawUsingPOSTPath = '/v1/replenishment/tryAllocateRaw';
  static readonly tryAllocateSpaceUsingPOSTPath = '/v1/replenishment/tryAllocateSpace';
  static readonly tryAllocateSpaceManualUsingPOSTPath = '/v1/replenishment/tryAllocateSpaceManual';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Перемещение запаса между носителями с рокировкой входящего резерва в месте
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param request request
   * @return OK
   */
  moveStockBetweenCarriersUsingPOSTResponse(request: MoveStockBetweenReplenishmentCarriersCmd): __Observable<__StrictHttpResponse<MoveStockBetweenReplenishmentCarriersResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/replenishment/moveStockBetweenCarriers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<MoveStockBetweenReplenishmentCarriersResponse>;
      })
    );
  }
  /**
   * Перемещение запаса между носителями с рокировкой входящего резерва в месте
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param request request
   * @return OK
   */
  moveStockBetweenCarriersUsingPOST(request: MoveStockBetweenReplenishmentCarriersCmd): __Observable<MoveStockBetweenReplenishmentCarriersResponse> {
    return this.moveStockBetweenCarriersUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as MoveStockBetweenReplenishmentCarriersResponse)
    );
  }

  /**
   * Размещение товара
   * @param cmd cmd
   */
  replenishUsingPOSTResponse(cmd: ReplenishCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/replenishment/replenish`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Размещение товара
   * @param cmd cmd
   */
  replenishUsingPOST(cmd: ReplenishCmd): __Observable<null> {
    return this.replenishUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на построение и резервирование маршрута для перехода между областями действий
   * @param cmd cmd
   * @return OK
   */
  findTransferUsingPOST1Response(cmd: AllocationZoneTransferReq): __Observable<__StrictHttpResponse<Route>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/replenishment/transfer/build`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Route>;
      })
    );
  }
  /**
   * Запрос на построение и резервирование маршрута для перехода между областями действий
   * @param cmd cmd
   * @return OK
   */
  findTransferUsingPOST1(cmd: AllocationZoneTransferReq): __Observable<Route> {
    return this.findTransferUsingPOST1Response(cmd).pipe(
      __map(_r => _r.body as Route)
    );
  }

  /**
   * Проверка места перед ручным перемещением сырья
   * @param cmd cmd
   * @return OK
   */
  tryAllocateSpaceRawUsingPOSTResponse(cmd: TryAllocateSpaceForReplenishmentCmd): __Observable<__StrictHttpResponse<TryAllocateSpaceForReplenishmentResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/replenishment/tryAllocateRaw`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TryAllocateSpaceForReplenishmentResponse>;
      })
    );
  }
  /**
   * Проверка места перед ручным перемещением сырья
   * @param cmd cmd
   * @return OK
   */
  tryAllocateSpaceRawUsingPOST(cmd: TryAllocateSpaceForReplenishmentCmd): __Observable<TryAllocateSpaceForReplenishmentResponse> {
    return this.tryAllocateSpaceRawUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as TryAllocateSpaceForReplenishmentResponse)
    );
  }

  /**
   * Проверка места перед размещением товара
   * @param cmd cmd
   * @return OK
   */
  tryAllocateSpaceUsingPOSTResponse(cmd: TryAllocateSpaceForReplenishmentCmd): __Observable<__StrictHttpResponse<TryAllocateSpaceForReplenishmentResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/replenishment/tryAllocateSpace`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TryAllocateSpaceForReplenishmentResponse>;
      })
    );
  }
  /**
   * Проверка места перед размещением товара
   * @param cmd cmd
   * @return OK
   */
  tryAllocateSpaceUsingPOST(cmd: TryAllocateSpaceForReplenishmentCmd): __Observable<TryAllocateSpaceForReplenishmentResponse> {
    return this.tryAllocateSpaceUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as TryAllocateSpaceForReplenishmentResponse)
    );
  }

  /**
   * Проверка места перед ручным перемещением товара
   * @param cmd cmd
   * @return OK
   */
  tryAllocateSpaceManualUsingPOSTResponse(cmd: TryAllocateSpaceForReplenishmentCmd): __Observable<__StrictHttpResponse<TryAllocateSpaceForReplenishmentResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/replenishment/tryAllocateSpaceManual`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TryAllocateSpaceForReplenishmentResponse>;
      })
    );
  }
  /**
   * Проверка места перед ручным перемещением товара
   * @param cmd cmd
   * @return OK
   */
  tryAllocateSpaceManualUsingPOST(cmd: TryAllocateSpaceForReplenishmentCmd): __Observable<TryAllocateSpaceForReplenishmentResponse> {
    return this.tryAllocateSpaceManualUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as TryAllocateSpaceForReplenishmentResponse)
    );
  }
}

module ReplenishmentGatewayService {
}

export { ReplenishmentGatewayService }
