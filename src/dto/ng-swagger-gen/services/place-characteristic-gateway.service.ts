/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceCharacteristicDto } from '../models/place-characteristic-dto';
import { OverridePlaceCommonCharacteristicCmd } from '../models/override-place-common-characteristic-cmd';

/**
 * Place Characteristic Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceCharacteristicGatewayService extends __BaseService {
  static readonly getPlaceCharacteristicsUsingGETPath = '/v1/topology/places/{placeId}/characteristics';
  static readonly getPlaceCharacteristicUsingGETPath = '/v1/topology/places/{placeId}/characteristics/{characteristicId}';
  static readonly overrideUsingPUTPath = '/v1/topology/places/{placeId}/characteristics/{characteristicId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getPlaceCharacteristics
   * @param placeId placeId
   * @return OK
   */
  getPlaceCharacteristicsUsingGETResponse(placeId: string): __Observable<__StrictHttpResponse<Array<PlaceCharacteristicDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/${encodeURIComponent(String(placeId))}/characteristics`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceCharacteristicDto>>;
      })
    );
  }
  /**
   * getPlaceCharacteristics
   * @param placeId placeId
   * @return OK
   */
  getPlaceCharacteristicsUsingGET(placeId: string): __Observable<Array<PlaceCharacteristicDto>> {
    return this.getPlaceCharacteristicsUsingGETResponse(placeId).pipe(
      __map(_r => _r.body as Array<PlaceCharacteristicDto>)
    );
  }

  /**
   * getPlaceCharacteristic
   * @param params The `PlaceCharacteristicGatewayService.GetPlaceCharacteristicUsingGETParams` containing the following parameters:
   *
   * - `placeId`: placeId
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  getPlaceCharacteristicUsingGETResponse(params: PlaceCharacteristicGatewayService.GetPlaceCharacteristicUsingGETParams): __Observable<__StrictHttpResponse<PlaceCharacteristicDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/${encodeURIComponent(String(params.placeId))}/characteristics/${encodeURIComponent(String(params.characteristicId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceCharacteristicDto>;
      })
    );
  }
  /**
   * getPlaceCharacteristic
   * @param params The `PlaceCharacteristicGatewayService.GetPlaceCharacteristicUsingGETParams` containing the following parameters:
   *
   * - `placeId`: placeId
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  getPlaceCharacteristicUsingGET(params: PlaceCharacteristicGatewayService.GetPlaceCharacteristicUsingGETParams): __Observable<PlaceCharacteristicDto> {
    return this.getPlaceCharacteristicUsingGETResponse(params).pipe(
      __map(_r => _r.body as PlaceCharacteristicDto)
    );
  }

  /**
   * override
   * @param params The `PlaceCharacteristicGatewayService.OverrideUsingPUTParams` containing the following parameters:
   *
   * - `placeId`: placeId
   *
   * - `cmd`: cmd
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  overrideUsingPUTResponse(params: PlaceCharacteristicGatewayService.OverrideUsingPUTParams): __Observable<__StrictHttpResponse<PlaceCharacteristicDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/places/${encodeURIComponent(String(params.placeId))}/characteristics/${encodeURIComponent(String(params.characteristicId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceCharacteristicDto>;
      })
    );
  }
  /**
   * override
   * @param params The `PlaceCharacteristicGatewayService.OverrideUsingPUTParams` containing the following parameters:
   *
   * - `placeId`: placeId
   *
   * - `cmd`: cmd
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  overrideUsingPUT(params: PlaceCharacteristicGatewayService.OverrideUsingPUTParams): __Observable<PlaceCharacteristicDto> {
    return this.overrideUsingPUTResponse(params).pipe(
      __map(_r => _r.body as PlaceCharacteristicDto)
    );
  }
}

module PlaceCharacteristicGatewayService {

  /**
   * Parameters for getPlaceCharacteristicUsingGET
   */
  export interface GetPlaceCharacteristicUsingGETParams {

    /**
     * placeId
     */
    placeId: string;

    /**
     * characteristicId
     */
    characteristicId: string;
  }

  /**
   * Parameters for overrideUsingPUT
   */
  export interface OverrideUsingPUTParams {

    /**
     * placeId
     */
    placeId: string;

    /**
     * cmd
     */
    cmd: OverridePlaceCommonCharacteristicCmd;

    /**
     * characteristicId
     */
    characteristicId: string;
  }
}

export { PlaceCharacteristicGatewayService }
