/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ZoneTypeDto } from '../models/zone-type-dto';
import { CreateZoneTypeCmd } from '../models/create-zone-type-cmd';
import { CodeExists } from '../models/code-exists';
import { ZoneTypeCodeExistsQuery } from '../models/zone-type-code-exists-query';
import { ZoneTypeWithZonesDto } from '../models/zone-type-with-zones-dto';
import { UpdateZoneTypeCmd } from '../models/update-zone-type-cmd';

/**
 * Zone Type Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ZoneTypeGatewayService extends __BaseService {
  static readonly getZoneTypesUsingGET2Path = '/v1/topology/zone-types';
  static readonly createUsingPOST3Path = '/v1/topology/zone-types';
  static readonly codeExistsUsingPOST4Path = '/v1/topology/zone-types/codeExists';
  static readonly getZoneTypesWithZonesUsingGET1Path = '/v1/topology/zone-types/withZones';
  static readonly getZoneTypeUsingGETPath = '/v1/topology/zone-types/{id}';
  static readonly deleteUsingDELETE2Path = '/v1/topology/zone-types/{id}';
  static readonly updateUsingPUT3Path = '/v1/topology/zone-types/{zoneTypeId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getZoneTypes
   * @return OK
   */
  getZoneTypesUsingGET2Response(): __Observable<__StrictHttpResponse<Array<ZoneTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zone-types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneTypeDto>>;
      })
    );
  }
  /**
   * getZoneTypes
   * @return OK
   */
  getZoneTypesUsingGET2(): __Observable<Array<ZoneTypeDto>> {
    return this.getZoneTypesUsingGET2Response().pipe(
      __map(_r => _r.body as Array<ZoneTypeDto>)
    );
  }

  /**
   * create
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST3Response(cmd: CreateZoneTypeCmd): __Observable<__StrictHttpResponse<ZoneTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zone-types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneTypeDto>;
      })
    );
  }
  /**
   * create
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST3(cmd: CreateZoneTypeCmd): __Observable<ZoneTypeDto> {
    return this.createUsingPOST3Response(cmd).pipe(
      __map(_r => _r.body as ZoneTypeDto)
    );
  }

  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST4Response(query: ZoneTypeCodeExistsQuery): __Observable<__StrictHttpResponse<CodeExists>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zone-types/codeExists`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CodeExists>;
      })
    );
  }
  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST4(query: ZoneTypeCodeExistsQuery): __Observable<CodeExists> {
    return this.codeExistsUsingPOST4Response(query).pipe(
      __map(_r => _r.body as CodeExists)
    );
  }

  /**
   * getZoneTypesWithZones
   * @param siteId siteId
   * @return OK
   */
  getZoneTypesWithZonesUsingGET1Response(siteId?: string): __Observable<__StrictHttpResponse<Array<ZoneTypeWithZonesDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (siteId != null) __params = __params.set('siteId', siteId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zone-types/withZones`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneTypeWithZonesDto>>;
      })
    );
  }
  /**
   * getZoneTypesWithZones
   * @param siteId siteId
   * @return OK
   */
  getZoneTypesWithZonesUsingGET1(siteId?: string): __Observable<Array<ZoneTypeWithZonesDto>> {
    return this.getZoneTypesWithZonesUsingGET1Response(siteId).pipe(
      __map(_r => _r.body as Array<ZoneTypeWithZonesDto>)
    );
  }

  /**
   * getZoneType
   * @param id id
   * @return OK
   */
  getZoneTypeUsingGETResponse(id: string): __Observable<__StrictHttpResponse<ZoneTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zone-types/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneTypeDto>;
      })
    );
  }
  /**
   * getZoneType
   * @param id id
   * @return OK
   */
  getZoneTypeUsingGET(id: string): __Observable<ZoneTypeDto> {
    return this.getZoneTypeUsingGETResponse(id).pipe(
      __map(_r => _r.body as ZoneTypeDto)
    );
  }

  /**
   * delete
   * @param id id
   */
  deleteUsingDELETE2Response(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/topology/zone-types/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * delete
   * @param id id
   */
  deleteUsingDELETE2(id: string): __Observable<null> {
    return this.deleteUsingDELETE2Response(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * update
   * @param params The `ZoneTypeGatewayService.UpdateUsingPUT3Params` containing the following parameters:
   *
   * - `zoneTypeId`: zoneTypeId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateUsingPUT3Response(params: ZoneTypeGatewayService.UpdateUsingPUT3Params): __Observable<__StrictHttpResponse<ZoneTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/zone-types/${encodeURIComponent(String(params.zoneTypeId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneTypeDto>;
      })
    );
  }
  /**
   * update
   * @param params The `ZoneTypeGatewayService.UpdateUsingPUT3Params` containing the following parameters:
   *
   * - `zoneTypeId`: zoneTypeId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateUsingPUT3(params: ZoneTypeGatewayService.UpdateUsingPUT3Params): __Observable<ZoneTypeDto> {
    return this.updateUsingPUT3Response(params).pipe(
      __map(_r => _r.body as ZoneTypeDto)
    );
  }
}

module ZoneTypeGatewayService {

  /**
   * Parameters for updateUsingPUT3
   */
  export interface UpdateUsingPUT3Params {

    /**
     * zoneTypeId
     */
    zoneTypeId: string;

    /**
     * cmd
     */
    cmd: UpdateZoneTypeCmd;
  }
}

export { ZoneTypeGatewayService }
