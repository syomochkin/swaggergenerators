/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ProductPlacementResponse } from '../models/product-placement-response';
import { MonoPalletPlacementRequest } from '../models/mono-pallet-placement-request';
import { ProductPlacementRequest } from '../models/product-placement-request';
import { RawPlacementRequest } from '../models/raw-placement-request';

/**
 * Allocation Gateway
 */
@Injectable({
  providedIn: 'root',
})
class AllocationGatewayService extends __BaseService {
  static readonly monoPalletProductPlacementUsingPOSTPath = '/v1/allocations/monoPalletProductPlacement';
  static readonly productPlacementUsingPOSTPath = '/v1/allocations/productPlacement';
  static readonly rawPlacementUsingPOSTPath = '/v1/allocations/rawPlacement';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос в МРТ на размещение монопаллеты товара
   *
   * operationId, productId, productBatchId - обязательные поля
   * @param request request
   * @return OK
   */
  monoPalletProductPlacementUsingPOSTResponse(request: MonoPalletPlacementRequest): __Observable<__StrictHttpResponse<ProductPlacementResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/allocations/monoPalletProductPlacement`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductPlacementResponse>;
      })
    );
  }
  /**
   * Запрос в МРТ на размещение монопаллеты товара
   *
   * operationId, productId, productBatchId - обязательные поля
   * @param request request
   * @return OK
   */
  monoPalletProductPlacementUsingPOST(request: MonoPalletPlacementRequest): __Observable<ProductPlacementResponse> {
    return this.monoPalletProductPlacementUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as ProductPlacementResponse)
    );
  }

  /**
   * Запрос в МРТ на обычное (не паллетное) размещение товара
   *
   * operationId, productId, productBatchId - обязательные поля
   * @param request request
   * @return OK
   */
  productPlacementUsingPOSTResponse(request: ProductPlacementRequest): __Observable<__StrictHttpResponse<ProductPlacementResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/allocations/productPlacement`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductPlacementResponse>;
      })
    );
  }
  /**
   * Запрос в МРТ на обычное (не паллетное) размещение товара
   *
   * operationId, productId, productBatchId - обязательные поля
   * @param request request
   * @return OK
   */
  productPlacementUsingPOST(request: ProductPlacementRequest): __Observable<ProductPlacementResponse> {
    return this.productPlacementUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as ProductPlacementResponse)
    );
  }

  /**
   * Запрос в МРТ на размещение сырья
   *
   * operationId, productId, productBatchId - обязательные поля
   * @param request request
   * @return OK
   */
  rawPlacementUsingPOSTResponse(request: RawPlacementRequest): __Observable<__StrictHttpResponse<ProductPlacementResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/allocations/rawPlacement`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductPlacementResponse>;
      })
    );
  }
  /**
   * Запрос в МРТ на размещение сырья
   *
   * operationId, productId, productBatchId - обязательные поля
   * @param request request
   * @return OK
   */
  rawPlacementUsingPOST(request: RawPlacementRequest): __Observable<ProductPlacementResponse> {
    return this.rawPlacementUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as ProductPlacementResponse)
    );
  }
}

module AllocationGatewayService {
}

export { AllocationGatewayService }
