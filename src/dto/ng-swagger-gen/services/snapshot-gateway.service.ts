/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';


/**
 * Snapshot Gateway
 */
@Injectable({
  providedIn: 'root',
})
class SnapshotGatewayService extends __BaseService {
  static readonly triggerSnapshotUsingPOSTPath = '/v1/snapshot/{entityId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * triggerSnapshot
   * @param params The `SnapshotGatewayService.TriggerSnapshotUsingPOSTParams` containing the following parameters:
   *
   * - `type`: type
   *
   * - `entityId`: entityId
   *
   * - `entity`: entity
   */
  triggerSnapshotUsingPOSTResponse(params: SnapshotGatewayService.TriggerSnapshotUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.type != null) __params = __params.set('type', params.type.toString());

    if (params.entity != null) __params = __params.set('entity', params.entity.toString());
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/snapshot/${encodeURIComponent(String(params.entityId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * triggerSnapshot
   * @param params The `SnapshotGatewayService.TriggerSnapshotUsingPOSTParams` containing the following parameters:
   *
   * - `type`: type
   *
   * - `entityId`: entityId
   *
   * - `entity`: entity
   */
  triggerSnapshotUsingPOST(params: SnapshotGatewayService.TriggerSnapshotUsingPOSTParams): __Observable<null> {
    return this.triggerSnapshotUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module SnapshotGatewayService {

  /**
   * Parameters for triggerSnapshotUsingPOST
   */
  export interface TriggerSnapshotUsingPOSTParams {

    /**
     * type
     */
    type: 'DELETE' | 'SNAPSHOT';

    /**
     * entityId
     */
    entityId: string;

    /**
     * entity
     */
    entity: 'PRODUCT_BATCH' | 'STOCK' | 'PLACE' | 'PLACE_TYPE';
  }
}

export { SnapshotGatewayService }
