/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceReservationSearchResponse } from '../models/place-reservation-search-response';
import { PlaceReservationSearchRequest } from '../models/place-reservation-search-request';

/**
 * Place Reservation Search Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceReservationSearchGatewayService extends __BaseService {
  static readonly searchUsingPOST1Path = '/v1/places/reservation/search';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Поиск носителей, резевов под носители и резервов мест под процесс в определенных местах
   * @param placeReservationSearchRequest placeReservationSearchRequest
   * @return OK
   */
  searchUsingPOST1Response(placeReservationSearchRequest: PlaceReservationSearchRequest): __Observable<__StrictHttpResponse<PlaceReservationSearchResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = placeReservationSearchRequest;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/places/reservation/search`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceReservationSearchResponse>;
      })
    );
  }
  /**
   * Поиск носителей, резевов под носители и резервов мест под процесс в определенных местах
   * @param placeReservationSearchRequest placeReservationSearchRequest
   * @return OK
   */
  searchUsingPOST1(placeReservationSearchRequest: PlaceReservationSearchRequest): __Observable<PlaceReservationSearchResponse> {
    return this.searchUsingPOST1Response(placeReservationSearchRequest).pipe(
      __map(_r => _r.body as PlaceReservationSearchResponse)
    );
  }
}

module PlaceReservationSearchGatewayService {
}

export { PlaceReservationSearchGatewayService }
