/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceDto } from '../models/place-dto';
import { CreatePlaceCmd } from '../models/create-place-cmd';
import { PlaceNodeWithPosition } from '../models/place-node-with-position';
import { PlaceHierarchyTreeDto } from '../models/place-hierarchy-tree-dto';
import { PlaceShortView } from '../models/place-short-view';
import { UpdatePlaceStatusResponse } from '../models/update-place-status-response';
import { UpdatePlaceStatusCmd } from '../models/update-place-status-cmd';
import { PlaceWithPosition } from '../models/place-with-position';
import { UpdatePlaceCmd } from '../models/update-place-cmd';

/**
 * Place Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceGatewayService extends __BaseService {
  static readonly getPlaceUsingGETPath = '/v1/topology/places';
  static readonly createUsingPOST2Path = '/v1/topology/places';
  static readonly findPlacesByProcessIdUsingGETPath = '/v1/topology/places/findByProcessId';
  static readonly getHierarchyUsingGETPath = '/v1/topology/places/hierarchy/{siteId}';
  static readonly getHierarchyForParentIdUsingGETPath = '/v1/topology/places/hierarchy/{siteId}/{parentId}';
  static readonly getHierarchyByTypeCodeUsingGETPath = '/v1/topology/places/hierarchyTree/{siteId}';
  static readonly getPlaceByCodeUsingGETPath = '/v1/topology/places/shortView';
  static readonly getTransferPlaceUsingGETPath = '/v1/topology/places/transfer/{siteId}';
  static readonly updatePlaceStatusUsingPUTPath = '/v1/topology/places/updatePlaceStatus';
  static readonly getPlaceByIdUsingGETPath = '/v1/topology/places/{id}';
  static readonly updateUsingPUT1Path = '/v1/topology/places/{id}';
  static readonly deleteUsingDELETE1Path = '/v1/topology/places/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getPlace
   * @param params The `PlaceGatewayService.GetPlaceUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `showChildren`: showChildren
   *
   * - `placeId`: placeId
   *
   * - `barcode`: barcode
   *
   * @return OK
   */
  getPlaceUsingGETResponse(params: PlaceGatewayService.GetPlaceUsingGETParams): __Observable<__StrictHttpResponse<PlaceDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    if (params.showChildren != null) __params = __params.set('showChildren', params.showChildren.toString());
    if (params.placeId != null) __params = __params.set('placeId', params.placeId.toString());
    if (params.barcode != null) __params = __params.set('barcode', params.barcode.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceDto>;
      })
    );
  }
  /**
   * getPlace
   * @param params The `PlaceGatewayService.GetPlaceUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `showChildren`: showChildren
   *
   * - `placeId`: placeId
   *
   * - `barcode`: barcode
   *
   * @return OK
   */
  getPlaceUsingGET(params: PlaceGatewayService.GetPlaceUsingGETParams): __Observable<PlaceDto> {
    return this.getPlaceUsingGETResponse(params).pipe(
      __map(_r => _r.body as PlaceDto)
    );
  }

  /**
   * create
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST2Response(cmd: CreatePlaceCmd): __Observable<__StrictHttpResponse<PlaceDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceDto>;
      })
    );
  }
  /**
   * create
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST2(cmd: CreatePlaceCmd): __Observable<PlaceDto> {
    return this.createUsingPOST2Response(cmd).pipe(
      __map(_r => _r.body as PlaceDto)
    );
  }

  /**
   * findPlacesByProcessId
   * @param params The `PlaceGatewayService.FindPlacesByProcessIdUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `processId`: processId
   *
   * - `placeTypeCode`: placeTypeCode
   *
   * - `showChildren`: showChildren
   *
   * @return OK
   */
  findPlacesByProcessIdUsingGETResponse(params: PlaceGatewayService.FindPlacesByProcessIdUsingGETParams): __Observable<__StrictHttpResponse<Array<PlaceDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    if (params.processId != null) __params = __params.set('processId', params.processId.toString());
    if (params.placeTypeCode != null) __params = __params.set('placeTypeCode', params.placeTypeCode.toString());
    if (params.showChildren != null) __params = __params.set('showChildren', params.showChildren.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/findByProcessId`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceDto>>;
      })
    );
  }
  /**
   * findPlacesByProcessId
   * @param params The `PlaceGatewayService.FindPlacesByProcessIdUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `processId`: processId
   *
   * - `placeTypeCode`: placeTypeCode
   *
   * - `showChildren`: showChildren
   *
   * @return OK
   */
  findPlacesByProcessIdUsingGET(params: PlaceGatewayService.FindPlacesByProcessIdUsingGETParams): __Observable<Array<PlaceDto>> {
    return this.findPlacesByProcessIdUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<PlaceDto>)
    );
  }

  /**
   * getHierarchy
   * @param siteId siteId
   * @return OK
   */
  getHierarchyUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<PlaceNodeWithPosition>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/hierarchy/${encodeURIComponent(String(siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceNodeWithPosition>;
      })
    );
  }
  /**
   * getHierarchy
   * @param siteId siteId
   * @return OK
   */
  getHierarchyUsingGET(siteId: string): __Observable<PlaceNodeWithPosition> {
    return this.getHierarchyUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as PlaceNodeWithPosition)
    );
  }

  /**
   * getHierarchyForParentId
   * @param params The `PlaceGatewayService.GetHierarchyForParentIdUsingGETParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `parentId`: parentId
   *
   * @return OK
   */
  getHierarchyForParentIdUsingGETResponse(params: PlaceGatewayService.GetHierarchyForParentIdUsingGETParams): __Observable<__StrictHttpResponse<PlaceNodeWithPosition>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/hierarchy/${encodeURIComponent(String(params.siteId))}/${encodeURIComponent(String(params.parentId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceNodeWithPosition>;
      })
    );
  }
  /**
   * getHierarchyForParentId
   * @param params The `PlaceGatewayService.GetHierarchyForParentIdUsingGETParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `parentId`: parentId
   *
   * @return OK
   */
  getHierarchyForParentIdUsingGET(params: PlaceGatewayService.GetHierarchyForParentIdUsingGETParams): __Observable<PlaceNodeWithPosition> {
    return this.getHierarchyForParentIdUsingGETResponse(params).pipe(
      __map(_r => _r.body as PlaceNodeWithPosition)
    );
  }

  /**
   * getHierarchyByTypeCode
   * @param params The `PlaceGatewayService.GetHierarchyByTypeCodeUsingGETParams` containing the following parameters:
   *
   * - `typeCode`: typeCode
   *
   * - `siteId`: siteId
   *
   * @return OK
   */
  getHierarchyByTypeCodeUsingGETResponse(params: PlaceGatewayService.GetHierarchyByTypeCodeUsingGETParams): __Observable<__StrictHttpResponse<Array<PlaceHierarchyTreeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.typeCode != null) __params = __params.set('typeCode', params.typeCode.toString());

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/hierarchyTree/${encodeURIComponent(String(params.siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceHierarchyTreeDto>>;
      })
    );
  }
  /**
   * getHierarchyByTypeCode
   * @param params The `PlaceGatewayService.GetHierarchyByTypeCodeUsingGETParams` containing the following parameters:
   *
   * - `typeCode`: typeCode
   *
   * - `siteId`: siteId
   *
   * @return OK
   */
  getHierarchyByTypeCodeUsingGET(params: PlaceGatewayService.GetHierarchyByTypeCodeUsingGETParams): __Observable<Array<PlaceHierarchyTreeDto>> {
    return this.getHierarchyByTypeCodeUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<PlaceHierarchyTreeDto>)
    );
  }

  /**
   * getPlaceByCode
   * @param params The `PlaceGatewayService.GetPlaceByCodeUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `placeBarcode`: placeBarcode
   *
   * @return OK
   */
  getPlaceByCodeUsingGETResponse(params: PlaceGatewayService.GetPlaceByCodeUsingGETParams): __Observable<__StrictHttpResponse<Array<PlaceShortView>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    if (params.placeBarcode != null) __params = __params.set('placeBarcode', params.placeBarcode.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/shortView`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceShortView>>;
      })
    );
  }
  /**
   * getPlaceByCode
   * @param params The `PlaceGatewayService.GetPlaceByCodeUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `placeBarcode`: placeBarcode
   *
   * @return OK
   */
  getPlaceByCodeUsingGET(params: PlaceGatewayService.GetPlaceByCodeUsingGETParams): __Observable<Array<PlaceShortView>> {
    return this.getPlaceByCodeUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<PlaceShortView>)
    );
  }

  /**
   * getTransferPlace
   * @param siteId siteId
   * @return OK
   */
  getTransferPlaceUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<PlaceShortView>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/transfer/${encodeURIComponent(String(siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceShortView>;
      })
    );
  }
  /**
   * getTransferPlace
   * @param siteId siteId
   * @return OK
   */
  getTransferPlaceUsingGET(siteId: string): __Observable<PlaceShortView> {
    return this.getTransferPlaceUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as PlaceShortView)
    );
  }

  /**
   * Изменение статуса места с удалением входящих резервов
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  updatePlaceStatusUsingPUTResponse(cmd: UpdatePlaceStatusCmd): __Observable<__StrictHttpResponse<UpdatePlaceStatusResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/places/updatePlaceStatus`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UpdatePlaceStatusResponse>;
      })
    );
  }
  /**
   * Изменение статуса места с удалением входящих резервов
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  updatePlaceStatusUsingPUT(cmd: UpdatePlaceStatusCmd): __Observable<UpdatePlaceStatusResponse> {
    return this.updatePlaceStatusUsingPUTResponse(cmd).pipe(
      __map(_r => _r.body as UpdatePlaceStatusResponse)
    );
  }

  /**
   * getPlaceById
   * @param id id
   * @return OK
   */
  getPlaceByIdUsingGETResponse(id: string): __Observable<__StrictHttpResponse<PlaceWithPosition>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceWithPosition>;
      })
    );
  }
  /**
   * getPlaceById
   * @param id id
   * @return OK
   */
  getPlaceByIdUsingGET(id: string): __Observable<PlaceWithPosition> {
    return this.getPlaceByIdUsingGETResponse(id).pipe(
      __map(_r => _r.body as PlaceWithPosition)
    );
  }

  /**
   * update
   * @param params The `PlaceGatewayService.UpdateUsingPUT1Params` containing the following parameters:
   *
   * - `id`: id
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateUsingPUT1Response(params: PlaceGatewayService.UpdateUsingPUT1Params): __Observable<__StrictHttpResponse<PlaceDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/places/${encodeURIComponent(String(params.id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceDto>;
      })
    );
  }
  /**
   * update
   * @param params The `PlaceGatewayService.UpdateUsingPUT1Params` containing the following parameters:
   *
   * - `id`: id
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateUsingPUT1(params: PlaceGatewayService.UpdateUsingPUT1Params): __Observable<PlaceDto> {
    return this.updateUsingPUT1Response(params).pipe(
      __map(_r => _r.body as PlaceDto)
    );
  }

  /**
   * delete
   * @param id id
   */
  deleteUsingDELETE1Response(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/topology/places/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * delete
   * @param id id
   */
  deleteUsingDELETE1(id: string): __Observable<null> {
    return this.deleteUsingDELETE1Response(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module PlaceGatewayService {

  /**
   * Parameters for getPlaceUsingGET
   */
  export interface GetPlaceUsingGETParams {

    /**
     * siteCode
     */
    siteCode?: string;

    /**
     * showChildren
     */
    showChildren?: boolean;

    /**
     * placeId
     */
    placeId?: string;

    /**
     * barcode
     */
    barcode?: string;
  }

  /**
   * Parameters for findPlacesByProcessIdUsingGET
   */
  export interface FindPlacesByProcessIdUsingGETParams {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * processId
     */
    processId: string;

    /**
     * placeTypeCode
     */
    placeTypeCode: string;

    /**
     * showChildren
     */
    showChildren?: boolean;
  }

  /**
   * Parameters for getHierarchyForParentIdUsingGET
   */
  export interface GetHierarchyForParentIdUsingGETParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * parentId
     */
    parentId: string;
  }

  /**
   * Parameters for getHierarchyByTypeCodeUsingGET
   */
  export interface GetHierarchyByTypeCodeUsingGETParams {

    /**
     * typeCode
     */
    typeCode: string;

    /**
     * siteId
     */
    siteId: string;
  }

  /**
   * Parameters for getPlaceByCodeUsingGET
   */
  export interface GetPlaceByCodeUsingGETParams {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * placeBarcode
     */
    placeBarcode: string;
  }

  /**
   * Parameters for updateUsingPUT1
   */
  export interface UpdateUsingPUT1Params {

    /**
     * id
     */
    id: string;

    /**
     * cmd
     */
    cmd: UpdatePlaceCmd;
  }
}

export { PlaceGatewayService }
