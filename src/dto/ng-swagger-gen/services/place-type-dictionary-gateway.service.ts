/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceTypeDto } from '../models/place-type-dto';

/**
 * Place Type Dictionary Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceTypeDictionaryGatewayService extends __BaseService {
  static readonly placeTypesUsingGETPath = '/v1/topology/dictionary/sites/{siteId}/placeTypes';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * placeTypes
   * @param siteId siteId
   * @return OK
   */
  placeTypesUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<Array<PlaceTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/dictionary/sites/${encodeURIComponent(String(siteId))}/placeTypes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceTypeDto>>;
      })
    );
  }
  /**
   * placeTypes
   * @param siteId siteId
   * @return OK
   */
  placeTypesUsingGET(siteId: string): __Observable<Array<PlaceTypeDto>> {
    return this.placeTypesUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as Array<PlaceTypeDto>)
    );
  }
}

module PlaceTypeDictionaryGatewayService {
}

export { PlaceTypeDictionaryGatewayService }
