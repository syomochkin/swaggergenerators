/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlacesSearchResult } from '../models/places-search-result';
import { PlacesSearchQuery } from '../models/places-search-query';
import { DetailedPlaceSearchResult } from '../models/detailed-place-search-result';
import { DetailedPlaceSearchQuery } from '../models/detailed-place-search-query';
import { PlaceTypeDto } from '../models/place-type-dto';
import { ZoneTypeWithZones } from '../models/zone-type-with-zones';

/**
 * Place Search Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceSearchGatewayService extends __BaseService {
  static readonly searchUsingPOST2Path = '/v1/topology/places/search';
  static readonly searchDetailedUsingPOSTPath = '/v1/topology/places/search/detailed';
  static readonly getPlaceTypesBySiteUsingGETPath = '/v1/topology/places/search/dicts/placeTypes/{siteId}';
  static readonly getZoneTypesWithZonesUsingGETPath = '/v1/topology/places/search/dicts/zoneTypes/{siteId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Search Places
   * @param query query
   * @return OK
   */
  searchUsingPOST2Response(query: PlacesSearchQuery): __Observable<__StrictHttpResponse<PlacesSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places/search`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlacesSearchResult>;
      })
    );
  }
  /**
   * Search Places
   * @param query query
   * @return OK
   */
  searchUsingPOST2(query: PlacesSearchQuery): __Observable<PlacesSearchResult> {
    return this.searchUsingPOST2Response(query).pipe(
      __map(_r => _r.body as PlacesSearchResult)
    );
  }

  /**
   * Универсальный поиск мест с иерархией
   * @param searchQuery searchQuery
   * @return OK
   */
  searchDetailedUsingPOSTResponse(searchQuery: DetailedPlaceSearchQuery): __Observable<__StrictHttpResponse<DetailedPlaceSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = searchQuery;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places/search/detailed`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DetailedPlaceSearchResult>;
      })
    );
  }
  /**
   * Универсальный поиск мест с иерархией
   * @param searchQuery searchQuery
   * @return OK
   */
  searchDetailedUsingPOST(searchQuery: DetailedPlaceSearchQuery): __Observable<DetailedPlaceSearchResult> {
    return this.searchDetailedUsingPOSTResponse(searchQuery).pipe(
      __map(_r => _r.body as DetailedPlaceSearchResult)
    );
  }

  /**
   * getPlaceTypesBySite
   * @param siteId siteId
   * @return OK
   */
  getPlaceTypesBySiteUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<Array<PlaceTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/search/dicts/placeTypes/${encodeURIComponent(String(siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceTypeDto>>;
      })
    );
  }
  /**
   * getPlaceTypesBySite
   * @param siteId siteId
   * @return OK
   */
  getPlaceTypesBySiteUsingGET(siteId: string): __Observable<Array<PlaceTypeDto>> {
    return this.getPlaceTypesBySiteUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as Array<PlaceTypeDto>)
    );
  }

  /**
   * getZoneTypesWithZones
   * @param siteId siteId
   * @return OK
   */
  getZoneTypesWithZonesUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<Array<ZoneTypeWithZones>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/search/dicts/zoneTypes/${encodeURIComponent(String(siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneTypeWithZones>>;
      })
    );
  }
  /**
   * getZoneTypesWithZones
   * @param siteId siteId
   * @return OK
   */
  getZoneTypesWithZonesUsingGET(siteId: string): __Observable<Array<ZoneTypeWithZones>> {
    return this.getZoneTypesWithZonesUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as Array<ZoneTypeWithZones>)
    );
  }
}

module PlaceSearchGatewayService {
}

export { PlaceSearchGatewayService }
