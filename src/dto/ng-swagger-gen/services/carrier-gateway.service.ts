/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CarrierDto } from '../models/carrier-dto';
import { CreateCarrierCmd } from '../models/create-carrier-cmd';
import { DetailedCarriersSearchQuery } from '../models/detailed-carriers-search-query';
import { CarrierShippedResponse } from '../models/carrier-shipped-response';
import { ShipCarrierCmd } from '../models/ship-carrier-cmd';

/**
 * Carrier Gateway
 */
@Injectable({
  providedIn: 'root',
})
class CarrierGatewayService extends __BaseService {
  static readonly getAllUsingGETPath = '/v1/stock/carriers';
  static readonly createUsingPOSTPath = '/v1/stock/carriers';
  static readonly getAllUsingPOSTPath = '/v1/stock/carriers/find';
  static readonly shipUsingPOSTPath = '/v1/stock/carriers/ship';
  static readonly moveCarrierIntoCarrierUsingPUTPath = '/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}';
  static readonly getByIdUsingGETPath = '/v1/stock/carriers/{id}';
  static readonly deleteUsingDELETEPath = '/v1/stock/carriers/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getAll
   * @param params The `CarrierGatewayService.GetAllUsingGETParams` containing the following parameters:
   *
   * - `rootOnly`: rootOnly
   *
   * - `placeId`: placeId
   *
   * - `barcode`: barcode
   *
   * @return OK
   */
  getAllUsingGETResponse(params: CarrierGatewayService.GetAllUsingGETParams): __Observable<__StrictHttpResponse<Array<CarrierDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.rootOnly != null) __params = __params.set('rootOnly', params.rootOnly.toString());
    if (params.placeId != null) __params = __params.set('placeId', params.placeId.toString());
    if (params.barcode != null) __params = __params.set('barcode', params.barcode.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/carriers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CarrierDto>>;
      })
    );
  }
  /**
   * getAll
   * @param params The `CarrierGatewayService.GetAllUsingGETParams` containing the following parameters:
   *
   * - `rootOnly`: rootOnly
   *
   * - `placeId`: placeId
   *
   * - `barcode`: barcode
   *
   * @return OK
   */
  getAllUsingGET(params: CarrierGatewayService.GetAllUsingGETParams): __Observable<Array<CarrierDto>> {
    return this.getAllUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<CarrierDto>)
    );
  }

  /**
   * create a Carrier
   *
   * Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
   * @param cmd cmd
   * @return OK
   */
  createUsingPOSTResponse(cmd: CreateCarrierCmd): __Observable<__StrictHttpResponse<CarrierDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carriers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierDto>;
      })
    );
  }
  /**
   * create a Carrier
   *
   * Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST(cmd: CreateCarrierCmd): __Observable<CarrierDto> {
    return this.createUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as CarrierDto)
    );
  }

  /**
   * getAll
   * @param detailedCarriersSearchQuery detailedCarriersSearchQuery
   * @return OK
   */
  getAllUsingPOSTResponse(detailedCarriersSearchQuery: DetailedCarriersSearchQuery): __Observable<__StrictHttpResponse<Array<CarrierDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = detailedCarriersSearchQuery;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carriers/find`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CarrierDto>>;
      })
    );
  }
  /**
   * getAll
   * @param detailedCarriersSearchQuery detailedCarriersSearchQuery
   * @return OK
   */
  getAllUsingPOST(detailedCarriersSearchQuery: DetailedCarriersSearchQuery): __Observable<Array<CarrierDto>> {
    return this.getAllUsingPOSTResponse(detailedCarriersSearchQuery).pipe(
      __map(_r => _r.body as Array<CarrierDto>)
    );
  }

  /**
   * Отгрузка носителя
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  shipUsingPOSTResponse(cmd: ShipCarrierCmd): __Observable<__StrictHttpResponse<CarrierShippedResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carriers/ship`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierShippedResponse>;
      })
    );
  }
  /**
   * Отгрузка носителя
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  shipUsingPOST(cmd: ShipCarrierCmd): __Observable<CarrierShippedResponse> {
    return this.shipUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as CarrierShippedResponse)
    );
  }

  /**
   * moveCarrierIntoCarrier
   * @param params The `CarrierGatewayService.MoveCarrierIntoCarrierUsingPUTParams` containing the following parameters:
   *
   * - `parentCarrierId`: parentCarrierId
   *
   * - `carrierId`: carrierId
   *
   * - `processType`: processType
   *
   * - `processId`: processId
   *
   * - `parentProcessId`: parentProcessId
   */
  moveCarrierIntoCarrierUsingPUTResponse(params: CarrierGatewayService.MoveCarrierIntoCarrierUsingPUTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (params.processType != null) __params = __params.set('processType', params.processType.toString());
    if (params.processId != null) __params = __params.set('processId', params.processId.toString());
    if (params.parentProcessId != null) __params = __params.set('parentProcessId', params.parentProcessId.toString());
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/carriers/${encodeURIComponent(String(params.carrierId))}/moveInto/${encodeURIComponent(String(params.parentCarrierId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * moveCarrierIntoCarrier
   * @param params The `CarrierGatewayService.MoveCarrierIntoCarrierUsingPUTParams` containing the following parameters:
   *
   * - `parentCarrierId`: parentCarrierId
   *
   * - `carrierId`: carrierId
   *
   * - `processType`: processType
   *
   * - `processId`: processId
   *
   * - `parentProcessId`: parentProcessId
   */
  moveCarrierIntoCarrierUsingPUT(params: CarrierGatewayService.MoveCarrierIntoCarrierUsingPUTParams): __Observable<null> {
    return this.moveCarrierIntoCarrierUsingPUTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getById
   * @param id id
   * @return OK
   */
  getByIdUsingGETResponse(id: string): __Observable<__StrictHttpResponse<CarrierDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/carriers/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierDto>;
      })
    );
  }
  /**
   * getById
   * @param id id
   * @return OK
   */
  getByIdUsingGET(id: string): __Observable<CarrierDto> {
    return this.getByIdUsingGETResponse(id).pipe(
      __map(_r => _r.body as CarrierDto)
    );
  }

  /**
   * delete a Carrier
   *
   * Delete a Carrier. CarrierId are required.
   * @param params The `CarrierGatewayService.DeleteUsingDELETEParams` containing the following parameters:
   *
   * - `id`: id
   *
   * - `processType`: processType
   *
   * - `processId`: processId
   */
  deleteUsingDELETEResponse(params: CarrierGatewayService.DeleteUsingDELETEParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.processType != null) __params = __params.set('processType', params.processType.toString());
    if (params.processId != null) __params = __params.set('processId', params.processId.toString());
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/carriers/${encodeURIComponent(String(params.id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * delete a Carrier
   *
   * Delete a Carrier. CarrierId are required.
   * @param params The `CarrierGatewayService.DeleteUsingDELETEParams` containing the following parameters:
   *
   * - `id`: id
   *
   * - `processType`: processType
   *
   * - `processId`: processId
   */
  deleteUsingDELETE(params: CarrierGatewayService.DeleteUsingDELETEParams): __Observable<null> {
    return this.deleteUsingDELETEResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module CarrierGatewayService {

  /**
   * Parameters for getAllUsingGET
   */
  export interface GetAllUsingGETParams {

    /**
     * rootOnly
     */
    rootOnly?: boolean;

    /**
     * placeId
     */
    placeId?: string;

    /**
     * barcode
     */
    barcode?: string;
  }

  /**
   * Parameters for moveCarrierIntoCarrierUsingPUT
   */
  export interface MoveCarrierIntoCarrierUsingPUTParams {

    /**
     * parentCarrierId
     */
    parentCarrierId: string;

    /**
     * carrierId
     */
    carrierId: string;

    /**
     * processType
     */
    processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';

    /**
     * processId
     */
    processId?: string;

    /**
     * parentProcessId
     */
    parentProcessId?: string;
  }

  /**
   * Parameters for deleteUsingDELETE
   */
  export interface DeleteUsingDELETEParams {

    /**
     * id
     */
    id: string;

    /**
     * processType
     */
    processType?: 'INBOUND' | 'REPLENISHMENT' | 'MANUFACTURING' | 'OUTBOUND' | 'PICKING' | 'CONSOLIDATION' | 'CLIENT_RETURNS' | 'INVENTORY' | 'UI' | 'TSD_MANUAL_MOVEMENT' | 'WMS_SUPPORT' | 'NOT_SPECIFIED';

    /**
     * processId
     */
    processId?: string;
  }
}

export { CarrierGatewayService }
