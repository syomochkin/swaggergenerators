/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ReservedPlacesForCarriersResponse } from '../models/reserved-places-for-carriers-response';
import { ReservePlacesForCarriersCmd } from '../models/reserve-places-for-carriers-cmd';
import { DropPlaceReservationsForTransferCmd } from '../models/drop-place-reservations-for-transfer-cmd';
import { ReservedChutePlacesForCarriersResponse } from '../models/reserved-chute-places-for-carriers-response';
import { ReserveChutePlacesForCarriersCmd } from '../models/reserve-chute-places-for-carriers-cmd';
import { ExchangeChuteReservationResponse } from '../models/exchange-chute-reservation-response';
import { ExchangeChuteReservationCmd } from '../models/exchange-chute-reservation-cmd';
import { ReservedPlacesForCarriersByClusterDeliveryResponse } from '../models/reserved-places-for-carriers-by-cluster-delivery-response';
import { ReservePlacesForCarriersByClusterDeliveryCmd } from '../models/reserve-places-for-carriers-by-cluster-delivery-cmd';
import { ReservedPlacesForCarriersByMultiProcessesResponse } from '../models/reserved-places-for-carriers-by-multi-processes-response';
import { ReservePlacesForCarriersByMultyProcessesCmd } from '../models/reserve-places-for-carriers-by-multy-processes-cmd';
import { ReducePlaceReservationForTransferCmd } from '../models/reduce-place-reservation-for-transfer-cmd';
import { ReservePlaceForProcessCmd } from '../models/reserve-place-for-process-cmd';
import { DropAllProcessReservationsCmd } from '../models/drop-all-process-reservations-cmd';
import { DropPlaceReservationForProcessCmd } from '../models/drop-place-reservation-for-process-cmd';

/**
 * Reservation Place Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ReservationPlaceGatewayService extends __BaseService {
  static readonly reserveUsingPOST1Path = '/v1/stock/places/reservation/for-carriers';
  static readonly dropReservationUsingDELETEPath = '/v1/stock/places/reservation/for-carriers';
  static readonly reserveUsingPOSTPath = '/v1/stock/places/reservation/for-carriers/chute';
  static readonly exchangeChuteReservationUsingPUTPath = '/v1/stock/places/reservation/for-carriers/chute/exchange';
  static readonly reserveByClusterDeliveryUsingPOSTPath = '/v1/stock/places/reservation/for-carriers/cluster-delivery';
  static readonly reserveMultiProcessUsingPOSTPath = '/v1/stock/places/reservation/for-carriers/multi-process';
  static readonly reduceReservationForTransferUsingPUTPath = '/v1/stock/places/reservation/for-carriers/reduce';
  static readonly reserveForProcessUsingPOST1Path = '/v1/stock/places/reservation/for-process';
  static readonly dropAllReservationsForProcessUsingDELETEPath = '/v1/stock/places/reservation/for-process/all-places';
  static readonly dropReservationForProcessUsingDELETE1Path = '/v1/stock/places/reservation/for-process/one-place';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Резервирование мест под носители
   * @param cmd cmd
   * @return OK
   */
  reserveUsingPOST1Response(cmd: ReservePlacesForCarriersCmd): __Observable<__StrictHttpResponse<ReservedPlacesForCarriersResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReservedPlacesForCarriersResponse>;
      })
    );
  }
  /**
   * Резервирование мест под носители
   * @param cmd cmd
   * @return OK
   */
  reserveUsingPOST1(cmd: ReservePlacesForCarriersCmd): __Observable<ReservedPlacesForCarriersResponse> {
    return this.reserveUsingPOST1Response(cmd).pipe(
      __map(_r => _r.body as ReservedPlacesForCarriersResponse)
    );
  }

  /**
   * Сброс резерва места под носитель
   * @param cmd cmd
   */
  dropReservationUsingDELETEResponse(cmd: DropPlaceReservationsForTransferCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Сброс резерва места под носитель
   * @param cmd cmd
   */
  dropReservationUsingDELETE(cmd: DropPlaceReservationsForTransferCmd): __Observable<null> {
    return this.dropReservationUsingDELETEResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Резервирование мест в шутах сортера
   * @param cmd cmd
   * @return OK
   */
  reserveUsingPOSTResponse(cmd: ReserveChutePlacesForCarriersCmd): __Observable<__StrictHttpResponse<ReservedChutePlacesForCarriersResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers/chute`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReservedChutePlacesForCarriersResponse>;
      })
    );
  }
  /**
   * Резервирование мест в шутах сортера
   * @param cmd cmd
   * @return OK
   */
  reserveUsingPOST(cmd: ReserveChutePlacesForCarriersCmd): __Observable<ReservedChutePlacesForCarriersResponse> {
    return this.reserveUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ReservedChutePlacesForCarriersResponse)
    );
  }

  /**
   * Замена резерва под носители в месте с sourceTransferId на targetTransferId
   *
   * Сделан для оптимального использования шутов сортера
   * @param cmd cmd
   * @return OK
   */
  exchangeChuteReservationUsingPUTResponse(cmd: ExchangeChuteReservationCmd): __Observable<__StrictHttpResponse<ExchangeChuteReservationResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers/chute/exchange`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ExchangeChuteReservationResponse>;
      })
    );
  }
  /**
   * Замена резерва под носители в месте с sourceTransferId на targetTransferId
   *
   * Сделан для оптимального использования шутов сортера
   * @param cmd cmd
   * @return OK
   */
  exchangeChuteReservationUsingPUT(cmd: ExchangeChuteReservationCmd): __Observable<ExchangeChuteReservationResponse> {
    return this.exchangeChuteReservationUsingPUTResponse(cmd).pipe(
      __map(_r => _r.body as ExchangeChuteReservationResponse)
    );
  }

  /**
   * Резервирование мест под носители для кластерной доставки
   * @param cmd cmd
   * @return OK
   */
  reserveByClusterDeliveryUsingPOSTResponse(cmd: ReservePlacesForCarriersByClusterDeliveryCmd): __Observable<__StrictHttpResponse<ReservedPlacesForCarriersByClusterDeliveryResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers/cluster-delivery`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReservedPlacesForCarriersByClusterDeliveryResponse>;
      })
    );
  }
  /**
   * Резервирование мест под носители для кластерной доставки
   * @param cmd cmd
   * @return OK
   */
  reserveByClusterDeliveryUsingPOST(cmd: ReservePlacesForCarriersByClusterDeliveryCmd): __Observable<ReservedPlacesForCarriersByClusterDeliveryResponse> {
    return this.reserveByClusterDeliveryUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ReservedPlacesForCarriersByClusterDeliveryResponse)
    );
  }

  /**
   * Резервирование мест под носители для нескольких процессов
   * @param cmd cmd
   * @return OK
   */
  reserveMultiProcessUsingPOSTResponse(cmd: ReservePlacesForCarriersByMultyProcessesCmd): __Observable<__StrictHttpResponse<ReservedPlacesForCarriersByMultiProcessesResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers/multi-process`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReservedPlacesForCarriersByMultiProcessesResponse>;
      })
    );
  }
  /**
   * Резервирование мест под носители для нескольких процессов
   * @param cmd cmd
   * @return OK
   */
  reserveMultiProcessUsingPOST(cmd: ReservePlacesForCarriersByMultyProcessesCmd): __Observable<ReservedPlacesForCarriersByMultiProcessesResponse> {
    return this.reserveMultiProcessUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ReservedPlacesForCarriersByMultiProcessesResponse)
    );
  }

  /**
   * Уменьшить резерв под носители в месте
   * @param cmd cmd
   */
  reduceReservationForTransferUsingPUTResponse(cmd: ReducePlaceReservationForTransferCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/places/reservation/for-carriers/reduce`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Уменьшить резерв под носители в месте
   * @param cmd cmd
   */
  reduceReservationForTransferUsingPUT(cmd: ReducePlaceReservationForTransferCmd): __Observable<null> {
    return this.reduceReservationForTransferUsingPUTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Резервирование места под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  reserveForProcessUsingPOST1Response(cmd: ReservePlaceForProcessCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reservation/for-process`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Резервирование места под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  reserveForProcessUsingPOST1(cmd: ReservePlaceForProcessCmd): __Observable<null> {
    return this.reserveForProcessUsingPOST1Response(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Сброс резервов всех мест под конкретный процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  dropAllReservationsForProcessUsingDELETEResponse(cmd: DropAllProcessReservationsCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/places/reservation/for-process/all-places`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Сброс резервов всех мест под конкретный процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  dropAllReservationsForProcessUsingDELETE(cmd: DropAllProcessReservationsCmd): __Observable<null> {
    return this.dropAllReservationsForProcessUsingDELETEResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Сброс резерва конкретного места под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  dropReservationForProcessUsingDELETE1Response(cmd: DropPlaceReservationForProcessCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/places/reservation/for-process/one-place`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Сброс резерва конкретного места под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  dropReservationForProcessUsingDELETE1(cmd: DropPlaceReservationForProcessCmd): __Observable<null> {
    return this.dropReservationForProcessUsingDELETE1Response(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module ReservationPlaceGatewayService {
}

export { ReservationPlaceGatewayService }
