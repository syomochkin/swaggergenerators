/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ZoneDto } from '../models/zone-dto';

/**
 * Zone Dictionary Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ZoneDictionaryGatewayService extends __BaseService {
  static readonly getZoneTypesUsingGETPath = '/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getZoneTypes
   * @param params The `ZoneDictionaryGatewayService.GetZoneTypesUsingGETParams` containing the following parameters:
   *
   * - `zoneTypeId`: zoneTypeId
   *
   * - `siteId`: siteId
   *
   * @return OK
   */
  getZoneTypesUsingGETResponse(params: ZoneDictionaryGatewayService.GetZoneTypesUsingGETParams): __Observable<__StrictHttpResponse<Array<ZoneDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/dictionary/site/${encodeURIComponent(String(params.siteId))}/zoneType/${encodeURIComponent(String(params.zoneTypeId))}/zones`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneDto>>;
      })
    );
  }
  /**
   * getZoneTypes
   * @param params The `ZoneDictionaryGatewayService.GetZoneTypesUsingGETParams` containing the following parameters:
   *
   * - `zoneTypeId`: zoneTypeId
   *
   * - `siteId`: siteId
   *
   * @return OK
   */
  getZoneTypesUsingGET(params: ZoneDictionaryGatewayService.GetZoneTypesUsingGETParams): __Observable<Array<ZoneDto>> {
    return this.getZoneTypesUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<ZoneDto>)
    );
  }
}

module ZoneDictionaryGatewayService {

  /**
   * Parameters for getZoneTypesUsingGET
   */
  export interface GetZoneTypesUsingGETParams {

    /**
     * zoneTypeId
     */
    zoneTypeId: string;

    /**
     * siteId
     */
    siteId: string;
  }
}

export { ZoneDictionaryGatewayService }
