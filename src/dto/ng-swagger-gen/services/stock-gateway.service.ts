/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { StockDto } from '../models/stock-dto';
import { StockAnnihilationResponse } from '../models/stock-annihilation-response';
import { StockAnnihilationCmd } from '../models/stock-annihilation-cmd';
import { CalculateNewProductPeiQuantityResponse } from '../models/calculate-new-product-pei-quantity-response';
import { CalculateNewProductPeiQuantityCmd } from '../models/calculate-new-product-pei-quantity-cmd';
import { AddStockToCarrierCmd } from '../models/add-stock-to-carrier-cmd';
import { ChangeStockProductResponse } from '../models/change-stock-product-response';
import { ChangeStockProductCmd } from '../models/change-stock-product-cmd';
import { ChangeRawStockTypeCmd } from '../models/change-raw-stock-type-cmd';
import { CheckPlaceForStockResponse } from '../models/check-place-for-stock-response';
import { CheckPlaceForStockCmd } from '../models/check-place-for-stock-cmd';
import { DetailedStocksSearchResult } from '../models/detailed-stocks-search-result';
import { DetailedStockSearchQuery } from '../models/detailed-stock-search-query';
import { MoveAllStocksToCarrierResponse } from '../models/move-all-stocks-to-carrier-response';
import { MoveAllStocksToCarrierCmd } from '../models/move-all-stocks-to-carrier-cmd';
import { MoveStockCmdResponse } from '../models/move-stock-cmd-response';
import { MoveStockCmd } from '../models/move-stock-cmd';
import { OutboundOrderCmd } from '../models/outbound-order-cmd';
import { StockInResponse } from '../models/stock-in-response';
import { StockInCmd } from '../models/stock-in-cmd';
import { TryTakeResponse } from '../models/try-take-response';
import { TryTakeRequest } from '../models/try-take-request';
import { UpdateStockTypeEvent } from '../models/update-stock-type-event';
import { UpdateStockTypeByInventoryCmd } from '../models/update-stock-type-by-inventory-cmd';
import { UpdateStockTypeByPickingCmd } from '../models/update-stock-type-by-picking-cmd';
import { UpdateStockTypeInStockByUICmd } from '../models/update-stock-type-in-stock-by-uicmd';
import { UpdateStockTypeForBatchCmd } from '../models/update-stock-type-for-batch-cmd';
import { WriteOffStockResponse } from '../models/write-off-stock-response';
import { WriteOffStockCmd } from '../models/write-off-stock-cmd';
import { WriteOffStockByUICmd } from '../models/write-off-stock-by-uicmd';
import { WriteOffRawResponse } from '../models/write-off-raw-response';
import { WriteOffRawCmd } from '../models/write-off-raw-cmd';
import { WriteOffRawStocksCmd } from '../models/write-off-raw-stocks-cmd';
import { MoveStockToCarrierCmd } from '../models/move-stock-to-carrier-cmd';

/**
 * Stock Gateway
 */
@Injectable({
  providedIn: 'root',
})
class StockGatewayService extends __BaseService {
  static readonly getAllUsingGET3Path = '/v1/stock';
  static readonly annihilateStockUsingDELETEPath = '/v1/stock/annihilate';
  static readonly calculatePeiQuantityUsingPOSTPath = '/v1/stock/calculatePeiQuantity';
  static readonly addStockToCarrierUsingPUTPath = '/v1/stock/carriers/{carrierId}';
  static readonly changeStockProductUsingPOSTPath = '/v1/stock/changeProduct';
  static readonly confirmStocksUsingPOSTPath = '/v1/stock/changeRawStockType';
  static readonly checkPlaceForStockUsingPOSTPath = '/v1/stock/checkPlaceForStock';
  static readonly findAllUsingPOST3Path = '/v1/stock/findAll';
  static readonly moveStockToCarrierUsingPUTPath = '/v1/stock/moveAllTo';
  static readonly moveStockUsingPOSTPath = '/v1/stock/moveStock';
  static readonly outboundOrderUsingPOSTPath = '/v1/stock/outboundOrder';
  static readonly stockInUsingPOSTPath = '/v1/stock/stock-in';
  static readonly tryTakeUsingPOSTPath = '/v1/stock/try-take';
  static readonly updateExpiredStockTypeUsingPOSTPath = '/v1/stock/updateExpired';
  static readonly updateBakeryExpiredStockTypeUsingPOSTPath = '/v1/stock/updateExpiredBakery';
  static readonly updateStockTypeInStockByInventoryUsingPOSTPath = '/v1/stock/updateStockTypeByInventory';
  static readonly updateStockTypeInStockByPickingUsingPOSTPath = '/v1/stock/updateStockTypeByPicking';
  static readonly updateStockTypeInStockByUIUsingPOSTPath = '/v1/stock/updateStockTypeByUI';
  static readonly updateStockTypeForBatchUsingPOSTPath = '/v1/stock/updateStockTypeForBatch';
  static readonly writeOffStockUsingPOSTPath = '/v1/stock/writeOff';
  static readonly writeOffStockByUIUsingPOSTPath = '/v1/stock/writeOffLost';
  static readonly writeOffRawUsingPOSTPath = '/v1/stock/writeOffRaw';
  static readonly writeOffRawStockUsingPOSTPath = '/v1/stock/writeOffRawStock';
  static readonly getByIdUsingGET2Path = '/v1/stock/{id}';
  static readonly moveStockToCarrierUsingPUT1Path = '/v1/stock/{stockId}/moveTo/{carrierId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getAll
   * @param params The `StockGatewayService.GetAllUsingGET3Params` containing the following parameters:
   *
   * - `stockTypeCode`: stockTypeCode
   *
   * - `siteCode`: siteCode
   *
   * - `productId`: productId
   *
   * - `productBatchId`: productBatchId
   *
   * - `placeId`: placeId
   *
   * - `placeBarcode`: placeBarcode
   *
   * - `carrierId`: carrierId
   *
   * @return OK
   */
  getAllUsingGET3Response(params: StockGatewayService.GetAllUsingGET3Params): __Observable<__StrictHttpResponse<Array<StockDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.stockTypeCode != null) __params = __params.set('stockTypeCode', params.stockTypeCode.toString());
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    if (params.productId != null) __params = __params.set('productId', params.productId.toString());
    if (params.productBatchId != null) __params = __params.set('productBatchId', params.productBatchId.toString());
    if (params.placeId != null) __params = __params.set('placeId', params.placeId.toString());
    if (params.placeBarcode != null) __params = __params.set('placeBarcode', params.placeBarcode.toString());
    if (params.carrierId != null) __params = __params.set('carrierId', params.carrierId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<StockDto>>;
      })
    );
  }
  /**
   * getAll
   * @param params The `StockGatewayService.GetAllUsingGET3Params` containing the following parameters:
   *
   * - `stockTypeCode`: stockTypeCode
   *
   * - `siteCode`: siteCode
   *
   * - `productId`: productId
   *
   * - `productBatchId`: productBatchId
   *
   * - `placeId`: placeId
   *
   * - `placeBarcode`: placeBarcode
   *
   * - `carrierId`: carrierId
   *
   * @return OK
   */
  getAllUsingGET3(params: StockGatewayService.GetAllUsingGET3Params): __Observable<Array<StockDto>> {
    return this.getAllUsingGET3Response(params).pipe(
      __map(_r => _r.body as Array<StockDto>)
    );
  }

  /**
   * Уничтожение запаса без проводок в ERP
   * @param cmd cmd
   * @return OK
   */
  annihilateStockUsingDELETEResponse(cmd: StockAnnihilationCmd): __Observable<__StrictHttpResponse<StockAnnihilationResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/annihilate`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockAnnihilationResponse>;
      })
    );
  }
  /**
   * Уничтожение запаса без проводок в ERP
   * @param cmd cmd
   * @return OK
   */
  annihilateStockUsingDELETE(cmd: StockAnnihilationCmd): __Observable<StockAnnihilationResponse> {
    return this.annihilateStockUsingDELETEResponse(cmd).pipe(
      __map(_r => _r.body as StockAnnihilationResponse)
    );
  }

  /**
   * Расчет кол-ва в ПЕИ
   * @param cmd cmd
   * @return OK
   */
  calculatePeiQuantityUsingPOSTResponse(cmd: CalculateNewProductPeiQuantityCmd): __Observable<__StrictHttpResponse<CalculateNewProductPeiQuantityResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/calculatePeiQuantity`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CalculateNewProductPeiQuantityResponse>;
      })
    );
  }
  /**
   * Расчет кол-ва в ПЕИ
   * @param cmd cmd
   * @return OK
   */
  calculatePeiQuantityUsingPOST(cmd: CalculateNewProductPeiQuantityCmd): __Observable<CalculateNewProductPeiQuantityResponse> {
    return this.calculatePeiQuantityUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as CalculateNewProductPeiQuantityResponse)
    );
  }

  /**
   * Добавление запаса в носитель
   * @param params The `StockGatewayService.AddStockToCarrierUsingPUTParams` containing the following parameters:
   *
   * - `cmd`: cmd
   *
   * - `carrierId`: carrierId
   *
   * @return OK
   */
  addStockToCarrierUsingPUTResponse(params: StockGatewayService.AddStockToCarrierUsingPUTParams): __Observable<__StrictHttpResponse<StockDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.cmd;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/carriers/${encodeURIComponent(String(params.carrierId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockDto>;
      })
    );
  }
  /**
   * Добавление запаса в носитель
   * @param params The `StockGatewayService.AddStockToCarrierUsingPUTParams` containing the following parameters:
   *
   * - `cmd`: cmd
   *
   * - `carrierId`: carrierId
   *
   * @return OK
   */
  addStockToCarrierUsingPUT(params: StockGatewayService.AddStockToCarrierUsingPUTParams): __Observable<StockDto> {
    return this.addStockToCarrierUsingPUTResponse(params).pipe(
      __map(_r => _r.body as StockDto)
    );
  }

  /**
   * Изменение productId в существующем запасе
   * @param cmd cmd
   * @return OK
   */
  changeStockProductUsingPOSTResponse(cmd: ChangeStockProductCmd): __Observable<__StrictHttpResponse<ChangeStockProductResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/changeProduct`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ChangeStockProductResponse>;
      })
    );
  }
  /**
   * Изменение productId в существующем запасе
   * @param cmd cmd
   * @return OK
   */
  changeStockProductUsingPOST(cmd: ChangeStockProductCmd): __Observable<ChangeStockProductResponse> {
    return this.changeStockProductUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ChangeStockProductResponse)
    );
  }

  /**
   * Отпуск сырья
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  confirmStocksUsingPOSTResponse(cmd: ChangeRawStockTypeCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/changeRawStockType`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Отпуск сырья
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  confirmStocksUsingPOST(cmd: ChangeRawStockTypeCmd): __Observable<null> {
    return this.confirmStocksUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Проверка, может ли сток лежать в его месте
   * @param cmd cmd
   * @return OK
   */
  checkPlaceForStockUsingPOSTResponse(cmd: CheckPlaceForStockCmd): __Observable<__StrictHttpResponse<CheckPlaceForStockResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/checkPlaceForStock`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CheckPlaceForStockResponse>;
      })
    );
  }
  /**
   * Проверка, может ли сток лежать в его месте
   * @param cmd cmd
   * @return OK
   */
  checkPlaceForStockUsingPOST(cmd: CheckPlaceForStockCmd): __Observable<CheckPlaceForStockResponse> {
    return this.checkPlaceForStockUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as CheckPlaceForStockResponse)
    );
  }

  /**
   * Детальный поиск по стокам
   * @param query query
   * @return OK
   */
  findAllUsingPOST3Response(query: DetailedStockSearchQuery): __Observable<__StrictHttpResponse<DetailedStocksSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/findAll`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DetailedStocksSearchResult>;
      })
    );
  }
  /**
   * Детальный поиск по стокам
   * @param query query
   * @return OK
   */
  findAllUsingPOST3(query: DetailedStockSearchQuery): __Observable<DetailedStocksSearchResult> {
    return this.findAllUsingPOST3Response(query).pipe(
      __map(_r => _r.body as DetailedStocksSearchResult)
    );
  }

  /**
   * Перемещение запаса в носитель с корректировкой резерва
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  moveStockToCarrierUsingPUTResponse(cmd: MoveAllStocksToCarrierCmd): __Observable<__StrictHttpResponse<MoveAllStocksToCarrierResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/moveAllTo`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<MoveAllStocksToCarrierResponse>;
      })
    );
  }
  /**
   * Перемещение запаса в носитель с корректировкой резерва
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  moveStockToCarrierUsingPUT(cmd: MoveAllStocksToCarrierCmd): __Observable<MoveAllStocksToCarrierResponse> {
    return this.moveStockToCarrierUsingPUTResponse(cmd).pipe(
      __map(_r => _r.body as MoveAllStocksToCarrierResponse)
    );
  }

  /**
   * Перемещение запаса (без проверок, без корректировки резервов)
   * @param cmd cmd
   * @return OK
   */
  moveStockUsingPOSTResponse(cmd: MoveStockCmd): __Observable<__StrictHttpResponse<MoveStockCmdResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/moveStock`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<MoveStockCmdResponse>;
      })
    );
  }
  /**
   * Перемещение запаса (без проверок, без корректировки резервов)
   * @param cmd cmd
   * @return OK
   */
  moveStockUsingPOST(cmd: MoveStockCmd): __Observable<MoveStockCmdResponse> {
    return this.moveStockUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as MoveStockCmdResponse)
    );
  }

  /**
   * Отгрузка заказов
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  outboundOrderUsingPOSTResponse(cmd: OutboundOrderCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/outboundOrder`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Отгрузка заказов
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  outboundOrderUsingPOST(cmd: OutboundOrderCmd): __Observable<null> {
    return this.outboundOrderUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Добавление найденного товара в место или носитель без проверок
   * @param cmd cmd
   * @return OK
   */
  stockInUsingPOSTResponse(cmd: StockInCmd): __Observable<__StrictHttpResponse<StockInResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/stock-in`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockInResponse>;
      })
    );
  }
  /**
   * Добавление найденного товара в место или носитель без проверок
   * @param cmd cmd
   * @return OK
   */
  stockInUsingPOST(cmd: StockInCmd): __Observable<StockInResponse> {
    return this.stockInUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as StockInResponse)
    );
  }

  /**
   * Запрос на попытку взять товар из ячейки
   * @param tryTakeRequest tryTakeRequest
   * @return OK
   */
  tryTakeUsingPOSTResponse(tryTakeRequest: TryTakeRequest): __Observable<__StrictHttpResponse<TryTakeResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = tryTakeRequest;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/try-take`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TryTakeResponse>;
      })
    );
  }
  /**
   * Запрос на попытку взять товар из ячейки
   * @param tryTakeRequest tryTakeRequest
   * @return OK
   */
  tryTakeUsingPOST(tryTakeRequest: TryTakeRequest): __Observable<TryTakeResponse> {
    return this.tryTakeUsingPOSTResponse(tryTakeRequest).pipe(
      __map(_r => _r.body as TryTakeResponse)
    );
  }

  /**
   * Перевод всех товаров с истекающим сроком реализации в SS
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  updateExpiredStockTypeUsingPOSTResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/updateExpired`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Перевод всех товаров с истекающим сроком реализации в SS
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  updateExpiredStockTypeUsingPOST(): __Observable<null> {
    return this.updateExpiredStockTypeUsingPOSTResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Перевод товаров пекарни с истекающим сроком реализации в SS
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  updateBakeryExpiredStockTypeUsingPOSTResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/updateExpiredBakery`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Перевод товаров пекарни с истекающим сроком реализации в SS
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  updateBakeryExpiredStockTypeUsingPOST(): __Observable<null> {
    return this.updateBakeryExpiredStockTypeUsingPOSTResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Изменение вида запаса инвентаризацией
   * @param updateStockTypeCmd updateStockTypeCmd
   * @return OK
   */
  updateStockTypeInStockByInventoryUsingPOSTResponse(updateStockTypeCmd: UpdateStockTypeByInventoryCmd): __Observable<__StrictHttpResponse<UpdateStockTypeEvent>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateStockTypeCmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/updateStockTypeByInventory`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UpdateStockTypeEvent>;
      })
    );
  }
  /**
   * Изменение вида запаса инвентаризацией
   * @param updateStockTypeCmd updateStockTypeCmd
   * @return OK
   */
  updateStockTypeInStockByInventoryUsingPOST(updateStockTypeCmd: UpdateStockTypeByInventoryCmd): __Observable<UpdateStockTypeEvent> {
    return this.updateStockTypeInStockByInventoryUsingPOSTResponse(updateStockTypeCmd).pipe(
      __map(_r => _r.body as UpdateStockTypeEvent)
    );
  }

  /**
   * Изменение вида запаса комплектацией
   * @param updateStockTypeCmd updateStockTypeCmd
   * @return OK
   */
  updateStockTypeInStockByPickingUsingPOSTResponse(updateStockTypeCmd: UpdateStockTypeByPickingCmd): __Observable<__StrictHttpResponse<UpdateStockTypeEvent>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateStockTypeCmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/updateStockTypeByPicking`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UpdateStockTypeEvent>;
      })
    );
  }
  /**
   * Изменение вида запаса комплектацией
   * @param updateStockTypeCmd updateStockTypeCmd
   * @return OK
   */
  updateStockTypeInStockByPickingUsingPOST(updateStockTypeCmd: UpdateStockTypeByPickingCmd): __Observable<UpdateStockTypeEvent> {
    return this.updateStockTypeInStockByPickingUsingPOSTResponse(updateStockTypeCmd).pipe(
      __map(_r => _r.body as UpdateStockTypeEvent)
    );
  }

  /**
   * Изменение вида запаса через UI
   * @param cmd cmd
   */
  updateStockTypeInStockByUIUsingPOSTResponse(cmd: UpdateStockTypeInStockByUICmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/updateStockTypeByUI`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Изменение вида запаса через UI
   * @param cmd cmd
   */
  updateStockTypeInStockByUIUsingPOST(cmd: UpdateStockTypeInStockByUICmd): __Observable<null> {
    return this.updateStockTypeInStockByUIUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Изменение вида запаса для партии. Вызывается после обеления.
   * @param updateStockTypeForBatchCmd updateStockTypeForBatchCmd
   */
  updateStockTypeForBatchUsingPOSTResponse(updateStockTypeForBatchCmd: UpdateStockTypeForBatchCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateStockTypeForBatchCmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/updateStockTypeForBatch`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Изменение вида запаса для партии. Вызывается после обеления.
   * @param updateStockTypeForBatchCmd updateStockTypeForBatchCmd
   */
  updateStockTypeForBatchUsingPOST(updateStockTypeForBatchCmd: UpdateStockTypeForBatchCmd): __Observable<null> {
    return this.updateStockTypeForBatchUsingPOSTResponse(updateStockTypeForBatchCmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Списание запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  writeOffStockUsingPOSTResponse(cmd: WriteOffStockCmd): __Observable<__StrictHttpResponse<WriteOffStockResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/writeOff`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<WriteOffStockResponse>;
      })
    );
  }
  /**
   * Списание запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  writeOffStockUsingPOST(cmd: WriteOffStockCmd): __Observable<WriteOffStockResponse> {
    return this.writeOffStockUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as WriteOffStockResponse)
    );
  }

  /**
   * Списание запаса
   * @param cmd cmd
   * @return OK
   */
  writeOffStockByUIUsingPOSTResponse(cmd: WriteOffStockByUICmd): __Observable<__StrictHttpResponse<WriteOffStockResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/writeOffLost`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<WriteOffStockResponse>;
      })
    );
  }
  /**
   * Списание запаса
   * @param cmd cmd
   * @return OK
   */
  writeOffStockByUIUsingPOST(cmd: WriteOffStockByUICmd): __Observable<WriteOffStockResponse> {
    return this.writeOffStockByUIUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as WriteOffStockResponse)
    );
  }

  /**
   * Отпуск сырья с доинвентаризацией выбывшего запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  writeOffRawUsingPOSTResponse(cmd: WriteOffRawCmd): __Observable<__StrictHttpResponse<WriteOffRawResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/writeOffRaw`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<WriteOffRawResponse>;
      })
    );
  }
  /**
   * Отпуск сырья с доинвентаризацией выбывшего запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  writeOffRawUsingPOST(cmd: WriteOffRawCmd): __Observable<WriteOffRawResponse> {
    return this.writeOffRawUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as WriteOffRawResponse)
    );
  }

  /**
   * Отпуск сырья
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  writeOffRawStockUsingPOSTResponse(cmd: WriteOffRawStocksCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/writeOffRawStock`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Отпуск сырья
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   */
  writeOffRawStockUsingPOST(cmd: WriteOffRawStocksCmd): __Observable<null> {
    return this.writeOffRawStockUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getById
   * @param id id
   * @return OK
   */
  getByIdUsingGET2Response(id: string): __Observable<__StrictHttpResponse<StockDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockDto>;
      })
    );
  }
  /**
   * getById
   * @param id id
   * @return OK
   */
  getByIdUsingGET2(id: string): __Observable<StockDto> {
    return this.getByIdUsingGET2Response(id).pipe(
      __map(_r => _r.body as StockDto)
    );
  }

  /**
   * Перемещение запаса в носитель с корректировкой резерва
   * @param params The `StockGatewayService.MoveStockToCarrierUsingPUT1Params` containing the following parameters:
   *
   * - `stockId`: stockId
   *
   * - `cmd`: cmd
   *
   * - `carrierId`: carrierId
   *
   * @return OK
   */
  moveStockToCarrierUsingPUT1Response(params: StockGatewayService.MoveStockToCarrierUsingPUT1Params): __Observable<__StrictHttpResponse<StockDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/${encodeURIComponent(String(params.stockId))}/moveTo/${encodeURIComponent(String(params.carrierId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockDto>;
      })
    );
  }
  /**
   * Перемещение запаса в носитель с корректировкой резерва
   * @param params The `StockGatewayService.MoveStockToCarrierUsingPUT1Params` containing the following parameters:
   *
   * - `stockId`: stockId
   *
   * - `cmd`: cmd
   *
   * - `carrierId`: carrierId
   *
   * @return OK
   */
  moveStockToCarrierUsingPUT1(params: StockGatewayService.MoveStockToCarrierUsingPUT1Params): __Observable<StockDto> {
    return this.moveStockToCarrierUsingPUT1Response(params).pipe(
      __map(_r => _r.body as StockDto)
    );
  }
}

module StockGatewayService {

  /**
   * Parameters for getAllUsingGET3
   */
  export interface GetAllUsingGET3Params {

    /**
     * stockTypeCode
     */
    stockTypeCode?: string;

    /**
     * siteCode
     */
    siteCode?: string;

    /**
     * productId
     */
    productId?: string;

    /**
     * productBatchId
     */
    productBatchId?: string;

    /**
     * placeId
     */
    placeId?: string;

    /**
     * placeBarcode
     */
    placeBarcode?: string;

    /**
     * carrierId
     */
    carrierId?: string;
  }

  /**
   * Parameters for addStockToCarrierUsingPUT
   */
  export interface AddStockToCarrierUsingPUTParams {

    /**
     * cmd
     */
    cmd: AddStockToCarrierCmd;

    /**
     * carrierId
     */
    carrierId: string;
  }

  /**
   * Parameters for moveStockToCarrierUsingPUT1
   */
  export interface MoveStockToCarrierUsingPUT1Params {

    /**
     * stockId
     */
    stockId: string;

    /**
     * cmd
     */
    cmd: MoveStockToCarrierCmd;

    /**
     * carrierId
     */
    carrierId: string;
  }
}

export { StockGatewayService }
