/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';


/**
 * Download Gateway
 */
@Injectable({
  providedIn: 'root',
})
class DownloadGatewayService extends __BaseService {
  static readonly clusterDeliveryReservationRouteDownloadUsingGETPath = '/v1/reservation/route/download/{siteCode}';
  static readonly routeStrategyDownloadUsingGETPath = '/v1/route-strategy/download/{strategyId}';
  static readonly stocksDownloadUsingGETPath = '/v1/stock/download/{siteCode}';
  static readonly placeTypesDownloadUsingGETPath = '/v1/topology/placeTypes/download/{siteCode}';
  static readonly placesDownloadUsingGETPath = '/v1/topology/places/download/{siteCode}';
  static readonly zoneTypesDownloadUsingGETPath = '/v1/topology/zoneTypes/download';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос на выгрузку змейки по буферам отгрузки в файл
   *
   * siteCode - обязательное поле
   * @param siteCode siteCode
   */
  clusterDeliveryReservationRouteDownloadUsingGETResponse(siteCode: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/reservation/route/download/${encodeURIComponent(String(siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на выгрузку змейки по буферам отгрузки в файл
   *
   * siteCode - обязательное поле
   * @param siteCode siteCode
   */
  clusterDeliveryReservationRouteDownloadUsingGET(siteCode: string): __Observable<null> {
    return this.clusterDeliveryReservationRouteDownloadUsingGETResponse(siteCode).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на выгрузку файла с приоритетом обхода секций для стратегии обхода
   * @param strategyId strategyId
   */
  routeStrategyDownloadUsingGETResponse(strategyId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/route-strategy/download/${encodeURIComponent(String(strategyId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на выгрузку файла с приоритетом обхода секций для стратегии обхода
   * @param strategyId strategyId
   */
  routeStrategyDownloadUsingGET(strategyId: string): __Observable<null> {
    return this.routeStrategyDownloadUsingGETResponse(strategyId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на выгрузку стоков
   *
   * siteCode - обязательное поле
   * @param siteCode siteCode
   */
  stocksDownloadUsingGETResponse(siteCode: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/download/${encodeURIComponent(String(siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на выгрузку стоков
   *
   * siteCode - обязательное поле
   * @param siteCode siteCode
   */
  stocksDownloadUsingGET(siteCode: string): __Observable<null> {
    return this.stocksDownloadUsingGETResponse(siteCode).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на выгрузку файла с типами мест
   *
   * siteCode - обязательные поля
   * @param siteCode siteCode
   */
  placeTypesDownloadUsingGETResponse(siteCode: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/placeTypes/download/${encodeURIComponent(String(siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на выгрузку файла с типами мест
   *
   * siteCode - обязательные поля
   * @param siteCode siteCode
   */
  placeTypesDownloadUsingGET(siteCode: string): __Observable<null> {
    return this.placeTypesDownloadUsingGETResponse(siteCode).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на выгрузку файла с топологией
   *
   * siteCode - обязательные поля
   * @param params The `DownloadGatewayService.PlacesDownloadUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `address`: address
   */
  placesDownloadUsingGETResponse(params: DownloadGatewayService.PlacesDownloadUsingGETParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.address != null) __params = __params.set('address', params.address.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/download/${encodeURIComponent(String(params.siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на выгрузку файла с топологией
   *
   * siteCode - обязательные поля
   * @param params The `DownloadGatewayService.PlacesDownloadUsingGETParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `address`: address
   */
  placesDownloadUsingGET(params: DownloadGatewayService.PlacesDownloadUsingGETParams): __Observable<null> {
    return this.placesDownloadUsingGETResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на выгрузку файла с типами зон
   */
  zoneTypesDownloadUsingGETResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zoneTypes/download`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на выгрузку файла с типами зон
   */
  zoneTypesDownloadUsingGET(): __Observable<null> {
    return this.zoneTypesDownloadUsingGETResponse().pipe(
      __map(_r => _r.body as null)
    );
  }
}

module DownloadGatewayService {

  /**
   * Parameters for placesDownloadUsingGET
   */
  export interface PlacesDownloadUsingGETParams {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * address
     */
    address?: string;
  }
}

export { DownloadGatewayService }
