/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { SiteSettingDto } from '../models/site-setting-dto';

/**
 * Site Settings Gateway
 */
@Injectable({
  providedIn: 'root',
})
class SiteSettingsGatewayService extends __BaseService {
  static readonly getSettingsForSiteUsingGETPath = '/v1/topology/sites/settings/{siteCode}';
  static readonly updateSettingsForSiteUsingPUTPath = '/v1/topology/sites/settings/{siteCode}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getSettingsForSite
   * @param siteCode siteCode
   * @return OK
   */
  getSettingsForSiteUsingGETResponse(siteCode: string): __Observable<__StrictHttpResponse<Array<SiteSettingDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites/settings/${encodeURIComponent(String(siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<SiteSettingDto>>;
      })
    );
  }
  /**
   * getSettingsForSite
   * @param siteCode siteCode
   * @return OK
   */
  getSettingsForSiteUsingGET(siteCode: string): __Observable<Array<SiteSettingDto>> {
    return this.getSettingsForSiteUsingGETResponse(siteCode).pipe(
      __map(_r => _r.body as Array<SiteSettingDto>)
    );
  }

  /**
   * updateSettingsForSite
   * @param params The `SiteSettingsGatewayService.UpdateSettingsForSiteUsingPUTParams` containing the following parameters:
   *
   * - `siteSettingDtos`: siteSettingDtos
   *
   * - `siteCode`: siteCode
   *
   * @return OK
   */
  updateSettingsForSiteUsingPUTResponse(params: SiteSettingsGatewayService.UpdateSettingsForSiteUsingPUTParams): __Observable<__StrictHttpResponse<Array<SiteSettingDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.siteSettingDtos;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/sites/settings/${encodeURIComponent(String(params.siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<SiteSettingDto>>;
      })
    );
  }
  /**
   * updateSettingsForSite
   * @param params The `SiteSettingsGatewayService.UpdateSettingsForSiteUsingPUTParams` containing the following parameters:
   *
   * - `siteSettingDtos`: siteSettingDtos
   *
   * - `siteCode`: siteCode
   *
   * @return OK
   */
  updateSettingsForSiteUsingPUT(params: SiteSettingsGatewayService.UpdateSettingsForSiteUsingPUTParams): __Observable<Array<SiteSettingDto>> {
    return this.updateSettingsForSiteUsingPUTResponse(params).pipe(
      __map(_r => _r.body as Array<SiteSettingDto>)
    );
  }
}

module SiteSettingsGatewayService {

  /**
   * Parameters for updateSettingsForSiteUsingPUT
   */
  export interface UpdateSettingsForSiteUsingPUTParams {

    /**
     * siteSettingDtos
     */
    siteSettingDtos: Array<SiteSettingDto>;

    /**
     * siteCode
     */
    siteCode: string;
  }
}

export { SiteSettingsGatewayService }
