/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ClusterDeliveryReservationRoute } from '../models/cluster-delivery-reservation-route';
import { SlottingRoute } from '../models/slotting-route';

/**
 * Route Gateway
 */
@Injectable({
  providedIn: 'root',
})
class RouteGatewayService extends __BaseService {
  static readonly reservationRouteUsingGETPath = '/v1/routes/reservation/{siteCode}';
  static readonly slottingRouteUsingGETPath = '/v1/routes/{transferId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос змейки для подбора мест в отгрузке
   * @param siteCode siteCode
   * @return OK
   */
  reservationRouteUsingGETResponse(siteCode: string): __Observable<__StrictHttpResponse<ClusterDeliveryReservationRoute>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/routes/reservation/${encodeURIComponent(String(siteCode))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ClusterDeliveryReservationRoute>;
      })
    );
  }
  /**
   * Запрос змейки для подбора мест в отгрузке
   * @param siteCode siteCode
   * @return OK
   */
  reservationRouteUsingGET(siteCode: string): __Observable<ClusterDeliveryReservationRoute> {
    return this.reservationRouteUsingGETResponse(siteCode).pipe(
      __map(_r => _r.body as ClusterDeliveryReservationRoute)
    );
  }

  /**
   * Запрос маршрута слотчика по области действий
   *
   * Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
   * @param params The `RouteGatewayService.SlottingRouteUsingGETParams` containing the following parameters:
   *
   * - `transferId`: transferId
   *
   * - `startPlaceId`: startPlaceId
   *
   * - `currentPlaceId`: currentPlaceId
   *
   * @return OK
   */
  slottingRouteUsingGETResponse(params: RouteGatewayService.SlottingRouteUsingGETParams): __Observable<__StrictHttpResponse<SlottingRoute>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.startPlaceId != null) __params = __params.set('startPlaceId', params.startPlaceId.toString());
    if (params.currentPlaceId != null) __params = __params.set('currentPlaceId', params.currentPlaceId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/routes/${encodeURIComponent(String(params.transferId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SlottingRoute>;
      })
    );
  }
  /**
   * Запрос маршрута слотчика по области действий
   *
   * Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
   * @param params The `RouteGatewayService.SlottingRouteUsingGETParams` containing the following parameters:
   *
   * - `transferId`: transferId
   *
   * - `startPlaceId`: startPlaceId
   *
   * - `currentPlaceId`: currentPlaceId
   *
   * @return OK
   */
  slottingRouteUsingGET(params: RouteGatewayService.SlottingRouteUsingGETParams): __Observable<SlottingRoute> {
    return this.slottingRouteUsingGETResponse(params).pipe(
      __map(_r => _r.body as SlottingRoute)
    );
  }
}

module RouteGatewayService {

  /**
   * Parameters for slottingRouteUsingGET
   */
  export interface SlottingRouteUsingGETParams {

    /**
     * transferId
     */
    transferId: string;

    /**
     * startPlaceId
     */
    startPlaceId: string;

    /**
     * currentPlaceId
     */
    currentPlaceId?: string;
  }
}

export { RouteGatewayService }
