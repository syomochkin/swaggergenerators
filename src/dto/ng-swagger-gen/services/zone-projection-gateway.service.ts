/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ZoneProjection } from '../models/zone-projection';

/**
 * Zone Projection Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ZoneProjectionGatewayService extends __BaseService {
  static readonly getZonesBySiteIdAndTypeIdUsingGETPath = '/v1/topology/zones/projections';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getZonesBySiteIdAndTypeId
   * @param params The `ZoneProjectionGatewayService.GetZonesBySiteIdAndTypeIdUsingGETParams` containing the following parameters:
   *
   * - `typeId`: typeId
   *
   * - `siteId`: siteId
   *
   * @return OK
   */
  getZonesBySiteIdAndTypeIdUsingGETResponse(params: ZoneProjectionGatewayService.GetZonesBySiteIdAndTypeIdUsingGETParams): __Observable<__StrictHttpResponse<Array<ZoneProjection>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.typeId != null) __params = __params.set('typeId', params.typeId.toString());
    if (params.siteId != null) __params = __params.set('siteId', params.siteId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zones/projections`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneProjection>>;
      })
    );
  }
  /**
   * getZonesBySiteIdAndTypeId
   * @param params The `ZoneProjectionGatewayService.GetZonesBySiteIdAndTypeIdUsingGETParams` containing the following parameters:
   *
   * - `typeId`: typeId
   *
   * - `siteId`: siteId
   *
   * @return OK
   */
  getZonesBySiteIdAndTypeIdUsingGET(params: ZoneProjectionGatewayService.GetZonesBySiteIdAndTypeIdUsingGETParams): __Observable<Array<ZoneProjection>> {
    return this.getZonesBySiteIdAndTypeIdUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<ZoneProjection>)
    );
  }
}

module ZoneProjectionGatewayService {

  /**
   * Parameters for getZonesBySiteIdAndTypeIdUsingGET
   */
  export interface GetZonesBySiteIdAndTypeIdUsingGETParams {

    /**
     * typeId
     */
    typeId: string;

    /**
     * siteId
     */
    siteId: string;
  }
}

export { ZoneProjectionGatewayService }
