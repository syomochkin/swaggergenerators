/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { SiteDto } from '../models/site-dto';
import { CreateSiteCmd } from '../models/create-site-cmd';
import { CodeExists } from '../models/code-exists';
import { SiteCodeExistsQuery } from '../models/site-code-exists-query';
import { UpdateSiteCmd } from '../models/update-site-cmd';

/**
 * Site Gateway
 */
@Injectable({
  providedIn: 'root',
})
class SiteGatewayService extends __BaseService {
  static readonly getSitesUsingGETPath = '/v1/topology/sites';
  static readonly createSiteUsingPOSTPath = '/v1/topology/sites';
  static readonly getSiteByCodeUsingGETPath = '/v1/topology/sites/byCode/{code}';
  static readonly codeExistsUsingPOST2Path = '/v1/topology/sites/codeExists';
  static readonly getSiteUsingGETPath = '/v1/topology/sites/{siteId}';
  static readonly updateSiteUsingPUTPath = '/v1/topology/sites/{siteId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getSites
   * @return OK
   */
  getSitesUsingGETResponse(): __Observable<__StrictHttpResponse<Array<SiteDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<SiteDto>>;
      })
    );
  }
  /**
   * getSites
   * @return OK
   */
  getSitesUsingGET(): __Observable<Array<SiteDto>> {
    return this.getSitesUsingGETResponse().pipe(
      __map(_r => _r.body as Array<SiteDto>)
    );
  }

  /**
   * createSite
   * @param cmd cmd
   * @return OK
   */
  createSiteUsingPOSTResponse(cmd: CreateSiteCmd): __Observable<__StrictHttpResponse<SiteDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/sites`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SiteDto>;
      })
    );
  }
  /**
   * createSite
   * @param cmd cmd
   * @return OK
   */
  createSiteUsingPOST(cmd: CreateSiteCmd): __Observable<SiteDto> {
    return this.createSiteUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as SiteDto)
    );
  }

  /**
   * getSiteByCode
   * @param code code
   * @return OK
   */
  getSiteByCodeUsingGETResponse(code: string): __Observable<__StrictHttpResponse<SiteDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites/byCode/${encodeURIComponent(String(code))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SiteDto>;
      })
    );
  }
  /**
   * getSiteByCode
   * @param code code
   * @return OK
   */
  getSiteByCodeUsingGET(code: string): __Observable<SiteDto> {
    return this.getSiteByCodeUsingGETResponse(code).pipe(
      __map(_r => _r.body as SiteDto)
    );
  }

  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST2Response(query: SiteCodeExistsQuery): __Observable<__StrictHttpResponse<CodeExists>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/sites/codeExists`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CodeExists>;
      })
    );
  }
  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST2(query: SiteCodeExistsQuery): __Observable<CodeExists> {
    return this.codeExistsUsingPOST2Response(query).pipe(
      __map(_r => _r.body as CodeExists)
    );
  }

  /**
   * getSite
   * @param siteId siteId
   * @return OK
   */
  getSiteUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<SiteDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SiteDto>;
      })
    );
  }
  /**
   * getSite
   * @param siteId siteId
   * @return OK
   */
  getSiteUsingGET(siteId: string): __Observable<SiteDto> {
    return this.getSiteUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as SiteDto)
    );
  }

  /**
   * updateSite
   * @param params The `SiteGatewayService.UpdateSiteUsingPUTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateSiteUsingPUTResponse(params: SiteGatewayService.UpdateSiteUsingPUTParams): __Observable<__StrictHttpResponse<SiteDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<SiteDto>;
      })
    );
  }
  /**
   * updateSite
   * @param params The `SiteGatewayService.UpdateSiteUsingPUTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateSiteUsingPUT(params: SiteGatewayService.UpdateSiteUsingPUTParams): __Observable<SiteDto> {
    return this.updateSiteUsingPUTResponse(params).pipe(
      __map(_r => _r.body as SiteDto)
    );
  }
}

module SiteGatewayService {

  /**
   * Parameters for updateSiteUsingPUT
   */
  export interface UpdateSiteUsingPUTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * cmd
     */
    cmd: UpdateSiteCmd;
  }
}

export { SiteGatewayService }
