/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CarrierGridView } from '../models/carrier-grid-view';

/**
 * Carrier View Gateway
 */
@Injectable({
  providedIn: 'root',
})
class CarrierViewGatewayService extends __BaseService {
  static readonly getByPlaceIdUsingGETPath = '/v1/stock/carriers-view';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get carriers hierarchy in place for grid representation
   * @param placeId placeId
   * @return OK
   */
  getByPlaceIdUsingGETResponse(placeId: string): __Observable<__StrictHttpResponse<Array<CarrierGridView>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (placeId != null) __params = __params.set('placeId', placeId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/carriers-view`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CarrierGridView>>;
      })
    );
  }
  /**
   * Get carriers hierarchy in place for grid representation
   * @param placeId placeId
   * @return OK
   */
  getByPlaceIdUsingGET(placeId: string): __Observable<Array<CarrierGridView>> {
    return this.getByPlaceIdUsingGETResponse(placeId).pipe(
      __map(_r => _r.body as Array<CarrierGridView>)
    );
  }
}

module CarrierViewGatewayService {
}

export { CarrierViewGatewayService }
