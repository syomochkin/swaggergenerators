/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { AuditMarkRecordSearchResponse } from '../models/audit-mark-record-search-response';
import { AuditMarkRecordSearchQuery } from '../models/audit-mark-record-search-query';

/**
 * Mark Audit Gateway
 */
@Injectable({
  providedIn: 'root',
})
class MarkAuditGatewayService extends __BaseService {
  static readonly findAllUsingPOST1Path = '/v1/audit/mark/findAll';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * История движения товаров
   * @param searchQuery searchQuery
   * @return OK
   */
  findAllUsingPOST1Response(searchQuery: AuditMarkRecordSearchQuery): __Observable<__StrictHttpResponse<AuditMarkRecordSearchResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = searchQuery;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/audit/mark/findAll`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<AuditMarkRecordSearchResponse>;
      })
    );
  }
  /**
   * История движения товаров
   * @param searchQuery searchQuery
   * @return OK
   */
  findAllUsingPOST1(searchQuery: AuditMarkRecordSearchQuery): __Observable<AuditMarkRecordSearchResponse> {
    return this.findAllUsingPOST1Response(searchQuery).pipe(
      __map(_r => _r.body as AuditMarkRecordSearchResponse)
    );
  }
}

module MarkAuditGatewayService {
}

export { MarkAuditGatewayService }
