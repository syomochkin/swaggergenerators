/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ZoneDto } from '../models/zone-dto';
import { CreateZoneCmd } from '../models/create-zone-cmd';
import { CodeExists } from '../models/code-exists';
import { ZoneCodeExistsQuery } from '../models/zone-code-exists-query';
import { UpdateZoneCmd } from '../models/update-zone-cmd';

/**
 * Zone Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ZoneGatewayService extends __BaseService {
  static readonly getZonesByTypeUsingGETPath = '/v1/topology/zones';
  static readonly createZoneUsingPOSTPath = '/v1/topology/zones';
  static readonly codeExistsUsingPOST3Path = '/v1/topology/zones/codeExists';
  static readonly getZoneByIdUsingGETPath = '/v1/topology/zones/{id}';
  static readonly updateZoneUsingPUTPath = '/v1/topology/zones/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getZonesByType
   * @param params The `ZoneGatewayService.GetZonesByTypeUsingGETParams` containing the following parameters:
   *
   * - `zoneTypeCode`: zoneTypeCode
   *
   * - `siteCode`: siteCode
   *
   * @return OK
   */
  getZonesByTypeUsingGETResponse(params: ZoneGatewayService.GetZonesByTypeUsingGETParams): __Observable<__StrictHttpResponse<Array<ZoneDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.zoneTypeCode != null) __params = __params.set('zoneTypeCode', params.zoneTypeCode.toString());
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zones`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneDto>>;
      })
    );
  }
  /**
   * getZonesByType
   * @param params The `ZoneGatewayService.GetZonesByTypeUsingGETParams` containing the following parameters:
   *
   * - `zoneTypeCode`: zoneTypeCode
   *
   * - `siteCode`: siteCode
   *
   * @return OK
   */
  getZonesByTypeUsingGET(params: ZoneGatewayService.GetZonesByTypeUsingGETParams): __Observable<Array<ZoneDto>> {
    return this.getZonesByTypeUsingGETResponse(params).pipe(
      __map(_r => _r.body as Array<ZoneDto>)
    );
  }

  /**
   * create a Zone
   *
   * Create a Zone. code, siteId, typeId are required fields.
   * @param cmd cmd
   * @return OK
   */
  createZoneUsingPOSTResponse(cmd: CreateZoneCmd): __Observable<__StrictHttpResponse<ZoneDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zones`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneDto>;
      })
    );
  }
  /**
   * create a Zone
   *
   * Create a Zone. code, siteId, typeId are required fields.
   * @param cmd cmd
   * @return OK
   */
  createZoneUsingPOST(cmd: CreateZoneCmd): __Observable<ZoneDto> {
    return this.createZoneUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ZoneDto)
    );
  }

  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST3Response(query: ZoneCodeExistsQuery): __Observable<__StrictHttpResponse<CodeExists>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zones/codeExists`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CodeExists>;
      })
    );
  }
  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST3(query: ZoneCodeExistsQuery): __Observable<CodeExists> {
    return this.codeExistsUsingPOST3Response(query).pipe(
      __map(_r => _r.body as CodeExists)
    );
  }

  /**
   * getZoneById
   * @param id id
   * @return OK
   */
  getZoneByIdUsingGETResponse(id: string): __Observable<__StrictHttpResponse<ZoneDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/zones/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneDto>;
      })
    );
  }
  /**
   * getZoneById
   * @param id id
   * @return OK
   */
  getZoneByIdUsingGET(id: string): __Observable<ZoneDto> {
    return this.getZoneByIdUsingGETResponse(id).pipe(
      __map(_r => _r.body as ZoneDto)
    );
  }

  /**
   * update the Zone
   *
   * Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
   * @param params The `ZoneGatewayService.UpdateZoneUsingPUTParams` containing the following parameters:
   *
   * - `id`: id
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateZoneUsingPUTResponse(params: ZoneGatewayService.UpdateZoneUsingPUTParams): __Observable<__StrictHttpResponse<ZoneDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/zones/${encodeURIComponent(String(params.id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneDto>;
      })
    );
  }
  /**
   * update the Zone
   *
   * Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
   * @param params The `ZoneGatewayService.UpdateZoneUsingPUTParams` containing the following parameters:
   *
   * - `id`: id
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateZoneUsingPUT(params: ZoneGatewayService.UpdateZoneUsingPUTParams): __Observable<ZoneDto> {
    return this.updateZoneUsingPUTResponse(params).pipe(
      __map(_r => _r.body as ZoneDto)
    );
  }
}

module ZoneGatewayService {

  /**
   * Parameters for getZonesByTypeUsingGET
   */
  export interface GetZonesByTypeUsingGETParams {

    /**
     * zoneTypeCode
     */
    zoneTypeCode: string;

    /**
     * siteCode
     */
    siteCode: string;
  }

  /**
   * Parameters for updateZoneUsingPUT
   */
  export interface UpdateZoneUsingPUTParams {

    /**
     * id
     */
    id: string;

    /**
     * cmd
     */
    cmd: UpdateZoneCmd;
  }
}

export { ZoneGatewayService }
