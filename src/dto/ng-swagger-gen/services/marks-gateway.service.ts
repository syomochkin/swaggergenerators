/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { DeleteMarksResponse } from '../models/delete-marks-response';
import { DeleteMarksCmd } from '../models/delete-marks-cmd';
import { ChangeMarksStatusResponse } from '../models/change-marks-status-response';
import { ChangeMarksStatusCmd } from '../models/change-marks-status-cmd';
import { CreateMarksResponse } from '../models/create-marks-response';
import { CreateMarksCmd } from '../models/create-marks-cmd';
import { CreateScannedChzMarkResponse } from '../models/create-scanned-chz-mark-response';
import { CreateScannedChzMarkCmd } from '../models/create-scanned-chz-mark-cmd';
import { MarkSearchResult } from '../models/mark-search-result';
import { MarkSearchQuery } from '../models/mark-search-query';
import { UpdateMarksResponse } from '../models/update-marks-response';
import { UpdateMarksCmd } from '../models/update-marks-cmd';

/**
 * Marks Gateway
 */
@Injectable({
  providedIn: 'root',
})
class MarksGatewayService extends __BaseService {
  static readonly deleteMarksUsingDELETEPath = '/v1/marks';
  static readonly changeMarksStatusUsingPOSTPath = '/v1/marks/changeStatus';
  static readonly createMarksUsingPOSTPath = '/v1/marks/create';
  static readonly createScannedChzMarkUsingPOSTPath = '/v1/marks/createScannedChzMark';
  static readonly searchUsingPOSTPath = '/v1/marks/search';
  static readonly updateMarksUsingPOSTPath = '/v1/marks/update';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Удаление марок
   * @param request request
   * @return OK
   */
  deleteMarksUsingDELETEResponse(request: DeleteMarksCmd): __Observable<__StrictHttpResponse<DeleteMarksResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/marks`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DeleteMarksResponse>;
      })
    );
  }
  /**
   * Удаление марок
   * @param request request
   * @return OK
   */
  deleteMarksUsingDELETE(request: DeleteMarksCmd): __Observable<DeleteMarksResponse> {
    return this.deleteMarksUsingDELETEResponse(request).pipe(
      __map(_r => _r.body as DeleteMarksResponse)
    );
  }

  /**
   * Изменение статусов марок
   * @param request request
   * @return OK
   */
  changeMarksStatusUsingPOSTResponse(request: ChangeMarksStatusCmd): __Observable<__StrictHttpResponse<ChangeMarksStatusResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/marks/changeStatus`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ChangeMarksStatusResponse>;
      })
    );
  }
  /**
   * Изменение статусов марок
   * @param request request
   * @return OK
   */
  changeMarksStatusUsingPOST(request: ChangeMarksStatusCmd): __Observable<ChangeMarksStatusResponse> {
    return this.changeMarksStatusUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as ChangeMarksStatusResponse)
    );
  }

  /**
   * Диагностический эндпоинт, предназначен для сохранения марок
   * @param request request
   * @return OK
   */
  createMarksUsingPOSTResponse(request: CreateMarksCmd): __Observable<__StrictHttpResponse<CreateMarksResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/marks/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CreateMarksResponse>;
      })
    );
  }
  /**
   * Диагностический эндпоинт, предназначен для сохранения марок
   * @param request request
   * @return OK
   */
  createMarksUsingPOST(request: CreateMarksCmd): __Observable<CreateMarksResponse> {
    return this.createMarksUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as CreateMarksResponse)
    );
  }

  /**
   * Создание отсканированной марки ЧЗ при комплектовании, инвентаризации и т.п.
   * @param request request
   * @return OK
   */
  createScannedChzMarkUsingPOSTResponse(request: CreateScannedChzMarkCmd): __Observable<__StrictHttpResponse<CreateScannedChzMarkResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/marks/createScannedChzMark`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CreateScannedChzMarkResponse>;
      })
    );
  }
  /**
   * Создание отсканированной марки ЧЗ при комплектовании, инвентаризации и т.п.
   * @param request request
   * @return OK
   */
  createScannedChzMarkUsingPOST(request: CreateScannedChzMarkCmd): __Observable<CreateScannedChzMarkResponse> {
    return this.createScannedChzMarkUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as CreateScannedChzMarkResponse)
    );
  }

  /**
   * Поиск марок по параметрам, для внешних модулей
   * @param request request
   * @return OK
   */
  searchUsingPOSTResponse(request: MarkSearchQuery): __Observable<__StrictHttpResponse<MarkSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/marks/search`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<MarkSearchResult>;
      })
    );
  }
  /**
   * Поиск марок по параметрам, для внешних модулей
   * @param request request
   * @return OK
   */
  searchUsingPOST(request: MarkSearchQuery): __Observable<MarkSearchResult> {
    return this.searchUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as MarkSearchResult)
    );
  }

  /**
   * Диагностический эндпоинт, предназначен для изменения свойств марок
   * @param request request
   * @return OK
   */
  updateMarksUsingPOSTResponse(request: UpdateMarksCmd): __Observable<__StrictHttpResponse<UpdateMarksResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/marks/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UpdateMarksResponse>;
      })
    );
  }
  /**
   * Диагностический эндпоинт, предназначен для изменения свойств марок
   * @param request request
   * @return OK
   */
  updateMarksUsingPOST(request: UpdateMarksCmd): __Observable<UpdateMarksResponse> {
    return this.updateMarksUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as UpdateMarksResponse)
    );
  }
}

module MarksGatewayService {
}

export { MarksGatewayService }
