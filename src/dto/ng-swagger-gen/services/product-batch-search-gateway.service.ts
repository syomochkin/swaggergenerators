/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ProductBatchesSearchResult } from '../models/product-batches-search-result';
import { ProductBatchesSearchQuery } from '../models/product-batches-search-query';

/**
 * Product Batch Search Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ProductBatchSearchGatewayService extends __BaseService {
  static readonly searchUsingPOST3Path = '/v1/product-batch/search';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Search Batches
   * @param query query
   * @return OK
   */
  searchUsingPOST3Response(query: ProductBatchesSearchQuery): __Observable<__StrictHttpResponse<ProductBatchesSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/product-batch/search`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductBatchesSearchResult>;
      })
    );
  }
  /**
   * Search Batches
   * @param query query
   * @return OK
   */
  searchUsingPOST3(query: ProductBatchesSearchQuery): __Observable<ProductBatchesSearchResult> {
    return this.searchUsingPOST3Response(query).pipe(
      __map(_r => _r.body as ProductBatchesSearchResult)
    );
  }
}

module ProductBatchSearchGatewayService {
}

export { ProductBatchSearchGatewayService }
