/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { StocksSearchResult } from '../models/stocks-search-result';
import { StockAddressesAndProductIdsSearchQuery } from '../models/stock-addresses-and-product-ids-search-query';
import { UIStockSearchResult } from '../models/uistock-search-result';
import { UIStockSearchQuery } from '../models/uistock-search-query';
import { StockHierarchySearchQuery } from '../models/stock-hierarchy-search-query';

/**
 * Stock Search Gateway
 */
@Injectable({
  providedIn: 'root',
})
class StockSearchGatewayService extends __BaseService {
  static readonly searchUsingPOST4Path = '/v1/stock/addressAndProductIdsSearch';
  static readonly findAllUIUsingPOSTPath = '/v1/stock/findAllUI';
  static readonly searchUsingPOST5Path = '/v1/stock/search';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Search Stocks by Addresses and Product Ids
   * @param query query
   * @return OK
   */
  searchUsingPOST4Response(query: StockAddressesAndProductIdsSearchQuery): __Observable<__StrictHttpResponse<StocksSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/addressAndProductIdsSearch`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StocksSearchResult>;
      })
    );
  }
  /**
   * Search Stocks by Addresses and Product Ids
   * @param query query
   * @return OK
   */
  searchUsingPOST4(query: StockAddressesAndProductIdsSearchQuery): __Observable<StocksSearchResult> {
    return this.searchUsingPOST4Response(query).pipe(
      __map(_r => _r.body as StocksSearchResult)
    );
  }

  /**
   * Поиск для грида запасов
   * @param query query
   * @return OK
   */
  findAllUIUsingPOSTResponse(query: UIStockSearchQuery): __Observable<__StrictHttpResponse<UIStockSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/findAllUI`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<UIStockSearchResult>;
      })
    );
  }
  /**
   * Поиск для грида запасов
   * @param query query
   * @return OK
   */
  findAllUIUsingPOST(query: UIStockSearchQuery): __Observable<UIStockSearchResult> {
    return this.findAllUIUsingPOSTResponse(query).pipe(
      __map(_r => _r.body as UIStockSearchResult)
    );
  }

  /**
   * Search Stocks
   * @param query query
   * @return OK
   */
  searchUsingPOST5Response(query: StockHierarchySearchQuery): __Observable<__StrictHttpResponse<StocksSearchResult>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/search`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StocksSearchResult>;
      })
    );
  }
  /**
   * Search Stocks
   * @param query query
   * @return OK
   */
  searchUsingPOST5(query: StockHierarchySearchQuery): __Observable<StocksSearchResult> {
    return this.searchUsingPOST5Response(query).pipe(
      __map(_r => _r.body as StocksSearchResult)
    );
  }
}

module StockSearchGatewayService {
}

export { StockSearchGatewayService }
