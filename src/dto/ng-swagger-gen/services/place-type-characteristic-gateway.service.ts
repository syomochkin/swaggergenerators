/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceTypeCharacteristicDTO } from '../models/place-type-characteristic-dto';
import { AddPlaceTypeCommonCharacteristicDto } from '../models/add-place-type-common-characteristic-dto';
import { UpdatePlaceTypeCommonCharacteristicDto } from '../models/update-place-type-common-characteristic-dto';

/**
 * Place Type Characteristic Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceTypeCharacteristicGatewayService extends __BaseService {
  static readonly getPlaceTypeCharacteristicsUsingGETPath = '/v1/topology/placeTypes/{placeTypeId}/characteristics';
  static readonly addUsingPOSTPath = '/v1/topology/placeTypes/{placeTypeId}/characteristics';
  static readonly getPlaceTypeCharacteristicUsingGETPath = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
  static readonly updateUsingPUT2Path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
  static readonly removeUsingDELETEPath = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getPlaceTypeCharacteristics
   * @param placeTypeId placeTypeId
   * @return OK
   */
  getPlaceTypeCharacteristicsUsingGETResponse(placeTypeId: string): __Observable<__StrictHttpResponse<Array<PlaceTypeCharacteristicDTO>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/placeTypes/${encodeURIComponent(String(placeTypeId))}/characteristics`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceTypeCharacteristicDTO>>;
      })
    );
  }
  /**
   * getPlaceTypeCharacteristics
   * @param placeTypeId placeTypeId
   * @return OK
   */
  getPlaceTypeCharacteristicsUsingGET(placeTypeId: string): __Observable<Array<PlaceTypeCharacteristicDTO>> {
    return this.getPlaceTypeCharacteristicsUsingGETResponse(placeTypeId).pipe(
      __map(_r => _r.body as Array<PlaceTypeCharacteristicDTO>)
    );
  }

  /**
   * add
   * @param params The `PlaceTypeCharacteristicGatewayService.AddUsingPOSTParams` containing the following parameters:
   *
   * - `req`: req
   *
   * - `placeTypeId`: placeTypeId
   *
   * @return OK
   */
  addUsingPOSTResponse(params: PlaceTypeCharacteristicGatewayService.AddUsingPOSTParams): __Observable<__StrictHttpResponse<PlaceTypeCharacteristicDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.req;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/placeTypes/${encodeURIComponent(String(params.placeTypeId))}/characteristics`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceTypeCharacteristicDTO>;
      })
    );
  }
  /**
   * add
   * @param params The `PlaceTypeCharacteristicGatewayService.AddUsingPOSTParams` containing the following parameters:
   *
   * - `req`: req
   *
   * - `placeTypeId`: placeTypeId
   *
   * @return OK
   */
  addUsingPOST(params: PlaceTypeCharacteristicGatewayService.AddUsingPOSTParams): __Observable<PlaceTypeCharacteristicDTO> {
    return this.addUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as PlaceTypeCharacteristicDTO)
    );
  }

  /**
   * getPlaceTypeCharacteristic
   * @param params The `PlaceTypeCharacteristicGatewayService.GetPlaceTypeCharacteristicUsingGETParams` containing the following parameters:
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  getPlaceTypeCharacteristicUsingGETResponse(params: PlaceTypeCharacteristicGatewayService.GetPlaceTypeCharacteristicUsingGETParams): __Observable<__StrictHttpResponse<PlaceTypeCharacteristicDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/placeTypes/${encodeURIComponent(String(params.placeTypeId))}/characteristics/${encodeURIComponent(String(params.characteristicId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceTypeCharacteristicDTO>;
      })
    );
  }
  /**
   * getPlaceTypeCharacteristic
   * @param params The `PlaceTypeCharacteristicGatewayService.GetPlaceTypeCharacteristicUsingGETParams` containing the following parameters:
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  getPlaceTypeCharacteristicUsingGET(params: PlaceTypeCharacteristicGatewayService.GetPlaceTypeCharacteristicUsingGETParams): __Observable<PlaceTypeCharacteristicDTO> {
    return this.getPlaceTypeCharacteristicUsingGETResponse(params).pipe(
      __map(_r => _r.body as PlaceTypeCharacteristicDTO)
    );
  }

  /**
   * update
   * @param params The `PlaceTypeCharacteristicGatewayService.UpdateUsingPUT2Params` containing the following parameters:
   *
   * - `req`: req
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  updateUsingPUT2Response(params: PlaceTypeCharacteristicGatewayService.UpdateUsingPUT2Params): __Observable<__StrictHttpResponse<PlaceTypeCharacteristicDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.req;


    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/topology/placeTypes/${encodeURIComponent(String(params.placeTypeId))}/characteristics/${encodeURIComponent(String(params.characteristicId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<PlaceTypeCharacteristicDTO>;
      })
    );
  }
  /**
   * update
   * @param params The `PlaceTypeCharacteristicGatewayService.UpdateUsingPUT2Params` containing the following parameters:
   *
   * - `req`: req
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `characteristicId`: characteristicId
   *
   * @return OK
   */
  updateUsingPUT2(params: PlaceTypeCharacteristicGatewayService.UpdateUsingPUT2Params): __Observable<PlaceTypeCharacteristicDTO> {
    return this.updateUsingPUT2Response(params).pipe(
      __map(_r => _r.body as PlaceTypeCharacteristicDTO)
    );
  }

  /**
   * remove
   * @param params The `PlaceTypeCharacteristicGatewayService.RemoveUsingDELETEParams` containing the following parameters:
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `characteristicId`: characteristicId
   */
  removeUsingDELETEResponse(params: PlaceTypeCharacteristicGatewayService.RemoveUsingDELETEParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/topology/placeTypes/${encodeURIComponent(String(params.placeTypeId))}/characteristics/${encodeURIComponent(String(params.characteristicId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * remove
   * @param params The `PlaceTypeCharacteristicGatewayService.RemoveUsingDELETEParams` containing the following parameters:
   *
   * - `placeTypeId`: placeTypeId
   *
   * - `characteristicId`: characteristicId
   */
  removeUsingDELETE(params: PlaceTypeCharacteristicGatewayService.RemoveUsingDELETEParams): __Observable<null> {
    return this.removeUsingDELETEResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module PlaceTypeCharacteristicGatewayService {

  /**
   * Parameters for addUsingPOST
   */
  export interface AddUsingPOSTParams {

    /**
     * req
     */
    req: AddPlaceTypeCommonCharacteristicDto;

    /**
     * placeTypeId
     */
    placeTypeId: string;
  }

  /**
   * Parameters for getPlaceTypeCharacteristicUsingGET
   */
  export interface GetPlaceTypeCharacteristicUsingGETParams {

    /**
     * placeTypeId
     */
    placeTypeId: string;

    /**
     * characteristicId
     */
    characteristicId: string;
  }

  /**
   * Parameters for updateUsingPUT2
   */
  export interface UpdateUsingPUT2Params {

    /**
     * req
     */
    req: UpdatePlaceTypeCommonCharacteristicDto;

    /**
     * placeTypeId
     */
    placeTypeId: string;

    /**
     * characteristicId
     */
    characteristicId: string;
  }

  /**
   * Parameters for removeUsingDELETE
   */
  export interface RemoveUsingDELETEParams {

    /**
     * placeTypeId
     */
    placeTypeId: string;

    /**
     * characteristicId
     */
    characteristicId: string;
  }
}

export { PlaceTypeCharacteristicGatewayService }
