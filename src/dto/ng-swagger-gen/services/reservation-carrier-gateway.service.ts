/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ReserveCarrierForProcessResponse } from '../models/reserve-carrier-for-process-response';
import { ReserveCarrierForProcessCmd } from '../models/reserve-carrier-for-process-cmd';
import { DropCarrierReservationForProcessResponse } from '../models/drop-carrier-reservation-for-process-response';
import { DropCarrierReservationForProcessCmd } from '../models/drop-carrier-reservation-for-process-cmd';

/**
 * Reservation Carrier Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ReservationCarrierGatewayService extends __BaseService {
  static readonly reserveForProcessUsingPOSTPath = '/v1/stock/carriers/reservation/for-process';
  static readonly dropReservationForProcessUsingDELETEPath = '/v1/stock/carriers/reservation/for-process';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Резервирование носителя под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  reserveForProcessUsingPOSTResponse(cmd: ReserveCarrierForProcessCmd): __Observable<__StrictHttpResponse<ReserveCarrierForProcessResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carriers/reservation/for-process`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReserveCarrierForProcessResponse>;
      })
    );
  }
  /**
   * Резервирование носителя под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  reserveForProcessUsingPOST(cmd: ReserveCarrierForProcessCmd): __Observable<ReserveCarrierForProcessResponse> {
    return this.reserveForProcessUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ReserveCarrierForProcessResponse)
    );
  }

  /**
   * Сброс резерва носителя под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  dropReservationForProcessUsingDELETEResponse(cmd: DropCarrierReservationForProcessCmd): __Observable<__StrictHttpResponse<DropCarrierReservationForProcessResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/carriers/reservation/for-process`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<DropCarrierReservationForProcessResponse>;
      })
    );
  }
  /**
   * Сброс резерва носителя под процесс
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  dropReservationForProcessUsingDELETE(cmd: DropCarrierReservationForProcessCmd): __Observable<DropCarrierReservationForProcessResponse> {
    return this.dropReservationForProcessUsingDELETEResponse(cmd).pipe(
      __map(_r => _r.body as DropCarrierReservationForProcessResponse)
    );
  }
}

module ReservationCarrierGatewayService {
}

export { ReservationCarrierGatewayService }
