/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { OutboundOrderCmd } from '../models/outbound-order-cmd';

/**
 * Tool Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ToolGatewayService extends __BaseService {
  static readonly outboundOrderUsingPOST1Path = '/v1/tool/pushOrder';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Отгрузка заказа
   *
   * Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
   * @param cmd cmd
   */
  outboundOrderUsingPOST1Response(cmd: OutboundOrderCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/tool/pushOrder`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Отгрузка заказа
   *
   * Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
   * @param cmd cmd
   */
  outboundOrderUsingPOST1(cmd: OutboundOrderCmd): __Observable<null> {
    return this.outboundOrderUsingPOST1Response(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module ToolGatewayService {
}

export { ToolGatewayService }
