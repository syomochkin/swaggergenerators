/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { PlaceStatusDto } from '../models/place-status-dto';

/**
 * Place Status Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceStatusGatewayService extends __BaseService {
  static readonly getPlaceStatusesUsingGETPath = '/v1/topology/placeStatuses';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getPlaceStatuses
   * @return OK
   */
  getPlaceStatusesUsingGETResponse(): __Observable<__StrictHttpResponse<Array<PlaceStatusDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/placeStatuses`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<PlaceStatusDto>>;
      })
    );
  }
  /**
   * getPlaceStatuses
   * @return OK
   */
  getPlaceStatusesUsingGET(): __Observable<Array<PlaceStatusDto>> {
    return this.getPlaceStatusesUsingGETResponse().pipe(
      __map(_r => _r.body as Array<PlaceStatusDto>)
    );
  }
}

module PlaceStatusGatewayService {
}

export { PlaceStatusGatewayService }
