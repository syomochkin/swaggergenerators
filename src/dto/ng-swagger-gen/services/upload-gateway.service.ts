/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CarrierUploadResponse } from '../models/carrier-upload-response';

/**
 * Upload Gateway
 */
@Injectable({
  providedIn: 'root',
})
class UploadGatewayService extends __BaseService {
  static readonly clusterDeliveryReservationRouteUsingPOSTPath = '/v1/cluster-delivery-reservation-route/upload';
  static readonly uploadProductBatchesUsingPOSTPath = '/v1/product-batch/upload';
  static readonly routeStartRulesUploadUsingPOSTPath = '/v1/route-start-rules/upload';
  static readonly uploadRouteStrategyUsingPOST1Path = '/v1/route-strategy/upload';
  static readonly uploadRouteStrategyUsingPOSTPath = '/v1/route-strategy/upload-strategy/{strategyId}';
  static readonly carrierTypesUploadUsingPOSTPath = '/v1/stock/carrier-types/upload';
  static readonly carriersUploadUsingPOSTPath = '/v1/stock/carriers/upload';
  static readonly stocksUploadUsingPOSTPath = '/v1/stock/upload';
  static readonly placesUploadUsingPOSTPath = '/v1/topology/places/upload';
  static readonly placeTypesUploadUsingPOSTPath = '/v1/topology/sites/{siteId}/placeTypes/upload';
  static readonly zoneTypesUploadUsingPOSTPath = '/v1/topology/zone-types/upload';
  static readonly zonesUploadUsingPOSTPath = '/v1/topology/zones/upload';
  static readonly pickingZonesUploadUsingPOSTPath = '/v1/topology/zones/upload/ok';
  static readonly storageZonesUploadUsingPOSTPath = '/v1/topology/zones/upload/su';
  static readonly uploadWriteOffSettingsUsingPOSTPath = '/v1/write-off-settings/upload';
  static readonly uploadZoneCharacteristicsUsingPOSTPath = '/v1/zone-characteristics/upload';
  static readonly bindToRouteStrategyUsingPOSTPath = '/v1/zone-route-strategy/upload';
  static readonly zoneTransferRulesUploadUsingPOSTPath = '/v1/zone-transfer-rules/upload';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос на загрузку файла со змейкой по буферам отгрузки
   *
   * uploadClusterDeliveryReservationRouteFile, siteCode - обязательные поля
   * @param params The `UploadGatewayService.ClusterDeliveryReservationRouteUsingPOSTParams` containing the following parameters:
   *
   * - `uploadClusterDeliveryReservationRouteFile`: uploadClusterDeliveryReservationRouteFile
   *
   * - `siteCode`: siteCode
   */
  clusterDeliveryReservationRouteUsingPOSTResponse(params: UploadGatewayService.ClusterDeliveryReservationRouteUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.uploadClusterDeliveryReservationRouteFile != null) { __formData.append('uploadClusterDeliveryReservationRouteFile', params.uploadClusterDeliveryReservationRouteFile as string | Blob);}
    if (params.siteCode != null) { __formData.append('siteCode', params.siteCode as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/cluster-delivery-reservation-route/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла со змейкой по буферам отгрузки
   *
   * uploadClusterDeliveryReservationRouteFile, siteCode - обязательные поля
   * @param params The `UploadGatewayService.ClusterDeliveryReservationRouteUsingPOSTParams` containing the following parameters:
   *
   * - `uploadClusterDeliveryReservationRouteFile`: uploadClusterDeliveryReservationRouteFile
   *
   * - `siteCode`: siteCode
   */
  clusterDeliveryReservationRouteUsingPOST(params: UploadGatewayService.ClusterDeliveryReservationRouteUsingPOSTParams): __Observable<null> {
    return this.clusterDeliveryReservationRouteUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с партиями товара
   *
   * batchesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.UploadProductBatchesUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `batchesFile`: batchesFile
   */
  uploadProductBatchesUsingPOSTResponse(params: UploadGatewayService.UploadProductBatchesUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    if (params.batchesFile != null) { __formData.append('batchesFile', params.batchesFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/product-batch/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с партиями товара
   *
   * batchesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.UploadProductBatchesUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `batchesFile`: batchesFile
   */
  uploadProductBatchesUsingPOST(params: UploadGatewayService.UploadProductBatchesUsingPOSTParams): __Observable<null> {
    return this.uploadProductBatchesUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла правил мест старта
   *
   * routeStartRuleFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.RouteStartRulesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `routeStartRuleFile`: routeStartRuleFile
   */
  routeStartRulesUploadUsingPOSTResponse(params: UploadGatewayService.RouteStartRulesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    if (params.routeStartRuleFile != null) { __formData.append('routeStartRuleFile', params.routeStartRuleFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/route-start-rules/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла правил мест старта
   *
   * routeStartRuleFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.RouteStartRulesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `routeStartRuleFile`: routeStartRuleFile
   */
  routeStartRulesUploadUsingPOST(params: UploadGatewayService.RouteStartRulesUploadUsingPOSTParams): __Observable<null> {
    return this.routeStartRulesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с приоритетом обхода секций
   *
   * routeStrategyFile, siteCode - обязательные поля
   * @param params The `UploadGatewayService.UploadRouteStrategyUsingPOST1Params` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `routeStrategyFile`: routeStrategyFile
   */
  uploadRouteStrategyUsingPOST1Response(params: UploadGatewayService.UploadRouteStrategyUsingPOST1Params): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.siteCode != null) { __formData.append('siteCode', params.siteCode as string | Blob);}
    if (params.routeStrategyFile != null) { __formData.append('routeStrategyFile', params.routeStrategyFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/route-strategy/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с приоритетом обхода секций
   *
   * routeStrategyFile, siteCode - обязательные поля
   * @param params The `UploadGatewayService.UploadRouteStrategyUsingPOST1Params` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `routeStrategyFile`: routeStrategyFile
   */
  uploadRouteStrategyUsingPOST1(params: UploadGatewayService.UploadRouteStrategyUsingPOST1Params): __Observable<null> {
    return this.uploadRouteStrategyUsingPOST1Response(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Новый запрос на загрузку файла с приоритетом обхода секций
   *
   * routeStrategyFile, strategyId - обязательные поля
   * @param params The `UploadGatewayService.UploadRouteStrategyUsingPOSTParams` containing the following parameters:
   *
   * - `strategyId`: strategyId
   *
   * - `routeStrategyFile`: routeStrategyFile
   */
  uploadRouteStrategyUsingPOSTResponse(params: UploadGatewayService.UploadRouteStrategyUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;

    if (params.routeStrategyFile != null) { __formData.append('routeStrategyFile', params.routeStrategyFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/route-strategy/upload-strategy/${encodeURIComponent(String(params.strategyId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Новый запрос на загрузку файла с приоритетом обхода секций
   *
   * routeStrategyFile, strategyId - обязательные поля
   * @param params The `UploadGatewayService.UploadRouteStrategyUsingPOSTParams` containing the following parameters:
   *
   * - `strategyId`: strategyId
   *
   * - `routeStrategyFile`: routeStrategyFile
   */
  uploadRouteStrategyUsingPOST(params: UploadGatewayService.UploadRouteStrategyUsingPOSTParams): __Observable<null> {
    return this.uploadRouteStrategyUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с типами носителя
   *
   * carrierTypeFile - обязательные поля
   * @param carrierTypeFile carrierTypeFile
   */
  carrierTypesUploadUsingPOSTResponse(carrierTypeFile: Blob): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (carrierTypeFile != null) { __formData.append('carrierTypeFile', carrierTypeFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carrier-types/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с типами носителя
   *
   * carrierTypeFile - обязательные поля
   * @param carrierTypeFile carrierTypeFile
   */
  carrierTypesUploadUsingPOST(carrierTypeFile: Blob): __Observable<null> {
    return this.carrierTypesUploadUsingPOSTResponse(carrierTypeFile).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с носителями
   *
   * carrierFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.CarriersUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `carrierFile`: carrierFile
   *
   * @return OK
   */
  carriersUploadUsingPOSTResponse(params: UploadGatewayService.CarriersUploadUsingPOSTParams): __Observable<__StrictHttpResponse<CarrierUploadResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    if (params.carrierFile != null) { __formData.append('carrierFile', params.carrierFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carriers/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierUploadResponse>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с носителями
   *
   * carrierFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.CarriersUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `carrierFile`: carrierFile
   *
   * @return OK
   */
  carriersUploadUsingPOST(params: UploadGatewayService.CarriersUploadUsingPOSTParams): __Observable<CarrierUploadResponse> {
    return this.carriersUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as CarrierUploadResponse)
    );
  }

  /**
   * Запрос на загрузку файла с запасами
   *
   * stocksFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.StocksUploadUsingPOSTParams` containing the following parameters:
   *
   * - `stocksFile`: stocksFile
   *
   * - `siteId`: siteId
   */
  stocksUploadUsingPOSTResponse(params: UploadGatewayService.StocksUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.stocksFile != null) { __formData.append('stocksFile', params.stocksFile as string | Blob);}
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с запасами
   *
   * stocksFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.StocksUploadUsingPOSTParams` containing the following parameters:
   *
   * - `stocksFile`: stocksFile
   *
   * - `siteId`: siteId
   */
  stocksUploadUsingPOST(params: UploadGatewayService.StocksUploadUsingPOSTParams): __Observable<null> {
    return this.stocksUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с топологией
   *
   * placesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.PlacesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placesFile`: placesFile
   */
  placesUploadUsingPOSTResponse(params: UploadGatewayService.PlacesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    if (params.placesFile != null) { __formData.append('placesFile', params.placesFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с топологией
   *
   * placesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.PlacesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placesFile`: placesFile
   */
  placesUploadUsingPOST(params: UploadGatewayService.PlacesUploadUsingPOSTParams): __Observable<null> {
    return this.placesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла типов мест с характеристиками
   *
   * placesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.PlaceTypesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placesFile`: placesFile
   */
  placeTypesUploadUsingPOSTResponse(params: UploadGatewayService.PlaceTypesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;

    if (params.placesFile != null) { __formData.append('placesFile', params.placesFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/sites/${encodeURIComponent(String(params.siteId))}/placeTypes/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла типов мест с характеристиками
   *
   * placesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.PlaceTypesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `placesFile`: placesFile
   */
  placeTypesUploadUsingPOST(params: UploadGatewayService.PlaceTypesUploadUsingPOSTParams): __Observable<null> {
    return this.placeTypesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с типами зон
   *
   * zoneTypeFile - обязательные поля
   * @param zoneTypeFile zoneTypeFile
   */
  zoneTypesUploadUsingPOSTResponse(zoneTypeFile: Blob): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (zoneTypeFile != null) { __formData.append('zoneTypeFile', zoneTypeFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zone-types/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с типами зон
   *
   * zoneTypeFile - обязательные поля
   * @param zoneTypeFile zoneTypeFile
   */
  zoneTypesUploadUsingPOST(zoneTypeFile: Blob): __Observable<null> {
    return this.zoneTypesUploadUsingPOSTResponse(zoneTypeFile).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с зонами
   *
   * zonesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.ZonesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zonesFile`: zonesFile
   *
   * - `siteId`: siteId
   */
  zonesUploadUsingPOSTResponse(params: UploadGatewayService.ZonesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.zonesFile != null) { __formData.append('zonesFile', params.zonesFile as string | Blob);}
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zones/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с зонами
   *
   * zonesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.ZonesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zonesFile`: zonesFile
   *
   * - `siteId`: siteId
   */
  zonesUploadUsingPOST(params: UploadGatewayService.ZonesUploadUsingPOSTParams): __Observable<null> {
    return this.zonesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с областями комплектования
   *
   * zonesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.PickingZonesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zonesFile`: zonesFile
   *
   * - `siteId`: siteId
   */
  pickingZonesUploadUsingPOSTResponse(params: UploadGatewayService.PickingZonesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.zonesFile != null) { __formData.append('zonesFile', params.zonesFile as string | Blob);}
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zones/upload/ok`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с областями комплектования
   *
   * zonesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.PickingZonesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zonesFile`: zonesFile
   *
   * - `siteId`: siteId
   */
  pickingZonesUploadUsingPOST(params: UploadGatewayService.PickingZonesUploadUsingPOSTParams): __Observable<null> {
    return this.pickingZonesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла со складскими участками
   *
   * zonesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.StorageZonesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zonesFile`: zonesFile
   *
   * - `siteId`: siteId
   */
  storageZonesUploadUsingPOSTResponse(params: UploadGatewayService.StorageZonesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.zonesFile != null) { __formData.append('zonesFile', params.zonesFile as string | Blob);}
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/zones/upload/su`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла со складскими участками
   *
   * zonesFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.StorageZonesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zonesFile`: zonesFile
   *
   * - `siteId`: siteId
   */
  storageZonesUploadUsingPOST(params: UploadGatewayService.StorageZonesUploadUsingPOSTParams): __Observable<null> {
    return this.storageZonesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с настройками списания
   *
   * settingsFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.UploadWriteOffSettingsUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `settingsFile`: settingsFile
   */
  uploadWriteOffSettingsUsingPOSTResponse(params: UploadGatewayService.UploadWriteOffSettingsUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    if (params.settingsFile != null) { __formData.append('settingsFile', params.settingsFile as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/write-off-settings/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с настройками списания
   *
   * settingsFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.UploadWriteOffSettingsUsingPOSTParams` containing the following parameters:
   *
   * - `siteId`: siteId
   *
   * - `settingsFile`: settingsFile
   */
  uploadWriteOffSettingsUsingPOST(params: UploadGatewayService.UploadWriteOffSettingsUsingPOSTParams): __Observable<null> {
    return this.uploadWriteOffSettingsUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с настройкой приоритетов секторов комплектования
   *
   * zoneCharacteristics, siteCode - обязательные поля
   * @param params The `UploadGatewayService.UploadZoneCharacteristicsUsingPOSTParams` containing the following parameters:
   *
   * - `zoneCharacteristics`: zoneCharacteristics
   *
   * - `siteCode`: siteCode
   */
  uploadZoneCharacteristicsUsingPOSTResponse(params: UploadGatewayService.UploadZoneCharacteristicsUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.zoneCharacteristics != null) { __formData.append('zoneCharacteristics', params.zoneCharacteristics as string | Blob);}
    if (params.siteCode != null) { __formData.append('siteCode', params.siteCode as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/zone-characteristics/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с настройкой приоритетов секторов комплектования
   *
   * zoneCharacteristics, siteCode - обязательные поля
   * @param params The `UploadGatewayService.UploadZoneCharacteristicsUsingPOSTParams` containing the following parameters:
   *
   * - `zoneCharacteristics`: zoneCharacteristics
   *
   * - `siteCode`: siteCode
   */
  uploadZoneCharacteristicsUsingPOST(params: UploadGatewayService.UploadZoneCharacteristicsUsingPOSTParams): __Observable<null> {
    return this.uploadZoneCharacteristicsUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла с привязкой стратегии обхода змейки
   *
   * zoneRouteStrategy, siteCode - обязательные поля
   * @param params The `UploadGatewayService.BindToRouteStrategyUsingPOSTParams` containing the following parameters:
   *
   * - `zoneRouteStrategy`: zoneRouteStrategy
   *
   * - `siteCode`: siteCode
   */
  bindToRouteStrategyUsingPOSTResponse(params: UploadGatewayService.BindToRouteStrategyUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.zoneRouteStrategy != null) { __formData.append('zoneRouteStrategy', params.zoneRouteStrategy as string | Blob);}
    if (params.siteCode != null) { __formData.append('siteCode', params.siteCode as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/zone-route-strategy/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла с привязкой стратегии обхода змейки
   *
   * zoneRouteStrategy, siteCode - обязательные поля
   * @param params The `UploadGatewayService.BindToRouteStrategyUsingPOSTParams` containing the following parameters:
   *
   * - `zoneRouteStrategy`: zoneRouteStrategy
   *
   * - `siteCode`: siteCode
   */
  bindToRouteStrategyUsingPOST(params: UploadGatewayService.BindToRouteStrategyUsingPOSTParams): __Observable<null> {
    return this.bindToRouteStrategyUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Запрос на загрузку файла правил переходов между местами
   *
   * zoneTransferRuleFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.ZoneTransferRulesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zoneTransferRuleFile`: zoneTransferRuleFile
   *
   * - `siteId`: siteId
   */
  zoneTransferRulesUploadUsingPOSTResponse(params: UploadGatewayService.ZoneTransferRulesUploadUsingPOSTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (params.zoneTransferRuleFile != null) { __formData.append('zoneTransferRuleFile', params.zoneTransferRuleFile as string | Blob);}
    if (params.siteId != null) { __formData.append('siteId', params.siteId as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/zone-transfer-rules/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на загрузку файла правил переходов между местами
   *
   * zoneTransferRuleFile, siteId - обязательные поля
   * @param params The `UploadGatewayService.ZoneTransferRulesUploadUsingPOSTParams` containing the following parameters:
   *
   * - `zoneTransferRuleFile`: zoneTransferRuleFile
   *
   * - `siteId`: siteId
   */
  zoneTransferRulesUploadUsingPOST(params: UploadGatewayService.ZoneTransferRulesUploadUsingPOSTParams): __Observable<null> {
    return this.zoneTransferRulesUploadUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module UploadGatewayService {

  /**
   * Parameters for clusterDeliveryReservationRouteUsingPOST
   */
  export interface ClusterDeliveryReservationRouteUsingPOSTParams {

    /**
     * uploadClusterDeliveryReservationRouteFile
     */
    uploadClusterDeliveryReservationRouteFile: Blob;

    /**
     * siteCode
     */
    siteCode: string;
  }

  /**
   * Parameters for uploadProductBatchesUsingPOST
   */
  export interface UploadProductBatchesUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * batchesFile
     */
    batchesFile: Blob;
  }

  /**
   * Parameters for routeStartRulesUploadUsingPOST
   */
  export interface RouteStartRulesUploadUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * routeStartRuleFile
     */
    routeStartRuleFile: Blob;
  }

  /**
   * Parameters for uploadRouteStrategyUsingPOST1
   */
  export interface UploadRouteStrategyUsingPOST1Params {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * routeStrategyFile
     */
    routeStrategyFile: Blob;
  }

  /**
   * Parameters for uploadRouteStrategyUsingPOST
   */
  export interface UploadRouteStrategyUsingPOSTParams {

    /**
     * strategyId
     */
    strategyId: string;

    /**
     * routeStrategyFile
     */
    routeStrategyFile: Blob;
  }

  /**
   * Parameters for carriersUploadUsingPOST
   */
  export interface CarriersUploadUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * carrierFile
     */
    carrierFile: Blob;
  }

  /**
   * Parameters for stocksUploadUsingPOST
   */
  export interface StocksUploadUsingPOSTParams {

    /**
     * stocksFile
     */
    stocksFile: Blob;

    /**
     * siteId
     */
    siteId: string;
  }

  /**
   * Parameters for placesUploadUsingPOST
   */
  export interface PlacesUploadUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * placesFile
     */
    placesFile: Blob;
  }

  /**
   * Parameters for placeTypesUploadUsingPOST
   */
  export interface PlaceTypesUploadUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * placesFile
     */
    placesFile: Blob;
  }

  /**
   * Parameters for zonesUploadUsingPOST
   */
  export interface ZonesUploadUsingPOSTParams {

    /**
     * zonesFile
     */
    zonesFile: Blob;

    /**
     * siteId
     */
    siteId: string;
  }

  /**
   * Parameters for pickingZonesUploadUsingPOST
   */
  export interface PickingZonesUploadUsingPOSTParams {

    /**
     * zonesFile
     */
    zonesFile: Blob;

    /**
     * siteId
     */
    siteId: string;
  }

  /**
   * Parameters for storageZonesUploadUsingPOST
   */
  export interface StorageZonesUploadUsingPOSTParams {

    /**
     * zonesFile
     */
    zonesFile: Blob;

    /**
     * siteId
     */
    siteId: string;
  }

  /**
   * Parameters for uploadWriteOffSettingsUsingPOST
   */
  export interface UploadWriteOffSettingsUsingPOSTParams {

    /**
     * siteId
     */
    siteId: string;

    /**
     * settingsFile
     */
    settingsFile: Blob;
  }

  /**
   * Parameters for uploadZoneCharacteristicsUsingPOST
   */
  export interface UploadZoneCharacteristicsUsingPOSTParams {

    /**
     * zoneCharacteristics
     */
    zoneCharacteristics: Blob;

    /**
     * siteCode
     */
    siteCode: string;
  }

  /**
   * Parameters for bindToRouteStrategyUsingPOST
   */
  export interface BindToRouteStrategyUsingPOSTParams {

    /**
     * zoneRouteStrategy
     */
    zoneRouteStrategy: Blob;

    /**
     * siteCode
     */
    siteCode: string;
  }

  /**
   * Parameters for zoneTransferRulesUploadUsingPOST
   */
  export interface ZoneTransferRulesUploadUsingPOSTParams {

    /**
     * zoneTransferRuleFile
     */
    zoneTransferRuleFile: Blob;

    /**
     * siteId
     */
    siteId: string;
  }
}

export { UploadGatewayService }
