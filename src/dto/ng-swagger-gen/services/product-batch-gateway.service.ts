/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ProductBatchDto } from '../models/product-batch-dto';
import { AssignProductBatchCmd } from '../models/assign-product-batch-cmd';
import { ProductBatchBalanceDifferenceDto } from '../models/product-batch-balance-difference-dto';
import { ConfirmProductBatchCmd } from '../models/confirm-product-batch-cmd';

/**
 * Product Batch Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ProductBatchGatewayService extends __BaseService {
  static readonly getAllUsingGET1Path = '/v1/product-batch';
  static readonly assignBatchUsingPOSTPath = '/v1/product-batch';
  static readonly getBatchBalanceDifferenceUsingGETPath = '/v1/product-batch/balance/{productBatchId}/difference';
  static readonly fixBatchBalanceDifferenceUsingPOSTPath = '/v1/product-batch/balance/{productBatchId}/fix';
  static readonly confirmBatchUsingPOSTPath = '/v1/product-batch/confirm';
  static readonly synchronizeBatchUsingPOSTPath = '/v1/product-batch/sync/{productBatchId}';
  static readonly getByIdUsingGET1Path = '/v1/product-batch/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getAll
   * @param params The `ProductBatchGatewayService.GetAllUsingGET1Params` containing the following parameters:
   *
   * - `vendorCode`: vendorCode
   *
   * - `siteCode`: siteCode
   *
   * - `productId`: productId
   *
   * - `inboundId`: inboundId
   *
   * - `type`: type
   *
   * - `manufactureTime`: manufactureTime
   *
   * @return OK
   */
  getAllUsingGET1Response(params: ProductBatchGatewayService.GetAllUsingGET1Params): __Observable<__StrictHttpResponse<Array<ProductBatchDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.vendorCode != null) __params = __params.set('vendorCode', params.vendorCode.toString());
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    if (params.productId != null) __params = __params.set('productId', params.productId.toString());
    if (params.inboundId != null) __params = __params.set('inboundId', params.inboundId.toString());
    if (params.type != null) __params = __params.set('type', params.type.toString());
    if (params.manufactureTime != null) __params = __params.set('manufactureTime', params.manufactureTime.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/product-batch`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ProductBatchDto>>;
      })
    );
  }
  /**
   * getAll
   * @param params The `ProductBatchGatewayService.GetAllUsingGET1Params` containing the following parameters:
   *
   * - `vendorCode`: vendorCode
   *
   * - `siteCode`: siteCode
   *
   * - `productId`: productId
   *
   * - `inboundId`: inboundId
   *
   * - `type`: type
   *
   * - `manufactureTime`: manufactureTime
   *
   * @return OK
   */
  getAllUsingGET1(params: ProductBatchGatewayService.GetAllUsingGET1Params): __Observable<Array<ProductBatchDto>> {
    return this.getAllUsingGET1Response(params).pipe(
      __map(_r => _r.body as Array<ProductBatchDto>)
    );
  }

  /**
   * assignBatch
   * @param cmd cmd
   * @return OK
   */
  assignBatchUsingPOSTResponse(cmd: AssignProductBatchCmd): __Observable<__StrictHttpResponse<ProductBatchDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/product-batch`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductBatchDto>;
      })
    );
  }
  /**
   * assignBatch
   * @param cmd cmd
   * @return OK
   */
  assignBatchUsingPOST(cmd: AssignProductBatchCmd): __Observable<ProductBatchDto> {
    return this.assignBatchUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ProductBatchDto)
    );
  }

  /**
   * getBatchBalanceDifference
   * @param productBatchId productBatchId
   * @return OK
   */
  getBatchBalanceDifferenceUsingGETResponse(productBatchId: string): __Observable<__StrictHttpResponse<ProductBatchBalanceDifferenceDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/product-batch/balance/${encodeURIComponent(String(productBatchId))}/difference`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductBatchBalanceDifferenceDto>;
      })
    );
  }
  /**
   * getBatchBalanceDifference
   * @param productBatchId productBatchId
   * @return OK
   */
  getBatchBalanceDifferenceUsingGET(productBatchId: string): __Observable<ProductBatchBalanceDifferenceDto> {
    return this.getBatchBalanceDifferenceUsingGETResponse(productBatchId).pipe(
      __map(_r => _r.body as ProductBatchBalanceDifferenceDto)
    );
  }

  /**
   * fixBatchBalanceDifference
   * @param productBatchId productBatchId
   */
  fixBatchBalanceDifferenceUsingPOSTResponse(productBatchId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/product-batch/balance/${encodeURIComponent(String(productBatchId))}/fix`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * fixBatchBalanceDifference
   * @param productBatchId productBatchId
   */
  fixBatchBalanceDifferenceUsingPOST(productBatchId: string): __Observable<null> {
    return this.fixBatchBalanceDifferenceUsingPOSTResponse(productBatchId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * "Обеление" вида запаса
   * @param confirmProductBatchCmd confirmProductBatchCmd
   */
  confirmBatchUsingPOSTResponse(confirmProductBatchCmd: ConfirmProductBatchCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = confirmProductBatchCmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/product-batch/confirm`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * "Обеление" вида запаса
   * @param confirmProductBatchCmd confirmProductBatchCmd
   */
  confirmBatchUsingPOST(confirmProductBatchCmd: ConfirmProductBatchCmd): __Observable<null> {
    return this.confirmBatchUsingPOSTResponse(confirmProductBatchCmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * synchronizeBatch
   * @param productBatchId productBatchId
   */
  synchronizeBatchUsingPOSTResponse(productBatchId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/product-batch/sync/${encodeURIComponent(String(productBatchId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * synchronizeBatch
   * @param productBatchId productBatchId
   */
  synchronizeBatchUsingPOST(productBatchId: string): __Observable<null> {
    return this.synchronizeBatchUsingPOSTResponse(productBatchId).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * getById
   * @param id id
   * @return OK
   */
  getByIdUsingGET1Response(id: string): __Observable<__StrictHttpResponse<ProductBatchDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/product-batch/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ProductBatchDto>;
      })
    );
  }
  /**
   * getById
   * @param id id
   * @return OK
   */
  getByIdUsingGET1(id: string): __Observable<ProductBatchDto> {
    return this.getByIdUsingGET1Response(id).pipe(
      __map(_r => _r.body as ProductBatchDto)
    );
  }
}

module ProductBatchGatewayService {

  /**
   * Parameters for getAllUsingGET1
   */
  export interface GetAllUsingGET1Params {

    /**
     * vendorCode
     */
    vendorCode: string;

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * productId
     */
    productId: string;

    /**
     * inboundId
     */
    inboundId: string;

    /**
     * type
     */
    type?: 'REGULAR' | 'RETURN';

    /**
     * manufactureTime
     */
    manufactureTime?: number;
  }
}

export { ProductBatchGatewayService }
