/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { AuditCarrierRecordSearchResponse } from '../models/audit-carrier-record-search-response';
import { AuditCarrierRecordSearchQuery } from '../models/audit-carrier-record-search-query';

/**
 * Carrier Audit Gateway
 */
@Injectable({
  providedIn: 'root',
})
class CarrierAuditGatewayService extends __BaseService {
  static readonly findAllUsingPOSTPath = '/v1/audit/carrier/findAll';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * История движения носителей
   * @param searchQuery searchQuery
   * @return OK
   */
  findAllUsingPOSTResponse(searchQuery: AuditCarrierRecordSearchQuery): __Observable<__StrictHttpResponse<AuditCarrierRecordSearchResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = searchQuery;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/audit/carrier/findAll`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<AuditCarrierRecordSearchResponse>;
      })
    );
  }
  /**
   * История движения носителей
   * @param searchQuery searchQuery
   * @return OK
   */
  findAllUsingPOST(searchQuery: AuditCarrierRecordSearchQuery): __Observable<AuditCarrierRecordSearchResponse> {
    return this.findAllUsingPOSTResponse(searchQuery).pipe(
      __map(_r => _r.body as AuditCarrierRecordSearchResponse)
    );
  }
}

module CarrierAuditGatewayService {
}

export { CarrierAuditGatewayService }
