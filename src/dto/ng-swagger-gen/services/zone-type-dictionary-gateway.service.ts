/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ZoneTypeDto } from '../models/zone-type-dto';

/**
 * Zone Type Dictionary Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ZoneTypeDictionaryGatewayService extends __BaseService {
  static readonly getZoneTypesUsingGET1Path = '/v1/topology/dictionary/zoneTypes';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getZoneTypes
   * @return OK
   */
  getZoneTypesUsingGET1Response(): __Observable<__StrictHttpResponse<Array<ZoneTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/dictionary/zoneTypes`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ZoneTypeDto>>;
      })
    );
  }
  /**
   * getZoneTypes
   * @return OK
   */
  getZoneTypesUsingGET1(): __Observable<Array<ZoneTypeDto>> {
    return this.getZoneTypesUsingGET1Response().pipe(
      __map(_r => _r.body as Array<ZoneTypeDto>)
    );
  }
}

module ZoneTypeDictionaryGatewayService {
}

export { ZoneTypeDictionaryGatewayService }
