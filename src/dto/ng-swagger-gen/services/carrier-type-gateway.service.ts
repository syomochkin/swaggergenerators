/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CarrierTypeDto } from '../models/carrier-type-dto';
import { CreateCarrierTypeCmd } from '../models/create-carrier-type-cmd';
import { CodeExists } from '../models/code-exists';
import { CarrierTypeCodeExistsQuery } from '../models/carrier-type-code-exists-query';
import { UpdateCarrierTypeCmd } from '../models/update-carrier-type-cmd';

/**
 * Carrier Type Gateway
 */
@Injectable({
  providedIn: 'root',
})
class CarrierTypeGatewayService extends __BaseService {
  static readonly getCarrierTypesUsingGETPath = '/v1/stock/carrier-types';
  static readonly createUsingPOST1Path = '/v1/stock/carrier-types';
  static readonly codeExistsUsingPOSTPath = '/v1/stock/carrier-types/codeExists';
  static readonly updateUsingPUTPath = '/v1/stock/carrier-types/{carrierTypeId}';
  static readonly getCarrierTypeUsingGETPath = '/v1/stock/carrier-types/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * getCarrierTypes
   * @param code code
   * @return OK
   */
  getCarrierTypesUsingGETResponse(code?: Array<string>): __Observable<__StrictHttpResponse<Array<CarrierTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (code || []).forEach(val => {if (val != null) __params = __params.append('code', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/carrier-types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<CarrierTypeDto>>;
      })
    );
  }
  /**
   * getCarrierTypes
   * @param code code
   * @return OK
   */
  getCarrierTypesUsingGET(code?: Array<string>): __Observable<Array<CarrierTypeDto>> {
    return this.getCarrierTypesUsingGETResponse(code).pipe(
      __map(_r => _r.body as Array<CarrierTypeDto>)
    );
  }

  /**
   * create
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST1Response(cmd: CreateCarrierTypeCmd): __Observable<__StrictHttpResponse<CarrierTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carrier-types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierTypeDto>;
      })
    );
  }
  /**
   * create
   * @param cmd cmd
   * @return OK
   */
  createUsingPOST1(cmd: CreateCarrierTypeCmd): __Observable<CarrierTypeDto> {
    return this.createUsingPOST1Response(cmd).pipe(
      __map(_r => _r.body as CarrierTypeDto)
    );
  }

  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOSTResponse(query: CarrierTypeCodeExistsQuery): __Observable<__StrictHttpResponse<CodeExists>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = query;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/carrier-types/codeExists`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CodeExists>;
      })
    );
  }
  /**
   * codeExists
   * @param query query
   * @return OK
   */
  codeExistsUsingPOST(query: CarrierTypeCodeExistsQuery): __Observable<CodeExists> {
    return this.codeExistsUsingPOSTResponse(query).pipe(
      __map(_r => _r.body as CodeExists)
    );
  }

  /**
   * update
   * @param params The `CarrierTypeGatewayService.UpdateUsingPUTParams` containing the following parameters:
   *
   * - `cmd`: cmd
   *
   * - `carrierTypeId`: carrierTypeId
   *
   * @return OK
   */
  updateUsingPUTResponse(params: CarrierTypeGatewayService.UpdateUsingPUTParams): __Observable<__StrictHttpResponse<CarrierTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.cmd;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/carrier-types/${encodeURIComponent(String(params.carrierTypeId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierTypeDto>;
      })
    );
  }
  /**
   * update
   * @param params The `CarrierTypeGatewayService.UpdateUsingPUTParams` containing the following parameters:
   *
   * - `cmd`: cmd
   *
   * - `carrierTypeId`: carrierTypeId
   *
   * @return OK
   */
  updateUsingPUT(params: CarrierTypeGatewayService.UpdateUsingPUTParams): __Observable<CarrierTypeDto> {
    return this.updateUsingPUTResponse(params).pipe(
      __map(_r => _r.body as CarrierTypeDto)
    );
  }

  /**
   * getCarrierType
   * @param id id
   * @return OK
   */
  getCarrierTypeUsingGETResponse(id: string): __Observable<__StrictHttpResponse<CarrierTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/carrier-types/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CarrierTypeDto>;
      })
    );
  }
  /**
   * getCarrierType
   * @param id id
   * @return OK
   */
  getCarrierTypeUsingGET(id: string): __Observable<CarrierTypeDto> {
    return this.getCarrierTypeUsingGETResponse(id).pipe(
      __map(_r => _r.body as CarrierTypeDto)
    );
  }
}

module CarrierTypeGatewayService {

  /**
   * Parameters for updateUsingPUT
   */
  export interface UpdateUsingPUTParams {

    /**
     * cmd
     */
    cmd: UpdateCarrierTypeCmd;

    /**
     * carrierTypeId
     */
    carrierTypeId: string;
  }
}

export { CarrierTypeGatewayService }
