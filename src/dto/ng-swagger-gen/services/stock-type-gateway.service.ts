/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { StockTypeDto } from '../models/stock-type-dto';
import { ModifyStockTypeCmd } from '../models/modify-stock-type-cmd';
import { CodeExists } from '../models/code-exists';

/**
 * Stock Type Gateway
 */
@Injectable({
  providedIn: 'root',
})
class StockTypeGatewayService extends __BaseService {
  static readonly getStockTypesUsingGETPath = '/v1/stock/stock-types';
  static readonly createStockTypeUsingPOSTPath = '/v1/stock/stock-types';
  static readonly getStockTypeByCodeUsingGETPath = '/v1/stock/stock-types/code/{code}';
  static readonly codeExistsUsingGETPath = '/v1/stock/stock-types/codeExists/{code}';
  static readonly getStockTypeByIdUsingGETPath = '/v1/stock/stock-types/{id}';
  static readonly updateStockTypeUsingPUTPath = '/v1/stock/stock-types/{id}';
  static readonly archiveStockTypeUsingDELETEPath = '/v1/stock/stock-types/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос списка видов запаса
   * @return OK
   */
  getStockTypesUsingGETResponse(): __Observable<__StrictHttpResponse<Array<StockTypeDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/stock-types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<StockTypeDto>>;
      })
    );
  }
  /**
   * Запрос списка видов запаса
   * @return OK
   */
  getStockTypesUsingGET(): __Observable<Array<StockTypeDto>> {
    return this.getStockTypesUsingGETResponse().pipe(
      __map(_r => _r.body as Array<StockTypeDto>)
    );
  }

  /**
   * Создание нового справочника вида запаса
   *
   * code - обязательное поле
   * @param cmd cmd
   * @return OK
   */
  createStockTypeUsingPOSTResponse(cmd: ModifyStockTypeCmd): __Observable<__StrictHttpResponse<StockTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/stock-types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockTypeDto>;
      })
    );
  }
  /**
   * Создание нового справочника вида запаса
   *
   * code - обязательное поле
   * @param cmd cmd
   * @return OK
   */
  createStockTypeUsingPOST(cmd: ModifyStockTypeCmd): __Observable<StockTypeDto> {
    return this.createStockTypeUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as StockTypeDto)
    );
  }

  /**
   * Запрос вида запаса по коду
   * @param code code
   * @return OK
   */
  getStockTypeByCodeUsingGETResponse(code: string): __Observable<__StrictHttpResponse<StockTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/stock-types/code/${encodeURIComponent(String(code))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockTypeDto>;
      })
    );
  }
  /**
   * Запрос вида запаса по коду
   * @param code code
   * @return OK
   */
  getStockTypeByCodeUsingGET(code: string): __Observable<StockTypeDto> {
    return this.getStockTypeByCodeUsingGETResponse(code).pipe(
      __map(_r => _r.body as StockTypeDto)
    );
  }

  /**
   * Проверка существует ли вид запаса с указанным кодом
   * @param code code
   * @return OK
   */
  codeExistsUsingGETResponse(code: string): __Observable<__StrictHttpResponse<CodeExists>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/stock-types/codeExists/${encodeURIComponent(String(code))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<CodeExists>;
      })
    );
  }
  /**
   * Проверка существует ли вид запаса с указанным кодом
   * @param code code
   * @return OK
   */
  codeExistsUsingGET(code: string): __Observable<CodeExists> {
    return this.codeExistsUsingGETResponse(code).pipe(
      __map(_r => _r.body as CodeExists)
    );
  }

  /**
   * Запрос вида запаса по идентификатору
   * @param id id
   * @return OK
   */
  getStockTypeByIdUsingGETResponse(id: string): __Observable<__StrictHttpResponse<StockTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/stock-types/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockTypeDto>;
      })
    );
  }
  /**
   * Запрос вида запаса по идентификатору
   * @param id id
   * @return OK
   */
  getStockTypeByIdUsingGET(id: string): __Observable<StockTypeDto> {
    return this.getStockTypeByIdUsingGETResponse(id).pipe(
      __map(_r => _r.body as StockTypeDto)
    );
  }

  /**
   * Обновление вида запаса
   *
   * code - обязательное поле
   * @param params The `StockTypeGatewayService.UpdateStockTypeUsingPUTParams` containing the following parameters:
   *
   * - `id`: id
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateStockTypeUsingPUTResponse(params: StockTypeGatewayService.UpdateStockTypeUsingPUTParams): __Observable<__StrictHttpResponse<StockTypeDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/stock/stock-types/${encodeURIComponent(String(params.id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<StockTypeDto>;
      })
    );
  }
  /**
   * Обновление вида запаса
   *
   * code - обязательное поле
   * @param params The `StockTypeGatewayService.UpdateStockTypeUsingPUTParams` containing the following parameters:
   *
   * - `id`: id
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateStockTypeUsingPUT(params: StockTypeGatewayService.UpdateStockTypeUsingPUTParams): __Observable<StockTypeDto> {
    return this.updateStockTypeUsingPUTResponse(params).pipe(
      __map(_r => _r.body as StockTypeDto)
    );
  }

  /**
   * Установка признака заархивирован
   * @param id id
   */
  archiveStockTypeUsingDELETEResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/stock/stock-types/${encodeURIComponent(String(id))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Установка признака заархивирован
   * @param id id
   */
  archiveStockTypeUsingDELETE(id: string): __Observable<null> {
    return this.archiveStockTypeUsingDELETEResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module StockTypeGatewayService {

  /**
   * Parameters for updateStockTypeUsingPUT
   */
  export interface UpdateStockTypeUsingPUTParams {

    /**
     * id
     */
    id: string;

    /**
     * cmd
     */
    cmd: ModifyStockTypeCmd;
  }
}

export { StockTypeGatewayService }
