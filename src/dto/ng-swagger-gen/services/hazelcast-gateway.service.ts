/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { DeletePlacesFromHazelcastCmd } from '../models/delete-places-from-hazelcast-cmd';
import { InitialPopulateHazelcastCmd } from '../models/initial-populate-hazelcast-cmd';

/**
 * Hazelcast Gateway
 */
@Injectable({
  providedIn: 'root',
})
class HazelcastGatewayService extends __BaseService {
  static readonly deletePlacesUsingPOSTPath = '/v1/hazelcast/delete';
  static readonly initialPopulateUsingPOSTPath = '/v1/hazelcast/init';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Удаление мест из кэша для МРТ
   * @param cmd cmd
   */
  deletePlacesUsingPOSTResponse(cmd: DeletePlacesFromHazelcastCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/hazelcast/delete`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Удаление мест из кэша для МРТ
   * @param cmd cmd
   */
  deletePlacesUsingPOST(cmd: DeletePlacesFromHazelcastCmd): __Observable<null> {
    return this.deletePlacesUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Начальное заполнение кэша мест для МРТ
   * @param cmd cmd
   */
  initialPopulateUsingPOSTResponse(cmd: InitialPopulateHazelcastCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/hazelcast/init`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Начальное заполнение кэша мест для МРТ
   * @param cmd cmd
   */
  initialPopulateUsingPOST(cmd: InitialPopulateHazelcastCmd): __Observable<null> {
    return this.initialPopulateUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module HazelcastGatewayService {
}

export { HazelcastGatewayService }
