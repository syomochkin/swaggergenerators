/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ReduceIncomingQuantityCmd } from '../models/reduce-incoming-quantity-cmd';
import { ReservationCancelledResponse } from '../models/reservation-cancelled-response';
import { CancelStockReservationCmd } from '../models/cancel-stock-reservation-cmd';
import { ReservationDto } from '../models/reservation-dto';
import { ReserveStockOutgoingQuantityResponse } from '../models/reserve-stock-outgoing-quantity-response';
import { ReserveStockOutgoingQuantityCmd } from '../models/reserve-stock-outgoing-quantity-cmd';
import { TransferDroppedResponse } from '../models/transfer-dropped-response';
import { DropTransferCmd } from '../models/drop-transfer-cmd';

/**
 * Reservation Stock Gateway
 */
@Injectable({
  providedIn: 'root',
})
class ReservationStockGatewayService extends __BaseService {
  static readonly reduceIncomingQuantityUsingPOSTPath = '/v1/stock/places/reduceIncomingQuantity';
  static readonly cancelStockReservationUsingPOSTPath = '/v1/stock/places/reservation/cancelByStock';
  static readonly getAllUsingGET2Path = '/v1/stock/places/reservations';
  static readonly reserveOutgoingUsingPOSTPath = '/v1/stock/places/reserveOutgoingQuantity';
  static readonly dropTransferUsingPOSTPath = '/v1/stock/places/transfers/drop';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Уменьшение входящего резерва запаса
   *
   * reduce incoming quantity
   * @param request request
   */
  reduceIncomingQuantityUsingPOSTResponse(request: ReduceIncomingQuantityCmd): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = request;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reduceIncomingQuantity`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Уменьшение входящего резерва запаса
   *
   * reduce incoming quantity
   * @param request request
   */
  reduceIncomingQuantityUsingPOST(request: ReduceIncomingQuantityCmd): __Observable<null> {
    return this.reduceIncomingQuantityUsingPOSTResponse(request).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * Точечная отмена резерва запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей.
   * @param cmd cmd
   * @return OK
   */
  cancelStockReservationUsingPOSTResponse(cmd: CancelStockReservationCmd): __Observable<__StrictHttpResponse<ReservationCancelledResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reservation/cancelByStock`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReservationCancelledResponse>;
      })
    );
  }
  /**
   * Точечная отмена резерва запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей.
   * @param cmd cmd
   * @return OK
   */
  cancelStockReservationUsingPOST(cmd: CancelStockReservationCmd): __Observable<ReservationCancelledResponse> {
    return this.cancelStockReservationUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ReservationCancelledResponse)
    );
  }

  /**
   * Получение списка резервов запаса по списку transferId
   * @param transferId transferId
   * @return OK
   */
  getAllUsingGET2Response(transferId?: Array<string>): __Observable<__StrictHttpResponse<Array<ReservationDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    (transferId || []).forEach(val => {if (val != null) __params = __params.append('transferId', val.toString())});
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/stock/places/reservations`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<ReservationDto>>;
      })
    );
  }
  /**
   * Получение списка резервов запаса по списку transferId
   * @param transferId transferId
   * @return OK
   */
  getAllUsingGET2(transferId?: Array<string>): __Observable<Array<ReservationDto>> {
    return this.getAllUsingGET2Response(transferId).pipe(
      __map(_r => _r.body as Array<ReservationDto>)
    );
  }

  /**
   * Создание исходящего резерва запаса по id запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  reserveOutgoingUsingPOSTResponse(cmd: ReserveStockOutgoingQuantityCmd): __Observable<__StrictHttpResponse<ReserveStockOutgoingQuantityResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/reserveOutgoingQuantity`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ReserveStockOutgoingQuantityResponse>;
      })
    );
  }
  /**
   * Создание исходящего резерва запаса по id запаса
   *
   * Диагностический эндпоинт, предназначен только для тестовых целей
   * @param cmd cmd
   * @return OK
   */
  reserveOutgoingUsingPOST(cmd: ReserveStockOutgoingQuantityCmd): __Observable<ReserveStockOutgoingQuantityResponse> {
    return this.reserveOutgoingUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as ReserveStockOutgoingQuantityResponse)
    );
  }

  /**
   * Отмена резервов запаса
   *
   * Диагностический эндпоинт, предназначен только для служебных целей
   * @param cmd cmd
   * @return OK
   */
  dropTransferUsingPOSTResponse(cmd: DropTransferCmd): __Observable<__StrictHttpResponse<TransferDroppedResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/stock/places/transfers/drop`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<TransferDroppedResponse>;
      })
    );
  }
  /**
   * Отмена резервов запаса
   *
   * Диагностический эндпоинт, предназначен только для служебных целей
   * @param cmd cmd
   * @return OK
   */
  dropTransferUsingPOST(cmd: DropTransferCmd): __Observable<TransferDroppedResponse> {
    return this.dropTransferUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as TransferDroppedResponse)
    );
  }
}

module ReservationStockGatewayService {
}

export { ReservationStockGatewayService }
