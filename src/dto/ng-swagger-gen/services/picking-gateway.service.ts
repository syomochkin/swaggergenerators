/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { BoxRouteResponse } from '../models/box-route-response';
import { BoxRouteCmd } from '../models/box-route-cmd';
import { BoxRoute } from '../models/box-route';
import { BuildBoxRouteCmd } from '../models/build-box-route-cmd';
import { Route } from '../models/route';
import { PickerRouteReq } from '../models/picker-route-req';
import { PickingZoneTransferReq } from '../models/picking-zone-transfer-req';
import { ZoneDto } from '../models/zone-dto';

/**
 * Picking Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PickingGatewayService extends __BaseService {
  static readonly boxRouteUsingPOSTPath = '/v1/box-route/build';
  static readonly boxRouteUsingPOST1Path = '/v1/box-routes/build';
  static readonly pickerRoutesUsingPOSTPath = '/v1/picker-routes/build';
  static readonly findTransferUsingPOSTPath = '/v1/picker-transfer/build';
  static readonly getConsolidationZoneUsingGETPath = '/v1/picking/consolidation-zone';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос в МKТ для маршрутизации коробов комплектации
   * @param cmd cmd
   * @return OK
   */
  boxRouteUsingPOSTResponse(cmd: BoxRouteCmd): __Observable<__StrictHttpResponse<BoxRouteResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/box-route/build`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BoxRouteResponse>;
      })
    );
  }
  /**
   * Запрос в МKТ для маршрутизации коробов комплектации
   * @param cmd cmd
   * @return OK
   */
  boxRouteUsingPOST(cmd: BoxRouteCmd): __Observable<BoxRouteResponse> {
    return this.boxRouteUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as BoxRouteResponse)
    );
  }

  /**
   * Запрос в МKТ для маршрутизации коробов комплектации
   * @param cmd cmd
   * @return OK
   */
  boxRouteUsingPOST1Response(cmd: BuildBoxRouteCmd): __Observable<__StrictHttpResponse<BoxRoute>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/box-routes/build`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BoxRoute>;
      })
    );
  }
  /**
   * Запрос в МKТ для маршрутизации коробов комплектации
   * @param cmd cmd
   * @return OK
   */
  boxRouteUsingPOST1(cmd: BuildBoxRouteCmd): __Observable<BoxRoute> {
    return this.boxRouteUsingPOST1Response(cmd).pipe(
      __map(_r => _r.body as BoxRoute)
    );
  }

  /**
   * Запрос в МKТ для текущего маршрута комплектовщика
   * @param cmd cmd
   * @return OK
   */
  pickerRoutesUsingPOSTResponse(cmd: PickerRouteReq): __Observable<__StrictHttpResponse<Route>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/picker-routes/build`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Route>;
      })
    );
  }
  /**
   * Запрос в МKТ для текущего маршрута комплектовщика
   * @param cmd cmd
   * @return OK
   */
  pickerRoutesUsingPOST(cmd: PickerRouteReq): __Observable<Route> {
    return this.pickerRoutesUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as Route)
    );
  }

  /**
   * Запрос в МKТ для перехода между областями действий
   * @param cmd cmd
   * @return OK
   */
  findTransferUsingPOSTResponse(cmd: PickingZoneTransferReq): __Observable<__StrictHttpResponse<Route>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/picker-transfer/build`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Route>;
      })
    );
  }
  /**
   * Запрос в МKТ для перехода между областями действий
   * @param cmd cmd
   * @return OK
   */
  findTransferUsingPOST(cmd: PickingZoneTransferReq): __Observable<Route> {
    return this.findTransferUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as Route)
    );
  }

  /**
   * Запрос для получения области консолидации для области комплектации
   * @param zoneId zoneId
   * @return OK
   */
  getConsolidationZoneUsingGETResponse(zoneId: string): __Observable<__StrictHttpResponse<ZoneDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (zoneId != null) __params = __params.set('zoneId', zoneId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/picking/consolidation-zone`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ZoneDto>;
      })
    );
  }
  /**
   * Запрос для получения области консолидации для области комплектации
   * @param zoneId zoneId
   * @return OK
   */
  getConsolidationZoneUsingGET(zoneId: string): __Observable<ZoneDto> {
    return this.getConsolidationZoneUsingGETResponse(zoneId).pipe(
      __map(_r => _r.body as ZoneDto)
    );
  }
}

module PickingGatewayService {
}

export { PickingGatewayService }
