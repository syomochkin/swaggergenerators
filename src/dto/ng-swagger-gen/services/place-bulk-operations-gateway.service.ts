/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { BulkOperationDto } from '../models/bulk-operation-dto';
import { BulkPlacesDeleteCmd } from '../models/bulk-places-delete-cmd';
import { BulkPlaceUpdateStatusCmd } from '../models/bulk-place-update-status-cmd';
import { BulkPlaceUpdateTypeCmd } from '../models/bulk-place-update-type-cmd';
import { BulkPlaceUpdateZoneCmd } from '../models/bulk-place-update-zone-cmd';

/**
 * Place Bulk Operations Gateway
 */
@Injectable({
  providedIn: 'root',
})
class PlaceBulkOperationsGatewayService extends __BaseService {
  static readonly startBulkDeletePlacesUsingDELETEPath = '/v1/topology/places/bulk/delete-places';
  static readonly startBulkUpdatePlaceStatusUsingPOSTPath = '/v1/topology/places/bulk/update-place-status';
  static readonly startBulkUpdatePlaceTypeUsingPOSTPath = '/v1/topology/places/bulk/update-place-type';
  static readonly startBulkUpdateZoneUsingPOSTPath = '/v1/topology/places/bulk/update-zone';
  static readonly getBulkUpdateOperationDataUsingGETPath = '/v1/topology/places/bulk/{operationId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * startBulkDeletePlaces
   * @param cmd cmd
   * @return OK
   */
  startBulkDeletePlacesUsingDELETEResponse(cmd: BulkPlacesDeleteCmd): __Observable<__StrictHttpResponse<BulkOperationDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/topology/places/bulk/delete-places`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BulkOperationDto>;
      })
    );
  }
  /**
   * startBulkDeletePlaces
   * @param cmd cmd
   * @return OK
   */
  startBulkDeletePlacesUsingDELETE(cmd: BulkPlacesDeleteCmd): __Observable<BulkOperationDto> {
    return this.startBulkDeletePlacesUsingDELETEResponse(cmd).pipe(
      __map(_r => _r.body as BulkOperationDto)
    );
  }

  /**
   * startBulkUpdatePlaceStatus
   * @param params The `PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceStatusUsingPOSTParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  startBulkUpdatePlaceStatusUsingPOSTResponse(params: PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceStatusUsingPOSTParams): __Observable<__StrictHttpResponse<BulkOperationDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    __body = params.cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places/bulk/update-place-status`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BulkOperationDto>;
      })
    );
  }
  /**
   * startBulkUpdatePlaceStatus
   * @param params The `PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceStatusUsingPOSTParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  startBulkUpdatePlaceStatusUsingPOST(params: PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceStatusUsingPOSTParams): __Observable<BulkOperationDto> {
    return this.startBulkUpdatePlaceStatusUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as BulkOperationDto)
    );
  }

  /**
   * startBulkUpdatePlaceType
   * @param params The `PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceTypeUsingPOSTParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  startBulkUpdatePlaceTypeUsingPOSTResponse(params: PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceTypeUsingPOSTParams): __Observable<__StrictHttpResponse<BulkOperationDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    __body = params.cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places/bulk/update-place-type`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BulkOperationDto>;
      })
    );
  }
  /**
   * startBulkUpdatePlaceType
   * @param params The `PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceTypeUsingPOSTParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  startBulkUpdatePlaceTypeUsingPOST(params: PlaceBulkOperationsGatewayService.StartBulkUpdatePlaceTypeUsingPOSTParams): __Observable<BulkOperationDto> {
    return this.startBulkUpdatePlaceTypeUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as BulkOperationDto)
    );
  }

  /**
   * startBulkUpdateZone
   * @param params The `PlaceBulkOperationsGatewayService.StartBulkUpdateZoneUsingPOSTParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  startBulkUpdateZoneUsingPOSTResponse(params: PlaceBulkOperationsGatewayService.StartBulkUpdateZoneUsingPOSTParams): __Observable<__StrictHttpResponse<BulkOperationDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.siteCode != null) __params = __params.set('siteCode', params.siteCode.toString());
    __body = params.cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/topology/places/bulk/update-zone`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BulkOperationDto>;
      })
    );
  }
  /**
   * startBulkUpdateZone
   * @param params The `PlaceBulkOperationsGatewayService.StartBulkUpdateZoneUsingPOSTParams` containing the following parameters:
   *
   * - `siteCode`: siteCode
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  startBulkUpdateZoneUsingPOST(params: PlaceBulkOperationsGatewayService.StartBulkUpdateZoneUsingPOSTParams): __Observable<BulkOperationDto> {
    return this.startBulkUpdateZoneUsingPOSTResponse(params).pipe(
      __map(_r => _r.body as BulkOperationDto)
    );
  }

  /**
   * getBulkUpdateOperationData
   * @param operationId operationId
   * @return OK
   */
  getBulkUpdateOperationDataUsingGETResponse(operationId: string): __Observable<__StrictHttpResponse<BulkOperationDto>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/topology/places/bulk/${encodeURIComponent(String(operationId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<BulkOperationDto>;
      })
    );
  }
  /**
   * getBulkUpdateOperationData
   * @param operationId operationId
   * @return OK
   */
  getBulkUpdateOperationDataUsingGET(operationId: string): __Observable<BulkOperationDto> {
    return this.getBulkUpdateOperationDataUsingGETResponse(operationId).pipe(
      __map(_r => _r.body as BulkOperationDto)
    );
  }
}

module PlaceBulkOperationsGatewayService {

  /**
   * Parameters for startBulkUpdatePlaceStatusUsingPOST
   */
  export interface StartBulkUpdatePlaceStatusUsingPOSTParams {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * cmd
     */
    cmd: BulkPlaceUpdateStatusCmd;
  }

  /**
   * Parameters for startBulkUpdatePlaceTypeUsingPOST
   */
  export interface StartBulkUpdatePlaceTypeUsingPOSTParams {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * cmd
     */
    cmd: BulkPlaceUpdateTypeCmd;
  }

  /**
   * Parameters for startBulkUpdateZoneUsingPOST
   */
  export interface StartBulkUpdateZoneUsingPOSTParams {

    /**
     * siteCode
     */
    siteCode: string;

    /**
     * cmd
     */
    cmd: BulkPlaceUpdateZoneCmd;
  }
}

export { PlaceBulkOperationsGatewayService }
