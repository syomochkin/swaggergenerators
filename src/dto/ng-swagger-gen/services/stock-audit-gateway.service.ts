/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { AuditStockRecordSearchResponse } from '../models/audit-stock-record-search-response';
import { AuditStockRecordSearchQuery } from '../models/audit-stock-record-search-query';

/**
 * Stock Audit Gateway
 */
@Injectable({
  providedIn: 'root',
})
class StockAuditGatewayService extends __BaseService {
  static readonly findAllUsingPOST2Path = '/v1/audit/stock/findAll';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * История движения товаров
   * @param searchQuery searchQuery
   * @return OK
   */
  findAllUsingPOST2Response(searchQuery: AuditStockRecordSearchQuery): __Observable<__StrictHttpResponse<AuditStockRecordSearchResponse>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = searchQuery;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/audit/stock/findAll`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<AuditStockRecordSearchResponse>;
      })
    );
  }
  /**
   * История движения товаров
   * @param searchQuery searchQuery
   * @return OK
   */
  findAllUsingPOST2(searchQuery: AuditStockRecordSearchQuery): __Observable<AuditStockRecordSearchResponse> {
    return this.findAllUsingPOST2Response(searchQuery).pipe(
      __map(_r => _r.body as AuditStockRecordSearchResponse)
    );
  }
}

module StockAuditGatewayService {
}

export { StockAuditGatewayService }
