/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RouteStrategyDTO } from '../models/route-strategy-dto';
import { CreateRouteStrategyCmd } from '../models/create-route-strategy-cmd';
import { RouteStrategySteps } from '../models/route-strategy-steps';
import { UpdateRouteStrategyCmd } from '../models/update-route-strategy-cmd';

/**
 * Route Strategy Gateway
 */
@Injectable({
  providedIn: 'root',
})
class RouteStrategyGatewayService extends __BaseService {
  static readonly createStrategyUsingPOSTPath = '/v1/route-strategy';
  static readonly getRouteStrategiesUsingGETPath = '/v1/route-strategy/strategies';
  static readonly getRouteStrategyStepsUsingGETPath = '/v1/route-strategy/strategies/{routeStrategyId}';
  static readonly updateRouteStrategyUsingPUTPath = '/v1/route-strategy/{routeStrategyId}';
  static readonly deleteStrategyUsingDELETEPath = '/v1/route-strategy/{routeStrategyId}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Запрос на создание стратегии обхода на площадке
   * @param cmd cmd
   * @return OK
   */
  createStrategyUsingPOSTResponse(cmd: CreateRouteStrategyCmd): __Observable<__StrictHttpResponse<RouteStrategyDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = cmd;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/v1/route-strategy`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RouteStrategyDTO>;
      })
    );
  }
  /**
   * Запрос на создание стратегии обхода на площадке
   * @param cmd cmd
   * @return OK
   */
  createStrategyUsingPOST(cmd: CreateRouteStrategyCmd): __Observable<RouteStrategyDTO> {
    return this.createStrategyUsingPOSTResponse(cmd).pipe(
      __map(_r => _r.body as RouteStrategyDTO)
    );
  }

  /**
   * Запрос на получение всех стратегий обхода в рамках площадки
   * @param siteId siteId
   * @return OK
   */
  getRouteStrategiesUsingGETResponse(siteId: string): __Observable<__StrictHttpResponse<Array<RouteStrategyDTO>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (siteId != null) __params = __params.set('siteId', siteId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/route-strategy/strategies`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<RouteStrategyDTO>>;
      })
    );
  }
  /**
   * Запрос на получение всех стратегий обхода в рамках площадки
   * @param siteId siteId
   * @return OK
   */
  getRouteStrategiesUsingGET(siteId: string): __Observable<Array<RouteStrategyDTO>> {
    return this.getRouteStrategiesUsingGETResponse(siteId).pipe(
      __map(_r => _r.body as Array<RouteStrategyDTO>)
    );
  }

  /**
   * Запрос на получение змейки с порядком обхода секций для стратегии обхода в рамках площадки
   * @param routeStrategyId routeStrategyId
   * @return OK
   */
  getRouteStrategyStepsUsingGETResponse(routeStrategyId: string): __Observable<__StrictHttpResponse<RouteStrategySteps>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/v1/route-strategy/strategies/${encodeURIComponent(String(routeStrategyId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RouteStrategySteps>;
      })
    );
  }
  /**
   * Запрос на получение змейки с порядком обхода секций для стратегии обхода в рамках площадки
   * @param routeStrategyId routeStrategyId
   * @return OK
   */
  getRouteStrategyStepsUsingGET(routeStrategyId: string): __Observable<RouteStrategySteps> {
    return this.getRouteStrategyStepsUsingGETResponse(routeStrategyId).pipe(
      __map(_r => _r.body as RouteStrategySteps)
    );
  }

  /**
   * Запрос на обновление стратегии обхода
   * @param params The `RouteStrategyGatewayService.UpdateRouteStrategyUsingPUTParams` containing the following parameters:
   *
   * - `routeStrategyId`: routeStrategyId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateRouteStrategyUsingPUTResponse(params: RouteStrategyGatewayService.UpdateRouteStrategyUsingPUTParams): __Observable<__StrictHttpResponse<RouteStrategyDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.cmd;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/v1/route-strategy/${encodeURIComponent(String(params.routeStrategyId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<RouteStrategyDTO>;
      })
    );
  }
  /**
   * Запрос на обновление стратегии обхода
   * @param params The `RouteStrategyGatewayService.UpdateRouteStrategyUsingPUTParams` containing the following parameters:
   *
   * - `routeStrategyId`: routeStrategyId
   *
   * - `cmd`: cmd
   *
   * @return OK
   */
  updateRouteStrategyUsingPUT(params: RouteStrategyGatewayService.UpdateRouteStrategyUsingPUTParams): __Observable<RouteStrategyDTO> {
    return this.updateRouteStrategyUsingPUTResponse(params).pipe(
      __map(_r => _r.body as RouteStrategyDTO)
    );
  }

  /**
   * Запрос на удаление стратегии обхода
   * @param routeStrategyId routeStrategyId
   */
  deleteStrategyUsingDELETEResponse(routeStrategyId: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/v1/route-strategy/${encodeURIComponent(String(routeStrategyId))}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * Запрос на удаление стратегии обхода
   * @param routeStrategyId routeStrategyId
   */
  deleteStrategyUsingDELETE(routeStrategyId: string): __Observable<null> {
    return this.deleteStrategyUsingDELETEResponse(routeStrategyId).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module RouteStrategyGatewayService {

  /**
   * Parameters for updateRouteStrategyUsingPUT
   */
  export interface UpdateRouteStrategyUsingPUTParams {

    /**
     * routeStrategyId
     */
    routeStrategyId: string;

    /**
     * cmd
     */
    cmd: UpdateRouteStrategyCmd;
  }
}

export { RouteStrategyGatewayService }
