/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface AbstractWarehouseError {
  code?: string;
  message?: string;
}

export interface AddPlaceTypeCommonCharacteristicDto {
  characteristic?: string;
  defaultValue?: DefaultValue;
  settings?: CommonCharacteristicSettings;
}

export interface AddStockToCarrierCmd {
  baseAmount?: number;
  baseUnit?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /** @format uuid */
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  stockType?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

export type AllocationError = AbstractWarehouseError & { code?: string; message?: string };

export interface AllocationZoneTransferReq {
  /** @format uuid */
  carrierId?: string;

  /** @format uuid */
  destinationPlaceId?: string;
  operationId?: string;
  processBusinessKey?: string;

  /** @format uuid */
  startPlaceId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface AssignProductBatchCmd {
  createMarksOnPicking?: boolean;

  /** @format date-time */
  expirationTime?: string;

  /** @format uuid */
  inboundId?: string;

  /** @format date-time */
  inboundTime?: string;

  /** @format date-time */
  manufactureTime?: string;
  marksAccountingType?: "EGAIS" | "CHZ";
  mercuryExtId?: string;
  number?: string;
  productId?: string;
  productName?: string;
  referenceBNumber?: string;
  siteCode?: string;
  type?: "REGULAR" | "RETURN";
  unit?: string;
  vendorCode?: string;
  vendorName?: string;
  weightProduct?: boolean;
}

export interface AuditCarrierRecordDto {
  /** @format uuid */
  carrierId?: string;
  carrierNumber?: string;

  /** @format int32 */
  createdTime?: number;

  /** @format int64 */
  id?: number;
  login?: string;
  operation?: string;
  operationId?: string;
  processType?: string;
  sourceCarrierReservationKey?: string;

  /** @format uuid */
  sourceParentId?: string;
  sourceParentNumber?: string;
  sourceParentTypeCode?: string;
  sourcePlaceAddress?: string;
  sourceSiteCode?: string;
  sourceTypeCode?: string;
  tabNumber?: string;
  targetCarrierReservationKey?: string;

  /** @format uuid */
  targetParentId?: string;
  targetParentNumber?: string;
  targetParentTypeCode?: string;
  targetPlaceAddress?: string;
  targetSiteCode?: string;
  targetTypeCode?: string;
  userName?: string;
}

export interface AuditCarrierRecordSearchQuery {
  /** @format uuid */
  carrierId?: string;
  carrierNumber?: string;

  /** @format date-time */
  createdAtFrom: string;

  /** @format date-time */
  createdAtTo: string;
  login?: string;

  /** @format int32 */
  maxResponseSize?: number;
  operation?: string;
  operationId?: string;
  processType?: string;
  sourceCarrierReservationKey?: string;

  /** @format uuid */
  sourceParentId?: string;
  sourceParentNumber?: string;
  sourceParentTypeCode?: string;
  sourcePlaceAddress?: string;
  sourceSiteCode?: string;
  sourceTypeCode?: string;
  tabNumber?: string;
  targetCarrierReservationKey?: string;

  /** @format uuid */
  targetParentId?: string;
  targetParentNumber?: string;
  targetParentTypeCode?: string;
  targetPlaceAddress?: string;
  targetSiteCode?: string;
  targetTypeCode?: string;
  userName?: string;
}

export interface AuditCarrierRecordSearchResponse {
  records?: AuditCarrierRecordDto[];

  /** @format int64 */
  recordsCount?: number;
}

export interface AuditMarkRecordDto {
  /** @format int32 */
  createdTime: number;

  /** @format int64 */
  id?: number;

  /** @format uuid */
  inboundDeliveryId?: string;

  /** @format int32 */
  inboundDeliveryPosition?: number;
  level: "TRANSPORT" | "GROUP" | "INDIVIDUAL";
  login?: string;
  markCode: string;
  operation: string;
  parentCode?: string;
  process: string;
  productId: string;
  productName?: string;
  siteCode: string;
  sourceOrderNumber?: string;

  /** @format int32 */
  sourceOrderPosition?: number;
  sourceProductBatchNumber?: string;
  sourceStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  tabNumber?: string;
  targetOrderNumber?: string;

  /** @format int32 */
  targetOrderPosition?: number;
  targetProductBatchNumber?: string;
  targetStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  type: "EGAIS" | "CHZ";
  unit?: string;
  userName?: string;
}

export interface AuditMarkRecordSearchQuery {
  /** @format date-time */
  createdAtFrom: string;

  /** @format date-time */
  createdAtTo: string;

  /** @format int32 */
  inboundDeliveryPosition?: number;
  level?: "TRANSPORT" | "GROUP" | "INDIVIDUAL";
  login?: string;
  markCode?: string;
  operation?: string;
  process?: string;
  productId?: string;
  siteCode: string;
  sourceProductBatchNumber?: string;
  sourceStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  tabNumber?: string;
  targetProductBatchNumber?: string;
  targetStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  type?: "EGAIS" | "CHZ";
  unit?: string;
  userName?: string;
}

export interface AuditMarkRecordSearchResponse {
  moreRecordsLeft?: boolean;
  records?: AuditMarkRecordDto[];
}

export interface AuditStockRecordDto {
  additionalInfo?: Record<string, string>;
  baseAmount: number;
  baseUnit: string;
  calculatedBaseAmount: boolean;

  /** @format int32 */
  createdTime: number;

  /** @format int64 */
  id?: number;
  operation: string;
  process: string;
  quantity: number;
  siteCode: string;
  sourceCarrierNumber?: string;
  sourcePlaceAddress?: string;

  /** @format uuid */
  sourceProductBatchId?: string;
  sourceProductBatchNumber?: string;
  sourceProductId?: string;
  sourceProductName?: string;
  sourceStockTypeCode?: string;
  tabNumber?: string;
  targetCarrierNumber?: string;
  targetPlaceAddress?: string;

  /** @format uuid */
  targetProductBatchId?: string;
  targetProductBatchNumber?: string;
  targetProductId?: string;
  targetProductName?: string;
  targetStockTypeCode?: string;
  unit: string;
  userName?: string;
}

export interface AuditStockRecordSearchQuery {
  /** @format date-time */
  createdAtFrom: string;

  /** @format date-time */
  createdAtTo: string;
  operation?: string;
  process?: string;
  siteCode: string;
  sourceCarrierNumber?: string;
  sourcePlaceAddress?: string;
  sourceProductBatchNumber?: string;
  sourceProductId?: string;
  sourceStockTypeCode?: string;
  tabNumber?: string;
  targetCarrierNumber?: string;
  targetPlaceAddress?: string;
  targetProductBatchNumber?: string;
  targetProductId?: string;
  targetStockTypeCode?: string;
}

export interface AuditStockRecordSearchResponse {
  moreRecordsLeft?: boolean;
  records?: AuditStockRecordDto[];
}

export interface AvailabilityPlaceForTransferCarrierCmd {
  carrierTypeCodes: string[];
  siteCode: string;

  /** @format uuid */
  transferId: string;
}

export interface AvailabilityPlaceForTransferCarrierResponse {
  availableCarrierTypes: AvailableCarrierTypeDto[];
}

export interface AvailableCarrierTypeDto {
  availabilityStatus: boolean;
  carrierTypeCode: string;
}

export interface BoxRoute {
  operationId?: string;

  /** @format int32 */
  plannedTotalDuration?: number;
  processBusinessKey?: string;
  rejectedItems?: ProductAmountDto[];
  steps?: RouteStep[];

  /** @format uuid */
  transferId?: string;
}

export interface BoxRouteCmd {
  items?: ProductAmountItem[];
  joinedItemByProductId?: Record<string, ProductAmountItem>;
  operationId?: string;
  processBusinessKey?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  useRoute?: "NO_ROUTE";
  useStartPlace?: "NO_START_PLACE";
}

export interface BoxRouteResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  rejectedItems?: RejectedProductItemAmount[];
  transferRoutes?: TransferRoute[];
}

export interface BuildBoxRouteCmd {
  items?: ProductAmountDto[];
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;

  /** @format uuid */
  transferId?: string;
}

export interface BulkOperationDto {
  operationId?: string;
  status?: string;
}

export interface BulkPlaceUpdateStatusCmd {
  operationId?: string;
  placeIds?: string[];
  statusCode?: string;
}

export interface BulkPlaceUpdateTypeCmd {
  operationId?: string;
  placeIds?: string[];

  /** @format uuid */
  targetPlaceTypeId?: string;
}

export interface BulkPlaceUpdateZoneCmd {
  operationId?: string;
  placeIds?: string[];

  /** @format uuid */
  zoneId?: string;
}

export interface BulkPlacesDeleteCmd {
  operationId?: string;
  placeIds?: string[];
  siteCode?: string;
}

export interface CalculateNewProductPeiQuantityCmd {
  productId: string;

  /** @format int32 */
  quantity: number;

  /** @format uuid */
  stockId: string;
}

export interface CalculateNewProductPeiQuantityResponse {
  quantity: number;
  unit: string;
}

export interface CancelStockReservationCmd {
  incomingQuantity?: number;
  operationId?: string;
  outgoingQuantity?: number;
  siteCode?: string;

  /** @format uuid */
  stockId?: string;

  /** @format uuid */
  transferId?: string;
  unit?: string;
}

export interface CarrierDto {
  author?: string;
  barcode?: string;

  /** @format uuid */
  carriersTypeId?: string;

  /** @format int32 */
  createdTime?: number;
  editor?: string;

  /** @format uuid */
  id?: string;
  nestedCarriers?: CarrierDto[];

  /** @format uuid */
  parentId?: string;
  parentNumber?: string;
  placeAddress?: string;

  /** @format uuid */
  placeId?: string;
  processId?: string;
  type?: CarrierTypeDto;

  /** @format int32 */
  updatedTime?: number;
}

export interface CarrierGridView {
  barcode?: string;

  /** @format uuid */
  id?: string;
  nestedCarriers?: CarrierGridView[];
  status?: "incoming" | "outgoing" | "actual";
  type?: CarrierTypeGridView;
}

export interface CarrierInfo {
  /** @format uuid */
  carrierId?: string;
  carrierNumber?: string;
}

export interface CarrierShippedResponse {
  /** @format uuid */
  carrierId?: string;
  operationId?: string;

  /** @format uuid */
  placeId?: string;
}

export interface CarrierTransferReservationInfo {
  carrierTypeCode?: string;

  /** @format int32 */
  quantity?: number;

  /** @format uuid */
  transferId?: string;
}

export interface CarrierTypeCodeExistsQuery {
  code?: string;
}

export interface CarrierTypeDto {
  archived?: boolean;
  author?: string;
  code?: string;

  /** @format int32 */
  createdTime?: number;
  description?: string;
  editor?: string;

  /** @format int32 */
  height?: number;

  /** @format uuid */
  id?: string;

  /** @format int32 */
  length?: number;
  maxVolume?: number;
  maxWeight?: number;
  name?: string;

  /** @format int32 */
  updatedTime?: number;

  /** @format int32 */
  width?: number;
}

export interface CarrierTypeGridView {
  code?: string;
  description?: string;

  /** @format uuid */
  id?: string;
  name?: string;
}

export interface CarrierUploadResponse {
  reservedCarriersWithProcesses: Record<string, string>;
}

export interface ChangeMarksStatusCmd {
  marks?: MarkChangeStatusDTO[];
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface ChangeMarksStatusResponse {
  error?: AbstractWarehouseError;
  marks?: ResponseMarkDTO[];
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface ChangeRawStock {
  /** @format int64 */
  position?: number;

  /** @format uuid */
  stockId?: string;
}

export interface ChangeRawStockTypeCmd {
  operationId?: string;
  orderVersion?: string;
  outboundDeliveryNumber?: string;

  /** @format date-time */
  outboundTime?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  stocks?: ChangeRawStock[];
  tabNumber?: string;
  userName?: string;
}

export interface ChangeStockProductCmd {
  operationId: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  productId: string;
  quantity: number;

  /** @format uuid */
  stockId: string;
  tabNumber?: string;
  userName?: string;
}

export interface ChangeStockProductResponse {
  /** @format uuid */
  newStockId?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity?: number;

  /** @format uuid */
  stockId?: string;
  tabNumber?: string;
  userName?: string;
}

export interface CharacteristicDto {
  type?: string;
  value?: ValueDto;
}

export interface CheckPlaceForStockCmd {
  processBusinessKey?: string;
  quantity: number;
  siteCode: string;

  /** @format uuid */
  stockId: string;
  unit: string;
}

export interface CheckPlaceForStockResponse {
  approvedQuantity?: number;
  processBusinessKey?: string;
  rejectedInfo?: RejectedInfo;
  siteCode?: string;
}

export interface ClusterDeliveryReservationRoute {
  author?: string;

  /** @format int32 */
  createdTime: number;
  editor?: string;
  steps: RouteStep[];

  /** @format int32 */
  updatedTime: number;
}

export interface CodeExists {
  exists?: boolean;
}

export interface CommonCharacteristicSettings {
  editable?: boolean;
  mandatory?: boolean;
  parentInheritance?: "check" | "ignore";
  unit?: string;
}

export interface ConfirmProductBatchCmd {
  /** @format uuid */
  batchId?: string;
  targetStockType?: string;
}

export interface Coordinates3D {
  x?: number;
  y?: number;
  z?: number;
}

export interface CoordinatesDto {
  x?: number;
  y?: number;
  z?: number;
}

export interface CreateCarrierCmd {
  carrierBarcode?: string;

  /** @format uuid */
  carrierId?: string;
  carrierReservationKey?: string;
  carrierTypeCode?: string;
  operationId?: string;
  placeBarcode?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface CreateCarrierTypeCmd {
  code?: string;
  description?: string;

  /** @format int32 */
  height?: number;

  /** @format uuid */
  id?: string;

  /** @format int32 */
  length?: number;
  maxVolume?: number;
  maxWeight?: number;
  name?: string;

  /** @format int32 */
  width?: number;
}

export interface CreateMarksCmd {
  marks?: MarkCreateDTO[];
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface CreateMarksResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface CreatePlaceCmd {
  address?: string;

  /** @format uuid */
  addressTypeId?: string;
  coordinates?: Coordinates3D;
  description?: string;

  /** @format uuid */
  id?: string;

  /** @format int32 */
  number?: number;

  /** @format uuid */
  parentId?: string;

  /** @format uuid */
  siteId?: string;
  status?: PlaceStatusDto;
  statusReason?: string;

  /** @format uuid */
  typeId?: string;
  zoneIds?: string[];
}

export interface CreatePlaceTypeCmd {
  code?: string;
  coordinatesRequired?: boolean;
  description?: string;

  /** @format int32 */
  maxMixBatches?: number;

  /** @format int32 */
  maxMixProducts?: number;
  name?: string;
  numerationRule?:
    | "LEFT_TO_RIGHT_TOP_TO_BOTTOM"
    | "LEFT_TO_RIGHT_BOTTOM_UP"
    | "RIGHT_TO_LEFT_TOP_TO_BOTTOM"
    | "RIGHT_TO_LEFT_BOTTOM_UP";

  /** @format uuid */
  placeTypeId?: string;
  storagePlace?: boolean;
  virtualPlace?: boolean;
}

export interface CreateRouteStrategyCmd {
  /** Название стратегии (русскоязычное понятное навание стратегии) */
  name: string;

  /** Код площадки */
  siteCode: string;
}

export interface CreateScannedChzMarkCmd {
  code?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /** @format uuid */
  productBatchId?: string;
  siteCode?: string;
  status?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

export interface CreateScannedChzMarkResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface CreateSiteCmd {
  code?: string;
  description?: string;

  /** @format uuid */
  id?: string;
  name?: string;
}

export interface CreateZoneCmd {
  boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
  code?: string;
  description?: string;

  /** @format uuid */
  id?: string;
  name?: string;

  /** @format uuid */
  siteId?: string;

  /** @format uuid */
  typeId?: string;
}

export interface CreateZoneTypeCmd {
  code?: string;
  description?: string;

  /** @format uuid */
  id?: string;
  name?: string;
}

export interface Data {
  address?: string;
  author?: string;

  /** @format int32 */
  childrenAmount?: number;

  /** @format int32 */
  createdTime?: number;
  editor?: string;

  /** @format uuid */
  id?: string;
  status?: PlaceStatusDto;
  typeName?: string;

  /** @format int32 */
  updatedTime?: number;
  zoneNames?: string[];
}

export interface DefaultValue {
  decValue?: number;

  /** @format int32 */
  intValue?: number;

  /** @format int32 */
  maxIntValue?: number;

  /** @format int32 */
  minIntValue?: number;
}

export interface DeleteMarksCmd {
  marks?: MarkDeleteDTO[];
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface DeleteMarksResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface DeletePlacesFromHazelcastCmd {
  siteCode?: string;
  storageAreaCodes?: string[];
}

export interface DetailedCarriersSearchQuery {
  barcodes?: string[];
}

export interface DetailedPlaceSearchFilter {
  placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
  placeIds?: string[];
  placeStatus?: string;
  placeTypeCode?: string;
  siteCode: string;
  withHierarchy?: boolean;
  zoneCode?: string;

  /** @format uuid */
  zoneId?: string;
  zoneTypeCode?: string;
}

export interface DetailedPlaceSearchQuery {
  filter: DetailedPlaceSearchFilter;

  /** @format int32 */
  maxResponseSize?: number;

  /** @format int32 */
  pageNumber?: number;
}

export interface DetailedPlaceSearchResult {
  places?: PlaceDto[];

  /** @format int64 */
  totalCount?: number;
}

export interface DetailedStockSearchQuery {
  /** @format uuid */
  carrierId?: string;
  carrierNumber?: string;
  inCarrier?: "IN_ONLY" | "OUT_ONLY" | "ALL";
  inVirtualPlace?: "VIRTUAL" | "NOT_VIRTUAL" | "ALL";

  /** @format int32 */
  maxResponseSize?: number;
  placeAddress?: string;

  /** @format uuid */
  placeId?: string;

  /** @format uuid */
  productBatchId?: string;
  productBatchNumber?: string;
  productId?: string;
  productIds?: string[];
  siteCode?: string;

  /** @format uuid */
  stockId?: string;
  stockTypeCode?: string;
}

export interface DetailedStocksSearchResult {
  stocks?: StockDto[];

  /** @format int64 */
  totalCount?: number;
}

export interface DropAllProcessReservationsCmd {
  operationId?: string;
  processId?: string;
  siteCode?: string;
}

export interface DropCarrierReservationForProcessCmd {
  carrierNumber?: string;
  dropForNestedCarriers?: boolean;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface DropCarrierReservationForProcessResponse {
  carrierIds?: string[];
  processBusinessKey?: string;
  siteCode?: string;
}

export interface DropPlaceReservationForProcessCmd {
  operationId?: string;
  placeBarcode?: string;
  processId?: string;
  siteCode?: string;
}

export interface DropPlaceReservationsForTransferCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  siteCode?: string;

  /** @format uuid */
  transferId?: string;
}

export interface DropTransferCmd {
  processBusinessKey?: string;

  /** @format uuid */
  transferId?: string;
}

export interface ExchangeChuteReservationCmd {
  carrierTypeCode?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;

  /** @format uuid */
  sourceTransferId?: string;

  /** @format uuid */
  targetTransferId?: string;
  zoneCodes?: string[];
}

export interface ExchangeChuteReservationResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;

  /** @format uuid */
  sourceTransferId?: string;
  targetReserveInfo?: ReserveInfo;
}

export interface FullAmount {
  quantity?: number;
  unit?: string;
  volume?: number;

  /** @format int64 */
  weight?: number;
}

export type GeneralWarehouseError = AbstractWarehouseError & { code?: string; message?: string };

export interface InitialPopulateHazelcastCmd {
  siteCode?: string;
  storageAreaCodes?: string[];
}

export interface MarkChangeStatusDTO {
  code?: string;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;
  sourceStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  targetStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  type?: "EGAIS" | "CHZ";
  unit?: string;
}

export interface MarkCreateDTO {
  code?: string;

  /** @format uuid */
  inboundDeliveryId?: string;

  /** @format int32 */
  inboundDeliveryPosition?: number;
  level?: "TRANSPORT" | "GROUP" | "INDIVIDUAL";
  parentCode?: string;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;
  status?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  type?: "EGAIS" | "CHZ";
  unit?: string;
}

export interface MarkDTO {
  code: string;

  /** @format int32 */
  createdAt: number;

  /** @format uuid */
  inboundDeliveryId?: string;

  /** @format int32 */
  inboundDeliveryPosition?: number;
  level: "TRANSPORT" | "GROUP" | "INDIVIDUAL";
  markId?: string;
  markStatus:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  orderNumber?: string;

  /** @format int32 */
  orderPosition?: number;
  parentCode?: string;

  /** @format uuid */
  productBatchId?: string;
  productBatchNumber?: string;
  productId: string;
  productName?: string;
  type: "EGAIS" | "CHZ";
  unit?: string;
}

export interface MarkDeleteDTO {
  code?: string;
  type?: "EGAIS" | "CHZ";
}

export type MarkError = AbstractWarehouseError & {
  alreadyExistingCodes?: string[];
  code?: string;
  deleteMarkHasChildren?: string[];
  message?: string;
  notFoundCodes?: string[];
  wrongParent?: string[];
  wrongProduct?: string[];
  wrongProductBatches?: string[];
  wrongStatusCodes?: string[];
  wrongUnit?: string[];
};

export interface MarkSearchDTO {
  code?: string;
  type?: "EGAIS" | "CHZ";
}

export interface MarkSearchFilter {
  /** @format date-time */
  createdAtFrom?: string;

  /** @format date-time */
  createdAtTo?: string;

  /** @format uuid */
  inboundDeliveryId?: string;

  /** @format int32 */
  inboundDeliveryPosition?: number;
  level?: "TRANSPORT" | "GROUP" | "INDIVIDUAL";
  marks?: MarkSearchDTO[];
  parentCode?: string;

  /** @format uuid */
  productBatchId?: string;
  productBatchNumber?: string;
  productId?: string;
  siteCode: string;
  statuses?: (
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED"
  )[];
  type?: "EGAIS" | "CHZ";
  unit?: string;
}

export interface MarkSearchQuery {
  filter?: MarkSearchFilter;

  /** @format int32 */
  maxResponseSize?: number;
  orderInfo?: OrderByCriteria[];

  /** @format int32 */
  pageNumber?: number;
}

export interface MarkSearchResult {
  marks?: MarkDTO[];

  /** @format int64 */
  totalCount?: number;
}

export interface MarkUpdateDTO {
  code?: string;
  orderNumber?: string;

  /** @format int32 */
  orderPosition?: number;

  /** @format uuid */
  productBatchId?: string;
  type?: "EGAIS" | "CHZ";
}

export interface ModifyStockTypeCmd {
  code: string;
  name?: string;
  writeOffAllowed: boolean;
}

export interface MonoPalletPlacementRequest {
  /** @format uuid */
  incomingCarrierId?: string;
  openCarriers?: string[];
  operationId?: string;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;

  /** @format int32 */
  quantity?: number;
  siteCode?: string;
  stockTypeCode?: string;
}

export interface MoveAllStocksToCarrierCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;

  /** @format uuid */
  sourcePlaceId?: string;
  tabNumber?: string;

  /** @format uuid */
  targetCarrierId?: string;

  /** @format uuid */
  transferId?: string;
  userName?: string;
}

export interface MoveAllStocksToCarrierResponse {
  operationId?: string;
  stockDto?: StockDto[];

  /** @format uuid */
  targetCarrierId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface MoveCarrierToPlaceCmd {
  /** @format uuid */
  carrierId?: string;
  carrierReservationKey?: string;
  operationId?: string;

  /** @format uuid */
  placeId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  tabNumber?: string;

  /** @format uuid */
  transferId?: string;
  userName?: string;
}

export interface MoveCarrierToPlaceResponse {
  /** @format uuid */
  carrierId?: string;
  processBusinessKey?: string;
  rejected?: boolean;
  rejectedReason?: string;
  transferComplete?: boolean;
}

export interface MoveStockBetweenReplenishmentCarriersCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity?: number;
  siteCode?: string;
  sourceCarrierReservationKey?: string;

  /** @format uuid */
  stockId?: string;
  tabNumber?: string;

  /** @format uuid */
  targetCarrierId?: string;
  targetCarrierReservationKey?: string;
  userName?: string;
}

export interface MoveStockBetweenReplenishmentCarriersResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface MoveStockCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity?: number;

  /** @format uuid */
  stockId?: string;
  tabNumber?: string;

  /** @format uuid */
  targetCarrierId?: string;

  /** @format uuid */
  targetPlaceId?: string;
  userName?: string;
}

export interface MoveStockCmdResponse {
  movedQuantity?: number;
  operationId?: string;

  /** @format uuid */
  originalStockId?: string;
  targetStockActualQuantity?: number;

  /** @format uuid */
  targetStockId?: string;
}

export interface MoveStockToCarrierCmd {
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity?: number;

  /** @format uuid */
  sourcePlaceId?: string;

  /** @format uuid */
  sourceStockId?: string;
  tabNumber?: string;

  /** @format uuid */
  targetCarrierId?: string;

  /** @format uuid */
  transferId?: string;
  unit?: string;
  userName?: string;
}

export interface NamedId {
  label?: string;

  /** @format uuid */
  value?: string;
}

export interface OrderBoxDto {
  /** @format uuid */
  carrierId?: string;
  orderItems?: OutboundOrderItemDto[];
}

export interface OrderByCriteria {
  field?: string;
  order?: "ASC" | "DESC";
}

export interface OutboundOrderCmd {
  operationId?: string;
  orderBoxes?: OrderBoxDto[];
  orderVersion?: string;
  outboundDeliveryNumber?: string;

  /** @format date-time */
  outboundTime?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  unshippedOrderItems?: UnshippedOrderItemDto[];
  userName?: string;
}

export interface OutboundOrderItemDto {
  baseAmount?: number;
  baseUnit?: string;

  /** @format int64 */
  position?: number;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;

  /** @format int32 */
  quantity?: number;
  unit?: string;
}

export interface OverridePlaceCommonCharacteristicCmd {
  characteristic?: string;

  /** @format uuid */
  siteId?: string;
  value?: ValueCmd;
}

export interface PageRequestInfo {
  /** @format int32 */
  limit?: number;

  /** @format int32 */
  offset?: number;
}

export interface PickerRouteReq {
  /** @format uuid */
  pickingZoneId?: string;
  siteCode?: string;

  /** @format uuid */
  startPlaceId?: string;
  transferIds?: string[];
}

export interface PickingZoneTransferReq {
  /** @format uuid */
  carrierId?: string;

  /** @format uuid */
  destinationPlaceId?: string;
  operationId?: string;
  processBusinessKey?: string;

  /** @format uuid */
  startPlaceId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface PlaceCharacteristicDto {
  author?: string;

  /** @format uuid */
  characteristicId?: string;
  checkParents?: string;

  /** @format int32 */
  createdTime?: number;
  editable?: boolean;
  editor?: string;
  mandatory?: boolean;

  /** @format uuid */
  placeId?: string;

  /** @format uuid */
  siteId?: string;
  type?: string;

  /** @format uuid */
  typeCharacteristicId?: string;

  /** @format int32 */
  updatedTime?: number;
  value?: ValueDto;
}

export type PlaceCheckError = AbstractWarehouseError & { code?: string; message?: string };

export interface PlaceDto {
  address?: string;

  /** @format uuid */
  addressTypeId?: string;
  author?: string;
  childPlaces?: PlaceDto[];
  coordinates?: CoordinatesDto;

  /** @format int32 */
  createdTime?: number;
  description?: string;
  editor?: string;

  /** @format uuid */
  id?: string;

  /** @format int32 */
  number?: number;

  /** @format uuid */
  parentId?: string;
  parentPlaces?: PlaceDto[];

  /** @format uuid */
  siteId?: string;
  status?: PlaceStatusDto;
  statusReason?: string;
  storagePlace?: boolean;
  typeCode?: string;

  /** @format uuid */
  typeId?: string;
  typeName?: string;

  /** @format int32 */
  updatedTime?: number;
  virtualPlace?: boolean;
  zones?: ZoneInfo[];
}

export interface PlaceHierarchyFilter {
  hierarchyElements?: PlaceHierarchyFilterCondition[];

  /** @format uuid */
  siteId?: string;
}

export interface PlaceHierarchyFilterCondition {
  criteria?: string;

  /** @format uuid */
  placeTypeId?: string;
}

export interface PlaceHierarchyTreeDto {
  address?: string;
  characteristics?: CharacteristicDto[];
  children?: PlaceHierarchyTreeDto[];

  /** @format uuid */
  id?: string;

  /** @format int32 */
  number?: number;
  status?: string;
  type?: Type;
  zones?: Zone[];
}

export interface PlaceInfo {
  address?: string;
  author?: string;

  /** @format int32 */
  childrenAmount?: number;

  /** @format int32 */
  createdTime?: number;
  editor?: string;

  /** @format uuid */
  id?: string;
  placeZones?: PlaceZoneInfo[];

  /** @format uuid */
  siteId?: string;
  siteName?: string;
  typeName?: string;

  /** @format int32 */
  updatedTime?: number;
  zoneNames?: string[];
}

export interface PlaceNodeWithPosition {
  childPlaces?: Data[];
  placeNodeChainFromTop?: NamedId[];
}

export type PlaceReservationForCarriersNotFoundError = AbstractWarehouseError & { code?: string; message?: string };

export interface PlaceReservationSearchRequest {
  placeIds?: string[];
}

export interface PlaceReservationSearchResponse {
  placeResponses?: PlaceResponse[];
}

export interface PlaceResponse {
  carrierTransferReservations?: CarrierTransferReservationInfo[];
  carriers?: CarrierInfo[];
  placeAddress?: string;

  /** @format uuid */
  placeId?: string;
  processId?: string;
}

export interface PlaceSearchFilter {
  hierarchy?: PlaceHierarchyFilter;
  placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
  placeTypes?: string[];
  zonesType?: PlaceZonesFilter[];
}

export interface PlaceShortView {
  barcode?: string;
  description?: string;

  /** @format uuid */
  id?: string;
  siteCode?: string;
  typeCode?: string;
  typeName?: string;
}

export interface PlaceStatusDto {
  code?: string;
  default?: boolean;
  title?: string;
}

export interface PlaceTypeCharacteristicDTO {
  author?: string;

  /** @format uuid */
  characteristicId?: string;
  checkParents?: string;

  /** @format int32 */
  createdTime?: number;
  defaultValue?: DefaultValue;
  editable?: boolean;
  editor?: string;
  mandatory?: boolean;

  /** @format uuid */
  placeTypeId?: string;
  type?: string;

  /** @format int32 */
  updatedTime?: number;
}

export interface PlaceTypeCodeExistsQuery {
  code?: string;
}

export interface PlaceTypeDto {
  author?: string;
  code?: string;
  coordinatesRequired?: boolean;

  /** @format int32 */
  createdTime?: number;
  description?: string;
  editor?: string;

  /** @format int32 */
  maxMixBatches?: number;

  /** @format int32 */
  maxMixProducts?: number;
  name?: string;

  /** @format int64 */
  numberOfImplementation?: number;
  numerationRule?: string;

  /** @format uuid */
  placeTypeId?: string;

  /** @format uuid */
  siteId?: string;
  storagePlace?: boolean;

  /** @format int32 */
  updatedTime?: number;
  virtualPlace?: boolean;
}

export interface PlaceWithPosition {
  place?: PlaceDto;
  placeNodeChainFromTop?: NamedId[];
}

export interface PlaceWithType {
  address?: string;

  /** @format uuid */
  id?: string;

  /** @format int32 */
  number?: number;
  type?: Type;
}

export interface PlaceZoneInfo {
  zoneCode?: string;
  zoneName?: string;
  zoneTypeCode?: string;
  zoneTypeName?: string;
}

export interface PlaceZonesFilter {
  /** @format uuid */
  zoneTypeId?: string;
  zonesId?: string[];
}

export interface PlacesSearchQuery {
  filter?: PlaceSearchFilter;
  orderInfo?: OrderByCriteria[];
  pageInfo?: PageRequestInfo;
}

export interface PlacesSearchResult {
  places?: PlaceInfo[];

  /** @format int32 */
  totalCount?: number;
}

export interface ProcessTransferInfo {
  carrierTypeCode?: string;
  processId?: string;

  /** @format int32 */
  quantity?: number;

  /** @format uuid */
  transferId?: string;
}

export interface ProductAmountDto {
  productId?: string;
  quantity?: number;
  rejectionReason?: "NO_STOCK" | "NO_ROUTE";
  unit?: string;
}

export interface ProductAmountItem {
  productId?: string;
  quantity?: number;
  unit?: string;
}

export interface ProductBatchBalanceByStockType {
  baseAmountERP?: number;
  baseAmountWMS?: number;
  quantityERP?: number;
  quantityWMS?: number;
  stockTypeCode?: string;
}

export interface ProductBatchBalanceDifferenceDto {
  canCorrect?: boolean;
  needCorrect?: boolean;
  stockTypes?: ProductBatchBalanceByStockType[];
}

export interface ProductBatchDto {
  createMarksOnPicking?: boolean;

  /** @format int32 */
  expirationTime?: number;

  /** @format uuid */
  id?: string;

  /** @format uuid */
  inboundId?: string;

  /** @format int32 */
  inboundTime?: number;

  /** @format int32 */
  manufactureTime?: number;
  marksAccountingType?: "EGAIS" | "CHZ";
  mercuryExtId?: string;
  number?: string;
  productId?: string;
  productName?: string;
  referenceBNumber?: string;

  /** @format uuid */
  siteId?: string;
  type?: "REGULAR" | "RETURN";
  vendorCode?: string;
  vendorName?: string;
  weightProduct?: boolean;
}

export interface ProductBatchSearchDto {
  beiUnit?: string;

  /** @format int32 */
  beiUnitDenominator?: number;

  /** @format int32 */
  beiUnitNumerator?: number;
  confirmed?: boolean;

  /** @format int32 */
  createdTime?: number;

  /** @format int32 */
  expirationTime?: number;

  /** @format int32 */
  height?: number;

  /** @format uuid */
  id?: string;

  /** @format int32 */
  inboundTime?: number;

  /** @format int32 */
  length?: number;

  /** @format int32 */
  manufactureTime?: number;
  marksAccountingType?: "EGAIS" | "CHZ";
  mercuryExtId?: string;
  peiUnit?: string;
  productBatchNumber?: string;
  productId?: string;
  productName?: string;

  /** @format int32 */
  sellByTime?: number;
  siteCode?: string;

  /** @format uuid */
  siteId?: string;
  totalBaseAmountRemainder?: number;
  totalQuantityRemainder?: number;
  type?: "REGULAR" | "RETURN";
  vendorCode?: string;
  vendorName?: string;

  /** @format int32 */
  volume?: number;

  /** @format int32 */
  weightGross?: number;

  /** @format int32 */
  weightNet?: number;

  /** @format int32 */
  width?: number;
}

export interface ProductBatchSearchFilter {
  confirmed?: boolean;

  /** @format date-time */
  expirationDateFrom?: string;

  /** @format date-time */
  expirationDateTo?: string;

  /** @format date-time */
  inboundTimeFrom?: string;

  /** @format date-time */
  inboundTimeTo?: string;

  /** @format date-time */
  manufactureTimeFrom?: string;

  /** @format date-time */
  manufactureTimeTo?: string;
  marksAccountingType?: "EGAIS" | "CHZ";
  mercuryExtId?: string;
  number?: string;
  productId?: string;

  /** @format date-time */
  sellByTimeFrom?: string;

  /** @format date-time */
  sellByTimeTo?: string;
  siteCode?: string;

  /** @format uuid */
  siteId?: string;
  types?: ("REGULAR" | "RETURN")[];
}

export interface ProductBatchesSearchQuery {
  filter?: ProductBatchSearchFilter;
  orderInfo?: OrderByCriteria[];
  pageInfo?: PageRequestInfo;
}

export interface ProductBatchesSearchResult {
  productBatches?: ProductBatchSearchDto[];

  /** @format int64 */
  totalCount?: number;
}

export interface ProductPlacementRequest {
  openCarriers?: string[];
  operationId?: string;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;

  /** @format int32 */
  quantity?: number;
  siteCode?: string;
  stockTypeCode?: string;
}

export interface ProductPlacementResponse {
  operationId?: string;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;
  rejectedInfo?: RejectedInfo;
  reservedCarriers?: ReservedCarrier[];
}

export interface RawPlacementRequest {
  /** @format uuid */
  incomingCarrierId?: string;
  openCarriers?: string[];
  operationId?: string;

  /** @format uuid */
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  siteCode?: string;
  stockTypeCode?: string;
}

export interface RawStock {
  /** @format int64 */
  position?: number;

  /** @format uuid */
  productBatchId?: string;
  quantity?: number;
}

export interface ReduceIncomingQuantityCmd {
  operationId?: string;

  /** @format uuid */
  productBatchId?: string;
  quantity?: number;
  stockTypeCode?: string;

  /** @format uuid */
  transferId?: string;
}

export interface ReducePlaceReservationForTransferCmd {
  carrierTypeCode?: string;
  operationId?: string;

  /** @format uuid */
  placeId?: string;

  /** @format int32 */
  quantity?: number;
  siteCode?: string;

  /** @format uuid */
  transferId?: string;
  zoneCodes?: string[];
}

export interface RejectedInfo {
  clientError?: AbstractWarehouseError;
  quantity?: number;
}

export interface RejectedProductItemAmount {
  productId?: string;
  quantity?: number;
  rejectionReason?: "NO_STOCK";
  unit?: string;
}

export interface ReplenishCmd {
  destPlaceBarcode?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /** @format uuid */
  productBatchId?: string;
  productId?: string;
  quantity?: number;
  siteCode?: string;

  /** @format uuid */
  srcCarrierId?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  targetStockTypeCode?: string;
  userName?: string;
}

export interface ReservationCancelledResponse {
  incomingQuantity?: number;
  operationId?: string;
  outgoingQuantity?: number;
  siteCode?: string;

  /** @format uuid */
  stockId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface ReservationDto {
  /** @format uuid */
  batchId?: string;
  placeAddress?: string;

  /** @format uuid */
  placeId?: string;
  productId?: string;
  quantityIncoming?: number;
  quantityOutgoing?: number;

  /** @format uuid */
  stockId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface ReservationInfoDto {
  /** @format uuid */
  batchId?: string;
  incomingQuantity?: number;
  outgoingQuantity?: number;
  productId?: string;

  /** @format uuid */
  stockId?: string;
  stockTypeCode?: string;
  unit?: string;
}

export interface ReserveCarrierForProcessCmd {
  carrierNumber?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export type ReserveCarrierForProcessError = AbstractWarehouseError & { code?: string; message?: string };

export interface ReserveCarrierForProcessResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
}

export interface ReserveChutePlacesForCarriersCmd {
  carrierPlaceTypeCodes?: string[];
  carriers?: TransferDto[];
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  processPlaceTypeCodes?: string[];
  siteCode?: string;
  zones?: ZoneReference[];
}

export interface ReserveInfo {
  placeAddress?: string;

  /** @format uuid */
  placeId?: string;
  placeTypeCode?: string;
  transfer?: TransferDto;
}

export interface ReservePlaceForProcessCmd {
  operationId?: string;
  placeBarcode?: string;
  processId?: string;
  siteCode?: string;
}

export interface ReservePlacesForCarriersByClusterDeliveryCmd {
  carrierPlaceTypeCode?: string;
  carriers?: ProcessTransferInfo[];
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  processPlaceTypeCode?: string;
  reservationMode?: "FULL" | "PARTIAL" | "SUSTAINED";
  siteCode?: string;
  zones?: ZoneReference[];
}

export interface ReservePlacesForCarriersByMultyProcessesCmd {
  carrierPlaceTypeCode?: string;
  carriers?: ProcessTransferInfo[];
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  processPlaceTypeCode?: string;
  siteCode?: string;
  zones?: ZoneReference[];
}

export interface ReservePlacesForCarriersCmd {
  carrierPlaceTypeCode?: string;
  carriers?: TransferDto[];
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  processPlaceTypeCode?: string;
  reservationMode?: "FULL" | "PARTIAL" | "SUSTAINED";
  siteCode?: string;
  zones?: ZoneReference[];
}

export type ReservePlacesForCarriersError = AbstractWarehouseError & { code?: string; message?: string };

export interface ReserveStockOutgoingQuantityCmd {
  operationId?: string;
  quantity?: number;

  /** @format uuid */
  stockId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface ReserveStockOutgoingQuantityResponse {
  operationId?: string;
  reservation?: ReservationDto;
}

export interface ReservedCarrier {
  openNewCarrier?: boolean;

  /** @format uuid */
  openedCarrierId?: string;
  quantity?: number;
}

export interface ReservedChutePlacesForCarriersResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  rejectedTransfers?: TransferDto[];
  reserves?: ReserveInfo[];
  siteCode?: string;
}

export interface ReservedPlacesForCarriersByClusterDeliveryResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  rejectedTransfers?: ProcessTransferInfo[];
  reserves?: ReserveInfo[];
  siteCode?: string;
}

export interface ReservedPlacesForCarriersByMultiProcessesResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  processName?: string;
  rejectedTransfers?: ProcessTransferInfo[];
  reserves?: ReserveInfo[];
  siteCode?: string;
}

export interface ReservedPlacesForCarriersResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processName?: string;
  rejectedTransfers?: TransferDto[];
  reserves?: ReserveInfo[];
  siteCode?: string;
}

export interface ResponseMarkDTO {
  actualCode?: string;
  newStatus?:
    | "INBOUND_DELIVERY"
    | "REJECTED"
    | "INBOUND_SCANNED"
    | "INBOUND_EGAIS_CHECK"
    | "FOR_SALE"
    | "PICKED"
    | "SOLD"
    | "CLIENT_RETURN"
    | "RETURN_EGAIS_CHECK"
    | "WRITTEN_OFF"
    | "FOUND"
    | "CANCELLED";
  requestedCode?: string;
}

export interface Route {
  steps?: RouteStep[];
}

export type RouteNoEdgeError = AbstractWarehouseError & { code?: string; message?: string };

export type RouteNotVacantError = AbstractWarehouseError & { code?: string; message?: string };

export interface RouteStep {
  /** @format int32 */
  aisleNumber?: number;

  /** @format int32 */
  cameraNumber?: number;

  /** @format int32 */
  floorNumber?: number;

  /** @format int32 */
  orderNumber: number;
  placeAddress: string;

  /** @format int32 */
  regionNumber?: number;

  /** @format int32 */
  rowNumber?: number;

  /** @format int32 */
  sectionNumber?: number;
}

export interface RouteStrategyDTO {
  /**
   * Идентификатор стратегии
   * @format uuid
   */
  id: string;

  /** Название стратегии (русскоязычное понятное навание стратегии) */
  name: string;

  /** Код площадки */
  siteCode: string;

  /** коды секторов, к которым будет привязана данная стратегия */
  zoneCodes: string[];
}

export interface RouteStrategyStep {
  /** Адрес места/секции */
  sectionAddress: string;

  /** Признак, является ли данное место - местом старта  */
  startPlace: boolean;

  /**
   * Порядковый номер секции в стратегии обхода
   * @format int32
   */
  step: number;
}

export interface RouteStrategySteps {
  /**
   * Последенее время редактирования стратегии
   * @format int32
   */
  lastUpdateTime: number;

  /** Пользователь, который последний раз редактировал стратегию */
  lastUpdateUser: string;

  /**
   * Идентификатор стратегии
   * @format uuid
   */
  routeStrategyId: string;

  /** Название стратегии */
  routeStrategyName: string;

  /** Последовательность секций для обхода */
  steps: RouteStrategyStep[];
}

export interface SetCarrierPlaceCmd {
  actualPlaceAddress?: string;

  /** @format uuid */
  carrierId?: string;
  operationId?: string;
  planPlaceAddress?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;

  /** @format uuid */
  transferId?: string;
  userName?: string;
}

export interface ShipCarrierCmd {
  /** @format uuid */
  carrierId?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface SiteCodeExistsQuery {
  code?: string;
}

export interface SiteDto {
  archived?: boolean;
  author?: string;
  code?: string;

  /** @format int32 */
  createdTime?: number;
  description?: string;
  editor?: string;

  /** @format uuid */
  id?: string;
  name?: string;

  /** @format int32 */
  updatedTime?: number;
}

export interface SiteSettingDto {
  bigDecimalValue?: number;
  code?: "MAX_MIX_PRODUCTS";

  /** @format int32 */
  intValue?: number;
  stringValue?: string;
  type?: "INTEGER" | "STRING" | "BIG_DECIMAL";
}

export interface SlottingRoute {
  /** @format int32 */
  plannedDuration?: number;
  steps?: SlottingRouteStep[];

  /** @format uuid */
  transferId?: string;
}

export interface SlottingRouteStep {
  characteristics?: CharacteristicDto[];
  hierarchy?: PlaceWithType[];
  place?: PlaceWithType;

  /** @format int32 */
  plannedDuration?: number;
  reservation?: ReservationInfoDto;

  /** @format uuid */
  transferId?: string;
  zones?: ZoneWithType[];
}

export interface StockAddressesAndProductIdsSearchFilter {
  addresses?: string[];
  productIds?: string[];

  /** @format uuid */
  siteId?: string;
}

export interface StockAddressesAndProductIdsSearchQuery {
  filter?: StockAddressesAndProductIdsSearchFilter;
  orderInfo?: OrderByCriteria[];
  pageInfo?: PageRequestInfo;
}

export interface StockAmountDto {
  createMarksOnPicking?: boolean;
  marksAccountingType?: "EGAIS" | "CHZ";
  productId?: string;
  quantity?: number;

  /** @format uuid */
  stockId?: string;
  unit?: string;
}

export interface StockAnnihilationCmd {
  baseAmount?: number;
  baseUnit?: string;

  /** @format uuid */
  carrierId?: string;
  carrierReservationKey?: string;
  operationId?: string;

  /** @format uuid */
  placeId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /** @format uuid */
  productBatchId?: string;
  quantity?: number;
  siteCode?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

export interface StockAnnihilationResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface StockDto {
  actualQuantity?: number;

  /** @format int32 */
  actualVolume?: number;

  /** @format int32 */
  actualWeight?: number;
  availableQuantity?: number;

  /** @format int32 */
  availableVolume?: number;

  /** @format int32 */
  availableWeight?: number;
  baseUnit?: string;
  batchConfirmed?: boolean;

  /** @format uuid */
  batchId?: string;
  batchNumber?: string;

  /** @format uuid */
  carrierId?: string;
  incomingQuantity?: number;

  /** @format int32 */
  incomingVolume?: number;

  /** @format int32 */
  incomingWeight?: number;
  outgoingQuantity?: number;

  /** @format int32 */
  outgoingVolume?: number;

  /** @format int32 */
  outgoingWeight?: number;

  /** @format uuid */
  placeId?: string;
  productId?: string;
  productName?: string;

  /** @format uuid */
  stockId?: string;
  stockTypeCode?: string;
  unit?: string;
}

export interface StockHierarchySearchFilter {
  hierarchy?: PlaceHierarchyFilter;
  placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
  placeTypes?: string[];
  productIds?: string[];
  zonesType?: PlaceZonesFilter[];
}

export interface StockHierarchySearchQuery {
  filter?: StockHierarchySearchFilter;
  orderInfo?: OrderByCriteria[];
  pageInfo?: PageRequestInfo;
}

export interface StockInCmd {
  baseAmount?: number;
  baseUnit?: string;

  /** @format uuid */
  carrierId?: string;

  /** @format date-time */
  documentDate?: string;
  documentNumber?: string;
  operationId?: string;

  /** @format uuid */
  placeId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /** @format uuid */
  productBatchId?: string;
  quantity?: number;
  stockTypeCode?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

export interface StockInResponse {
  operationId?: string;

  /** @format uuid */
  stockId?: string;
}

export interface StockInfo {
  actual?: FullAmount;
  available?: FullAmount;
  batchConfirmed?: boolean;

  /** @format uuid */
  batchId?: string;
  batchNumber?: string;
  carrier?: boolean;

  /** @format int32 */
  expirationTime?: number;
  inbound?: FullAmount;

  /** @format int32 */
  inboundTime?: number;

  /** @format int32 */
  manufactureTime?: number;
  outbound?: FullAmount;
  placeAddress?: string;

  /** @format uuid */
  placeId?: string;
  productId?: string;
  productName?: string;

  /** @format int32 */
  sellByTime?: number;
  stockTypeCode?: string;

  /** @format uuid */
  stockTypeId?: string;
}

export type StockReservationsNotFoundError = AbstractWarehouseError & { code?: string; message?: string };

export interface StockTypeDto {
  author?: string;
  code?: string;

  /** @format int32 */
  createdTime?: number;
  editor?: string;

  /** @format uuid */
  id?: string;
  name?: string;

  /** @format int32 */
  updatedTime?: number;
  writeOffAllowed?: boolean;
}

export interface StocksSearchResult {
  stocks?: StockInfo[];

  /** @format int32 */
  totalCount?: number;
}

export interface TransferDroppedResponse {
  canceledReservations?: ReservationDto[];
  processBusinessKey?: string;

  /** @format uuid */
  transferId?: string;
}

export interface TransferDto {
  carrierTypeCode?: string;

  /** @format int32 */
  quantity?: number;

  /** @format uuid */
  transferId?: string;
}

export interface TransferRoute {
  route?: string[];

  /** @format uuid */
  transferId?: string;
}

export interface TryAllocateSpaceForReplenishmentCmd {
  destPlaceBarcode?: string;
  operationId?: string;
  processId?: string;

  /** @format uuid */
  productBatchId?: string;
  quantity?: number;
  siteCode?: string;

  /** @format uuid */
  srcCarrierId?: string;
  stockTypeCode?: string;
}

export interface TryAllocateSpaceForReplenishmentResponse {
  approvedQuantity?: number;
  operationId?: string;
  rejectedInfo?: RejectedInfo;
}

export interface TryTakeRequest {
  operationId?: string;

  /** @format uuid */
  placeId?: string;
  quantity?: number;

  /** @format uuid */
  stockId?: string;

  /** @format uuid */
  transferId?: string;
  unit?: string;
}

export interface TryTakeResponse {
  acceptedQuantity?: number;
  operationId?: string;
  rejectReason?: string;
  rejected?: boolean;

  /** @format uuid */
  stockId?: string;

  /** @format uuid */
  transferId?: string;
}

export interface Type {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
}

export interface UIStockDto {
  /** Фактический остаток в ПЕИ */
  actualQuantity: number;

  /**
   * Фактический остаток в мл
   * @format int32
   */
  actualVolume: number;

  /**
   * Фактический остаток в граммах
   * @format int32
   */
  actualWeight: number;

  /** Доступный остаток в ПЕИ */
  availableQuantity: number;

  /**
   * Доступный остаток в мл
   * @format int32
   */
  availableVolume: number;

  /**
   * Доступный остаток в граммах
   * @format int32
   */
  availableWeight: number;

  /** Носитель */
  carrierBarcode?: string;

  /** Тип носителя */
  carrierTypeCode?: string;

  /**
   * Годен до
   * @format int32
   */
  expirationTime: number;

  /**
   * Дата и время приемки партии запаса
   * @format int32
   */
  inboundTime: number;

  /** Входящий остаток в ПЕИ */
  incomingQuantity: number;

  /**
   * Входящий остаток в мл
   * @format int32
   */
  incomingVolume: number;

  /**
   * Входящий остаток в граммах
   * @format int32
   */
  incomingWeight: number;

  /**
   * Дата и время производства партии запаса
   * @format int32
   */
  manufactureTime: number;

  /** Тип помарочного учета */
  marksAccountingType?: "EGAIS" | "CHZ";

  /** Исходящий остаток в ПЕИ */
  outgoingQuantity: number;

  /**
   * Исходящий остаток в мл
   * @format int32
   */
  outgoingVolume: number;

  /**
   * Исходящий остаток в граммах
   * @format int32
   */
  outgoingWeight: number;

  /** Родительский носитель */
  parentCarrierBarcode?: string;

  /** Тип родительского носителя */
  parentCarrierTypeCode?: string;

  /** Адрес места */
  placeAddress: string;

  /** Статус места */
  placeStatus: string;

  /** Тип места */
  placeTypeCode: string;

  /** Партия принята к учету */
  productBatchConfirmed: boolean;

  /** Номер партии */
  productBatchNumber: string;

  /** Тип партии */
  productBatchType: "REGULAR" | "RETURN";

  /** Артикул */
  productId: string;

  /** Название товара */
  productName: string;

  /**
   * Срок реализации
   * @format int32
   */
  sellByTime: number;

  /**
   * id запаса
   * @format uuid
   */
  stockId: string;

  /** Код вида запаса */
  stockTypeCode: string;

  /** ПЕИ */
  unit: string;
}

export interface UIStockSearchQuery {
  /** Номер носителя */
  carrierBarcodes?: string[];

  /** Тип носителя */
  carrierTypeCodes?: string[];

  /** Признак "Принято к учету" */
  confirmed?: boolean;

  /** Типы зон, исключенные из поиска */
  excludedZoneTypeIds?: string[];

  /**
   * Годен до (от)
   * @format date-time
   */
  expirationDateFrom?: string;

  /**
   * Годен до (до)
   * @format date-time
   */
  expirationDateTo?: string;

  /**
   * Принят (от)
   * @format date-time
   */
  inboundTimeFrom?: string;

  /**
   * Принят (до)
   * @format date-time
   */
  inboundTimeTo?: string;

  /**
   * Произведен (от)
   * @format date-time
   */
  manufactureTimeFrom?: string;

  /**
   * Произведен (до)
   * @format date-time
   */
  manufactureTimeTo?: string;

  /** Тип маркировки */
  markAccountingTypes?: ("EGAIS" | "CHZ")[];

  /** @format int32 */
  maxResponseSize?: number;

  /** Адреса мест */
  placeAddresses?: string[];

  /** Статус места */
  placeStatusCodes?: string[];

  /** Тип места */
  placeTypeCodes?: string[];

  /** Номер партии */
  productBatchNumbers?: string[];

  /** Тип партии */
  productBatchTypes?: ("REGULAR" | "RETURN")[];

  /** Артикулы товара */
  productIds?: string[];

  /**
   * Срок реализации (от)
   * @format date-time
   */
  sellByTimeFrom?: string;

  /**
   * Срок реализации (до)
   * @format date-time
   */
  sellByTimeTo?: string;
  siteCode: string;

  /** Вид запаса */
  stockTypeCodes?: string[];

  /** Признак "Виртуальное место" */
  virtualPlace?: boolean;

  /** Без маркировки */
  withoutMarkAccountingType?: boolean;

  /** Зоны */
  zoneIds?: string[];
}

export interface UIStockSearchResult {
  stocks: UIStockDto[];

  /** @format int64 */
  totalCount: number;
}

export interface UnshippedOrderItemDto {
  baseAmount?: number;
  baseUnit?: string;

  /** @format int64 */
  position?: number;
  productId?: string;

  /** @format int32 */
  quantity?: number;
  unit?: string;
}

export interface UpdateCarrierTypeCmd {
  description?: string;

  /** @format int32 */
  height?: number;

  /** @format int32 */
  length?: number;
  maxVolume?: number;
  maxWeight?: number;
  name?: string;

  /** @format int32 */
  width?: number;
}

export interface UpdateMarksCmd {
  marks?: MarkUpdateDTO[];
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface UpdateMarksResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface UpdatePlaceCmd {
  address: string;

  /** @format uuid */
  addressTypeId?: string;
  coordinates?: Coordinates3D;
  description?: string;

  /** @format int32 */
  number: number;
  status?: PlaceStatusDto;
  statusReason?: string;

  /** @format uuid */
  typeId: string;
  zoneIds?: string[];
}

export interface UpdatePlaceStatusCmd {
  operationId?: string;
  placeAddress?: string;
  placeStatusCode?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  tabNumber?: string;
  userName?: string;
}

export interface UpdatePlaceStatusResponse {
  error?: AbstractWarehouseError;
  placeAddress?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface UpdatePlaceTypeCmd {
  description?: string;

  /** @format int32 */
  maxMixBatches?: number;

  /** @format int32 */
  maxMixProducts?: number;
  name?: string;
  numerationRule?:
    | "LEFT_TO_RIGHT_TOP_TO_BOTTOM"
    | "LEFT_TO_RIGHT_BOTTOM_UP"
    | "RIGHT_TO_LEFT_TOP_TO_BOTTOM"
    | "RIGHT_TO_LEFT_BOTTOM_UP";
  storagePlace?: boolean;
  virtualPlace?: boolean;
}

export interface UpdatePlaceTypeCommonCharacteristicDto {
  characteristic?: string;
  defaultValue?: DefaultValue;
}

export interface UpdateRouteStrategyCmd {
  /** Название стратегии (русскоязычное понятное навание стратегии) */
  name: string;

  /** Код площадки */
  siteCode: string;

  /** коды секторов, к которым будет привязана данная стратегия. */
  zoneCodes?: string[];
}

export interface UpdateSiteCmd {
  description?: string;
  name?: string;
}

export interface UpdateStockTypeByInventoryCmd {
  baseAmount?: number;

  /** @format date-time */
  docDate?: string;
  documentNumber?: string;
  operationId?: string;
  operationType?: "MOVE";

  /** @format int32 */
  orderItem?: number;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  saleAmount?: number;
  salesOrder?: string;
  specStock?: string;

  /** @format uuid */
  stockId?: string;
  stockTypeCode?: string;
  tabNumber?: string;

  /** @format uuid */
  transferId?: string;
  updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
  userName?: string;
}

export interface UpdateStockTypeByPickingCmd {
  /** @format date-time */
  docDate?: string;
  documentNumber?: string;
  operationId?: string;
  operationType?: "MOVE";
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /** @format uuid */
  stockId?: string;
  stockTypeCode?: string;
  tabNumber?: string;
  updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
  userName?: string;
}

export interface UpdateStockTypeEvent {
  actualQuantity?: number;

  /** @format uuid */
  batchId?: string;
  batchNumber?: string;
  beiQuantity?: number;
  beiUnit?: string;

  /** @format uuid */
  carrierId?: string;

  /** @format date-time */
  documentDate?: string;
  documentNumber?: string;

  /** @format date-time */
  expirationDate?: string;

  /** @format date-time */
  manufactureDate?: string;
  marksAccountingType?: "EGAIS" | "CHZ";

  /** @format uuid */
  newStockId?: string;
  newStockTypeCode?: string;

  /** @format uuid */
  oldStockId?: string;
  oldStockTypeCode?: string;
  operationId?: string;
  operationType?: "MOVE";

  /** @format int32 */
  orderItemIndex?: number;
  placeWithType?: PlaceWithType;
  productId?: string;
  productName?: string;
  salesOrder?: string;
  shouldNotifyERP?: boolean;
  siteCode?: string;
  specialStockCode?: string;

  /** @format date-time */
  storageDate?: string;
  tabNumber?: string;
  unit?: string;
  updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
}

export interface UpdateStockTypeForBatchCmd {
  /** @format uuid */
  batchId?: string;
  newStockTypeCode?: string;
  oldStockTypeCode?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  tabNumber?: string;
  userName?: string;
}

export interface UpdateStockTypeInStockByUICmd {
  operationId: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity: number;

  /** @format uuid */
  stockId: string;
  stockTypeCode: string;
  tabNumber?: string;
  userName?: string;
}

export interface UpdateZoneCmd {
  boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
  code?: string;
  description?: string;
  name?: string;
}

export interface UpdateZoneTypeCmd {
  description?: string;
  name?: string;
}

export interface ValueCmd {
  decValue?: number;

  /** @format int32 */
  intValue?: number;

  /** @format int32 */
  maxIntValue?: number;

  /** @format int32 */
  minIntValue?: number;
}

export interface ValueDto {
  decValue?: number;

  /** @format int32 */
  intValue?: number;
  maxDecValue?: number;

  /** @format int32 */
  maxIntValue?: number;
  minDecValue?: number;

  /** @format int32 */
  minIntValue?: number;
}

export interface WarehouseProductGroupDto {
  code?: string;
}

export interface WriteOffRawCmd {
  operationId?: string;
  orderVersion?: string;
  outboundDeliveryNumber?: string;

  /** @format date-time */
  outboundTime?: string;
  placeAddress?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  siteCode?: string;
  stocks?: RawStock[];
  tabNumber?: string;
  userName?: string;
}

export interface WriteOffRawResponse {
  error?: AbstractWarehouseError;
  operationId?: string;
  outboundDeliveryNumber?: string;
  processBusinessKey?: string;
  siteCode?: string;
}

export interface WriteOffStockByUICmd {
  operationId: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity: number;

  /** @format uuid */
  stockId: string;
  tabNumber?: string;
  userName?: string;
}

export interface WriteOffStockCmd {
  baseAmount?: number;
  baseUnit?: string;

  /** @format date-time */
  documentDate?: string;
  documentNumber?: string;
  operationId?: string;
  processBusinessKey?: string;
  processId?: string;
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";
  quantity?: number;

  /** @format uuid */
  stockId?: string;
  tabNumber?: string;
  unit?: string;
  userName?: string;
}

export interface WriteOffStockResponse {
  clientError?: AbstractWarehouseError;
  operationId?: string;

  /** @format uuid */
  stockId?: string;
}

export interface Zone {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
  type?: Type;
}

export interface ZoneCodeExistsQuery {
  code?: string;

  /** @format uuid */
  siteId?: string;
}

export interface ZoneDto {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
}

export interface ZoneInfo {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
  typeCode?: string;

  /** @format uuid */
  typeId?: string;
  typeName?: string;
}

export interface ZoneProjection {
  author?: string;
  boundedWarehouseProductGroups?: WarehouseProductGroupDto[];

  /** @format int32 */
  childAmount?: number;
  code?: string;

  /** @format int32 */
  createdTime?: number;
  description?: string;
  editor?: string;

  /** @format uuid */
  id?: string;
  name?: string;

  /** @format uuid */
  siteId?: string;

  /** @format uuid */
  typeId?: string;
  typeName?: string;

  /** @format int32 */
  updatedTime?: number;
}

export interface ZoneReference {
  zoneCode?: string;
  zoneTypeCode?: string;
}

export interface ZoneTypeCodeExistsQuery {
  code?: string;
}

export interface ZoneTypeDto {
  author?: string;
  code?: string;

  /** @format int32 */
  createdTime?: number;
  description?: string;
  editor?: string;

  /** @format uuid */
  id?: string;
  name?: string;

  /** @format int64 */
  numberOfImplementation?: number;

  /** @format int32 */
  updatedTime?: number;
  warehouseProductGroupCanBeBounded?: boolean;
}

export interface ZoneTypeWithZones {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
  zones?: ZoneDto[];
}

export interface ZoneTypeWithZonesDto {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
  zones?: ZoneDto[];
}

export interface ZoneWithType {
  code?: string;

  /** @format uuid */
  id?: string;
  name?: string;
  type?: Type;
}

export interface GetConsolidationZoneUsingGetParams {
  /**
   * zoneId
   * @format uuid
   */
  zoneId: string;
}

export interface GetAllUsingGet1Params {
  /**
   * inboundId
   * @format uuid
   */
  inboundId: string;

  /**
   * manufactureTime
   * @format int32
   */
  manufactureTime?: number;

  /** productId */
  productId: string;

  /** siteCode */
  siteCode: string;

  /** type */
  type?: "REGULAR" | "RETURN";

  /** vendorCode */
  vendorCode: string;
}

export interface GetRouteStrategiesUsingGetParams {
  /**
   * siteId
   * @format uuid
   */
  siteId: string;
}

export interface SlottingRouteUsingGetParams {
  /**
   * currentPlaceId
   * @format uuid
   */
  currentPlaceId?: string;

  /**
   * startPlaceId
   * @format uuid
   */
  startPlaceId: string;

  /**
   * transferId
   * @format uuid
   */
  transferId: string;
}

export interface TriggerSnapshotUsingPostParams {
  /** entity */
  entity: "PRODUCT_BATCH" | "STOCK" | "PLACE" | "PLACE_TYPE";

  /** type */
  type: "DELETE" | "SNAPSHOT";

  /**
   * entityId
   * @format uuid
   */
  entityId: string;
}

export interface GetAllUsingGet3Params {
  /**
   * carrierId
   * @format uuid
   */
  carrierId?: string;

  /** placeBarcode */
  placeBarcode?: string;

  /**
   * placeId
   * @format uuid
   */
  placeId?: string;

  /**
   * productBatchId
   * @format uuid
   */
  productBatchId?: string;

  /** productId */
  productId?: string;

  /** siteCode */
  siteCode?: string;

  /** stockTypeCode */
  stockTypeCode?: string;
}

export interface GetCarrierTypesUsingGetParams {
  /** code */
  code?: string[];
}

export interface GetAllUsingGetParams {
  /** barcode */
  barcode?: string;

  /**
   * placeId
   * @format uuid
   */
  placeId?: string;

  /** rootOnly */
  rootOnly?: boolean;
}

export interface GetByPlaceIdUsingGetParams {
  /**
   * placeId
   * @format uuid
   */
  placeId: string;
}

export interface MoveCarrierIntoCarrierUsingPutParams {
  /** parentProcessId */
  parentProcessId?: string;

  /** processId */
  processId?: string;

  /** processType */
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /**
   * carrierId
   * @format uuid
   */
  carrierId: string;

  /**
   * parentCarrierId
   * @format uuid
   */
  parentCarrierId: string;
}

export interface DeleteUsingDeleteParams {
  /** processId */
  processId?: string;

  /** processType */
  processType?:
    | "INBOUND"
    | "REPLENISHMENT"
    | "MANUFACTURING"
    | "OUTBOUND"
    | "PICKING"
    | "CONSOLIDATION"
    | "CLIENT_RETURNS"
    | "INVENTORY"
    | "UI"
    | "TSD_MANUAL_MOVEMENT"
    | "WMS_SUPPORT"
    | "NOT_SPECIFIED";

  /**
   * id
   * @format uuid
   */
  id: string;
}

export interface GetAllUsingGet2Params {
  /** transferId */
  transferId?: string[];
}

export interface GetPlaceUsingGetParams {
  /** barcode */
  barcode?: string;

  /**
   * placeId
   * @format uuid
   */
  placeId?: string;

  /** showChildren */
  showChildren?: boolean;

  /** siteCode */
  siteCode?: string;
}

export interface StartBulkUpdatePlaceStatusUsingPostParams {
  /** siteCode */
  siteCode: string;
}

export interface StartBulkUpdatePlaceTypeUsingPostParams {
  /** siteCode */
  siteCode: string;
}

export interface StartBulkUpdateZoneUsingPostParams {
  /** siteCode */
  siteCode: string;
}

export interface PlacesDownloadUsingGetParams {
  /** address */
  address?: string;

  /** siteCode */
  siteCode: string;
}

export interface FindPlacesByProcessIdUsingGetParams {
  /** placeTypeCode */
  placeTypeCode: string;

  /** processId */
  processId: string;

  /** showChildren */
  showChildren?: boolean;

  /** siteCode */
  siteCode: string;
}

export interface GetHierarchyByTypeCodeUsingGetParams {
  /** typeCode */
  typeCode: string;

  /**
   * siteId
   * @format uuid
   */
  siteId: string;
}

export interface GetPlaceByCodeUsingGetParams {
  /** placeBarcode */
  placeBarcode: string;

  /** siteCode */
  siteCode: string;
}

export interface GetAvailablePlaceTypesByParentPlaceIdUsingGetParams {
  /**
   * parentPlaceId
   * @format uuid
   */
  parentPlaceId?: string;

  /**
   * siteId
   * @format uuid
   */
  siteId: string;
}

export interface GetZoneTypesWithZonesUsingGet1Params {
  /**
   * siteId
   * @format uuid
   */
  siteId?: string;
}

export interface GetZonesByTypeUsingGetParams {
  /** siteCode */
  siteCode: string;

  /** zoneTypeCode */
  zoneTypeCode: string;
}

export interface GetZonesBySiteIdAndTypeIdUsingGetParams {
  /**
   * siteId
   * @format uuid
   */
  siteId: string;

  /**
   * typeId
   * @format uuid
   */
  typeId: string;
}

export namespace v1 {
  /**
   * @description operationId, productId, productBatchId - обязательные поля
   * @tags allocation-gateway
   * @name MonoPalletProductPlacementUsingPost
   * @summary Запрос в МРТ на размещение монопаллеты товара
   * @request POST:/v1/allocations/monoPalletProductPlacement
   * @secure
   * @response `200` `ProductPlacementResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MonoPalletProductPlacementUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = MonoPalletPlacementRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ProductPlacementResponse;
  }
  /**
   * @description operationId, productId, productBatchId - обязательные поля
   * @tags allocation-gateway
   * @name ProductPlacementUsingPost
   * @summary Запрос в МРТ на обычное (не паллетное) размещение товара
   * @request POST:/v1/allocations/productPlacement
   * @secure
   * @response `200` `ProductPlacementResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ProductPlacementUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ProductPlacementRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ProductPlacementResponse;
  }
  /**
   * @description operationId, productId, productBatchId - обязательные поля
   * @tags allocation-gateway
   * @name RawPlacementUsingPost
   * @summary Запрос в МРТ на размещение сырья
   * @request POST:/v1/allocations/rawPlacement
   * @secure
   * @response `200` `ProductPlacementResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace RawPlacementUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = RawPlacementRequest;
    export type RequestHeaders = {};
    export type ResponseBody = ProductPlacementResponse;
  }
  /**
   * No description
   * @tags carrier-audit-gateway
   * @name FindAllUsingPost
   * @summary История движения носителей
   * @request POST:/v1/audit/carrier/findAll
   * @secure
   * @response `200` `AuditCarrierRecordSearchResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindAllUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AuditCarrierRecordSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = AuditCarrierRecordSearchResponse;
  }
  /**
   * No description
   * @tags mark-audit-gateway
   * @name FindAllUsingPost1
   * @summary История движения товаров
   * @request POST:/v1/audit/mark/findAll
   * @secure
   * @response `200` `AuditMarkRecordSearchResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindAllUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AuditMarkRecordSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = AuditMarkRecordSearchResponse;
  }
  /**
   * No description
   * @tags stock-audit-gateway
   * @name FindAllUsingPost2
   * @summary История движения товаров
   * @request POST:/v1/audit/stock/findAll
   * @secure
   * @response `200` `AuditStockRecordSearchResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindAllUsingPost2 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AuditStockRecordSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = AuditStockRecordSearchResponse;
  }
  /**
   * No description
   * @tags picking-gateway
   * @name BoxRouteUsingPost
   * @summary Запрос в МKТ для маршрутизации коробов комплектации
   * @request POST:/v1/box-route/build
   * @secure
   * @response `200` `BoxRouteResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace BoxRouteUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = BoxRouteCmd;
    export type RequestHeaders = {};
    export type ResponseBody = BoxRouteResponse;
  }
  /**
   * No description
   * @tags picking-gateway
   * @name BoxRouteUsingPost1
   * @summary Запрос в МKТ для маршрутизации коробов комплектации
   * @request POST:/v1/box-routes/build
   * @secure
   * @response `200` `BoxRoute` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace BoxRouteUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = BuildBoxRouteCmd;
    export type RequestHeaders = {};
    export type ResponseBody = BoxRoute;
  }
  /**
   * @description uploadClusterDeliveryReservationRouteFile, siteCode - обязательные поля
   * @tags upload-gateway
   * @name ClusterDeliveryReservationRouteUsingPost
   * @summary Запрос на загрузку файла со змейкой по буферам отгрузки
   * @request POST:/v1/cluster-delivery-reservation-route/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ClusterDeliveryReservationRouteUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { uploadClusterDeliveryReservationRouteFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags hazelcast-gateway
   * @name DeletePlacesUsingPost
   * @summary Удаление мест из кэша для МРТ
   * @request POST:/v1/hazelcast/delete
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DeletePlacesUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DeletePlacesFromHazelcastCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags hazelcast-gateway
   * @name InitialPopulateUsingPost
   * @summary Начальное заполнение кэша мест для МРТ
   * @request POST:/v1/hazelcast/init
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace InitialPopulateUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = InitialPopulateHazelcastCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags marks-gateway
   * @name DeleteMarksUsingDelete
   * @summary Удаление марок
   * @request DELETE:/v1/marks
   * @secure
   * @response `200` `DeleteMarksResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DeleteMarksUsingDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DeleteMarksCmd;
    export type RequestHeaders = {};
    export type ResponseBody = DeleteMarksResponse;
  }
  /**
   * No description
   * @tags marks-gateway
   * @name ChangeMarksStatusUsingPost
   * @summary Изменение статусов марок
   * @request POST:/v1/marks/changeStatus
   * @secure
   * @response `200` `ChangeMarksStatusResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ChangeMarksStatusUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ChangeMarksStatusCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ChangeMarksStatusResponse;
  }
  /**
   * No description
   * @tags marks-gateway
   * @name CreateMarksUsingPost
   * @summary Диагностический эндпоинт, предназначен для сохранения марок
   * @request POST:/v1/marks/create
   * @secure
   * @response `200` `CreateMarksResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateMarksUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateMarksCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CreateMarksResponse;
  }
  /**
   * No description
   * @tags marks-gateway
   * @name CreateScannedChzMarkUsingPost
   * @summary Создание отсканированной марки ЧЗ при комплектовании, инвентаризации и т.п.
   * @request POST:/v1/marks/createScannedChzMark
   * @secure
   * @response `200` `CreateScannedChzMarkResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateScannedChzMarkUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateScannedChzMarkCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CreateScannedChzMarkResponse;
  }
  /**
   * No description
   * @tags marks-gateway
   * @name SearchUsingPost
   * @summary Поиск марок по параметрам, для внешних модулей
   * @request POST:/v1/marks/search
   * @secure
   * @response `200` `MarkSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = MarkSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = MarkSearchResult;
  }
  /**
   * No description
   * @tags marks-gateway
   * @name UpdateMarksUsingPost
   * @summary Диагностический эндпоинт, предназначен для изменения свойств марок
   * @request POST:/v1/marks/update
   * @secure
   * @response `200` `UpdateMarksResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateMarksUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UpdateMarksCmd;
    export type RequestHeaders = {};
    export type ResponseBody = UpdateMarksResponse;
  }
  /**
   * No description
   * @tags picking-gateway
   * @name PickerRoutesUsingPost
   * @summary Запрос в МKТ для текущего маршрута комплектовщика
   * @request POST:/v1/picker-routes/build
   * @secure
   * @response `200` `Route` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PickerRoutesUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = PickerRouteReq;
    export type RequestHeaders = {};
    export type ResponseBody = Route;
  }
  /**
   * No description
   * @tags picking-gateway
   * @name FindTransferUsingPost
   * @summary Запрос в МKТ для перехода между областями действий
   * @request POST:/v1/picker-transfer/build
   * @secure
   * @response `200` `Route` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindTransferUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = PickingZoneTransferReq;
    export type RequestHeaders = {};
    export type ResponseBody = Route;
  }
  /**
   * No description
   * @tags picking-gateway
   * @name GetConsolidationZoneUsingGet
   * @summary Запрос для получения области консолидации для области комплектации
   * @request GET:/v1/picking/consolidation-zone
   * @secure
   * @response `200` `ZoneDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetConsolidationZoneUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { zoneId: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneDto;
  }
  /**
   * No description
   * @tags place-reservation-search-gateway
   * @name SearchUsingPost1
   * @summary Поиск носителей, резевов под носители и резервов мест под процесс в определенных местах
   * @request POST:/v1/places/reservation/search
   * @secure
   * @response `200` `PlaceReservationSearchResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = PlaceReservationSearchRequest;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceReservationSearchResponse;
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name GetAllUsingGet1
   * @summary getAll
   * @request GET:/v1/product-batch
   * @secure
   * @response `200` `(ProductBatchDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetAllUsingGet1 {
    export type RequestParams = {};
    export type RequestQuery = {
      inboundId: string;
      manufactureTime?: number;
      productId: string;
      siteCode: string;
      type?: "REGULAR" | "RETURN";
      vendorCode: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProductBatchDto[];
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name AssignBatchUsingPost
   * @summary assignBatch
   * @request POST:/v1/product-batch
   * @secure
   * @response `200` `ProductBatchDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace AssignBatchUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AssignProductBatchCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ProductBatchDto;
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name GetBatchBalanceDifferenceUsingGet
   * @summary getBatchBalanceDifference
   * @request GET:/v1/product-batch/balance/{productBatchId}/difference
   * @secure
   * @response `200` `ProductBatchBalanceDifferenceDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetBatchBalanceDifferenceUsingGet {
    export type RequestParams = { productBatchId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProductBatchBalanceDifferenceDto;
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name FixBatchBalanceDifferenceUsingPost
   * @summary fixBatchBalanceDifference
   * @request POST:/v1/product-batch/balance/{productBatchId}/fix
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FixBatchBalanceDifferenceUsingPost {
    export type RequestParams = { productBatchId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name ConfirmBatchUsingPost
   * @summary "Обеление" вида запаса
   * @request POST:/v1/product-batch/confirm
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ConfirmBatchUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ConfirmProductBatchCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags product-batch-search-gateway
   * @name SearchUsingPost3
   * @summary Search Batches
   * @request POST:/v1/product-batch/search
   * @secure
   * @response `200` `ProductBatchesSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchUsingPost3 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ProductBatchesSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = ProductBatchesSearchResult;
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name SynchronizeBatchUsingPost
   * @summary synchronizeBatch
   * @request POST:/v1/product-batch/sync/{productBatchId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SynchronizeBatchUsingPost {
    export type RequestParams = { productBatchId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description batchesFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name UploadProductBatchesUsingPost
   * @summary Запрос на загрузку файла с партиями товара
   * @request POST:/v1/product-batch/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UploadProductBatchesUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags product-batch-gateway
   * @name GetByIdUsingGet1
   * @summary getById
   * @request GET:/v1/product-batch/{id}
   * @secure
   * @response `200` `ProductBatchDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetByIdUsingGet1 {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ProductBatchDto;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags replenishment-gateway
   * @name MoveStockBetweenCarriersUsingPost
   * @summary Перемещение запаса между носителями с рокировкой входящего резерва в месте
   * @request POST:/v1/replenishment/moveStockBetweenCarriers
   * @secure
   * @response `200` `MoveStockBetweenReplenishmentCarriersResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MoveStockBetweenCarriersUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = MoveStockBetweenReplenishmentCarriersCmd;
    export type RequestHeaders = {};
    export type ResponseBody = MoveStockBetweenReplenishmentCarriersResponse;
  }
  /**
   * No description
   * @tags replenishment-gateway
   * @name ReplenishUsingPost
   * @summary Размещение товара
   * @request POST:/v1/replenishment/replenish
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReplenishUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReplenishCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags replenishment-gateway
   * @name FindTransferUsingPost1
   * @summary Запрос на построение и резервирование маршрута для перехода между областями действий
   * @request POST:/v1/replenishment/transfer/build
   * @secure
   * @response `200` `Route` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindTransferUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AllocationZoneTransferReq;
    export type RequestHeaders = {};
    export type ResponseBody = Route;
  }
  /**
   * No description
   * @tags replenishment-gateway
   * @name TryAllocateSpaceRawUsingPost
   * @summary Проверка места перед ручным перемещением сырья
   * @request POST:/v1/replenishment/tryAllocateRaw
   * @secure
   * @response `200` `TryAllocateSpaceForReplenishmentResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace TryAllocateSpaceRawUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TryAllocateSpaceForReplenishmentCmd;
    export type RequestHeaders = {};
    export type ResponseBody = TryAllocateSpaceForReplenishmentResponse;
  }
  /**
   * No description
   * @tags replenishment-gateway
   * @name TryAllocateSpaceUsingPost
   * @summary Проверка места перед размещением товара
   * @request POST:/v1/replenishment/tryAllocateSpace
   * @secure
   * @response `200` `TryAllocateSpaceForReplenishmentResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace TryAllocateSpaceUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TryAllocateSpaceForReplenishmentCmd;
    export type RequestHeaders = {};
    export type ResponseBody = TryAllocateSpaceForReplenishmentResponse;
  }
  /**
   * No description
   * @tags replenishment-gateway
   * @name TryAllocateSpaceManualUsingPost
   * @summary Проверка места перед ручным перемещением товара
   * @request POST:/v1/replenishment/tryAllocateSpaceManual
   * @secure
   * @response `200` `TryAllocateSpaceForReplenishmentResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace TryAllocateSpaceManualUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TryAllocateSpaceForReplenishmentCmd;
    export type RequestHeaders = {};
    export type ResponseBody = TryAllocateSpaceForReplenishmentResponse;
  }
  /**
   * @description siteCode - обязательное поле
   * @tags download-gateway
   * @name ClusterDeliveryReservationRouteDownloadUsingGet
   * @summary Запрос на выгрузку змейки по буферам отгрузки в файл
   * @request GET:/v1/reservation/route/download/{siteCode}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ClusterDeliveryReservationRouteDownloadUsingGet {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description routeStartRuleFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name RouteStartRulesUploadUsingPost
   * @summary Запрос на загрузку файла правил мест старта
   * @request POST:/v1/route-start-rules/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace RouteStartRulesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags route-strategy-gateway
   * @name CreateStrategyUsingPost
   * @summary Запрос на создание стратегии обхода на площадке
   * @request POST:/v1/route-strategy
   * @secure
   * @response `200` `RouteStrategyDTO` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateStrategyUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateRouteStrategyCmd;
    export type RequestHeaders = {};
    export type ResponseBody = RouteStrategyDTO;
  }
  /**
   * No description
   * @tags download-gateway
   * @name RouteStrategyDownloadUsingGet
   * @summary Запрос на выгрузку файла с приоритетом обхода секций для стратегии обхода
   * @request GET:/v1/route-strategy/download/{strategyId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace RouteStrategyDownloadUsingGet {
    export type RequestParams = { strategyId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags route-strategy-gateway
   * @name GetRouteStrategiesUsingGet
   * @summary Запрос на получение всех стратегий обхода в рамках площадки
   * @request GET:/v1/route-strategy/strategies
   * @secure
   * @response `200` `(RouteStrategyDTO)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetRouteStrategiesUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { siteId: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RouteStrategyDTO[];
  }
  /**
   * No description
   * @tags route-strategy-gateway
   * @name GetRouteStrategyStepsUsingGet
   * @summary Запрос на получение змейки с порядком обхода секций для стратегии обхода в рамках площадки
   * @request GET:/v1/route-strategy/strategies/{routeStrategyId}
   * @secure
   * @response `200` `RouteStrategySteps` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetRouteStrategyStepsUsingGet {
    export type RequestParams = { routeStrategyId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = RouteStrategySteps;
  }
  /**
   * @description routeStrategyFile, siteCode - обязательные поля
   * @tags upload-gateway
   * @name UploadRouteStrategyUsingPost1
   * @summary Запрос на загрузку файла с приоритетом обхода секций
   * @request POST:/v1/route-strategy/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UploadRouteStrategyUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = string;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description routeStrategyFile, strategyId - обязательные поля
   * @tags upload-gateway
   * @name UploadRouteStrategyUsingPost
   * @summary Новый запрос на загрузку файла с приоритетом обхода секций
   * @request POST:/v1/route-strategy/upload-strategy/{strategyId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UploadRouteStrategyUsingPost {
    export type RequestParams = { strategyId: string };
    export type RequestQuery = {};
    export type RequestBody = { routeStrategyFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags route-strategy-gateway
   * @name UpdateRouteStrategyUsingPut
   * @summary Запрос на обновление стратегии обхода
   * @request PUT:/v1/route-strategy/{routeStrategyId}
   * @secure
   * @response `200` `RouteStrategyDTO` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateRouteStrategyUsingPut {
    export type RequestParams = { routeStrategyId: string };
    export type RequestQuery = {};
    export type RequestBody = UpdateRouteStrategyCmd;
    export type RequestHeaders = {};
    export type ResponseBody = RouteStrategyDTO;
  }
  /**
   * No description
   * @tags route-strategy-gateway
   * @name DeleteStrategyUsingDelete
   * @summary Запрос на удаление стратегии обхода
   * @request DELETE:/v1/route-strategy/{routeStrategyId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DeleteStrategyUsingDelete {
    export type RequestParams = { routeStrategyId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags route-gateway
   * @name ReservationRouteUsingGet
   * @summary Запрос змейки для подбора мест в отгрузке
   * @request GET:/v1/routes/reservation/{siteCode}
   * @secure
   * @response `200` `ClusterDeliveryReservationRoute` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReservationRouteUsingGet {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ClusterDeliveryReservationRoute;
  }
  /**
   * @description Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
   * @tags route-gateway
   * @name SlottingRouteUsingGet
   * @summary Запрос маршрута слотчика по области действий
   * @request GET:/v1/routes/{transferId}
   * @secure
   * @response `200` `SlottingRoute` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SlottingRouteUsingGet {
    export type RequestParams = { transferId: string };
    export type RequestQuery = { currentPlaceId?: string; startPlaceId: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SlottingRoute;
  }
  /**
   * No description
   * @tags snapshot-gateway
   * @name TriggerSnapshotUsingPost
   * @summary triggerSnapshot
   * @request POST:/v1/snapshot/{entityId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace TriggerSnapshotUsingPost {
    export type RequestParams = { entityId: string };
    export type RequestQuery = {
      entity: "PRODUCT_BATCH" | "STOCK" | "PLACE" | "PLACE_TYPE";
      type: "DELETE" | "SNAPSHOT";
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name GetAllUsingGet3
   * @summary getAll
   * @request GET:/v1/stock
   * @secure
   * @response `200` `(StockDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetAllUsingGet3 {
    export type RequestParams = {};
    export type RequestQuery = {
      carrierId?: string;
      placeBarcode?: string;
      placeId?: string;
      productBatchId?: string;
      productId?: string;
      siteCode?: string;
      stockTypeCode?: string;
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = StockDto[];
  }
  /**
   * No description
   * @tags stock-search-gateway
   * @name SearchUsingPost4
   * @summary Search Stocks by Addresses and Product Ids
   * @request POST:/v1/stock/addressAndProductIdsSearch
   * @secure
   * @response `200` `StocksSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchUsingPost4 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = StockAddressesAndProductIdsSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = StocksSearchResult;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name AnnihilateStockUsingDelete
   * @summary Уничтожение запаса без проводок в ERP
   * @request DELETE:/v1/stock/annihilate
   * @secure
   * @response `200` `StockAnnihilationResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace AnnihilateStockUsingDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = StockAnnihilationCmd;
    export type RequestHeaders = {};
    export type ResponseBody = StockAnnihilationResponse;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name CalculatePeiQuantityUsingPost
   * @summary Расчет кол-ва в ПЕИ
   * @request POST:/v1/stock/calculatePeiQuantity
   * @secure
   * @response `200` `CalculateNewProductPeiQuantityResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CalculatePeiQuantityUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CalculateNewProductPeiQuantityCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CalculateNewProductPeiQuantityResponse;
  }
  /**
   * No description
   * @tags carrier-type-gateway
   * @name GetCarrierTypesUsingGet
   * @summary getCarrierTypes
   * @request GET:/v1/stock/carrier-types
   * @secure
   * @response `200` `(CarrierTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetCarrierTypesUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { code?: string[] };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierTypeDto[];
  }
  /**
   * No description
   * @tags carrier-type-gateway
   * @name CreateUsingPost1
   * @summary create
   * @request POST:/v1/stock/carrier-types
   * @secure
   * @response `200` `CarrierTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateCarrierTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierTypeDto;
  }
  /**
   * No description
   * @tags carrier-type-gateway
   * @name CodeExistsUsingPost
   * @summary codeExists
   * @request POST:/v1/stock/carrier-types/codeExists
   * @secure
   * @response `200` `CodeExists` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CodeExistsUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CarrierTypeCodeExistsQuery;
    export type RequestHeaders = {};
    export type ResponseBody = CodeExists;
  }
  /**
   * @description carrierTypeFile - обязательные поля
   * @tags upload-gateway
   * @name CarrierTypesUploadUsingPost
   * @summary Запрос на загрузку файла с типами носителя
   * @request POST:/v1/stock/carrier-types/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CarrierTypesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { carrierTypeFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags carrier-type-gateway
   * @name UpdateUsingPut
   * @summary update
   * @request PUT:/v1/stock/carrier-types/{carrierTypeId}
   * @secure
   * @response `200` `CarrierTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateUsingPut {
    export type RequestParams = { carrierTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = UpdateCarrierTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierTypeDto;
  }
  /**
   * No description
   * @tags carrier-type-gateway
   * @name GetCarrierTypeUsingGet
   * @summary getCarrierType
   * @request GET:/v1/stock/carrier-types/{id}
   * @secure
   * @response `200` `CarrierTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetCarrierTypeUsingGet {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierTypeDto;
  }
  /**
   * No description
   * @tags carrier-gateway
   * @name GetAllUsingGet
   * @summary getAll
   * @request GET:/v1/stock/carriers
   * @secure
   * @response `200` `(CarrierDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetAllUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { barcode?: string; placeId?: string; rootOnly?: boolean };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierDto[];
  }
  /**
   * @description Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
   * @tags carrier-gateway
   * @name CreateUsingPost
   * @summary create a Carrier
   * @request POST:/v1/stock/carriers
   * @secure
   * @response `200` `CarrierDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateCarrierCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierDto;
  }
  /**
   * No description
   * @tags carrier-view-gateway
   * @name GetByPlaceIdUsingGet
   * @summary Get carriers hierarchy in place for grid representation
   * @request GET:/v1/stock/carriers-view
   * @secure
   * @response `200` `(CarrierGridView)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetByPlaceIdUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { placeId: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierGridView[];
  }
  /**
   * No description
   * @tags carrier-gateway
   * @name GetAllUsingPost
   * @summary getAll
   * @request POST:/v1/stock/carriers/find
   * @secure
   * @response `200` `(CarrierDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetAllUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DetailedCarriersSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierDto[];
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags reservation-carrier-gateway
   * @name ReserveForProcessUsingPost
   * @summary Резервирование носителя под процесс
   * @request POST:/v1/stock/carriers/reservation/for-process
   * @secure
   * @response `200` `ReserveCarrierForProcessResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveForProcessUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReserveCarrierForProcessCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReserveCarrierForProcessResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags reservation-carrier-gateway
   * @name DropReservationForProcessUsingDelete
   * @summary Сброс резерва носителя под процесс
   * @request DELETE:/v1/stock/carriers/reservation/for-process
   * @secure
   * @response `200` `DropCarrierReservationForProcessResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DropReservationForProcessUsingDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DropCarrierReservationForProcessCmd;
    export type RequestHeaders = {};
    export type ResponseBody = DropCarrierReservationForProcessResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags carrier-gateway
   * @name ShipUsingPost
   * @summary Отгрузка носителя
   * @request POST:/v1/stock/carriers/ship
   * @secure
   * @response `200` `CarrierShippedResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ShipUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ShipCarrierCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierShippedResponse;
  }
  /**
   * @description carrierFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name CarriersUploadUsingPost
   * @summary Запрос на загрузку файла с носителями
   * @request POST:/v1/stock/carriers/upload
   * @secure
   * @response `200` `CarrierUploadResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CarriersUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierUploadResponse;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name AddStockToCarrierUsingPut
   * @summary Добавление запаса в носитель
   * @request PUT:/v1/stock/carriers/{carrierId}
   * @secure
   * @response `200` `StockDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace AddStockToCarrierUsingPut {
    export type RequestParams = { carrierId: string };
    export type RequestQuery = {};
    export type RequestBody = AddStockToCarrierCmd;
    export type RequestHeaders = {};
    export type ResponseBody = StockDto;
  }
  /**
   * No description
   * @tags carrier-gateway
   * @name MoveCarrierIntoCarrierUsingPut
   * @summary moveCarrierIntoCarrier
   * @request PUT:/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MoveCarrierIntoCarrierUsingPut {
    export type RequestParams = { carrierId: string; parentCarrierId: string };
    export type RequestQuery = {
      parentProcessId?: string;
      processId?: string;
      processType?:
        | "INBOUND"
        | "REPLENISHMENT"
        | "MANUFACTURING"
        | "OUTBOUND"
        | "PICKING"
        | "CONSOLIDATION"
        | "CLIENT_RETURNS"
        | "INVENTORY"
        | "UI"
        | "TSD_MANUAL_MOVEMENT"
        | "WMS_SUPPORT"
        | "NOT_SPECIFIED";
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags carrier-gateway
   * @name GetByIdUsingGet
   * @summary getById
   * @request GET:/v1/stock/carriers/{id}
   * @secure
   * @response `200` `CarrierDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetByIdUsingGet {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CarrierDto;
  }
  /**
   * @description Delete a Carrier. CarrierId are required.
   * @tags carrier-gateway
   * @name DeleteUsingDelete
   * @summary delete a Carrier
   * @request DELETE:/v1/stock/carriers/{id}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DeleteUsingDelete {
    export type RequestParams = { id: string };
    export type RequestQuery = {
      processId?: string;
      processType?:
        | "INBOUND"
        | "REPLENISHMENT"
        | "MANUFACTURING"
        | "OUTBOUND"
        | "PICKING"
        | "CONSOLIDATION"
        | "CLIENT_RETURNS"
        | "INVENTORY"
        | "UI"
        | "TSD_MANUAL_MOVEMENT"
        | "WMS_SUPPORT"
        | "NOT_SPECIFIED";
    };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name ChangeStockProductUsingPost
   * @summary Изменение productId в существующем запасе
   * @request POST:/v1/stock/changeProduct
   * @secure
   * @response `200` `ChangeStockProductResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ChangeStockProductUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ChangeStockProductCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ChangeStockProductResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name ConfirmStocksUsingPost
   * @summary Отпуск сырья
   * @request POST:/v1/stock/changeRawStockType
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ConfirmStocksUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ChangeRawStockTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name CheckPlaceForStockUsingPost
   * @summary Проверка, может ли сток лежать в его месте
   * @request POST:/v1/stock/checkPlaceForStock
   * @secure
   * @response `200` `CheckPlaceForStockResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CheckPlaceForStockUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CheckPlaceForStockCmd;
    export type RequestHeaders = {};
    export type ResponseBody = CheckPlaceForStockResponse;
  }
  /**
   * @description siteCode - обязательное поле
   * @tags download-gateway
   * @name StocksDownloadUsingGet
   * @summary Запрос на выгрузку стоков
   * @request GET:/v1/stock/download/{siteCode}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StocksDownloadUsingGet {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name FindAllUsingPost3
   * @summary Детальный поиск по стокам
   * @request POST:/v1/stock/findAll
   * @secure
   * @response `200` `DetailedStocksSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindAllUsingPost3 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DetailedStockSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = DetailedStocksSearchResult;
  }
  /**
   * No description
   * @tags stock-search-gateway
   * @name FindAllUiUsingPost
   * @summary Поиск для грида запасов
   * @request POST:/v1/stock/findAllUI
   * @secure
   * @response `200` `UIStockSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindAllUiUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UIStockSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = UIStockSearchResult;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name MoveStockToCarrierUsingPut
   * @summary Перемещение запаса в носитель с корректировкой резерва
   * @request PUT:/v1/stock/moveAllTo
   * @secure
   * @response `200` `MoveAllStocksToCarrierResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MoveStockToCarrierUsingPut {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = MoveAllStocksToCarrierCmd;
    export type RequestHeaders = {};
    export type ResponseBody = MoveAllStocksToCarrierResponse;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name MoveStockUsingPost
   * @summary Перемещение запаса (без проверок, без корректировки резервов)
   * @request POST:/v1/stock/moveStock
   * @secure
   * @response `200` `MoveStockCmdResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MoveStockUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = MoveStockCmd;
    export type RequestHeaders = {};
    export type ResponseBody = MoveStockCmdResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name OutboundOrderUsingPost
   * @summary Отгрузка заказов
   * @request POST:/v1/stock/outboundOrder
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace OutboundOrderUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = OutboundOrderCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description reduce incoming quantity
   * @tags reservation-stock-gateway
   * @name ReduceIncomingQuantityUsingPost
   * @summary Уменьшение входящего резерва запаса
   * @request POST:/v1/stock/places/reduceIncomingQuantity
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReduceIncomingQuantityUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReduceIncomingQuantityCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей.
   * @tags reservation-stock-gateway
   * @name CancelStockReservationUsingPost
   * @summary Точечная отмена резерва запаса
   * @request POST:/v1/stock/places/reservation/cancelByStock
   * @secure
   * @response `200` `ReservationCancelledResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CancelStockReservationUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CancelStockReservationCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReservationCancelledResponse;
  }
  /**
   * No description
   * @tags reservation-place-gateway
   * @name ReserveUsingPost1
   * @summary Резервирование мест под носители
   * @request POST:/v1/stock/places/reservation/for-carriers
   * @secure
   * @response `200` `ReservedPlacesForCarriersResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReservePlacesForCarriersCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReservedPlacesForCarriersResponse;
  }
  /**
   * No description
   * @tags reservation-place-gateway
   * @name DropReservationUsingDelete
   * @summary Сброс резерва места под носитель
   * @request DELETE:/v1/stock/places/reservation/for-carriers
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DropReservationUsingDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DropPlaceReservationsForTransferCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags reservation-place-gateway
   * @name ReserveUsingPost
   * @summary Резервирование мест в шутах сортера
   * @request POST:/v1/stock/places/reservation/for-carriers/chute
   * @secure
   * @response `200` `ReservedChutePlacesForCarriersResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReserveChutePlacesForCarriersCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReservedChutePlacesForCarriersResponse;
  }
  /**
   * @description Сделан для оптимального использования шутов сортера
   * @tags reservation-place-gateway
   * @name ExchangeChuteReservationUsingPut
   * @summary Замена резерва под носители в месте с sourceTransferId на targetTransferId
   * @request PUT:/v1/stock/places/reservation/for-carriers/chute/exchange
   * @secure
   * @response `200` `ExchangeChuteReservationResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ExchangeChuteReservationUsingPut {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ExchangeChuteReservationCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ExchangeChuteReservationResponse;
  }
  /**
   * No description
   * @tags reservation-place-gateway
   * @name ReserveByClusterDeliveryUsingPost
   * @summary Резервирование мест под носители для кластерной доставки
   * @request POST:/v1/stock/places/reservation/for-carriers/cluster-delivery
   * @secure
   * @response `200` `ReservedPlacesForCarriersByClusterDeliveryResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveByClusterDeliveryUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReservePlacesForCarriersByClusterDeliveryCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReservedPlacesForCarriersByClusterDeliveryResponse;
  }
  /**
   * No description
   * @tags reservation-place-gateway
   * @name ReserveMultiProcessUsingPost
   * @summary Резервирование мест под носители для нескольких процессов
   * @request POST:/v1/stock/places/reservation/for-carriers/multi-process
   * @secure
   * @response `200` `ReservedPlacesForCarriersByMultiProcessesResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveMultiProcessUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReservePlacesForCarriersByMultyProcessesCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReservedPlacesForCarriersByMultiProcessesResponse;
  }
  /**
   * No description
   * @tags reservation-place-gateway
   * @name ReduceReservationForTransferUsingPut
   * @summary Уменьшить резерв под носители в месте
   * @request PUT:/v1/stock/places/reservation/for-carriers/reduce
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReduceReservationForTransferUsingPut {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReducePlaceReservationForTransferCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags reservation-place-gateway
   * @name ReserveForProcessUsingPost1
   * @summary Резервирование места под процесс
   * @request POST:/v1/stock/places/reservation/for-process
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveForProcessUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReservePlaceForProcessCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags reservation-place-gateway
   * @name DropAllReservationsForProcessUsingDelete
   * @summary Сброс резервов всех мест под конкретный процесс
   * @request DELETE:/v1/stock/places/reservation/for-process/all-places
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DropAllReservationsForProcessUsingDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DropAllProcessReservationsCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags reservation-place-gateway
   * @name DropReservationForProcessUsingDelete1
   * @summary Сброс резерва конкретного места под процесс
   * @request DELETE:/v1/stock/places/reservation/for-process/one-place
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DropReservationForProcessUsingDelete1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DropPlaceReservationForProcessCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags reservation-stock-gateway
   * @name GetAllUsingGet2
   * @summary Получение списка резервов запаса по списку transferId
   * @request GET:/v1/stock/places/reservations
   * @secure
   * @response `200` `(ReservationDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetAllUsingGet2 {
    export type RequestParams = {};
    export type RequestQuery = { transferId?: string[] };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ReservationDto[];
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags reservation-stock-gateway
   * @name ReserveOutgoingUsingPost
   * @summary Создание исходящего резерва запаса по id запаса
   * @request POST:/v1/stock/places/reserveOutgoingQuantity
   * @secure
   * @response `200` `ReserveStockOutgoingQuantityResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ReserveOutgoingUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ReserveStockOutgoingQuantityCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ReserveStockOutgoingQuantityResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для служебных целей
   * @tags reservation-stock-gateway
   * @name DropTransferUsingPost
   * @summary Отмена резервов запаса
   * @request POST:/v1/stock/places/transfers/drop
   * @secure
   * @response `200` `TransferDroppedResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DropTransferUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DropTransferCmd;
    export type RequestHeaders = {};
    export type ResponseBody = TransferDroppedResponse;
  }
  /**
   * No description
   * @tags stock-search-gateway
   * @name SearchUsingPost5
   * @summary Search Stocks
   * @request POST:/v1/stock/search
   * @secure
   * @response `200` `StocksSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchUsingPost5 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = StockHierarchySearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = StocksSearchResult;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name StockInUsingPost
   * @summary Добавление найденного товара в место или носитель без проверок
   * @request POST:/v1/stock/stock-in
   * @secure
   * @response `200` `StockInResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StockInUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = StockInCmd;
    export type RequestHeaders = {};
    export type ResponseBody = StockInResponse;
  }
  /**
   * No description
   * @tags stock-type-gateway
   * @name GetStockTypesUsingGet
   * @summary Запрос списка видов запаса
   * @request GET:/v1/stock/stock-types
   * @secure
   * @response `200` `(StockTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetStockTypesUsingGet {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = StockTypeDto[];
  }
  /**
   * @description code - обязательное поле
   * @tags stock-type-gateway
   * @name CreateStockTypeUsingPost
   * @summary Создание нового справочника вида запаса
   * @request POST:/v1/stock/stock-types
   * @secure
   * @response `200` `StockTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateStockTypeUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ModifyStockTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = StockTypeDto;
  }
  /**
   * No description
   * @tags stock-type-gateway
   * @name GetStockTypeByCodeUsingGet
   * @summary Запрос вида запаса по коду
   * @request GET:/v1/stock/stock-types/code/{code}
   * @secure
   * @response `200` `StockTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetStockTypeByCodeUsingGet {
    export type RequestParams = { code: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = StockTypeDto;
  }
  /**
   * No description
   * @tags stock-type-gateway
   * @name CodeExistsUsingGet
   * @summary Проверка существует ли вид запаса с указанным кодом
   * @request GET:/v1/stock/stock-types/codeExists/{code}
   * @secure
   * @response `200` `CodeExists` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CodeExistsUsingGet {
    export type RequestParams = { code: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = CodeExists;
  }
  /**
   * No description
   * @tags stock-type-gateway
   * @name GetStockTypeByIdUsingGet
   * @summary Запрос вида запаса по идентификатору
   * @request GET:/v1/stock/stock-types/{id}
   * @secure
   * @response `200` `StockTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetStockTypeByIdUsingGet {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = StockTypeDto;
  }
  /**
   * @description code - обязательное поле
   * @tags stock-type-gateway
   * @name UpdateStockTypeUsingPut
   * @summary Обновление вида запаса
   * @request PUT:/v1/stock/stock-types/{id}
   * @secure
   * @response `200` `StockTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateStockTypeUsingPut {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = ModifyStockTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = StockTypeDto;
  }
  /**
   * No description
   * @tags stock-type-gateway
   * @name ArchiveStockTypeUsingDelete
   * @summary Установка признака заархивирован
   * @request DELETE:/v1/stock/stock-types/{id}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ArchiveStockTypeUsingDelete {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name TryTakeUsingPost
   * @summary Запрос на попытку взять товар из ячейки
   * @request POST:/v1/stock/try-take
   * @secure
   * @response `200` `TryTakeResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace TryTakeUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = TryTakeRequest;
    export type RequestHeaders = {};
    export type ResponseBody = TryTakeResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name UpdateExpiredStockTypeUsingPost
   * @summary Перевод всех товаров с истекающим сроком реализации в SS
   * @request POST:/v1/stock/updateExpired
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateExpiredStockTypeUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name UpdateBakeryExpiredStockTypeUsingPost
   * @summary Перевод товаров пекарни с истекающим сроком реализации в SS
   * @request POST:/v1/stock/updateExpiredBakery
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateBakeryExpiredStockTypeUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name UpdateStockTypeInStockByInventoryUsingPost
   * @summary Изменение вида запаса инвентаризацией
   * @request POST:/v1/stock/updateStockTypeByInventory
   * @secure
   * @response `200` `UpdateStockTypeEvent` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateStockTypeInStockByInventoryUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UpdateStockTypeByInventoryCmd;
    export type RequestHeaders = {};
    export type ResponseBody = UpdateStockTypeEvent;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name UpdateStockTypeInStockByPickingUsingPost
   * @summary Изменение вида запаса комплектацией
   * @request POST:/v1/stock/updateStockTypeByPicking
   * @secure
   * @response `200` `UpdateStockTypeEvent` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateStockTypeInStockByPickingUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UpdateStockTypeByPickingCmd;
    export type RequestHeaders = {};
    export type ResponseBody = UpdateStockTypeEvent;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name UpdateStockTypeInStockByUiUsingPost
   * @summary Изменение вида запаса через UI
   * @request POST:/v1/stock/updateStockTypeByUI
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateStockTypeInStockByUiUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UpdateStockTypeInStockByUICmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name UpdateStockTypeForBatchUsingPost
   * @summary Изменение вида запаса для партии. Вызывается после обеления.
   * @request POST:/v1/stock/updateStockTypeForBatch
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateStockTypeForBatchUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UpdateStockTypeForBatchCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description stocksFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name StocksUploadUsingPost
   * @summary Запрос на загрузку файла с запасами
   * @request POST:/v1/stock/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StocksUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { stocksFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name WriteOffStockUsingPost
   * @summary Списание запаса
   * @request POST:/v1/stock/writeOff
   * @secure
   * @response `200` `WriteOffStockResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace WriteOffStockUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = WriteOffStockCmd;
    export type RequestHeaders = {};
    export type ResponseBody = WriteOffStockResponse;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name WriteOffStockByUiUsingPost
   * @summary Списание запаса
   * @request POST:/v1/stock/writeOffLost
   * @secure
   * @response `200` `WriteOffStockResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace WriteOffStockByUiUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = WriteOffStockByUICmd;
    export type RequestHeaders = {};
    export type ResponseBody = WriteOffStockResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags stock-gateway
   * @name WriteOffRawUsingPost
   * @summary Отпуск сырья с доинвентаризацией выбывшего запаса
   * @request POST:/v1/stock/writeOffRaw
   * @secure
   * @response `200` `WriteOffRawResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace WriteOffRawUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = WriteOffRawCmd;
    export type RequestHeaders = {};
    export type ResponseBody = WriteOffRawResponse;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name GetByIdUsingGet2
   * @summary getById
   * @request GET:/v1/stock/{id}
   * @secure
   * @response `200` `StockDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetByIdUsingGet2 {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = StockDto;
  }
  /**
   * No description
   * @tags stock-gateway
   * @name MoveStockToCarrierUsingPut1
   * @summary Перемещение запаса в носитель с корректировкой резерва
   * @request PUT:/v1/stock/{stockId}/moveTo/{carrierId}
   * @secure
   * @response `200` `StockDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MoveStockToCarrierUsingPut1 {
    export type RequestParams = { carrierId: string; stockId: string };
    export type RequestQuery = {};
    export type RequestBody = MoveStockToCarrierCmd;
    export type RequestHeaders = {};
    export type ResponseBody = StockDto;
  }
  /**
   * @description Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
   * @tags tool-gateway
   * @name OutboundOrderUsingPost1
   * @summary Отгрузка заказа
   * @request POST:/v1/tool/pushOrder
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace OutboundOrderUsingPost1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = OutboundOrderCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags zone-dictionary-gateway
   * @name GetZoneTypesUsingGet
   * @summary getZoneTypes
   * @request GET:/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones
   * @secure
   * @response `200` `(ZoneDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneTypesUsingGet {
    export type RequestParams = { siteId: string; zoneTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneDto[];
  }
  /**
   * No description
   * @tags place-type-dictionary-gateway
   * @name PlaceTypesUsingGet
   * @summary placeTypes
   * @request GET:/v1/topology/dictionary/sites/{siteId}/placeTypes
   * @secure
   * @response `200` `(PlaceTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PlaceTypesUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto[];
  }
  /**
   * No description
   * @tags zone-type-dictionary-gateway
   * @name GetZoneTypesUsingGet1
   * @summary getZoneTypes
   * @request GET:/v1/topology/dictionary/zoneTypes
   * @secure
   * @response `200` `(ZoneTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneTypesUsingGet1 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeDto[];
  }
  /**
   * No description
   * @tags place-status-gateway
   * @name GetPlaceStatusesUsingGet
   * @summary getPlaceStatuses
   * @request GET:/v1/topology/placeStatuses
   * @secure
   * @response `200` `(PlaceStatusDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceStatusesUsingGet {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceStatusDto[];
  }
  /**
   * @description siteCode - обязательные поля
   * @tags download-gateway
   * @name PlaceTypesDownloadUsingGet
   * @summary Запрос на выгрузку файла с типами мест
   * @request GET:/v1/topology/placeTypes/download/{siteCode}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PlaceTypesDownloadUsingGet {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags place-type-characteristic-gateway
   * @name GetPlaceTypeCharacteristicsUsingGet
   * @summary getPlaceTypeCharacteristics
   * @request GET:/v1/topology/placeTypes/{placeTypeId}/characteristics
   * @secure
   * @response `200` `(PlaceTypeCharacteristicDTO)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceTypeCharacteristicsUsingGet {
    export type RequestParams = { placeTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeCharacteristicDTO[];
  }
  /**
   * No description
   * @tags place-type-characteristic-gateway
   * @name AddUsingPost
   * @summary add
   * @request POST:/v1/topology/placeTypes/{placeTypeId}/characteristics
   * @secure
   * @response `200` `PlaceTypeCharacteristicDTO` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace AddUsingPost {
    export type RequestParams = { placeTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = AddPlaceTypeCommonCharacteristicDto;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeCharacteristicDTO;
  }
  /**
   * No description
   * @tags place-type-characteristic-gateway
   * @name GetPlaceTypeCharacteristicUsingGet
   * @summary getPlaceTypeCharacteristic
   * @request GET:/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}
   * @secure
   * @response `200` `PlaceTypeCharacteristicDTO` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceTypeCharacteristicUsingGet {
    export type RequestParams = { characteristicId: string; placeTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeCharacteristicDTO;
  }
  /**
   * No description
   * @tags place-type-characteristic-gateway
   * @name UpdateUsingPut2
   * @summary update
   * @request PUT:/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}
   * @secure
   * @response `200` `PlaceTypeCharacteristicDTO` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateUsingPut2 {
    export type RequestParams = { characteristicId: string; placeTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = UpdatePlaceTypeCommonCharacteristicDto;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeCharacteristicDTO;
  }
  /**
   * No description
   * @tags place-type-characteristic-gateway
   * @name RemoveUsingDelete
   * @summary remove
   * @request DELETE:/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace RemoveUsingDelete {
    export type RequestParams = { characteristicId: string; placeTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetPlaceUsingGet
   * @summary getPlace
   * @request GET:/v1/topology/places
   * @secure
   * @response `200` `PlaceDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { barcode?: string; placeId?: string; showChildren?: boolean; siteCode?: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceDto;
  }
  /**
   * No description
   * @tags place-gateway
   * @name CreateUsingPost2
   * @summary create
   * @request POST:/v1/topology/places
   * @secure
   * @response `200` `PlaceDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateUsingPost2 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreatePlaceCmd;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceDto;
  }
  /**
   * No description
   * @tags place-bulk-operations-gateway
   * @name StartBulkDeletePlacesUsingDelete
   * @summary startBulkDeletePlaces
   * @request DELETE:/v1/topology/places/bulk/delete-places
   * @secure
   * @response `200` `BulkOperationDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StartBulkDeletePlacesUsingDelete {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = BulkPlacesDeleteCmd;
    export type RequestHeaders = {};
    export type ResponseBody = BulkOperationDto;
  }
  /**
   * No description
   * @tags place-bulk-operations-gateway
   * @name StartBulkUpdatePlaceStatusUsingPost
   * @summary startBulkUpdatePlaceStatus
   * @request POST:/v1/topology/places/bulk/update-place-status
   * @secure
   * @response `200` `BulkOperationDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StartBulkUpdatePlaceStatusUsingPost {
    export type RequestParams = {};
    export type RequestQuery = { siteCode: string };
    export type RequestBody = BulkPlaceUpdateStatusCmd;
    export type RequestHeaders = {};
    export type ResponseBody = BulkOperationDto;
  }
  /**
   * No description
   * @tags place-bulk-operations-gateway
   * @name StartBulkUpdatePlaceTypeUsingPost
   * @summary startBulkUpdatePlaceType
   * @request POST:/v1/topology/places/bulk/update-place-type
   * @secure
   * @response `200` `BulkOperationDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StartBulkUpdatePlaceTypeUsingPost {
    export type RequestParams = {};
    export type RequestQuery = { siteCode: string };
    export type RequestBody = BulkPlaceUpdateTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = BulkOperationDto;
  }
  /**
   * No description
   * @tags place-bulk-operations-gateway
   * @name StartBulkUpdateZoneUsingPost
   * @summary startBulkUpdateZone
   * @request POST:/v1/topology/places/bulk/update-zone
   * @secure
   * @response `200` `BulkOperationDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StartBulkUpdateZoneUsingPost {
    export type RequestParams = {};
    export type RequestQuery = { siteCode: string };
    export type RequestBody = BulkPlaceUpdateZoneCmd;
    export type RequestHeaders = {};
    export type ResponseBody = BulkOperationDto;
  }
  /**
   * No description
   * @tags place-bulk-operations-gateway
   * @name GetBulkUpdateOperationDataUsingGet
   * @summary getBulkUpdateOperationData
   * @request GET:/v1/topology/places/bulk/{operationId}
   * @secure
   * @response `200` `BulkOperationDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetBulkUpdateOperationDataUsingGet {
    export type RequestParams = { operationId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = BulkOperationDto;
  }
  /**
   * @description siteCode - обязательные поля
   * @tags download-gateway
   * @name PlacesDownloadUsingGet
   * @summary Запрос на выгрузку файла с топологией
   * @request GET:/v1/topology/places/download/{siteCode}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PlacesDownloadUsingGet {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = { address?: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags place-gateway
   * @name FindPlacesByProcessIdUsingGet
   * @summary findPlacesByProcessId
   * @request GET:/v1/topology/places/findByProcessId
   * @secure
   * @response `200` `(PlaceDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace FindPlacesByProcessIdUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { placeTypeCode: string; processId: string; showChildren?: boolean; siteCode: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceDto[];
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetHierarchyUsingGet
   * @summary getHierarchy
   * @request GET:/v1/topology/places/hierarchy/{siteId}
   * @secure
   * @response `200` `PlaceNodeWithPosition` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetHierarchyUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceNodeWithPosition;
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetHierarchyForParentIdUsingGet
   * @summary getHierarchyForParentId
   * @request GET:/v1/topology/places/hierarchy/{siteId}/{parentId}
   * @secure
   * @response `200` `PlaceNodeWithPosition` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetHierarchyForParentIdUsingGet {
    export type RequestParams = { parentId: string; siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceNodeWithPosition;
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetHierarchyByTypeCodeUsingGet
   * @summary getHierarchyByTypeCode
   * @request GET:/v1/topology/places/hierarchyTree/{siteId}
   * @secure
   * @response `200` `(PlaceHierarchyTreeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetHierarchyByTypeCodeUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = { typeCode: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceHierarchyTreeDto[];
  }
  /**
   * No description
   * @tags place-search-gateway
   * @name SearchUsingPost2
   * @summary Search Places
   * @request POST:/v1/topology/places/search
   * @secure
   * @response `200` `PlacesSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchUsingPost2 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = PlacesSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = PlacesSearchResult;
  }
  /**
   * No description
   * @tags place-search-gateway
   * @name SearchDetailedUsingPost
   * @summary Универсальный поиск мест с иерархией
   * @request POST:/v1/topology/places/search/detailed
   * @secure
   * @response `200` `DetailedPlaceSearchResult` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SearchDetailedUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = DetailedPlaceSearchQuery;
    export type RequestHeaders = {};
    export type ResponseBody = DetailedPlaceSearchResult;
  }
  /**
   * No description
   * @tags place-search-gateway
   * @name GetPlaceTypesBySiteUsingGet
   * @summary getPlaceTypesBySite
   * @request GET:/v1/topology/places/search/dicts/placeTypes/{siteId}
   * @secure
   * @response `200` `(PlaceTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceTypesBySiteUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto[];
  }
  /**
   * No description
   * @tags place-search-gateway
   * @name GetZoneTypesWithZonesUsingGet
   * @summary getZoneTypesWithZones
   * @request GET:/v1/topology/places/search/dicts/zoneTypes/{siteId}
   * @secure
   * @response `200` `(ZoneTypeWithZones)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneTypesWithZonesUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeWithZones[];
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetPlaceByCodeUsingGet
   * @summary getPlaceByCode
   * @request GET:/v1/topology/places/shortView
   * @secure
   * @response `200` `(PlaceShortView)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceByCodeUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { placeBarcode: string; siteCode: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceShortView[];
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetTransferPlaceUsingGet
   * @summary getTransferPlace
   * @request GET:/v1/topology/places/transfer/{siteId}
   * @secure
   * @response `200` `PlaceShortView` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetTransferPlaceUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceShortView;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags place-gateway
   * @name UpdatePlaceStatusUsingPut
   * @summary Изменение статуса места с удалением входящих резервов
   * @request PUT:/v1/topology/places/updatePlaceStatus
   * @secure
   * @response `200` `UpdatePlaceStatusResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdatePlaceStatusUsingPut {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = UpdatePlaceStatusCmd;
    export type RequestHeaders = {};
    export type ResponseBody = UpdatePlaceStatusResponse;
  }
  /**
   * @description placesFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name PlacesUploadUsingPost
   * @summary Запрос на загрузку файла с топологией
   * @request POST:/v1/topology/places/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PlacesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags place-gateway
   * @name GetPlaceByIdUsingGet
   * @summary getPlaceById
   * @request GET:/v1/topology/places/{id}
   * @secure
   * @response `200` `PlaceWithPosition` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceByIdUsingGet {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceWithPosition;
  }
  /**
   * No description
   * @tags place-gateway
   * @name UpdateUsingPut1
   * @summary update
   * @request PUT:/v1/topology/places/{id}
   * @secure
   * @response `200` `PlaceDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateUsingPut1 {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = UpdatePlaceCmd;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceDto;
  }
  /**
   * No description
   * @tags place-gateway
   * @name DeleteUsingDelete1
   * @summary delete
   * @request DELETE:/v1/topology/places/{id}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DeleteUsingDelete1 {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags place-characteristic-gateway
   * @name GetPlaceCharacteristicsUsingGet
   * @summary getPlaceCharacteristics
   * @request GET:/v1/topology/places/{placeId}/characteristics
   * @secure
   * @response `200` `(PlaceCharacteristicDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceCharacteristicsUsingGet {
    export type RequestParams = { placeId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceCharacteristicDto[];
  }
  /**
   * No description
   * @tags place-characteristic-gateway
   * @name GetPlaceCharacteristicUsingGet
   * @summary getPlaceCharacteristic
   * @request GET:/v1/topology/places/{placeId}/characteristics/{characteristicId}
   * @secure
   * @response `200` `PlaceCharacteristicDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceCharacteristicUsingGet {
    export type RequestParams = { characteristicId: string; placeId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceCharacteristicDto;
  }
  /**
   * No description
   * @tags place-characteristic-gateway
   * @name OverrideUsingPut
   * @summary override
   * @request PUT:/v1/topology/places/{placeId}/characteristics/{characteristicId}
   * @secure
   * @response `200` `PlaceCharacteristicDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace OverrideUsingPut {
    export type RequestParams = { characteristicId: string; placeId: string };
    export type RequestQuery = {};
    export type RequestBody = OverridePlaceCommonCharacteristicCmd;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceCharacteristicDto;
  }
  /**
   * No description
   * @tags site-gateway
   * @name GetSitesUsingGet
   * @summary getSites
   * @request GET:/v1/topology/sites
   * @secure
   * @response `200` `(SiteDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetSitesUsingGet {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SiteDto[];
  }
  /**
   * No description
   * @tags site-gateway
   * @name CreateSiteUsingPost
   * @summary createSite
   * @request POST:/v1/topology/sites
   * @secure
   * @response `200` `SiteDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateSiteUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateSiteCmd;
    export type RequestHeaders = {};
    export type ResponseBody = SiteDto;
  }
  /**
   * No description
   * @tags site-gateway
   * @name GetSiteByCodeUsingGet
   * @summary getSiteByCode
   * @request GET:/v1/topology/sites/byCode/{code}
   * @secure
   * @response `200` `SiteDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetSiteByCodeUsingGet {
    export type RequestParams = { code: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SiteDto;
  }
  /**
   * No description
   * @tags site-gateway
   * @name CodeExistsUsingPost2
   * @summary codeExists
   * @request POST:/v1/topology/sites/codeExists
   * @secure
   * @response `200` `CodeExists` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CodeExistsUsingPost2 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = SiteCodeExistsQuery;
    export type RequestHeaders = {};
    export type ResponseBody = CodeExists;
  }
  /**
   * No description
   * @tags site-settings-gateway
   * @name GetSettingsForSiteUsingGet
   * @summary getSettingsForSite
   * @request GET:/v1/topology/sites/settings/{siteCode}
   * @secure
   * @response `200` `(SiteSettingDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetSettingsForSiteUsingGet {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SiteSettingDto[];
  }
  /**
   * No description
   * @tags site-settings-gateway
   * @name UpdateSettingsForSiteUsingPut
   * @summary updateSettingsForSite
   * @request PUT:/v1/topology/sites/settings/{siteCode}
   * @secure
   * @response `200` `(SiteSettingDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateSettingsForSiteUsingPut {
    export type RequestParams = { siteCode: string };
    export type RequestQuery = {};
    export type RequestBody = SiteSettingDto[];
    export type RequestHeaders = {};
    export type ResponseBody = SiteSettingDto[];
  }
  /**
   * No description
   * @tags site-gateway
   * @name GetSiteUsingGet
   * @summary getSite
   * @request GET:/v1/topology/sites/{siteId}
   * @secure
   * @response `200` `SiteDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetSiteUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = SiteDto;
  }
  /**
   * No description
   * @tags site-gateway
   * @name UpdateSiteUsingPut
   * @summary updateSite
   * @request PUT:/v1/topology/sites/{siteId}
   * @secure
   * @response `200` `SiteDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateSiteUsingPut {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = UpdateSiteCmd;
    export type RequestHeaders = {};
    export type ResponseBody = SiteDto;
  }
  /**
   * @description siteId - обязательное поле
   * @tags place-type-gateway
   * @name GetPlaceTypesBySiteUsingGet1
   * @summary Запрос на получение типов мест для площадки с количеством их экземпляров
   * @request GET:/v1/topology/sites/{siteId}/placeTypes
   * @secure
   * @response `200` `(PlaceTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceTypesBySiteUsingGet1 {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto[];
  }
  /**
   * No description
   * @tags place-type-gateway
   * @name CreatePlaceTypeUsingPost
   * @summary createPlaceType
   * @request POST:/v1/topology/sites/{siteId}/placeTypes
   * @secure
   * @response `200` `PlaceTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreatePlaceTypeUsingPost {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = CreatePlaceTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto;
  }
  /**
   * No description
   * @tags place-type-gateway
   * @name GetAvailablePlaceTypesByParentPlaceIdUsingGet
   * @summary getAvailablePlaceTypesByParentPlaceId
   * @request GET:/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId
   * @secure
   * @response `200` `(PlaceTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetAvailablePlaceTypesByParentPlaceIdUsingGet {
    export type RequestParams = { siteId: string };
    export type RequestQuery = { parentPlaceId?: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto[];
  }
  /**
   * No description
   * @tags place-type-gateway
   * @name CodeExistsUsingPost1
   * @summary codeExists
   * @request POST:/v1/topology/sites/{siteId}/placeTypes/codeExists
   * @secure
   * @response `200` `CodeExists` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CodeExistsUsingPost1 {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = PlaceTypeCodeExistsQuery;
    export type RequestHeaders = {};
    export type ResponseBody = CodeExists;
  }
  /**
   * @description placesFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name PlaceTypesUploadUsingPost
   * @summary Запрос на загрузку файла типов мест с характеристиками
   * @request POST:/v1/topology/sites/{siteId}/placeTypes/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PlaceTypesUploadUsingPost {
    export type RequestParams = { siteId: string };
    export type RequestQuery = {};
    export type RequestBody = { placesFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags place-type-gateway
   * @name GetPlaceTypeUsingGet
   * @summary getPlaceType
   * @request GET:/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}
   * @secure
   * @response `200` `PlaceTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetPlaceTypeUsingGet {
    export type RequestParams = { placeTypeId: string; siteId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto;
  }
  /**
   * No description
   * @tags place-type-gateway
   * @name UpdatePlaceTypeUsingPut
   * @summary updatePlaceType
   * @request PUT:/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}
   * @secure
   * @response `200` `PlaceTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdatePlaceTypeUsingPut {
    export type RequestParams = { placeTypeId: string; siteId: string };
    export type RequestQuery = {};
    export type RequestBody = UpdatePlaceTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceTypeDto;
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name GetZoneTypesUsingGet2
   * @summary getZoneTypes
   * @request GET:/v1/topology/zone-types
   * @secure
   * @response `200` `(ZoneTypeDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneTypesUsingGet2 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeDto[];
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name CreateUsingPost3
   * @summary create
   * @request POST:/v1/topology/zone-types
   * @secure
   * @response `200` `ZoneTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateUsingPost3 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateZoneTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeDto;
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name CodeExistsUsingPost4
   * @summary codeExists
   * @request POST:/v1/topology/zone-types/codeExists
   * @secure
   * @response `200` `CodeExists` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CodeExistsUsingPost4 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ZoneTypeCodeExistsQuery;
    export type RequestHeaders = {};
    export type ResponseBody = CodeExists;
  }
  /**
   * @description zoneTypeFile - обязательные поля
   * @tags upload-gateway
   * @name ZoneTypesUploadUsingPost
   * @summary Запрос на загрузку файла с типами зон
   * @request POST:/v1/topology/zone-types/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ZoneTypesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { zoneTypeFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name GetZoneTypesWithZonesUsingGet1
   * @summary getZoneTypesWithZones
   * @request GET:/v1/topology/zone-types/withZones
   * @secure
   * @response `200` `(ZoneTypeWithZonesDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneTypesWithZonesUsingGet1 {
    export type RequestParams = {};
    export type RequestQuery = { siteId?: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeWithZonesDto[];
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name GetZoneTypeUsingGet
   * @summary getZoneType
   * @request GET:/v1/topology/zone-types/{id}
   * @secure
   * @response `200` `ZoneTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneTypeUsingGet {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeDto;
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name DeleteUsingDelete2
   * @summary delete
   * @request DELETE:/v1/topology/zone-types/{id}
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace DeleteUsingDelete2 {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags zone-type-gateway
   * @name UpdateUsingPut3
   * @summary update
   * @request PUT:/v1/topology/zone-types/{zoneTypeId}
   * @secure
   * @response `200` `ZoneTypeDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateUsingPut3 {
    export type RequestParams = { zoneTypeId: string };
    export type RequestQuery = {};
    export type RequestBody = UpdateZoneTypeCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneTypeDto;
  }
  /**
   * No description
   * @tags download-gateway
   * @name ZoneTypesDownloadUsingGet
   * @summary Запрос на выгрузку файла с типами зон
   * @request GET:/v1/topology/zoneTypes/download
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ZoneTypesDownloadUsingGet {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags zone-gateway
   * @name GetZonesByTypeUsingGet
   * @summary getZonesByType
   * @request GET:/v1/topology/zones
   * @secure
   * @response `200` `(ZoneDto)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZonesByTypeUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { siteCode: string; zoneTypeCode: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneDto[];
  }
  /**
   * @description Create a Zone. code, siteId, typeId are required fields.
   * @tags zone-gateway
   * @name CreateZoneUsingPost
   * @summary create a Zone
   * @request POST:/v1/topology/zones
   * @secure
   * @response `200` `ZoneDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CreateZoneUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = CreateZoneCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneDto;
  }
  /**
   * No description
   * @tags zone-gateway
   * @name CodeExistsUsingPost3
   * @summary codeExists
   * @request POST:/v1/topology/zones/codeExists
   * @secure
   * @response `200` `CodeExists` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace CodeExistsUsingPost3 {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = ZoneCodeExistsQuery;
    export type RequestHeaders = {};
    export type ResponseBody = CodeExists;
  }
  /**
   * No description
   * @tags zone-projection-gateway
   * @name GetZonesBySiteIdAndTypeIdUsingGet
   * @summary getZonesBySiteIdAndTypeId
   * @request GET:/v1/topology/zones/projections
   * @secure
   * @response `200` `(ZoneProjection)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZonesBySiteIdAndTypeIdUsingGet {
    export type RequestParams = {};
    export type RequestQuery = { siteId: string; typeId: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneProjection[];
  }
  /**
   * @description zonesFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name ZonesUploadUsingPost
   * @summary Запрос на загрузку файла с зонами
   * @request POST:/v1/topology/zones/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ZonesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description zonesFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name PickingZonesUploadUsingPost
   * @summary Запрос на загрузку файла с областями комплектования
   * @request POST:/v1/topology/zones/upload/ok
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace PickingZonesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description zonesFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name StorageZonesUploadUsingPost
   * @summary Запрос на загрузку файла со складскими участками
   * @request POST:/v1/topology/zones/upload/su
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace StorageZonesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * No description
   * @tags zone-gateway
   * @name GetZoneByIdUsingGet
   * @summary getZoneById
   * @request GET:/v1/topology/zones/{id}
   * @secure
   * @response `200` `ZoneDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetZoneByIdUsingGet {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneDto;
  }
  /**
   * @description Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
   * @tags zone-gateway
   * @name UpdateZoneUsingPut
   * @summary update the Zone
   * @request PUT:/v1/topology/zones/{id}
   * @secure
   * @response `200` `ZoneDto` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UpdateZoneUsingPut {
    export type RequestParams = { id: string };
    export type RequestQuery = {};
    export type RequestBody = UpdateZoneCmd;
    export type RequestHeaders = {};
    export type ResponseBody = ZoneDto;
  }
  /**
   * No description
   * @tags transfer-gateway
   * @name AvailabilityAisleUsingPut
   * @summary Проверка доступности места для трансфера носителя
   * @request PUT:/v1/transfer/availabilityPlaceForTransferCarrier
   * @secure
   * @response `200` `AvailabilityPlaceForTransferCarrierResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace AvailabilityAisleUsingPut {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = AvailabilityPlaceForTransferCarrierCmd;
    export type RequestHeaders = {};
    export type ResponseBody = AvailabilityPlaceForTransferCarrierResponse;
  }
  /**
   * No description
   * @tags transfer-gateway
   * @name GetRouteUsingPost
   * @summary getRoute
   * @request POST:/v1/transfer/getRoute/{carrierId}
   * @secure
   * @response `200` `(PlaceShortView)[]` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace GetRouteUsingPost {
    export type RequestParams = { carrierId: string };
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = PlaceShortView[];
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags transfer-gateway
   * @name MoveCarrierToPlaceUsingPost
   * @summary Перенос носителя в место
   * @request POST:/v1/transfer/moveCarrierToPlace
   * @secure
   * @response `200` `MoveCarrierToPlaceResponse` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace MoveCarrierToPlaceUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = MoveCarrierToPlaceCmd;
    export type RequestHeaders = {};
    export type ResponseBody = MoveCarrierToPlaceResponse;
  }
  /**
   * @description Диагностический эндпоинт, предназначен только для тестовых целей
   * @tags transfer-gateway
   * @name SetCarrierPlaceUsingPost
   * @summary Размещение носителя в месте без проверок
   * @request POST:/v1/transfer/setCarrierPlace
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace SetCarrierPlaceUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = SetCarrierPlaceCmd;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description settingsFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name UploadWriteOffSettingsUsingPost
   * @summary Запрос на загрузку файла с настройками списания
   * @request POST:/v1/write-off-settings/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UploadWriteOffSettingsUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = any;
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description zoneCharacteristics, siteCode - обязательные поля
   * @tags upload-gateway
   * @name UploadZoneCharacteristicsUsingPost
   * @summary Запрос на загрузку файла с настройкой приоритетов секторов комплектования
   * @request POST:/v1/zone-characteristics/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace UploadZoneCharacteristicsUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { zoneCharacteristics: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description zoneRouteStrategy, siteCode - обязательные поля
   * @tags upload-gateway
   * @name BindToRouteStrategyUsingPost
   * @summary Запрос на загрузку файла с привязкой стратегии обхода змейки
   * @request POST:/v1/zone-route-strategy/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace BindToRouteStrategyUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { zoneRouteStrategy: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
  /**
   * @description zoneTransferRuleFile, siteId - обязательные поля
   * @tags upload-gateway
   * @name ZoneTransferRulesUploadUsingPost
   * @summary Запрос на загрузку файла правил переходов между местами
   * @request POST:/v1/zone-transfer-rules/upload
   * @secure
   * @response `200` `void` OK
   * @response `401` `void` Unauthorized
   * @response `403` `void` Forbidden
   * @response `500` `void` Internal Server Error
   */
  export namespace ZoneTransferRulesUploadUsingPost {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = { zoneTransferRuleFile: File };
    export type RequestHeaders = {};
    export type ResponseBody = void;
  }
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: keyof Omit<Body, "body" | "bodyUsed">;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType) => RequestParams | void;
}

/** Overrided Promise type. Needs for additional typings of `.catch` callback */
type TPromise<ResolveType, RejectType = any> = Omit<Promise<ResolveType>, "then" | "catch"> & {
  then<TResult1 = ResolveType, TResult2 = never>(
    onfulfilled?: ((value: ResolveType) => TResult1 | PromiseLike<TResult1>) | undefined | null,
    onrejected?: ((reason: RejectType) => TResult2 | PromiseLike<TResult2>) | undefined | null,
  ): TPromise<TResult1 | TResult2, RejectType>;
  catch<TResult = never>(
    onrejected?: ((reason: RejectType) => TResult | PromiseLike<TResult>) | undefined | null,
  ): TPromise<ResolveType | TResult, RejectType>;
};

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "//wms.utkonos.dev/api/warehouse";
  private securityData: SecurityDataType = null as any;
  private securityWorker: null | ApiConfig<SecurityDataType>["securityWorker"] = null;
  private abortControllers = new Map<CancelToken, AbortController>();

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType) => {
    this.securityData = data;
  };

  private addQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];

    return (
      encodeURIComponent(key) +
      "=" +
      encodeURIComponent(Array.isArray(value) ? value.join(",") : typeof value === "number" ? value : `${value}`)
    );
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) =>
        typeof query[key] === "object" && !Array.isArray(query[key])
          ? this.toQueryString(query[key] as QueryParamsType)
          : this.addQueryParam(query, key),
      )
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((data, key) => {
        data.append(key, input[key]);
        return data;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  private mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format = "json",
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): TPromise<HttpResponse<T, E>> => {
    const secureParams = (secure && this.securityWorker && this.securityWorker(this.securityData)) || {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];

    return fetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : void 0,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = (null as unknown) as T;
      r.error = (null as unknown) as E;

      const data = await response[format]()
        .then((data) => {
          if (r.ok) {
            r.data = data;
          } else {
            r.error = data;
          }
          return r;
        })
        .catch((e) => {
          r.error = e;
          return r;
        });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Api Documentation
 * @version unspecified
 * @baseUrl //wms.utkonos.dev/api/warehouse
 * Api Documentation
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  v1 = {
    /**
     * @description operationId, productId, productBatchId - обязательные поля
     *
     * @tags allocation-gateway
     * @name MonoPalletProductPlacementUsingPost
     * @summary Запрос в МРТ на размещение монопаллеты товара
     * @request POST:/v1/allocations/monoPalletProductPlacement
     * @secure
     * @response `200` `ProductPlacementResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    monoPalletProductPlacementUsingPost: (request: MonoPalletPlacementRequest, params: RequestParams = {}) =>
      this.request<ProductPlacementResponse, void>({
        path: `/v1/allocations/monoPalletProductPlacement`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description operationId, productId, productBatchId - обязательные поля
     *
     * @tags allocation-gateway
     * @name ProductPlacementUsingPost
     * @summary Запрос в МРТ на обычное (не паллетное) размещение товара
     * @request POST:/v1/allocations/productPlacement
     * @secure
     * @response `200` `ProductPlacementResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    productPlacementUsingPost: (request: ProductPlacementRequest, params: RequestParams = {}) =>
      this.request<ProductPlacementResponse, void>({
        path: `/v1/allocations/productPlacement`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description operationId, productId, productBatchId - обязательные поля
     *
     * @tags allocation-gateway
     * @name RawPlacementUsingPost
     * @summary Запрос в МРТ на размещение сырья
     * @request POST:/v1/allocations/rawPlacement
     * @secure
     * @response `200` `ProductPlacementResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    rawPlacementUsingPost: (request: RawPlacementRequest, params: RequestParams = {}) =>
      this.request<ProductPlacementResponse, void>({
        path: `/v1/allocations/rawPlacement`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-audit-gateway
     * @name FindAllUsingPost
     * @summary История движения носителей
     * @request POST:/v1/audit/carrier/findAll
     * @secure
     * @response `200` `AuditCarrierRecordSearchResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findAllUsingPost: (searchQuery: AuditCarrierRecordSearchQuery, params: RequestParams = {}) =>
      this.request<AuditCarrierRecordSearchResponse, void>({
        path: `/v1/audit/carrier/findAll`,
        method: "POST",
        body: searchQuery,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags mark-audit-gateway
     * @name FindAllUsingPost1
     * @summary История движения товаров
     * @request POST:/v1/audit/mark/findAll
     * @secure
     * @response `200` `AuditMarkRecordSearchResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findAllUsingPost1: (searchQuery: AuditMarkRecordSearchQuery, params: RequestParams = {}) =>
      this.request<AuditMarkRecordSearchResponse, void>({
        path: `/v1/audit/mark/findAll`,
        method: "POST",
        body: searchQuery,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-audit-gateway
     * @name FindAllUsingPost2
     * @summary История движения товаров
     * @request POST:/v1/audit/stock/findAll
     * @secure
     * @response `200` `AuditStockRecordSearchResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findAllUsingPost2: (searchQuery: AuditStockRecordSearchQuery, params: RequestParams = {}) =>
      this.request<AuditStockRecordSearchResponse, void>({
        path: `/v1/audit/stock/findAll`,
        method: "POST",
        body: searchQuery,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags picking-gateway
     * @name BoxRouteUsingPost
     * @summary Запрос в МKТ для маршрутизации коробов комплектации
     * @request POST:/v1/box-route/build
     * @secure
     * @response `200` `BoxRouteResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    boxRouteUsingPost: (cmd: BoxRouteCmd, params: RequestParams = {}) =>
      this.request<BoxRouteResponse, void>({
        path: `/v1/box-route/build`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags picking-gateway
     * @name BoxRouteUsingPost1
     * @summary Запрос в МKТ для маршрутизации коробов комплектации
     * @request POST:/v1/box-routes/build
     * @secure
     * @response `200` `BoxRoute` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    boxRouteUsingPost1: (cmd: BuildBoxRouteCmd, params: RequestParams = {}) =>
      this.request<BoxRoute, void>({
        path: `/v1/box-routes/build`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description uploadClusterDeliveryReservationRouteFile, siteCode - обязательные поля
     *
     * @tags upload-gateway
     * @name ClusterDeliveryReservationRouteUsingPost
     * @summary Запрос на загрузку файла со змейкой по буферам отгрузки
     * @request POST:/v1/cluster-delivery-reservation-route/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    clusterDeliveryReservationRouteUsingPost: (
      data: { uploadClusterDeliveryReservationRouteFile: File },
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/v1/cluster-delivery-reservation-route/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags hazelcast-gateway
     * @name DeletePlacesUsingPost
     * @summary Удаление мест из кэша для МРТ
     * @request POST:/v1/hazelcast/delete
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    deletePlacesUsingPost: (cmd: DeletePlacesFromHazelcastCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/hazelcast/delete`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags hazelcast-gateway
     * @name InitialPopulateUsingPost
     * @summary Начальное заполнение кэша мест для МРТ
     * @request POST:/v1/hazelcast/init
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    initialPopulateUsingPost: (cmd: InitialPopulateHazelcastCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/hazelcast/init`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags marks-gateway
     * @name DeleteMarksUsingDelete
     * @summary Удаление марок
     * @request DELETE:/v1/marks
     * @secure
     * @response `200` `DeleteMarksResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    deleteMarksUsingDelete: (request: DeleteMarksCmd, params: RequestParams = {}) =>
      this.request<DeleteMarksResponse, void>({
        path: `/v1/marks`,
        method: "DELETE",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags marks-gateway
     * @name ChangeMarksStatusUsingPost
     * @summary Изменение статусов марок
     * @request POST:/v1/marks/changeStatus
     * @secure
     * @response `200` `ChangeMarksStatusResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    changeMarksStatusUsingPost: (request: ChangeMarksStatusCmd, params: RequestParams = {}) =>
      this.request<ChangeMarksStatusResponse, void>({
        path: `/v1/marks/changeStatus`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags marks-gateway
     * @name CreateMarksUsingPost
     * @summary Диагностический эндпоинт, предназначен для сохранения марок
     * @request POST:/v1/marks/create
     * @secure
     * @response `200` `CreateMarksResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createMarksUsingPost: (request: CreateMarksCmd, params: RequestParams = {}) =>
      this.request<CreateMarksResponse, void>({
        path: `/v1/marks/create`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags marks-gateway
     * @name CreateScannedChzMarkUsingPost
     * @summary Создание отсканированной марки ЧЗ при комплектовании, инвентаризации и т.п.
     * @request POST:/v1/marks/createScannedChzMark
     * @secure
     * @response `200` `CreateScannedChzMarkResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createScannedChzMarkUsingPost: (request: CreateScannedChzMarkCmd, params: RequestParams = {}) =>
      this.request<CreateScannedChzMarkResponse, void>({
        path: `/v1/marks/createScannedChzMark`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags marks-gateway
     * @name SearchUsingPost
     * @summary Поиск марок по параметрам, для внешних модулей
     * @request POST:/v1/marks/search
     * @secure
     * @response `200` `MarkSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchUsingPost: (request: MarkSearchQuery, params: RequestParams = {}) =>
      this.request<MarkSearchResult, void>({
        path: `/v1/marks/search`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags marks-gateway
     * @name UpdateMarksUsingPost
     * @summary Диагностический эндпоинт, предназначен для изменения свойств марок
     * @request POST:/v1/marks/update
     * @secure
     * @response `200` `UpdateMarksResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateMarksUsingPost: (request: UpdateMarksCmd, params: RequestParams = {}) =>
      this.request<UpdateMarksResponse, void>({
        path: `/v1/marks/update`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags picking-gateway
     * @name PickerRoutesUsingPost
     * @summary Запрос в МKТ для текущего маршрута комплектовщика
     * @request POST:/v1/picker-routes/build
     * @secure
     * @response `200` `Route` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    pickerRoutesUsingPost: (cmd: PickerRouteReq, params: RequestParams = {}) =>
      this.request<Route, void>({
        path: `/v1/picker-routes/build`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags picking-gateway
     * @name FindTransferUsingPost
     * @summary Запрос в МKТ для перехода между областями действий
     * @request POST:/v1/picker-transfer/build
     * @secure
     * @response `200` `Route` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findTransferUsingPost: (cmd: PickingZoneTransferReq, params: RequestParams = {}) =>
      this.request<Route, void>({
        path: `/v1/picker-transfer/build`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags picking-gateway
     * @name GetConsolidationZoneUsingGet
     * @summary Запрос для получения области консолидации для области комплектации
     * @request GET:/v1/picking/consolidation-zone
     * @secure
     * @response `200` `ZoneDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getConsolidationZoneUsingGet: (query: GetConsolidationZoneUsingGetParams, params: RequestParams = {}) =>
      this.request<ZoneDto, void>({
        path: `/v1/picking/consolidation-zone`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-reservation-search-gateway
     * @name SearchUsingPost1
     * @summary Поиск носителей, резевов под носители и резервов мест под процесс в определенных местах
     * @request POST:/v1/places/reservation/search
     * @secure
     * @response `200` `PlaceReservationSearchResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchUsingPost1: (placeReservationSearchRequest: PlaceReservationSearchRequest, params: RequestParams = {}) =>
      this.request<PlaceReservationSearchResponse, void>({
        path: `/v1/places/reservation/search`,
        method: "POST",
        body: placeReservationSearchRequest,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name GetAllUsingGet1
     * @summary getAll
     * @request GET:/v1/product-batch
     * @secure
     * @response `200` `(ProductBatchDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getAllUsingGet1: (query: GetAllUsingGet1Params, params: RequestParams = {}) =>
      this.request<ProductBatchDto[], void>({
        path: `/v1/product-batch`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name AssignBatchUsingPost
     * @summary assignBatch
     * @request POST:/v1/product-batch
     * @secure
     * @response `200` `ProductBatchDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    assignBatchUsingPost: (cmd: AssignProductBatchCmd, params: RequestParams = {}) =>
      this.request<ProductBatchDto, void>({
        path: `/v1/product-batch`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name GetBatchBalanceDifferenceUsingGet
     * @summary getBatchBalanceDifference
     * @request GET:/v1/product-batch/balance/{productBatchId}/difference
     * @secure
     * @response `200` `ProductBatchBalanceDifferenceDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getBatchBalanceDifferenceUsingGet: (productBatchId: string, params: RequestParams = {}) =>
      this.request<ProductBatchBalanceDifferenceDto, void>({
        path: `/v1/product-batch/balance/${productBatchId}/difference`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name FixBatchBalanceDifferenceUsingPost
     * @summary fixBatchBalanceDifference
     * @request POST:/v1/product-batch/balance/{productBatchId}/fix
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    fixBatchBalanceDifferenceUsingPost: (productBatchId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/product-batch/balance/${productBatchId}/fix`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name ConfirmBatchUsingPost
     * @summary "Обеление" вида запаса
     * @request POST:/v1/product-batch/confirm
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    confirmBatchUsingPost: (confirmProductBatchCmd: ConfirmProductBatchCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/product-batch/confirm`,
        method: "POST",
        body: confirmProductBatchCmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-search-gateway
     * @name SearchUsingPost3
     * @summary Search Batches
     * @request POST:/v1/product-batch/search
     * @secure
     * @response `200` `ProductBatchesSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchUsingPost3: (query: ProductBatchesSearchQuery, params: RequestParams = {}) =>
      this.request<ProductBatchesSearchResult, void>({
        path: `/v1/product-batch/search`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name SynchronizeBatchUsingPost
     * @summary synchronizeBatch
     * @request POST:/v1/product-batch/sync/{productBatchId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    synchronizeBatchUsingPost: (productBatchId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/product-batch/sync/${productBatchId}`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description batchesFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name UploadProductBatchesUsingPost
     * @summary Запрос на загрузку файла с партиями товара
     * @request POST:/v1/product-batch/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    uploadProductBatchesUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/product-batch/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags product-batch-gateway
     * @name GetByIdUsingGet1
     * @summary getById
     * @request GET:/v1/product-batch/{id}
     * @secure
     * @response `200` `ProductBatchDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getByIdUsingGet1: (id: string, params: RequestParams = {}) =>
      this.request<ProductBatchDto, void>({
        path: `/v1/product-batch/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags replenishment-gateway
     * @name MoveStockBetweenCarriersUsingPost
     * @summary Перемещение запаса между носителями с рокировкой входящего резерва в месте
     * @request POST:/v1/replenishment/moveStockBetweenCarriers
     * @secure
     * @response `200` `MoveStockBetweenReplenishmentCarriersResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    moveStockBetweenCarriersUsingPost: (
      request: MoveStockBetweenReplenishmentCarriersCmd,
      params: RequestParams = {},
    ) =>
      this.request<MoveStockBetweenReplenishmentCarriersResponse, void>({
        path: `/v1/replenishment/moveStockBetweenCarriers`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags replenishment-gateway
     * @name ReplenishUsingPost
     * @summary Размещение товара
     * @request POST:/v1/replenishment/replenish
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    replenishUsingPost: (cmd: ReplenishCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/replenishment/replenish`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags replenishment-gateway
     * @name FindTransferUsingPost1
     * @summary Запрос на построение и резервирование маршрута для перехода между областями действий
     * @request POST:/v1/replenishment/transfer/build
     * @secure
     * @response `200` `Route` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findTransferUsingPost1: (cmd: AllocationZoneTransferReq, params: RequestParams = {}) =>
      this.request<Route, void>({
        path: `/v1/replenishment/transfer/build`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags replenishment-gateway
     * @name TryAllocateSpaceRawUsingPost
     * @summary Проверка места перед ручным перемещением сырья
     * @request POST:/v1/replenishment/tryAllocateRaw
     * @secure
     * @response `200` `TryAllocateSpaceForReplenishmentResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    tryAllocateSpaceRawUsingPost: (cmd: TryAllocateSpaceForReplenishmentCmd, params: RequestParams = {}) =>
      this.request<TryAllocateSpaceForReplenishmentResponse, void>({
        path: `/v1/replenishment/tryAllocateRaw`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags replenishment-gateway
     * @name TryAllocateSpaceUsingPost
     * @summary Проверка места перед размещением товара
     * @request POST:/v1/replenishment/tryAllocateSpace
     * @secure
     * @response `200` `TryAllocateSpaceForReplenishmentResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    tryAllocateSpaceUsingPost: (cmd: TryAllocateSpaceForReplenishmentCmd, params: RequestParams = {}) =>
      this.request<TryAllocateSpaceForReplenishmentResponse, void>({
        path: `/v1/replenishment/tryAllocateSpace`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags replenishment-gateway
     * @name TryAllocateSpaceManualUsingPost
     * @summary Проверка места перед ручным перемещением товара
     * @request POST:/v1/replenishment/tryAllocateSpaceManual
     * @secure
     * @response `200` `TryAllocateSpaceForReplenishmentResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    tryAllocateSpaceManualUsingPost: (cmd: TryAllocateSpaceForReplenishmentCmd, params: RequestParams = {}) =>
      this.request<TryAllocateSpaceForReplenishmentResponse, void>({
        path: `/v1/replenishment/tryAllocateSpaceManual`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description siteCode - обязательное поле
     *
     * @tags download-gateway
     * @name ClusterDeliveryReservationRouteDownloadUsingGet
     * @summary Запрос на выгрузку змейки по буферам отгрузки в файл
     * @request GET:/v1/reservation/route/download/{siteCode}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    clusterDeliveryReservationRouteDownloadUsingGet: (siteCode: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/reservation/route/download/${siteCode}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description routeStartRuleFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name RouteStartRulesUploadUsingPost
     * @summary Запрос на загрузку файла правил мест старта
     * @request POST:/v1/route-start-rules/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    routeStartRulesUploadUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/route-start-rules/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags route-strategy-gateway
     * @name CreateStrategyUsingPost
     * @summary Запрос на создание стратегии обхода на площадке
     * @request POST:/v1/route-strategy
     * @secure
     * @response `200` `RouteStrategyDTO` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createStrategyUsingPost: (cmd: CreateRouteStrategyCmd, params: RequestParams = {}) =>
      this.request<RouteStrategyDTO, void>({
        path: `/v1/route-strategy`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags download-gateway
     * @name RouteStrategyDownloadUsingGet
     * @summary Запрос на выгрузку файла с приоритетом обхода секций для стратегии обхода
     * @request GET:/v1/route-strategy/download/{strategyId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    routeStrategyDownloadUsingGet: (strategyId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/route-strategy/download/${strategyId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags route-strategy-gateway
     * @name GetRouteStrategiesUsingGet
     * @summary Запрос на получение всех стратегий обхода в рамках площадки
     * @request GET:/v1/route-strategy/strategies
     * @secure
     * @response `200` `(RouteStrategyDTO)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getRouteStrategiesUsingGet: (query: GetRouteStrategiesUsingGetParams, params: RequestParams = {}) =>
      this.request<RouteStrategyDTO[], void>({
        path: `/v1/route-strategy/strategies`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags route-strategy-gateway
     * @name GetRouteStrategyStepsUsingGet
     * @summary Запрос на получение змейки с порядком обхода секций для стратегии обхода в рамках площадки
     * @request GET:/v1/route-strategy/strategies/{routeStrategyId}
     * @secure
     * @response `200` `RouteStrategySteps` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getRouteStrategyStepsUsingGet: (routeStrategyId: string, params: RequestParams = {}) =>
      this.request<RouteStrategySteps, void>({
        path: `/v1/route-strategy/strategies/${routeStrategyId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description routeStrategyFile, siteCode - обязательные поля
     *
     * @tags upload-gateway
     * @name UploadRouteStrategyUsingPost1
     * @summary Запрос на загрузку файла с приоритетом обхода секций
     * @request POST:/v1/route-strategy/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    uploadRouteStrategyUsingPost1: (data: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/route-strategy/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description routeStrategyFile, strategyId - обязательные поля
     *
     * @tags upload-gateway
     * @name UploadRouteStrategyUsingPost
     * @summary Новый запрос на загрузку файла с приоритетом обхода секций
     * @request POST:/v1/route-strategy/upload-strategy/{strategyId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    uploadRouteStrategyUsingPost: (strategyId: string, data: { routeStrategyFile: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/route-strategy/upload-strategy/${strategyId}`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags route-strategy-gateway
     * @name UpdateRouteStrategyUsingPut
     * @summary Запрос на обновление стратегии обхода
     * @request PUT:/v1/route-strategy/{routeStrategyId}
     * @secure
     * @response `200` `RouteStrategyDTO` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateRouteStrategyUsingPut: (routeStrategyId: string, cmd: UpdateRouteStrategyCmd, params: RequestParams = {}) =>
      this.request<RouteStrategyDTO, void>({
        path: `/v1/route-strategy/${routeStrategyId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags route-strategy-gateway
     * @name DeleteStrategyUsingDelete
     * @summary Запрос на удаление стратегии обхода
     * @request DELETE:/v1/route-strategy/{routeStrategyId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    deleteStrategyUsingDelete: (routeStrategyId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/route-strategy/${routeStrategyId}`,
        method: "DELETE",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags route-gateway
     * @name ReservationRouteUsingGet
     * @summary Запрос змейки для подбора мест в отгрузке
     * @request GET:/v1/routes/reservation/{siteCode}
     * @secure
     * @response `200` `ClusterDeliveryReservationRoute` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reservationRouteUsingGet: (siteCode: string, params: RequestParams = {}) =>
      this.request<ClusterDeliveryReservationRoute, void>({
        path: `/v1/routes/reservation/${siteCode}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
     *
     * @tags route-gateway
     * @name SlottingRouteUsingGet
     * @summary Запрос маршрута слотчика по области действий
     * @request GET:/v1/routes/{transferId}
     * @secure
     * @response `200` `SlottingRoute` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    slottingRouteUsingGet: ({ transferId, ...query }: SlottingRouteUsingGetParams, params: RequestParams = {}) =>
      this.request<SlottingRoute, void>({
        path: `/v1/routes/${transferId}`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags snapshot-gateway
     * @name TriggerSnapshotUsingPost
     * @summary triggerSnapshot
     * @request POST:/v1/snapshot/{entityId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    triggerSnapshotUsingPost: ({ entityId, ...query }: TriggerSnapshotUsingPostParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/snapshot/${entityId}`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name GetAllUsingGet3
     * @summary getAll
     * @request GET:/v1/stock
     * @secure
     * @response `200` `(StockDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getAllUsingGet3: (query: GetAllUsingGet3Params, params: RequestParams = {}) =>
      this.request<StockDto[], void>({
        path: `/v1/stock`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-search-gateway
     * @name SearchUsingPost4
     * @summary Search Stocks by Addresses and Product Ids
     * @request POST:/v1/stock/addressAndProductIdsSearch
     * @secure
     * @response `200` `StocksSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchUsingPost4: (query: StockAddressesAndProductIdsSearchQuery, params: RequestParams = {}) =>
      this.request<StocksSearchResult, void>({
        path: `/v1/stock/addressAndProductIdsSearch`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name AnnihilateStockUsingDelete
     * @summary Уничтожение запаса без проводок в ERP
     * @request DELETE:/v1/stock/annihilate
     * @secure
     * @response `200` `StockAnnihilationResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    annihilateStockUsingDelete: (cmd: StockAnnihilationCmd, params: RequestParams = {}) =>
      this.request<StockAnnihilationResponse, void>({
        path: `/v1/stock/annihilate`,
        method: "DELETE",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name CalculatePeiQuantityUsingPost
     * @summary Расчет кол-ва в ПЕИ
     * @request POST:/v1/stock/calculatePeiQuantity
     * @secure
     * @response `200` `CalculateNewProductPeiQuantityResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    calculatePeiQuantityUsingPost: (cmd: CalculateNewProductPeiQuantityCmd, params: RequestParams = {}) =>
      this.request<CalculateNewProductPeiQuantityResponse, void>({
        path: `/v1/stock/calculatePeiQuantity`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-type-gateway
     * @name GetCarrierTypesUsingGet
     * @summary getCarrierTypes
     * @request GET:/v1/stock/carrier-types
     * @secure
     * @response `200` `(CarrierTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getCarrierTypesUsingGet: (query: GetCarrierTypesUsingGetParams, params: RequestParams = {}) =>
      this.request<CarrierTypeDto[], void>({
        path: `/v1/stock/carrier-types`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-type-gateway
     * @name CreateUsingPost1
     * @summary create
     * @request POST:/v1/stock/carrier-types
     * @secure
     * @response `200` `CarrierTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createUsingPost1: (cmd: CreateCarrierTypeCmd, params: RequestParams = {}) =>
      this.request<CarrierTypeDto, void>({
        path: `/v1/stock/carrier-types`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-type-gateway
     * @name CodeExistsUsingPost
     * @summary codeExists
     * @request POST:/v1/stock/carrier-types/codeExists
     * @secure
     * @response `200` `CodeExists` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    codeExistsUsingPost: (query: CarrierTypeCodeExistsQuery, params: RequestParams = {}) =>
      this.request<CodeExists, void>({
        path: `/v1/stock/carrier-types/codeExists`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description carrierTypeFile - обязательные поля
     *
     * @tags upload-gateway
     * @name CarrierTypesUploadUsingPost
     * @summary Запрос на загрузку файла с типами носителя
     * @request POST:/v1/stock/carrier-types/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    carrierTypesUploadUsingPost: (data: { carrierTypeFile: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/carrier-types/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-type-gateway
     * @name UpdateUsingPut
     * @summary update
     * @request PUT:/v1/stock/carrier-types/{carrierTypeId}
     * @secure
     * @response `200` `CarrierTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateUsingPut: (carrierTypeId: string, cmd: UpdateCarrierTypeCmd, params: RequestParams = {}) =>
      this.request<CarrierTypeDto, void>({
        path: `/v1/stock/carrier-types/${carrierTypeId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-type-gateway
     * @name GetCarrierTypeUsingGet
     * @summary getCarrierType
     * @request GET:/v1/stock/carrier-types/{id}
     * @secure
     * @response `200` `CarrierTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getCarrierTypeUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<CarrierTypeDto, void>({
        path: `/v1/stock/carrier-types/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-gateway
     * @name GetAllUsingGet
     * @summary getAll
     * @request GET:/v1/stock/carriers
     * @secure
     * @response `200` `(CarrierDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getAllUsingGet: (query: GetAllUsingGetParams, params: RequestParams = {}) =>
      this.request<CarrierDto[], void>({
        path: `/v1/stock/carriers`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
     *
     * @tags carrier-gateway
     * @name CreateUsingPost
     * @summary create a Carrier
     * @request POST:/v1/stock/carriers
     * @secure
     * @response `200` `CarrierDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createUsingPost: (cmd: CreateCarrierCmd, params: RequestParams = {}) =>
      this.request<CarrierDto, void>({
        path: `/v1/stock/carriers`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-view-gateway
     * @name GetByPlaceIdUsingGet
     * @summary Get carriers hierarchy in place for grid representation
     * @request GET:/v1/stock/carriers-view
     * @secure
     * @response `200` `(CarrierGridView)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getByPlaceIdUsingGet: (query: GetByPlaceIdUsingGetParams, params: RequestParams = {}) =>
      this.request<CarrierGridView[], void>({
        path: `/v1/stock/carriers-view`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-gateway
     * @name GetAllUsingPost
     * @summary getAll
     * @request POST:/v1/stock/carriers/find
     * @secure
     * @response `200` `(CarrierDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getAllUsingPost: (detailedCarriersSearchQuery: DetailedCarriersSearchQuery, params: RequestParams = {}) =>
      this.request<CarrierDto[], void>({
        path: `/v1/stock/carriers/find`,
        method: "POST",
        body: detailedCarriersSearchQuery,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags reservation-carrier-gateway
     * @name ReserveForProcessUsingPost
     * @summary Резервирование носителя под процесс
     * @request POST:/v1/stock/carriers/reservation/for-process
     * @secure
     * @response `200` `ReserveCarrierForProcessResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveForProcessUsingPost: (cmd: ReserveCarrierForProcessCmd, params: RequestParams = {}) =>
      this.request<ReserveCarrierForProcessResponse, void>({
        path: `/v1/stock/carriers/reservation/for-process`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags reservation-carrier-gateway
     * @name DropReservationForProcessUsingDelete
     * @summary Сброс резерва носителя под процесс
     * @request DELETE:/v1/stock/carriers/reservation/for-process
     * @secure
     * @response `200` `DropCarrierReservationForProcessResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    dropReservationForProcessUsingDelete: (cmd: DropCarrierReservationForProcessCmd, params: RequestParams = {}) =>
      this.request<DropCarrierReservationForProcessResponse, void>({
        path: `/v1/stock/carriers/reservation/for-process`,
        method: "DELETE",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags carrier-gateway
     * @name ShipUsingPost
     * @summary Отгрузка носителя
     * @request POST:/v1/stock/carriers/ship
     * @secure
     * @response `200` `CarrierShippedResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    shipUsingPost: (cmd: ShipCarrierCmd, params: RequestParams = {}) =>
      this.request<CarrierShippedResponse, void>({
        path: `/v1/stock/carriers/ship`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description carrierFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name CarriersUploadUsingPost
     * @summary Запрос на загрузку файла с носителями
     * @request POST:/v1/stock/carriers/upload
     * @secure
     * @response `200` `CarrierUploadResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    carriersUploadUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<CarrierUploadResponse, void>({
        path: `/v1/stock/carriers/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name AddStockToCarrierUsingPut
     * @summary Добавление запаса в носитель
     * @request PUT:/v1/stock/carriers/{carrierId}
     * @secure
     * @response `200` `StockDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    addStockToCarrierUsingPut: (carrierId: string, cmd: AddStockToCarrierCmd, params: RequestParams = {}) =>
      this.request<StockDto, void>({
        path: `/v1/stock/carriers/${carrierId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-gateway
     * @name MoveCarrierIntoCarrierUsingPut
     * @summary moveCarrierIntoCarrier
     * @request PUT:/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    moveCarrierIntoCarrierUsingPut: (
      { carrierId, parentCarrierId, ...query }: MoveCarrierIntoCarrierUsingPutParams,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/v1/stock/carriers/${carrierId}/moveInto/${parentCarrierId}`,
        method: "PUT",
        query: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags carrier-gateway
     * @name GetByIdUsingGet
     * @summary getById
     * @request GET:/v1/stock/carriers/{id}
     * @secure
     * @response `200` `CarrierDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getByIdUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<CarrierDto, void>({
        path: `/v1/stock/carriers/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description Delete a Carrier. CarrierId are required.
     *
     * @tags carrier-gateway
     * @name DeleteUsingDelete
     * @summary delete a Carrier
     * @request DELETE:/v1/stock/carriers/{id}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    deleteUsingDelete: ({ id, ...query }: DeleteUsingDeleteParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/carriers/${id}`,
        method: "DELETE",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name ChangeStockProductUsingPost
     * @summary Изменение productId в существующем запасе
     * @request POST:/v1/stock/changeProduct
     * @secure
     * @response `200` `ChangeStockProductResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    changeStockProductUsingPost: (cmd: ChangeStockProductCmd, params: RequestParams = {}) =>
      this.request<ChangeStockProductResponse, void>({
        path: `/v1/stock/changeProduct`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name ConfirmStocksUsingPost
     * @summary Отпуск сырья
     * @request POST:/v1/stock/changeRawStockType
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    confirmStocksUsingPost: (cmd: ChangeRawStockTypeCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/changeRawStockType`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name CheckPlaceForStockUsingPost
     * @summary Проверка, может ли сток лежать в его месте
     * @request POST:/v1/stock/checkPlaceForStock
     * @secure
     * @response `200` `CheckPlaceForStockResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    checkPlaceForStockUsingPost: (cmd: CheckPlaceForStockCmd, params: RequestParams = {}) =>
      this.request<CheckPlaceForStockResponse, void>({
        path: `/v1/stock/checkPlaceForStock`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description siteCode - обязательное поле
     *
     * @tags download-gateway
     * @name StocksDownloadUsingGet
     * @summary Запрос на выгрузку стоков
     * @request GET:/v1/stock/download/{siteCode}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    stocksDownloadUsingGet: (siteCode: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/download/${siteCode}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name FindAllUsingPost3
     * @summary Детальный поиск по стокам
     * @request POST:/v1/stock/findAll
     * @secure
     * @response `200` `DetailedStocksSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findAllUsingPost3: (query: DetailedStockSearchQuery, params: RequestParams = {}) =>
      this.request<DetailedStocksSearchResult, void>({
        path: `/v1/stock/findAll`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-search-gateway
     * @name FindAllUiUsingPost
     * @summary Поиск для грида запасов
     * @request POST:/v1/stock/findAllUI
     * @secure
     * @response `200` `UIStockSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findAllUiUsingPost: (query: UIStockSearchQuery, params: RequestParams = {}) =>
      this.request<UIStockSearchResult, void>({
        path: `/v1/stock/findAllUI`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name MoveStockToCarrierUsingPut
     * @summary Перемещение запаса в носитель с корректировкой резерва
     * @request PUT:/v1/stock/moveAllTo
     * @secure
     * @response `200` `MoveAllStocksToCarrierResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    moveStockToCarrierUsingPut: (cmd: MoveAllStocksToCarrierCmd, params: RequestParams = {}) =>
      this.request<MoveAllStocksToCarrierResponse, void>({
        path: `/v1/stock/moveAllTo`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name MoveStockUsingPost
     * @summary Перемещение запаса (без проверок, без корректировки резервов)
     * @request POST:/v1/stock/moveStock
     * @secure
     * @response `200` `MoveStockCmdResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    moveStockUsingPost: (cmd: MoveStockCmd, params: RequestParams = {}) =>
      this.request<MoveStockCmdResponse, void>({
        path: `/v1/stock/moveStock`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name OutboundOrderUsingPost
     * @summary Отгрузка заказов
     * @request POST:/v1/stock/outboundOrder
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    outboundOrderUsingPost: (cmd: OutboundOrderCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/outboundOrder`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description reduce incoming quantity
     *
     * @tags reservation-stock-gateway
     * @name ReduceIncomingQuantityUsingPost
     * @summary Уменьшение входящего резерва запаса
     * @request POST:/v1/stock/places/reduceIncomingQuantity
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reduceIncomingQuantityUsingPost: (request: ReduceIncomingQuantityCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/places/reduceIncomingQuantity`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей.
     *
     * @tags reservation-stock-gateway
     * @name CancelStockReservationUsingPost
     * @summary Точечная отмена резерва запаса
     * @request POST:/v1/stock/places/reservation/cancelByStock
     * @secure
     * @response `200` `ReservationCancelledResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    cancelStockReservationUsingPost: (cmd: CancelStockReservationCmd, params: RequestParams = {}) =>
      this.request<ReservationCancelledResponse, void>({
        path: `/v1/stock/places/reservation/cancelByStock`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-place-gateway
     * @name ReserveUsingPost1
     * @summary Резервирование мест под носители
     * @request POST:/v1/stock/places/reservation/for-carriers
     * @secure
     * @response `200` `ReservedPlacesForCarriersResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveUsingPost1: (cmd: ReservePlacesForCarriersCmd, params: RequestParams = {}) =>
      this.request<ReservedPlacesForCarriersResponse, void>({
        path: `/v1/stock/places/reservation/for-carriers`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-place-gateway
     * @name DropReservationUsingDelete
     * @summary Сброс резерва места под носитель
     * @request DELETE:/v1/stock/places/reservation/for-carriers
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    dropReservationUsingDelete: (cmd: DropPlaceReservationsForTransferCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/places/reservation/for-carriers`,
        method: "DELETE",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-place-gateway
     * @name ReserveUsingPost
     * @summary Резервирование мест в шутах сортера
     * @request POST:/v1/stock/places/reservation/for-carriers/chute
     * @secure
     * @response `200` `ReservedChutePlacesForCarriersResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveUsingPost: (cmd: ReserveChutePlacesForCarriersCmd, params: RequestParams = {}) =>
      this.request<ReservedChutePlacesForCarriersResponse, void>({
        path: `/v1/stock/places/reservation/for-carriers/chute`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Сделан для оптимального использования шутов сортера
     *
     * @tags reservation-place-gateway
     * @name ExchangeChuteReservationUsingPut
     * @summary Замена резерва под носители в месте с sourceTransferId на targetTransferId
     * @request PUT:/v1/stock/places/reservation/for-carriers/chute/exchange
     * @secure
     * @response `200` `ExchangeChuteReservationResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    exchangeChuteReservationUsingPut: (cmd: ExchangeChuteReservationCmd, params: RequestParams = {}) =>
      this.request<ExchangeChuteReservationResponse, void>({
        path: `/v1/stock/places/reservation/for-carriers/chute/exchange`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-place-gateway
     * @name ReserveByClusterDeliveryUsingPost
     * @summary Резервирование мест под носители для кластерной доставки
     * @request POST:/v1/stock/places/reservation/for-carriers/cluster-delivery
     * @secure
     * @response `200` `ReservedPlacesForCarriersByClusterDeliveryResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveByClusterDeliveryUsingPost: (
      cmd: ReservePlacesForCarriersByClusterDeliveryCmd,
      params: RequestParams = {},
    ) =>
      this.request<ReservedPlacesForCarriersByClusterDeliveryResponse, void>({
        path: `/v1/stock/places/reservation/for-carriers/cluster-delivery`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-place-gateway
     * @name ReserveMultiProcessUsingPost
     * @summary Резервирование мест под носители для нескольких процессов
     * @request POST:/v1/stock/places/reservation/for-carriers/multi-process
     * @secure
     * @response `200` `ReservedPlacesForCarriersByMultiProcessesResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveMultiProcessUsingPost: (cmd: ReservePlacesForCarriersByMultyProcessesCmd, params: RequestParams = {}) =>
      this.request<ReservedPlacesForCarriersByMultiProcessesResponse, void>({
        path: `/v1/stock/places/reservation/for-carriers/multi-process`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-place-gateway
     * @name ReduceReservationForTransferUsingPut
     * @summary Уменьшить резерв под носители в месте
     * @request PUT:/v1/stock/places/reservation/for-carriers/reduce
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reduceReservationForTransferUsingPut: (cmd: ReducePlaceReservationForTransferCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/places/reservation/for-carriers/reduce`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags reservation-place-gateway
     * @name ReserveForProcessUsingPost1
     * @summary Резервирование места под процесс
     * @request POST:/v1/stock/places/reservation/for-process
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveForProcessUsingPost1: (cmd: ReservePlaceForProcessCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/places/reservation/for-process`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags reservation-place-gateway
     * @name DropAllReservationsForProcessUsingDelete
     * @summary Сброс резервов всех мест под конкретный процесс
     * @request DELETE:/v1/stock/places/reservation/for-process/all-places
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    dropAllReservationsForProcessUsingDelete: (cmd: DropAllProcessReservationsCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/places/reservation/for-process/all-places`,
        method: "DELETE",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags reservation-place-gateway
     * @name DropReservationForProcessUsingDelete1
     * @summary Сброс резерва конкретного места под процесс
     * @request DELETE:/v1/stock/places/reservation/for-process/one-place
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    dropReservationForProcessUsingDelete1: (cmd: DropPlaceReservationForProcessCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/places/reservation/for-process/one-place`,
        method: "DELETE",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags reservation-stock-gateway
     * @name GetAllUsingGet2
     * @summary Получение списка резервов запаса по списку transferId
     * @request GET:/v1/stock/places/reservations
     * @secure
     * @response `200` `(ReservationDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getAllUsingGet2: (query: GetAllUsingGet2Params, params: RequestParams = {}) =>
      this.request<ReservationDto[], void>({
        path: `/v1/stock/places/reservations`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags reservation-stock-gateway
     * @name ReserveOutgoingUsingPost
     * @summary Создание исходящего резерва запаса по id запаса
     * @request POST:/v1/stock/places/reserveOutgoingQuantity
     * @secure
     * @response `200` `ReserveStockOutgoingQuantityResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    reserveOutgoingUsingPost: (cmd: ReserveStockOutgoingQuantityCmd, params: RequestParams = {}) =>
      this.request<ReserveStockOutgoingQuantityResponse, void>({
        path: `/v1/stock/places/reserveOutgoingQuantity`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для служебных целей
     *
     * @tags reservation-stock-gateway
     * @name DropTransferUsingPost
     * @summary Отмена резервов запаса
     * @request POST:/v1/stock/places/transfers/drop
     * @secure
     * @response `200` `TransferDroppedResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    dropTransferUsingPost: (cmd: DropTransferCmd, params: RequestParams = {}) =>
      this.request<TransferDroppedResponse, void>({
        path: `/v1/stock/places/transfers/drop`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-search-gateway
     * @name SearchUsingPost5
     * @summary Search Stocks
     * @request POST:/v1/stock/search
     * @secure
     * @response `200` `StocksSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchUsingPost5: (query: StockHierarchySearchQuery, params: RequestParams = {}) =>
      this.request<StocksSearchResult, void>({
        path: `/v1/stock/search`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name StockInUsingPost
     * @summary Добавление найденного товара в место или носитель без проверок
     * @request POST:/v1/stock/stock-in
     * @secure
     * @response `200` `StockInResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    stockInUsingPost: (cmd: StockInCmd, params: RequestParams = {}) =>
      this.request<StockInResponse, void>({
        path: `/v1/stock/stock-in`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-type-gateway
     * @name GetStockTypesUsingGet
     * @summary Запрос списка видов запаса
     * @request GET:/v1/stock/stock-types
     * @secure
     * @response `200` `(StockTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getStockTypesUsingGet: (params: RequestParams = {}) =>
      this.request<StockTypeDto[], void>({
        path: `/v1/stock/stock-types`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description code - обязательное поле
     *
     * @tags stock-type-gateway
     * @name CreateStockTypeUsingPost
     * @summary Создание нового справочника вида запаса
     * @request POST:/v1/stock/stock-types
     * @secure
     * @response `200` `StockTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createStockTypeUsingPost: (cmd: ModifyStockTypeCmd, params: RequestParams = {}) =>
      this.request<StockTypeDto, void>({
        path: `/v1/stock/stock-types`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-type-gateway
     * @name GetStockTypeByCodeUsingGet
     * @summary Запрос вида запаса по коду
     * @request GET:/v1/stock/stock-types/code/{code}
     * @secure
     * @response `200` `StockTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getStockTypeByCodeUsingGet: (code: string, params: RequestParams = {}) =>
      this.request<StockTypeDto, void>({
        path: `/v1/stock/stock-types/code/${code}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-type-gateway
     * @name CodeExistsUsingGet
     * @summary Проверка существует ли вид запаса с указанным кодом
     * @request GET:/v1/stock/stock-types/codeExists/{code}
     * @secure
     * @response `200` `CodeExists` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    codeExistsUsingGet: (code: string, params: RequestParams = {}) =>
      this.request<CodeExists, void>({
        path: `/v1/stock/stock-types/codeExists/${code}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-type-gateway
     * @name GetStockTypeByIdUsingGet
     * @summary Запрос вида запаса по идентификатору
     * @request GET:/v1/stock/stock-types/{id}
     * @secure
     * @response `200` `StockTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getStockTypeByIdUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<StockTypeDto, void>({
        path: `/v1/stock/stock-types/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description code - обязательное поле
     *
     * @tags stock-type-gateway
     * @name UpdateStockTypeUsingPut
     * @summary Обновление вида запаса
     * @request PUT:/v1/stock/stock-types/{id}
     * @secure
     * @response `200` `StockTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateStockTypeUsingPut: (id: string, cmd: ModifyStockTypeCmd, params: RequestParams = {}) =>
      this.request<StockTypeDto, void>({
        path: `/v1/stock/stock-types/${id}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-type-gateway
     * @name ArchiveStockTypeUsingDelete
     * @summary Установка признака заархивирован
     * @request DELETE:/v1/stock/stock-types/{id}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    archiveStockTypeUsingDelete: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/stock-types/${id}`,
        method: "DELETE",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name TryTakeUsingPost
     * @summary Запрос на попытку взять товар из ячейки
     * @request POST:/v1/stock/try-take
     * @secure
     * @response `200` `TryTakeResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    tryTakeUsingPost: (tryTakeRequest: TryTakeRequest, params: RequestParams = {}) =>
      this.request<TryTakeResponse, void>({
        path: `/v1/stock/try-take`,
        method: "POST",
        body: tryTakeRequest,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name UpdateExpiredStockTypeUsingPost
     * @summary Перевод всех товаров с истекающим сроком реализации в SS
     * @request POST:/v1/stock/updateExpired
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateExpiredStockTypeUsingPost: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/updateExpired`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name UpdateBakeryExpiredStockTypeUsingPost
     * @summary Перевод товаров пекарни с истекающим сроком реализации в SS
     * @request POST:/v1/stock/updateExpiredBakery
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateBakeryExpiredStockTypeUsingPost: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/updateExpiredBakery`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name UpdateStockTypeInStockByInventoryUsingPost
     * @summary Изменение вида запаса инвентаризацией
     * @request POST:/v1/stock/updateStockTypeByInventory
     * @secure
     * @response `200` `UpdateStockTypeEvent` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateStockTypeInStockByInventoryUsingPost: (
      updateStockTypeCmd: UpdateStockTypeByInventoryCmd,
      params: RequestParams = {},
    ) =>
      this.request<UpdateStockTypeEvent, void>({
        path: `/v1/stock/updateStockTypeByInventory`,
        method: "POST",
        body: updateStockTypeCmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name UpdateStockTypeInStockByPickingUsingPost
     * @summary Изменение вида запаса комплектацией
     * @request POST:/v1/stock/updateStockTypeByPicking
     * @secure
     * @response `200` `UpdateStockTypeEvent` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateStockTypeInStockByPickingUsingPost: (
      updateStockTypeCmd: UpdateStockTypeByPickingCmd,
      params: RequestParams = {},
    ) =>
      this.request<UpdateStockTypeEvent, void>({
        path: `/v1/stock/updateStockTypeByPicking`,
        method: "POST",
        body: updateStockTypeCmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name UpdateStockTypeInStockByUiUsingPost
     * @summary Изменение вида запаса через UI
     * @request POST:/v1/stock/updateStockTypeByUI
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateStockTypeInStockByUiUsingPost: (cmd: UpdateStockTypeInStockByUICmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/updateStockTypeByUI`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name UpdateStockTypeForBatchUsingPost
     * @summary Изменение вида запаса для партии. Вызывается после обеления.
     * @request POST:/v1/stock/updateStockTypeForBatch
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateStockTypeForBatchUsingPost: (
      updateStockTypeForBatchCmd: UpdateStockTypeForBatchCmd,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/v1/stock/updateStockTypeForBatch`,
        method: "POST",
        body: updateStockTypeForBatchCmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description stocksFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name StocksUploadUsingPost
     * @summary Запрос на загрузку файла с запасами
     * @request POST:/v1/stock/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    stocksUploadUsingPost: (data: { stocksFile: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/stock/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name WriteOffStockUsingPost
     * @summary Списание запаса
     * @request POST:/v1/stock/writeOff
     * @secure
     * @response `200` `WriteOffStockResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    writeOffStockUsingPost: (cmd: WriteOffStockCmd, params: RequestParams = {}) =>
      this.request<WriteOffStockResponse, void>({
        path: `/v1/stock/writeOff`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name WriteOffStockByUiUsingPost
     * @summary Списание запаса
     * @request POST:/v1/stock/writeOffLost
     * @secure
     * @response `200` `WriteOffStockResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    writeOffStockByUiUsingPost: (cmd: WriteOffStockByUICmd, params: RequestParams = {}) =>
      this.request<WriteOffStockResponse, void>({
        path: `/v1/stock/writeOffLost`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags stock-gateway
     * @name WriteOffRawUsingPost
     * @summary Отпуск сырья с доинвентаризацией выбывшего запаса
     * @request POST:/v1/stock/writeOffRaw
     * @secure
     * @response `200` `WriteOffRawResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    writeOffRawUsingPost: (cmd: WriteOffRawCmd, params: RequestParams = {}) =>
      this.request<WriteOffRawResponse, void>({
        path: `/v1/stock/writeOffRaw`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name GetByIdUsingGet2
     * @summary getById
     * @request GET:/v1/stock/{id}
     * @secure
     * @response `200` `StockDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getByIdUsingGet2: (id: string, params: RequestParams = {}) =>
      this.request<StockDto, void>({
        path: `/v1/stock/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags stock-gateway
     * @name MoveStockToCarrierUsingPut1
     * @summary Перемещение запаса в носитель с корректировкой резерва
     * @request PUT:/v1/stock/{stockId}/moveTo/{carrierId}
     * @secure
     * @response `200` `StockDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    moveStockToCarrierUsingPut1: (
      carrierId: string,
      stockId: string,
      cmd: MoveStockToCarrierCmd,
      params: RequestParams = {},
    ) =>
      this.request<StockDto, void>({
        path: `/v1/stock/${stockId}/moveTo/${carrierId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
     *
     * @tags tool-gateway
     * @name OutboundOrderUsingPost1
     * @summary Отгрузка заказа
     * @request POST:/v1/tool/pushOrder
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    outboundOrderUsingPost1: (cmd: OutboundOrderCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/tool/pushOrder`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-dictionary-gateway
     * @name GetZoneTypesUsingGet
     * @summary getZoneTypes
     * @request GET:/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones
     * @secure
     * @response `200` `(ZoneDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneTypesUsingGet: (siteId: string, zoneTypeId: string, params: RequestParams = {}) =>
      this.request<ZoneDto[], void>({
        path: `/v1/topology/dictionary/site/${siteId}/zoneType/${zoneTypeId}/zones`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-dictionary-gateway
     * @name PlaceTypesUsingGet
     * @summary placeTypes
     * @request GET:/v1/topology/dictionary/sites/{siteId}/placeTypes
     * @secure
     * @response `200` `(PlaceTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    placeTypesUsingGet: (siteId: string, params: RequestParams = {}) =>
      this.request<PlaceTypeDto[], void>({
        path: `/v1/topology/dictionary/sites/${siteId}/placeTypes`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-dictionary-gateway
     * @name GetZoneTypesUsingGet1
     * @summary getZoneTypes
     * @request GET:/v1/topology/dictionary/zoneTypes
     * @secure
     * @response `200` `(ZoneTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneTypesUsingGet1: (params: RequestParams = {}) =>
      this.request<ZoneTypeDto[], void>({
        path: `/v1/topology/dictionary/zoneTypes`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-status-gateway
     * @name GetPlaceStatusesUsingGet
     * @summary getPlaceStatuses
     * @request GET:/v1/topology/placeStatuses
     * @secure
     * @response `200` `(PlaceStatusDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceStatusesUsingGet: (params: RequestParams = {}) =>
      this.request<PlaceStatusDto[], void>({
        path: `/v1/topology/placeStatuses`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description siteCode - обязательные поля
     *
     * @tags download-gateway
     * @name PlaceTypesDownloadUsingGet
     * @summary Запрос на выгрузку файла с типами мест
     * @request GET:/v1/topology/placeTypes/download/{siteCode}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    placeTypesDownloadUsingGet: (siteCode: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/placeTypes/download/${siteCode}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-characteristic-gateway
     * @name GetPlaceTypeCharacteristicsUsingGet
     * @summary getPlaceTypeCharacteristics
     * @request GET:/v1/topology/placeTypes/{placeTypeId}/characteristics
     * @secure
     * @response `200` `(PlaceTypeCharacteristicDTO)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceTypeCharacteristicsUsingGet: (placeTypeId: string, params: RequestParams = {}) =>
      this.request<PlaceTypeCharacteristicDTO[], void>({
        path: `/v1/topology/placeTypes/${placeTypeId}/characteristics`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-characteristic-gateway
     * @name AddUsingPost
     * @summary add
     * @request POST:/v1/topology/placeTypes/{placeTypeId}/characteristics
     * @secure
     * @response `200` `PlaceTypeCharacteristicDTO` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    addUsingPost: (placeTypeId: string, req: AddPlaceTypeCommonCharacteristicDto, params: RequestParams = {}) =>
      this.request<PlaceTypeCharacteristicDTO, void>({
        path: `/v1/topology/placeTypes/${placeTypeId}/characteristics`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-characteristic-gateway
     * @name GetPlaceTypeCharacteristicUsingGet
     * @summary getPlaceTypeCharacteristic
     * @request GET:/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}
     * @secure
     * @response `200` `PlaceTypeCharacteristicDTO` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceTypeCharacteristicUsingGet: (characteristicId: string, placeTypeId: string, params: RequestParams = {}) =>
      this.request<PlaceTypeCharacteristicDTO, void>({
        path: `/v1/topology/placeTypes/${placeTypeId}/characteristics/${characteristicId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-characteristic-gateway
     * @name UpdateUsingPut2
     * @summary update
     * @request PUT:/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}
     * @secure
     * @response `200` `PlaceTypeCharacteristicDTO` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateUsingPut2: (
      characteristicId: string,
      placeTypeId: string,
      req: UpdatePlaceTypeCommonCharacteristicDto,
      params: RequestParams = {},
    ) =>
      this.request<PlaceTypeCharacteristicDTO, void>({
        path: `/v1/topology/placeTypes/${placeTypeId}/characteristics/${characteristicId}`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-characteristic-gateway
     * @name RemoveUsingDelete
     * @summary remove
     * @request DELETE:/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    removeUsingDelete: (characteristicId: string, placeTypeId: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/placeTypes/${placeTypeId}/characteristics/${characteristicId}`,
        method: "DELETE",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetPlaceUsingGet
     * @summary getPlace
     * @request GET:/v1/topology/places
     * @secure
     * @response `200` `PlaceDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceUsingGet: (query: GetPlaceUsingGetParams, params: RequestParams = {}) =>
      this.request<PlaceDto, void>({
        path: `/v1/topology/places`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name CreateUsingPost2
     * @summary create
     * @request POST:/v1/topology/places
     * @secure
     * @response `200` `PlaceDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createUsingPost2: (cmd: CreatePlaceCmd, params: RequestParams = {}) =>
      this.request<PlaceDto, void>({
        path: `/v1/topology/places`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-bulk-operations-gateway
     * @name StartBulkDeletePlacesUsingDelete
     * @summary startBulkDeletePlaces
     * @request DELETE:/v1/topology/places/bulk/delete-places
     * @secure
     * @response `200` `BulkOperationDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    startBulkDeletePlacesUsingDelete: (cmd: BulkPlacesDeleteCmd, params: RequestParams = {}) =>
      this.request<BulkOperationDto, void>({
        path: `/v1/topology/places/bulk/delete-places`,
        method: "DELETE",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-bulk-operations-gateway
     * @name StartBulkUpdatePlaceStatusUsingPost
     * @summary startBulkUpdatePlaceStatus
     * @request POST:/v1/topology/places/bulk/update-place-status
     * @secure
     * @response `200` `BulkOperationDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    startBulkUpdatePlaceStatusUsingPost: (
      query: StartBulkUpdatePlaceStatusUsingPostParams,
      cmd: BulkPlaceUpdateStatusCmd,
      params: RequestParams = {},
    ) =>
      this.request<BulkOperationDto, void>({
        path: `/v1/topology/places/bulk/update-place-status`,
        method: "POST",
        query: query,
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-bulk-operations-gateway
     * @name StartBulkUpdatePlaceTypeUsingPost
     * @summary startBulkUpdatePlaceType
     * @request POST:/v1/topology/places/bulk/update-place-type
     * @secure
     * @response `200` `BulkOperationDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    startBulkUpdatePlaceTypeUsingPost: (
      query: StartBulkUpdatePlaceTypeUsingPostParams,
      cmd: BulkPlaceUpdateTypeCmd,
      params: RequestParams = {},
    ) =>
      this.request<BulkOperationDto, void>({
        path: `/v1/topology/places/bulk/update-place-type`,
        method: "POST",
        query: query,
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-bulk-operations-gateway
     * @name StartBulkUpdateZoneUsingPost
     * @summary startBulkUpdateZone
     * @request POST:/v1/topology/places/bulk/update-zone
     * @secure
     * @response `200` `BulkOperationDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    startBulkUpdateZoneUsingPost: (
      query: StartBulkUpdateZoneUsingPostParams,
      cmd: BulkPlaceUpdateZoneCmd,
      params: RequestParams = {},
    ) =>
      this.request<BulkOperationDto, void>({
        path: `/v1/topology/places/bulk/update-zone`,
        method: "POST",
        query: query,
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-bulk-operations-gateway
     * @name GetBulkUpdateOperationDataUsingGet
     * @summary getBulkUpdateOperationData
     * @request GET:/v1/topology/places/bulk/{operationId}
     * @secure
     * @response `200` `BulkOperationDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getBulkUpdateOperationDataUsingGet: (operationId: string, params: RequestParams = {}) =>
      this.request<BulkOperationDto, void>({
        path: `/v1/topology/places/bulk/${operationId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description siteCode - обязательные поля
     *
     * @tags download-gateway
     * @name PlacesDownloadUsingGet
     * @summary Запрос на выгрузку файла с топологией
     * @request GET:/v1/topology/places/download/{siteCode}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    placesDownloadUsingGet: ({ siteCode, ...query }: PlacesDownloadUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/places/download/${siteCode}`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name FindPlacesByProcessIdUsingGet
     * @summary findPlacesByProcessId
     * @request GET:/v1/topology/places/findByProcessId
     * @secure
     * @response `200` `(PlaceDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    findPlacesByProcessIdUsingGet: (query: FindPlacesByProcessIdUsingGetParams, params: RequestParams = {}) =>
      this.request<PlaceDto[], void>({
        path: `/v1/topology/places/findByProcessId`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetHierarchyUsingGet
     * @summary getHierarchy
     * @request GET:/v1/topology/places/hierarchy/{siteId}
     * @secure
     * @response `200` `PlaceNodeWithPosition` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getHierarchyUsingGet: (siteId: string, params: RequestParams = {}) =>
      this.request<PlaceNodeWithPosition, void>({
        path: `/v1/topology/places/hierarchy/${siteId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetHierarchyForParentIdUsingGet
     * @summary getHierarchyForParentId
     * @request GET:/v1/topology/places/hierarchy/{siteId}/{parentId}
     * @secure
     * @response `200` `PlaceNodeWithPosition` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getHierarchyForParentIdUsingGet: (parentId: string, siteId: string, params: RequestParams = {}) =>
      this.request<PlaceNodeWithPosition, void>({
        path: `/v1/topology/places/hierarchy/${siteId}/${parentId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetHierarchyByTypeCodeUsingGet
     * @summary getHierarchyByTypeCode
     * @request GET:/v1/topology/places/hierarchyTree/{siteId}
     * @secure
     * @response `200` `(PlaceHierarchyTreeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getHierarchyByTypeCodeUsingGet: (
      { siteId, ...query }: GetHierarchyByTypeCodeUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<PlaceHierarchyTreeDto[], void>({
        path: `/v1/topology/places/hierarchyTree/${siteId}`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-search-gateway
     * @name SearchUsingPost2
     * @summary Search Places
     * @request POST:/v1/topology/places/search
     * @secure
     * @response `200` `PlacesSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchUsingPost2: (query: PlacesSearchQuery, params: RequestParams = {}) =>
      this.request<PlacesSearchResult, void>({
        path: `/v1/topology/places/search`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-search-gateway
     * @name SearchDetailedUsingPost
     * @summary Универсальный поиск мест с иерархией
     * @request POST:/v1/topology/places/search/detailed
     * @secure
     * @response `200` `DetailedPlaceSearchResult` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    searchDetailedUsingPost: (searchQuery: DetailedPlaceSearchQuery, params: RequestParams = {}) =>
      this.request<DetailedPlaceSearchResult, void>({
        path: `/v1/topology/places/search/detailed`,
        method: "POST",
        body: searchQuery,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-search-gateway
     * @name GetPlaceTypesBySiteUsingGet
     * @summary getPlaceTypesBySite
     * @request GET:/v1/topology/places/search/dicts/placeTypes/{siteId}
     * @secure
     * @response `200` `(PlaceTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceTypesBySiteUsingGet: (siteId: string, params: RequestParams = {}) =>
      this.request<PlaceTypeDto[], void>({
        path: `/v1/topology/places/search/dicts/placeTypes/${siteId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-search-gateway
     * @name GetZoneTypesWithZonesUsingGet
     * @summary getZoneTypesWithZones
     * @request GET:/v1/topology/places/search/dicts/zoneTypes/{siteId}
     * @secure
     * @response `200` `(ZoneTypeWithZones)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneTypesWithZonesUsingGet: (siteId: string, params: RequestParams = {}) =>
      this.request<ZoneTypeWithZones[], void>({
        path: `/v1/topology/places/search/dicts/zoneTypes/${siteId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetPlaceByCodeUsingGet
     * @summary getPlaceByCode
     * @request GET:/v1/topology/places/shortView
     * @secure
     * @response `200` `(PlaceShortView)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceByCodeUsingGet: (query: GetPlaceByCodeUsingGetParams, params: RequestParams = {}) =>
      this.request<PlaceShortView[], void>({
        path: `/v1/topology/places/shortView`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetTransferPlaceUsingGet
     * @summary getTransferPlace
     * @request GET:/v1/topology/places/transfer/{siteId}
     * @secure
     * @response `200` `PlaceShortView` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getTransferPlaceUsingGet: (siteId: string, params: RequestParams = {}) =>
      this.request<PlaceShortView, void>({
        path: `/v1/topology/places/transfer/${siteId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags place-gateway
     * @name UpdatePlaceStatusUsingPut
     * @summary Изменение статуса места с удалением входящих резервов
     * @request PUT:/v1/topology/places/updatePlaceStatus
     * @secure
     * @response `200` `UpdatePlaceStatusResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updatePlaceStatusUsingPut: (cmd: UpdatePlaceStatusCmd, params: RequestParams = {}) =>
      this.request<UpdatePlaceStatusResponse, void>({
        path: `/v1/topology/places/updatePlaceStatus`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description placesFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name PlacesUploadUsingPost
     * @summary Запрос на загрузку файла с топологией
     * @request POST:/v1/topology/places/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    placesUploadUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/places/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name GetPlaceByIdUsingGet
     * @summary getPlaceById
     * @request GET:/v1/topology/places/{id}
     * @secure
     * @response `200` `PlaceWithPosition` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceByIdUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<PlaceWithPosition, void>({
        path: `/v1/topology/places/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name UpdateUsingPut1
     * @summary update
     * @request PUT:/v1/topology/places/{id}
     * @secure
     * @response `200` `PlaceDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateUsingPut1: (id: string, cmd: UpdatePlaceCmd, params: RequestParams = {}) =>
      this.request<PlaceDto, void>({
        path: `/v1/topology/places/${id}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-gateway
     * @name DeleteUsingDelete1
     * @summary delete
     * @request DELETE:/v1/topology/places/{id}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    deleteUsingDelete1: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/places/${id}`,
        method: "DELETE",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-characteristic-gateway
     * @name GetPlaceCharacteristicsUsingGet
     * @summary getPlaceCharacteristics
     * @request GET:/v1/topology/places/{placeId}/characteristics
     * @secure
     * @response `200` `(PlaceCharacteristicDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceCharacteristicsUsingGet: (placeId: string, params: RequestParams = {}) =>
      this.request<PlaceCharacteristicDto[], void>({
        path: `/v1/topology/places/${placeId}/characteristics`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-characteristic-gateway
     * @name GetPlaceCharacteristicUsingGet
     * @summary getPlaceCharacteristic
     * @request GET:/v1/topology/places/{placeId}/characteristics/{characteristicId}
     * @secure
     * @response `200` `PlaceCharacteristicDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceCharacteristicUsingGet: (characteristicId: string, placeId: string, params: RequestParams = {}) =>
      this.request<PlaceCharacteristicDto, void>({
        path: `/v1/topology/places/${placeId}/characteristics/${characteristicId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-characteristic-gateway
     * @name OverrideUsingPut
     * @summary override
     * @request PUT:/v1/topology/places/{placeId}/characteristics/{characteristicId}
     * @secure
     * @response `200` `PlaceCharacteristicDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    overrideUsingPut: (
      characteristicId: string,
      placeId: string,
      cmd: OverridePlaceCommonCharacteristicCmd,
      params: RequestParams = {},
    ) =>
      this.request<PlaceCharacteristicDto, void>({
        path: `/v1/topology/places/${placeId}/characteristics/${characteristicId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-gateway
     * @name GetSitesUsingGet
     * @summary getSites
     * @request GET:/v1/topology/sites
     * @secure
     * @response `200` `(SiteDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getSitesUsingGet: (params: RequestParams = {}) =>
      this.request<SiteDto[], void>({
        path: `/v1/topology/sites`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-gateway
     * @name CreateSiteUsingPost
     * @summary createSite
     * @request POST:/v1/topology/sites
     * @secure
     * @response `200` `SiteDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createSiteUsingPost: (cmd: CreateSiteCmd, params: RequestParams = {}) =>
      this.request<SiteDto, void>({
        path: `/v1/topology/sites`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-gateway
     * @name GetSiteByCodeUsingGet
     * @summary getSiteByCode
     * @request GET:/v1/topology/sites/byCode/{code}
     * @secure
     * @response `200` `SiteDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getSiteByCodeUsingGet: (code: string, params: RequestParams = {}) =>
      this.request<SiteDto, void>({
        path: `/v1/topology/sites/byCode/${code}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-gateway
     * @name CodeExistsUsingPost2
     * @summary codeExists
     * @request POST:/v1/topology/sites/codeExists
     * @secure
     * @response `200` `CodeExists` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    codeExistsUsingPost2: (query: SiteCodeExistsQuery, params: RequestParams = {}) =>
      this.request<CodeExists, void>({
        path: `/v1/topology/sites/codeExists`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-settings-gateway
     * @name GetSettingsForSiteUsingGet
     * @summary getSettingsForSite
     * @request GET:/v1/topology/sites/settings/{siteCode}
     * @secure
     * @response `200` `(SiteSettingDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getSettingsForSiteUsingGet: (siteCode: string, params: RequestParams = {}) =>
      this.request<SiteSettingDto[], void>({
        path: `/v1/topology/sites/settings/${siteCode}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-settings-gateway
     * @name UpdateSettingsForSiteUsingPut
     * @summary updateSettingsForSite
     * @request PUT:/v1/topology/sites/settings/{siteCode}
     * @secure
     * @response `200` `(SiteSettingDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateSettingsForSiteUsingPut: (siteCode: string, siteSettingDtos: SiteSettingDto[], params: RequestParams = {}) =>
      this.request<SiteSettingDto[], void>({
        path: `/v1/topology/sites/settings/${siteCode}`,
        method: "PUT",
        body: siteSettingDtos,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-gateway
     * @name GetSiteUsingGet
     * @summary getSite
     * @request GET:/v1/topology/sites/{siteId}
     * @secure
     * @response `200` `SiteDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getSiteUsingGet: (siteId: string, params: RequestParams = {}) =>
      this.request<SiteDto, void>({
        path: `/v1/topology/sites/${siteId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags site-gateway
     * @name UpdateSiteUsingPut
     * @summary updateSite
     * @request PUT:/v1/topology/sites/{siteId}
     * @secure
     * @response `200` `SiteDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateSiteUsingPut: (siteId: string, cmd: UpdateSiteCmd, params: RequestParams = {}) =>
      this.request<SiteDto, void>({
        path: `/v1/topology/sites/${siteId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description siteId - обязательное поле
     *
     * @tags place-type-gateway
     * @name GetPlaceTypesBySiteUsingGet1
     * @summary Запрос на получение типов мест для площадки с количеством их экземпляров
     * @request GET:/v1/topology/sites/{siteId}/placeTypes
     * @secure
     * @response `200` `(PlaceTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceTypesBySiteUsingGet1: (siteId: string, params: RequestParams = {}) =>
      this.request<PlaceTypeDto[], void>({
        path: `/v1/topology/sites/${siteId}/placeTypes`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-gateway
     * @name CreatePlaceTypeUsingPost
     * @summary createPlaceType
     * @request POST:/v1/topology/sites/{siteId}/placeTypes
     * @secure
     * @response `200` `PlaceTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createPlaceTypeUsingPost: (siteId: string, cmd: CreatePlaceTypeCmd, params: RequestParams = {}) =>
      this.request<PlaceTypeDto, void>({
        path: `/v1/topology/sites/${siteId}/placeTypes`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-gateway
     * @name GetAvailablePlaceTypesByParentPlaceIdUsingGet
     * @summary getAvailablePlaceTypesByParentPlaceId
     * @request GET:/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId
     * @secure
     * @response `200` `(PlaceTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getAvailablePlaceTypesByParentPlaceIdUsingGet: (
      { siteId, ...query }: GetAvailablePlaceTypesByParentPlaceIdUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<PlaceTypeDto[], void>({
        path: `/v1/topology/sites/${siteId}/placeTypes/availableByParentPlaceId`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-gateway
     * @name CodeExistsUsingPost1
     * @summary codeExists
     * @request POST:/v1/topology/sites/{siteId}/placeTypes/codeExists
     * @secure
     * @response `200` `CodeExists` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    codeExistsUsingPost1: (siteId: string, query: PlaceTypeCodeExistsQuery, params: RequestParams = {}) =>
      this.request<CodeExists, void>({
        path: `/v1/topology/sites/${siteId}/placeTypes/codeExists`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description placesFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name PlaceTypesUploadUsingPost
     * @summary Запрос на загрузку файла типов мест с характеристиками
     * @request POST:/v1/topology/sites/{siteId}/placeTypes/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    placeTypesUploadUsingPost: (siteId: string, data: { placesFile: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/sites/${siteId}/placeTypes/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-gateway
     * @name GetPlaceTypeUsingGet
     * @summary getPlaceType
     * @request GET:/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}
     * @secure
     * @response `200` `PlaceTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getPlaceTypeUsingGet: (placeTypeId: string, siteId: string, params: RequestParams = {}) =>
      this.request<PlaceTypeDto, void>({
        path: `/v1/topology/sites/${siteId}/placeTypes/${placeTypeId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags place-type-gateway
     * @name UpdatePlaceTypeUsingPut
     * @summary updatePlaceType
     * @request PUT:/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}
     * @secure
     * @response `200` `PlaceTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updatePlaceTypeUsingPut: (
      placeTypeId: string,
      siteId: string,
      cmd: UpdatePlaceTypeCmd,
      params: RequestParams = {},
    ) =>
      this.request<PlaceTypeDto, void>({
        path: `/v1/topology/sites/${siteId}/placeTypes/${placeTypeId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name GetZoneTypesUsingGet2
     * @summary getZoneTypes
     * @request GET:/v1/topology/zone-types
     * @secure
     * @response `200` `(ZoneTypeDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneTypesUsingGet2: (params: RequestParams = {}) =>
      this.request<ZoneTypeDto[], void>({
        path: `/v1/topology/zone-types`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name CreateUsingPost3
     * @summary create
     * @request POST:/v1/topology/zone-types
     * @secure
     * @response `200` `ZoneTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createUsingPost3: (cmd: CreateZoneTypeCmd, params: RequestParams = {}) =>
      this.request<ZoneTypeDto, void>({
        path: `/v1/topology/zone-types`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name CodeExistsUsingPost4
     * @summary codeExists
     * @request POST:/v1/topology/zone-types/codeExists
     * @secure
     * @response `200` `CodeExists` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    codeExistsUsingPost4: (query: ZoneTypeCodeExistsQuery, params: RequestParams = {}) =>
      this.request<CodeExists, void>({
        path: `/v1/topology/zone-types/codeExists`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description zoneTypeFile - обязательные поля
     *
     * @tags upload-gateway
     * @name ZoneTypesUploadUsingPost
     * @summary Запрос на загрузку файла с типами зон
     * @request POST:/v1/topology/zone-types/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    zoneTypesUploadUsingPost: (data: { zoneTypeFile: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/zone-types/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name GetZoneTypesWithZonesUsingGet1
     * @summary getZoneTypesWithZones
     * @request GET:/v1/topology/zone-types/withZones
     * @secure
     * @response `200` `(ZoneTypeWithZonesDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneTypesWithZonesUsingGet1: (query: GetZoneTypesWithZonesUsingGet1Params, params: RequestParams = {}) =>
      this.request<ZoneTypeWithZonesDto[], void>({
        path: `/v1/topology/zone-types/withZones`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name GetZoneTypeUsingGet
     * @summary getZoneType
     * @request GET:/v1/topology/zone-types/{id}
     * @secure
     * @response `200` `ZoneTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneTypeUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<ZoneTypeDto, void>({
        path: `/v1/topology/zone-types/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name DeleteUsingDelete2
     * @summary delete
     * @request DELETE:/v1/topology/zone-types/{id}
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    deleteUsingDelete2: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/zone-types/${id}`,
        method: "DELETE",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-type-gateway
     * @name UpdateUsingPut3
     * @summary update
     * @request PUT:/v1/topology/zone-types/{zoneTypeId}
     * @secure
     * @response `200` `ZoneTypeDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateUsingPut3: (zoneTypeId: string, cmd: UpdateZoneTypeCmd, params: RequestParams = {}) =>
      this.request<ZoneTypeDto, void>({
        path: `/v1/topology/zone-types/${zoneTypeId}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags download-gateway
     * @name ZoneTypesDownloadUsingGet
     * @summary Запрос на выгрузку файла с типами зон
     * @request GET:/v1/topology/zoneTypes/download
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    zoneTypesDownloadUsingGet: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/zoneTypes/download`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-gateway
     * @name GetZonesByTypeUsingGet
     * @summary getZonesByType
     * @request GET:/v1/topology/zones
     * @secure
     * @response `200` `(ZoneDto)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZonesByTypeUsingGet: (query: GetZonesByTypeUsingGetParams, params: RequestParams = {}) =>
      this.request<ZoneDto[], void>({
        path: `/v1/topology/zones`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description Create a Zone. code, siteId, typeId are required fields.
     *
     * @tags zone-gateway
     * @name CreateZoneUsingPost
     * @summary create a Zone
     * @request POST:/v1/topology/zones
     * @secure
     * @response `200` `ZoneDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    createZoneUsingPost: (cmd: CreateZoneCmd, params: RequestParams = {}) =>
      this.request<ZoneDto, void>({
        path: `/v1/topology/zones`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-gateway
     * @name CodeExistsUsingPost3
     * @summary codeExists
     * @request POST:/v1/topology/zones/codeExists
     * @secure
     * @response `200` `CodeExists` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    codeExistsUsingPost3: (query: ZoneCodeExistsQuery, params: RequestParams = {}) =>
      this.request<CodeExists, void>({
        path: `/v1/topology/zones/codeExists`,
        method: "POST",
        body: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-projection-gateway
     * @name GetZonesBySiteIdAndTypeIdUsingGet
     * @summary getZonesBySiteIdAndTypeId
     * @request GET:/v1/topology/zones/projections
     * @secure
     * @response `200` `(ZoneProjection)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZonesBySiteIdAndTypeIdUsingGet: (query: GetZonesBySiteIdAndTypeIdUsingGetParams, params: RequestParams = {}) =>
      this.request<ZoneProjection[], void>({
        path: `/v1/topology/zones/projections`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description zonesFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name ZonesUploadUsingPost
     * @summary Запрос на загрузку файла с зонами
     * @request POST:/v1/topology/zones/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    zonesUploadUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/zones/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description zonesFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name PickingZonesUploadUsingPost
     * @summary Запрос на загрузку файла с областями комплектования
     * @request POST:/v1/topology/zones/upload/ok
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    pickingZonesUploadUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/zones/upload/ok`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description zonesFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name StorageZonesUploadUsingPost
     * @summary Запрос на загрузку файла со складскими участками
     * @request POST:/v1/topology/zones/upload/su
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    storageZonesUploadUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/topology/zones/upload/su`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * No description
     *
     * @tags zone-gateway
     * @name GetZoneByIdUsingGet
     * @summary getZoneById
     * @request GET:/v1/topology/zones/{id}
     * @secure
     * @response `200` `ZoneDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getZoneByIdUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<ZoneDto, void>({
        path: `/v1/topology/zones/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
     *
     * @tags zone-gateway
     * @name UpdateZoneUsingPut
     * @summary update the Zone
     * @request PUT:/v1/topology/zones/{id}
     * @secure
     * @response `200` `ZoneDto` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    updateZoneUsingPut: (id: string, cmd: UpdateZoneCmd, params: RequestParams = {}) =>
      this.request<ZoneDto, void>({
        path: `/v1/topology/zones/${id}`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags transfer-gateway
     * @name AvailabilityAisleUsingPut
     * @summary Проверка доступности места для трансфера носителя
     * @request PUT:/v1/transfer/availabilityPlaceForTransferCarrier
     * @secure
     * @response `200` `AvailabilityPlaceForTransferCarrierResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    availabilityAisleUsingPut: (cmd: AvailabilityPlaceForTransferCarrierCmd, params: RequestParams = {}) =>
      this.request<AvailabilityPlaceForTransferCarrierResponse, void>({
        path: `/v1/transfer/availabilityPlaceForTransferCarrier`,
        method: "PUT",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags transfer-gateway
     * @name GetRouteUsingPost
     * @summary getRoute
     * @request POST:/v1/transfer/getRoute/{carrierId}
     * @secure
     * @response `200` `(PlaceShortView)[]` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    getRouteUsingPost: (carrierId: string, params: RequestParams = {}) =>
      this.request<PlaceShortView[], void>({
        path: `/v1/transfer/getRoute/${carrierId}`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags transfer-gateway
     * @name MoveCarrierToPlaceUsingPost
     * @summary Перенос носителя в место
     * @request POST:/v1/transfer/moveCarrierToPlace
     * @secure
     * @response `200` `MoveCarrierToPlaceResponse` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    moveCarrierToPlaceUsingPost: (cmd: MoveCarrierToPlaceCmd, params: RequestParams = {}) =>
      this.request<MoveCarrierToPlaceResponse, void>({
        path: `/v1/transfer/moveCarrierToPlace`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description Диагностический эндпоинт, предназначен только для тестовых целей
     *
     * @tags transfer-gateway
     * @name SetCarrierPlaceUsingPost
     * @summary Размещение носителя в месте без проверок
     * @request POST:/v1/transfer/setCarrierPlace
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    setCarrierPlaceUsingPost: (cmd: SetCarrierPlaceCmd, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/transfer/setCarrierPlace`,
        method: "POST",
        body: cmd,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description settingsFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name UploadWriteOffSettingsUsingPost
     * @summary Запрос на загрузку файла с настройками списания
     * @request POST:/v1/write-off-settings/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    uploadWriteOffSettingsUsingPost: (data: any, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/write-off-settings/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description zoneCharacteristics, siteCode - обязательные поля
     *
     * @tags upload-gateway
     * @name UploadZoneCharacteristicsUsingPost
     * @summary Запрос на загрузку файла с настройкой приоритетов секторов комплектования
     * @request POST:/v1/zone-characteristics/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    uploadZoneCharacteristicsUsingPost: (data: { zoneCharacteristics: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/zone-characteristics/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description zoneRouteStrategy, siteCode - обязательные поля
     *
     * @tags upload-gateway
     * @name BindToRouteStrategyUsingPost
     * @summary Запрос на загрузку файла с привязкой стратегии обхода змейки
     * @request POST:/v1/zone-route-strategy/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    bindToRouteStrategyUsingPost: (data: { zoneRouteStrategy: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/zone-route-strategy/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description zoneTransferRuleFile, siteId - обязательные поля
     *
     * @tags upload-gateway
     * @name ZoneTransferRulesUploadUsingPost
     * @summary Запрос на загрузку файла правил переходов между местами
     * @request POST:/v1/zone-transfer-rules/upload
     * @secure
     * @response `200` `void` OK
     * @response `401` `void` Unauthorized
     * @response `403` `void` Forbidden
     * @response `500` `void` Internal Server Error
     */
    zoneTransferRulesUploadUsingPost: (data: { zoneTransferRuleFile: File }, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/v1/zone-transfer-rules/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        ...params,
      }),
  };
}
