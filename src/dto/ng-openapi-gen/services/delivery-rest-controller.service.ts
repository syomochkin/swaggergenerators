/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { DeliveryDto } from '../models/delivery-dto';


/**
 * Delivery Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class DeliveryRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet
   */
  static readonly FindAllUsingGetPath = '/api/yard/v1/deliveries';

  /**
   * Get deliveries.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet$Response(params?: {

    /**
     * Exclude finished deliveries
     */
    excludeUnloaded?: boolean;

    /**
     * Reservation time from
     */
    reservationTimeFrom?: string;

    /**
     * Reservation time to
     */
    reservationTimeTo?: string;

    /**
     * Site code
     */
    siteCode?: string;
  }): Observable<StrictHttpResponse<Array<DeliveryDto>>> {

    const rb = new RequestBuilder(this.rootUrl, DeliveryRestControllerService.FindAllUsingGetPath, 'get');
    if (params) {
      rb.query('excludeUnloaded', params.excludeUnloaded, {});
      rb.query('reservationTimeFrom', params.reservationTimeFrom, {});
      rb.query('reservationTimeTo', params.reservationTimeTo, {});
      rb.query('siteCode', params.siteCode, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<DeliveryDto>>;
      })
    );
  }

  /**
   * Get deliveries.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet(params?: {

    /**
     * Exclude finished deliveries
     */
    excludeUnloaded?: boolean;

    /**
     * Reservation time from
     */
    reservationTimeFrom?: string;

    /**
     * Reservation time to
     */
    reservationTimeTo?: string;

    /**
     * Site code
     */
    siteCode?: string;
  }): Observable<Array<DeliveryDto>> {

    return this.findAllUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<DeliveryDto>>) => r.body as Array<DeliveryDto>)
    );
  }

}
