/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { CreateGateRequest } from '../models/create-gate-request';
import { GateDto } from '../models/gate-dto';


/**
 * Gate Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class GateRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet2
   */
  static readonly FindAllUsingGet2Path = '/api/yard/v1/gates';

  /**
   * Get information about gates.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet2()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet2$Response(params?: {

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Status
     */
    status?: Array<string>;
  }): Observable<StrictHttpResponse<Array<GateDto>>> {

    const rb = new RequestBuilder(this.rootUrl, GateRestControllerService.FindAllUsingGet2Path, 'get');
    if (params) {
      rb.query('siteCode', params.siteCode, {});
      rb.query('status', params.status, {"explode":true});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<GateDto>>;
      })
    );
  }

  /**
   * Get information about gates.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet2$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet2(params?: {

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Status
     */
    status?: Array<string>;
  }): Observable<Array<GateDto>> {

    return this.findAllUsingGet2$Response(params).pipe(
      map((r: StrictHttpResponse<Array<GateDto>>) => r.body as Array<GateDto>)
    );
  }

  /**
   * Path part for operation createUsingPost1
   */
  static readonly CreateUsingPost1Path = '/api/yard/v1/gates';

  /**
   * Create gate.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUsingPost1()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost1$Response(params: {

    /**
     * request
     */
    body: CreateGateRequest
  }): Observable<StrictHttpResponse<GateDto>> {

    const rb = new RequestBuilder(this.rootUrl, GateRestControllerService.CreateUsingPost1Path, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<GateDto>;
      })
    );
  }

  /**
   * Create gate.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createUsingPost1$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost1(params: {

    /**
     * request
     */
    body: CreateGateRequest
  }): Observable<GateDto> {

    return this.createUsingPost1$Response(params).pipe(
      map((r: StrictHttpResponse<GateDto>) => r.body as GateDto)
    );
  }

}
