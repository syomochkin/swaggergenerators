/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { CalculateMonthLimitRequest } from '../models/calculate-month-limit-request';
import { EditDayLimitRequest } from '../models/edit-day-limit-request';
import { MonthLimitDto } from '../models/month-limit-dto';


/**
 * Month Limit Planning Controller
 */
@Injectable({
  providedIn: 'root',
})
export class MonthLimitPlanningControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findOneUsingGet
   */
  static readonly FindOneUsingGetPath = '/api/yard/v1/month-limits';

  /**
   * Get information about month limits.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findOneUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  findOneUsingGet$Response(params: {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * month
     */
    month: string;

    /**
     * siteCode
     */
    siteCode: string;
  }): Observable<StrictHttpResponse<MonthLimitDto>> {

    const rb = new RequestBuilder(this.rootUrl, MonthLimitPlanningControllerService.FindOneUsingGetPath, 'get');
    if (params) {
      rb.query('categoryId', params.categoryId, {});
      rb.query('month', params.month, {});
      rb.query('siteCode', params.siteCode, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MonthLimitDto>;
      })
    );
  }

  /**
   * Get information about month limits.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findOneUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findOneUsingGet(params: {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * month
     */
    month: string;

    /**
     * siteCode
     */
    siteCode: string;
  }): Observable<MonthLimitDto> {

    return this.findOneUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<MonthLimitDto>) => r.body as MonthLimitDto)
    );
  }

  /**
   * Path part for operation calculateMonthLimitUsingPost
   */
  static readonly CalculateMonthLimitUsingPostPath = '/api/yard/v1/month-limits/calculate';

  /**
   * Calculate monthly supply limits.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `calculateMonthLimitUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  calculateMonthLimitUsingPost$Response(params: {

    /**
     * request
     */
    body: CalculateMonthLimitRequest
  }): Observable<StrictHttpResponse<MonthLimitDto>> {

    const rb = new RequestBuilder(this.rootUrl, MonthLimitPlanningControllerService.CalculateMonthLimitUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MonthLimitDto>;
      })
    );
  }

  /**
   * Calculate monthly supply limits.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `calculateMonthLimitUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  calculateMonthLimitUsingPost(params: {

    /**
     * request
     */
    body: CalculateMonthLimitRequest
  }): Observable<MonthLimitDto> {

    return this.calculateMonthLimitUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<MonthLimitDto>) => r.body as MonthLimitDto)
    );
  }

  /**
   * Path part for operation updateDayLimitUsingPatch
   */
  static readonly UpdateDayLimitUsingPatchPath = '/api/yard/v1/month-limits/day/{id}';

  /**
   * Manually edit day supply limit.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateDayLimitUsingPatch()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateDayLimitUsingPatch$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: EditDayLimitRequest
  }): Observable<StrictHttpResponse<MonthLimitDto>> {

    const rb = new RequestBuilder(this.rootUrl, MonthLimitPlanningControllerService.UpdateDayLimitUsingPatchPath, 'patch');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MonthLimitDto>;
      })
    );
  }

  /**
   * Manually edit day supply limit.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `updateDayLimitUsingPatch$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateDayLimitUsingPatch(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: EditDayLimitRequest
  }): Observable<MonthLimitDto> {

    return this.updateDayLimitUsingPatch$Response(params).pipe(
      map((r: StrictHttpResponse<MonthLimitDto>) => r.body as MonthLimitDto)
    );
  }

  /**
   * Path part for operation approveMonthLimitUsingPatch
   */
  static readonly ApproveMonthLimitUsingPatchPath = '/api/yard/v1/month-limits/{id}/approve';

  /**
   * Approve month limit.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `approveMonthLimitUsingPatch()` instead.
   *
   * This method doesn't expect any request body.
   */
  approveMonthLimitUsingPatch$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<MonthLimitDto>> {

    const rb = new RequestBuilder(this.rootUrl, MonthLimitPlanningControllerService.ApproveMonthLimitUsingPatchPath, 'patch');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MonthLimitDto>;
      })
    );
  }

  /**
   * Approve month limit.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `approveMonthLimitUsingPatch$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  approveMonthLimitUsingPatch(params: {

    /**
     * id
     */
    id: number;
  }): Observable<MonthLimitDto> {

    return this.approveMonthLimitUsingPatch$Response(params).pipe(
      map((r: StrictHttpResponse<MonthLimitDto>) => r.body as MonthLimitDto)
    );
  }

  /**
   * Path part for operation resetMonthLimitUsingPatch
   */
  static readonly ResetMonthLimitUsingPatchPath = '/api/yard/v1/month-limits/{id}/reset';

  /**
   * Reset month limit changes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `resetMonthLimitUsingPatch()` instead.
   *
   * This method doesn't expect any request body.
   */
  resetMonthLimitUsingPatch$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<MonthLimitDto>> {

    const rb = new RequestBuilder(this.rootUrl, MonthLimitPlanningControllerService.ResetMonthLimitUsingPatchPath, 'patch');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<MonthLimitDto>;
      })
    );
  }

  /**
   * Reset month limit changes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `resetMonthLimitUsingPatch$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  resetMonthLimitUsingPatch(params: {

    /**
     * id
     */
    id: number;
  }): Observable<MonthLimitDto> {

    return this.resetMonthLimitUsingPatch$Response(params).pipe(
      map((r: StrictHttpResponse<MonthLimitDto>) => r.body as MonthLimitDto)
    );
  }

}
