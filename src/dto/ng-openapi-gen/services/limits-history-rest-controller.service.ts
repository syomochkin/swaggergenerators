/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { LimitHistoryRecordDto } from '../models/limit-history-record-dto';


/**
 * Limits History Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class LimitsHistoryRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getMonthLimitHistoryUsingGet1
   */
  static readonly GetMonthLimitHistoryUsingGet1Path = '/api/yard/v1/limits-history';

  /**
   * Get information about month limit history changes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMonthLimitHistoryUsingGet1()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMonthLimitHistoryUsingGet1$Response(params: {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * month
     */
    month: string;

    /**
     * siteCode
     */
    siteCode: string;
  }): Observable<StrictHttpResponse<Array<LimitHistoryRecordDto>>> {

    const rb = new RequestBuilder(this.rootUrl, LimitsHistoryRestControllerService.GetMonthLimitHistoryUsingGet1Path, 'get');
    if (params) {
      rb.query('categoryId', params.categoryId, {});
      rb.query('month', params.month, {});
      rb.query('siteCode', params.siteCode, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<LimitHistoryRecordDto>>;
      })
    );
  }

  /**
   * Get information about month limit history changes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getMonthLimitHistoryUsingGet1$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMonthLimitHistoryUsingGet1(params: {

    /**
     * categoryId
     */
    categoryId: number;

    /**
     * month
     */
    month: string;

    /**
     * siteCode
     */
    siteCode: string;
  }): Observable<Array<LimitHistoryRecordDto>> {

    return this.getMonthLimitHistoryUsingGet1$Response(params).pipe(
      map((r: StrictHttpResponse<Array<LimitHistoryRecordDto>>) => r.body as Array<LimitHistoryRecordDto>)
    );
  }

  /**
   * Path part for operation getDayLimitHistoryUsingGet
   */
  static readonly GetDayLimitHistoryUsingGetPath = '/api/yard/v1/limits-history/day/{id}';

  /**
   * Get information about day limit history changes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getDayLimitHistoryUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getDayLimitHistoryUsingGet$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<Array<LimitHistoryRecordDto>>> {

    const rb = new RequestBuilder(this.rootUrl, LimitsHistoryRestControllerService.GetDayLimitHistoryUsingGetPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<LimitHistoryRecordDto>>;
      })
    );
  }

  /**
   * Get information about day limit history changes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getDayLimitHistoryUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getDayLimitHistoryUsingGet(params: {

    /**
     * id
     */
    id: number;
  }): Observable<Array<LimitHistoryRecordDto>> {

    return this.getDayLimitHistoryUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<LimitHistoryRecordDto>>) => r.body as Array<LimitHistoryRecordDto>)
    );
  }

  /**
   * Path part for operation getMonthLimitHistoryUsingGet
   */
  static readonly GetMonthLimitHistoryUsingGetPath = '/api/yard/v1/limits-history/month/{id}';

  /**
   * Get information about month limit history changes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getMonthLimitHistoryUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMonthLimitHistoryUsingGet$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<Array<LimitHistoryRecordDto>>> {

    const rb = new RequestBuilder(this.rootUrl, LimitsHistoryRestControllerService.GetMonthLimitHistoryUsingGetPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<LimitHistoryRecordDto>>;
      })
    );
  }

  /**
   * Get information about month limit history changes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getMonthLimitHistoryUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getMonthLimitHistoryUsingGet(params: {

    /**
     * id
     */
    id: number;
  }): Observable<Array<LimitHistoryRecordDto>> {

    return this.getMonthLimitHistoryUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<LimitHistoryRecordDto>>) => r.body as Array<LimitHistoryRecordDto>)
    );
  }

}
