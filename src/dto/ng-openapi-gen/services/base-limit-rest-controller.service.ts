/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { BaseLimitsDto } from '../models/base-limits-dto';


/**
 * Base Limit Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class BaseLimitRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getAllUsingGet
   */
  static readonly GetAllUsingGetPath = '/api/yard/v1/baselimits';

  /**
   * Get base limits.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllUsingGet$Response(params: {

    /**
     * siteCode
     */
    siteCode: string;
  }): Observable<StrictHttpResponse<Array<BaseLimitsDto>>> {

    const rb = new RequestBuilder(this.rootUrl, BaseLimitRestControllerService.GetAllUsingGetPath, 'get');
    if (params) {
      rb.query('siteCode', params.siteCode, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<BaseLimitsDto>>;
      })
    );
  }

  /**
   * Get base limits.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllUsingGet(params: {

    /**
     * siteCode
     */
    siteCode: string;
  }): Observable<Array<BaseLimitsDto>> {

    return this.getAllUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<BaseLimitsDto>>) => r.body as Array<BaseLimitsDto>)
    );
  }

  /**
   * Path part for operation createUsingPost
   */
  static readonly CreateUsingPostPath = '/api/yard/v1/baselimits';

  /**
   * Create base limit.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost$Response(params: {

    /**
     * request
     */
    body: BaseLimitsDto
  }): Observable<StrictHttpResponse<BaseLimitsDto>> {

    const rb = new RequestBuilder(this.rootUrl, BaseLimitRestControllerService.CreateUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<BaseLimitsDto>;
      })
    );
  }

  /**
   * Create base limit.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost(params: {

    /**
     * request
     */
    body: BaseLimitsDto
  }): Observable<BaseLimitsDto> {

    return this.createUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<BaseLimitsDto>) => r.body as BaseLimitsDto)
    );
  }

  /**
   * Path part for operation deleteUsingDelete
   */
  static readonly DeleteUsingDeletePath = '/api/yard/v1/baselimits/{id}';

  /**
   * Delete base limit by id.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUsingDelete$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, BaseLimitRestControllerService.DeleteUsingDeletePath, 'delete');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Delete base limit by id.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUsingDelete(params: {

    /**
     * id
     */
    id: number;
  }): Observable<void> {

    return this.deleteUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation updateUsingPatch
   */
  static readonly UpdateUsingPatchPath = '/api/yard/v1/baselimits/{id}';

  /**
   * Update base limit by id.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateUsingPatch()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUsingPatch$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: BaseLimitsDto
  }): Observable<StrictHttpResponse<BaseLimitsDto>> {

    const rb = new RequestBuilder(this.rootUrl, BaseLimitRestControllerService.UpdateUsingPatchPath, 'patch');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<BaseLimitsDto>;
      })
    );
  }

  /**
   * Update base limit by id.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `updateUsingPatch$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUsingPatch(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: BaseLimitsDto
  }): Observable<BaseLimitsDto> {

    return this.updateUsingPatch$Response(params).pipe(
      map((r: StrictHttpResponse<BaseLimitsDto>) => r.body as BaseLimitsDto)
    );
  }

}
