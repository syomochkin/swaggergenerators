/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { CheckpointDeliveryVehicleDto } from '../models/checkpoint-delivery-vehicle-dto';
import { DeliveryVehicleRequest } from '../models/delivery-vehicle-request';
import { DictionaryDto } from '../models/dictionary-dto';


/**
 * Delivery Vehicle Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class DeliveryVehicleRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet1
   */
  static readonly FindAllUsingGet1Path = '/api/yard/v1/delivery-vehicles';

  /**
   * Get delivery vehicles.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet1()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet1$Response(params?: {

    /**
     * Exclude finished deliveries
     */
    excludeUnloaded?: boolean;

    /**
     * Reservation time from
     */
    reservationTimeFrom?: string;

    /**
     * Reservation time to
     */
    reservationTimeTo?: string;

    /**
     * Site code
     */
    siteCode?: string;
  }): Observable<StrictHttpResponse<Array<CheckpointDeliveryVehicleDto>>> {

    const rb = new RequestBuilder(this.rootUrl, DeliveryVehicleRestControllerService.FindAllUsingGet1Path, 'get');
    if (params) {
      rb.query('excludeUnloaded', params.excludeUnloaded, {});
      rb.query('reservationTimeFrom', params.reservationTimeFrom, {});
      rb.query('reservationTimeTo', params.reservationTimeTo, {});
      rb.query('siteCode', params.siteCode, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<CheckpointDeliveryVehicleDto>>;
      })
    );
  }

  /**
   * Get delivery vehicles.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet1$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet1(params?: {

    /**
     * Exclude finished deliveries
     */
    excludeUnloaded?: boolean;

    /**
     * Reservation time from
     */
    reservationTimeFrom?: string;

    /**
     * Reservation time to
     */
    reservationTimeTo?: string;

    /**
     * Site code
     */
    siteCode?: string;
  }): Observable<Array<CheckpointDeliveryVehicleDto>> {

    return this.findAllUsingGet1$Response(params).pipe(
      map((r: StrictHttpResponse<Array<CheckpointDeliveryVehicleDto>>) => r.body as Array<CheckpointDeliveryVehicleDto>)
    );
  }

  /**
   * Path part for operation createVehicleUsingPost
   */
  static readonly CreateVehicleUsingPostPath = '/api/yard/v1/delivery-vehicles';

  /**
   * Create delivery vehicle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createVehicleUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createVehicleUsingPost$Response(params: {

    /**
     * request
     */
    body: DeliveryVehicleRequest
  }): Observable<StrictHttpResponse<CheckpointDeliveryVehicleDto>> {

    const rb = new RequestBuilder(this.rootUrl, DeliveryVehicleRestControllerService.CreateVehicleUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CheckpointDeliveryVehicleDto>;
      })
    );
  }

  /**
   * Create delivery vehicle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createVehicleUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createVehicleUsingPost(params: {

    /**
     * request
     */
    body: DeliveryVehicleRequest
  }): Observable<CheckpointDeliveryVehicleDto> {

    return this.createVehicleUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<CheckpointDeliveryVehicleDto>) => r.body as CheckpointDeliveryVehicleDto)
    );
  }

  /**
   * Path part for operation findAllStatusesUsingGet
   */
  static readonly FindAllStatusesUsingGetPath = '/api/yard/v1/delivery-vehicles/statuses';

  /**
   * Get all delivery vehicle statuses.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllStatusesUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllStatusesUsingGet$Response(params?: {
  }): Observable<StrictHttpResponse<Array<DictionaryDto>>> {

    const rb = new RequestBuilder(this.rootUrl, DeliveryVehicleRestControllerService.FindAllStatusesUsingGetPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<DictionaryDto>>;
      })
    );
  }

  /**
   * Get all delivery vehicle statuses.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllStatusesUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllStatusesUsingGet(params?: {
  }): Observable<Array<DictionaryDto>> {

    return this.findAllStatusesUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<DictionaryDto>>) => r.body as Array<DictionaryDto>)
    );
  }

  /**
   * Path part for operation patchUsingPatch
   */
  static readonly PatchUsingPatchPath = '/api/yard/v1/delivery-vehicles/{id}';

  /**
   * Update delivery vehicle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `patchUsingPatch()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: DeliveryVehicleRequest
  }): Observable<StrictHttpResponse<CheckpointDeliveryVehicleDto>> {

    const rb = new RequestBuilder(this.rootUrl, DeliveryVehicleRestControllerService.PatchUsingPatchPath, 'patch');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CheckpointDeliveryVehicleDto>;
      })
    );
  }

  /**
   * Update delivery vehicle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `patchUsingPatch$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: DeliveryVehicleRequest
  }): Observable<CheckpointDeliveryVehicleDto> {

    return this.patchUsingPatch$Response(params).pipe(
      map((r: StrictHttpResponse<CheckpointDeliveryVehicleDto>) => r.body as CheckpointDeliveryVehicleDto)
    );
  }

}
