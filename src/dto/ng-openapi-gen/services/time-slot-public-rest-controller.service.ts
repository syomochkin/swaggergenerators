/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { TimeSlotOptionDto } from '../models/time-slot-option-dto';
import { TimeSlotOptionRequest } from '../models/time-slot-option-request';


/**
 * Time Slot Public Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class TimeSlotPublicRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation pickAvailableTimeSlotsUsingPost1
   */
  static readonly PickAvailableTimeSlotsUsingPost1Path = '/api/yard/v1/public/time-slots/available';

  /**
   * Pick available time slots.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pickAvailableTimeSlotsUsingPost1()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pickAvailableTimeSlotsUsingPost1$Response(params: {

    /**
     * request
     */
    body: TimeSlotOptionRequest
  }): Observable<StrictHttpResponse<TimeSlotOptionDto>> {

    const rb = new RequestBuilder(this.rootUrl, TimeSlotPublicRestControllerService.PickAvailableTimeSlotsUsingPost1Path, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<TimeSlotOptionDto>;
      })
    );
  }

  /**
   * Pick available time slots.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pickAvailableTimeSlotsUsingPost1$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pickAvailableTimeSlotsUsingPost1(params: {

    /**
     * request
     */
    body: TimeSlotOptionRequest
  }): Observable<TimeSlotOptionDto> {

    return this.pickAvailableTimeSlotsUsingPost1$Response(params).pipe(
      map((r: StrictHttpResponse<TimeSlotOptionDto>) => r.body as TimeSlotOptionDto)
    );
  }

}
