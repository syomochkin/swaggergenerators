/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { StandardDto } from '../models/standard-dto';


/**
 * Standard Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class StandardRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet6
   */
  static readonly FindAllUsingGet6Path = '/api/yard/v1/standards';

  /**
   * Информация по нормативам и их правилам.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet6()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet6$Response(params?: {

    /**
     * Base standard
     */
    base?: number;

    /**
     * Product category ID
     */
    categoryId?: number;

    /**
     * ID
     */
    id?: number;

    /**
     * Site code
     */
    siteCode?: string;
  }): Observable<StrictHttpResponse<Array<StandardDto>>> {

    const rb = new RequestBuilder(this.rootUrl, StandardRestControllerService.FindAllUsingGet6Path, 'get');
    if (params) {
      rb.query('base', params.base, {});
      rb.query('categoryId', params.categoryId, {});
      rb.query('id', params.id, {});
      rb.query('siteCode', params.siteCode, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<StandardDto>>;
      })
    );
  }

  /**
   * Информация по нормативам и их правилам.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet6$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet6(params?: {

    /**
     * Base standard
     */
    base?: number;

    /**
     * Product category ID
     */
    categoryId?: number;

    /**
     * ID
     */
    id?: number;

    /**
     * Site code
     */
    siteCode?: string;
  }): Observable<Array<StandardDto>> {

    return this.findAllUsingGet6$Response(params).pipe(
      map((r: StrictHttpResponse<Array<StandardDto>>) => r.body as Array<StandardDto>)
    );
  }

  /**
   * Path part for operation createUsingPost3
   */
  static readonly CreateUsingPost3Path = '/api/yard/v1/standards';

  /**
   * Создать норматив.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUsingPost3()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost3$Response(params: {

    /**
     * request
     */
    body: StandardDto
  }): Observable<StrictHttpResponse<StandardDto>> {

    const rb = new RequestBuilder(this.rootUrl, StandardRestControllerService.CreateUsingPost3Path, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<StandardDto>;
      })
    );
  }

  /**
   * Создать норматив.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createUsingPost3$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost3(params: {

    /**
     * request
     */
    body: StandardDto
  }): Observable<StandardDto> {

    return this.createUsingPost3$Response(params).pipe(
      map((r: StrictHttpResponse<StandardDto>) => r.body as StandardDto)
    );
  }

  /**
   * Path part for operation deleteUsingDelete1
   */
  static readonly DeleteUsingDelete1Path = '/api/yard/v1/standards/{id}';

  /**
   * Удалить норматив.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUsingDelete1()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUsingDelete1$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, StandardRestControllerService.DeleteUsingDelete1Path, 'delete');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Удалить норматив.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteUsingDelete1$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUsingDelete1(params: {

    /**
     * id
     */
    id: number;
  }): Observable<void> {

    return this.deleteUsingDelete1$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation patchUsingPatch3
   */
  static readonly PatchUsingPatch3Path = '/api/yard/v1/standards/{id}';

  /**
   * Изменить норматив.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `patchUsingPatch3()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch3$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: StandardDto
  }): Observable<StrictHttpResponse<{
}>> {

    const rb = new RequestBuilder(this.rootUrl, StandardRestControllerService.PatchUsingPatch3Path, 'patch');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        }>;
      })
    );
  }

  /**
   * Изменить норматив.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `patchUsingPatch3$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch3(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: StandardDto
  }): Observable<{
}> {

    return this.patchUsingPatch3$Response(params).pipe(
      map((r: StrictHttpResponse<{
}>) => r.body as {
})
    );
  }

}
