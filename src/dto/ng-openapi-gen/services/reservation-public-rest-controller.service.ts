/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ReservationDto } from '../models/reservation-dto';
import { ReservationPublicDto } from '../models/reservation-public-dto';
import { ReservationRequest } from '../models/reservation-request';


/**
 * Reservation Public Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class ReservationPublicRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation createUsingPost2
   */
  static readonly CreateUsingPost2Path = '/api/yard/v1/public/reservations';

  /**
   * Create reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUsingPost2()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost2$Response(params: {

    /**
     * request
     */
    body: ReservationRequest
  }): Observable<StrictHttpResponse<ReservationPublicDto>> {

    const rb = new RequestBuilder(this.rootUrl, ReservationPublicRestControllerService.CreateUsingPost2Path, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ReservationPublicDto>;
      })
    );
  }

  /**
   * Create reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createUsingPost2$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUsingPost2(params: {

    /**
     * request
     */
    body: ReservationRequest
  }): Observable<ReservationPublicDto> {

    return this.createUsingPost2$Response(params).pipe(
      map((r: StrictHttpResponse<ReservationPublicDto>) => r.body as ReservationPublicDto)
    );
  }

  /**
   * Path part for operation patchUsingPatch1
   */
  static readonly PatchUsingPatch1Path = '/api/yard/v1/public/reservations/{id}';

  /**
   * Update information about specific reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `patchUsingPatch1()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch1$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: ReservationPublicDto
  }): Observable<StrictHttpResponse<ReservationDto>> {

    const rb = new RequestBuilder(this.rootUrl, ReservationPublicRestControllerService.PatchUsingPatch1Path, 'patch');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ReservationDto>;
      })
    );
  }

  /**
   * Update information about specific reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `patchUsingPatch1$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch1(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: ReservationPublicDto
  }): Observable<ReservationDto> {

    return this.patchUsingPatch1$Response(params).pipe(
      map((r: StrictHttpResponse<ReservationDto>) => r.body as ReservationDto)
    );
  }

  /**
   * Path part for operation cancelUsingPut1
   */
  static readonly CancelUsingPut1Path = '/api/yard/v1/public/reservations/{id}/cancel';

  /**
   * Cancel gate reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `cancelUsingPut1()` instead.
   *
   * This method doesn't expect any request body.
   */
  cancelUsingPut1$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<ReservationPublicDto>> {

    const rb = new RequestBuilder(this.rootUrl, ReservationPublicRestControllerService.CancelUsingPut1Path, 'put');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ReservationPublicDto>;
      })
    );
  }

  /**
   * Cancel gate reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `cancelUsingPut1$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  cancelUsingPut1(params: {

    /**
     * id
     */
    id: number;
  }): Observable<ReservationPublicDto> {

    return this.cancelUsingPut1$Response(params).pipe(
      map((r: StrictHttpResponse<ReservationPublicDto>) => r.body as ReservationPublicDto)
    );
  }

}
