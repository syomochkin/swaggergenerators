/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { RegularReservationCancelEventDto } from '../models/regular-reservation-cancel-event-dto';
import { RegularReservationDtoReq } from '../models/regular-reservation-dto-req';
import { RegularReservationDtoRes } from '../models/regular-reservation-dto-res';
import { RegularReservationWithTimeSlotsDto } from '../models/regular-reservation-with-time-slots-dto';


/**
 * Regular Reservation Public Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class RegularReservationPublicRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation createRegularReservationUsingPost
   */
  static readonly CreateRegularReservationUsingPostPath = '/api/yard/v1/public/regular-reservations';

  /**
   * Create regular time slot reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createRegularReservationUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createRegularReservationUsingPost$Response(params: {

    /**
     * dto
     */
    body: RegularReservationDtoReq
  }): Observable<StrictHttpResponse<RegularReservationDtoRes>> {

    const rb = new RequestBuilder(this.rootUrl, RegularReservationPublicRestControllerService.CreateRegularReservationUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RegularReservationDtoRes>;
      })
    );
  }

  /**
   * Create regular time slot reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createRegularReservationUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createRegularReservationUsingPost(params: {

    /**
     * dto
     */
    body: RegularReservationDtoReq
  }): Observable<RegularReservationDtoRes> {

    return this.createRegularReservationUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<RegularReservationDtoRes>) => r.body as RegularReservationDtoRes)
    );
  }

  /**
   * Path part for operation cancelUsingPut
   */
  static readonly CancelUsingPutPath = '/api/yard/v1/public/regular-reservations/{id}/cancel';

  /**
   * Cancel regular gate reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `cancelUsingPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  cancelUsingPut$Response(params: {

    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<RegularReservationDtoRes>> {

    const rb = new RequestBuilder(this.rootUrl, RegularReservationPublicRestControllerService.CancelUsingPutPath, 'put');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RegularReservationDtoRes>;
      })
    );
  }

  /**
   * Cancel regular gate reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `cancelUsingPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  cancelUsingPut(params: {

    /**
     * id
     */
    id: number;
  }): Observable<RegularReservationDtoRes> {

    return this.cancelUsingPut$Response(params).pipe(
      map((r: StrictHttpResponse<RegularReservationDtoRes>) => r.body as RegularReservationDtoRes)
    );
  }

  /**
   * Path part for operation findTimeSlotsUsingGet
   */
  static readonly FindTimeSlotsUsingGetPath = '/api/yard/v1/public/regular-reservations/{id}/events';

  /**
   * Find regular reservation time slots accordingly the provided filter.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findTimeSlotsUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  findTimeSlotsUsingGet$Response(params: {

    /**
     * End datetime
     */
    end?: string;

    /**
     * id
     */
    id: number;

    /**
     * Start datetime
     */
    start?: string;
  }): Observable<StrictHttpResponse<RegularReservationWithTimeSlotsDto>> {

    const rb = new RequestBuilder(this.rootUrl, RegularReservationPublicRestControllerService.FindTimeSlotsUsingGetPath, 'get');
    if (params) {
      rb.query('end', params.end, {});
      rb.path('id', params.id, {});
      rb.query('start', params.start, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RegularReservationWithTimeSlotsDto>;
      })
    );
  }

  /**
   * Find regular reservation time slots accordingly the provided filter.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findTimeSlotsUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findTimeSlotsUsingGet(params: {

    /**
     * End datetime
     */
    end?: string;

    /**
     * id
     */
    id: number;

    /**
     * Start datetime
     */
    start?: string;
  }): Observable<RegularReservationWithTimeSlotsDto> {

    return this.findTimeSlotsUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<RegularReservationWithTimeSlotsDto>) => r.body as RegularReservationWithTimeSlotsDto)
    );
  }

  /**
   * Path part for operation cancelRegularReservationsUsingPost
   */
  static readonly CancelRegularReservationsUsingPostPath = '/api/yard/v1/public/regular-reservations/{id}/events/cancel';

  /**
   * Exclude time slots from regular reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `cancelRegularReservationsUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  cancelRegularReservationsUsingPost$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: RegularReservationCancelEventDto
  }): Observable<StrictHttpResponse<RegularReservationCancelEventDto>> {

    const rb = new RequestBuilder(this.rootUrl, RegularReservationPublicRestControllerService.CancelRegularReservationsUsingPostPath, 'post');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RegularReservationCancelEventDto>;
      })
    );
  }

  /**
   * Exclude time slots from regular reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `cancelRegularReservationsUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  cancelRegularReservationsUsingPost(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: RegularReservationCancelEventDto
  }): Observable<RegularReservationCancelEventDto> {

    return this.cancelRegularReservationsUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<RegularReservationCancelEventDto>) => r.body as RegularReservationCancelEventDto)
    );
  }

}
