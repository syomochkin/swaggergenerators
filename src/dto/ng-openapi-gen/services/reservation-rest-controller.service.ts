/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ReservationDto } from '../models/reservation-dto';


/**
 * Reservation Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class ReservationRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet5
   */
  static readonly FindAllUsingGet5Path = '/api/yard/v1/reservations';

  /**
   * Get information about reservations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet5()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet5$Response(params?: {
    deliveryId?: Array<number>;

    /**
     * End datetime
     */
    end?: string;

    /**
     * Order number
     */
    orderNumber?: Array<string>;

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Start datetime
     */
    start?: string;
  }): Observable<StrictHttpResponse<Array<ReservationDto>>> {

    const rb = new RequestBuilder(this.rootUrl, ReservationRestControllerService.FindAllUsingGet5Path, 'get');
    if (params) {
      rb.query('deliveryId', params.deliveryId, {"explode":true});
      rb.query('end', params.end, {});
      rb.query('orderNumber', params.orderNumber, {"explode":true});
      rb.query('siteCode', params.siteCode, {});
      rb.query('start', params.start, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<ReservationDto>>;
      })
    );
  }

  /**
   * Get information about reservations.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet5$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet5(params?: {
    deliveryId?: Array<number>;

    /**
     * End datetime
     */
    end?: string;

    /**
     * Order number
     */
    orderNumber?: Array<string>;

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Start datetime
     */
    start?: string;
  }): Observable<Array<ReservationDto>> {

    return this.findAllUsingGet5$Response(params).pipe(
      map((r: StrictHttpResponse<Array<ReservationDto>>) => r.body as Array<ReservationDto>)
    );
  }

  /**
   * Path part for operation patchUsingPatch2
   */
  static readonly PatchUsingPatch2Path = '/api/yard/v1/reservations/{id}';

  /**
   * Update gate reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `patchUsingPatch2()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch2$Response(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: ReservationDto
  }): Observable<StrictHttpResponse<ReservationDto>> {

    const rb = new RequestBuilder(this.rootUrl, ReservationRestControllerService.PatchUsingPatch2Path, 'patch');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ReservationDto>;
      })
    );
  }

  /**
   * Update gate reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `patchUsingPatch2$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  patchUsingPatch2(params: {

    /**
     * id
     */
    id: number;

    /**
     * request
     */
    body: ReservationDto
  }): Observable<ReservationDto> {

    return this.patchUsingPatch2$Response(params).pipe(
      map((r: StrictHttpResponse<ReservationDto>) => r.body as ReservationDto)
    );
  }

}
