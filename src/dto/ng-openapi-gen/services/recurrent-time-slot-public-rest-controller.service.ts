/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { RecurrentReservationTimeSlotDto } from '../models/recurrent-reservation-time-slot-dto';
import { RecurrentTimeSlotOptionRequest } from '../models/recurrent-time-slot-option-request';


/**
 * Recurrent Time Slot Public Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class RecurrentTimeSlotPublicRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation pickAvailableTimeSlotsUsingPost
   */
  static readonly PickAvailableTimeSlotsUsingPostPath = '/api/yard/v1/public/time-slots/recurrent/available';

  /**
   * Pick available time slots for regular reservation.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `pickAvailableTimeSlotsUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pickAvailableTimeSlotsUsingPost$Response(params: {

    /**
     * request
     */
    body: RecurrentTimeSlotOptionRequest
  }): Observable<StrictHttpResponse<RecurrentReservationTimeSlotDto>> {

    const rb = new RequestBuilder(this.rootUrl, RecurrentTimeSlotPublicRestControllerService.PickAvailableTimeSlotsUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RecurrentReservationTimeSlotDto>;
      })
    );
  }

  /**
   * Pick available time slots for regular reservation.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `pickAvailableTimeSlotsUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  pickAvailableTimeSlotsUsingPost(params: {

    /**
     * request
     */
    body: RecurrentTimeSlotOptionRequest
  }): Observable<RecurrentReservationTimeSlotDto> {

    return this.pickAvailableTimeSlotsUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<RecurrentReservationTimeSlotDto>) => r.body as RecurrentReservationTimeSlotDto)
    );
  }

}
