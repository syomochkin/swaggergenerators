/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ProductCategoryDto } from '../models/product-category-dto';


/**
 * Product Category Controller
 */
@Injectable({
  providedIn: 'root',
})
export class ProductCategoryControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet4
   */
  static readonly FindAllUsingGet4Path = '/api/yard/v1/product-categories';

  /**
   * Get all product categories.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet4()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet4$Response(params?: {

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Product Categories with Base Limits flag
     */
    withBaseLimits?: boolean;
  }): Observable<StrictHttpResponse<Array<ProductCategoryDto>>> {

    const rb = new RequestBuilder(this.rootUrl, ProductCategoryControllerService.FindAllUsingGet4Path, 'get');
    if (params) {
      rb.query('siteCode', params.siteCode, {});
      rb.query('withBaseLimits', params.withBaseLimits, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<ProductCategoryDto>>;
      })
    );
  }

  /**
   * Get all product categories.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet4$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet4(params?: {

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Product Categories with Base Limits flag
     */
    withBaseLimits?: boolean;
  }): Observable<Array<ProductCategoryDto>> {

    return this.findAllUsingGet4$Response(params).pipe(
      map((r: StrictHttpResponse<Array<ProductCategoryDto>>) => r.body as Array<ProductCategoryDto>)
    );
  }

}
