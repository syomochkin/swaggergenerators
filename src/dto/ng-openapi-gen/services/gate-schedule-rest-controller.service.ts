/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { GateScheduleSlotDto } from '../models/gate-schedule-slot-dto';


/**
 * Gate Schedule Rest Controller
 */
@Injectable({
  providedIn: 'root',
})
export class GateScheduleRestControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation findAllUsingGet3
   */
  static readonly FindAllUsingGet3Path = '/api/yard/v1/gate-schedules';

  /**
   * Список всех резервов, включая регулярные и нерегулярные, за промежуток времени.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `findAllUsingGet3()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet3$Response(params?: {

    /**
     * End datetime
     */
    end?: string;

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Start datetime
     */
    start?: string;
  }): Observable<StrictHttpResponse<Array<GateScheduleSlotDto>>> {

    const rb = new RequestBuilder(this.rootUrl, GateScheduleRestControllerService.FindAllUsingGet3Path, 'get');
    if (params) {
      rb.query('end', params.end, {});
      rb.query('siteCode', params.siteCode, {});
      rb.query('start', params.start, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<GateScheduleSlotDto>>;
      })
    );
  }

  /**
   * Список всех резервов, включая регулярные и нерегулярные, за промежуток времени.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `findAllUsingGet3$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  findAllUsingGet3(params?: {

    /**
     * End datetime
     */
    end?: string;

    /**
     * Site code
     */
    siteCode?: string;

    /**
     * Start datetime
     */
    start?: string;
  }): Observable<Array<GateScheduleSlotDto>> {

    return this.findAllUsingGet3$Response(params).pipe(
      map((r: StrictHttpResponse<Array<GateScheduleSlotDto>>) => r.body as Array<GateScheduleSlotDto>)
    );
  }

}
