/* tslint:disable */
/* eslint-disable */
import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationParams } from './api-configuration';

import { BaseLimitRestControllerService } from './services/base-limit-rest-controller.service';
import { DeliveryRestControllerService } from './services/delivery-rest-controller.service';
import { DeliveryVehicleRestControllerService } from './services/delivery-vehicle-rest-controller.service';
import { GateScheduleRestControllerService } from './services/gate-schedule-rest-controller.service';
import { GateRestControllerService } from './services/gate-rest-controller.service';
import { LimitsHistoryRestControllerService } from './services/limits-history-rest-controller.service';
import { MonthLimitPlanningControllerService } from './services/month-limit-planning-controller.service';
import { ProductCategoryControllerService } from './services/product-category-controller.service';
import { RegularReservationPublicRestControllerService } from './services/regular-reservation-public-rest-controller.service';
import { ReservationPublicRestControllerService } from './services/reservation-public-rest-controller.service';
import { TimeSlotPublicRestControllerService } from './services/time-slot-public-rest-controller.service';
import { RecurrentTimeSlotPublicRestControllerService } from './services/recurrent-time-slot-public-rest-controller.service';
import { ReservationLimitRestControllerService } from './services/reservation-limit-rest-controller.service';
import { ReservationRestControllerService } from './services/reservation-rest-controller.service';
import { StandardRestControllerService } from './services/standard-rest-controller.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    BaseLimitRestControllerService,
    DeliveryRestControllerService,
    DeliveryVehicleRestControllerService,
    GateScheduleRestControllerService,
    GateRestControllerService,
    LimitsHistoryRestControllerService,
    MonthLimitPlanningControllerService,
    ProductCategoryControllerService,
    RegularReservationPublicRestControllerService,
    ReservationPublicRestControllerService,
    TimeSlotPublicRestControllerService,
    RecurrentTimeSlotPublicRestControllerService,
    ReservationLimitRestControllerService,
    ReservationRestControllerService,
    StandardRestControllerService,
    ApiConfiguration
  ],
})
export class ApiWarehouseModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiWarehouseModule> {
    return {
      ngModule: ApiWarehouseModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor( 
    @Optional() @SkipSelf() parentModule: ApiWarehouseModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiWarehouseModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
      'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
