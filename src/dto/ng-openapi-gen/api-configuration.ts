/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';

/**
 * Global configuration
 */
@Injectable({
  providedIn: 'root',
})
export class ApiConfiguration {
  rootUrl: string = '//wms.utkonos.dev';
}

/**
 * Parameters for `ApiWarehouseModule.forRoot()`
 */
export interface ApiConfigurationParams {
  rootUrl?: string;
}
