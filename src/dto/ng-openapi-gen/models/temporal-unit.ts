/* tslint:disable */
/* eslint-disable */
import { Duration } from './duration';

/* hello world */

export interface TemporalUnit {
  dateBased?: boolean;
  duration?: Duration;
  durationEstimated?: boolean;
  timeBased?: boolean;
}
