/* tslint:disable */
/* eslint-disable */
import { DictionaryDto } from './dictionary-dto';

/* hello world */

export interface GateDto {
  configuration?: string;
  id?: number;
  number?: number;
  siteCode?: string;
  status?: DictionaryDto;
  temperatureConditions?: Array<string>;
  type?: string;
}
