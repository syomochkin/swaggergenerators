/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface DeliveryVehicleRequest {
  deliveryId?: number;
  driverName?: string;
  driverPhoneNumber?: string;
  hasTransitCargo?: boolean;
  vehicleNumber?: string;
  vehicleStatusCode?: string;
}
