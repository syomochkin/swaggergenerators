/* tslint:disable */
/* eslint-disable */
import { TemporalUnit } from './temporal-unit';

/* hello world */

export interface Duration {
  nano?: number;
  negative?: boolean;
  seconds?: number;
  units?: Array<TemporalUnit>;
  zero?: boolean;
}
