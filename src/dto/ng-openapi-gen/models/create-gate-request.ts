/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface CreateGateRequest {
  configurationId: number;
  number: number;
  siteCode: string;
  temperatureConditionIds?: Array<number>;
  typeId: number;
}
