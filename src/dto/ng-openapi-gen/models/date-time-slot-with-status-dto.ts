/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface DateTimeSlotWithStatusDto {
  duration: number;
  start: string;
  status?: 'CANCELLED' | 'SCHEDULED';
}
