/* tslint:disable */
/* eslint-disable */
import { DictionaryDto } from './dictionary-dto';

/* hello world */

export interface StandardRuleDto {
  id?: number;
  target?: number;
  type: DictionaryDto;
  value?: number;
}
