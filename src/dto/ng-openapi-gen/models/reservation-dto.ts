/* tslint:disable */
/* eslint-disable */
import { DeliveryDto } from './delivery-dto';
import { DictionaryDto } from './dictionary-dto';

/* hello world */

export interface ReservationDto {
  delivery?: DeliveryDto;
  end?: string;
  endDelay?: number;
  gateId?: number;
  id?: number;
  start?: string;
  startDelay?: number;
  status?: DictionaryDto;
}
