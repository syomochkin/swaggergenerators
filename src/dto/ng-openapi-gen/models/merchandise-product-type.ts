/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface MerchandiseProductType {
  amount: number;
  type: string;
  unit?: string;
}
