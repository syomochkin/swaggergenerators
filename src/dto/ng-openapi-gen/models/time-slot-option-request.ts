/* tslint:disable */
/* eslint-disable */
import { DeliveryDetails } from './delivery-details';
import { GateConfigurationRequest } from './gate-configuration-request';

/* hello world */

export interface TimeSlotOptionRequest {
  delivery: DeliveryDetails;
  end: string;
  gate: GateConfigurationRequest;
  siteCode: string;
  start: string;
  trustedAcceptance?: boolean;
}
