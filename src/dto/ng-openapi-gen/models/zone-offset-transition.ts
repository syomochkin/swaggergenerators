/* tslint:disable */
/* eslint-disable */
import { Duration } from './duration';
import { ZoneOffsetRes } from './zone-offset-res';

/* hello world */

export interface ZoneOffsetTransition {
  dateTimeAfter?: string;
  dateTimeBefore?: string;
  duration?: Duration;
  gap?: boolean;
  instant?: number;
  offsetAfter?: ZoneOffsetRes;
  offsetBefore?: ZoneOffsetRes;
  overlap?: boolean;
}
