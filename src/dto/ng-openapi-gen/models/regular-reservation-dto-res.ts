/* tslint:disable */
/* eslint-disable */
import { RecurrentTimeSlotDtoRes } from './recurrent-time-slot-dto-res';

/* hello world */

export interface RegularReservationDtoRes {
  confirmationDeadline?: number;
  id?: number;
  ruleId: number;
  siteCode: string;
  status?: 'CANCELLED' | 'SCHEDULED' | 'UNPROCESSED';
  supplierId?: number;
  timeSlot: RecurrentTimeSlotDtoRes;
}
