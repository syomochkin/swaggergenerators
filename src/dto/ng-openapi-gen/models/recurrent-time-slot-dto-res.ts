/* tslint:disable */
/* eslint-disable */
import { OffsetTimeRes } from './offset-time-res';

/* hello world */

export interface RecurrentTimeSlotDtoRes {
  duration: number;
  start: OffsetTimeRes;
}
