/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface DayLimitDto {
  active?: boolean;
  actualAmount?: number;
  amount?: number;
  date?: string;
  editable?: boolean;
  id?: number;
  manualEdited?: boolean;
  percent?: number;
  rejectionReason?: string;
  weekNumber?: number;
}
