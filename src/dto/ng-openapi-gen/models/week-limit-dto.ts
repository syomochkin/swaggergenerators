/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface WeekLimitDto {
  active?: boolean;
  amount?: number;
  id?: number;
  manualEdited?: boolean;
  percent?: number;
  weekNumber?: number;
}
