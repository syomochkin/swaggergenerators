/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface BaseLimitsDto {
  base: number;
  categoryId: number;
  categoryName?: string;
  created?: string;
  id?: number;
  siteCode: string;
  updated?: string;
  week?: Array<number>;
}
