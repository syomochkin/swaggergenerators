/* tslint:disable */
/* eslint-disable */
import { RecurrencePattern } from './recurrence-pattern';
import { RecurrenceRange } from './recurrence-range';

/* hello world */

export interface RecurrenceDetails {
  pattern: RecurrencePattern;
  range: RecurrenceRange;
}
