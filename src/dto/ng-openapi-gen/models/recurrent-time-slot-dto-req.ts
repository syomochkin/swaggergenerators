/* tslint:disable */
/* eslint-disable */
import { OffsetTimeReq } from './offset-time-req';

/* hello world */

export interface RecurrentTimeSlotDtoReq {
  duration: number;
  start: OffsetTimeReq;
}
