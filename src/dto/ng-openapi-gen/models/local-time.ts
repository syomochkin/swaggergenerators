/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface LocalTime {
  hour?: number;
  minute?: number;
  nano?: number;
  second?: number;
}
