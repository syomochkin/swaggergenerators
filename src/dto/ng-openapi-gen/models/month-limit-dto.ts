/* tslint:disable */
/* eslint-disable */
import { DayLimitDto } from './day-limit-dto';
import { WeekLimitDto } from './week-limit-dto';

/* hello world */

export interface MonthLimitDto {
  actualAmount?: number;
  approvalInfo?: string;
  baseLimits?: Array<number>;
  categoryId?: number;
  dayLimits?: Array<DayLimitDto>;
  diffAmount?: number;
  firstDate?: string;
  forecast?: number;
  id?: number;
  lastDate?: string;
  siteCode?: string;
  status?: 'APPROVED' | 'APPROVED_EDITING' | 'BLANK' | 'UNAPPROVED';
  weekLimits?: Array<WeekLimitDto>;
  yearMonth?: string;
}
