/* tslint:disable */
/* eslint-disable */
import { ZoneOffsetTransition } from './zone-offset-transition';
import { ZoneOffsetTransitionRule } from './zone-offset-transition-rule';

/* hello world */

export interface ZoneRules {
  fixedOffset?: boolean;
  transitionRules?: Array<ZoneOffsetTransitionRule>;
  transitions?: Array<ZoneOffsetTransition>;
}
