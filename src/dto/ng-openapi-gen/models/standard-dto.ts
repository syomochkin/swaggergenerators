/* tslint:disable */
/* eslint-disable */
import { ProductCategoryDto } from './product-category-dto';
import { StandardRuleDto } from './standard-rule-dto';

/* hello world */

export interface StandardDto {
  base?: number;
  category: ProductCategoryDto;
  created?: string;
  id?: number;
  rules?: Array<StandardRuleDto>;
  siteCode?: string;
  updated?: string;
}
