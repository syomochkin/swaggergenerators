/* tslint:disable */
/* eslint-disable */
import { WeekLimitRequest } from './week-limit-request';

/* hello world */

export interface CalculateMonthLimitRequest {
  baseLimits?: Array<number>;
  categoryId: number;
  comment?: string;
  forecast: number;
  month: string;
  siteCode: string;
  weekLimits?: Array<WeekLimitRequest>;
}
