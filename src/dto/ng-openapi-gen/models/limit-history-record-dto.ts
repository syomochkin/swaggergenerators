/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface LimitHistoryRecordDto {
  comment?: string;
  id: number;
  limitDetails?: string;
  newAmount?: number;
  newPercent?: number;
  oldAmount?: number;
  oldPercent?: number;
  operationType?: string;
  timestamp?: number;
  user?: string;
}
