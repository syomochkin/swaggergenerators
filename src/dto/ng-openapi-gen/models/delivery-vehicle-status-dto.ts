/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface DeliveryVehicleStatusDto {
  code?: string;
  description?: string;
}
