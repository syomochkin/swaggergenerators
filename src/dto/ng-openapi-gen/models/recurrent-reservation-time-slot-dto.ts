/* tslint:disable */
/* eslint-disable */
import { RecurrenceDetails } from './recurrence-details';
import { TimeSlotDto } from './time-slot-dto';

/* hello world */

export interface RecurrentReservationTimeSlotDto {
  recurrence?: RecurrenceDetails;
  ruleId?: number;
  siteCode?: string;
  supplierId?: number;
  timeSlots?: Array<TimeSlotDto>;
  trustedAcceptance?: boolean;
}
