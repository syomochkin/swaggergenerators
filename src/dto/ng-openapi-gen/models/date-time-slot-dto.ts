/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface DateTimeSlotDto {
  duration?: number;
  start?: string;
}
