/* tslint:disable */
/* eslint-disable */
import { LocalTime } from './local-time';
import { ZoneOffsetRes } from './zone-offset-res';

/* hello world */

export interface ZoneOffsetTransitionRule {
  dayOfMonthIndicator?: number;
  dayOfWeek?: 'FRIDAY' | 'MONDAY' | 'SATURDAY' | 'SUNDAY' | 'THURSDAY' | 'TUESDAY' | 'WEDNESDAY';
  localTime?: LocalTime;
  midnightEndOfDay?: boolean;
  month?: 'APRIL' | 'AUGUST' | 'DECEMBER' | 'FEBRUARY' | 'JANUARY' | 'JULY' | 'JUNE' | 'MARCH' | 'MAY' | 'NOVEMBER' | 'OCTOBER' | 'SEPTEMBER';
  offsetAfter?: ZoneOffsetRes;
  offsetBefore?: ZoneOffsetRes;
  standardOffset?: ZoneOffsetRes;
  timeDefinition?: 'STANDARD' | 'UTC' | 'WALL';
}
