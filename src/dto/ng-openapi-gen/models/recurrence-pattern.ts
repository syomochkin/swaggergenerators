/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface RecurrencePattern {
  dayOfMonth?: number;
  daysOfWeek?: Array<string>;
  index?: string;
  interval: number;
  type: string;
}
