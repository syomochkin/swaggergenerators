/* tslint:disable */
/* eslint-disable */
import { DictionaryDto } from './dictionary-dto';
import { GateDeliveryDto } from './gate-delivery-dto';
import { SupplierDto } from './supplier-dto';

/* hello world */

export interface GateScheduleSlotDto {
  delivery?: GateDeliveryDto;
  end?: string;
  endDelay?: number;
  gateId?: number;
  id?: number;
  start?: string;
  startDelay?: number;
  status?: DictionaryDto;
  supplier?: SupplierDto;
  type?: 'HARD' | 'SOFT';
}
