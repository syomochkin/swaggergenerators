/* tslint:disable */
/* eslint-disable */
import { ZoneOffset } from './zone-offset';

/* hello world */

export interface OffsetTime {
  hour?: number;
  minute?: number;
  nano?: number;
  offset?: ZoneOffset;
  second?: number;
}
