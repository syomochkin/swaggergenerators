/* tslint:disable */
/* eslint-disable */
import { DateTimeSlotDto } from './date-time-slot-dto';
import { DeliveryDto } from './delivery-dto';

/* hello world */

export interface ReservationPublicDto {
  delivery?: DeliveryDto;
  id?: number;
  regularReservationId?: number;
  siteCode?: string;
  status?: string;
  timeSlot?: DateTimeSlotDto;
}
