/* tslint:disable */
/* eslint-disable */
import { ProductDto } from './product-dto';

/* hello world */

export interface OrderDetails {
  number: string;
  products: Array<ProductDto>;
}
