/* tslint:disable */
/* eslint-disable */
import { ZoneRules } from './zone-rules';

/* hello world */

export interface ZoneOffsetRes {
  id?: string;
  rules?: ZoneRules;
  totalSeconds?: number;
}
