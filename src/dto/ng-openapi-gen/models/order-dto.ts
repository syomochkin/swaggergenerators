/* tslint:disable */
/* eslint-disable */
import { ProductDto } from './product-dto';

/* hello world */

export interface OrderDto {
  number?: string;
  products?: Array<ProductDto>;
}
