/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface EditDayLimitRequest {
  amount: number;
  comment?: string;
}
