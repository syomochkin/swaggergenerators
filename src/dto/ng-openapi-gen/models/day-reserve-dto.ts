/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface DayReserveDto {
  categoryId?: number;
  categoryName?: string;
  categoryProductsAmount?: number;
  orderNumber?: string;
  otherProductsAmount?: number;
  slotType?: string;
  supplierName?: string;
}
