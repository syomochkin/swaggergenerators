/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface CheckpointDeliveryVehicleDto {
  deliveryId?: number;
  driverName?: string;
  driverPhoneNumber?: string;
  hasTransitCargo?: boolean;
  orderNumbers?: Array<string>;
  reservationExpectedTime?: string;
  supplierName?: string;
  vehicleArrivalTime?: string;
  vehicleDepartureTime?: string;
  vehicleId?: number;
  vehicleNumber?: string;
  vehicleStatusCode?: string;
  vehicleStatusDescription?: string;
}
