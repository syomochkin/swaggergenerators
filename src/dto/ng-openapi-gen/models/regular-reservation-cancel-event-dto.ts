/* tslint:disable */
/* eslint-disable */
import { DateTimeSlotWithStatusDto } from './date-time-slot-with-status-dto';

/* hello world */

export interface RegularReservationCancelEventDto {
  timeSlots?: Array<DateTimeSlotWithStatusDto>;
}
