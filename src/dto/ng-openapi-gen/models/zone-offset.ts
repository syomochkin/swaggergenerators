/* tslint:disable */
/* eslint-disable */
import { ZoneRules } from './zone-rules';

/* hello world */

export interface ZoneOffset {
  id?: string;
  rules?: ZoneRules;
  totalSeconds?: number;
}
