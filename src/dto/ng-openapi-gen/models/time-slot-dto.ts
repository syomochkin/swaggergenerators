/* tslint:disable */
/* eslint-disable */
import { OffsetTime } from './offset-time';

/* hello world */

export interface TimeSlotDto {
  duration?: number;
  start?: OffsetTime;
}
