/* tslint:disable */
/* eslint-disable */
import { GateConfigurationRequest } from './gate-configuration-request';
import { MerchandiseProductType } from './merchandise-product-type';
import { RecurrenceDetails } from './recurrence-details';
import { Supplier } from './supplier';

/* hello world */

export interface RecurrentTimeSlotOptionRequest {

  /**
   * End time
   */
  end?: string;
  gate: GateConfigurationRequest;
  merchandiseProductTypes?: Array<MerchandiseProductType>;
  recurrence: RecurrenceDetails;
  ruleId: number;
  siteCode: string;

  /**
   * Start time
   */
  start?: string;
  supplier: Supplier;
  trustedAcceptance?: boolean;
}
