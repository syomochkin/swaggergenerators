/* tslint:disable */
/* eslint-disable */

/* hello world */

export interface RecurrenceRange {
  endDate?: string;
  numberOfOccurrences?: number;
  startDate: string;
  type: string;
}
