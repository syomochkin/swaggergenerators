/* tslint:disable */
/* eslint-disable */
import { ZoneOffsetReq } from './zone-offset-req';

/* hello world */

export interface OffsetTimeReq {
  offset?: ZoneOffsetReq;
}
