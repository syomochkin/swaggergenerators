/* tslint:disable */
/* eslint-disable */
import { OrderDetails } from './order-details';
import { SupplierReq } from './supplier-req';

/* hello world */

export interface DeliveryDetails {
  id: number;
  orders?: Array<OrderDetails>;
  supplier: SupplierReq;
}
