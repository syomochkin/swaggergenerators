/* tslint:disable */
/* eslint-disable */
import { DateTimeSlotDto } from './date-time-slot-dto';
import { DeliveryDto } from './delivery-dto';

/* hello world */

export interface ReservationRequest {
  delivery: DeliveryDto;
  regularReservationId?: number;
  siteCode: string;
  timeSlot: DateTimeSlotDto;
}
