/* tslint:disable */
/* eslint-disable */
import { DeliveryVehicleDto } from './delivery-vehicle-dto';
import { OrderDto } from './order-dto';
import { SupplierDto } from './supplier-dto';

/* hello world */

export interface DeliveryDto {
  id: number;
  orders?: Array<OrderDto>;
  supplier?: SupplierDto;
  vehicles?: Array<DeliveryVehicleDto>;
}
