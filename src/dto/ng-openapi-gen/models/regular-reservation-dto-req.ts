/* tslint:disable */
/* eslint-disable */
import { RecurrentTimeSlotDtoReq } from './recurrent-time-slot-dto-req';

/* hello world */

export interface RegularReservationDtoReq {
  confirmationDeadline?: number;
  id?: number;
  ruleId: number;
  siteCode: string;
  status?: 'CANCELLED' | 'SCHEDULED' | 'UNPROCESSED';
  supplierId?: number;
  timeSlot: RecurrentTimeSlotDtoReq;
}
