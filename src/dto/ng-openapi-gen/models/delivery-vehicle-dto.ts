/* tslint:disable */
/* eslint-disable */
import { DeliveryVehicleStatusDto } from './delivery-vehicle-status-dto';

/* hello world */

export interface DeliveryVehicleDto {
  fullName?: string;
  hasTransitCargo?: boolean;
  model?: string;
  number?: string;
  phoneNumber?: string;
  status?: DeliveryVehicleStatusDto;
}
