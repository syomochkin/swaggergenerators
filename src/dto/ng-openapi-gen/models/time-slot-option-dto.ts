/* tslint:disable */
/* eslint-disable */
import { DateTimeSlotDto } from './date-time-slot-dto';

/* hello world */

export interface TimeSlotOptionDto {
  orderNumber?: string;
  timeSlots?: Array<DateTimeSlotDto>;
}
