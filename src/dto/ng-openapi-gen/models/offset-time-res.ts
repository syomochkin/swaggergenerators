/* tslint:disable */
/* eslint-disable */
import { ZoneOffsetRes } from './zone-offset-res';

/* hello world */

export interface OffsetTimeRes {
  hour?: number;
  minute?: number;
  nano?: number;
  offset?: ZoneOffsetRes;
  second?: number;
}
