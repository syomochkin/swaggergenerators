/* tslint:disable */
/* eslint-disable */
import { DeliveryVehicleDto } from './delivery-vehicle-dto';
import { OrderDto } from './order-dto';

/* hello world */

export interface GateDeliveryDto {
  id?: number;
  orders?: Array<OrderDto>;
  vehicles?: Array<DeliveryVehicleDto>;
}
