import * as request from "superagent";
import {
    SuperAgentStatic
} from "superagent";

type CallbackHandler = (err: any, res ? : request.Response) => void;
type AddPlaceTypeCommonCharacteristicDto = {
    'characteristic' ? : string

    'defaultValue' ? : DefaultValue

    'settings' ? : CommonCharacteristicSettings

};
type AddStockToCarrierCmd = {
    'baseAmount' ? : number

    'baseUnit' ? : string

    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'productBatchId' ? : string

    'productId' ? : string

    'quantity' ? : number

    'stockType' ? : string

    'tabNumber' ? : string

    'unit' ? : string

    'userName' ? : string

};
type AllocationSettingsDto = {
    'maxMixedProducts' ? : number

    'placeTypeCode' ? : string

    'zoneCode' ? : string

};
type AllocationZoneTransferReq = {
    'carrierId' ? : string

    'destinationPlaceId' ? : string

    'operationId' ? : string

    'startPlaceId' ? : string

    'transferId' ? : string

};
type AssignProductBatchCmd = {
    'expirationTime' ? : string

    'inboundId' ? : string

    'inboundTime' ? : string

    'manufactureTime' ? : string

    'mercuryExtId' ? : string

    'number' ? : string

    'productId' ? : string

    'productName' ? : string

    'siteCode' ? : string

    'type' ? : "REGULAR" | "RETURN"

    'unit' ? : string

    'vendorCode' ? : string

    'vendorName' ? : string

    'weightProduct' ? : boolean

};
type BoxRoute = {
    'operationId' ? : string

    'plannedTotalDuration' ? : number

    'rejectedItems' ? : Array < ProductAmountDto >
        | ProductAmountDto

    'steps' ? : Array < RouteStep >
        | RouteStep

    'transferId' ? : string

};
type BuildBoxRouteCmd = {
    'items' ? : Array < ProductAmountDto >
        | ProductAmountDto

    'operationId' ? : string

    'siteCode' ? : string

    'transferId' ? : string

};
type BulkOperationDto = {
    'operationId' ? : string

    'status' ? : string

};
type BulkPlaceUpdateStatusCmd = {
    'operationId' ? : string

    'placeIds' ? : Array < string >
        | string

    'statusCode' ? : string

};
type BulkPlaceUpdateTypeCmd = {
    'operationId' ? : string

    'placeIds' ? : Array < string >
        | string

    'targetPlaceTypeId' ? : string

};
type BulkPlaceUpdateZoneCmd = {
    'operationId' ? : string

    'placeIds' ? : Array < string >
        | string

    'zoneId' ? : string

};
type CancelStockReservationCmd = {
    'incomingQuantity' ? : number

    'operationId' ? : string

    'outgoingQuantity' ? : number

    'siteCode' ? : string

    'stockId' ? : string

    'transferId' ? : string

    'unit' ? : string

};
type CarrierDto = {
    'author' ? : string

    'barcode' ? : string

    'carriersTypeId' ? : string

    'createdTime' ? : number

    'editor' ? : string

    'id' ? : string

    'nestedCarriers' ? : Array < CarrierDto >
        | CarrierDto

    'parentId' ? : string

    'parentNumber' ? : string

    'placeAddress' ? : string

    'placeId' ? : string

    'processId' ? : string

    'type' ? : CarrierTypeDto

    'updatedTime' ? : number

};
type CarrierGridView = {
    'barcode' ? : string

    'id' ? : string

    'nestedCarriers' ? : Array < CarrierGridView >
        | CarrierGridView

    'status' ? : "incoming" | "outgoing" | "actual"

    'type' ? : CarrierTypeGridView

};
type CarrierShippedResponse = {
    'carrierId' ? : string

    'carrierMoveResponses' ? : Array < MoveCarrierToPlaceResponse >
        | MoveCarrierToPlaceResponse

    'operationId' ? : string

    'placeId' ? : string

};
type CarrierTypeCodeExistsQuery = {
    'code' ? : string

};
type CarrierTypeDto = {
    'archived' ? : boolean

    'author' ? : string

    'code' ? : string

    'createdTime' ? : number

    'description' ? : string

    'editor' ? : string

    'height' ? : number

    'id' ? : string

    'length' ? : number

    'maxVolume' ? : number

    'maxWeight' ? : number

    'name' ? : string

    'updatedTime' ? : number

    'width' ? : number

};
type CarrierTypeGridView = {
    'code' ? : string

    'description' ? : string

    'id' ? : string

    'name' ? : string

};
type ChangeRawStock = {
    'position' ? : number

    'stockId' ? : string

};
type ChangeRawStockTypeCmd = {
    'operationId' ? : string

    'orderVersion' ? : string

    'outboundDeliveryNumber' ? : string

    'outboundTime' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'siteCode' ? : string

    'stocks' ? : Array < ChangeRawStock >
        | ChangeRawStock

    'tabNumber' ? : string

    'userName' ? : string

};
type CharacteristicDto = {
    'type' ? : string

    'value' ? : ValueDto

};
type CodeExists = {
    'exists' ? : boolean

};
type CommonCharacteristicSettings = {
    'editable' ? : boolean

    'mandatory' ? : boolean

    'parentInheritance' ? : "check" | "ignore"

    'unit' ? : string

};
type ConfirmProductBatchCmd = {
    'batchId' ? : string

    'targetStockType' ? : string

};
type Coordinates3D = {
    'x' ? : number

    'y' ? : number

    'z' ? : number

};
type CoordinatesDto = {
    'x' ? : number

    'y' ? : number

    'z' ? : number

};
type CreateCarrierCmd = {
    'carrierBarcode' ? : string

    'carrierId' ? : string

    'carrierTypeCode' ? : string

    'placeBarcode' ? : string

    'processId' ? : string

    'siteCode' ? : string

};
type CreateCarrierTypeCmd = {
    'code' ? : string

    'description' ? : string

    'height' ? : number

    'id' ? : string

    'length' ? : number

    'maxVolume' ? : number

    'maxWeight' ? : number

    'name' ? : string

    'width' ? : number

};
type CreatePlaceCmd = {
    'address' ? : string

    'addressTypeId' ? : string

    'coordinates' ? : Coordinates3D

    'description' ? : string

    'id' ? : string

    'number' ? : number

    'parentId' ? : string

    'siteId' ? : string

    'status' ? : PlaceStatusDto

    'statusReason' ? : string

    'typeId' ? : string

    'zoneIds' ? : Array < string >
        | string

};
type CreatePlaceTypeCmd = {
    'code' ? : string

    'coordinatesRequired' ? : boolean

    'description' ? : string

    'maxMixBatches' ? : number

    'name' ? : string

    'numerationRule' ? : "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP"

    'placeTypeId' ? : string

    'storagePlace' ? : boolean

};
type CreatePlanRequest = {
    'carrierId' ? : string

    'startPlaceId' ? : string

    'targetZoneId' ? : string

    'transferId' ? : string

};
type CreateSiteCmd = {
    'code' ? : string

    'description' ? : string

    'id' ? : string

    'name' ? : string

};
type CreateZoneCmd = {
    'boundedWarehouseProductGroups' ? : Array < WarehouseProductGroupDto >
        | WarehouseProductGroupDto

    'code' ? : string

    'description' ? : string

    'id' ? : string

    'name' ? : string

    'siteId' ? : string

    'typeId' ? : string

};
type CreateZoneTypeCmd = {
    'code' ? : string

    'description' ? : string

    'id' ? : string

    'name' ? : string

};
type Data = {
    'address' ? : string

    'author' ? : string

    'childrenAmount' ? : number

    'createdTime' ? : number

    'editor' ? : string

    'id' ? : string

    'status' ? : PlaceStatusDto

    'typeName' ? : string

    'updatedTime' ? : number

    'zoneNames' ? : Array < string >
        | string

};
type DefaultValue = {
    'decValue' ? : number

    'intValue' ? : number

    'maxIntValue' ? : number

    'minIntValue' ? : number

};
type DetailedCarriersSearchQuery = {
    'barcodes' ? : Array < string >
        | string

};
type DetailedStockSearchQuery = {
    'carrierId' ? : string

    'carrierNumber' ? : string

    'inCarrier' ? : "IN_ONLY" | "OUT_ONLY" | "ALL"

    'maxResponseSize' ? : number

    'placeAddress' ? : string

    'placeId' ? : string

    'productBatchId' ? : string

    'productBatchNumber' ? : string

    'productId' ? : string

    'siteCode' ? : string

    'stockId' ? : string

    'stockTypeCode' ? : string

};
type DetailedStocksSearchResult = {
    'stocks' ? : Array < StockDto >
        | StockDto

    'totalCount' ? : number

};
type DropAllProcessReservationsCmd = {
    'operationId' ? : string

    'processId' ? : string

    'siteCode' ? : string

};
type DropCarrierReservationForProcessCmd = {
    'carrierNumber' ? : string

    'dropForNestedCarriers' ? : boolean

    'operationId' ? : string

    'processId' ? : string

    'siteCode' ? : string

};
type DropPlaceReservationForProcessCmd = {
    'operationId' ? : string

    'placeBarcode' ? : string

    'processId' ? : string

    'siteCode' ? : string

};
type DropPlaceReservationsForTransferCmd = {
    'operationId' ? : string

    'processId' ? : string

    'siteCode' ? : string

    'transferId' ? : string

};
type DropTransferCmd = {
    'transferId' ? : string

};
type Error = {
    'code' ? : string

    'message': string

};
type FullAmount = {
    'quantity' ? : number

    'unit' ? : string

    'volume' ? : number

    'weight' ? : number

};
type ModifyStockTypeCmd = {
    'code' ? : string

    'name' ? : string

};
type MonoPalletPlacementRequest = {
    'incomingCarrierId' ? : string

    'openCarriers' ? : Array < string >
        | string

    'operationId' ? : string

    'productBatchId' ? : string

    'productId' ? : string

    'quantity' ? : number

    'siteCode' ? : string

    'stockTypeCode' ? : string

};
type MoveAllStocksToCarrierCmd = {
    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'siteCode' ? : string

    'sourcePlaceId' ? : string

    'tabNumber' ? : string

    'targetCarrierId' ? : string

    'transferId' ? : string

    'userName' ? : string

};
type MoveAllStocksToCarrierResponse = {
    'operationId' ? : string

    'stockDto' ? : Array < StockDto >
        | StockDto

    'targetCarrierId' ? : string

    'transferId' ? : string

};
type MoveCarrierToPlaceResponse = {
    'carrierId' ? : string

    'operationId' ? : string

    'rejected' ? : boolean

    'transferComplete' ? : boolean

};
type MoveStockCmd = {
    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'quantity' ? : number

    'stockId' ? : string

    'tabNumber' ? : string

    'targetCarrierId' ? : string

    'targetPlaceId' ? : string

    'userName' ? : string

};
type MoveStockCmdResponse = {
    'movedQuantity' ? : number

    'operationId' ? : string

    'originalStockId' ? : string

    'targetStockActualQuantity' ? : number

    'targetStockId' ? : string

};
type MoveStockToCarrierCmd = {
    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'quantity' ? : number

    'sourcePlaceId' ? : string

    'sourceStockId' ? : string

    'tabNumber' ? : string

    'targetCarrierId' ? : string

    'transferId' ? : string

    'unit' ? : string

    'userName' ? : string

};
type NamedId = {
    'label' ? : string

    'value' ? : string

};
type OrderBoxDto = {
    'carrierId' ? : string

    'orderItems' ? : Array < OutboundOrderItemDto >
        | OutboundOrderItemDto

};
type OrderByCriteria = {
    'field' ? : string

    'order' ? : "ASC" | "DESC"

};
type OutboundOrderCmd = {
    'operationId' ? : string

    'orderBoxes' ? : Array < OrderBoxDto >
        | OrderBoxDto

    'orderVersion' ? : string

    'outboundDeliveryNumber' ? : string

    'outboundTime' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'siteCode' ? : string

    'tabNumber' ? : string

    'unshippedOrderItems' ? : Array < UnshippedOrderItemDto >
        | UnshippedOrderItemDto

    'userName' ? : string

};
type OutboundOrderItemDto = {
    'baseAmount' ? : number

    'baseUnit' ? : string

    'position' ? : number

    'productBatchId' ? : string

    'productId' ? : string

    'quantity' ? : number

    'unit' ? : string

};
type OverridePlaceCommonCharacteristicCmd = {
    'characteristic' ? : string

    'siteId' ? : string

    'value' ? : ValueCmd

};
type PageRequestInfo = {
    'limit' ? : number

    'offset' ? : number

};
type PickerRouteReq = {
    'pickingZoneId' ? : string

    'siteCode' ? : string

    'startPlaceId' ? : string

    'transferIds' ? : Array < string >
        | string

};
type PickingZoneTransferReq = {
    'carrierId' ? : string

    'destinationPlaceId' ? : string

    'operationId' ? : string

    'startPlaceId' ? : string

    'transferId' ? : string

};
type PlaceCharacteristicDto = {
    'author' ? : string

    'characteristicId' ? : string

    'checkParents' ? : string

    'createdTime' ? : number

    'editable' ? : boolean

    'editor' ? : string

    'mandatory' ? : boolean

    'placeId' ? : string

    'siteId' ? : string

    'type' ? : string

    'typeCharacteristicId' ? : string

    'updatedTime' ? : number

    'value' ? : ValueDto

};
type PlaceDto = {
    'address' ? : string

    'addressTypeId' ? : string

    'author' ? : string

    'childPlaces' ? : Array < PlaceDto >
        | PlaceDto

    'coordinates' ? : CoordinatesDto

    'createdTime' ? : number

    'description' ? : string

    'editor' ? : string

    'id' ? : string

    'number' ? : number

    'parentId' ? : string

    'parentPlaces' ? : Array < PlaceDto >
        | PlaceDto

    'siteId' ? : string

    'status' ? : PlaceStatusDto

    'statusReason' ? : string

    'storagePlace' ? : boolean

    'typeCode' ? : string

    'typeId' ? : string

    'typeName' ? : string

    'updatedTime' ? : number

    'zones' ? : Array < ZoneInfo >
        | ZoneInfo

};
type PlaceHierarchyFilter = {
    'hierarchyElements' ? : Array < PlaceHierarchyFilterCondition >
        | PlaceHierarchyFilterCondition

    'siteId' ? : string

};
type PlaceHierarchyFilterCondition = {
    'criteria' ? : string

    'placeTypeId' ? : string

};
type PlaceHierarchyTreeDto = {
    'address' ? : string

    'characteristics' ? : Array < CharacteristicDto >
        | CharacteristicDto

    'children' ? : Array < PlaceHierarchyTreeDto >
        | PlaceHierarchyTreeDto

    'id' ? : string

    'number' ? : number

    'status' ? : string

    'type' ? : Type

    'zones' ? : Array < Zone >
        | Zone

};
type PlaceInfo = {
    'address' ? : string

    'author' ? : string

    'childrenAmount' ? : number

    'createdTime' ? : number

    'editor' ? : string

    'id' ? : string

    'placeZones' ? : Array < PlaceZoneInfo >
        | PlaceZoneInfo

    'siteId' ? : string

    'siteName' ? : string

    'typeName' ? : string

    'updatedTime' ? : number

    'zoneNames' ? : Array < string >
        | string

};
type PlaceNodeWithPosition = {
    'childPlaces' ? : Array < Data >
        | Data

    'placeNodeChainFromTop' ? : Array < NamedId >
        | NamedId

};
type PlaceSearchFilter = {
    'hierarchy' ? : PlaceHierarchyFilter

    'placeCategory' ? : "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL"

    'placeTypes' ? : Array < string >
        | string

    'zonesType' ? : Array < PlaceZonesFilter >
        | PlaceZonesFilter

};
type PlaceShortView = {
    'barcode' ? : string

    'description' ? : string

    'id' ? : string

    'siteCode' ? : string

    'typeCode' ? : string

    'typeName' ? : string

};
type PlaceStatusDto = {
    'code' ? : string

    'default' ? : boolean

    'title' ? : string

};
type PlaceTypeCharacteristicDTO = {
    'author' ? : string

    'characteristicId' ? : string

    'checkParents' ? : string

    'createdTime' ? : number

    'defaultValue' ? : DefaultValue

    'editable' ? : boolean

    'editor' ? : string

    'mandatory' ? : boolean

    'placeTypeId' ? : string

    'type' ? : string

    'updatedTime' ? : number

};
type PlaceTypeCodeExistsQuery = {
    'code' ? : string

};
type PlaceTypeDto = {
    'author' ? : string

    'code' ? : string

    'coordinatesRequired' ? : boolean

    'createdTime' ? : number

    'description' ? : string

    'editor' ? : string

    'maxMixBatches' ? : number

    'name' ? : string

    'numberOfImplementation' ? : number

    'numerationRule' ? : string

    'placeTypeId' ? : string

    'siteId' ? : string

    'storagePlace' ? : boolean

    'updatedTime' ? : number

};
type PlaceWithPosition = {
    'place' ? : PlaceDto

    'placeNodeChainFromTop' ? : Array < NamedId >
        | NamedId

};
type PlaceWithType = {
    'address' ? : string

    'id' ? : string

    'number' ? : number

    'type' ? : Type

};
type PlaceZoneInfo = {
    'zoneCode' ? : string

    'zoneName' ? : string

    'zoneTypeCode' ? : string

    'zoneTypeName' ? : string

};
type PlaceZonesFilter = {
    'zoneTypeId' ? : string

    'zonesId' ? : Array < string >
        | string

};
type PlacesSearchQuery = {
    'filter' ? : PlaceSearchFilter

    'orderInfo' ? : Array < OrderByCriteria >
        | OrderByCriteria

    'pageInfo' ? : PageRequestInfo

};
type PlacesSearchResult = {
    'places' ? : Array < PlaceInfo >
        | PlaceInfo

    'totalCount' ? : number

};
type ProductAmountDto = {
    'productId' ? : string

    'quantity' ? : number

    'rejectionReason' ? : "NO_STOCK" | "NO_ROUTE"

    'unit' ? : string

};
type ProductBatchBalanceByStockType = {
    'baseAmountERP' ? : number

    'baseAmountWMS' ? : number

    'quantityERP' ? : number

    'quantityWMS' ? : number

    'stockTypeCode' ? : string

};
type ProductBatchBalanceDifferenceDto = {
    'canCorrect' ? : boolean

    'needCorrect' ? : boolean

    'stockTypes' ? : Array < ProductBatchBalanceByStockType >
        | ProductBatchBalanceByStockType

};
type ProductBatchDto = {
    'expirationTime' ? : number

    'id' ? : string

    'inboundId' ? : string

    'inboundTime' ? : number

    'manufactureTime' ? : number

    'mercuryExtId' ? : string

    'number' ? : string

    'productId' ? : string

    'productName' ? : string

    'siteId' ? : string

    'type' ? : "REGULAR" | "RETURN"

    'vendorCode' ? : string

    'vendorName' ? : string

    'weightProduct' ? : boolean

};
type ProductBatchSearchDto = {
    'confirmed' ? : boolean

    'expirationTime' ? : number

    'id' ? : string

    'inboundTime' ? : number

    'manufactureTime' ? : number

    'productBatchNumber' ? : string

    'productId' ? : string

    'productName' ? : string

    'siteCode' ? : string

    'siteId' ? : string

    'type' ? : "REGULAR" | "RETURN"

    'vendorCode' ? : string

    'vendorName' ? : string

};
type ProductBatchSearchFilter = {
    'confirmed' ? : boolean

    'expirationDateFrom' ? : string

    'expirationDateTo' ? : string

    'manufactureTimeFrom' ? : string

    'manufactureTimeTo' ? : string

    'number' ? : string

    'productId' ? : string

    'siteCode' ? : string

    'siteId' ? : string

    'types' ? : Array < "REGULAR" | "RETURN" >
        | "REGULAR" | "RETURN"

};
type ProductBatchesSearchQuery = {
    'filter' ? : ProductBatchSearchFilter

    'orderInfo' ? : Array < OrderByCriteria >
        | OrderByCriteria

    'pageInfo' ? : PageRequestInfo

};
type ProductBatchesSearchResult = {
    'productBatches' ? : Array < ProductBatchSearchDto >
        | ProductBatchSearchDto

    'totalCount' ? : number

};
type ProductPlacementRequest = {
    'openCarriers' ? : Array < string >
        | string

    'operationId' ? : string

    'productBatchId' ? : string

    'productId' ? : string

    'quantity' ? : number

    'siteCode' ? : string

    'stockTypeCode' ? : string

};
type ProductPlacementResponse = {
    'clientError' ? : Error

    'operationId' ? : string

    'productId' ? : string

    'rejectedInfo' ? : RejectedInfo

    'reservedCarriers' ? : Array < ReservedCarrier >
        | ReservedCarrier

};
type RawPlacementRequest = {
    'incomingCarrierId' ? : string

    'openCarriers' ? : Array < string >
        | string

    'operationId' ? : string

    'productBatchId' ? : string

    'productId' ? : string

    'quantity' ? : number

    'siteCode' ? : string

    'stockTypeCode' ? : string

};
type RawStock = {
    'position' ? : number

    'quantity' ? : number

    'stockId' ? : string

};
type ReduceIncomingQuantityCmd = {
    'operationId' ? : string

    'productBatchId' ? : string

    'quantity' ? : number

    'stockTypeCode' ? : string

    'transferId' ? : string

};
type ReducePlaceReservationForTransferCmd = {
    'carrierTypeCode' ? : string

    'operationId' ? : string

    'placeId' ? : string

    'quantity' ? : number

    'siteCode' ? : string

    'transferId' ? : string

};
type RejectedInfo = {
    'clientError' ? : Error

    'quantity' ? : number

};
type ReplenishCmd = {
    'destPlaceBarcode' ? : string

    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'productBatchId' ? : string

    'productId' ? : string

    'quantity' ? : number

    'siteCode' ? : string

    'srcCarrierId' ? : string

    'stockTypeCode' ? : string

    'tabNumber' ? : string

    'targetStockTypeCode' ? : string

    'userName' ? : string

};
type ReservationCancelledResponse = {
    'incomingQuantity' ? : number

    'operationId' ? : string

    'outgoingQuantity' ? : number

    'siteCode' ? : string

    'stockId' ? : string

    'transferId' ? : string

};
type ReservationDto = {
    'batchId' ? : string

    'placeAddress' ? : string

    'placeId' ? : string

    'productId' ? : string

    'quantityIncoming' ? : number

    'quantityOutgoing' ? : number

    'stockId' ? : string

    'transferId' ? : string

};
type ReservationInfoDto = {
    'batchId' ? : string

    'incomingQuantity' ? : number

    'outgoingQuantity' ? : number

    'productId' ? : string

    'stockId' ? : string

    'stockTypeCode' ? : string

    'unit' ? : string

};
type ReserveCarrierForProcessCmd = {
    'carrierNumber' ? : string

    'operationId' ? : string

    'processId' ? : string

    'siteCode' ? : string

};
type ReserveCarrierForProcessResponse = {
    'clientError' ? : Error

    'operationId' ? : string

};
type ReserveInfo = {
    'placeAddress' ? : string

    'placeId' ? : string

    'transfer' ? : TransferDto

};
type ReservePlaceForProcessCmd = {
    'operationId' ? : string

    'placeBarcode' ? : string

    'processId' ? : string

    'siteCode' ? : string

};
type ReservePlacesForCarriersCmd = {
    'carrierPlaceTypeCode' ? : string

    'carriers' ? : Array < TransferDto >
        | TransferDto

    'operationId' ? : string

    'processId' ? : string

    'processName' ? : string

    'processPlaceTypeCode' ? : string

    'reservationMode' ? : "FULL" | "PARTIAL"

    'siteCode' ? : string

    'zones' ? : Array < ZoneReference >
        | ZoneReference

};
type ReserveStockOutgoingQuantityResponse = {
    'operationId' ? : string

    'reservation' ? : ReservationDto

};
type ReservedCarrier = {
    'openNewCarrier' ? : boolean

    'openedCarrierId' ? : string

    'quantity' ? : number

};
type ReservedPlacesForCarriersResponse = {
    'clientError' ? : Error

    'operationId' ? : string

    'processId' ? : string

    'processName' ? : string

    'rejectedTransfers' ? : Array < TransferDto >
        | TransferDto

    'reserves' ? : Array < ReserveInfo >
        | ReserveInfo

    'siteCode' ? : string

};
type Route = {
    'steps' ? : Array < RouteStep >
        | RouteStep

};
type RouteStep = {
    'allocationSector' ? : ZoneWithType

    'allocationZone' ? : ZoneWithType

    'characteristics' ? : Array < CharacteristicDto >
        | CharacteristicDto

    'floor' ? : PlaceWithType

    'hierarchy' ? : Array < PlaceWithType >
        | PlaceWithType

    'item' ? : StockAmountDto

    'pickingSector' ? : ZoneWithType

    'pickingZone' ? : ZoneWithType

    'place' ? : PlaceWithType

    'plannedDuration' ? : number

    'transferId' ? : string

    'zones' ? : Array < ZoneWithType >
        | ZoneWithType

};
type ShipCarrierCmd = {
    'carrierId' ? : string

    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'siteCode' ? : string

    'tabNumber' ? : string

    'userName' ? : string

};
type SiteCodeExistsQuery = {
    'code' ? : string

};
type SiteDto = {
    'archived' ? : boolean

    'author' ? : string

    'code' ? : string

    'createdTime' ? : number

    'description' ? : string

    'editor' ? : string

    'id' ? : string

    'name' ? : string

    'updatedTime' ? : number

};
type SiteSettingDto = {
    'bigDecimalValue' ? : number

    'code' ? : "MAX_MIX_PRODUCTS"

    'intValue' ? : number

    'stringValue' ? : string

    'type' ? : "INTEGER" | "STRING" | "BIG_DECIMAL"

};
type SlottingRoute = {
    'plannedDuration' ? : number

    'steps' ? : Array < SlottingRouteStep >
        | SlottingRouteStep

    'transferId' ? : string

};
type SlottingRouteStep = {
    'characteristics' ? : Array < CharacteristicDto >
        | CharacteristicDto

    'hierarchy' ? : Array < PlaceWithType >
        | PlaceWithType

    'place' ? : PlaceWithType

    'plannedDuration' ? : number

    'reservation' ? : ReservationInfoDto

    'transferId' ? : string

    'zones' ? : Array < ZoneWithType >
        | ZoneWithType

};
type StockAddressesAndProductIdsSearchFilter = {
    'addresses' ? : Array < string >
        | string

    'productIds' ? : Array < string >
        | string

    'siteId' ? : string

};
type StockAddressesAndProductIdsSearchQuery = {
    'filter' ? : StockAddressesAndProductIdsSearchFilter

    'orderInfo' ? : Array < OrderByCriteria >
        | OrderByCriteria

    'pageInfo' ? : PageRequestInfo

};
type StockAmountDto = {
    'productId' ? : string

    'quantity' ? : number

    'stockId' ? : string

    'unit' ? : string

};
type StockDto = {
    'actualQuantity' ? : number

    'actualVolume' ? : number

    'actualWeight' ? : number

    'availableQuantity' ? : number

    'availableVolume' ? : number

    'availableWeight' ? : number

    'baseUnit' ? : string

    'batchConfirmed' ? : boolean

    'batchId' ? : string

    'batchNumber' ? : string

    'carrierId' ? : string

    'incomingQuantity' ? : number

    'incomingVolume' ? : number

    'incomingWeight' ? : number

    'outgoingQuantity' ? : number

    'outgoingVolume' ? : number

    'outgoingWeight' ? : number

    'placeId' ? : string

    'productId' ? : string

    'productName' ? : string

    'stockId' ? : string

    'stockTypeCode' ? : string

    'unit' ? : string

};
type StockHierarchySearchFilter = {
    'hierarchy' ? : PlaceHierarchyFilter

    'placeCategory' ? : "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL"

    'placeTypes' ? : Array < string >
        | string

    'productIds' ? : Array < string >
        | string

    'zonesType' ? : Array < PlaceZonesFilter >
        | PlaceZonesFilter

};
type StockHierarchySearchQuery = {
    'filter' ? : StockHierarchySearchFilter

    'orderInfo' ? : Array < OrderByCriteria >
        | OrderByCriteria

    'pageInfo' ? : PageRequestInfo

};
type StockInCmd = {
    'baseAmount' ? : number

    'baseUnit' ? : string

    'carrierId' ? : string

    'documentDate' ? : string

    'documentNumber' ? : string

    'operationId' ? : string

    'placeId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'productBatchId' ? : string

    'quantity' ? : number

    'stockTypeCode' ? : string

    'tabNumber' ? : string

    'unit' ? : string

    'userName' ? : string

};
type StockInResponse = {
    'operationId' ? : string

    'stockId' ? : string

};
type StockInfo = {
    'actual' ? : FullAmount

    'available' ? : FullAmount

    'batchConfirmed' ? : boolean

    'batchId' ? : string

    'batchNumber' ? : string

    'carrier' ? : boolean

    'expirationTime' ? : number

    'inbound' ? : FullAmount

    'inboundTime' ? : number

    'manufactureTime' ? : number

    'outbound' ? : FullAmount

    'placeAddress' ? : string

    'placeId' ? : string

    'productId' ? : string

    'productName' ? : string

    'sellByTime' ? : number

    'stockTypeCode' ? : string

    'stockTypeId' ? : string

};
type StockTypeDto = {
    'author' ? : string

    'code' ? : string

    'createdTime' ? : number

    'editor' ? : string

    'id' ? : string

    'name' ? : string

    'updatedTime' ? : number

};
type StocksSearchResult = {
    'stocks' ? : Array < StockInfo >
        | StockInfo

    'totalCount' ? : number

};
type TransferDroppedResponse = {
    'canceledReservations' ? : Array < ReservationDto >
        | ReservationDto

    'transferId' ? : string

};
type TransferDto = {
    'carrierTypeCode' ? : string

    'quantity' ? : number

    'transferId' ? : string

};
type TryAllocateSpaceForReplenishmentCmd = {
    'destPlaceBarcode' ? : string

    'operationId' ? : string

    'processId' ? : string

    'productBatchId' ? : string

    'quantity' ? : number

    'siteCode' ? : string

    'srcCarrierId' ? : string

    'stockTypeCode' ? : string

};
type TryAllocateSpaceForReplenishmentResponse = {
    'approvedQuantity' ? : number

    'operationId' ? : string

    'rejectedInfo' ? : RejectedInfo

};
type TryTakeRequest = {
    'operationId' ? : string

    'placeId' ? : string

    'quantity' ? : number

    'stockId' ? : string

    'transferId' ? : string

    'unit' ? : string

};
type TryTakeResponse = {
    'acceptedQuantity' ? : number

    'operationId' ? : string

    'rejectReason' ? : string

    'rejected' ? : boolean

    'stockId' ? : string

    'transferId' ? : string

};
type Type = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

};
type UnshippedOrderItemDto = {
    'baseAmount' ? : number

    'baseUnit' ? : string

    'position' ? : number

    'productId' ? : string

    'quantity' ? : number

    'unit' ? : string

};
type UpdateAllocationSettingsRequest = {
    'maxMixedProducts' ? : number

    'placeTypeCode' ? : string

    'siteCode' ? : string

    'zoneCodes' ? : Array < string >
        | string

};
type UpdateCarrierTypeCmd = {
    'description' ? : string

    'height' ? : number

    'length' ? : number

    'maxVolume' ? : number

    'maxWeight' ? : number

    'name' ? : string

    'width' ? : number

};
type UpdatePlaceCmd = {
    'address' ? : string

    'addressTypeId' ? : string

    'coordinates' ? : Coordinates3D

    'description' ? : string

    'number' ? : number

    'status' ? : PlaceStatusDto

    'statusReason' ? : string

    'typeId' ? : string

    'zoneIds' ? : Array < string >
        | string

};
type UpdatePlaceTypeCmd = {
    'description' ? : string

    'maxMixBatches' ? : number

    'name' ? : string

    'numerationRule' ? : "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP"

    'storagePlace' ? : boolean

};
type UpdatePlaceTypeCommonCharacteristicDto = {
    'characteristic' ? : string

    'defaultValue' ? : DefaultValue

};
type UpdateSiteCmd = {
    'description' ? : string

    'name' ? : string

};
type UpdateStockTypeByInventoryCmd = {
    'baseAmount' ? : number

    'docDate' ? : string

    'documentNumber' ? : string

    'operationId' ? : string

    'operationType' ? : "MOVE"

    'orderItem' ? : number

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'saleAmount' ? : number

    'salesOrder' ? : string

    'specStock' ? : string

    'stockId' ? : string

    'stockTypeCode' ? : string

    'tabNumber' ? : string

    'transferId' ? : string

    'updateReason' ? : "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED"

    'userName' ? : string

};
type UpdateStockTypeByPickingCmd = {
    'docDate' ? : string

    'documentNumber' ? : string

    'operationId' ? : string

    'operationType' ? : "MOVE"

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'stockId' ? : string

    'stockTypeCode' ? : string

    'tabNumber' ? : string

    'updateReason' ? : "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED"

    'userName' ? : string

};
type UpdateStockTypeEvent = {
    'actualQuantity' ? : number

    'batchId' ? : string

    'batchNumber' ? : string

    'beiQuantity' ? : number

    'beiUnit' ? : string

    'carrierId' ? : string

    'documentDate' ? : string

    'documentNumber' ? : string

    'expirationDate' ? : string

    'manufactureDate' ? : string

    'newStockId' ? : string

    'newStockTypeCode' ? : string

    'oldStockId' ? : string

    'oldStockTypeCode' ? : string

    'operationId' ? : string

    'operationType' ? : "MOVE"

    'orderItemIndex' ? : number

    'placeWithType' ? : PlaceWithType

    'productId' ? : string

    'productName' ? : string

    'salesOrder' ? : string

    'shouldNotifyERP' ? : boolean

    'siteCode' ? : string

    'specialStockCode' ? : string

    'storageDate' ? : string

    'tabNumber' ? : string

    'unit' ? : string

    'updateReason' ? : "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED"

};
type UpdateStockTypeForBatchCmd = {
    'batchId' ? : string

    'newStockTypeCode' ? : string

    'oldStockTypeCode' ? : string

    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'tabNumber' ? : string

    'userName' ? : string

};
type UpdateStockTypeInStockByUICmd = {
    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'quantity' ? : number

    'stockId' ? : string

    'stockTypeCode' ? : string

    'tabNumber' ? : string

    'userName' ? : string

};
type UpdateZoneCmd = {
    'boundedWarehouseProductGroups' ? : Array < WarehouseProductGroupDto >
        | WarehouseProductGroupDto

    'code' ? : string

    'description' ? : string

    'name' ? : string

};
type UpdateZoneTypeCmd = {
    'description' ? : string

    'name' ? : string

};
type ValueCmd = {
    'decValue' ? : number

    'intValue' ? : number

    'maxIntValue' ? : number

    'minIntValue' ? : number

};
type ValueDto = {
    'decValue' ? : number

    'intValue' ? : number

    'maxDecValue' ? : number

    'maxIntValue' ? : number

    'minDecValue' ? : number

    'minIntValue' ? : number

};
type WarehouseProductGroupDto = {
    'code' ? : string

};
type WriteOffLostStockCmd = {
    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'quantity' ? : number

    'stockId' ? : string

    'tabNumber' ? : string

    'userName' ? : string

};
type WriteOffRawStocksCmd = {
    'operationId' ? : string

    'orderVersion' ? : string

    'outboundDeliveryNumber' ? : string

    'outboundTime' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'siteCode' ? : string

    'stocks' ? : Array < RawStock >
        | RawStock

    'tabNumber' ? : string

    'userName' ? : string

};
type WriteOffStockCmd = {
    'baseAmount' ? : number

    'baseUnit' ? : string

    'documentDate' ? : string

    'documentNumber' ? : string

    'operationId' ? : string

    'processId' ? : string

    'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION"

    'quantity' ? : number

    'stockId' ? : string

    'tabNumber' ? : string

    'unit' ? : string

    'userName' ? : string

};
type WriteOffStockResponse = {
    'clientError' ? : Error

    'operationId' ? : string

    'stockId' ? : string

};
type Zone = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

    'type' ? : Type

};
type ZoneCodeExistsQuery = {
    'code' ? : string

    'siteId' ? : string

    'typeId' ? : string

};
type ZoneDto = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

};
type ZoneInfo = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

    'typeCode' ? : string

    'typeId' ? : string

    'typeName' ? : string

};
type ZoneProjection = {
    'author' ? : string

    'boundedWarehouseProductGroups' ? : Array < WarehouseProductGroupDto >
        | WarehouseProductGroupDto

    'childAmount' ? : number

    'code' ? : string

    'createdTime' ? : number

    'description' ? : string

    'editor' ? : string

    'id' ? : string

    'name' ? : string

    'siteId' ? : string

    'typeId' ? : string

    'typeName' ? : string

    'updatedTime' ? : number

};
type ZoneReference = {
    'zoneCode' ? : string

    'zoneTypeCode' ? : string

};
type ZoneTypeCodeExistsQuery = {
    'code' ? : string

};
type ZoneTypeDto = {
    'author' ? : string

    'code' ? : string

    'createdTime' ? : number

    'description' ? : string

    'editor' ? : string

    'id' ? : string

    'name' ? : string

    'numberOfImplementation' ? : number

    'updatedTime' ? : number

    'warehouseProductGroupCanBeBounded' ? : boolean

};
type ZoneTypeWithZones = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

    'zones' ? : Array < ZoneDto >
        | ZoneDto

};
type ZoneTypeWithZonesDto = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

    'zones' ? : Array < ZoneDto >
        | ZoneDto

};
type ZoneWithType = {
    'code' ? : string

    'id' ? : string

    'name' ? : string

    'type' ? : Type

};

type Logger = {
    log: (line: string) => any
};

/**
 * Api Documentation
 * @class Test
 * @param {(string)} [domainOrOptions] - The project domain.
 */
export default class Test {

    private domain: string = "";
    private errorHandlers: CallbackHandler[] = [];

    constructor(domain ? : string, private logger ? : Logger) {
        if (domain) {
            this.domain = domain;
        }
    }

    getDomain() {
        return this.domain;
    }

    addErrorHandler(handler: CallbackHandler) {
        this.errorHandlers.push(handler);
    }

    private request(method: string, url: string, body: any, headers: any, queryParameters: any, form: any, reject: CallbackHandler, resolve: CallbackHandler) {
        if (this.logger) {
            this.logger.log(`Call ${method} ${url}`);
        }

        let req = (request as SuperAgentStatic)(method, url).query(queryParameters);

        Object.keys(headers).forEach(key => {
            req.set(key, headers[key]);
        });

        if (body) {
            req.send(body);
        }

        if (typeof(body) === 'object' && !(body.constructor.name === 'Buffer')) {
            req.set('Content-Type', 'application/json');
        }

        if (Object.keys(form).length > 0) {
            req.type('form');
            req.send(form);
        }

        req.end((error, response) => {
            if (error || !response.ok) {
                reject(error);
                this.errorHandlers.forEach(handler => handler(error));
            } else {
                resolve(response);
            }
        });
    }

    uploadAllocationSettingsUsingPOSTURL(parameters: {
        'settingsFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocation-settings/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * settingsFile, siteId - обязательные поля
     * @method
     * @name Test#uploadAllocationSettingsUsingPOST
     * @param {file} settingsFile - settingsFile
     * @param {} siteId - siteId
     */
    uploadAllocationSettingsUsingPOST(parameters: {
        'settingsFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocation-settings/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['settingsFile'] !== undefined) {
                form['settingsFile'] = parameters['settingsFile'];
            }

            if (parameters['settingsFile'] === undefined) {
                reject(new Error('Missing required  parameter: settingsFile'));
                return;
            }

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    monoPalletProductPlacementUsingPOSTURL(parameters: {
        'request': MonoPalletPlacementRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/monoPalletProductPlacement';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * operationId, productId, productBatchId - обязательные поля
     * @method
     * @name Test#monoPalletProductPlacementUsingPOST
     * @param {} request - request
     */
    monoPalletProductPlacementUsingPOST(parameters: {
        'request': MonoPalletPlacementRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/monoPalletProductPlacement';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['request'] !== undefined) {
                body = parameters['request'];
            }

            if (parameters['request'] === undefined) {
                reject(new Error('Missing required  parameter: request'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    productPlacementUsingPOSTURL(parameters: {
        'request': ProductPlacementRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/productPlacement';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * operationId, productId, productBatchId - обязательные поля
     * @method
     * @name Test#productPlacementUsingPOST
     * @param {} request - request
     */
    productPlacementUsingPOST(parameters: {
        'request': ProductPlacementRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/productPlacement';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['request'] !== undefined) {
                body = parameters['request'];
            }

            if (parameters['request'] === undefined) {
                reject(new Error('Missing required  parameter: request'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    rawPlacementUsingPOSTURL(parameters: {
        'request': RawPlacementRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/rawPlacement';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * operationId, productId, productBatchId - обязательные поля
     * @method
     * @name Test#rawPlacementUsingPOST
     * @param {} request - request
     */
    rawPlacementUsingPOST(parameters: {
        'request': RawPlacementRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/rawPlacement';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['request'] !== undefined) {
                body = parameters['request'];
            }

            if (parameters['request'] === undefined) {
                reject(new Error('Missing required  parameter: request'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getSettingsForSiteAndPlaceTypeUsingGETURL(parameters: {
        'placeTypeCode' ? : string,
        'siteCode' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/settings';
        if (parameters['placeTypeCode'] !== undefined) {
            queryParameters['placeTypeCode'] = parameters['placeTypeCode'];
        }

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * siteCode, placeTypeCode - обязательные поля
     * @method
     * @name Test#getSettingsForSiteAndPlaceTypeUsingGET
     * @param {string} placeTypeCode - placeTypeCode
     * @param {string} siteCode - siteCode
     */
    getSettingsForSiteAndPlaceTypeUsingGET(parameters: {
        'placeTypeCode' ? : string,
        'siteCode' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/settings';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['placeTypeCode'] !== undefined) {
                queryParameters['placeTypeCode'] = parameters['placeTypeCode'];
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateAllocationSettingsUsingPOSTURL(parameters: {
        'request': UpdateAllocationSettingsRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/settings';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * siteCode, placeTypeCode, maxMixedProducts  - обязательные поля
     * @method
     * @name Test#updateAllocationSettingsUsingPOST
     * @param {} request - request
     */
    updateAllocationSettingsUsingPOST(parameters: {
        'request': UpdateAllocationSettingsRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/allocations/settings';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['request'] !== undefined) {
                body = parameters['request'];
            }

            if (parameters['request'] === undefined) {
                reject(new Error('Missing required  parameter: request'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    boxRouteUsingPOSTURL(parameters: {
        'cmd': BuildBoxRouteCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/box-routes/build';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос в МKТ для маршрутизации коробов комплектации
     * @method
     * @name Test#boxRouteUsingPOST
     * @param {} cmd - cmd
     */
    boxRouteUsingPOST(parameters: {
        'cmd': BuildBoxRouteCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/box-routes/build';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    pickerRoutesUsingPOSTURL(parameters: {
        'cmd': PickerRouteReq,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/picker-routes/build';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос в МKТ для текущего маршрута комплектовщика
     * @method
     * @name Test#pickerRoutesUsingPOST
     * @param {} cmd - cmd
     */
    pickerRoutesUsingPOST(parameters: {
        'cmd': PickerRouteReq,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/picker-routes/build';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    findTransferUsingPOSTURL(parameters: {
        'cmd': PickingZoneTransferReq,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/picker-transfer/build';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос в МKТ для перехода между областями действий
     * @method
     * @name Test#findTransferUsingPOST
     * @param {} cmd - cmd
     */
    findTransferUsingPOST(parameters: {
        'cmd': PickingZoneTransferReq,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/picker-transfer/build';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getConsolidationZoneUsingGETURL(parameters: {
        'zoneId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/picking/consolidation-zone';
        if (parameters['zoneId'] !== undefined) {
            queryParameters['zoneId'] = parameters['zoneId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос для получения области консолидации для области комплектации
     * @method
     * @name Test#getConsolidationZoneUsingGET
     * @param {string} zoneId - zoneId
     */
    getConsolidationZoneUsingGET(parameters: {
        'zoneId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/picking/consolidation-zone';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['zoneId'] !== undefined) {
                queryParameters['zoneId'] = parameters['zoneId'];
            }

            if (parameters['zoneId'] === undefined) {
                reject(new Error('Missing required  parameter: zoneId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getAllUsingGET_1URL(parameters: {
        'inboundId': string,
        'manufactureTime' ? : number,
        'productId': string,
        'siteCode': string,
        'type' ? : "REGULAR" | "RETURN",
        'vendorCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch';
        if (parameters['inboundId'] !== undefined) {
            queryParameters['inboundId'] = parameters['inboundId'];
        }

        if (parameters['manufactureTime'] !== undefined) {
            queryParameters['manufactureTime'] = parameters['manufactureTime'];
        }

        if (parameters['productId'] !== undefined) {
            queryParameters['productId'] = parameters['productId'];
        }

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters['type'] !== undefined) {
            queryParameters['type'] = parameters['type'];
        }

        if (parameters['vendorCode'] !== undefined) {
            queryParameters['vendorCode'] = parameters['vendorCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getAll
     * @method
     * @name Test#getAllUsingGET_1
     * @param {string} inboundId - inboundId
     * @param {integer} manufactureTime - manufactureTime
     * @param {string} productId - productId
     * @param {string} siteCode - siteCode
     * @param {string} type - type
     * @param {string} vendorCode - vendorCode
     */
    getAllUsingGET_1(parameters: {
        'inboundId': string,
        'manufactureTime' ? : number,
        'productId': string,
        'siteCode': string,
        'type' ? : "REGULAR" | "RETURN",
        'vendorCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['inboundId'] !== undefined) {
                queryParameters['inboundId'] = parameters['inboundId'];
            }

            if (parameters['inboundId'] === undefined) {
                reject(new Error('Missing required  parameter: inboundId'));
                return;
            }

            if (parameters['manufactureTime'] !== undefined) {
                queryParameters['manufactureTime'] = parameters['manufactureTime'];
            }

            if (parameters['productId'] !== undefined) {
                queryParameters['productId'] = parameters['productId'];
            }

            if (parameters['productId'] === undefined) {
                reject(new Error('Missing required  parameter: productId'));
                return;
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters['type'] !== undefined) {
                queryParameters['type'] = parameters['type'];
            }

            if (parameters['vendorCode'] !== undefined) {
                queryParameters['vendorCode'] = parameters['vendorCode'];
            }

            if (parameters['vendorCode'] === undefined) {
                reject(new Error('Missing required  parameter: vendorCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    assignBatchUsingPOSTURL(parameters: {
        'cmd': AssignProductBatchCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * assignBatch
     * @method
     * @name Test#assignBatchUsingPOST
     * @param {} cmd - cmd
     */
    assignBatchUsingPOST(parameters: {
        'cmd': AssignProductBatchCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getBatchBalanceDifferenceUsingGETURL(parameters: {
        'productBatchId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/balance/{productBatchId}/difference';

        path = path.replace('{productBatchId}', `${parameters['productBatchId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getBatchBalanceDifference
     * @method
     * @name Test#getBatchBalanceDifferenceUsingGET
     * @param {string} productBatchId - productBatchId
     */
    getBatchBalanceDifferenceUsingGET(parameters: {
        'productBatchId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/balance/{productBatchId}/difference';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{productBatchId}', `${parameters['productBatchId']}`);

            if (parameters['productBatchId'] === undefined) {
                reject(new Error('Missing required  parameter: productBatchId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    fixBatchBalanceDifferenceUsingPOSTURL(parameters: {
        'productBatchId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/balance/{productBatchId}/fix';

        path = path.replace('{productBatchId}', `${parameters['productBatchId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * fixBatchBalanceDifference
     * @method
     * @name Test#fixBatchBalanceDifferenceUsingPOST
     * @param {string} productBatchId - productBatchId
     */
    fixBatchBalanceDifferenceUsingPOST(parameters: {
        'productBatchId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/balance/{productBatchId}/fix';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{productBatchId}', `${parameters['productBatchId']}`);

            if (parameters['productBatchId'] === undefined) {
                reject(new Error('Missing required  parameter: productBatchId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    confirmBatchUsingPOSTURL(parameters: {
        'confirmProductBatchCmd': ConfirmProductBatchCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/confirm';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * "Обеление" вида запаса
     * @method
     * @name Test#confirmBatchUsingPOST
     * @param {} confirmProductBatchCmd - confirmProductBatchCmd
     */
    confirmBatchUsingPOST(parameters: {
        'confirmProductBatchCmd': ConfirmProductBatchCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/confirm';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['confirmProductBatchCmd'] !== undefined) {
                body = parameters['confirmProductBatchCmd'];
            }

            if (parameters['confirmProductBatchCmd'] === undefined) {
                reject(new Error('Missing required  parameter: confirmProductBatchCmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    searchUsingPOST_1URL(parameters: {
        'query': ProductBatchesSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/search';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Search Batches
     * @method
     * @name Test#searchUsingPOST_1
     * @param {} query - query
     */
    searchUsingPOST_1(parameters: {
        'query': ProductBatchesSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/search';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    uploadProductBatchesUsingPOSTURL(parameters: {
        'batchesFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * batchesFile, siteId - обязательные поля
     * @method
     * @name Test#uploadProductBatchesUsingPOST
     * @param {file} batchesFile - batchesFile
     * @param {} siteId - siteId
     */
    uploadProductBatchesUsingPOST(parameters: {
        'batchesFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['batchesFile'] !== undefined) {
                form['batchesFile'] = parameters['batchesFile'];
            }

            if (parameters['batchesFile'] === undefined) {
                reject(new Error('Missing required  parameter: batchesFile'));
                return;
            }

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getByIdUsingGET_1URL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getById
     * @method
     * @name Test#getByIdUsingGET_1
     * @param {string} id - id
     */
    getByIdUsingGET_1(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/product-batch/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    replenishUsingPOSTURL(parameters: {
        'cmd': ReplenishCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/replenish';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Размещение товара
     * @method
     * @name Test#replenishUsingPOST
     * @param {} cmd - cmd
     */
    replenishUsingPOST(parameters: {
        'cmd': ReplenishCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/replenish';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    findTransferUsingPOST_1URL(parameters: {
        'cmd': AllocationZoneTransferReq,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/transfer/build';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос на построение и резервирование маршрута для перехода между областями действий
     * @method
     * @name Test#findTransferUsingPOST_1
     * @param {} cmd - cmd
     */
    findTransferUsingPOST_1(parameters: {
        'cmd': AllocationZoneTransferReq,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/transfer/build';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getTransferUsingGETURL(parameters: {
        'transferId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/transfer/{transferId}';

        path = path.replace('{transferId}', `${parameters['transferId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос на получение маршрута для перехода между областями действий
     * @method
     * @name Test#getTransferUsingGET
     * @param {string} transferId - transferId
     */
    getTransferUsingGET(parameters: {
        'transferId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/transfer/{transferId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{transferId}', `${parameters['transferId']}`);

            if (parameters['transferId'] === undefined) {
                reject(new Error('Missing required  parameter: transferId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    tryAllocateSpaceUsingPOSTURL(parameters: {
        'cmd': TryAllocateSpaceForReplenishmentCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/tryAllocateSpace';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Проверка места перед размещением товара
     * @method
     * @name Test#tryAllocateSpaceUsingPOST
     * @param {} cmd - cmd
     */
    tryAllocateSpaceUsingPOST(parameters: {
        'cmd': TryAllocateSpaceForReplenishmentCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/tryAllocateSpace';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    tryAllocateSpaceManualUsingPOSTURL(parameters: {
        'cmd': TryAllocateSpaceForReplenishmentCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/tryAllocateSpaceManual';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Проверка места перед ручным перемещением товара
     * @method
     * @name Test#tryAllocateSpaceManualUsingPOST
     * @param {} cmd - cmd
     */
    tryAllocateSpaceManualUsingPOST(parameters: {
        'cmd': TryAllocateSpaceForReplenishmentCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/replenishment/tryAllocateSpaceManual';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    routeStartRulesUploadUsingPOSTURL(parameters: {
        'routeStartRuleFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/route-start-rules/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * routeStartRuleFile, siteId - обязательные поля
     * @method
     * @name Test#routeStartRulesUploadUsingPOST
     * @param {file} routeStartRuleFile - routeStartRuleFile
     * @param {} siteId - siteId
     */
    routeStartRulesUploadUsingPOST(parameters: {
        'routeStartRuleFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/route-start-rules/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['routeStartRuleFile'] !== undefined) {
                form['routeStartRuleFile'] = parameters['routeStartRuleFile'];
            }

            if (parameters['routeStartRuleFile'] === undefined) {
                reject(new Error('Missing required  parameter: routeStartRuleFile'));
                return;
            }

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    uploadRouteStrategyUsingPOSTURL(parameters: {
        'routeStrategyFile': {},
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/route-strategy/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * routeStrategyFile, siteId - обязательные поля
     * @method
     * @name Test#uploadRouteStrategyUsingPOST
     * @param {file} routeStrategyFile - routeStrategyFile
     * @param {} siteCode - siteCode
     */
    uploadRouteStrategyUsingPOST(parameters: {
        'routeStrategyFile': {},
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/route-strategy/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['routeStrategyFile'] !== undefined) {
                form['routeStrategyFile'] = parameters['routeStrategyFile'];
            }

            if (parameters['routeStrategyFile'] === undefined) {
                reject(new Error('Missing required  parameter: routeStrategyFile'));
                return;
            }

            if (parameters['siteCode'] !== undefined) {
                form['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    slottingRouteUsingGETURL(parameters: {
        'currentPlaceId' ? : string,
        'startPlaceId': string,
        'transferId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/routes/{transferId}';
        if (parameters['currentPlaceId'] !== undefined) {
            queryParameters['currentPlaceId'] = parameters['currentPlaceId'];
        }

        if (parameters['startPlaceId'] !== undefined) {
            queryParameters['startPlaceId'] = parameters['startPlaceId'];
        }

        path = path.replace('{transferId}', `${parameters['transferId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
     * @method
     * @name Test#slottingRouteUsingGET
     * @param {string} currentPlaceId - currentPlaceId
     * @param {string} startPlaceId - startPlaceId
     * @param {string} transferId - transferId
     */
    slottingRouteUsingGET(parameters: {
        'currentPlaceId' ? : string,
        'startPlaceId': string,
        'transferId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/routes/{transferId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['currentPlaceId'] !== undefined) {
                queryParameters['currentPlaceId'] = parameters['currentPlaceId'];
            }

            if (parameters['startPlaceId'] !== undefined) {
                queryParameters['startPlaceId'] = parameters['startPlaceId'];
            }

            if (parameters['startPlaceId'] === undefined) {
                reject(new Error('Missing required  parameter: startPlaceId'));
                return;
            }

            path = path.replace('{transferId}', `${parameters['transferId']}`);

            if (parameters['transferId'] === undefined) {
                reject(new Error('Missing required  parameter: transferId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    triggerSnapshotUsingPOSTURL(parameters: {
        'entity': "PRODUCT_BATCH" | "STOCK" | "PLACE" | "PLACE_TYPE",
        'entityId': string,
        'type': "DELETE" | "SNAPSHOT",
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/snapshot/{entityId}';
        if (parameters['entity'] !== undefined) {
            queryParameters['entity'] = parameters['entity'];
        }

        path = path.replace('{entityId}', `${parameters['entityId']}`);
        if (parameters['type'] !== undefined) {
            queryParameters['type'] = parameters['type'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * triggerSnapshot
     * @method
     * @name Test#triggerSnapshotUsingPOST
     * @param {string} entity - entity
     * @param {string} entityId - entityId
     * @param {string} type - type
     */
    triggerSnapshotUsingPOST(parameters: {
        'entity': "PRODUCT_BATCH" | "STOCK" | "PLACE" | "PLACE_TYPE",
        'entityId': string,
        'type': "DELETE" | "SNAPSHOT",
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/snapshot/{entityId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['entity'] !== undefined) {
                queryParameters['entity'] = parameters['entity'];
            }

            if (parameters['entity'] === undefined) {
                reject(new Error('Missing required  parameter: entity'));
                return;
            }

            path = path.replace('{entityId}', `${parameters['entityId']}`);

            if (parameters['entityId'] === undefined) {
                reject(new Error('Missing required  parameter: entityId'));
                return;
            }

            if (parameters['type'] !== undefined) {
                queryParameters['type'] = parameters['type'];
            }

            if (parameters['type'] === undefined) {
                reject(new Error('Missing required  parameter: type'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getAllUsingGET_3URL(parameters: {
        'carrierId' ? : string,
        'placeBarcode' ? : string,
        'placeId' ? : string,
        'productBatchId' ? : string,
        'productId' ? : string,
        'siteCode' ? : string,
        'stockTypeCode' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock';
        if (parameters['carrierId'] !== undefined) {
            queryParameters['carrierId'] = parameters['carrierId'];
        }

        if (parameters['placeBarcode'] !== undefined) {
            queryParameters['placeBarcode'] = parameters['placeBarcode'];
        }

        if (parameters['placeId'] !== undefined) {
            queryParameters['placeId'] = parameters['placeId'];
        }

        if (parameters['productBatchId'] !== undefined) {
            queryParameters['productBatchId'] = parameters['productBatchId'];
        }

        if (parameters['productId'] !== undefined) {
            queryParameters['productId'] = parameters['productId'];
        }

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters['stockTypeCode'] !== undefined) {
            queryParameters['stockTypeCode'] = parameters['stockTypeCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getAll
     * @method
     * @name Test#getAllUsingGET_3
     * @param {string} carrierId - carrierId
     * @param {string} placeBarcode - placeBarcode
     * @param {string} placeId - placeId
     * @param {string} productBatchId - productBatchId
     * @param {string} productId - productId
     * @param {string} siteCode - siteCode
     * @param {string} stockTypeCode - stockTypeCode
     */
    getAllUsingGET_3(parameters: {
        'carrierId' ? : string,
        'placeBarcode' ? : string,
        'placeId' ? : string,
        'productBatchId' ? : string,
        'productId' ? : string,
        'siteCode' ? : string,
        'stockTypeCode' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['carrierId'] !== undefined) {
                queryParameters['carrierId'] = parameters['carrierId'];
            }

            if (parameters['placeBarcode'] !== undefined) {
                queryParameters['placeBarcode'] = parameters['placeBarcode'];
            }

            if (parameters['placeId'] !== undefined) {
                queryParameters['placeId'] = parameters['placeId'];
            }

            if (parameters['productBatchId'] !== undefined) {
                queryParameters['productBatchId'] = parameters['productBatchId'];
            }

            if (parameters['productId'] !== undefined) {
                queryParameters['productId'] = parameters['productId'];
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['stockTypeCode'] !== undefined) {
                queryParameters['stockTypeCode'] = parameters['stockTypeCode'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    searchUsingPOST_2URL(parameters: {
        'query': StockAddressesAndProductIdsSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/addressAndProductIdsSearch';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Search Stocks by Addresses and Product Ids
     * @method
     * @name Test#searchUsingPOST_2
     * @param {} query - query
     */
    searchUsingPOST_2(parameters: {
        'query': StockAddressesAndProductIdsSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/addressAndProductIdsSearch';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getCarrierTypesUsingGETURL(parameters: {
        'code' ? : Array < string >
            | string

            ,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types';
        if (parameters['code'] !== undefined) {
            queryParameters['code'] = parameters['code'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getCarrierTypes
     * @method
     * @name Test#getCarrierTypesUsingGET
     * @param {array} code - code
     */
    getCarrierTypesUsingGET(parameters: {
        'code' ? : Array < string >
            | string

            ,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['code'] !== undefined) {
                queryParameters['code'] = parameters['code'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createUsingPOST_1URL(parameters: {
        'cmd': CreateCarrierTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * create
     * @method
     * @name Test#createUsingPOST_1
     * @param {} cmd - cmd
     */
    createUsingPOST_1(parameters: {
        'cmd': CreateCarrierTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    codeExistsUsingPOSTURL(parameters: {
        'query': CarrierTypeCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/codeExists';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * codeExists
     * @method
     * @name Test#codeExistsUsingPOST
     * @param {} query - query
     */
    codeExistsUsingPOST(parameters: {
        'query': CarrierTypeCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/codeExists';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    carrierTypesUploadUsingPOSTURL(parameters: {
        'carrierTypeFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * carrierTypeFile - обязательные поля
     * @method
     * @name Test#carrierTypesUploadUsingPOST
     * @param {file} carrierTypeFile - carrierTypeFile
     */
    carrierTypesUploadUsingPOST(parameters: {
        'carrierTypeFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['carrierTypeFile'] !== undefined) {
                form['carrierTypeFile'] = parameters['carrierTypeFile'];
            }

            if (parameters['carrierTypeFile'] === undefined) {
                reject(new Error('Missing required  parameter: carrierTypeFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateUsingPUTURL(parameters: {
        'carrierTypeId': string,
        'cmd': UpdateCarrierTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/{carrierTypeId}';

        path = path.replace('{carrierTypeId}', `${parameters['carrierTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * update
     * @method
     * @name Test#updateUsingPUT
     * @param {string} carrierTypeId - carrierTypeId
     * @param {} cmd - cmd
     */
    updateUsingPUT(parameters: {
        'carrierTypeId': string,
        'cmd': UpdateCarrierTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/{carrierTypeId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{carrierTypeId}', `${parameters['carrierTypeId']}`);

            if (parameters['carrierTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: carrierTypeId'));
                return;
            }

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getCarrierTypeUsingGETURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getCarrierType
     * @method
     * @name Test#getCarrierTypeUsingGET
     * @param {string} id - id
     */
    getCarrierTypeUsingGET(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carrier-types/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getAllUsingGETURL(parameters: {
        'barcode' ? : string,
        'placeId' ? : string,
        'rootOnly' ? : boolean,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers';
        if (parameters['barcode'] !== undefined) {
            queryParameters['barcode'] = parameters['barcode'];
        }

        if (parameters['placeId'] !== undefined) {
            queryParameters['placeId'] = parameters['placeId'];
        }

        if (parameters['rootOnly'] !== undefined) {
            queryParameters['rootOnly'] = parameters['rootOnly'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getAll
     * @method
     * @name Test#getAllUsingGET
     * @param {string} barcode - barcode
     * @param {string} placeId - placeId
     * @param {boolean} rootOnly - rootOnly
     */
    getAllUsingGET(parameters: {
        'barcode' ? : string,
        'placeId' ? : string,
        'rootOnly' ? : boolean,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['barcode'] !== undefined) {
                queryParameters['barcode'] = parameters['barcode'];
            }

            if (parameters['placeId'] !== undefined) {
                queryParameters['placeId'] = parameters['placeId'];
            }

            if (parameters['rootOnly'] !== undefined) {
                queryParameters['rootOnly'] = parameters['rootOnly'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createUsingPOSTURL(parameters: {
        'cmd': CreateCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
     * @method
     * @name Test#createUsingPOST
     * @param {} cmd - cmd
     */
    createUsingPOST(parameters: {
        'cmd': CreateCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getByPlaceIdUsingGETURL(parameters: {
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers-view';
        if (parameters['placeId'] !== undefined) {
            queryParameters['placeId'] = parameters['placeId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Get carriers hierarchy in place for grid representation
     * @method
     * @name Test#getByPlaceIdUsingGET
     * @param {string} placeId - placeId
     */
    getByPlaceIdUsingGET(parameters: {
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers-view';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['placeId'] !== undefined) {
                queryParameters['placeId'] = parameters['placeId'];
            }

            if (parameters['placeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    dropReservationForProcessUsingPOSTURL(parameters: {
        'cmd': DropCarrierReservationForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/dropReservationForProcess';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#dropReservationForProcessUsingPOST
     * @param {} cmd - cmd
     */
    dropReservationForProcessUsingPOST(parameters: {
        'cmd': DropCarrierReservationForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/dropReservationForProcess';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getAllUsingPOSTURL(parameters: {
        'detailedCarriersSearchQuery': DetailedCarriersSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/find';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getAll
     * @method
     * @name Test#getAllUsingPOST
     * @param {} detailedCarriersSearchQuery - detailedCarriersSearchQuery
     */
    getAllUsingPOST(parameters: {
        'detailedCarriersSearchQuery': DetailedCarriersSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/find';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['detailedCarriersSearchQuery'] !== undefined) {
                body = parameters['detailedCarriersSearchQuery'];
            }

            if (parameters['detailedCarriersSearchQuery'] === undefined) {
                reject(new Error('Missing required  parameter: detailedCarriersSearchQuery'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    reserveForProcessUsingPOSTURL(parameters: {
        'cmd': ReserveCarrierForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/reserveForProcess';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#reserveForProcessUsingPOST
     * @param {} cmd - cmd
     */
    reserveForProcessUsingPOST(parameters: {
        'cmd': ReserveCarrierForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/reserveForProcess';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    shipUsingPOSTURL(parameters: {
        'cmd': ShipCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/ship';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#shipUsingPOST
     * @param {} cmd - cmd
     */
    shipUsingPOST(parameters: {
        'cmd': ShipCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/ship';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    carriersUploadUsingPOSTURL(parameters: {
        'carrierFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * carrierFile, siteId - обязательные поля
     * @method
     * @name Test#carriersUploadUsingPOST
     * @param {file} carrierFile - carrierFile
     * @param {} siteId - siteId
     */
    carriersUploadUsingPOST(parameters: {
        'carrierFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['carrierFile'] !== undefined) {
                form['carrierFile'] = parameters['carrierFile'];
            }

            if (parameters['carrierFile'] === undefined) {
                reject(new Error('Missing required  parameter: carrierFile'));
                return;
            }

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    addStockToCarrierUsingPUTURL(parameters: {
        'carrierId': string,
        'cmd': AddStockToCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{carrierId}';

        path = path.replace('{carrierId}', `${parameters['carrierId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Добавление запаса в носитель
     * @method
     * @name Test#addStockToCarrierUsingPUT
     * @param {string} carrierId - carrierId
     * @param {} cmd - cmd
     */
    addStockToCarrierUsingPUT(parameters: {
        'carrierId': string,
        'cmd': AddStockToCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{carrierId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{carrierId}', `${parameters['carrierId']}`);

            if (parameters['carrierId'] === undefined) {
                reject(new Error('Missing required  parameter: carrierId'));
                return;
            }

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    moveCarrierIntoCarrierUsingPUTURL(parameters: {
        'carrierId': string,
        'parentCarrierId': string,
        'processId' ? : string,
        'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION",
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}';

        path = path.replace('{carrierId}', `${parameters['carrierId']}`);

        path = path.replace('{parentCarrierId}', `${parameters['parentCarrierId']}`);
        if (parameters['processId'] !== undefined) {
            queryParameters['processId'] = parameters['processId'];
        }

        if (parameters['processType'] !== undefined) {
            queryParameters['processType'] = parameters['processType'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * moveCarrierIntoCarrier
     * @method
     * @name Test#moveCarrierIntoCarrierUsingPUT
     * @param {string} carrierId - carrierId
     * @param {string} parentCarrierId - parentCarrierId
     * @param {string} processId - processId
     * @param {string} processType - processType
     */
    moveCarrierIntoCarrierUsingPUT(parameters: {
        'carrierId': string,
        'parentCarrierId': string,
        'processId' ? : string,
        'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION",
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{carrierId}', `${parameters['carrierId']}`);

            if (parameters['carrierId'] === undefined) {
                reject(new Error('Missing required  parameter: carrierId'));
                return;
            }

            path = path.replace('{parentCarrierId}', `${parameters['parentCarrierId']}`);

            if (parameters['parentCarrierId'] === undefined) {
                reject(new Error('Missing required  parameter: parentCarrierId'));
                return;
            }

            if (parameters['processId'] !== undefined) {
                queryParameters['processId'] = parameters['processId'];
            }

            if (parameters['processType'] !== undefined) {
                queryParameters['processType'] = parameters['processType'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getByIdUsingGETURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getById
     * @method
     * @name Test#getByIdUsingGET
     * @param {string} id - id
     */
    getByIdUsingGET(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    deleteUsingDELETEURL(parameters: {
        'id': string,
        'processId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{id}';

        path = path.replace('{id}', `${parameters['id']}`);
        if (parameters['processId'] !== undefined) {
            queryParameters['processId'] = parameters['processId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Delete a Carrier. CarrierId are required.
     * @method
     * @name Test#deleteUsingDELETE
     * @param {string} id - id
     * @param {string} processId - processId
     */
    deleteUsingDELETE(parameters: {
        'id': string,
        'processId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/carriers/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters['processId'] !== undefined) {
                queryParameters['processId'] = parameters['processId'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('DELETE', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    confirmStocksUsingPOSTURL(parameters: {
        'cmd': ChangeRawStockTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/changeRawStockType';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#confirmStocksUsingPOST
     * @param {} cmd - cmd
     */
    confirmStocksUsingPOST(parameters: {
        'cmd': ChangeRawStockTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/changeRawStockType';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    findAllUsingPOSTURL(parameters: {
        'query': DetailedStockSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/findAll';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Детальный поиск по стокам
     * @method
     * @name Test#findAllUsingPOST
     * @param {} query - query
     */
    findAllUsingPOST(parameters: {
        'query': DetailedStockSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/findAll';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    moveStockToCarrierUsingPUTURL(parameters: {
        'cmd': MoveAllStocksToCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/moveAllTo';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#moveStockToCarrierUsingPUT
     * @param {} cmd - cmd
     */
    moveStockToCarrierUsingPUT(parameters: {
        'cmd': MoveAllStocksToCarrierCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/moveAllTo';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    moveStockUsingPOSTURL(parameters: {
        'cmd': MoveStockCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/moveStock';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Перемещение запаса (без проверок, без корректировки резервов)
     * @method
     * @name Test#moveStockUsingPOST
     * @param {} cmd - cmd
     */
    moveStockUsingPOST(parameters: {
        'cmd': MoveStockCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/moveStock';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    outboundOrderUsingPOSTURL(parameters: {
        'cmd': OutboundOrderCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/outboundOrder';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#outboundOrderUsingPOST
     * @param {} cmd - cmd
     */
    outboundOrderUsingPOST(parameters: {
        'cmd': OutboundOrderCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/outboundOrder';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    reduceIncomingQuantityUsingPOSTURL(parameters: {
        'request': ReduceIncomingQuantityCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reduceIncomingQuantity';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * reduce incoming quantity
     * @method
     * @name Test#reduceIncomingQuantityUsingPOST
     * @param {} request - request
     */
    reduceIncomingQuantityUsingPOST(parameters: {
        'request': ReduceIncomingQuantityCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reduceIncomingQuantity';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['request'] !== undefined) {
                body = parameters['request'];
            }

            if (parameters['request'] === undefined) {
                reject(new Error('Missing required  parameter: request'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    cancelStockReservationUsingPOSTURL(parameters: {
        'cmd': CancelStockReservationCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/cancelByStock';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей.
     * @method
     * @name Test#cancelStockReservationUsingPOST
     * @param {} cmd - cmd
     */
    cancelStockReservationUsingPOST(parameters: {
        'cmd': CancelStockReservationCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/cancelByStock';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    reserveUsingPOSTURL(parameters: {
        'cmd': ReservePlacesForCarriersCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/for-carriers';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Резервирование мест под носители
     * @method
     * @name Test#reserveUsingPOST
     * @param {} cmd - cmd
     */
    reserveUsingPOST(parameters: {
        'cmd': ReservePlacesForCarriersCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/for-carriers';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    dropReservationUsingDELETEURL(parameters: {
        'cmd': DropPlaceReservationsForTransferCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/for-carriers';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Сброс резерва места под носитель
     * @method
     * @name Test#dropReservationUsingDELETE
     * @param {} cmd - cmd
     */
    dropReservationUsingDELETE(parameters: {
        'cmd': DropPlaceReservationsForTransferCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/for-carriers';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('DELETE', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    reduceReservationForTransferUsingPUTURL(parameters: {
        'cmd': ReducePlaceReservationForTransferCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/reduce-for-carriers';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Уменьшить резерв под носители в месте
     * @method
     * @name Test#reduceReservationForTransferUsingPUT
     * @param {} cmd - cmd
     */
    reduceReservationForTransferUsingPUT(parameters: {
        'cmd': ReducePlaceReservationForTransferCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservation/reduce-for-carriers';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getAllUsingGET_2URL(parameters: {
        'transferId' ? : Array < string >
            | string

            ,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservations';
        if (parameters['transferId'] !== undefined) {
            queryParameters['transferId'] = parameters['transferId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос на получение списка резервов
     * @method
     * @name Test#getAllUsingGET_2
     * @param {array} transferId - transferId
     */
    getAllUsingGET_2(parameters: {
        'transferId' ? : Array < string >
            | string

            ,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reservations';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['transferId'] !== undefined) {
                queryParameters['transferId'] = parameters['transferId'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    reserveOutgoingUsingPOSTURL(parameters: {
        'operationId' ? : string,
        'quantity' ? : number,
        'quantityOptionalPresent' ? : boolean,
        'stockId' ? : string,
        'transferId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reserveOutgoingQuantity';
        if (parameters['operationId'] !== undefined) {
            queryParameters['operationId'] = parameters['operationId'];
        }

        if (parameters['quantity'] !== undefined) {
            queryParameters['quantity'] = parameters['quantity'];
        }

        if (parameters['quantityOptionalPresent'] !== undefined) {
            queryParameters['quantityOptional.present'] = parameters['quantityOptionalPresent'];
        }

        if (parameters['stockId'] !== undefined) {
            queryParameters['stockId'] = parameters['stockId'];
        }

        if (parameters['transferId'] !== undefined) {
            queryParameters['transferId'] = parameters['transferId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#reserveOutgoingUsingPOST
     * @param {string} operationId - Api Documentation
     * @param {integer} quantity - Api Documentation
     * @param {boolean} quantityOptionalPresent - Api Documentation
     * @param {string} stockId - Api Documentation
     * @param {string} transferId - Api Documentation
     */
    reserveOutgoingUsingPOST(parameters: {
        'operationId' ? : string,
        'quantity' ? : number,
        'quantityOptionalPresent' ? : boolean,
        'stockId' ? : string,
        'transferId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/reserveOutgoingQuantity';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['operationId'] !== undefined) {
                queryParameters['operationId'] = parameters['operationId'];
            }

            if (parameters['quantity'] !== undefined) {
                queryParameters['quantity'] = parameters['quantity'];
            }

            if (parameters['quantityOptionalPresent'] !== undefined) {
                queryParameters['quantityOptional.present'] = parameters['quantityOptionalPresent'];
            }

            if (parameters['stockId'] !== undefined) {
                queryParameters['stockId'] = parameters['stockId'];
            }

            if (parameters['transferId'] !== undefined) {
                queryParameters['transferId'] = parameters['transferId'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    dropTransferUsingPOSTURL(parameters: {
        'cmd': DropTransferCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/transfers/drop';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для служебных целей
     * @method
     * @name Test#dropTransferUsingPOST
     * @param {} cmd - cmd
     */
    dropTransferUsingPOST(parameters: {
        'cmd': DropTransferCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/places/transfers/drop';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    searchUsingPOST_3URL(parameters: {
        'query': StockHierarchySearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/search';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Search Stocks
     * @method
     * @name Test#searchUsingPOST_3
     * @param {} query - query
     */
    searchUsingPOST_3(parameters: {
        'query': StockHierarchySearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/search';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    stockInUsingPOSTURL(parameters: {
        'cmd': StockInCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-in';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Добавление найденного товара в место или носитель без проверок
     * @method
     * @name Test#stockInUsingPOST
     * @param {} cmd - cmd
     */
    stockInUsingPOST(parameters: {
        'cmd': StockInCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-in';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getStockTypesUsingGETURL(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос списка видов запаса
     * @method
     * @name Test#getStockTypesUsingGET
     */
    getStockTypesUsingGET(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createStockTypeUsingPOSTURL(parameters: {
        'cmd': ModifyStockTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * code - обязательное поле
     * @method
     * @name Test#createStockTypeUsingPOST
     * @param {} cmd - cmd
     */
    createStockTypeUsingPOST(parameters: {
        'cmd': ModifyStockTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getStockTypeByCodeUsingGETURL(parameters: {
        'code': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/code/{code}';

        path = path.replace('{code}', `${parameters['code']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос вида запаса по коду
     * @method
     * @name Test#getStockTypeByCodeUsingGET
     * @param {string} code - code
     */
    getStockTypeByCodeUsingGET(parameters: {
        'code': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/code/{code}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{code}', `${parameters['code']}`);

            if (parameters['code'] === undefined) {
                reject(new Error('Missing required  parameter: code'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    codeExistsUsingGETURL(parameters: {
        'code': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/codeExists/{code}';

        path = path.replace('{code}', `${parameters['code']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Проверка существует ли вид запаса с указанным кодом
     * @method
     * @name Test#codeExistsUsingGET
     * @param {string} code - code
     */
    codeExistsUsingGET(parameters: {
        'code': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/codeExists/{code}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{code}', `${parameters['code']}`);

            if (parameters['code'] === undefined) {
                reject(new Error('Missing required  parameter: code'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getStockTypeByIdUsingGETURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос вида запаса по идентификатору
     * @method
     * @name Test#getStockTypeByIdUsingGET
     * @param {string} id - id
     */
    getStockTypeByIdUsingGET(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateStockTypeUsingPUTURL(parameters: {
        'cmd': ModifyStockTypeCmd,
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * code - обязательное поле
     * @method
     * @name Test#updateStockTypeUsingPUT
     * @param {} cmd - cmd
     * @param {string} id - id
     */
    updateStockTypeUsingPUT(parameters: {
        'cmd': ModifyStockTypeCmd,
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    archiveStockTypeUsingDELETEURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Установка признака заархивирован
     * @method
     * @name Test#archiveStockTypeUsingDELETE
     * @param {string} id - id
     */
    archiveStockTypeUsingDELETE(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/stock-types/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('DELETE', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    tryTakeUsingPOSTURL(parameters: {
        'tryTakeRequest': TryTakeRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/try-take';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Запрос на попытку взять товар из ячейки
     * @method
     * @name Test#tryTakeUsingPOST
     * @param {} tryTakeRequest - tryTakeRequest
     */
    tryTakeUsingPOST(parameters: {
        'tryTakeRequest': TryTakeRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/try-take';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['tryTakeRequest'] !== undefined) {
                body = parameters['tryTakeRequest'];
            }

            if (parameters['tryTakeRequest'] === undefined) {
                reject(new Error('Missing required  parameter: tryTakeRequest'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateExpiredStockTypeUsingPOSTURL(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateExpired';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#updateExpiredStockTypeUsingPOST
     */
    updateExpiredStockTypeUsingPOST(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateExpired';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateStockTypeInStockByInventoryUsingPOSTURL(parameters: {
        'updateStockTypeCmd': UpdateStockTypeByInventoryCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeByInventory';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Изменение вида запаса инвентаризацией
     * @method
     * @name Test#updateStockTypeInStockByInventoryUsingPOST
     * @param {} updateStockTypeCmd - updateStockTypeCmd
     */
    updateStockTypeInStockByInventoryUsingPOST(parameters: {
        'updateStockTypeCmd': UpdateStockTypeByInventoryCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeByInventory';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['updateStockTypeCmd'] !== undefined) {
                body = parameters['updateStockTypeCmd'];
            }

            if (parameters['updateStockTypeCmd'] === undefined) {
                reject(new Error('Missing required  parameter: updateStockTypeCmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateStockTypeInStockByPickingUsingPOSTURL(parameters: {
        'updateStockTypeCmd': UpdateStockTypeByPickingCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeByPicking';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Изменение вида запаса комплектацией
     * @method
     * @name Test#updateStockTypeInStockByPickingUsingPOST
     * @param {} updateStockTypeCmd - updateStockTypeCmd
     */
    updateStockTypeInStockByPickingUsingPOST(parameters: {
        'updateStockTypeCmd': UpdateStockTypeByPickingCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeByPicking';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['updateStockTypeCmd'] !== undefined) {
                body = parameters['updateStockTypeCmd'];
            }

            if (parameters['updateStockTypeCmd'] === undefined) {
                reject(new Error('Missing required  parameter: updateStockTypeCmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateStockTypeInStockByUIUsingPOSTURL(parameters: {
        'cmd': UpdateStockTypeInStockByUICmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeByUI';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Изменение вида запаса через UI
     * @method
     * @name Test#updateStockTypeInStockByUIUsingPOST
     * @param {} cmd - cmd
     */
    updateStockTypeInStockByUIUsingPOST(parameters: {
        'cmd': UpdateStockTypeInStockByUICmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeByUI';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateStockTypeForBatchUsingPOSTURL(parameters: {
        'updateStockTypeForBatchCmd': UpdateStockTypeForBatchCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeForBatch';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Изменение вида запаса для партии. Вызывается после обеления.
     * @method
     * @name Test#updateStockTypeForBatchUsingPOST
     * @param {} updateStockTypeForBatchCmd - updateStockTypeForBatchCmd
     */
    updateStockTypeForBatchUsingPOST(parameters: {
        'updateStockTypeForBatchCmd': UpdateStockTypeForBatchCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/updateStockTypeForBatch';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['updateStockTypeForBatchCmd'] !== undefined) {
                body = parameters['updateStockTypeForBatchCmd'];
            }

            if (parameters['updateStockTypeForBatchCmd'] === undefined) {
                reject(new Error('Missing required  parameter: updateStockTypeForBatchCmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    stocksUploadUsingPOSTURL(parameters: {
        'siteId': string,
        'stocksFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * stocksFile, siteId - обязательные поля
     * @method
     * @name Test#stocksUploadUsingPOST
     * @param {} siteId - siteId
     * @param {file} stocksFile - stocksFile
     */
    stocksUploadUsingPOST(parameters: {
        'siteId': string,
        'stocksFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['stocksFile'] !== undefined) {
                form['stocksFile'] = parameters['stocksFile'];
            }

            if (parameters['stocksFile'] === undefined) {
                reject(new Error('Missing required  parameter: stocksFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    writeOffStockUsingPOSTURL(parameters: {
        'cmd': WriteOffStockCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/writeOff';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#writeOffStockUsingPOST
     * @param {} cmd - cmd
     */
    writeOffStockUsingPOST(parameters: {
        'cmd': WriteOffStockCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/writeOff';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    writeOffLostStockUsingPOSTURL(parameters: {
        'cmd': WriteOffLostStockCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/writeOffLost';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Списание потерянного запаса
     * @method
     * @name Test#writeOffLostStockUsingPOST
     * @param {} cmd - cmd
     */
    writeOffLostStockUsingPOST(parameters: {
        'cmd': WriteOffLostStockCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/writeOffLost';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    writeOffRawStockUsingPOSTURL(parameters: {
        'cmd': WriteOffRawStocksCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/writeOffRaw';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#writeOffRawStockUsingPOST
     * @param {} cmd - cmd
     */
    writeOffRawStockUsingPOST(parameters: {
        'cmd': WriteOffRawStocksCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/writeOffRaw';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getByIdUsingGET_2URL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getById
     * @method
     * @name Test#getByIdUsingGET_2
     * @param {string} id - id
     */
    getByIdUsingGET_2(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    moveStockToCarrierUsingPUT_1URL(parameters: {
        'carrierId': string,
        'cmd': MoveStockToCarrierCmd,
        'stockId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/{stockId}/moveTo/{carrierId}';

        path = path.replace('{carrierId}', `${parameters['carrierId']}`);

        path = path.replace('{stockId}', `${parameters['stockId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Перемещение запаса в носитель с корректировкой резерва
     * @method
     * @name Test#moveStockToCarrierUsingPUT_1
     * @param {string} carrierId - carrierId
     * @param {} cmd - cmd
     * @param {string} stockId - stockId
     */
    moveStockToCarrierUsingPUT_1(parameters: {
        'carrierId': string,
        'cmd': MoveStockToCarrierCmd,
        'stockId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/stock/{stockId}/moveTo/{carrierId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{carrierId}', `${parameters['carrierId']}`);

            if (parameters['carrierId'] === undefined) {
                reject(new Error('Missing required  parameter: carrierId'));
                return;
            }

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{stockId}', `${parameters['stockId']}`);

            if (parameters['stockId'] === undefined) {
                reject(new Error('Missing required  parameter: stockId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    outboundOrderUsingPOST_1URL(parameters: {
        'cmd': OutboundOrderCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/tool/outboundOrder';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
     * @method
     * @name Test#outboundOrderUsingPOST_1
     * @param {} cmd - cmd
     */
    outboundOrderUsingPOST_1(parameters: {
        'cmd': OutboundOrderCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/tool/outboundOrder';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneTypesUsingGETURL(parameters: {
        'siteId': string,
        'zoneTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        path = path.replace('{zoneTypeId}', `${parameters['zoneTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneTypes
     * @method
     * @name Test#getZoneTypesUsingGET
     * @param {string} siteId - siteId
     * @param {string} zoneTypeId - zoneTypeId
     */
    getZoneTypesUsingGET(parameters: {
        'siteId': string,
        'zoneTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            path = path.replace('{zoneTypeId}', `${parameters['zoneTypeId']}`);

            if (parameters['zoneTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: zoneTypeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    placeTypesUsingGETURL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/dictionary/sites/{siteId}/placeTypes';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * placeTypes
     * @method
     * @name Test#placeTypesUsingGET
     * @param {string} siteId - siteId
     */
    placeTypesUsingGET(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/dictionary/sites/{siteId}/placeTypes';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneTypesUsingGET_1URL(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/dictionary/zoneTypes';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneTypes
     * @method
     * @name Test#getZoneTypesUsingGET_1
     */
    getZoneTypesUsingGET_1(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/dictionary/zoneTypes';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceStatusesUsingGETURL(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeStatuses';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceStatuses
     * @method
     * @name Test#getPlaceStatusesUsingGET
     */
    getPlaceStatusesUsingGET(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeStatuses';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceTypeCharacteristicsUsingGETURL(parameters: {
        'placeTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics';

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceTypeCharacteristics
     * @method
     * @name Test#getPlaceTypeCharacteristicsUsingGET
     * @param {string} placeTypeId - placeTypeId
     */
    getPlaceTypeCharacteristicsUsingGET(parameters: {
        'placeTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    addUsingPOSTURL(parameters: {
        'placeTypeId': string,
        'req': AddPlaceTypeCommonCharacteristicDto,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics';

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * add
     * @method
     * @name Test#addUsingPOST
     * @param {string} placeTypeId - placeTypeId
     * @param {} req - req
     */
    addUsingPOST(parameters: {
        'placeTypeId': string,
        'req': AddPlaceTypeCommonCharacteristicDto,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            if (parameters['req'] !== undefined) {
                body = parameters['req'];
            }

            if (parameters['req'] === undefined) {
                reject(new Error('Missing required  parameter: req'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceTypeCharacteristicUsingGETURL(parameters: {
        'characteristicId': string,
        'placeTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';

        path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceTypeCharacteristic
     * @method
     * @name Test#getPlaceTypeCharacteristicUsingGET
     * @param {string} characteristicId - characteristicId
     * @param {string} placeTypeId - placeTypeId
     */
    getPlaceTypeCharacteristicUsingGET(parameters: {
        'characteristicId': string,
        'placeTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

            if (parameters['characteristicId'] === undefined) {
                reject(new Error('Missing required  parameter: characteristicId'));
                return;
            }

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateUsingPUT_2URL(parameters: {
        'characteristicId': string,
        'placeTypeId': string,
        'req': UpdatePlaceTypeCommonCharacteristicDto,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';

        path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * update
     * @method
     * @name Test#updateUsingPUT_2
     * @param {string} characteristicId - characteristicId
     * @param {string} placeTypeId - placeTypeId
     * @param {} req - req
     */
    updateUsingPUT_2(parameters: {
        'characteristicId': string,
        'placeTypeId': string,
        'req': UpdatePlaceTypeCommonCharacteristicDto,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

            if (parameters['characteristicId'] === undefined) {
                reject(new Error('Missing required  parameter: characteristicId'));
                return;
            }

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            if (parameters['req'] !== undefined) {
                body = parameters['req'];
            }

            if (parameters['req'] === undefined) {
                reject(new Error('Missing required  parameter: req'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    removeUsingDELETEURL(parameters: {
        'characteristicId': string,
        'placeTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';

        path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * remove
     * @method
     * @name Test#removeUsingDELETE
     * @param {string} characteristicId - characteristicId
     * @param {string} placeTypeId - placeTypeId
     */
    removeUsingDELETE(parameters: {
        'characteristicId': string,
        'placeTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

            if (parameters['characteristicId'] === undefined) {
                reject(new Error('Missing required  parameter: characteristicId'));
                return;
            }

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('DELETE', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceUsingGETURL(parameters: {
        'barcode' ? : string,
        'placeId' ? : string,
        'showChildren' ? : boolean,
        'siteCode' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places';
        if (parameters['barcode'] !== undefined) {
            queryParameters['barcode'] = parameters['barcode'];
        }

        if (parameters['placeId'] !== undefined) {
            queryParameters['placeId'] = parameters['placeId'];
        }

        if (parameters['showChildren'] !== undefined) {
            queryParameters['showChildren'] = parameters['showChildren'];
        }

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlace
     * @method
     * @name Test#getPlaceUsingGET
     * @param {string} barcode - barcode
     * @param {string} placeId - placeId
     * @param {boolean} showChildren - showChildren
     * @param {string} siteCode - siteCode
     */
    getPlaceUsingGET(parameters: {
        'barcode' ? : string,
        'placeId' ? : string,
        'showChildren' ? : boolean,
        'siteCode' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['barcode'] !== undefined) {
                queryParameters['barcode'] = parameters['barcode'];
            }

            if (parameters['placeId'] !== undefined) {
                queryParameters['placeId'] = parameters['placeId'];
            }

            if (parameters['showChildren'] !== undefined) {
                queryParameters['showChildren'] = parameters['showChildren'];
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createUsingPOST_2URL(parameters: {
        'cmd': CreatePlaceCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * create
     * @method
     * @name Test#createUsingPOST_2
     * @param {} cmd - cmd
     */
    createUsingPOST_2(parameters: {
        'cmd': CreatePlaceCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    startBulkUpdatePlaceStatusUsingPOSTURL(parameters: {
        'cmd': BulkPlaceUpdateStatusCmd,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/update-place-status';

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * startBulkUpdatePlaceStatus
     * @method
     * @name Test#startBulkUpdatePlaceStatusUsingPOST
     * @param {} cmd - cmd
     * @param {string} siteCode - siteCode
     */
    startBulkUpdatePlaceStatusUsingPOST(parameters: {
        'cmd': BulkPlaceUpdateStatusCmd,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/update-place-status';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    startBulkUpdatePlaceTypeUsingPOSTURL(parameters: {
        'cmd': BulkPlaceUpdateTypeCmd,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/update-place-type';

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * startBulkUpdatePlaceType
     * @method
     * @name Test#startBulkUpdatePlaceTypeUsingPOST
     * @param {} cmd - cmd
     * @param {string} siteCode - siteCode
     */
    startBulkUpdatePlaceTypeUsingPOST(parameters: {
        'cmd': BulkPlaceUpdateTypeCmd,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/update-place-type';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    startBulkUpdateZoneUsingPOSTURL(parameters: {
        'cmd': BulkPlaceUpdateZoneCmd,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/update-zone';

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * startBulkUpdateZone
     * @method
     * @name Test#startBulkUpdateZoneUsingPOST
     * @param {} cmd - cmd
     * @param {string} siteCode - siteCode
     */
    startBulkUpdateZoneUsingPOST(parameters: {
        'cmd': BulkPlaceUpdateZoneCmd,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/update-zone';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getBulkUpdateOperationDataUsingGETURL(parameters: {
        'operationId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/{operationId}';

        path = path.replace('{operationId}', `${parameters['operationId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getBulkUpdateOperationData
     * @method
     * @name Test#getBulkUpdateOperationDataUsingGET
     * @param {string} operationId - operationId
     */
    getBulkUpdateOperationDataUsingGET(parameters: {
        'operationId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/bulk/{operationId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{operationId}', `${parameters['operationId']}`);

            if (parameters['operationId'] === undefined) {
                reject(new Error('Missing required  parameter: operationId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    placesDownloadUsingGETURL(parameters: {
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/download/{siteCode}';

        path = path.replace('{siteCode}', `${parameters['siteCode']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * siteId - обязательные поля
     * @method
     * @name Test#placesDownloadUsingGET
     * @param {string} siteCode - siteCode
     */
    placesDownloadUsingGET(parameters: {
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/download/{siteCode}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteCode}', `${parameters['siteCode']}`);

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    dropAllReservationsForProcessUsingPOSTURL(parameters: {
        'cmd': DropAllProcessReservationsCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/dropAllReservationsForProcess';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#dropAllReservationsForProcessUsingPOST
     * @param {} cmd - cmd
     */
    dropAllReservationsForProcessUsingPOST(parameters: {
        'cmd': DropAllProcessReservationsCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/dropAllReservationsForProcess';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    dropReservationForProcessUsingPOST_1URL(parameters: {
        'cmd': DropPlaceReservationForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/dropReservationForProcess';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#dropReservationForProcessUsingPOST_1
     * @param {} cmd - cmd
     */
    dropReservationForProcessUsingPOST_1(parameters: {
        'cmd': DropPlaceReservationForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/dropReservationForProcess';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    findPlacesByProcessIdUsingGETURL(parameters: {
        'placeTypeCode': string,
        'processId': string,
        'showChildren' ? : boolean,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/findByProcessId';
        if (parameters['placeTypeCode'] !== undefined) {
            queryParameters['placeTypeCode'] = parameters['placeTypeCode'];
        }

        if (parameters['processId'] !== undefined) {
            queryParameters['processId'] = parameters['processId'];
        }

        if (parameters['showChildren'] !== undefined) {
            queryParameters['showChildren'] = parameters['showChildren'];
        }

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * findPlacesByProcessId
     * @method
     * @name Test#findPlacesByProcessIdUsingGET
     * @param {string} placeTypeCode - placeTypeCode
     * @param {string} processId - processId
     * @param {boolean} showChildren - showChildren
     * @param {string} siteCode - siteCode
     */
    findPlacesByProcessIdUsingGET(parameters: {
        'placeTypeCode': string,
        'processId': string,
        'showChildren' ? : boolean,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/findByProcessId';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['placeTypeCode'] !== undefined) {
                queryParameters['placeTypeCode'] = parameters['placeTypeCode'];
            }

            if (parameters['placeTypeCode'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeCode'));
                return;
            }

            if (parameters['processId'] !== undefined) {
                queryParameters['processId'] = parameters['processId'];
            }

            if (parameters['processId'] === undefined) {
                reject(new Error('Missing required  parameter: processId'));
                return;
            }

            if (parameters['showChildren'] !== undefined) {
                queryParameters['showChildren'] = parameters['showChildren'];
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getHierarchyUsingGETURL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/hierarchy/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getHierarchy
     * @method
     * @name Test#getHierarchyUsingGET
     * @param {string} siteId - siteId
     */
    getHierarchyUsingGET(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/hierarchy/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getHierarchyForParentIdUsingGETURL(parameters: {
        'parentId': string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/hierarchy/{siteId}/{parentId}';

        path = path.replace('{parentId}', `${parameters['parentId']}`);

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getHierarchyForParentId
     * @method
     * @name Test#getHierarchyForParentIdUsingGET
     * @param {string} parentId - parentId
     * @param {string} siteId - siteId
     */
    getHierarchyForParentIdUsingGET(parameters: {
        'parentId': string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/hierarchy/{siteId}/{parentId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{parentId}', `${parameters['parentId']}`);

            if (parameters['parentId'] === undefined) {
                reject(new Error('Missing required  parameter: parentId'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getHierarchyByTypeCodeUsingGETURL(parameters: {
        'siteId': string,
        'typeCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/hierarchyTree/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);
        if (parameters['typeCode'] !== undefined) {
            queryParameters['typeCode'] = parameters['typeCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getHierarchyByTypeCode
     * @method
     * @name Test#getHierarchyByTypeCodeUsingGET
     * @param {string} siteId - siteId
     * @param {string} typeCode - typeCode
     */
    getHierarchyByTypeCodeUsingGET(parameters: {
        'siteId': string,
        'typeCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/hierarchyTree/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['typeCode'] !== undefined) {
                queryParameters['typeCode'] = parameters['typeCode'];
            }

            if (parameters['typeCode'] === undefined) {
                reject(new Error('Missing required  parameter: typeCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    reserveForProcessUsingPOST_1URL(parameters: {
        'cmd': ReservePlaceForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/reserveForProcess';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Диагностический эндпоинт, предназначен только для тестовых целей
     * @method
     * @name Test#reserveForProcessUsingPOST_1
     * @param {} cmd - cmd
     */
    reserveForProcessUsingPOST_1(parameters: {
        'cmd': ReservePlaceForProcessCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/reserveForProcess';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    searchUsingPOSTURL(parameters: {
        'query': PlacesSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/search';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Search Places
     * @method
     * @name Test#searchUsingPOST
     * @param {} query - query
     */
    searchUsingPOST(parameters: {
        'query': PlacesSearchQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/search';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceTypesBySiteUsingGETURL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/search/dicts/placeTypes/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceTypesBySite
     * @method
     * @name Test#getPlaceTypesBySiteUsingGET
     * @param {string} siteId - siteId
     */
    getPlaceTypesBySiteUsingGET(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/search/dicts/placeTypes/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneTypesWithZonesUsingGETURL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/search/dicts/zoneTypes/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneTypesWithZones
     * @method
     * @name Test#getZoneTypesWithZonesUsingGET
     * @param {string} siteId - siteId
     */
    getZoneTypesWithZonesUsingGET(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/search/dicts/zoneTypes/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceByCodeUsingGETURL(parameters: {
        'placeBarcode': string,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/shortView';
        if (parameters['placeBarcode'] !== undefined) {
            queryParameters['placeBarcode'] = parameters['placeBarcode'];
        }

        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceByCode
     * @method
     * @name Test#getPlaceByCodeUsingGET
     * @param {string} placeBarcode - placeBarcode
     * @param {string} siteCode - siteCode
     */
    getPlaceByCodeUsingGET(parameters: {
        'placeBarcode': string,
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/shortView';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['placeBarcode'] !== undefined) {
                queryParameters['placeBarcode'] = parameters['placeBarcode'];
            }

            if (parameters['placeBarcode'] === undefined) {
                reject(new Error('Missing required  parameter: placeBarcode'));
                return;
            }

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getTransferPlaceUsingGETURL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/transfer/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getTransferPlace
     * @method
     * @name Test#getTransferPlaceUsingGET
     * @param {string} siteId - siteId
     */
    getTransferPlaceUsingGET(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/transfer/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    placesUploadUsingPOSTURL(parameters: {
        'placesFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * placesFile, siteId - обязательные поля
     * @method
     * @name Test#placesUploadUsingPOST
     * @param {file} placesFile - placesFile
     * @param {} siteId - siteId
     */
    placesUploadUsingPOST(parameters: {
        'placesFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['placesFile'] !== undefined) {
                form['placesFile'] = parameters['placesFile'];
            }

            if (parameters['placesFile'] === undefined) {
                reject(new Error('Missing required  parameter: placesFile'));
                return;
            }

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceByIdUsingGETURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceById
     * @method
     * @name Test#getPlaceByIdUsingGET
     * @param {string} id - id
     */
    getPlaceByIdUsingGET(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateUsingPUT_1URL(parameters: {
        'cmd': UpdatePlaceCmd,
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * update
     * @method
     * @name Test#updateUsingPUT_1
     * @param {} cmd - cmd
     * @param {string} id - id
     */
    updateUsingPUT_1(parameters: {
        'cmd': UpdatePlaceCmd,
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    deleteUsingDELETE_1URL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * delete
     * @method
     * @name Test#deleteUsingDELETE_1
     * @param {string} id - id
     */
    deleteUsingDELETE_1(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('DELETE', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceCharacteristicsUsingGETURL(parameters: {
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{placeId}/characteristics';

        path = path.replace('{placeId}', `${parameters['placeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceCharacteristics
     * @method
     * @name Test#getPlaceCharacteristicsUsingGET
     * @param {string} placeId - placeId
     */
    getPlaceCharacteristicsUsingGET(parameters: {
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{placeId}/characteristics';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{placeId}', `${parameters['placeId']}`);

            if (parameters['placeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceCharacteristicUsingGETURL(parameters: {
        'characteristicId': string,
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{placeId}/characteristics/{characteristicId}';

        path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

        path = path.replace('{placeId}', `${parameters['placeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceCharacteristic
     * @method
     * @name Test#getPlaceCharacteristicUsingGET
     * @param {string} characteristicId - characteristicId
     * @param {string} placeId - placeId
     */
    getPlaceCharacteristicUsingGET(parameters: {
        'characteristicId': string,
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{placeId}/characteristics/{characteristicId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

            if (parameters['characteristicId'] === undefined) {
                reject(new Error('Missing required  parameter: characteristicId'));
                return;
            }

            path = path.replace('{placeId}', `${parameters['placeId']}`);

            if (parameters['placeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    overrideUsingPUTURL(parameters: {
        'characteristicId': string,
        'cmd': OverridePlaceCommonCharacteristicCmd,
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{placeId}/characteristics/{characteristicId}';

        path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

        path = path.replace('{placeId}', `${parameters['placeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * override
     * @method
     * @name Test#overrideUsingPUT
     * @param {string} characteristicId - characteristicId
     * @param {} cmd - cmd
     * @param {string} placeId - placeId
     */
    overrideUsingPUT(parameters: {
        'characteristicId': string,
        'cmd': OverridePlaceCommonCharacteristicCmd,
        'placeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/places/{placeId}/characteristics/{characteristicId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{characteristicId}', `${parameters['characteristicId']}`);

            if (parameters['characteristicId'] === undefined) {
                reject(new Error('Missing required  parameter: characteristicId'));
                return;
            }

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{placeId}', `${parameters['placeId']}`);

            if (parameters['placeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getSitesUsingGETURL(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getSites
     * @method
     * @name Test#getSitesUsingGET
     */
    getSitesUsingGET(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createSiteUsingPOSTURL(parameters: {
        'cmd': CreateSiteCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * createSite
     * @method
     * @name Test#createSiteUsingPOST
     * @param {} cmd - cmd
     */
    createSiteUsingPOST(parameters: {
        'cmd': CreateSiteCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getSiteByCodeUsingGETURL(parameters: {
        'code': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/byCode/{code}';

        path = path.replace('{code}', `${parameters['code']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getSiteByCode
     * @method
     * @name Test#getSiteByCodeUsingGET
     * @param {string} code - code
     */
    getSiteByCodeUsingGET(parameters: {
        'code': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/byCode/{code}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{code}', `${parameters['code']}`);

            if (parameters['code'] === undefined) {
                reject(new Error('Missing required  parameter: code'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    codeExistsUsingPOST_2URL(parameters: {
        'query': SiteCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/codeExists';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * codeExists
     * @method
     * @name Test#codeExistsUsingPOST_2
     * @param {} query - query
     */
    codeExistsUsingPOST_2(parameters: {
        'query': SiteCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/codeExists';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getSettingsForSiteUsingGETURL(parameters: {
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/settings/{siteCode}';

        path = path.replace('{siteCode}', `${parameters['siteCode']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getSettingsForSite
     * @method
     * @name Test#getSettingsForSiteUsingGET
     * @param {string} siteCode - siteCode
     */
    getSettingsForSiteUsingGET(parameters: {
        'siteCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/settings/{siteCode}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteCode}', `${parameters['siteCode']}`);

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateSettingsForSiteUsingPUTURL(parameters: {
        'siteCode': string,
        'siteSettingDtos': Array < SiteSettingDto >
            | SiteSettingDto

            ,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/settings/{siteCode}';

        path = path.replace('{siteCode}', `${parameters['siteCode']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * updateSettingsForSite
     * @method
     * @name Test#updateSettingsForSiteUsingPUT
     * @param {string} siteCode - siteCode
     * @param {} siteSettingDtos - siteSettingDtos
     */
    updateSettingsForSiteUsingPUT(parameters: {
        'siteCode': string,
        'siteSettingDtos': Array < SiteSettingDto >
            | SiteSettingDto

            ,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/settings/{siteCode}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{siteCode}', `${parameters['siteCode']}`);

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters['siteSettingDtos'] !== undefined) {
                body = parameters['siteSettingDtos'];
            }

            if (parameters['siteSettingDtos'] === undefined) {
                reject(new Error('Missing required  parameter: siteSettingDtos'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getSiteUsingGETURL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getSite
     * @method
     * @name Test#getSiteUsingGET
     * @param {string} siteId - siteId
     */
    getSiteUsingGET(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateSiteUsingPUTURL(parameters: {
        'cmd': UpdateSiteCmd,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * updateSite
     * @method
     * @name Test#updateSiteUsingPUT
     * @param {} cmd - cmd
     * @param {string} siteId - siteId
     */
    updateSiteUsingPUT(parameters: {
        'cmd': UpdateSiteCmd,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceTypesBySiteUsingGET_1URL(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * siteId - обязательное поле
     * @method
     * @name Test#getPlaceTypesBySiteUsingGET_1
     * @param {string} siteId - siteId
     */
    getPlaceTypesBySiteUsingGET_1(parameters: {
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createPlaceTypeUsingPOSTURL(parameters: {
        'cmd': CreatePlaceTypeCmd,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * createPlaceType
     * @method
     * @name Test#createPlaceTypeUsingPOST
     * @param {} cmd - cmd
     * @param {string} siteId - siteId
     */
    createPlaceTypeUsingPOST(parameters: {
        'cmd': CreatePlaceTypeCmd,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getAvailablePlaceTypesByParentPlaceIdUsingGETURL(parameters: {
        'parentPlaceId' ? : string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId';
        if (parameters['parentPlaceId'] !== undefined) {
            queryParameters['parentPlaceId'] = parameters['parentPlaceId'];
        }

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getAvailablePlaceTypesByParentPlaceId
     * @method
     * @name Test#getAvailablePlaceTypesByParentPlaceIdUsingGET
     * @param {string} parentPlaceId - parentPlaceId
     * @param {string} siteId - siteId
     */
    getAvailablePlaceTypesByParentPlaceIdUsingGET(parameters: {
        'parentPlaceId' ? : string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['parentPlaceId'] !== undefined) {
                queryParameters['parentPlaceId'] = parameters['parentPlaceId'];
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    codeExistsUsingPOST_1URL(parameters: {
        'query': PlaceTypeCodeExistsQuery,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/codeExists';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * codeExists
     * @method
     * @name Test#codeExistsUsingPOST_1
     * @param {} query - query
     * @param {string} siteId - siteId
     */
    codeExistsUsingPOST_1(parameters: {
        'query': PlaceTypeCodeExistsQuery,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/codeExists';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    placeTypesUploadUsingPOSTURL(parameters: {
        'placesFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/upload';

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * placesFile, siteId - обязательные поля
     * @method
     * @name Test#placeTypesUploadUsingPOST
     * @param {file} placesFile - placesFile
     * @param {string} siteId - siteId
     */
    placeTypesUploadUsingPOST(parameters: {
        'placesFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['placesFile'] !== undefined) {
                form['placesFile'] = parameters['placesFile'];
            }

            if (parameters['placesFile'] === undefined) {
                reject(new Error('Missing required  parameter: placesFile'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getPlaceTypeUsingGETURL(parameters: {
        'placeTypeId': string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getPlaceType
     * @method
     * @name Test#getPlaceTypeUsingGET
     * @param {string} placeTypeId - placeTypeId
     * @param {string} siteId - siteId
     */
    getPlaceTypeUsingGET(parameters: {
        'placeTypeId': string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updatePlaceTypeUsingPUTURL(parameters: {
        'cmd': UpdatePlaceTypeCmd,
        'placeTypeId': string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';

        path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

        path = path.replace('{siteId}', `${parameters['siteId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * updatePlaceType
     * @method
     * @name Test#updatePlaceTypeUsingPUT
     * @param {} cmd - cmd
     * @param {string} placeTypeId - placeTypeId
     * @param {string} siteId - siteId
     */
    updatePlaceTypeUsingPUT(parameters: {
        'cmd': UpdatePlaceTypeCmd,
        'placeTypeId': string,
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{placeTypeId}', `${parameters['placeTypeId']}`);

            if (parameters['placeTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeTypeId'));
                return;
            }

            path = path.replace('{siteId}', `${parameters['siteId']}`);

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneTypesUsingGET_2URL(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneTypes
     * @method
     * @name Test#getZoneTypesUsingGET_2
     */
    getZoneTypesUsingGET_2(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createUsingPOST_3URL(parameters: {
        'cmd': CreateZoneTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * create
     * @method
     * @name Test#createUsingPOST_3
     * @param {} cmd - cmd
     */
    createUsingPOST_3(parameters: {
        'cmd': CreateZoneTypeCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    codeExistsUsingPOST_4URL(parameters: {
        'query': ZoneTypeCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/codeExists';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * codeExists
     * @method
     * @name Test#codeExistsUsingPOST_4
     * @param {} query - query
     */
    codeExistsUsingPOST_4(parameters: {
        'query': ZoneTypeCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/codeExists';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    zoneTypesUploadUsingPOSTURL(parameters: {
        'zoneTypeFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zoneTypeFile - обязательные поля
     * @method
     * @name Test#zoneTypesUploadUsingPOST
     * @param {file} zoneTypeFile - zoneTypeFile
     */
    zoneTypesUploadUsingPOST(parameters: {
        'zoneTypeFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['zoneTypeFile'] !== undefined) {
                form['zoneTypeFile'] = parameters['zoneTypeFile'];
            }

            if (parameters['zoneTypeFile'] === undefined) {
                reject(new Error('Missing required  parameter: zoneTypeFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneTypesWithZonesUsingGET_1URL(parameters: {
        'siteId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/withZones';
        if (parameters['siteId'] !== undefined) {
            queryParameters['siteId'] = parameters['siteId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneTypesWithZones
     * @method
     * @name Test#getZoneTypesWithZonesUsingGET_1
     * @param {string} siteId - siteId
     */
    getZoneTypesWithZonesUsingGET_1(parameters: {
        'siteId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/withZones';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['siteId'] !== undefined) {
                queryParameters['siteId'] = parameters['siteId'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneTypeUsingGETURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneType
     * @method
     * @name Test#getZoneTypeUsingGET
     * @param {string} id - id
     */
    getZoneTypeUsingGET(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateUsingPUT_3URL(parameters: {
        'cmd': UpdateZoneTypeCmd,
        'zoneTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/{zoneTypeId}';

        path = path.replace('{zoneTypeId}', `${parameters['zoneTypeId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * update
     * @method
     * @name Test#updateUsingPUT_3
     * @param {} cmd - cmd
     * @param {string} zoneTypeId - zoneTypeId
     */
    updateUsingPUT_3(parameters: {
        'cmd': UpdateZoneTypeCmd,
        'zoneTypeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zone-types/{zoneTypeId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{zoneTypeId}', `${parameters['zoneTypeId']}`);

            if (parameters['zoneTypeId'] === undefined) {
                reject(new Error('Missing required  parameter: zoneTypeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZonesByTypeUsingGETURL(parameters: {
        'siteCode': string,
        'zoneTypeCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones';
        if (parameters['siteCode'] !== undefined) {
            queryParameters['siteCode'] = parameters['siteCode'];
        }

        if (parameters['zoneTypeCode'] !== undefined) {
            queryParameters['zoneTypeCode'] = parameters['zoneTypeCode'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZonesByType
     * @method
     * @name Test#getZonesByTypeUsingGET
     * @param {string} siteCode - siteCode
     * @param {string} zoneTypeCode - zoneTypeCode
     */
    getZonesByTypeUsingGET(parameters: {
        'siteCode': string,
        'zoneTypeCode': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['siteCode'] !== undefined) {
                queryParameters['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters['zoneTypeCode'] !== undefined) {
                queryParameters['zoneTypeCode'] = parameters['zoneTypeCode'];
            }

            if (parameters['zoneTypeCode'] === undefined) {
                reject(new Error('Missing required  parameter: zoneTypeCode'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createZoneUsingPOSTURL(parameters: {
        'cmd': CreateZoneCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Create a Zone. code, siteId, typeId are required fields.
     * @method
     * @name Test#createZoneUsingPOST
     * @param {} cmd - cmd
     */
    createZoneUsingPOST(parameters: {
        'cmd': CreateZoneCmd,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    codeExistsUsingPOST_3URL(parameters: {
        'query': ZoneCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/codeExists';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * codeExists
     * @method
     * @name Test#codeExistsUsingPOST_3
     * @param {} query - query
     */
    codeExistsUsingPOST_3(parameters: {
        'query': ZoneCodeExistsQuery,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/codeExists';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['query'] !== undefined) {
                body = parameters['query'];
            }

            if (parameters['query'] === undefined) {
                reject(new Error('Missing required  parameter: query'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZonesBySiteIdAndTypeIdUsingGETURL(parameters: {
        'siteId': string,
        'typeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/projections';
        if (parameters['siteId'] !== undefined) {
            queryParameters['siteId'] = parameters['siteId'];
        }

        if (parameters['typeId'] !== undefined) {
            queryParameters['typeId'] = parameters['typeId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZonesBySiteIdAndTypeId
     * @method
     * @name Test#getZonesBySiteIdAndTypeIdUsingGET
     * @param {string} siteId - siteId
     * @param {string} typeId - typeId
     */
    getZonesBySiteIdAndTypeIdUsingGET(parameters: {
        'siteId': string,
        'typeId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/projections';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            if (parameters['siteId'] !== undefined) {
                queryParameters['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['typeId'] !== undefined) {
                queryParameters['typeId'] = parameters['typeId'];
            }

            if (parameters['typeId'] === undefined) {
                reject(new Error('Missing required  parameter: typeId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    zonesUploadUsingPOSTURL(parameters: {
        'siteId': string,
        'zonesFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zonesFile, siteId - обязательные поля
     * @method
     * @name Test#zonesUploadUsingPOST
     * @param {} siteId - siteId
     * @param {file} zonesFile - zonesFile
     */
    zonesUploadUsingPOST(parameters: {
        'siteId': string,
        'zonesFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['zonesFile'] !== undefined) {
                form['zonesFile'] = parameters['zonesFile'];
            }

            if (parameters['zonesFile'] === undefined) {
                reject(new Error('Missing required  parameter: zonesFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    pickingZonesUploadUsingPOSTURL(parameters: {
        'siteId': string,
        'zonesFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/upload/ok';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zonesFile, siteId - обязательные поля
     * @method
     * @name Test#pickingZonesUploadUsingPOST
     * @param {} siteId - siteId
     * @param {file} zonesFile - zonesFile
     */
    pickingZonesUploadUsingPOST(parameters: {
        'siteId': string,
        'zonesFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/upload/ok';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['zonesFile'] !== undefined) {
                form['zonesFile'] = parameters['zonesFile'];
            }

            if (parameters['zonesFile'] === undefined) {
                reject(new Error('Missing required  parameter: zonesFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    storageZonesUploadUsingPOSTURL(parameters: {
        'siteId': string,
        'zonesFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/upload/su';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zonesFile, siteId - обязательные поля
     * @method
     * @name Test#storageZonesUploadUsingPOST
     * @param {} siteId - siteId
     * @param {file} zonesFile - zonesFile
     */
    storageZonesUploadUsingPOST(parameters: {
        'siteId': string,
        'zonesFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/upload/su';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['zonesFile'] !== undefined) {
                form['zonesFile'] = parameters['zonesFile'];
            }

            if (parameters['zonesFile'] === undefined) {
                reject(new Error('Missing required  parameter: zonesFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getZoneByIdUsingGETURL(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getZoneById
     * @method
     * @name Test#getZoneByIdUsingGET
     * @param {string} id - id
     */
    getZoneByIdUsingGET(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    updateZoneUsingPUTURL(parameters: {
        'cmd': UpdateZoneCmd,
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/{id}';

        path = path.replace('{id}', `${parameters['id']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
     * @method
     * @name Test#updateZoneUsingPUT
     * @param {} cmd - cmd
     * @param {string} id - id
     */
    updateZoneUsingPUT(parameters: {
        'cmd': UpdateZoneCmd,
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/topology/zones/{id}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            path = path.replace('{id}', `${parameters['id']}`);

            if (parameters['id'] === undefined) {
                reject(new Error('Missing required  parameter: id'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createTransferPlanAllocationUsingPOSTURL(parameters: {
        'cmd': CreatePlanRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/allocation/createTransferPlan';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * создать план перемещения между зонами
     * @method
     * @name Test#createTransferPlanAllocationUsingPOST
     * @param {} cmd - cmd
     */
    createTransferPlanAllocationUsingPOST(parameters: {
        'cmd': CreatePlanRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/allocation/createTransferPlan';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getRouteUsingPOSTURL(parameters: {
        'carrierId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/getRoute/{carrierId}';

        path = path.replace('{carrierId}', `${parameters['carrierId']}`);

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * getRoute
     * @method
     * @name Test#getRouteUsingPOST
     * @param {string} carrierId - carrierId
     */
    getRouteUsingPOST(parameters: {
        'carrierId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/getRoute/{carrierId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{carrierId}', `${parameters['carrierId']}`);

            if (parameters['carrierId'] === undefined) {
                reject(new Error('Missing required  parameter: carrierId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    createTransferPlanPickingUsingPOSTURL(parameters: {
        'cmd': CreatePlanRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/picking/createPlan';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * создать план перемещения между зонами
     * @method
     * @name Test#createTransferPlanPickingUsingPOST
     * @param {} cmd - cmd
     */
    createTransferPlanPickingUsingPOST(parameters: {
        'cmd': CreatePlanRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/picking/createPlan';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            if (parameters['cmd'] !== undefined) {
                body = parameters['cmd'];
            }

            if (parameters['cmd'] === undefined) {
                reject(new Error('Missing required  parameter: cmd'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    moveCarrierToPlaceUsingPUTURL(parameters: {
        'carrierId': string,
        'placeId': string,
        'processId' ? : string,
        'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION",
        'transferId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/{carrierId}/moveTo/{placeId}';

        path = path.replace('{carrierId}', `${parameters['carrierId']}`);

        path = path.replace('{placeId}', `${parameters['placeId']}`);
        if (parameters['processId'] !== undefined) {
            queryParameters['processId'] = parameters['processId'];
        }

        if (parameters['processType'] !== undefined) {
            queryParameters['processType'] = parameters['processType'];
        }

        if (parameters['transferId'] !== undefined) {
            queryParameters['transferId'] = parameters['transferId'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * moveCarrierToPlace
     * @method
     * @name Test#moveCarrierToPlaceUsingPUT
     * @param {string} carrierId - carrierId
     * @param {string} placeId - placeId
     * @param {string} processId - processId
     * @param {string} processType - processType
     * @param {string} transferId - transferId
     */
    moveCarrierToPlaceUsingPUT(parameters: {
        'carrierId': string,
        'placeId': string,
        'processId' ? : string,
        'processType' ? : "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION",
        'transferId' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/transfer/{carrierId}/moveTo/{placeId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{carrierId}', `${parameters['carrierId']}`);

            if (parameters['carrierId'] === undefined) {
                reject(new Error('Missing required  parameter: carrierId'));
                return;
            }

            path = path.replace('{placeId}', `${parameters['placeId']}`);

            if (parameters['placeId'] === undefined) {
                reject(new Error('Missing required  parameter: placeId'));
                return;
            }

            if (parameters['processId'] !== undefined) {
                queryParameters['processId'] = parameters['processId'];
            }

            if (parameters['processType'] !== undefined) {
                queryParameters['processType'] = parameters['processType'];
            }

            if (parameters['transferId'] !== undefined) {
                queryParameters['transferId'] = parameters['transferId'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('PUT', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    uploadWriteOffSettingsUsingPOSTURL(parameters: {
        'settingsFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/write-off-settings/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * settingsFile, siteId - обязательные поля
     * @method
     * @name Test#uploadWriteOffSettingsUsingPOST
     * @param {file} settingsFile - settingsFile
     * @param {} siteId - siteId
     */
    uploadWriteOffSettingsUsingPOST(parameters: {
        'settingsFile': {},
        'siteId': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/write-off-settings/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['settingsFile'] !== undefined) {
                form['settingsFile'] = parameters['settingsFile'];
            }

            if (parameters['settingsFile'] === undefined) {
                reject(new Error('Missing required  parameter: settingsFile'));
                return;
            }

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    uploadZoneCharacteristicsUsingPOSTURL(parameters: {
        'siteCode': string,
        'zoneCharacteristics': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/zone-characteristics/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zoneCharacteristics, siteCode - обязательные поля
     * @method
     * @name Test#uploadZoneCharacteristicsUsingPOST
     * @param {} siteCode - siteCode
     * @param {file} zoneCharacteristics - zoneCharacteristics
     */
    uploadZoneCharacteristicsUsingPOST(parameters: {
        'siteCode': string,
        'zoneCharacteristics': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/zone-characteristics/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteCode'] !== undefined) {
                form['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters['zoneCharacteristics'] !== undefined) {
                form['zoneCharacteristics'] = parameters['zoneCharacteristics'];
            }

            if (parameters['zoneCharacteristics'] === undefined) {
                reject(new Error('Missing required  parameter: zoneCharacteristics'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    bindToRouteStrategyUsingPOSTURL(parameters: {
        'siteCode': string,
        'zoneRouteStrategy': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/zone-route-strategy/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zoneRouteStrategy, siteId - обязательные поля
     * @method
     * @name Test#bindToRouteStrategyUsingPOST
     * @param {} siteCode - siteCode
     * @param {file} zoneRouteStrategy - zoneRouteStrategy
     */
    bindToRouteStrategyUsingPOST(parameters: {
        'siteCode': string,
        'zoneRouteStrategy': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/zone-route-strategy/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteCode'] !== undefined) {
                form['siteCode'] = parameters['siteCode'];
            }

            if (parameters['siteCode'] === undefined) {
                reject(new Error('Missing required  parameter: siteCode'));
                return;
            }

            if (parameters['zoneRouteStrategy'] !== undefined) {
                form['zoneRouteStrategy'] = parameters['zoneRouteStrategy'];
            }

            if (parameters['zoneRouteStrategy'] === undefined) {
                reject(new Error('Missing required  parameter: zoneRouteStrategy'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    zoneTransferRulesUploadUsingPOSTURL(parameters: {
        'siteId': string,
        'zoneTransferRuleFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/zone-transfer-rules/upload';

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        queryParameters = {};

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * zoneTransferRuleFile, siteId - обязательные поля
     * @method
     * @name Test#zoneTransferRulesUploadUsingPOST
     * @param {} siteId - siteId
     * @param {file} zoneTransferRuleFile - zoneTransferRuleFile
     */
    zoneTransferRulesUploadUsingPOST(parameters: {
        'siteId': string,
        'zoneTransferRuleFile': {},
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/v1/zone-transfer-rules/upload';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = '*/*';
            headers['Content-Type'] = 'multipart/form-data';

            if (parameters['siteId'] !== undefined) {
                form['siteId'] = parameters['siteId'];
            }

            if (parameters['siteId'] === undefined) {
                reject(new Error('Missing required  parameter: siteId'));
                return;
            }

            if (parameters['zoneTransferRuleFile'] !== undefined) {
                form['zoneTransferRuleFile'] = parameters['zoneTransferRuleFile'];
            }

            if (parameters['zoneTransferRuleFile'] === undefined) {
                reject(new Error('Missing required  parameter: zoneTransferRuleFile'));
                return;
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            form = queryParameters;
            queryParameters = {};

            this.request('POST', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

}