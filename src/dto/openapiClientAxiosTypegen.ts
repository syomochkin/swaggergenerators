import {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios'; 

declare namespace Definitions {
  /**
   * AddPlaceTypeCommonCharacteristicDto
   */
  export interface AddPlaceTypeCommonCharacteristicDto {
    characteristic?: string;
    defaultValue?: DefaultValue;
    settings?: CommonCharacteristicSettings;
  }
  /**
   * AddStockToCarrierCmd
   */
  export interface AddStockToCarrierCmd {
    baseAmount?: number;
    baseUnit?: string;
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    productBatchId?: string; // uuid
    productId?: string;
    quantity?: number;
    stockType?: string;
    tabNumber?: string;
    unit?: string;
    userName?: string;
  }
  /**
   * AllocationSettingsDto
   */
  export interface AllocationSettingsDto {
    maxMixedProducts?: number; // int32
    placeTypeCode?: string;
    zoneCode?: string;
  }
  /**
   * AllocationZoneTransferReq
   */
  export interface AllocationZoneTransferReq {
    carrierId?: string; // uuid
    destinationPlaceId?: string; // uuid
    operationId?: string;
    startPlaceId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * AssignProductBatchCmd
   */
  export interface AssignProductBatchCmd {
    expirationTime?: string; // date-time
    inboundId?: string; // uuid
    inboundTime?: string; // date-time
    manufactureTime?: string; // date-time
    mercuryExtId?: string;
    number?: string;
    productId?: string;
    productName?: string;
    siteCode?: string;
    type?: "REGULAR" | "RETURN";
    unit?: string;
    vendorCode?: string;
    vendorName?: string;
    weightProduct?: boolean;
  }
  /**
   * BoxRoute
   */
  export interface BoxRoute {
    operationId?: string;
    plannedTotalDuration?: number; // int32
    rejectedItems?: ProductAmountDto[];
    steps?: RouteStep[];
    transferId?: string; // uuid
  }
  /**
   * BuildBoxRouteCmd
   */
  export interface BuildBoxRouteCmd {
    items?: ProductAmountDto[];
    operationId?: string;
    siteCode?: string;
    transferId?: string; // uuid
  }
  /**
   * BulkOperationDto
   */
  export interface BulkOperationDto {
    operationId?: string;
    status?: string;
  }
  /**
   * BulkPlaceUpdateStatusCmd
   */
  export interface BulkPlaceUpdateStatusCmd {
    operationId?: string;
    placeIds?: string /* uuid */ [];
    statusCode?: string;
  }
  /**
   * BulkPlaceUpdateTypeCmd
   */
  export interface BulkPlaceUpdateTypeCmd {
    operationId?: string;
    placeIds?: string /* uuid */ [];
    targetPlaceTypeId?: string; // uuid
  }
  /**
   * BulkPlaceUpdateZoneCmd
   */
  export interface BulkPlaceUpdateZoneCmd {
    operationId?: string;
    placeIds?: string /* uuid */ [];
    zoneId?: string; // uuid
  }
  /**
   * CancelStockReservationCmd
   */
  export interface CancelStockReservationCmd {
    incomingQuantity?: number; // int32
    operationId?: string;
    outgoingQuantity?: number; // int32
    siteCode?: string;
    stockId?: string; // uuid
    transferId?: string; // uuid
    unit?: string;
  }
  /**
   * CarrierDto
   */
  export interface CarrierDto {
    author?: string;
    barcode?: string;
    carriersTypeId?: string; // uuid
    createdTime?: number; // int32
    editor?: string;
    id?: string; // uuid
    nestedCarriers?: CarrierDto[];
    parentId?: string; // uuid
    parentNumber?: string;
    placeAddress?: string;
    placeId?: string; // uuid
    processId?: string;
    type?: CarrierTypeDto;
    updatedTime?: number; // int32
  }
  /**
   * CarrierGridView
   */
  export interface CarrierGridView {
    barcode?: string;
    id?: string; // uuid
    nestedCarriers?: CarrierGridView[];
    status?: "incoming" | "outgoing" | "actual";
    type?: CarrierTypeGridView;
  }
  /**
   * CarrierShippedResponse
   */
  export interface CarrierShippedResponse {
    carrierId?: string; // uuid
    carrierMoveResponses?: MoveCarrierToPlaceResponse[];
    operationId?: string;
    placeId?: string; // uuid
  }
  /**
   * CarrierTypeCodeExistsQuery
   */
  export interface CarrierTypeCodeExistsQuery {
    code?: string;
  }
  /**
   * CarrierTypeDto
   */
  export interface CarrierTypeDto {
    archived?: boolean;
    author?: string;
    code?: string;
    createdTime?: number; // int32
    description?: string;
    editor?: string;
    height?: number; // int32
    id?: string; // uuid
    length?: number; // int32
    maxVolume?: number;
    maxWeight?: number;
    name?: string;
    updatedTime?: number; // int32
    width?: number; // int32
  }
  /**
   * CarrierTypeGridView
   */
  export interface CarrierTypeGridView {
    code?: string;
    description?: string;
    id?: string; // uuid
    name?: string;
  }
  /**
   * ChangeRawStock
   */
  export interface ChangeRawStock {
    position?: number; // int64
    stockId?: string; // uuid
  }
  /**
   * ChangeRawStockTypeCmd
   */
  export interface ChangeRawStockTypeCmd {
    operationId?: string;
    orderVersion?: string;
    outboundDeliveryNumber?: string;
    outboundTime?: string; // date-time
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    siteCode?: string;
    stocks?: ChangeRawStock[];
    tabNumber?: string;
    userName?: string;
  }
  /**
   * CharacteristicDto
   */
  export interface CharacteristicDto {
    type?: string;
    value?: ValueDto;
  }
  /**
   * CodeExists
   */
  export interface CodeExists {
    exists?: boolean;
  }
  /**
   * CommonCharacteristicSettings
   */
  export interface CommonCharacteristicSettings {
    editable?: boolean;
    mandatory?: boolean;
    parentInheritance?: "check" | "ignore";
    unit?: string;
  }
  /**
   * ConfirmProductBatchCmd
   */
  export interface ConfirmProductBatchCmd {
    batchId?: string; // uuid
    targetStockType?: string;
  }
  /**
   * Coordinates3D
   */
  export interface Coordinates3D {
    x?: number;
    y?: number;
    z?: number;
  }
  /**
   * CoordinatesDto
   */
  export interface CoordinatesDto {
    x?: number;
    y?: number;
    z?: number;
  }
  /**
   * CreateCarrierCmd
   */
  export interface CreateCarrierCmd {
    carrierBarcode?: string;
    carrierId?: string; // uuid
    carrierTypeCode?: string;
    placeBarcode?: string;
    processId?: string;
    siteCode?: string;
  }
  /**
   * CreateCarrierTypeCmd
   */
  export interface CreateCarrierTypeCmd {
    code?: string;
    description?: string;
    height?: number; // int32
    id?: string; // uuid
    length?: number; // int32
    maxVolume?: number;
    maxWeight?: number;
    name?: string;
    width?: number; // int32
  }
  /**
   * CreatePlaceCmd
   */
  export interface CreatePlaceCmd {
    address?: string;
    addressTypeId?: string; // uuid
    coordinates?: Coordinates3D;
    description?: string;
    id?: string; // uuid
    number?: number; // int32
    parentId?: string; // uuid
    siteId?: string; // uuid
    status?: PlaceStatusDto;
    statusReason?: string;
    typeId?: string; // uuid
    zoneIds?: string /* uuid */ [];
  }
  /**
   * CreatePlaceTypeCmd
   */
  export interface CreatePlaceTypeCmd {
    code?: string;
    coordinatesRequired?: boolean;
    description?: string;
    maxMixBatches?: number; // int32
    name?: string;
    numerationRule?: "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP";
    placeTypeId?: string; // uuid
    storagePlace?: boolean;
  }
  /**
   * CreatePlanRequest
   */
  export interface CreatePlanRequest {
    carrierId?: string; // uuid
    startPlaceId?: string; // uuid
    targetZoneId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * CreateSiteCmd
   */
  export interface CreateSiteCmd {
    code?: string;
    description?: string;
    id?: string; // uuid
    name?: string;
  }
  /**
   * CreateZoneCmd
   */
  export interface CreateZoneCmd {
    boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
    code?: string;
    description?: string;
    id?: string; // uuid
    name?: string;
    siteId?: string; // uuid
    typeId?: string; // uuid
  }
  /**
   * CreateZoneTypeCmd
   */
  export interface CreateZoneTypeCmd {
    code?: string;
    description?: string;
    id?: string; // uuid
    name?: string;
  }
  /**
   * Data
   */
  export interface Data {
    address?: string;
    author?: string;
    childrenAmount?: number; // int32
    createdTime?: number; // int32
    editor?: string;
    id?: string; // uuid
    status?: PlaceStatusDto;
    typeName?: string;
    updatedTime?: number; // int32
    zoneNames?: string[];
  }
  /**
   * DefaultValue
   */
  export interface DefaultValue {
    decValue?: number;
    intValue?: number; // int32
    maxIntValue?: number; // int32
    minIntValue?: number; // int32
  }
  /**
   * DetailedCarriersSearchQuery
   */
  export interface DetailedCarriersSearchQuery {
    barcodes?: string[];
  }
  /**
   * DetailedStockSearchQuery
   */
  export interface DetailedStockSearchQuery {
    carrierId?: string; // uuid
    carrierNumber?: string;
    inCarrier?: "IN_ONLY" | "OUT_ONLY" | "ALL";
    maxResponseSize?: number; // int32
    placeAddress?: string;
    placeId?: string; // uuid
    productBatchId?: string; // uuid
    productBatchNumber?: string;
    productId?: string;
    siteCode?: string;
    stockId?: string; // uuid
    stockTypeCode?: string;
  }
  /**
   * DetailedStocksSearchResult
   */
  export interface DetailedStocksSearchResult {
    stocks?: StockDto[];
    totalCount?: number; // int64
  }
  /**
   * DropAllProcessReservationsCmd
   */
  export interface DropAllProcessReservationsCmd {
    operationId?: string;
    processId?: string;
    siteCode?: string;
  }
  /**
   * DropCarrierReservationForProcessCmd
   */
  export interface DropCarrierReservationForProcessCmd {
    carrierNumber?: string;
    dropForNestedCarriers?: boolean;
    operationId?: string;
    processId?: string;
    siteCode?: string;
  }
  /**
   * DropPlaceReservationForProcessCmd
   */
  export interface DropPlaceReservationForProcessCmd {
    operationId?: string;
    placeBarcode?: string;
    processId?: string;
    siteCode?: string;
  }
  /**
   * DropPlaceReservationsForTransferCmd
   */
  export interface DropPlaceReservationsForTransferCmd {
    operationId?: string;
    processId?: string;
    siteCode?: string;
    transferId?: string; // uuid
  }
  /**
   * DropTransferCmd
   */
  export interface DropTransferCmd {
    transferId?: string; // uuid
  }
  /**
   * Error
   */
  export interface Error {
    code?: string;
    message: string;
  }
  /**
   * FullAmount
   */
  export interface FullAmount {
    quantity?: number;
    unit?: string;
    volume?: number;
    weight?: number; // int64
  }
  /**
   * ModifyStockTypeCmd
   */
  export interface ModifyStockTypeCmd {
    code?: string;
    name?: string;
  }
  /**
   * MonoPalletPlacementRequest
   */
  export interface MonoPalletPlacementRequest {
    incomingCarrierId?: string; // uuid
    openCarriers?: string /* uuid */ [];
    operationId?: string;
    productBatchId?: string; // uuid
    productId?: string;
    quantity?: number;
    siteCode?: string;
    stockTypeCode?: string;
  }
  /**
   * MoveAllStocksToCarrierCmd
   */
  export interface MoveAllStocksToCarrierCmd {
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    siteCode?: string;
    sourcePlaceId?: string; // uuid
    tabNumber?: string;
    targetCarrierId?: string; // uuid
    transferId?: string; // uuid
    userName?: string;
  }
  /**
   * MoveAllStocksToCarrierResponse
   */
  export interface MoveAllStocksToCarrierResponse {
    operationId?: string;
    stockDto?: StockDto[];
    targetCarrierId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * MoveCarrierToPlaceResponse
   */
  export interface MoveCarrierToPlaceResponse {
    carrierId?: string; // uuid
    operationId?: string;
    rejected?: boolean;
    transferComplete?: boolean;
  }
  /**
   * MoveStockCmd
   */
  export interface MoveStockCmd {
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    quantity?: number; // int32
    stockId?: string; // uuid
    tabNumber?: string;
    targetCarrierId?: string; // uuid
    targetPlaceId?: string; // uuid
    userName?: string;
  }
  /**
   * MoveStockCmdResponse
   */
  export interface MoveStockCmdResponse {
    movedQuantity?: number; // int32
    operationId?: string;
    originalStockId?: string; // uuid
    targetStockActualQuantity?: number;
    targetStockId?: string; // uuid
  }
  /**
   * MoveStockToCarrierCmd
   */
  export interface MoveStockToCarrierCmd {
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    quantity?: number; // int32
    sourcePlaceId?: string; // uuid
    sourceStockId?: string; // uuid
    tabNumber?: string;
    targetCarrierId?: string; // uuid
    transferId?: string; // uuid
    unit?: string;
    userName?: string;
  }
  /**
   * NamedId
   */
  export interface NamedId {
    label?: string;
    value?: string; // uuid
  }
  /**
   * OrderBoxDto
   */
  export interface OrderBoxDto {
    carrierId?: string; // uuid
    orderItems?: OutboundOrderItemDto[];
  }
  /**
   * OrderByCriteria
   */
  export interface OrderByCriteria {
    field?: string;
    order?: "ASC" | "DESC";
  }
  /**
   * OutboundOrderCmd
   */
  export interface OutboundOrderCmd {
    operationId?: string;
    orderBoxes?: OrderBoxDto[];
    orderVersion?: string;
    outboundDeliveryNumber?: string;
    outboundTime?: string; // date-time
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    siteCode?: string;
    tabNumber?: string;
    unshippedOrderItems?: UnshippedOrderItemDto[];
    userName?: string;
  }
  /**
   * OutboundOrderItemDto
   */
  export interface OutboundOrderItemDto {
    baseAmount?: number;
    baseUnit?: string;
    position?: number; // int64
    productBatchId?: string; // uuid
    productId?: string;
    quantity?: number; // int32
    unit?: string;
  }
  /**
   * OverridePlaceCommonCharacteristicCmd
   */
  export interface OverridePlaceCommonCharacteristicCmd {
    characteristic?: string;
    siteId?: string; // uuid
    value?: ValueCmd;
  }
  /**
   * PageRequestInfo
   */
  export interface PageRequestInfo {
    limit?: number; // int32
    offset?: number; // int32
  }
  /**
   * PickerRouteReq
   */
  export interface PickerRouteReq {
    pickingZoneId?: string; // uuid
    siteCode?: string;
    startPlaceId?: string; // uuid
    transferIds?: string /* uuid */ [];
  }
  /**
   * PickingZoneTransferReq
   */
  export interface PickingZoneTransferReq {
    carrierId?: string; // uuid
    destinationPlaceId?: string; // uuid
    operationId?: string;
    startPlaceId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * PlaceCharacteristicDto
   */
  export interface PlaceCharacteristicDto {
    author?: string;
    characteristicId?: string; // uuid
    checkParents?: string;
    createdTime?: number; // int32
    editable?: boolean;
    editor?: string;
    mandatory?: boolean;
    placeId?: string; // uuid
    siteId?: string; // uuid
    type?: string;
    typeCharacteristicId?: string; // uuid
    updatedTime?: number; // int32
    value?: ValueDto;
  }
  /**
   * PlaceDto
   */
  export interface PlaceDto {
    address?: string;
    addressTypeId?: string; // uuid
    author?: string;
    childPlaces?: PlaceDto[];
    coordinates?: CoordinatesDto;
    createdTime?: number; // int32
    description?: string;
    editor?: string;
    id?: string; // uuid
    number?: number; // int32
    parentId?: string; // uuid
    parentPlaces?: PlaceDto[];
    siteId?: string; // uuid
    status?: PlaceStatusDto;
    statusReason?: string;
    storagePlace?: boolean;
    typeCode?: string;
    typeId?: string; // uuid
    typeName?: string;
    updatedTime?: number; // int32
    zones?: ZoneInfo[];
  }
  /**
   * PlaceHierarchyFilter
   */
  export interface PlaceHierarchyFilter {
    hierarchyElements?: PlaceHierarchyFilterCondition[];
    siteId?: string; // uuid
  }
  /**
   * PlaceHierarchyFilterCondition
   */
  export interface PlaceHierarchyFilterCondition {
    criteria?: string;
    placeTypeId?: string; // uuid
  }
  /**
   * PlaceHierarchyTreeDto
   */
  export interface PlaceHierarchyTreeDto {
    address?: string;
    characteristics?: CharacteristicDto[];
    children?: PlaceHierarchyTreeDto[];
    id?: string; // uuid
    number?: number; // int32
    status?: string;
    type?: Type;
    zones?: Zone[];
  }
  /**
   * PlaceInfo
   */
  export interface PlaceInfo {
    address?: string;
    author?: string;
    childrenAmount?: number; // int32
    createdTime?: number; // int32
    editor?: string;
    id?: string; // uuid
    placeZones?: PlaceZoneInfo[];
    siteId?: string; // uuid
    siteName?: string;
    typeName?: string;
    updatedTime?: number; // int32
    zoneNames?: string[];
  }
  /**
   * PlaceNodeWithPosition
   */
  export interface PlaceNodeWithPosition {
    childPlaces?: Data[];
    placeNodeChainFromTop?: NamedId[];
  }
  /**
   * PlaceSearchFilter
   */
  export interface PlaceSearchFilter {
    hierarchy?: PlaceHierarchyFilter;
    placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
    placeTypes?: string /* uuid */ [];
    zonesType?: PlaceZonesFilter[];
  }
  /**
   * PlaceShortView
   */
  export interface PlaceShortView {
    barcode?: string;
    description?: string;
    id?: string; // uuid
    siteCode?: string;
    typeCode?: string;
    typeName?: string;
  }
  /**
   * PlaceStatusDto
   */
  export interface PlaceStatusDto {
    code?: string;
    default?: boolean;
    title?: string;
  }
  /**
   * PlaceTypeCharacteristicDTO
   */
  export interface PlaceTypeCharacteristicDTO {
    author?: string;
    characteristicId?: string; // uuid
    checkParents?: string;
    createdTime?: number; // int32
    defaultValue?: DefaultValue;
    editable?: boolean;
    editor?: string;
    mandatory?: boolean;
    placeTypeId?: string; // uuid
    type?: string;
    updatedTime?: number; // int32
  }
  /**
   * PlaceTypeCodeExistsQuery
   */
  export interface PlaceTypeCodeExistsQuery {
    code?: string;
  }
  /**
   * PlaceTypeDto
   */
  export interface PlaceTypeDto {
    author?: string;
    code?: string;
    coordinatesRequired?: boolean;
    createdTime?: number; // int32
    description?: string;
    editor?: string;
    maxMixBatches?: number; // int32
    name?: string;
    numberOfImplementation?: number; // int64
    numerationRule?: string;
    placeTypeId?: string; // uuid
    siteId?: string; // uuid
    storagePlace?: boolean;
    updatedTime?: number; // int32
  }
  /**
   * PlaceWithPosition
   */
  export interface PlaceWithPosition {
    place?: PlaceDto;
    placeNodeChainFromTop?: NamedId[];
  }
  /**
   * PlaceWithType
   */
  export interface PlaceWithType {
    address?: string;
    id?: string; // uuid
    number?: number; // int32
    type?: Type;
  }
  /**
   * PlaceZoneInfo
   */
  export interface PlaceZoneInfo {
    zoneCode?: string;
    zoneName?: string;
    zoneTypeCode?: string;
    zoneTypeName?: string;
  }
  /**
   * PlaceZonesFilter
   */
  export interface PlaceZonesFilter {
    zoneTypeId?: string; // uuid
    zonesId?: string /* uuid */ [];
  }
  /**
   * PlacesSearchQuery
   */
  export interface PlacesSearchQuery {
    filter?: PlaceSearchFilter;
    orderInfo?: OrderByCriteria[];
    pageInfo?: PageRequestInfo;
  }
  /**
   * PlacesSearchResult
   */
  export interface PlacesSearchResult {
    places?: PlaceInfo[];
    totalCount?: number; // int32
  }
  /**
   * ProductAmountDto
   */
  export interface ProductAmountDto {
    productId?: string;
    quantity?: number;
    rejectionReason?: "NO_STOCK" | "NO_ROUTE";
    unit?: string;
  }
  /**
   * ProductBatchBalanceByStockType
   */
  export interface ProductBatchBalanceByStockType {
    baseAmountERP?: number;
    baseAmountWMS?: number;
    quantityERP?: number; // int32
    quantityWMS?: number;
    stockTypeCode?: string;
  }
  /**
   * ProductBatchBalanceDifferenceDto
   */
  export interface ProductBatchBalanceDifferenceDto {
    canCorrect?: boolean;
    needCorrect?: boolean;
    stockTypes?: ProductBatchBalanceByStockType[];
  }
  /**
   * ProductBatchDto
   */
  export interface ProductBatchDto {
    expirationTime?: number; // int32
    id?: string; // uuid
    inboundId?: string; // uuid
    inboundTime?: number; // int32
    manufactureTime?: number; // int32
    mercuryExtId?: string;
    number?: string;
    productId?: string;
    productName?: string;
    siteId?: string; // uuid
    type?: "REGULAR" | "RETURN";
    vendorCode?: string;
    vendorName?: string;
    weightProduct?: boolean;
  }
  /**
   * ProductBatchSearchDto
   */
  export interface ProductBatchSearchDto {
    confirmed?: boolean;
    expirationTime?: number; // int32
    id?: string; // uuid
    inboundTime?: number; // int32
    manufactureTime?: number; // int32
    productBatchNumber?: string;
    productId?: string;
    productName?: string;
    siteCode?: string;
    siteId?: string; // uuid
    type?: "REGULAR" | "RETURN";
    vendorCode?: string;
    vendorName?: string;
  }
  /**
   * ProductBatchSearchFilter
   */
  export interface ProductBatchSearchFilter {
    confirmed?: boolean;
    expirationDateFrom?: string; // date-time
    expirationDateTo?: string; // date-time
    manufactureTimeFrom?: string; // date-time
    manufactureTimeTo?: string; // date-time
    number?: string;
    productId?: string;
    siteCode?: string;
    siteId?: string; // uuid
    types?: ("REGULAR" | "RETURN")[];
  }
  /**
   * ProductBatchesSearchQuery
   */
  export interface ProductBatchesSearchQuery {
    filter?: ProductBatchSearchFilter;
    orderInfo?: OrderByCriteria[];
    pageInfo?: PageRequestInfo;
  }
  /**
   * ProductBatchesSearchResult
   */
  export interface ProductBatchesSearchResult {
    productBatches?: ProductBatchSearchDto[];
    totalCount?: number; // int64
  }
  /**
   * ProductPlacementRequest
   */
  export interface ProductPlacementRequest {
    openCarriers?: string /* uuid */ [];
    operationId?: string;
    productBatchId?: string; // uuid
    productId?: string;
    quantity?: number;
    siteCode?: string;
    stockTypeCode?: string;
  }
  /**
   * ProductPlacementResponse
   */
  export interface ProductPlacementResponse {
    clientError?: Error;
    operationId?: string;
    productId?: string;
    rejectedInfo?: RejectedInfo;
    reservedCarriers?: ReservedCarrier[];
  }
  /**
   * RawPlacementRequest
   */
  export interface RawPlacementRequest {
    incomingCarrierId?: string; // uuid
    openCarriers?: string /* uuid */ [];
    operationId?: string;
    productBatchId?: string; // uuid
    productId?: string;
    quantity?: number;
    siteCode?: string;
    stockTypeCode?: string;
  }
  /**
   * RawStock
   */
  export interface RawStock {
    position?: number; // int64
    quantity?: number;
    stockId?: string; // uuid
  }
  /**
   * ReduceIncomingQuantityCmd
   */
  export interface ReduceIncomingQuantityCmd {
    operationId?: string;
    productBatchId?: string; // uuid
    quantity?: number; // int32
    stockTypeCode?: string;
    transferId?: string; // uuid
  }
  /**
   * ReducePlaceReservationForTransferCmd
   */
  export interface ReducePlaceReservationForTransferCmd {
    carrierTypeCode?: string;
    operationId?: string;
    placeId?: string; // uuid
    quantity?: number; // int32
    siteCode?: string;
    transferId?: string; // uuid
  }
  /**
   * RejectedInfo
   */
  export interface RejectedInfo {
    clientError?: Error;
    quantity?: number;
  }
  /**
   * ReplenishCmd
   */
  export interface ReplenishCmd {
    destPlaceBarcode?: string;
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    productBatchId?: string; // uuid
    productId?: string;
    quantity?: number; // int32
    siteCode?: string;
    srcCarrierId?: string; // uuid
    stockTypeCode?: string;
    tabNumber?: string;
    targetStockTypeCode?: string;
    userName?: string;
  }
  /**
   * ReservationCancelledResponse
   */
  export interface ReservationCancelledResponse {
    incomingQuantity?: number;
    operationId?: string;
    outgoingQuantity?: number;
    siteCode?: string;
    stockId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * ReservationDto
   */
  export interface ReservationDto {
    batchId?: string; // uuid
    placeAddress?: string;
    placeId?: string; // uuid
    productId?: string;
    quantityIncoming?: number;
    quantityOutgoing?: number;
    stockId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * ReservationInfoDto
   */
  export interface ReservationInfoDto {
    batchId?: string; // uuid
    incomingQuantity?: number;
    outgoingQuantity?: number;
    productId?: string;
    stockId?: string; // uuid
    stockTypeCode?: string;
    unit?: string;
  }
  /**
   * ReserveCarrierForProcessCmd
   */
  export interface ReserveCarrierForProcessCmd {
    carrierNumber?: string;
    operationId?: string;
    processId?: string;
    siteCode?: string;
  }
  /**
   * ReserveCarrierForProcessResponse
   */
  export interface ReserveCarrierForProcessResponse {
    clientError?: Error;
    operationId?: string;
  }
  /**
   * ReserveInfo
   */
  export interface ReserveInfo {
    placeAddress?: string;
    placeId?: string; // uuid
    transfer?: TransferDto;
  }
  /**
   * ReservePlaceForProcessCmd
   */
  export interface ReservePlaceForProcessCmd {
    operationId?: string;
    placeBarcode?: string;
    processId?: string;
    siteCode?: string;
  }
  /**
   * ReservePlacesForCarriersCmd
   */
  export interface ReservePlacesForCarriersCmd {
    carrierPlaceTypeCode?: string;
    carriers?: TransferDto[];
    operationId?: string;
    processId?: string;
    processName?: string;
    processPlaceTypeCode?: string;
    reservationMode?: "FULL" | "PARTIAL";
    siteCode?: string;
    zones?: ZoneReference[];
  }
  /**
   * ReserveStockOutgoingQuantityResponse
   */
  export interface ReserveStockOutgoingQuantityResponse {
    operationId?: string;
    reservation?: ReservationDto;
  }
  /**
   * ReservedCarrier
   */
  export interface ReservedCarrier {
    openNewCarrier?: boolean;
    openedCarrierId?: string; // uuid
    quantity?: number;
  }
  /**
   * ReservedPlacesForCarriersResponse
   */
  export interface ReservedPlacesForCarriersResponse {
    clientError?: Error;
    operationId?: string;
    processId?: string;
    processName?: string;
    rejectedTransfers?: TransferDto[];
    reserves?: ReserveInfo[];
    siteCode?: string;
  }
  /**
   * Route
   */
  export interface Route {
    steps?: RouteStep[];
  }
  /**
   * RouteStep
   */
  export interface RouteStep {
    allocationSector?: ZoneWithType;
    allocationZone?: ZoneWithType;
    characteristics?: CharacteristicDto[];
    floor?: PlaceWithType;
    hierarchy?: PlaceWithType[];
    item?: StockAmountDto;
    pickingSector?: ZoneWithType;
    pickingZone?: ZoneWithType;
    place?: PlaceWithType;
    plannedDuration?: number; // int32
    transferId?: string; // uuid
    zones?: ZoneWithType[];
  }
  /**
   * ShipCarrierCmd
   */
  export interface ShipCarrierCmd {
    carrierId?: string; // uuid
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    siteCode?: string;
    tabNumber?: string;
    userName?: string;
  }
  /**
   * SiteCodeExistsQuery
   */
  export interface SiteCodeExistsQuery {
    code?: string;
  }
  /**
   * SiteDto
   */
  export interface SiteDto {
    archived?: boolean;
    author?: string;
    code?: string;
    createdTime?: number; // int32
    description?: string;
    editor?: string;
    id?: string; // uuid
    name?: string;
    updatedTime?: number; // int32
  }
  /**
   * SiteSettingDto
   */
  export interface SiteSettingDto {
    bigDecimalValue?: number;
    code?: "MAX_MIX_PRODUCTS";
    intValue?: number; // int32
    stringValue?: string;
    type?: "INTEGER" | "STRING" | "BIG_DECIMAL";
  }
  /**
   * SlottingRoute
   */
  export interface SlottingRoute {
    plannedDuration?: number; // int32
    steps?: SlottingRouteStep[];
    transferId?: string; // uuid
  }
  /**
   * SlottingRouteStep
   */
  export interface SlottingRouteStep {
    characteristics?: CharacteristicDto[];
    hierarchy?: PlaceWithType[];
    place?: PlaceWithType;
    plannedDuration?: number; // int32
    reservation?: ReservationInfoDto;
    transferId?: string; // uuid
    zones?: ZoneWithType[];
  }
  /**
   * StockAddressesAndProductIdsSearchFilter
   */
  export interface StockAddressesAndProductIdsSearchFilter {
    addresses?: string[];
    productIds?: string[];
    siteId?: string; // uuid
  }
  /**
   * StockAddressesAndProductIdsSearchQuery
   */
  export interface StockAddressesAndProductIdsSearchQuery {
    filter?: StockAddressesAndProductIdsSearchFilter;
    orderInfo?: OrderByCriteria[];
    pageInfo?: PageRequestInfo;
  }
  /**
   * StockAmountDto
   */
  export interface StockAmountDto {
    productId?: string;
    quantity?: number;
    stockId?: string; // uuid
    unit?: string;
  }
  /**
   * StockDto
   */
  export interface StockDto {
    actualQuantity?: number;
    actualVolume?: number; // int32
    actualWeight?: number; // int32
    availableQuantity?: number;
    availableVolume?: number; // int32
    availableWeight?: number; // int32
    baseUnit?: string;
    batchConfirmed?: boolean;
    batchId?: string; // uuid
    batchNumber?: string;
    carrierId?: string; // uuid
    incomingQuantity?: number;
    incomingVolume?: number; // int32
    incomingWeight?: number; // int32
    outgoingQuantity?: number;
    outgoingVolume?: number; // int32
    outgoingWeight?: number; // int32
    placeId?: string; // uuid
    productId?: string;
    productName?: string;
    stockId?: string; // uuid
    stockTypeCode?: string;
    unit?: string;
  }
  /**
   * StockHierarchySearchFilter
   */
  export interface StockHierarchySearchFilter {
    hierarchy?: PlaceHierarchyFilter;
    placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
    placeTypes?: string /* uuid */ [];
    productIds?: string[];
    zonesType?: PlaceZonesFilter[];
  }
  /**
   * StockHierarchySearchQuery
   */
  export interface StockHierarchySearchQuery {
    filter?: StockHierarchySearchFilter;
    orderInfo?: OrderByCriteria[];
    pageInfo?: PageRequestInfo;
  }
  /**
   * StockInCmd
   */
  export interface StockInCmd {
    baseAmount?: number;
    baseUnit?: string;
    carrierId?: string; // uuid
    documentDate?: string; // date-time
    documentNumber?: string;
    operationId?: string;
    placeId?: string; // uuid
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    productBatchId?: string; // uuid
    quantity?: number; // int32
    stockTypeCode?: string;
    tabNumber?: string;
    unit?: string;
    userName?: string;
  }
  /**
   * StockInResponse
   */
  export interface StockInResponse {
    operationId?: string;
    stockId?: string; // uuid
  }
  /**
   * StockInfo
   */
  export interface StockInfo {
    actual?: FullAmount;
    available?: FullAmount;
    batchConfirmed?: boolean;
    batchId?: string; // uuid
    batchNumber?: string;
    carrier?: boolean;
    expirationTime?: number; // int32
    inbound?: FullAmount;
    inboundTime?: number; // int32
    manufactureTime?: number; // int32
    outbound?: FullAmount;
    placeAddress?: string;
    placeId?: string; // uuid
    productId?: string;
    productName?: string;
    sellByTime?: number; // int32
    stockTypeCode?: string;
    stockTypeId?: string; // uuid
  }
  /**
   * StockTypeDto
   */
  export interface StockTypeDto {
    author?: string;
    code?: string;
    createdTime?: number; // int32
    editor?: string;
    id?: string; // uuid
    name?: string;
    updatedTime?: number; // int32
  }
  /**
   * StocksSearchResult
   */
  export interface StocksSearchResult {
    stocks?: StockInfo[];
    totalCount?: number; // int32
  }
  /**
   * TransferDroppedResponse
   */
  export interface TransferDroppedResponse {
    canceledReservations?: ReservationDto[];
    transferId?: string; // uuid
  }
  /**
   * TransferDto
   */
  export interface TransferDto {
    carrierTypeCode?: string;
    quantity?: number; // int32
    transferId?: string; // uuid
  }
  /**
   * TryAllocateSpaceForReplenishmentCmd
   */
  export interface TryAllocateSpaceForReplenishmentCmd {
    destPlaceBarcode?: string;
    operationId?: string;
    processId?: string;
    productBatchId?: string; // uuid
    quantity?: number; // int32
    siteCode?: string;
    srcCarrierId?: string; // uuid
    stockTypeCode?: string;
  }
  /**
   * TryAllocateSpaceForReplenishmentResponse
   */
  export interface TryAllocateSpaceForReplenishmentResponse {
    approvedQuantity?: number;
    operationId?: string;
    rejectedInfo?: RejectedInfo;
  }
  /**
   * TryTakeRequest
   */
  export interface TryTakeRequest {
    operationId?: string;
    placeId?: string; // uuid
    quantity?: number; // int32
    stockId?: string; // uuid
    transferId?: string; // uuid
    unit?: string;
  }
  /**
   * TryTakeResponse
   */
  export interface TryTakeResponse {
    acceptedQuantity?: number;
    operationId?: string;
    rejectReason?: string;
    rejected?: boolean;
    stockId?: string; // uuid
    transferId?: string; // uuid
  }
  /**
   * Type
   */
  export interface Type {
    code?: string;
    id?: string; // uuid
    name?: string;
  }
  /**
   * UnshippedOrderItemDto
   */
  export interface UnshippedOrderItemDto {
    baseAmount?: number;
    baseUnit?: string;
    position?: number; // int64
    productId?: string;
    quantity?: number; // int32
    unit?: string;
  }
  /**
   * UpdateAllocationSettingsRequest
   */
  export interface UpdateAllocationSettingsRequest {
    maxMixedProducts?: number; // int32
    placeTypeCode?: string;
    siteCode?: string;
    zoneCodes?: string[];
  }
  /**
   * UpdateCarrierTypeCmd
   */
  export interface UpdateCarrierTypeCmd {
    description?: string;
    height?: number; // int32
    length?: number; // int32
    maxVolume?: number;
    maxWeight?: number;
    name?: string;
    width?: number; // int32
  }
  /**
   * UpdatePlaceCmd
   */
  export interface UpdatePlaceCmd {
    address?: string;
    addressTypeId?: string; // uuid
    coordinates?: Coordinates3D;
    description?: string;
    number?: number; // int32
    status?: PlaceStatusDto;
    statusReason?: string;
    typeId?: string; // uuid
    zoneIds?: string /* uuid */ [];
  }
  /**
   * UpdatePlaceTypeCmd
   */
  export interface UpdatePlaceTypeCmd {
    description?: string;
    maxMixBatches?: number; // int32
    name?: string;
    numerationRule?: "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP";
    storagePlace?: boolean;
  }
  /**
   * UpdatePlaceTypeCommonCharacteristicDto
   */
  export interface UpdatePlaceTypeCommonCharacteristicDto {
    characteristic?: string;
    defaultValue?: DefaultValue;
  }
  /**
   * UpdateSiteCmd
   */
  export interface UpdateSiteCmd {
    description?: string;
    name?: string;
  }
  /**
   * UpdateStockTypeByInventoryCmd
   */
  export interface UpdateStockTypeByInventoryCmd {
    baseAmount?: number;
    docDate?: string; // date-time
    documentNumber?: string;
    operationId?: string;
    operationType?: "MOVE";
    orderItem?: number; // int32
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    saleAmount?: number; // int32
    salesOrder?: string;
    specStock?: string;
    stockId?: string; // uuid
    stockTypeCode?: string;
    tabNumber?: string;
    transferId?: string; // uuid
    updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
    userName?: string;
  }
  /**
   * UpdateStockTypeByPickingCmd
   */
  export interface UpdateStockTypeByPickingCmd {
    docDate?: string; // date-time
    documentNumber?: string;
    operationId?: string;
    operationType?: "MOVE";
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    stockId?: string; // uuid
    stockTypeCode?: string;
    tabNumber?: string;
    updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
    userName?: string;
  }
  /**
   * UpdateStockTypeEvent
   */
  export interface UpdateStockTypeEvent {
    actualQuantity?: number;
    batchId?: string; // uuid
    batchNumber?: string;
    beiQuantity?: number;
    beiUnit?: string;
    carrierId?: string; // uuid
    documentDate?: string; // date-time
    documentNumber?: string;
    expirationDate?: string; // date-time
    manufactureDate?: string; // date-time
    newStockId?: string; // uuid
    newStockTypeCode?: string;
    oldStockId?: string; // uuid
    oldStockTypeCode?: string;
    operationId?: string;
    operationType?: "MOVE";
    orderItemIndex?: number; // int32
    placeWithType?: PlaceWithType;
    productId?: string;
    productName?: string;
    salesOrder?: string;
    shouldNotifyERP?: boolean;
    siteCode?: string;
    specialStockCode?: string;
    storageDate?: string; // date-time
    tabNumber?: string;
    unit?: string;
    updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
  }
  /**
   * UpdateStockTypeForBatchCmd
   */
  export interface UpdateStockTypeForBatchCmd {
    batchId?: string; // uuid
    newStockTypeCode?: string;
    oldStockTypeCode?: string;
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    tabNumber?: string;
    userName?: string;
  }
  /**
   * UpdateStockTypeInStockByUICmd
   */
  export interface UpdateStockTypeInStockByUICmd {
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    quantity?: number; // int32
    stockId?: string; // uuid
    stockTypeCode?: string;
    tabNumber?: string;
    userName?: string;
  }
  /**
   * UpdateZoneCmd
   */
  export interface UpdateZoneCmd {
    boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
    code?: string;
    description?: string;
    name?: string;
  }
  /**
   * UpdateZoneTypeCmd
   */
  export interface UpdateZoneTypeCmd {
    description?: string;
    name?: string;
  }
  /**
   * ValueCmd
   */
  export interface ValueCmd {
    decValue?: number;
    intValue?: number; // int32
    maxIntValue?: number; // int32
    minIntValue?: number; // int32
  }
  /**
   * ValueDto
   */
  export interface ValueDto {
    decValue?: number;
    intValue?: number; // int32
    maxDecValue?: number;
    maxIntValue?: number; // int32
    minDecValue?: number;
    minIntValue?: number; // int32
  }
  /**
   * WarehouseProductGroupDto
   */
  export interface WarehouseProductGroupDto {
    code?: string;
  }
  /**
   * WriteOffLostStockCmd
   */
  export interface WriteOffLostStockCmd {
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    quantity?: number; // int32
    stockId?: string; // uuid
    tabNumber?: string;
    userName?: string;
  }
  /**
   * WriteOffRawStocksCmd
   */
  export interface WriteOffRawStocksCmd {
    operationId?: string;
    orderVersion?: string;
    outboundDeliveryNumber?: string;
    outboundTime?: string; // date-time
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    siteCode?: string;
    stocks?: RawStock[];
    tabNumber?: string;
    userName?: string;
  }
  /**
   * WriteOffStockCmd
   */
  export interface WriteOffStockCmd {
    baseAmount?: number;
    baseUnit?: string;
    documentDate?: string; // date-time
    documentNumber?: string;
    operationId?: string;
    processId?: string;
    processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
    quantity?: number; // int32
    stockId?: string; // uuid
    tabNumber?: string;
    unit?: string;
    userName?: string;
  }
  /**
   * WriteOffStockResponse
   */
  export interface WriteOffStockResponse {
    clientError?: Error;
    operationId?: string;
    stockId?: string; // uuid
  }
  /**
   * Zone
   */
  export interface Zone {
    code?: string;
    id?: string; // uuid
    name?: string;
    type?: Type;
  }
  /**
   * ZoneCodeExistsQuery
   */
  export interface ZoneCodeExistsQuery {
    code?: string;
    siteId?: string; // uuid
    typeId?: string; // uuid
  }
  /**
   * ZoneDto
   */
  export interface ZoneDto {
    code?: string;
    id?: string; // uuid
    name?: string;
  }
  /**
   * ZoneInfo
   */
  export interface ZoneInfo {
    code?: string;
    id?: string; // uuid
    name?: string;
    typeCode?: string;
    typeId?: string; // uuid
    typeName?: string;
  }
  /**
   * ZoneProjection
   */
  export interface ZoneProjection {
    author?: string;
    boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
    childAmount?: number; // int32
    code?: string;
    createdTime?: number; // int32
    description?: string;
    editor?: string;
    id?: string; // uuid
    name?: string;
    siteId?: string; // uuid
    typeId?: string; // uuid
    typeName?: string;
    updatedTime?: number; // int32
  }
  /**
   * ZoneReference
   */
  export interface ZoneReference {
    zoneCode?: string;
    zoneTypeCode?: string;
  }
  /**
   * ZoneTypeCodeExistsQuery
   */
  export interface ZoneTypeCodeExistsQuery {
    code?: string;
  }
  /**
   * ZoneTypeDto
   */
  export interface ZoneTypeDto {
    author?: string;
    code?: string;
    createdTime?: number; // int32
    description?: string;
    editor?: string;
    id?: string; // uuid
    name?: string;
    numberOfImplementation?: number; // int64
    updatedTime?: number; // int32
    warehouseProductGroupCanBeBounded?: boolean;
  }
  /**
   * ZoneTypeWithZones
   */
  export interface ZoneTypeWithZones {
    code?: string;
    id?: string; // uuid
    name?: string;
    zones?: ZoneDto[];
  }
  /**
   * ZoneTypeWithZonesDto
   */
  export interface ZoneTypeWithZonesDto {
    code?: string;
    id?: string; // uuid
    name?: string;
    zones?: ZoneDto[];
  }
  /**
   * ZoneWithType
   */
  export interface ZoneWithType {
    code?: string;
    id?: string; // uuid
    name?: string;
    type?: Type;
  }
}
declare namespace Paths {
  namespace AddStockToCarrierUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.AddStockToCarrierCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockDto;
    }
  }
  namespace AddUsingPOST {
    export interface BodyParameters {
      req: Parameters.Req;
    }
    namespace Parameters {
      export type Req = Definitions.AddPlaceTypeCommonCharacteristicDto;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceTypeCharacteristicDTO;
    }
  }
  namespace AssignBatchUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.AssignProductBatchCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ProductBatchDto;
    }
  }
  namespace BindToRouteStrategyUsingPOST {
    export interface FormDataParameters {
      siteCode: Parameters.SiteCode;
    }
    namespace Parameters {
      export type SiteCode = string;
    }
  }
  namespace BoxRouteUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.BuildBoxRouteCmd;
    }
    namespace Responses {
      export type $200 = Definitions.BoxRoute;
    }
  }
  namespace CancelStockReservationUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CancelStockReservationCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ReservationCancelledResponse;
    }
  }
  namespace CarriersUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace CodeExistsUsingGET {
    namespace Responses {
      export type $200 = Definitions.CodeExists;
    }
  }
  namespace CodeExistsUsingPOST {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.CarrierTypeCodeExistsQuery;
    }
    namespace Responses {
      export type $200 = Definitions.CodeExists;
    }
  }
  namespace CodeExistsUsingPOST1 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.PlaceTypeCodeExistsQuery;
    }
    namespace Responses {
      export type $200 = Definitions.CodeExists;
    }
  }
  namespace CodeExistsUsingPOST2 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.SiteCodeExistsQuery;
    }
    namespace Responses {
      export type $200 = Definitions.CodeExists;
    }
  }
  namespace CodeExistsUsingPOST3 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.ZoneCodeExistsQuery;
    }
    namespace Responses {
      export type $200 = Definitions.CodeExists;
    }
  }
  namespace CodeExistsUsingPOST4 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.ZoneTypeCodeExistsQuery;
    }
    namespace Responses {
      export type $200 = Definitions.CodeExists;
    }
  }
  namespace ConfirmBatchUsingPOST {
    export interface BodyParameters {
      confirmProductBatchCmd: Parameters.ConfirmProductBatchCmd;
    }
    namespace Parameters {
      export type ConfirmProductBatchCmd = Definitions.ConfirmProductBatchCmd;
    }
  }
  namespace ConfirmStocksUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ChangeRawStockTypeCmd;
    }
  }
  namespace CreatePlaceTypeUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreatePlaceTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto;
    }
  }
  namespace CreateSiteUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreateSiteCmd;
    }
    namespace Responses {
      export type $200 = Definitions.SiteDto;
    }
  }
  namespace CreateStockTypeUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ModifyStockTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockTypeDto;
    }
  }
  namespace CreateTransferPlanAllocationUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreatePlanRequest;
    }
  }
  namespace CreateTransferPlanPickingUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreatePlanRequest;
    }
  }
  namespace CreateUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreateCarrierCmd;
    }
    namespace Responses {
      export type $200 = Definitions.CarrierDto;
    }
  }
  namespace CreateUsingPOST1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreateCarrierTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.CarrierTypeDto;
    }
  }
  namespace CreateUsingPOST2 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreatePlaceCmd;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceDto;
    }
  }
  namespace CreateUsingPOST3 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreateZoneTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ZoneTypeDto;
    }
  }
  namespace CreateZoneUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.CreateZoneCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ZoneDto;
    }
  }
  namespace DropAllReservationsForProcessUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.DropAllProcessReservationsCmd;
    }
  }
  namespace DropReservationForProcessUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.DropCarrierReservationForProcessCmd;
    }
  }
  namespace DropReservationForProcessUsingPOST1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.DropPlaceReservationForProcessCmd;
    }
  }
  namespace DropReservationUsingDELETE {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.DropPlaceReservationsForTransferCmd;
    }
  }
  namespace DropTransferUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.DropTransferCmd;
    }
    namespace Responses {
      export type $200 = Definitions.TransferDroppedResponse;
    }
  }
  namespace FindAllUsingPOST {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.DetailedStockSearchQuery;
    }
    namespace Responses {
      export type $200 = Definitions.DetailedStocksSearchResult;
    }
  }
  namespace FindPlacesByProcessIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceDto[];
    }
  }
  namespace FindTransferUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.PickingZoneTransferReq;
    }
    namespace Responses {
      export type $200 = Definitions.Route;
    }
  }
  namespace FindTransferUsingPOST1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.AllocationZoneTransferReq;
    }
    namespace Responses {
      export type $200 = Definitions.Route;
    }
  }
  namespace GetAllUsingGET {
    namespace Responses {
      export type $200 = Definitions.CarrierDto[];
    }
  }
  namespace GetAllUsingGET1 {
    namespace Responses {
      export type $200 = Definitions.ProductBatchDto[];
    }
  }
  namespace GetAllUsingGET2 {
    namespace Responses {
      export type $200 = Definitions.ReservationDto[];
    }
  }
  namespace GetAllUsingGET3 {
    namespace Responses {
      export type $200 = Definitions.StockDto[];
    }
  }
  namespace GetAllUsingPOST {
    export interface BodyParameters {
      detailedCarriersSearchQuery: Parameters.DetailedCarriersSearchQuery;
    }
    namespace Parameters {
      export type DetailedCarriersSearchQuery = Definitions.DetailedCarriersSearchQuery;
    }
    namespace Responses {
      export type $200 = Definitions.CarrierDto[];
    }
  }
  namespace GetAvailablePlaceTypesByParentPlaceIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto[];
    }
  }
  namespace GetBatchBalanceDifferenceUsingGET {
    namespace Responses {
      export type $200 = Definitions.ProductBatchBalanceDifferenceDto;
    }
  }
  namespace GetBulkUpdateOperationDataUsingGET {
    namespace Responses {
      export type $200 = Definitions.BulkOperationDto;
    }
  }
  namespace GetByIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.CarrierDto;
    }
  }
  namespace GetByIdUsingGET1 {
    namespace Responses {
      export type $200 = Definitions.ProductBatchDto;
    }
  }
  namespace GetByIdUsingGET2 {
    namespace Responses {
      export type $200 = Definitions.StockDto;
    }
  }
  namespace GetByPlaceIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.CarrierGridView[];
    }
  }
  namespace GetCarrierTypeUsingGET {
    namespace Responses {
      export type $200 = Definitions.CarrierTypeDto;
    }
  }
  namespace GetCarrierTypesUsingGET {
    namespace Responses {
      export type $200 = Definitions.CarrierTypeDto[];
    }
  }
  namespace GetConsolidationZoneUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneDto;
    }
  }
  namespace GetHierarchyByTypeCodeUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceHierarchyTreeDto[];
    }
  }
  namespace GetHierarchyForParentIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceNodeWithPosition;
    }
  }
  namespace GetHierarchyUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceNodeWithPosition;
    }
  }
  namespace GetPlaceByCodeUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceShortView[];
    }
  }
  namespace GetPlaceByIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceWithPosition;
    }
  }
  namespace GetPlaceCharacteristicUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceCharacteristicDto;
    }
  }
  namespace GetPlaceCharacteristicsUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceCharacteristicDto[];
    }
  }
  namespace GetPlaceStatusesUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceStatusDto[];
    }
  }
  namespace GetPlaceTypeCharacteristicUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeCharacteristicDTO;
    }
  }
  namespace GetPlaceTypeCharacteristicsUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeCharacteristicDTO[];
    }
  }
  namespace GetPlaceTypeUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto;
    }
  }
  namespace GetPlaceTypesBySiteUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto[];
    }
  }
  namespace GetPlaceTypesBySiteUsingGET1 {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto[];
    }
  }
  namespace GetPlaceUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceDto;
    }
  }
  namespace GetRouteUsingPOST {
    namespace Responses {
      export type $200 = Definitions.PlaceShortView[];
    }
  }
  namespace GetSettingsForSiteAndPlaceTypeUsingGET {
    namespace Responses {
      export type $200 = Definitions.AllocationSettingsDto[];
    }
  }
  namespace GetSettingsForSiteUsingGET {
    namespace Responses {
      export type $200 = Definitions.SiteSettingDto[];
    }
  }
  namespace GetSiteByCodeUsingGET {
    namespace Responses {
      export type $200 = Definitions.SiteDto;
    }
  }
  namespace GetSiteUsingGET {
    namespace Responses {
      export type $200 = Definitions.SiteDto;
    }
  }
  namespace GetSitesUsingGET {
    namespace Responses {
      export type $200 = Definitions.SiteDto[];
    }
  }
  namespace GetStockTypeByCodeUsingGET {
    namespace Responses {
      export type $200 = Definitions.StockTypeDto;
    }
  }
  namespace GetStockTypeByIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.StockTypeDto;
    }
  }
  namespace GetStockTypesUsingGET {
    namespace Responses {
      export type $200 = Definitions.StockTypeDto[];
    }
  }
  namespace GetTransferPlaceUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceShortView;
    }
  }
  namespace GetTransferUsingGET {
    namespace Responses {
      export type $200 = Definitions.Route;
    }
  }
  namespace GetZoneByIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneDto;
    }
  }
  namespace GetZoneTypeUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneTypeDto;
    }
  }
  namespace GetZoneTypesUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneDto[];
    }
  }
  namespace GetZoneTypesUsingGET1 {
    namespace Responses {
      export type $200 = Definitions.ZoneTypeDto[];
    }
  }
  namespace GetZoneTypesUsingGET2 {
    namespace Responses {
      export type $200 = Definitions.ZoneTypeDto[];
    }
  }
  namespace GetZoneTypesWithZonesUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneTypeWithZones[];
    }
  }
  namespace GetZoneTypesWithZonesUsingGET1 {
    namespace Responses {
      export type $200 = Definitions.ZoneTypeWithZonesDto[];
    }
  }
  namespace GetZonesBySiteIdAndTypeIdUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneProjection[];
    }
  }
  namespace GetZonesByTypeUsingGET {
    namespace Responses {
      export type $200 = Definitions.ZoneDto[];
    }
  }
  namespace MonoPalletProductPlacementUsingPOST {
    export interface BodyParameters {
      request: Parameters.Request;
    }
    namespace Parameters {
      export type Request = Definitions.MonoPalletPlacementRequest;
    }
    namespace Responses {
      export type $200 = Definitions.ProductPlacementResponse;
    }
  }
  namespace MoveCarrierToPlaceUsingPUT {
    namespace Responses {
      export type $200 = Definitions.MoveCarrierToPlaceResponse;
    }
  }
  namespace MoveStockToCarrierUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.MoveAllStocksToCarrierCmd;
    }
    namespace Responses {
      export type $200 = Definitions.MoveAllStocksToCarrierResponse;
    }
  }
  namespace MoveStockToCarrierUsingPUT1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.MoveStockToCarrierCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockDto;
    }
  }
  namespace MoveStockUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.MoveStockCmd;
    }
    namespace Responses {
      export type $200 = Definitions.MoveStockCmdResponse;
    }
  }
  namespace OutboundOrderUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.OutboundOrderCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockDto[];
    }
  }
  namespace OutboundOrderUsingPOST1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.OutboundOrderCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockDto[];
    }
  }
  namespace OverrideUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.OverridePlaceCommonCharacteristicCmd;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceCharacteristicDto;
    }
  }
  namespace PickerRoutesUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.PickerRouteReq;
    }
    namespace Responses {
      export type $200 = Definitions.Route;
    }
  }
  namespace PickingZonesUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace PlaceTypesUsingGET {
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto[];
    }
  }
  namespace PlacesUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace ProductPlacementUsingPOST {
    export interface BodyParameters {
      request: Parameters.Request;
    }
    namespace Parameters {
      export type Request = Definitions.ProductPlacementRequest;
    }
    namespace Responses {
      export type $200 = Definitions.ProductPlacementResponse;
    }
  }
  namespace RawPlacementUsingPOST {
    export interface BodyParameters {
      request: Parameters.Request;
    }
    namespace Parameters {
      export type Request = Definitions.RawPlacementRequest;
    }
    namespace Responses {
      export type $200 = Definitions.ProductPlacementResponse;
    }
  }
  namespace ReduceIncomingQuantityUsingPOST {
    export interface BodyParameters {
      request: Parameters.Request;
    }
    namespace Parameters {
      export type Request = Definitions.ReduceIncomingQuantityCmd;
    }
  }
  namespace ReduceReservationForTransferUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ReducePlaceReservationForTransferCmd;
    }
  }
  namespace ReplenishUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ReplenishCmd;
    }
  }
  namespace ReserveForProcessUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ReserveCarrierForProcessCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ReserveCarrierForProcessResponse;
    }
  }
  namespace ReserveForProcessUsingPOST1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ReservePlaceForProcessCmd;
    }
  }
  namespace ReserveOutgoingUsingPOST {
    namespace Responses {
      export type $200 = Definitions.ReserveStockOutgoingQuantityResponse;
    }
  }
  namespace ReserveUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ReservePlacesForCarriersCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ReservedPlacesForCarriersResponse;
    }
  }
  namespace RouteStartRulesUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace SearchUsingPOST {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.PlacesSearchQuery;
    }
    namespace Responses {
      export type $200 = Definitions.PlacesSearchResult;
    }
  }
  namespace SearchUsingPOST1 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.ProductBatchesSearchQuery;
    }
    namespace Responses {
      export type $200 = Definitions.ProductBatchesSearchResult;
    }
  }
  namespace SearchUsingPOST2 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.StockAddressesAndProductIdsSearchQuery;
    }
    namespace Responses {
      export type $200 = Definitions.StocksSearchResult;
    }
  }
  namespace SearchUsingPOST3 {
    export interface BodyParameters {
      query: Parameters.Query;
    }
    namespace Parameters {
      export type Query = Definitions.StockHierarchySearchQuery;
    }
    namespace Responses {
      export type $200 = Definitions.StocksSearchResult;
    }
  }
  namespace ShipUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ShipCarrierCmd;
    }
    namespace Responses {
      export type $200 = Definitions.CarrierShippedResponse;
    }
  }
  namespace SlottingRouteUsingGET {
    namespace Responses {
      export type $200 = Definitions.SlottingRoute;
    }
  }
  namespace StartBulkUpdatePlaceStatusUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.BulkPlaceUpdateStatusCmd;
    }
    namespace Responses {
      export type $200 = Definitions.BulkOperationDto;
    }
  }
  namespace StartBulkUpdatePlaceTypeUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.BulkPlaceUpdateTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.BulkOperationDto;
    }
  }
  namespace StartBulkUpdateZoneUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.BulkPlaceUpdateZoneCmd;
    }
    namespace Responses {
      export type $200 = Definitions.BulkOperationDto;
    }
  }
  namespace StockInUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.StockInCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockInResponse;
    }
  }
  namespace StocksUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace StorageZonesUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace TryAllocateSpaceManualUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.TryAllocateSpaceForReplenishmentCmd;
    }
    namespace Responses {
      export type $200 = Definitions.TryAllocateSpaceForReplenishmentResponse;
    }
  }
  namespace TryAllocateSpaceUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.TryAllocateSpaceForReplenishmentCmd;
    }
    namespace Responses {
      export type $200 = Definitions.TryAllocateSpaceForReplenishmentResponse;
    }
  }
  namespace TryTakeUsingPOST {
    export interface BodyParameters {
      tryTakeRequest: Parameters.TryTakeRequest;
    }
    namespace Parameters {
      export type TryTakeRequest = Definitions.TryTakeRequest;
    }
    namespace Responses {
      export type $200 = Definitions.TryTakeResponse;
    }
  }
  namespace UpdateAllocationSettingsUsingPOST {
    export interface BodyParameters {
      request: Parameters.Request;
    }
    namespace Parameters {
      export type Request = Definitions.UpdateAllocationSettingsRequest;
    }
    namespace Responses {
      export type $200 = Definitions.AllocationSettingsDto[];
    }
  }
  namespace UpdatePlaceTypeUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdatePlaceTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceTypeDto;
    }
  }
  namespace UpdateSettingsForSiteUsingPUT {
    export interface BodyParameters {
      siteSettingDtos: Parameters.SiteSettingDtos;
    }
    namespace Parameters {
      export type SiteSettingDtos = Definitions.SiteSettingDto[];
    }
    namespace Responses {
      export type $200 = Definitions.SiteSettingDto[];
    }
  }
  namespace UpdateSiteUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdateSiteCmd;
    }
    namespace Responses {
      export type $200 = Definitions.SiteDto;
    }
  }
  namespace UpdateStockTypeForBatchUsingPOST {
    export interface BodyParameters {
      updateStockTypeForBatchCmd: Parameters.UpdateStockTypeForBatchCmd;
    }
    namespace Parameters {
      export type UpdateStockTypeForBatchCmd = Definitions.UpdateStockTypeForBatchCmd;
    }
  }
  namespace UpdateStockTypeInStockByInventoryUsingPOST {
    export interface BodyParameters {
      updateStockTypeCmd: Parameters.UpdateStockTypeCmd;
    }
    namespace Parameters {
      export type UpdateStockTypeCmd = Definitions.UpdateStockTypeByInventoryCmd;
    }
    namespace Responses {
      export type $200 = Definitions.UpdateStockTypeEvent;
    }
  }
  namespace UpdateStockTypeInStockByPickingUsingPOST {
    export interface BodyParameters {
      updateStockTypeCmd: Parameters.UpdateStockTypeCmd;
    }
    namespace Parameters {
      export type UpdateStockTypeCmd = Definitions.UpdateStockTypeByPickingCmd;
    }
    namespace Responses {
      export type $200 = Definitions.UpdateStockTypeEvent;
    }
  }
  namespace UpdateStockTypeInStockByUIUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdateStockTypeInStockByUICmd;
    }
  }
  namespace UpdateStockTypeUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.ModifyStockTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.StockTypeDto;
    }
  }
  namespace UpdateUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdateCarrierTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.CarrierTypeDto;
    }
  }
  namespace UpdateUsingPUT1 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdatePlaceCmd;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceDto;
    }
  }
  namespace UpdateUsingPUT2 {
    export interface BodyParameters {
      req: Parameters.Req;
    }
    namespace Parameters {
      export type Req = Definitions.UpdatePlaceTypeCommonCharacteristicDto;
    }
    namespace Responses {
      export type $200 = Definitions.PlaceTypeCharacteristicDTO;
    }
  }
  namespace UpdateUsingPUT3 {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdateZoneTypeCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ZoneTypeDto;
    }
  }
  namespace UpdateZoneUsingPUT {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.UpdateZoneCmd;
    }
    namespace Responses {
      export type $200 = Definitions.ZoneDto;
    }
  }
  namespace UploadAllocationSettingsUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace UploadProductBatchesUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace UploadRouteStrategyUsingPOST {
    export interface FormDataParameters {
      siteCode: Parameters.SiteCode;
    }
    namespace Parameters {
      export type SiteCode = string;
    }
  }
  namespace UploadWriteOffSettingsUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace UploadZoneCharacteristicsUsingPOST {
    export interface FormDataParameters {
      siteCode: Parameters.SiteCode;
    }
    namespace Parameters {
      export type SiteCode = string;
    }
  }
  namespace WriteOffLostStockUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.WriteOffLostStockCmd;
    }
    namespace Responses {
      export type $200 = Definitions.WriteOffStockResponse;
    }
  }
  namespace WriteOffRawStockUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.WriteOffRawStocksCmd;
    }
  }
  namespace WriteOffStockUsingPOST {
    export interface BodyParameters {
      cmd: Parameters.Cmd;
    }
    namespace Parameters {
      export type Cmd = Definitions.WriteOffStockCmd;
    }
    namespace Responses {
      export type $200 = Definitions.WriteOffStockResponse;
    }
  }
  namespace ZoneTransferRulesUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
  namespace ZonesUploadUsingPOST {
    export interface FormDataParameters {
      siteId: Parameters.SiteId; // uuid
    }
    namespace Parameters {
      export type SiteId = string; // uuid
    }
  }
}

export interface OperationMethods {
  /**
   * uploadAllocationSettingsUsingPOST - Запрос на загрузку файла с настройками миксования
   * 
   * settingsFile, siteId - обязательные поля
   */
  'uploadAllocationSettingsUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * monoPalletProductPlacementUsingPOST - Запрос в МРТ на размещение монопаллеты товара
   * 
   * operationId, productId, productBatchId - обязательные поля
   */
  'monoPalletProductPlacementUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.MonoPalletProductPlacementUsingPOST.Responses.$200>
  /**
   * productPlacementUsingPOST - Запрос в МРТ на обычное (не паллетное) размещение товара
   * 
   * operationId, productId, productBatchId - обязательные поля
   */
  'productPlacementUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ProductPlacementUsingPOST.Responses.$200>
  /**
   * rawPlacementUsingPOST - Запрос в МРТ на размещение сырья
   * 
   * operationId, productId, productBatchId - обязательные поля
   */
  'rawPlacementUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RawPlacementUsingPOST.Responses.$200>
  /**
   * getSettingsForSiteAndPlaceTypeUsingGET - Запрос в МРТ на получение настроек миксования
   * 
   * siteCode, placeTypeCode - обязательные поля
   */
  'getSettingsForSiteAndPlaceTypeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSettingsForSiteAndPlaceTypeUsingGET.Responses.$200>
  /**
   * updateAllocationSettingsUsingPOST - Запрос в МРТ для изменения настроек миксования
   * 
   * siteCode, placeTypeCode, maxMixedProducts  - обязательные поля
   */
  'updateAllocationSettingsUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateAllocationSettingsUsingPOST.Responses.$200>
  /**
   * boxRouteUsingPOST - Запрос в МKТ для маршрутизации коробов комплектации
   */
  'boxRouteUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.BoxRouteUsingPOST.Responses.$200>
  /**
   * pickerRoutesUsingPOST - Запрос в МKТ для текущего маршрута комплектовщика
   */
  'pickerRoutesUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PickerRoutesUsingPOST.Responses.$200>
  /**
   * findTransferUsingPOST - Запрос в МKТ для перехода между областями действий
   */
  'findTransferUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.FindTransferUsingPOST.Responses.$200>
  /**
   * getConsolidationZoneUsingGET - Запрос для получения области консолидации для области комплектации
   */
  'getConsolidationZoneUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetConsolidationZoneUsingGET.Responses.$200>
  /**
   * getAllUsingGET_1 - getAll
   */
  'getAllUsingGET_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAllUsingGET1.Responses.$200>
  /**
   * assignBatchUsingPOST - assignBatch
   */
  'assignBatchUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AssignBatchUsingPOST.Responses.$200>
  /**
   * getBatchBalanceDifferenceUsingGET - getBatchBalanceDifference
   */
  'getBatchBalanceDifferenceUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBatchBalanceDifferenceUsingGET.Responses.$200>
  /**
   * fixBatchBalanceDifferenceUsingPOST - fixBatchBalanceDifference
   */
  'fixBatchBalanceDifferenceUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * confirmBatchUsingPOST - "Обеление" вида запаса
   */
  'confirmBatchUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * searchUsingPOST_1 - Search Batches
   */
  'searchUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchUsingPOST1.Responses.$200>
  /**
   * uploadProductBatchesUsingPOST - Запрос на загрузку файла с партиями товара
   * 
   * batchesFile, siteId - обязательные поля
   */
  'uploadProductBatchesUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getByIdUsingGET_1 - getById
   */
  'getByIdUsingGET_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetByIdUsingGET1.Responses.$200>
  /**
   * replenishUsingPOST - Размещение товара
   */
  'replenishUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * findTransferUsingPOST_1 - Запрос на построение и резервирование маршрута для перехода между областями действий
   */
  'findTransferUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.FindTransferUsingPOST1.Responses.$200>
  /**
   * getTransferUsingGET - Запрос на получение маршрута для перехода между областями действий
   */
  'getTransferUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTransferUsingGET.Responses.$200>
  /**
   * tryAllocateSpaceUsingPOST - Проверка места перед размещением товара
   */
  'tryAllocateSpaceUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.TryAllocateSpaceUsingPOST.Responses.$200>
  /**
   * tryAllocateSpaceManualUsingPOST - Проверка места перед ручным перемещением товара
   */
  'tryAllocateSpaceManualUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.TryAllocateSpaceManualUsingPOST.Responses.$200>
  /**
   * routeStartRulesUploadUsingPOST - Запрос на загрузку файла правил мест старта
   * 
   * routeStartRuleFile, siteId - обязательные поля
   */
  'routeStartRulesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * uploadRouteStrategyUsingPOST - Запрос на загрузку файла с приоритетом обхода секций
   * 
   * routeStrategyFile, siteId - обязательные поля
   */
  'uploadRouteStrategyUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * slottingRouteUsingGET - Запрос маршрута слотчика по области действий
   * 
   * Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
   */
  'slottingRouteUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SlottingRouteUsingGET.Responses.$200>
  /**
   * triggerSnapshotUsingPOST - triggerSnapshot
   */
  'triggerSnapshotUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getAllUsingGET_3 - getAll
   */
  'getAllUsingGET_3'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAllUsingGET3.Responses.$200>
  /**
   * searchUsingPOST_2 - Search Stocks by Addresses and Product Ids
   */
  'searchUsingPOST_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchUsingPOST2.Responses.$200>
  /**
   * getCarrierTypesUsingGET - getCarrierTypes
   */
  'getCarrierTypesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCarrierTypesUsingGET.Responses.$200>
  /**
   * createUsingPOST_1 - create
   */
  'createUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateUsingPOST1.Responses.$200>
  /**
   * codeExistsUsingPOST - codeExists
   */
  'codeExistsUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CodeExistsUsingPOST.Responses.$200>
  /**
   * carrierTypesUploadUsingPOST - Запрос на загрузку файла с типами носителя
   * 
   * carrierTypeFile - обязательные поля
   */
  'carrierTypesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * updateUsingPUT - update
   */
  'updateUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateUsingPUT.Responses.$200>
  /**
   * getCarrierTypeUsingGET - getCarrierType
   */
  'getCarrierTypeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCarrierTypeUsingGET.Responses.$200>
  /**
   * getAllUsingGET - getAll
   */
  'getAllUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAllUsingGET.Responses.$200>
  /**
   * createUsingPOST - create a Carrier
   * 
   * Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
   */
  'createUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateUsingPOST.Responses.$200>
  /**
   * getByPlaceIdUsingGET - Get carriers hierarchy in place for grid representation
   */
  'getByPlaceIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetByPlaceIdUsingGET.Responses.$200>
  /**
   * dropReservationForProcessUsingPOST - Сброс резерва носителя под процесс
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'dropReservationForProcessUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getAllUsingPOST - getAll
   */
  'getAllUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAllUsingPOST.Responses.$200>
  /**
   * reserveForProcessUsingPOST - Резервирование носителя под процесс
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'reserveForProcessUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ReserveForProcessUsingPOST.Responses.$200>
  /**
   * shipUsingPOST - Отгрузка носителя
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'shipUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ShipUsingPOST.Responses.$200>
  /**
   * carriersUploadUsingPOST - Запрос на загрузку файла с носителями
   * 
   * carrierFile, siteId - обязательные поля
   */
  'carriersUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * addStockToCarrierUsingPUT - Добавление запаса в носитель
   */
  'addStockToCarrierUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AddStockToCarrierUsingPUT.Responses.$200>
  /**
   * moveCarrierIntoCarrierUsingPUT - moveCarrierIntoCarrier
   */
  'moveCarrierIntoCarrierUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getByIdUsingGET - getById
   */
  'getByIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetByIdUsingGET.Responses.$200>
  /**
   * deleteUsingDELETE - delete a Carrier
   * 
   * Delete a Carrier. CarrierId are required.
   */
  'deleteUsingDELETE'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * confirmStocksUsingPOST - Отпуск сырья
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'confirmStocksUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * findAllUsingPOST - Детальный поиск по стокам
   */
  'findAllUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.FindAllUsingPOST.Responses.$200>
  /**
   * moveStockToCarrierUsingPUT - Перемещение запаса в носитель с корректировкой резерва
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'moveStockToCarrierUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.MoveStockToCarrierUsingPUT.Responses.$200>
  /**
   * moveStockUsingPOST - Перемещение запаса (без проверок, без корректировки резервов)
   */
  'moveStockUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.MoveStockUsingPOST.Responses.$200>
  /**
   * outboundOrderUsingPOST - Отгрузка заказов
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'outboundOrderUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.OutboundOrderUsingPOST.Responses.$200>
  /**
   * reduceIncomingQuantityUsingPOST - Запрос на корректировку резервов
   * 
   * reduce incoming quantity
   */
  'reduceIncomingQuantityUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * cancelStockReservationUsingPOST - Точечная отмена резерва
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей.
   */
  'cancelStockReservationUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CancelStockReservationUsingPOST.Responses.$200>
  /**
   * reserveUsingPOST - Резервирование мест под носители
   */
  'reserveUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ReserveUsingPOST.Responses.$200>
  /**
   * dropReservationUsingDELETE - Сброс резерва места под носитель
   */
  'dropReservationUsingDELETE'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * reduceReservationForTransferUsingPUT - Уменьшить резерв под носители в месте
   */
  'reduceReservationForTransferUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getAllUsingGET_2 - Запрос на получение списка резервов
   */
  'getAllUsingGET_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAllUsingGET2.Responses.$200>
  /**
   * reserveOutgoingUsingPOST - Создание исходящего резерва на запас по id
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'reserveOutgoingUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ReserveOutgoingUsingPOST.Responses.$200>
  /**
   * dropTransferUsingPOST - Отмена трансфера
   * 
   * Диагностический эндпоинт, предназначен только для служебных целей
   */
  'dropTransferUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DropTransferUsingPOST.Responses.$200>
  /**
   * searchUsingPOST_3 - Search Stocks
   */
  'searchUsingPOST_3'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchUsingPOST3.Responses.$200>
  /**
   * stockInUsingPOST - Добавление найденного товара в место или носитель без проверок
   */
  'stockInUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.StockInUsingPOST.Responses.$200>
  /**
   * getStockTypesUsingGET - Запрос списка видов запаса
   */
  'getStockTypesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetStockTypesUsingGET.Responses.$200>
  /**
   * createStockTypeUsingPOST - Создание нового справочника вида запаса
   * 
   * code - обязательное поле
   */
  'createStockTypeUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateStockTypeUsingPOST.Responses.$200>
  /**
   * getStockTypeByCodeUsingGET - Запрос вида запаса по коду
   */
  'getStockTypeByCodeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetStockTypeByCodeUsingGET.Responses.$200>
  /**
   * codeExistsUsingGET - Проверка существует ли вид запаса с указанным кодом
   */
  'codeExistsUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CodeExistsUsingGET.Responses.$200>
  /**
   * getStockTypeByIdUsingGET - Запрос вида запаса по идентификатору
   */
  'getStockTypeByIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetStockTypeByIdUsingGET.Responses.$200>
  /**
   * updateStockTypeUsingPUT - Обновление вида запаса
   * 
   * code - обязательное поле
   */
  'updateStockTypeUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateStockTypeUsingPUT.Responses.$200>
  /**
   * archiveStockTypeUsingDELETE - Установка признака заархивирован
   */
  'archiveStockTypeUsingDELETE'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * tryTakeUsingPOST - Запрос на попытку взять товар из ячейки
   */
  'tryTakeUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.TryTakeUsingPOST.Responses.$200>
  /**
   * updateExpiredStockTypeUsingPOST - Перевод всех товаров с истекающим сроком реализации в SS
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'updateExpiredStockTypeUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * updateStockTypeInStockByInventoryUsingPOST - Изменение вида запаса инвентаризацией
   */
  'updateStockTypeInStockByInventoryUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateStockTypeInStockByInventoryUsingPOST.Responses.$200>
  /**
   * updateStockTypeInStockByPickingUsingPOST - Изменение вида запаса комплектацией
   */
  'updateStockTypeInStockByPickingUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateStockTypeInStockByPickingUsingPOST.Responses.$200>
  /**
   * updateStockTypeInStockByUIUsingPOST - Изменение вида запаса через UI
   */
  'updateStockTypeInStockByUIUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * updateStockTypeForBatchUsingPOST - Изменение вида запаса для партии. Вызывается после обеления.
   */
  'updateStockTypeForBatchUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * stocksUploadUsingPOST - Запрос на загрузку файла с запасами
   * 
   * stocksFile, siteId - обязательные поля
   */
  'stocksUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * writeOffStockUsingPOST - Списание запаса
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'writeOffStockUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.WriteOffStockUsingPOST.Responses.$200>
  /**
   * writeOffLostStockUsingPOST - Списание потерянного запаса
   */
  'writeOffLostStockUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.WriteOffLostStockUsingPOST.Responses.$200>
  /**
   * writeOffRawStockUsingPOST - Отпуск сырья
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'writeOffRawStockUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getByIdUsingGET_2 - getById
   */
  'getByIdUsingGET_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetByIdUsingGET2.Responses.$200>
  /**
   * moveStockToCarrierUsingPUT_1 - Перемещение запаса в носитель с корректировкой резерва
   */
  'moveStockToCarrierUsingPUT_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.MoveStockToCarrierUsingPUT1.Responses.$200>
  /**
   * outboundOrderUsingPOST_1 - Отгрузка заказов
   * 
   * Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
   */
  'outboundOrderUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.OutboundOrderUsingPOST1.Responses.$200>
  /**
   * getZoneTypesUsingGET - getZoneTypes
   */
  'getZoneTypesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneTypesUsingGET.Responses.$200>
  /**
   * placeTypesUsingGET - placeTypes
   */
  'placeTypesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PlaceTypesUsingGET.Responses.$200>
  /**
   * getZoneTypesUsingGET_1 - getZoneTypes
   */
  'getZoneTypesUsingGET_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneTypesUsingGET1.Responses.$200>
  /**
   * getPlaceStatusesUsingGET - getPlaceStatuses
   */
  'getPlaceStatusesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceStatusesUsingGET.Responses.$200>
  /**
   * getPlaceTypeCharacteristicsUsingGET - getPlaceTypeCharacteristics
   */
  'getPlaceTypeCharacteristicsUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceTypeCharacteristicsUsingGET.Responses.$200>
  /**
   * addUsingPOST - add
   */
  'addUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AddUsingPOST.Responses.$200>
  /**
   * getPlaceTypeCharacteristicUsingGET - getPlaceTypeCharacteristic
   */
  'getPlaceTypeCharacteristicUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceTypeCharacteristicUsingGET.Responses.$200>
  /**
   * updateUsingPUT_2 - update
   */
  'updateUsingPUT_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateUsingPUT2.Responses.$200>
  /**
   * removeUsingDELETE - remove
   */
  'removeUsingDELETE'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getPlaceUsingGET - getPlace
   */
  'getPlaceUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceUsingGET.Responses.$200>
  /**
   * createUsingPOST_2 - create
   */
  'createUsingPOST_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateUsingPOST2.Responses.$200>
  /**
   * startBulkUpdatePlaceStatusUsingPOST - startBulkUpdatePlaceStatus
   */
  'startBulkUpdatePlaceStatusUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.StartBulkUpdatePlaceStatusUsingPOST.Responses.$200>
  /**
   * startBulkUpdatePlaceTypeUsingPOST - startBulkUpdatePlaceType
   */
  'startBulkUpdatePlaceTypeUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.StartBulkUpdatePlaceTypeUsingPOST.Responses.$200>
  /**
   * startBulkUpdateZoneUsingPOST - startBulkUpdateZone
   */
  'startBulkUpdateZoneUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.StartBulkUpdateZoneUsingPOST.Responses.$200>
  /**
   * getBulkUpdateOperationDataUsingGET - getBulkUpdateOperationData
   */
  'getBulkUpdateOperationDataUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBulkUpdateOperationDataUsingGET.Responses.$200>
  /**
   * placesDownloadUsingGET - Запрос на выгрузку файла с топологией
   * 
   * siteId - обязательные поля
   */
  'placesDownloadUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * dropAllReservationsForProcessUsingPOST - Сброс все резервы места под процесс
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'dropAllReservationsForProcessUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * dropReservationForProcessUsingPOST_1 - Сброс резерва места под процесс
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'dropReservationForProcessUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * findPlacesByProcessIdUsingGET - findPlacesByProcessId
   */
  'findPlacesByProcessIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.FindPlacesByProcessIdUsingGET.Responses.$200>
  /**
   * getHierarchyUsingGET - getHierarchy
   */
  'getHierarchyUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetHierarchyUsingGET.Responses.$200>
  /**
   * getHierarchyForParentIdUsingGET - getHierarchyForParentId
   */
  'getHierarchyForParentIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetHierarchyForParentIdUsingGET.Responses.$200>
  /**
   * getHierarchyByTypeCodeUsingGET - getHierarchyByTypeCode
   */
  'getHierarchyByTypeCodeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetHierarchyByTypeCodeUsingGET.Responses.$200>
  /**
   * reserveForProcessUsingPOST_1 - Резервирование места под процесс
   * 
   * Диагностический эндпоинт, предназначен только для тестовых целей
   */
  'reserveForProcessUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * searchUsingPOST - Search Places
   */
  'searchUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchUsingPOST.Responses.$200>
  /**
   * getPlaceTypesBySiteUsingGET - getPlaceTypesBySite
   */
  'getPlaceTypesBySiteUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceTypesBySiteUsingGET.Responses.$200>
  /**
   * getZoneTypesWithZonesUsingGET - getZoneTypesWithZones
   */
  'getZoneTypesWithZonesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneTypesWithZonesUsingGET.Responses.$200>
  /**
   * getPlaceByCodeUsingGET - getPlaceByCode
   */
  'getPlaceByCodeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceByCodeUsingGET.Responses.$200>
  /**
   * getTransferPlaceUsingGET - getTransferPlace
   */
  'getTransferPlaceUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTransferPlaceUsingGET.Responses.$200>
  /**
   * placesUploadUsingPOST - Запрос на загрузку файла с топологией
   * 
   * placesFile, siteId - обязательные поля
   */
  'placesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getPlaceByIdUsingGET - getPlaceById
   */
  'getPlaceByIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceByIdUsingGET.Responses.$200>
  /**
   * updateUsingPUT_1 - update
   */
  'updateUsingPUT_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateUsingPUT1.Responses.$200>
  /**
   * deleteUsingDELETE_1 - delete
   */
  'deleteUsingDELETE_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getPlaceCharacteristicsUsingGET - getPlaceCharacteristics
   */
  'getPlaceCharacteristicsUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceCharacteristicsUsingGET.Responses.$200>
  /**
   * getPlaceCharacteristicUsingGET - getPlaceCharacteristic
   */
  'getPlaceCharacteristicUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceCharacteristicUsingGET.Responses.$200>
  /**
   * overrideUsingPUT - override
   */
  'overrideUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.OverrideUsingPUT.Responses.$200>
  /**
   * getSitesUsingGET - getSites
   */
  'getSitesUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSitesUsingGET.Responses.$200>
  /**
   * createSiteUsingPOST - createSite
   */
  'createSiteUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateSiteUsingPOST.Responses.$200>
  /**
   * getSiteByCodeUsingGET - getSiteByCode
   */
  'getSiteByCodeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSiteByCodeUsingGET.Responses.$200>
  /**
   * codeExistsUsingPOST_2 - codeExists
   */
  'codeExistsUsingPOST_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CodeExistsUsingPOST2.Responses.$200>
  /**
   * getSettingsForSiteUsingGET - getSettingsForSite
   */
  'getSettingsForSiteUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSettingsForSiteUsingGET.Responses.$200>
  /**
   * updateSettingsForSiteUsingPUT - updateSettingsForSite
   */
  'updateSettingsForSiteUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateSettingsForSiteUsingPUT.Responses.$200>
  /**
   * getSiteUsingGET - getSite
   */
  'getSiteUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSiteUsingGET.Responses.$200>
  /**
   * updateSiteUsingPUT - updateSite
   */
  'updateSiteUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateSiteUsingPUT.Responses.$200>
  /**
   * getPlaceTypesBySiteUsingGET_1 - Запрос на получение типов мест для площадки с количеством их экземпляров
   * 
   * siteId - обязательное поле
   */
  'getPlaceTypesBySiteUsingGET_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceTypesBySiteUsingGET1.Responses.$200>
  /**
   * createPlaceTypeUsingPOST - createPlaceType
   */
  'createPlaceTypeUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreatePlaceTypeUsingPOST.Responses.$200>
  /**
   * getAvailablePlaceTypesByParentPlaceIdUsingGET - getAvailablePlaceTypesByParentPlaceId
   */
  'getAvailablePlaceTypesByParentPlaceIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAvailablePlaceTypesByParentPlaceIdUsingGET.Responses.$200>
  /**
   * codeExistsUsingPOST_1 - codeExists
   */
  'codeExistsUsingPOST_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CodeExistsUsingPOST1.Responses.$200>
  /**
   * placeTypesUploadUsingPOST - Запрос на загрузку файла типов мест с характеристиками
   * 
   * placesFile, siteId - обязательные поля
   */
  'placeTypesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getPlaceTypeUsingGET - getPlaceType
   */
  'getPlaceTypeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPlaceTypeUsingGET.Responses.$200>
  /**
   * updatePlaceTypeUsingPUT - updatePlaceType
   */
  'updatePlaceTypeUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdatePlaceTypeUsingPUT.Responses.$200>
  /**
   * getZoneTypesUsingGET_2 - getZoneTypes
   */
  'getZoneTypesUsingGET_2'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneTypesUsingGET2.Responses.$200>
  /**
   * createUsingPOST_3 - create
   */
  'createUsingPOST_3'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateUsingPOST3.Responses.$200>
  /**
   * codeExistsUsingPOST_4 - codeExists
   */
  'codeExistsUsingPOST_4'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CodeExistsUsingPOST4.Responses.$200>
  /**
   * zoneTypesUploadUsingPOST - Запрос на загрузку файла с типами зон
   * 
   * zoneTypeFile - обязательные поля
   */
  'zoneTypesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getZoneTypesWithZonesUsingGET_1 - getZoneTypesWithZones
   */
  'getZoneTypesWithZonesUsingGET_1'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneTypesWithZonesUsingGET1.Responses.$200>
  /**
   * getZoneTypeUsingGET - getZoneType
   */
  'getZoneTypeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneTypeUsingGET.Responses.$200>
  /**
   * updateUsingPUT_3 - update
   */
  'updateUsingPUT_3'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateUsingPUT3.Responses.$200>
  /**
   * getZonesByTypeUsingGET - getZonesByType
   */
  'getZonesByTypeUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZonesByTypeUsingGET.Responses.$200>
  /**
   * createZoneUsingPOST - create a Zone
   * 
   * Create a Zone. code, siteId, typeId are required fields.
   */
  'createZoneUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateZoneUsingPOST.Responses.$200>
  /**
   * codeExistsUsingPOST_3 - codeExists
   */
  'codeExistsUsingPOST_3'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CodeExistsUsingPOST3.Responses.$200>
  /**
   * getZonesBySiteIdAndTypeIdUsingGET - getZonesBySiteIdAndTypeId
   */
  'getZonesBySiteIdAndTypeIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZonesBySiteIdAndTypeIdUsingGET.Responses.$200>
  /**
   * zonesUploadUsingPOST - Запрос на загрузку файла с зонами
   * 
   * zonesFile, siteId - обязательные поля
   */
  'zonesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * pickingZonesUploadUsingPOST - Запрос на загрузку файла с областями комплектования
   * 
   * zonesFile, siteId - обязательные поля
   */
  'pickingZonesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * storageZonesUploadUsingPOST - Запрос на загрузку файла со складскими участками
   * 
   * zonesFile, siteId - обязательные поля
   */
  'storageZonesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getZoneByIdUsingGET - getZoneById
   */
  'getZoneByIdUsingGET'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetZoneByIdUsingGET.Responses.$200>
  /**
   * updateZoneUsingPUT - update the Zone
   * 
   * Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
   */
  'updateZoneUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateZoneUsingPUT.Responses.$200>
  /**
   * createTransferPlanAllocationUsingPOST - создать план перемещения между зонами
   */
  'createTransferPlanAllocationUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getRouteUsingPOST - getRoute
   */
  'getRouteUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRouteUsingPOST.Responses.$200>
  /**
   * createTransferPlanPickingUsingPOST - создать план перемещения между зонами
   */
  'createTransferPlanPickingUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * moveCarrierToPlaceUsingPUT - moveCarrierToPlace
   */
  'moveCarrierToPlaceUsingPUT'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.MoveCarrierToPlaceUsingPUT.Responses.$200>
  /**
   * uploadWriteOffSettingsUsingPOST - Запрос на загрузку файла с настройками списания
   * 
   * settingsFile, siteId - обязательные поля
   */
  'uploadWriteOffSettingsUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * uploadZoneCharacteristicsUsingPOST - Запрос на загрузку файла с настройкой приоритетов секторов комплектования
   * 
   * zoneCharacteristics, siteCode - обязательные поля
   */
  'uploadZoneCharacteristicsUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * bindToRouteStrategyUsingPOST - Запрос на загрузку файла с привязкой стратегии обхода змейки
   * 
   * zoneRouteStrategy, siteId - обязательные поля
   */
  'bindToRouteStrategyUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * zoneTransferRulesUploadUsingPOST - Запрос на загрузку файла правил переходов между местами
   * 
   * zoneTransferRuleFile, siteId - обязательные поля
   */
  'zoneTransferRulesUploadUsingPOST'(
    parameters?: Parameters<UnknownParamsObject>,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
}

export interface PathsDictionary {
  ['/v1/allocation-settings/upload']: {
    /**
     * uploadAllocationSettingsUsingPOST - Запрос на загрузку файла с настройками миксования
     * 
     * settingsFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/allocations/monoPalletProductPlacement']: {
    /**
     * monoPalletProductPlacementUsingPOST - Запрос в МРТ на размещение монопаллеты товара
     * 
     * operationId, productId, productBatchId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.MonoPalletProductPlacementUsingPOST.Responses.$200>
  }
  ['/v1/allocations/productPlacement']: {
    /**
     * productPlacementUsingPOST - Запрос в МРТ на обычное (не паллетное) размещение товара
     * 
     * operationId, productId, productBatchId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ProductPlacementUsingPOST.Responses.$200>
  }
  ['/v1/allocations/rawPlacement']: {
    /**
     * rawPlacementUsingPOST - Запрос в МРТ на размещение сырья
     * 
     * operationId, productId, productBatchId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RawPlacementUsingPOST.Responses.$200>
  }
  ['/v1/allocations/settings']: {
    /**
     * getSettingsForSiteAndPlaceTypeUsingGET - Запрос в МРТ на получение настроек миксования
     * 
     * siteCode, placeTypeCode - обязательные поля
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSettingsForSiteAndPlaceTypeUsingGET.Responses.$200>
    /**
     * updateAllocationSettingsUsingPOST - Запрос в МРТ для изменения настроек миксования
     * 
     * siteCode, placeTypeCode, maxMixedProducts  - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateAllocationSettingsUsingPOST.Responses.$200>
  }
  ['/v1/box-routes/build']: {
    /**
     * boxRouteUsingPOST - Запрос в МKТ для маршрутизации коробов комплектации
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.BoxRouteUsingPOST.Responses.$200>
  }
  ['/v1/picker-routes/build']: {
    /**
     * pickerRoutesUsingPOST - Запрос в МKТ для текущего маршрута комплектовщика
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PickerRoutesUsingPOST.Responses.$200>
  }
  ['/v1/picker-transfer/build']: {
    /**
     * findTransferUsingPOST - Запрос в МKТ для перехода между областями действий
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.FindTransferUsingPOST.Responses.$200>
  }
  ['/v1/picking/consolidation-zone']: {
    /**
     * getConsolidationZoneUsingGET - Запрос для получения области консолидации для области комплектации
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetConsolidationZoneUsingGET.Responses.$200>
  }
  ['/v1/product-batch']: {
    /**
     * getAllUsingGET_1 - getAll
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAllUsingGET1.Responses.$200>
    /**
     * assignBatchUsingPOST - assignBatch
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AssignBatchUsingPOST.Responses.$200>
  }
  ['/v1/product-batch/balance/{productBatchId}/difference']: {
    /**
     * getBatchBalanceDifferenceUsingGET - getBatchBalanceDifference
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBatchBalanceDifferenceUsingGET.Responses.$200>
  }
  ['/v1/product-batch/balance/{productBatchId}/fix']: {
    /**
     * fixBatchBalanceDifferenceUsingPOST - fixBatchBalanceDifference
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/product-batch/confirm']: {
    /**
     * confirmBatchUsingPOST - "Обеление" вида запаса
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/product-batch/search']: {
    /**
     * searchUsingPOST_1 - Search Batches
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchUsingPOST1.Responses.$200>
  }
  ['/v1/product-batch/upload']: {
    /**
     * uploadProductBatchesUsingPOST - Запрос на загрузку файла с партиями товара
     * 
     * batchesFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/product-batch/{id}']: {
    /**
     * getByIdUsingGET_1 - getById
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetByIdUsingGET1.Responses.$200>
  }
  ['/v1/replenishment/replenish']: {
    /**
     * replenishUsingPOST - Размещение товара
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/replenishment/transfer/build']: {
    /**
     * findTransferUsingPOST_1 - Запрос на построение и резервирование маршрута для перехода между областями действий
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.FindTransferUsingPOST1.Responses.$200>
  }
  ['/v1/replenishment/transfer/{transferId}']: {
    /**
     * getTransferUsingGET - Запрос на получение маршрута для перехода между областями действий
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTransferUsingGET.Responses.$200>
  }
  ['/v1/replenishment/tryAllocateSpace']: {
    /**
     * tryAllocateSpaceUsingPOST - Проверка места перед размещением товара
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.TryAllocateSpaceUsingPOST.Responses.$200>
  }
  ['/v1/replenishment/tryAllocateSpaceManual']: {
    /**
     * tryAllocateSpaceManualUsingPOST - Проверка места перед ручным перемещением товара
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.TryAllocateSpaceManualUsingPOST.Responses.$200>
  }
  ['/v1/route-start-rules/upload']: {
    /**
     * routeStartRulesUploadUsingPOST - Запрос на загрузку файла правил мест старта
     * 
     * routeStartRuleFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/route-strategy/upload']: {
    /**
     * uploadRouteStrategyUsingPOST - Запрос на загрузку файла с приоритетом обхода секций
     * 
     * routeStrategyFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/routes/{transferId}']: {
    /**
     * slottingRouteUsingGET - Запрос маршрута слотчика по области действий
     * 
     * Параметр currentPlaceId сейчас не используется, есть в ТЗ ("Маршруты в области действия", КП 102), может понадобиться в дальнейшем
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SlottingRouteUsingGET.Responses.$200>
  }
  ['/v1/snapshot/{entityId}']: {
    /**
     * triggerSnapshotUsingPOST - triggerSnapshot
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock']: {
    /**
     * getAllUsingGET_3 - getAll
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAllUsingGET3.Responses.$200>
  }
  ['/v1/stock/addressAndProductIdsSearch']: {
    /**
     * searchUsingPOST_2 - Search Stocks by Addresses and Product Ids
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchUsingPOST2.Responses.$200>
  }
  ['/v1/stock/carrier-types']: {
    /**
     * getCarrierTypesUsingGET - getCarrierTypes
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCarrierTypesUsingGET.Responses.$200>
    /**
     * createUsingPOST_1 - create
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateUsingPOST1.Responses.$200>
  }
  ['/v1/stock/carrier-types/codeExists']: {
    /**
     * codeExistsUsingPOST - codeExists
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CodeExistsUsingPOST.Responses.$200>
  }
  ['/v1/stock/carrier-types/upload']: {
    /**
     * carrierTypesUploadUsingPOST - Запрос на загрузку файла с типами носителя
     * 
     * carrierTypeFile - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/carrier-types/{carrierTypeId}']: {
    /**
     * updateUsingPUT - update
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateUsingPUT.Responses.$200>
  }
  ['/v1/stock/carrier-types/{id}']: {
    /**
     * getCarrierTypeUsingGET - getCarrierType
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCarrierTypeUsingGET.Responses.$200>
  }
  ['/v1/stock/carriers']: {
    /**
     * getAllUsingGET - getAll
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAllUsingGET.Responses.$200>
    /**
     * createUsingPOST - create a Carrier
     * 
     * Create a Carrier. Topology placeId, carrierNumber, carrierTypeId are required fields.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateUsingPOST.Responses.$200>
  }
  ['/v1/stock/carriers-view']: {
    /**
     * getByPlaceIdUsingGET - Get carriers hierarchy in place for grid representation
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetByPlaceIdUsingGET.Responses.$200>
  }
  ['/v1/stock/carriers/dropReservationForProcess']: {
    /**
     * dropReservationForProcessUsingPOST - Сброс резерва носителя под процесс
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/carriers/find']: {
    /**
     * getAllUsingPOST - getAll
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAllUsingPOST.Responses.$200>
  }
  ['/v1/stock/carriers/reserveForProcess']: {
    /**
     * reserveForProcessUsingPOST - Резервирование носителя под процесс
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ReserveForProcessUsingPOST.Responses.$200>
  }
  ['/v1/stock/carriers/ship']: {
    /**
     * shipUsingPOST - Отгрузка носителя
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ShipUsingPOST.Responses.$200>
  }
  ['/v1/stock/carriers/upload']: {
    /**
     * carriersUploadUsingPOST - Запрос на загрузку файла с носителями
     * 
     * carrierFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/carriers/{carrierId}']: {
    /**
     * addStockToCarrierUsingPUT - Добавление запаса в носитель
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AddStockToCarrierUsingPUT.Responses.$200>
  }
  ['/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}']: {
    /**
     * moveCarrierIntoCarrierUsingPUT - moveCarrierIntoCarrier
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/carriers/{id}']: {
    /**
     * getByIdUsingGET - getById
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetByIdUsingGET.Responses.$200>
    /**
     * deleteUsingDELETE - delete a Carrier
     * 
     * Delete a Carrier. CarrierId are required.
     */
    'delete'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/changeRawStockType']: {
    /**
     * confirmStocksUsingPOST - Отпуск сырья
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/findAll']: {
    /**
     * findAllUsingPOST - Детальный поиск по стокам
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.FindAllUsingPOST.Responses.$200>
  }
  ['/v1/stock/moveAllTo']: {
    /**
     * moveStockToCarrierUsingPUT - Перемещение запаса в носитель с корректировкой резерва
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.MoveStockToCarrierUsingPUT.Responses.$200>
  }
  ['/v1/stock/moveStock']: {
    /**
     * moveStockUsingPOST - Перемещение запаса (без проверок, без корректировки резервов)
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.MoveStockUsingPOST.Responses.$200>
  }
  ['/v1/stock/outboundOrder']: {
    /**
     * outboundOrderUsingPOST - Отгрузка заказов
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.OutboundOrderUsingPOST.Responses.$200>
  }
  ['/v1/stock/places/reduceIncomingQuantity']: {
    /**
     * reduceIncomingQuantityUsingPOST - Запрос на корректировку резервов
     * 
     * reduce incoming quantity
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/places/reservation/cancelByStock']: {
    /**
     * cancelStockReservationUsingPOST - Точечная отмена резерва
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CancelStockReservationUsingPOST.Responses.$200>
  }
  ['/v1/stock/places/reservation/for-carriers']: {
    /**
     * reserveUsingPOST - Резервирование мест под носители
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ReserveUsingPOST.Responses.$200>
    /**
     * dropReservationUsingDELETE - Сброс резерва места под носитель
     */
    'delete'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/places/reservation/reduce-for-carriers']: {
    /**
     * reduceReservationForTransferUsingPUT - Уменьшить резерв под носители в месте
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/places/reservations']: {
    /**
     * getAllUsingGET_2 - Запрос на получение списка резервов
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAllUsingGET2.Responses.$200>
  }
  ['/v1/stock/places/reserveOutgoingQuantity']: {
    /**
     * reserveOutgoingUsingPOST - Создание исходящего резерва на запас по id
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ReserveOutgoingUsingPOST.Responses.$200>
  }
  ['/v1/stock/places/transfers/drop']: {
    /**
     * dropTransferUsingPOST - Отмена трансфера
     * 
     * Диагностический эндпоинт, предназначен только для служебных целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DropTransferUsingPOST.Responses.$200>
  }
  ['/v1/stock/search']: {
    /**
     * searchUsingPOST_3 - Search Stocks
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchUsingPOST3.Responses.$200>
  }
  ['/v1/stock/stock-in']: {
    /**
     * stockInUsingPOST - Добавление найденного товара в место или носитель без проверок
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.StockInUsingPOST.Responses.$200>
  }
  ['/v1/stock/stock-types']: {
    /**
     * getStockTypesUsingGET - Запрос списка видов запаса
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetStockTypesUsingGET.Responses.$200>
    /**
     * createStockTypeUsingPOST - Создание нового справочника вида запаса
     * 
     * code - обязательное поле
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateStockTypeUsingPOST.Responses.$200>
  }
  ['/v1/stock/stock-types/code/{code}']: {
    /**
     * getStockTypeByCodeUsingGET - Запрос вида запаса по коду
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetStockTypeByCodeUsingGET.Responses.$200>
  }
  ['/v1/stock/stock-types/codeExists/{code}']: {
    /**
     * codeExistsUsingGET - Проверка существует ли вид запаса с указанным кодом
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CodeExistsUsingGET.Responses.$200>
  }
  ['/v1/stock/stock-types/{id}']: {
    /**
     * getStockTypeByIdUsingGET - Запрос вида запаса по идентификатору
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetStockTypeByIdUsingGET.Responses.$200>
    /**
     * updateStockTypeUsingPUT - Обновление вида запаса
     * 
     * code - обязательное поле
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateStockTypeUsingPUT.Responses.$200>
    /**
     * archiveStockTypeUsingDELETE - Установка признака заархивирован
     */
    'delete'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/try-take']: {
    /**
     * tryTakeUsingPOST - Запрос на попытку взять товар из ячейки
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.TryTakeUsingPOST.Responses.$200>
  }
  ['/v1/stock/updateExpired']: {
    /**
     * updateExpiredStockTypeUsingPOST - Перевод всех товаров с истекающим сроком реализации в SS
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/updateStockTypeByInventory']: {
    /**
     * updateStockTypeInStockByInventoryUsingPOST - Изменение вида запаса инвентаризацией
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateStockTypeInStockByInventoryUsingPOST.Responses.$200>
  }
  ['/v1/stock/updateStockTypeByPicking']: {
    /**
     * updateStockTypeInStockByPickingUsingPOST - Изменение вида запаса комплектацией
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateStockTypeInStockByPickingUsingPOST.Responses.$200>
  }
  ['/v1/stock/updateStockTypeByUI']: {
    /**
     * updateStockTypeInStockByUIUsingPOST - Изменение вида запаса через UI
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/updateStockTypeForBatch']: {
    /**
     * updateStockTypeForBatchUsingPOST - Изменение вида запаса для партии. Вызывается после обеления.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/upload']: {
    /**
     * stocksUploadUsingPOST - Запрос на загрузку файла с запасами
     * 
     * stocksFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/writeOff']: {
    /**
     * writeOffStockUsingPOST - Списание запаса
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.WriteOffStockUsingPOST.Responses.$200>
  }
  ['/v1/stock/writeOffLost']: {
    /**
     * writeOffLostStockUsingPOST - Списание потерянного запаса
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.WriteOffLostStockUsingPOST.Responses.$200>
  }
  ['/v1/stock/writeOffRaw']: {
    /**
     * writeOffRawStockUsingPOST - Отпуск сырья
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/stock/{id}']: {
    /**
     * getByIdUsingGET_2 - getById
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetByIdUsingGET2.Responses.$200>
  }
  ['/v1/stock/{stockId}/moveTo/{carrierId}']: {
    /**
     * moveStockToCarrierUsingPUT_1 - Перемещение запаса в носитель с корректировкой резерва
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.MoveStockToCarrierUsingPUT1.Responses.$200>
  }
  ['/v1/tool/outboundOrder']: {
    /**
     * outboundOrderUsingPOST_1 - Отгрузка заказов
     * 
     * Системный эндпоинт, предназначен для переотправки отгрузок после сбоя
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.OutboundOrderUsingPOST1.Responses.$200>
  }
  ['/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones']: {
    /**
     * getZoneTypesUsingGET - getZoneTypes
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneTypesUsingGET.Responses.$200>
  }
  ['/v1/topology/dictionary/sites/{siteId}/placeTypes']: {
    /**
     * placeTypesUsingGET - placeTypes
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PlaceTypesUsingGET.Responses.$200>
  }
  ['/v1/topology/dictionary/zoneTypes']: {
    /**
     * getZoneTypesUsingGET_1 - getZoneTypes
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneTypesUsingGET1.Responses.$200>
  }
  ['/v1/topology/placeStatuses']: {
    /**
     * getPlaceStatusesUsingGET - getPlaceStatuses
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceStatusesUsingGET.Responses.$200>
  }
  ['/v1/topology/placeTypes/{placeTypeId}/characteristics']: {
    /**
     * getPlaceTypeCharacteristicsUsingGET - getPlaceTypeCharacteristics
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceTypeCharacteristicsUsingGET.Responses.$200>
    /**
     * addUsingPOST - add
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AddUsingPOST.Responses.$200>
  }
  ['/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}']: {
    /**
     * getPlaceTypeCharacteristicUsingGET - getPlaceTypeCharacteristic
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceTypeCharacteristicUsingGET.Responses.$200>
    /**
     * updateUsingPUT_2 - update
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateUsingPUT2.Responses.$200>
    /**
     * removeUsingDELETE - remove
     */
    'delete'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places']: {
    /**
     * getPlaceUsingGET - getPlace
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceUsingGET.Responses.$200>
    /**
     * createUsingPOST_2 - create
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateUsingPOST2.Responses.$200>
  }
  ['/v1/topology/places/bulk/update-place-status']: {
    /**
     * startBulkUpdatePlaceStatusUsingPOST - startBulkUpdatePlaceStatus
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.StartBulkUpdatePlaceStatusUsingPOST.Responses.$200>
  }
  ['/v1/topology/places/bulk/update-place-type']: {
    /**
     * startBulkUpdatePlaceTypeUsingPOST - startBulkUpdatePlaceType
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.StartBulkUpdatePlaceTypeUsingPOST.Responses.$200>
  }
  ['/v1/topology/places/bulk/update-zone']: {
    /**
     * startBulkUpdateZoneUsingPOST - startBulkUpdateZone
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.StartBulkUpdateZoneUsingPOST.Responses.$200>
  }
  ['/v1/topology/places/bulk/{operationId}']: {
    /**
     * getBulkUpdateOperationDataUsingGET - getBulkUpdateOperationData
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBulkUpdateOperationDataUsingGET.Responses.$200>
  }
  ['/v1/topology/places/download/{siteCode}']: {
    /**
     * placesDownloadUsingGET - Запрос на выгрузку файла с топологией
     * 
     * siteId - обязательные поля
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places/dropAllReservationsForProcess']: {
    /**
     * dropAllReservationsForProcessUsingPOST - Сброс все резервы места под процесс
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places/dropReservationForProcess']: {
    /**
     * dropReservationForProcessUsingPOST_1 - Сброс резерва места под процесс
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places/findByProcessId']: {
    /**
     * findPlacesByProcessIdUsingGET - findPlacesByProcessId
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.FindPlacesByProcessIdUsingGET.Responses.$200>
  }
  ['/v1/topology/places/hierarchy/{siteId}']: {
    /**
     * getHierarchyUsingGET - getHierarchy
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetHierarchyUsingGET.Responses.$200>
  }
  ['/v1/topology/places/hierarchy/{siteId}/{parentId}']: {
    /**
     * getHierarchyForParentIdUsingGET - getHierarchyForParentId
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetHierarchyForParentIdUsingGET.Responses.$200>
  }
  ['/v1/topology/places/hierarchyTree/{siteId}']: {
    /**
     * getHierarchyByTypeCodeUsingGET - getHierarchyByTypeCode
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetHierarchyByTypeCodeUsingGET.Responses.$200>
  }
  ['/v1/topology/places/reserveForProcess']: {
    /**
     * reserveForProcessUsingPOST_1 - Резервирование места под процесс
     * 
     * Диагностический эндпоинт, предназначен только для тестовых целей
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places/search']: {
    /**
     * searchUsingPOST - Search Places
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchUsingPOST.Responses.$200>
  }
  ['/v1/topology/places/search/dicts/placeTypes/{siteId}']: {
    /**
     * getPlaceTypesBySiteUsingGET - getPlaceTypesBySite
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceTypesBySiteUsingGET.Responses.$200>
  }
  ['/v1/topology/places/search/dicts/zoneTypes/{siteId}']: {
    /**
     * getZoneTypesWithZonesUsingGET - getZoneTypesWithZones
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneTypesWithZonesUsingGET.Responses.$200>
  }
  ['/v1/topology/places/shortView']: {
    /**
     * getPlaceByCodeUsingGET - getPlaceByCode
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceByCodeUsingGET.Responses.$200>
  }
  ['/v1/topology/places/transfer/{siteId}']: {
    /**
     * getTransferPlaceUsingGET - getTransferPlace
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTransferPlaceUsingGET.Responses.$200>
  }
  ['/v1/topology/places/upload']: {
    /**
     * placesUploadUsingPOST - Запрос на загрузку файла с топологией
     * 
     * placesFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places/{id}']: {
    /**
     * getPlaceByIdUsingGET - getPlaceById
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceByIdUsingGET.Responses.$200>
    /**
     * updateUsingPUT_1 - update
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateUsingPUT1.Responses.$200>
    /**
     * deleteUsingDELETE_1 - delete
     */
    'delete'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/places/{placeId}/characteristics']: {
    /**
     * getPlaceCharacteristicsUsingGET - getPlaceCharacteristics
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceCharacteristicsUsingGET.Responses.$200>
  }
  ['/v1/topology/places/{placeId}/characteristics/{characteristicId}']: {
    /**
     * getPlaceCharacteristicUsingGET - getPlaceCharacteristic
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceCharacteristicUsingGET.Responses.$200>
    /**
     * overrideUsingPUT - override
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.OverrideUsingPUT.Responses.$200>
  }
  ['/v1/topology/sites']: {
    /**
     * getSitesUsingGET - getSites
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSitesUsingGET.Responses.$200>
    /**
     * createSiteUsingPOST - createSite
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateSiteUsingPOST.Responses.$200>
  }
  ['/v1/topology/sites/byCode/{code}']: {
    /**
     * getSiteByCodeUsingGET - getSiteByCode
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSiteByCodeUsingGET.Responses.$200>
  }
  ['/v1/topology/sites/codeExists']: {
    /**
     * codeExistsUsingPOST_2 - codeExists
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CodeExistsUsingPOST2.Responses.$200>
  }
  ['/v1/topology/sites/settings/{siteCode}']: {
    /**
     * getSettingsForSiteUsingGET - getSettingsForSite
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSettingsForSiteUsingGET.Responses.$200>
    /**
     * updateSettingsForSiteUsingPUT - updateSettingsForSite
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateSettingsForSiteUsingPUT.Responses.$200>
  }
  ['/v1/topology/sites/{siteId}']: {
    /**
     * getSiteUsingGET - getSite
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSiteUsingGET.Responses.$200>
    /**
     * updateSiteUsingPUT - updateSite
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateSiteUsingPUT.Responses.$200>
  }
  ['/v1/topology/sites/{siteId}/placeTypes']: {
    /**
     * getPlaceTypesBySiteUsingGET_1 - Запрос на получение типов мест для площадки с количеством их экземпляров
     * 
     * siteId - обязательное поле
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceTypesBySiteUsingGET1.Responses.$200>
    /**
     * createPlaceTypeUsingPOST - createPlaceType
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreatePlaceTypeUsingPOST.Responses.$200>
  }
  ['/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId']: {
    /**
     * getAvailablePlaceTypesByParentPlaceIdUsingGET - getAvailablePlaceTypesByParentPlaceId
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAvailablePlaceTypesByParentPlaceIdUsingGET.Responses.$200>
  }
  ['/v1/topology/sites/{siteId}/placeTypes/codeExists']: {
    /**
     * codeExistsUsingPOST_1 - codeExists
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CodeExistsUsingPOST1.Responses.$200>
  }
  ['/v1/topology/sites/{siteId}/placeTypes/upload']: {
    /**
     * placeTypesUploadUsingPOST - Запрос на загрузку файла типов мест с характеристиками
     * 
     * placesFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}']: {
    /**
     * getPlaceTypeUsingGET - getPlaceType
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPlaceTypeUsingGET.Responses.$200>
    /**
     * updatePlaceTypeUsingPUT - updatePlaceType
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdatePlaceTypeUsingPUT.Responses.$200>
  }
  ['/v1/topology/zone-types']: {
    /**
     * getZoneTypesUsingGET_2 - getZoneTypes
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneTypesUsingGET2.Responses.$200>
    /**
     * createUsingPOST_3 - create
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateUsingPOST3.Responses.$200>
  }
  ['/v1/topology/zone-types/codeExists']: {
    /**
     * codeExistsUsingPOST_4 - codeExists
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CodeExistsUsingPOST4.Responses.$200>
  }
  ['/v1/topology/zone-types/upload']: {
    /**
     * zoneTypesUploadUsingPOST - Запрос на загрузку файла с типами зон
     * 
     * zoneTypeFile - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/zone-types/withZones']: {
    /**
     * getZoneTypesWithZonesUsingGET_1 - getZoneTypesWithZones
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneTypesWithZonesUsingGET1.Responses.$200>
  }
  ['/v1/topology/zone-types/{id}']: {
    /**
     * getZoneTypeUsingGET - getZoneType
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneTypeUsingGET.Responses.$200>
  }
  ['/v1/topology/zone-types/{zoneTypeId}']: {
    /**
     * updateUsingPUT_3 - update
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateUsingPUT3.Responses.$200>
  }
  ['/v1/topology/zones']: {
    /**
     * getZonesByTypeUsingGET - getZonesByType
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZonesByTypeUsingGET.Responses.$200>
    /**
     * createZoneUsingPOST - create a Zone
     * 
     * Create a Zone. code, siteId, typeId are required fields.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateZoneUsingPOST.Responses.$200>
  }
  ['/v1/topology/zones/codeExists']: {
    /**
     * codeExistsUsingPOST_3 - codeExists
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CodeExistsUsingPOST3.Responses.$200>
  }
  ['/v1/topology/zones/projections']: {
    /**
     * getZonesBySiteIdAndTypeIdUsingGET - getZonesBySiteIdAndTypeId
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZonesBySiteIdAndTypeIdUsingGET.Responses.$200>
  }
  ['/v1/topology/zones/upload']: {
    /**
     * zonesUploadUsingPOST - Запрос на загрузку файла с зонами
     * 
     * zonesFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/zones/upload/ok']: {
    /**
     * pickingZonesUploadUsingPOST - Запрос на загрузку файла с областями комплектования
     * 
     * zonesFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/zones/upload/su']: {
    /**
     * storageZonesUploadUsingPOST - Запрос на загрузку файла со складскими участками
     * 
     * zonesFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/topology/zones/{id}']: {
    /**
     * getZoneByIdUsingGET - getZoneById
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetZoneByIdUsingGET.Responses.$200>
    /**
     * updateZoneUsingPUT - update the Zone
     * 
     * Update the Zone. id, siteId, code are required fields. code, name, description are fields for edit.
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateZoneUsingPUT.Responses.$200>
  }
  ['/v1/transfer/allocation/createTransferPlan']: {
    /**
     * createTransferPlanAllocationUsingPOST - создать план перемещения между зонами
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/transfer/getRoute/{carrierId}']: {
    /**
     * getRouteUsingPOST - getRoute
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRouteUsingPOST.Responses.$200>
  }
  ['/v1/transfer/picking/createPlan']: {
    /**
     * createTransferPlanPickingUsingPOST - создать план перемещения между зонами
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/transfer/{carrierId}/moveTo/{placeId}']: {
    /**
     * moveCarrierToPlaceUsingPUT - moveCarrierToPlace
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.MoveCarrierToPlaceUsingPUT.Responses.$200>
  }
  ['/v1/write-off-settings/upload']: {
    /**
     * uploadWriteOffSettingsUsingPOST - Запрос на загрузку файла с настройками списания
     * 
     * settingsFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/zone-characteristics/upload']: {
    /**
     * uploadZoneCharacteristicsUsingPOST - Запрос на загрузку файла с настройкой приоритетов секторов комплектования
     * 
     * zoneCharacteristics, siteCode - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/zone-route-strategy/upload']: {
    /**
     * bindToRouteStrategyUsingPOST - Запрос на загрузку файла с привязкой стратегии обхода змейки
     * 
     * zoneRouteStrategy, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/v1/zone-transfer-rules/upload']: {
    /**
     * zoneTransferRulesUploadUsingPOST - Запрос на загрузку файла правил переходов между местами
     * 
     * zoneTransferRuleFile, siteId - обязательные поля
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject>,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
