declare namespace Custom {
    /**
     * AddPlaceTypeCommonCharacteristicDto
     */
    export interface AddPlaceTypeCommonCharacteristicDto {
        characteristic?: string;
        defaultValue?: DefaultValue;
        settings?: CommonCharacteristicSettings;
    }
    /**
     * AddStockToCarrierCmd
     */
    export interface AddStockToCarrierCmd {
        baseAmount?: number;
        baseUnit?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        stockType?: string;
        tabNumber?: string;
        unit?: string;
        userName?: string;
    }
    /**
     * AllocationSettingsDto
     */
    export interface AllocationSettingsDto {
        maxMixedProducts?: number; // int32
        placeTypeCode?: string;
        zoneCode?: string;
    }
    /**
     * AllocationZoneTransferReq
     */
    export interface AllocationZoneTransferReq {
        carrierId?: string; // uuid
        destinationPlaceId?: string; // uuid
        operationId?: string;
        startPlaceId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * AssignProductBatchCmd
     */
    export interface AssignProductBatchCmd {
        expirationTime?: string; // date-time
        inboundId?: string; // uuid
        inboundTime?: string; // date-time
        manufactureTime?: string; // date-time
        mercuryExtId?: string;
        number?: string;
        productId?: string;
        productName?: string;
        siteCode?: string;
        type?: "REGULAR" | "RETURN";
        unit?: string;
        vendorCode?: string;
        vendorName?: string;
        weightProduct?: boolean;
    }
    /**
     * BoxRoute
     */
    export interface BoxRoute {
        operationId?: string;
        plannedTotalDuration?: number; // int32
        rejectedItems?: ProductAmountDto[];
        steps?: RouteStep[];
        transferId?: string; // uuid
    }
    /**
     * BuildBoxRouteCmd
     */
    export interface BuildBoxRouteCmd {
        items?: ProductAmountDto[];
        operationId?: string;
        siteCode?: string;
        transferId?: string; // uuid
    }
    /**
     * BulkOperationDto
     */
    export interface BulkOperationDto {
        operationId?: string;
        status?: string;
    }
    /**
     * BulkPlaceUpdateStatusCmd
     */
    export interface BulkPlaceUpdateStatusCmd {
        operationId?: string;
        placeIds?: string /* uuid */ [];
        statusCode?: string;
    }
    /**
     * BulkPlaceUpdateTypeCmd
     */
    export interface BulkPlaceUpdateTypeCmd {
        operationId?: string;
        placeIds?: string /* uuid */ [];
        targetPlaceTypeId?: string; // uuid
    }
    /**
     * BulkPlaceUpdateZoneCmd
     */
    export interface BulkPlaceUpdateZoneCmd {
        operationId?: string;
        placeIds?: string /* uuid */ [];
        zoneId?: string; // uuid
    }
    /**
     * CancelStockReservationCmd
     */
    export interface CancelStockReservationCmd {
        incomingQuantity?: number; // int32
        operationId?: string;
        outgoingQuantity?: number; // int32
        siteCode?: string;
        stockId?: string; // uuid
        transferId?: string; // uuid
        unit?: string;
    }
    /**
     * CarrierDto
     */
    export interface CarrierDto {
        author?: string;
        barcode?: string;
        carriersTypeId?: string; // uuid
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        nestedCarriers?: CarrierDto[];
        parentId?: string; // uuid
        parentNumber?: string;
        placeAddress?: string;
        placeId?: string; // uuid
        processId?: string;
        type?: CarrierTypeDto;
        updatedTime?: number; // int32
    }
    /**
     * CarrierGridView
     */
    export interface CarrierGridView {
        barcode?: string;
        id?: string; // uuid
        nestedCarriers?: CarrierGridView[];
        status?: "incoming" | "outgoing" | "actual";
        type?: CarrierTypeGridView;
    }
    /**
     * CarrierShippedResponse
     */
    export interface CarrierShippedResponse {
        carrierId?: string; // uuid
        carrierMoveResponses?: MoveCarrierToPlaceResponse[];
        operationId?: string;
        placeId?: string; // uuid
    }
    /**
     * CarrierTypeCodeExistsQuery
     */
    export interface CarrierTypeCodeExistsQuery {
        code?: string;
    }
    /**
     * CarrierTypeDto
     */
    export interface CarrierTypeDto {
        archived?: boolean;
        author?: string;
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        height?: number; // int32
        id?: string; // uuid
        length?: number; // int32
        maxVolume?: number;
        maxWeight?: number;
        name?: string;
        updatedTime?: number; // int32
        width?: number; // int32
    }
    /**
     * CarrierTypeGridView
     */
    export interface CarrierTypeGridView {
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * ChangeRawStock
     */
    export interface ChangeRawStock {
        position?: number; // int64
        stockId?: string; // uuid
    }
    /**
     * ChangeRawStockTypeCmd
     */
    export interface ChangeRawStockTypeCmd {
        operationId?: string;
        orderVersion?: string;
        outboundDeliveryNumber?: string;
        outboundTime?: string; // date-time
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        stocks?: ChangeRawStock[];
        tabNumber?: string;
        userName?: string;
    }
    /**
     * CharacteristicDto
     */
    export interface CharacteristicDto {
        type?: string;
        value?: ValueDto;
    }
    /**
     * CodeExists
     */
    export interface CodeExists {
        exists?: boolean;
    }
    /**
     * CommonCharacteristicSettings
     */
    export interface CommonCharacteristicSettings {
        editable?: boolean;
        mandatory?: boolean;
        parentInheritance?: "check" | "ignore";
        unit?: string;
    }
    /**
     * ConfirmProductBatchCmd
     */
    export interface ConfirmProductBatchCmd {
        batchId?: string; // uuid
        targetStockType?: string;
    }
    /**
     * Coordinates3D
     */
    export interface Coordinates3D {
        x?: number;
        y?: number;
        z?: number;
    }
    /**
     * CoordinatesDto
     */
    export interface CoordinatesDto {
        x?: number;
        y?: number;
        z?: number;
    }
    /**
     * CreateCarrierCmd
     */
    export interface CreateCarrierCmd {
        carrierBarcode?: string;
        carrierId?: string; // uuid
        carrierTypeCode?: string;
        placeBarcode?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * CreateCarrierTypeCmd
     */
    export interface CreateCarrierTypeCmd {
        code?: string;
        description?: string;
        height?: number; // int32
        id?: string; // uuid
        length?: number; // int32
        maxVolume?: number;
        maxWeight?: number;
        name?: string;
        width?: number; // int32
    }
    /**
     * CreatePlaceCmd
     */
    export interface CreatePlaceCmd {
        address?: string;
        addressTypeId?: string; // uuid
        coordinates?: Coordinates3D;
        description?: string;
        id?: string; // uuid
        number?: number; // int32
        parentId?: string; // uuid
        siteId?: string; // uuid
        status?: PlaceStatusDto;
        statusReason?: string;
        typeId?: string; // uuid
        zoneIds?: string /* uuid */ [];
    }
    /**
     * CreatePlaceTypeCmd
     */
    export interface CreatePlaceTypeCmd {
        code?: string;
        coordinatesRequired?: boolean;
        description?: string;
        maxMixBatches?: number; // int32
        name?: string;
        numerationRule?: "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP";
        placeTypeId?: string; // uuid
        storagePlace?: boolean;
    }
    /**
     * CreatePlanRequest
     */
    export interface CreatePlanRequest {
        carrierId?: string; // uuid
        startPlaceId?: string; // uuid
        targetZoneId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * CreateSiteCmd
     */
    export interface CreateSiteCmd {
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * CreateZoneCmd
     */
    export interface CreateZoneCmd {
        boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
        siteId?: string; // uuid
        typeId?: string; // uuid
    }
    /**
     * CreateZoneTypeCmd
     */
    export interface CreateZoneTypeCmd {
        code?: string;
        description?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * Data
     */
    export interface Data {
        address?: string;
        author?: string;
        childrenAmount?: number; // int32
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        status?: PlaceStatusDto;
        typeName?: string;
        updatedTime?: number; // int32
        zoneNames?: string[];
    }
    /**
     * DefaultValue
     */
    export interface DefaultValue {
        decValue?: number;
        intValue?: number; // int32
        maxIntValue?: number; // int32
        minIntValue?: number; // int32
    }
    /**
     * DetailedCarriersSearchQuery
     */
    export interface DetailedCarriersSearchQuery {
        barcodes?: string[];
    }
    /**
     * DetailedStockSearchQuery
     */
    export interface DetailedStockSearchQuery {
        carrierId?: string; // uuid
        carrierNumber?: string;
        inCarrier?: "IN_ONLY" | "OUT_ONLY" | "ALL";
        maxResponseSize?: number; // int32
        placeAddress?: string;
        placeId?: string; // uuid
        productBatchId?: string; // uuid
        productBatchNumber?: string;
        productId?: string;
        siteCode?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
    }
    /**
     * DetailedStocksSearchResult
     */
    export interface DetailedStocksSearchResult {
        stocks?: StockDto[];
        totalCount?: number; // int64
    }
    /**
     * DropAllProcessReservationsCmd
     */
    export interface DropAllProcessReservationsCmd {
        operationId?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * DropCarrierReservationForProcessCmd
     */
    export interface DropCarrierReservationForProcessCmd {
        carrierNumber?: string;
        dropForNestedCarriers?: boolean;
        operationId?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * DropPlaceReservationForProcessCmd
     */
    export interface DropPlaceReservationForProcessCmd {
        operationId?: string;
        placeBarcode?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * DropPlaceReservationsForTransferCmd
     */
    export interface DropPlaceReservationsForTransferCmd {
        operationId?: string;
        processId?: string;
        siteCode?: string;
        transferId?: string; // uuid
    }
    /**
     * DropTransferCmd
     */
    export interface DropTransferCmd {
        transferId?: string; // uuid
    }
    /**
     * Error
     */
    export interface Error {
        code?: string;
        message: string;
    }
    /**
     * FullAmount
     */
    export interface FullAmount {
        quantity?: number;
        unit?: string;
        volume?: number;
        weight?: number; // int64
    }
    /**
     * ModifyStockTypeCmd
     */
    export interface ModifyStockTypeCmd {
        code?: string;
        name?: string;
    }
    /**
     * MonoPalletPlacementRequest
     */
    export interface MonoPalletPlacementRequest {
        incomingCarrierId?: string; // uuid
        openCarriers?: string /* uuid */ [];
        operationId?: string;
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        siteCode?: string;
        stockTypeCode?: string;
    }
    /**
     * MoveAllStocksToCarrierCmd
     */
    export interface MoveAllStocksToCarrierCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        sourcePlaceId?: string; // uuid
        tabNumber?: string;
        targetCarrierId?: string; // uuid
        transferId?: string; // uuid
        userName?: string;
    }
    /**
     * MoveAllStocksToCarrierResponse
     */
    export interface MoveAllStocksToCarrierResponse {
        operationId?: string;
        stockDto?: StockDto[];
        targetCarrierId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * MoveCarrierToPlaceResponse
     */
    export interface MoveCarrierToPlaceResponse {
        carrierId?: string; // uuid
        operationId?: string;
        rejected?: boolean;
        transferComplete?: boolean;
    }
    /**
     * MoveStockCmd
     */
    export interface MoveStockCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        tabNumber?: string;
        targetCarrierId?: string; // uuid
        targetPlaceId?: string; // uuid
        userName?: string;
    }
    /**
     * MoveStockCmdResponse
     */
    export interface MoveStockCmdResponse {
        movedQuantity?: number; // int32
        operationId?: string;
        originalStockId?: string; // uuid
        targetStockActualQuantity?: number;
        targetStockId?: string; // uuid
    }
    /**
     * MoveStockToCarrierCmd
     */
    export interface MoveStockToCarrierCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        sourcePlaceId?: string; // uuid
        sourceStockId?: string; // uuid
        tabNumber?: string;
        targetCarrierId?: string; // uuid
        transferId?: string; // uuid
        unit?: string;
        userName?: string;
    }
    /**
     * NamedId
     */
    export interface NamedId {
        label?: string;
        value?: string; // uuid
    }
    /**
     * OrderBoxDto
     */
    export interface OrderBoxDto {
        carrierId?: string; // uuid
        orderItems?: OutboundOrderItemDto[];
    }
    /**
     * OrderByCriteria
     */
    export interface OrderByCriteria {
        field?: string;
        order?: "ASC" | "DESC";
    }
    /**
     * OutboundOrderCmd
     */
    export interface OutboundOrderCmd {
        operationId?: string;
        orderBoxes?: OrderBoxDto[];
        orderVersion?: string;
        outboundDeliveryNumber?: string;
        outboundTime?: string; // date-time
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        tabNumber?: string;
        unshippedOrderItems?: UnshippedOrderItemDto[];
        userName?: string;
    }
    /**
     * OutboundOrderItemDto
     */
    export interface OutboundOrderItemDto {
        baseAmount?: number;
        baseUnit?: string;
        position?: number; // int64
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number; // int32
        unit?: string;
    }
    /**
     * OverridePlaceCommonCharacteristicCmd
     */
    export interface OverridePlaceCommonCharacteristicCmd {
        characteristic?: string;
        siteId?: string; // uuid
        value?: ValueCmd;
    }
    /**
     * PageRequestInfo
     */
    export interface PageRequestInfo {
        limit?: number; // int32
        offset?: number; // int32
    }
    /**
     * PickerRouteReq
     */
    export interface PickerRouteReq {
        pickingZoneId?: string; // uuid
        siteCode?: string;
        startPlaceId?: string; // uuid
        transferIds?: string /* uuid */ [];
    }
    /**
     * PickingZoneTransferReq
     */
    export interface PickingZoneTransferReq {
        carrierId?: string; // uuid
        destinationPlaceId?: string; // uuid
        operationId?: string;
        startPlaceId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * PlaceCharacteristicDto
     */
    export interface PlaceCharacteristicDto {
        author?: string;
        characteristicId?: string; // uuid
        checkParents?: string;
        createdTime?: number; // int32
        editable?: boolean;
        editor?: string;
        mandatory?: boolean;
        placeId?: string; // uuid
        siteId?: string; // uuid
        type?: string;
        typeCharacteristicId?: string; // uuid
        updatedTime?: number; // int32
        value?: ValueDto;
    }
    /**
     * PlaceDto
     */
    export interface PlaceDto {
        address?: string;
        addressTypeId?: string; // uuid
        author?: string;
        childPlaces?: PlaceDto[];
        coordinates?: CoordinatesDto;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        number?: number; // int32
        parentId?: string; // uuid
        parentPlaces?: PlaceDto[];
        siteId?: string; // uuid
        status?: PlaceStatusDto;
        statusReason?: string;
        storagePlace?: boolean;
        typeCode?: string;
        typeId?: string; // uuid
        typeName?: string;
        updatedTime?: number; // int32
        zones?: ZoneInfo[];
    }
    /**
     * PlaceHierarchyFilter
     */
    export interface PlaceHierarchyFilter {
        hierarchyElements?: PlaceHierarchyFilterCondition[];
        siteId?: string; // uuid
    }
    /**
     * PlaceHierarchyFilterCondition
     */
    export interface PlaceHierarchyFilterCondition {
        criteria?: string;
        placeTypeId?: string; // uuid
    }
    /**
     * PlaceHierarchyTreeDto
     */
    export interface PlaceHierarchyTreeDto {
        address?: string;
        characteristics?: CharacteristicDto[];
        children?: PlaceHierarchyTreeDto[];
        id?: string; // uuid
        number?: number; // int32
        status?: string;
        type?: Type;
        zones?: Zone[];
    }
    /**
     * PlaceInfo
     */
    export interface PlaceInfo {
        address?: string;
        author?: string;
        childrenAmount?: number; // int32
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        placeZones?: PlaceZoneInfo[];
        siteId?: string; // uuid
        siteName?: string;
        typeName?: string;
        updatedTime?: number; // int32
        zoneNames?: string[];
    }
    /**
     * PlaceNodeWithPosition
     */
    export interface PlaceNodeWithPosition {
        childPlaces?: Data[];
        placeNodeChainFromTop?: NamedId[];
    }
    /**
     * PlaceSearchFilter
     */
    export interface PlaceSearchFilter {
        hierarchy?: PlaceHierarchyFilter;
        placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
        placeTypes?: string /* uuid */ [];
        zonesType?: PlaceZonesFilter[];
    }
    /**
     * PlaceShortView
     */
    export interface PlaceShortView {
        barcode?: string;
        description?: string;
        id?: string; // uuid
        siteCode?: string;
        typeCode?: string;
        typeName?: string;
    }
    /**
     * PlaceStatusDto
     */
    export interface PlaceStatusDto {
        code?: string;
        default?: boolean;
        title?: string;
    }
    /**
     * PlaceTypeCharacteristicDTO
     */
    export interface PlaceTypeCharacteristicDTO {
        author?: string;
        characteristicId?: string; // uuid
        checkParents?: string;
        createdTime?: number; // int32
        defaultValue?: DefaultValue;
        editable?: boolean;
        editor?: string;
        mandatory?: boolean;
        placeTypeId?: string; // uuid
        type?: string;
        updatedTime?: number; // int32
    }
    /**
     * PlaceTypeCodeExistsQuery
     */
    export interface PlaceTypeCodeExistsQuery {
        code?: string;
    }
    /**
     * PlaceTypeDto
     */
    export interface PlaceTypeDto {
        author?: string;
        code?: string;
        coordinatesRequired?: boolean;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        maxMixBatches?: number; // int32
        name?: string;
        numberOfImplementation?: number; // int64
        numerationRule?: string;
        placeTypeId?: string; // uuid
        siteId?: string; // uuid
        storagePlace?: boolean;
        updatedTime?: number; // int32
    }
    /**
     * PlaceWithPosition
     */
    export interface PlaceWithPosition {
        place?: PlaceDto;
        placeNodeChainFromTop?: NamedId[];
    }
    /**
     * PlaceWithType
     */
    export interface PlaceWithType {
        address?: string;
        id?: string; // uuid
        number?: number; // int32
        type?: Type;
    }
    /**
     * PlaceZoneInfo
     */
    export interface PlaceZoneInfo {
        zoneCode?: string;
        zoneName?: string;
        zoneTypeCode?: string;
        zoneTypeName?: string;
    }
    /**
     * PlaceZonesFilter
     */
    export interface PlaceZonesFilter {
        zoneTypeId?: string; // uuid
        zonesId?: string /* uuid */ [];
    }
    /**
     * PlacesSearchQuery
     */
    export interface PlacesSearchQuery {
        filter?: PlaceSearchFilter;
        orderInfo?: OrderByCriteria[];
        pageInfo?: PageRequestInfo;
    }
    /**
     * PlacesSearchResult
     */
    export interface PlacesSearchResult {
        places?: PlaceInfo[];
        totalCount?: number; // int32
    }
    /**
     * ProductAmountDto
     */
    export interface ProductAmountDto {
        productId?: string;
        quantity?: number;
        rejectionReason?: "NO_STOCK" | "NO_ROUTE";
        unit?: string;
    }
    /**
     * ProductBatchBalanceByStockType
     */
    export interface ProductBatchBalanceByStockType {
        baseAmountERP?: number;
        baseAmountWMS?: number;
        quantityERP?: number; // int32
        quantityWMS?: number;
        stockTypeCode?: string;
    }
    /**
     * ProductBatchBalanceDifferenceDto
     */
    export interface ProductBatchBalanceDifferenceDto {
        canCorrect?: boolean;
        needCorrect?: boolean;
        stockTypes?: ProductBatchBalanceByStockType[];
    }
    /**
     * ProductBatchDto
     */
    export interface ProductBatchDto {
        expirationTime?: number; // int32
        id?: string; // uuid
        inboundId?: string; // uuid
        inboundTime?: number; // int32
        manufactureTime?: number; // int32
        mercuryExtId?: string;
        number?: string;
        productId?: string;
        productName?: string;
        siteId?: string; // uuid
        type?: "REGULAR" | "RETURN";
        vendorCode?: string;
        vendorName?: string;
        weightProduct?: boolean;
    }
    /**
     * ProductBatchSearchDto
     */
    export interface ProductBatchSearchDto {
        confirmed?: boolean;
        expirationTime?: number; // int32
        id?: string; // uuid
        inboundTime?: number; // int32
        manufactureTime?: number; // int32
        productBatchNumber?: string;
        productId?: string;
        productName?: string;
        siteCode?: string;
        siteId?: string; // uuid
        type?: "REGULAR" | "RETURN";
        vendorCode?: string;
        vendorName?: string;
    }
    /**
     * ProductBatchSearchFilter
     */
    export interface ProductBatchSearchFilter {
        confirmed?: boolean;
        expirationDateFrom?: string; // date-time
        expirationDateTo?: string; // date-time
        manufactureTimeFrom?: string; // date-time
        manufactureTimeTo?: string; // date-time
        number?: string;
        productId?: string;
        siteCode?: string;
        siteId?: string; // uuid
        types?: ("REGULAR" | "RETURN")[];
    }
    /**
     * ProductBatchesSearchQuery
     */
    export interface ProductBatchesSearchQuery {
        filter?: ProductBatchSearchFilter;
        orderInfo?: OrderByCriteria[];
        pageInfo?: PageRequestInfo;
    }
    /**
     * ProductBatchesSearchResult
     */
    export interface ProductBatchesSearchResult {
        productBatches?: ProductBatchSearchDto[];
        totalCount?: number; // int64
    }
    /**
     * ProductPlacementRequest
     */
    export interface ProductPlacementRequest {
        openCarriers?: string /* uuid */ [];
        operationId?: string;
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        siteCode?: string;
        stockTypeCode?: string;
    }
    /**
     * ProductPlacementResponse
     */
    export interface ProductPlacementResponse {
        clientError?: Error;
        operationId?: string;
        productId?: string;
        rejectedInfo?: RejectedInfo;
        reservedCarriers?: ReservedCarrier[];
    }
    /**
     * RawPlacementRequest
     */
    export interface RawPlacementRequest {
        incomingCarrierId?: string; // uuid
        openCarriers?: string /* uuid */ [];
        operationId?: string;
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number;
        siteCode?: string;
        stockTypeCode?: string;
    }
    /**
     * RawStock
     */
    export interface RawStock {
        position?: number; // int64
        quantity?: number;
        stockId?: string; // uuid
    }
    /**
     * ReduceIncomingQuantityCmd
     */
    export interface ReduceIncomingQuantityCmd {
        operationId?: string;
        productBatchId?: string; // uuid
        quantity?: number; // int32
        stockTypeCode?: string;
        transferId?: string; // uuid
    }
    /**
     * ReducePlaceReservationForTransferCmd
     */
    export interface ReducePlaceReservationForTransferCmd {
        carrierTypeCode?: string;
        operationId?: string;
        placeId?: string; // uuid
        quantity?: number; // int32
        siteCode?: string;
        transferId?: string; // uuid
    }
    /**
     * RejectedInfo
     */
    export interface RejectedInfo {
        clientError?: Error;
        quantity?: number;
    }
    /**
     * ReplenishCmd
     */
    export interface ReplenishCmd {
        destPlaceBarcode?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        productBatchId?: string; // uuid
        productId?: string;
        quantity?: number; // int32
        siteCode?: string;
        srcCarrierId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        targetStockTypeCode?: string;
        userName?: string;
    }
    /**
     * ReservationCancelledResponse
     */
    export interface ReservationCancelledResponse {
        incomingQuantity?: number;
        operationId?: string;
        outgoingQuantity?: number;
        siteCode?: string;
        stockId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * ReservationDto
     */
    export interface ReservationDto {
        batchId?: string; // uuid
        placeAddress?: string;
        placeId?: string; // uuid
        productId?: string;
        quantityIncoming?: number;
        quantityOutgoing?: number;
        stockId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * ReservationInfoDto
     */
    export interface ReservationInfoDto {
        batchId?: string; // uuid
        incomingQuantity?: number;
        outgoingQuantity?: number;
        productId?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
        unit?: string;
    }
    /**
     * ReserveCarrierForProcessCmd
     */
    export interface ReserveCarrierForProcessCmd {
        carrierNumber?: string;
        operationId?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * ReserveCarrierForProcessResponse
     */
    export interface ReserveCarrierForProcessResponse {
        clientError?: Error;
        operationId?: string;
    }
    /**
     * ReserveInfo
     */
    export interface ReserveInfo {
        placeAddress?: string;
        placeId?: string; // uuid
        transfer?: TransferDto;
    }
    /**
     * ReservePlaceForProcessCmd
     */
    export interface ReservePlaceForProcessCmd {
        operationId?: string;
        placeBarcode?: string;
        processId?: string;
        siteCode?: string;
    }
    /**
     * ReservePlacesForCarriersCmd
     */
    export interface ReservePlacesForCarriersCmd {
        carrierPlaceTypeCode?: string;
        carriers?: TransferDto[];
        operationId?: string;
        processId?: string;
        processName?: string;
        processPlaceTypeCode?: string;
        reservationMode?: "FULL" | "PARTIAL";
        siteCode?: string;
        zones?: ZoneReference[];
    }
    /**
     * ReserveStockOutgoingQuantityResponse
     */
    export interface ReserveStockOutgoingQuantityResponse {
        operationId?: string;
        reservation?: ReservationDto;
    }
    /**
     * ReservedCarrier
     */
    export interface ReservedCarrier {
        openNewCarrier?: boolean;
        openedCarrierId?: string; // uuid
        quantity?: number;
    }
    /**
     * ReservedPlacesForCarriersResponse
     */
    export interface ReservedPlacesForCarriersResponse {
        clientError?: Error;
        operationId?: string;
        processId?: string;
        processName?: string;
        rejectedTransfers?: TransferDto[];
        reserves?: ReserveInfo[];
        siteCode?: string;
    }
    /**
     * Route
     */
    export interface Route {
        steps?: RouteStep[];
    }
    /**
     * RouteStep
     */
    export interface RouteStep {
        allocationSector?: ZoneWithType;
        allocationZone?: ZoneWithType;
        characteristics?: CharacteristicDto[];
        floor?: PlaceWithType;
        hierarchy?: PlaceWithType[];
        item?: StockAmountDto;
        pickingSector?: ZoneWithType;
        pickingZone?: ZoneWithType;
        place?: PlaceWithType;
        plannedDuration?: number; // int32
        transferId?: string; // uuid
        zones?: ZoneWithType[];
    }
    /**
     * ShipCarrierCmd
     */
    export interface ShipCarrierCmd {
        carrierId?: string; // uuid
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        tabNumber?: string;
        userName?: string;
    }
    /**
     * SiteCodeExistsQuery
     */
    export interface SiteCodeExistsQuery {
        code?: string;
    }
    /**
     * SiteDto
     */
    export interface SiteDto {
        archived?: boolean;
        author?: string;
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        name?: string;
        updatedTime?: number; // int32
    }
    /**
     * SiteSettingDto
     */
    export interface SiteSettingDto {
        bigDecimalValue?: number;
        code?: "MAX_MIX_PRODUCTS";
        intValue?: number; // int32
        stringValue?: string;
        type?: "INTEGER" | "STRING" | "BIG_DECIMAL";
    }
    /**
     * SlottingRoute
     */
    export interface SlottingRoute {
        plannedDuration?: number; // int32
        steps?: SlottingRouteStep[];
        transferId?: string; // uuid
    }
    /**
     * SlottingRouteStep
     */
    export interface SlottingRouteStep {
        characteristics?: CharacteristicDto[];
        hierarchy?: PlaceWithType[];
        place?: PlaceWithType;
        plannedDuration?: number; // int32
        reservation?: ReservationInfoDto;
        transferId?: string; // uuid
        zones?: ZoneWithType[];
    }
    /**
     * StockAddressesAndProductIdsSearchFilter
     */
    export interface StockAddressesAndProductIdsSearchFilter {
        addresses?: string[];
        productIds?: string[];
        siteId?: string; // uuid
    }
    /**
     * StockAddressesAndProductIdsSearchQuery
     */
    export interface StockAddressesAndProductIdsSearchQuery {
        filter?: StockAddressesAndProductIdsSearchFilter;
        orderInfo?: OrderByCriteria[];
        pageInfo?: PageRequestInfo;
    }
    /**
     * StockAmountDto
     */
    export interface StockAmountDto {
        productId?: string;
        quantity?: number;
        stockId?: string; // uuid
        unit?: string;
    }
    /**
     * StockDto
     */
    export interface StockDto {
        actualQuantity?: number;
        actualVolume?: number; // int32
        actualWeight?: number; // int32
        availableQuantity?: number;
        availableVolume?: number; // int32
        availableWeight?: number; // int32
        baseUnit?: string;
        batchConfirmed?: boolean;
        batchId?: string; // uuid
        batchNumber?: string;
        carrierId?: string; // uuid
        incomingQuantity?: number;
        incomingVolume?: number; // int32
        incomingWeight?: number; // int32
        outgoingQuantity?: number;
        outgoingVolume?: number; // int32
        outgoingWeight?: number; // int32
        placeId?: string; // uuid
        productId?: string;
        productName?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
        unit?: string;
    }
    /**
     * StockHierarchySearchFilter
     */
    export interface StockHierarchySearchFilter {
        hierarchy?: PlaceHierarchyFilter;
        placeCategory?: "STORAGE_PLACE" | "NOT_STORAGE_PLACE" | "ALL";
        placeTypes?: string /* uuid */ [];
        productIds?: string[];
        zonesType?: PlaceZonesFilter[];
    }
    /**
     * StockHierarchySearchQuery
     */
    export interface StockHierarchySearchQuery {
        filter?: StockHierarchySearchFilter;
        orderInfo?: OrderByCriteria[];
        pageInfo?: PageRequestInfo;
    }
    /**
     * StockInCmd
     */
    export interface StockInCmd {
        baseAmount?: number;
        baseUnit?: string;
        carrierId?: string; // uuid
        documentDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        placeId?: string; // uuid
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        productBatchId?: string; // uuid
        quantity?: number; // int32
        stockTypeCode?: string;
        tabNumber?: string;
        unit?: string;
        userName?: string;
    }
    /**
     * StockInResponse
     */
    export interface StockInResponse {
        operationId?: string;
        stockId?: string; // uuid
    }
    /**
     * StockInfo
     */
    export interface StockInfo {
        actual?: FullAmount;
        available?: FullAmount;
        batchConfirmed?: boolean;
        batchId?: string; // uuid
        batchNumber?: string;
        carrier?: boolean;
        expirationTime?: number; // int32
        inbound?: FullAmount;
        inboundTime?: number; // int32
        manufactureTime?: number; // int32
        outbound?: FullAmount;
        placeAddress?: string;
        placeId?: string; // uuid
        productId?: string;
        productName?: string;
        sellByTime?: number; // int32
        stockTypeCode?: string;
        stockTypeId?: string; // uuid
    }
    /**
     * StockTypeDto
     */
    export interface StockTypeDto {
        author?: string;
        code?: string;
        createdTime?: number; // int32
        editor?: string;
        id?: string; // uuid
        name?: string;
        updatedTime?: number; // int32
    }
    /**
     * StocksSearchResult
     */
    export interface StocksSearchResult {
        stocks?: StockInfo[];
        totalCount?: number; // int32
    }
    /**
     * TransferDroppedResponse
     */
    export interface TransferDroppedResponse {
        canceledReservations?: ReservationDto[];
        transferId?: string; // uuid
    }
    /**
     * TransferDto
     */
    export interface TransferDto {
        carrierTypeCode?: string;
        quantity?: number; // int32
        transferId?: string; // uuid
    }
    /**
     * TryAllocateSpaceForReplenishmentCmd
     */
    export interface TryAllocateSpaceForReplenishmentCmd {
        destPlaceBarcode?: string;
        operationId?: string;
        processId?: string;
        productBatchId?: string; // uuid
        quantity?: number; // int32
        siteCode?: string;
        srcCarrierId?: string; // uuid
        stockTypeCode?: string;
    }
    /**
     * TryAllocateSpaceForReplenishmentResponse
     */
    export interface TryAllocateSpaceForReplenishmentResponse {
        approvedQuantity?: number;
        operationId?: string;
        rejectedInfo?: RejectedInfo;
    }
    /**
     * TryTakeRequest
     */
    export interface TryTakeRequest {
        operationId?: string;
        placeId?: string; // uuid
        quantity?: number; // int32
        stockId?: string; // uuid
        transferId?: string; // uuid
        unit?: string;
    }
    /**
     * TryTakeResponse
     */
    export interface TryTakeResponse {
        acceptedQuantity?: number;
        operationId?: string;
        rejectReason?: string;
        rejected?: boolean;
        stockId?: string; // uuid
        transferId?: string; // uuid
    }
    /**
     * Type
     */
    export interface Type {
        code?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * UnshippedOrderItemDto
     */
    export interface UnshippedOrderItemDto {
        baseAmount?: number;
        baseUnit?: string;
        position?: number; // int64
        productId?: string;
        quantity?: number; // int32
        unit?: string;
    }
    /**
     * UpdateAllocationSettingsRequest
     */
    export interface UpdateAllocationSettingsRequest {
        maxMixedProducts?: number; // int32
        placeTypeCode?: string;
        siteCode?: string;
        zoneCodes?: string[];
    }
    /**
     * UpdateCarrierTypeCmd
     */
    export interface UpdateCarrierTypeCmd {
        description?: string;
        height?: number; // int32
        length?: number; // int32
        maxVolume?: number;
        maxWeight?: number;
        name?: string;
        width?: number; // int32
    }
    /**
     * UpdatePlaceCmd
     */
    export interface UpdatePlaceCmd {
        address?: string;
        addressTypeId?: string; // uuid
        coordinates?: Coordinates3D;
        description?: string;
        number?: number; // int32
        status?: PlaceStatusDto;
        statusReason?: string;
        typeId?: string; // uuid
        zoneIds?: string /* uuid */ [];
    }
    /**
     * UpdatePlaceTypeCmd
     */
    export interface UpdatePlaceTypeCmd {
        description?: string;
        maxMixBatches?: number; // int32
        name?: string;
        numerationRule?: "LEFT_TO_RIGHT_TOP_TO_BOTTOM" | "LEFT_TO_RIGHT_BOTTOM_UP" | "RIGHT_TO_LEFT_TOP_TO_BOTTOM" | "RIGHT_TO_LEFT_BOTTOM_UP";
        storagePlace?: boolean;
    }
    /**
     * UpdatePlaceTypeCommonCharacteristicDto
     */
    export interface UpdatePlaceTypeCommonCharacteristicDto {
        characteristic?: string;
        defaultValue?: DefaultValue;
    }
    /**
     * UpdateSiteCmd
     */
    export interface UpdateSiteCmd {
        description?: string;
        name?: string;
    }
    /**
     * UpdateStockTypeByInventoryCmd
     */
    export interface UpdateStockTypeByInventoryCmd {
        baseAmount?: number;
        docDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        operationType?: "MOVE";
        orderItem?: number; // int32
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        saleAmount?: number; // int32
        salesOrder?: string;
        specStock?: string;
        stockId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        transferId?: string; // uuid
        updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
        userName?: string;
    }
    /**
     * UpdateStockTypeByPickingCmd
     */
    export interface UpdateStockTypeByPickingCmd {
        docDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        operationType?: "MOVE";
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        stockId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
        userName?: string;
    }
    /**
     * UpdateStockTypeEvent
     */
    export interface UpdateStockTypeEvent {
        actualQuantity?: number;
        batchId?: string; // uuid
        batchNumber?: string;
        beiQuantity?: number;
        beiUnit?: string;
        carrierId?: string; // uuid
        documentDate?: string; // date-time
        documentNumber?: string;
        expirationDate?: string; // date-time
        manufactureDate?: string; // date-time
        newStockId?: string; // uuid
        newStockTypeCode?: string;
        oldStockId?: string; // uuid
        oldStockTypeCode?: string;
        operationId?: string;
        operationType?: "MOVE";
        orderItemIndex?: number; // int32
        placeWithType?: PlaceWithType;
        productId?: string;
        productName?: string;
        salesOrder?: string;
        shouldNotifyERP?: boolean;
        siteCode?: string;
        specialStockCode?: string;
        storageDate?: string; // date-time
        tabNumber?: string;
        unit?: string;
        updateReason?: "UNKNOWN" | "PRODUCT_BATCH" | "CORRUPTED_BARCODE" | "DEFECT" | "NO_PRODUCT" | "EXPIRED";
    }
    /**
     * UpdateStockTypeForBatchCmd
     */
    export interface UpdateStockTypeForBatchCmd {
        batchId?: string; // uuid
        newStockTypeCode?: string;
        oldStockTypeCode?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        tabNumber?: string;
        userName?: string;
    }
    /**
     * UpdateStockTypeInStockByUICmd
     */
    export interface UpdateStockTypeInStockByUICmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        stockTypeCode?: string;
        tabNumber?: string;
        userName?: string;
    }
    /**
     * UpdateZoneCmd
     */
    export interface UpdateZoneCmd {
        boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
        code?: string;
        description?: string;
        name?: string;
    }
    /**
     * UpdateZoneTypeCmd
     */
    export interface UpdateZoneTypeCmd {
        description?: string;
        name?: string;
    }
    export interface V1AllocationsSettings {
        placeTypeCode?: string;
        siteCode?: string;
    }
    export interface V1PickingConsolidationZone {
        zoneId: string;
    }
    export interface V1ProductBatch {
        inboundId: string;
        manufactureTime?: number;
        productId: string;
        siteCode: string;
        type?: "REGULAR" | "RETURN";
        vendorCode: string;
    }
    export interface V1Stock {
        carrierId?: string;
        placeBarcode?: string;
        placeId?: string;
        productBatchId?: string;
        productId?: string;
        siteCode?: string;
        stockTypeCode?: string;
    }
    export interface V1StockCarrierTypes {
        code?: string[];
    }
    export interface V1StockCarriers {
        barcode?: string;
        placeId?: string;
        rootOnly?: boolean;
    }
    export interface V1StockCarriersView {
        placeId: string;
    }
    export interface V1StockPlacesReservations {
        transferId?: string /* uuid */ [];
    }
    export interface V1TopologyPlaces {
        barcode?: string;
        placeId?: string;
        showChildren?: boolean;
        siteCode?: string;
    }
    export interface V1TopologyPlacesFindByProcessId {
        placeTypeCode: string;
        processId: string;
        showChildren?: boolean;
        siteCode: string;
    }
    export interface V1TopologyPlacesShortView {
        placeBarcode: string;
        siteCode: string;
    }
    export interface V1TopologyZoneTypesWithZones {
        siteId?: string;
    }
    export interface V1TopologyZones {
        siteCode: string;
        zoneTypeCode: string;
    }
    export interface V1TopologyZonesProjections {
        siteId: string;
        typeId: string;
    }
    /**
     * ValueCmd
     */
    export interface ValueCmd {
        decValue?: number;
        intValue?: number; // int32
        maxIntValue?: number; // int32
        minIntValue?: number; // int32
    }
    /**
     * ValueDto
     */
    export interface ValueDto {
        decValue?: number;
        intValue?: number; // int32
        maxDecValue?: number;
        maxIntValue?: number; // int32
        minDecValue?: number;
        minIntValue?: number; // int32
    }
    /**
     * WarehouseProductGroupDto
     */
    export interface WarehouseProductGroupDto {
        code?: string;
    }
    /**
     * WriteOffLostStockCmd
     */
    export interface WriteOffLostStockCmd {
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        tabNumber?: string;
        userName?: string;
    }
    /**
     * WriteOffRawStocksCmd
     */
    export interface WriteOffRawStocksCmd {
        operationId?: string;
        orderVersion?: string;
        outboundDeliveryNumber?: string;
        outboundTime?: string; // date-time
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        siteCode?: string;
        stocks?: RawStock[];
        tabNumber?: string;
        userName?: string;
    }
    /**
     * WriteOffStockCmd
     */
    export interface WriteOffStockCmd {
        baseAmount?: number;
        baseUnit?: string;
        documentDate?: string; // date-time
        documentNumber?: string;
        operationId?: string;
        processId?: string;
        processType?: "CLIENT_RETURNS" | "INBOUND" | "OUTBOUND" | "PICKING" | "INVENTORY" | "REPLENISHMENT" | "MANUFACTURING" | "TSD_MANUAL_MOVEMENT" | "UI" | "CONSOLIDATION";
        quantity?: number; // int32
        stockId?: string; // uuid
        tabNumber?: string;
        unit?: string;
        userName?: string;
    }
    /**
     * WriteOffStockResponse
     */
    export interface WriteOffStockResponse {
        clientError?: Error;
        operationId?: string;
        stockId?: string; // uuid
    }
    /**
     * Zone
     */
    export interface Zone {
        code?: string;
        id?: string; // uuid
        name?: string;
        type?: Type;
    }
    /**
     * ZoneCodeExistsQuery
     */
    export interface ZoneCodeExistsQuery {
        code?: string;
        siteId?: string; // uuid
        typeId?: string; // uuid
    }
    /**
     * ZoneDto
     */
    export interface ZoneDto {
        code?: string;
        id?: string; // uuid
        name?: string;
    }
    /**
     * ZoneInfo
     */
    export interface ZoneInfo {
        code?: string;
        id?: string; // uuid
        name?: string;
        typeCode?: string;
        typeId?: string; // uuid
        typeName?: string;
    }
    /**
     * ZoneProjection
     */
    export interface ZoneProjection {
        author?: string;
        boundedWarehouseProductGroups?: WarehouseProductGroupDto[];
        childAmount?: number; // int32
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        name?: string;
        siteId?: string; // uuid
        typeId?: string; // uuid
        typeName?: string;
        updatedTime?: number; // int32
    }
    /**
     * ZoneReference
     */
    export interface ZoneReference {
        zoneCode?: string;
        zoneTypeCode?: string;
    }
    /**
     * ZoneTypeCodeExistsQuery
     */
    export interface ZoneTypeCodeExistsQuery {
        code?: string;
    }
    /**
     * ZoneTypeDto
     */
    export interface ZoneTypeDto {
        author?: string;
        code?: string;
        createdTime?: number; // int32
        description?: string;
        editor?: string;
        id?: string; // uuid
        name?: string;
        numberOfImplementation?: number; // int64
        updatedTime?: number; // int32
        warehouseProductGroupCanBeBounded?: boolean;
    }
    /**
     * ZoneTypeWithZones
     */
    export interface ZoneTypeWithZones {
        code?: string;
        id?: string; // uuid
        name?: string;
        zones?: ZoneDto[];
    }
    /**
     * ZoneTypeWithZonesDto
     */
    export interface ZoneTypeWithZonesDto {
        code?: string;
        id?: string; // uuid
        name?: string;
        zones?: ZoneDto[];
    }
    /**
     * ZoneWithType
     */
    export interface ZoneWithType {
        code?: string;
        id?: string; // uuid
        name?: string;
        type?: Type;
    }
}
