/** Generate by swagger-axios-codegen */
// @ts-nocheck
/* eslint-disable */

import { IRequestOptions, IRequestConfig, getConfigs, axios } from './serviceOptions';
export const basePath = '/api/warehouse';

export interface IList<T> extends Array<T> {}
export interface List<T> extends Array<T> {}
export interface IDictionary<TValue> {
  [key: string]: TValue;
}
export interface Dictionary<TValue> extends IDictionary<TValue> {}

export interface IListResult<T> {
  items?: T[];
}

export class ListResultDto<T> implements IListResult<T> {
  items?: T[];
}

export interface IPagedResult<T> extends IListResult<T> {
  totalCount?: number;
  items?: T[];
}

export class PagedResultDto<T = any> implements IPagedResult<T> {
  totalCount?: number;
  items?: T[];
}

// customer definition
// empty

export class AllocationGatewayService {
  /**
   * Запрос в МРТ на размещение монопаллеты товара
   */
  static monoPalletProductPlacement(
    params: {
      /** request */
      request: MonoPalletPlacementRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPlacementResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/allocations/monoPalletProductPlacement';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос в МРТ на обычное (не паллетное) размещение товара
   */
  static productPlacement(
    params: {
      /** request */
      request: ProductPlacementRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPlacementResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/allocations/productPlacement';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос в МРТ на размещение сырья
   */
  static rawPlacement(
    params: {
      /** request */
      request: RawPlacementRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPlacementResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/allocations/rawPlacement';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CarrierAuditGatewayService {
  /**
   * История движения носителей
   */
  static findAll(
    params: {
      /** searchQuery */
      searchQuery: AuditCarrierRecordSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<AuditCarrierRecordSearchResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/audit/carrier/findAll';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['searchQuery'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class MarkAuditGatewayService {
  /**
   * История движения товаров
   */
  static findAll(
    params: {
      /** searchQuery */
      searchQuery: AuditMarkRecordSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<AuditMarkRecordSearchResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/audit/mark/findAll';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['searchQuery'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class StockAuditGatewayService {
  /**
   * История движения товаров
   */
  static findAll(
    params: {
      /** searchQuery */
      searchQuery: AuditStockRecordSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<AuditStockRecordSearchResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/audit/stock/findAll';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['searchQuery'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PickingGatewayService {
  /**
   * Запрос в МKТ для маршрутизации коробов комплектации
   */
  static build(
    params: {
      /** cmd */
      cmd: BoxRouteCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BoxRouteResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/box-route/build';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос в МKТ для маршрутизации коробов комплектации
   */
  static build1(
    params: {
      /** cmd */
      cmd: BuildBoxRouteCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BoxRoute> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/box-routes/build';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос в МKТ для текущего маршрута комплектовщика
   */
  static build2(
    params: {
      /** cmd */
      cmd: PickerRouteReq;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<Route> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/picker-routes/build';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос в МKТ для перехода между областями действий
   */
  static build3(
    params: {
      /** cmd */
      cmd: PickingZoneTransferReq;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<Route> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/picker-transfer/build';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос для получения области консолидации для области комплектации
   */
  static consolidationZone(
    params: {
      /** zoneId */
      zoneId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/picking/consolidation-zone';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { zoneId: params['zoneId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class UploadGatewayService {
  /**
   * Запрос на загрузку файла со змейкой по буферам отгрузки
   */
  static upload(
    params: {
      /** siteCode */
      siteCode: string;
      /** uploadClusterDeliveryReservationRouteFile */
      uploadClusterDeliveryReservationRouteFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/cluster-delivery-reservation-route/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteCode']) {
        if (Object.prototype.toString.call(params['siteCode']) === '[object Array]') {
          for (const item of params['siteCode']) {
            data.append('siteCode', item as any);
          }
        } else {
          data.append('siteCode', params['siteCode'] as any);
        }
      }

      if (params['uploadClusterDeliveryReservationRouteFile']) {
        if (Object.prototype.toString.call(params['uploadClusterDeliveryReservationRouteFile']) === '[object Array]') {
          for (const item of params['uploadClusterDeliveryReservationRouteFile']) {
            data.append('uploadClusterDeliveryReservationRouteFile', item as any);
          }
        } else {
          data.append(
            'uploadClusterDeliveryReservationRouteFile',
            params['uploadClusterDeliveryReservationRouteFile'] as any
          );
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с партиями товара
   */
  static upload1(
    params: {
      /** batchesFile */
      batchesFile: any;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['batchesFile']) {
        if (Object.prototype.toString.call(params['batchesFile']) === '[object Array]') {
          for (const item of params['batchesFile']) {
            data.append('batchesFile', item as any);
          }
        } else {
          data.append('batchesFile', params['batchesFile'] as any);
        }
      }

      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла правил мест старта
   */
  static upload2(
    params: {
      /** routeStartRuleFile */
      routeStartRuleFile: any;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-start-rules/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['routeStartRuleFile']) {
        if (Object.prototype.toString.call(params['routeStartRuleFile']) === '[object Array]') {
          for (const item of params['routeStartRuleFile']) {
            data.append('routeStartRuleFile', item as any);
          }
        } else {
          data.append('routeStartRuleFile', params['routeStartRuleFile'] as any);
        }
      }

      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с приоритетом обхода секций
   */
  static upload3(
    params: {
      /** routeStrategyFile */
      routeStrategyFile: any;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['routeStrategyFile']) {
        if (Object.prototype.toString.call(params['routeStrategyFile']) === '[object Array]') {
          for (const item of params['routeStrategyFile']) {
            data.append('routeStrategyFile', item as any);
          }
        } else {
          data.append('routeStrategyFile', params['routeStrategyFile'] as any);
        }
      }

      if (params['siteCode']) {
        if (Object.prototype.toString.call(params['siteCode']) === '[object Array]') {
          for (const item of params['siteCode']) {
            data.append('siteCode', item as any);
          }
        } else {
          data.append('siteCode', params['siteCode'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Новый запрос на загрузку файла с приоритетом обхода секций
   */
  static uploadStrategy(
    params: {
      /** routeStrategyFile */
      routeStrategyFile: any;
      /** strategyId */
      strategyId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/upload-strategy/{strategyId}';
      url = url.replace('{strategyId}', params['strategyId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['routeStrategyFile']) {
        if (Object.prototype.toString.call(params['routeStrategyFile']) === '[object Array]') {
          for (const item of params['routeStrategyFile']) {
            data.append('routeStrategyFile', item as any);
          }
        } else {
          data.append('routeStrategyFile', params['routeStrategyFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с типами носителя
   */
  static upload4(
    params: {
      /** carrierTypeFile */
      carrierTypeFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carrier-types/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['carrierTypeFile']) {
        if (Object.prototype.toString.call(params['carrierTypeFile']) === '[object Array]') {
          for (const item of params['carrierTypeFile']) {
            data.append('carrierTypeFile', item as any);
          }
        } else {
          data.append('carrierTypeFile', params['carrierTypeFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с носителями
   */
  static upload5(
    params: {
      /** carrierFile */
      carrierFile: any;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierUploadResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['carrierFile']) {
        if (Object.prototype.toString.call(params['carrierFile']) === '[object Array]') {
          for (const item of params['carrierFile']) {
            data.append('carrierFile', item as any);
          }
        } else {
          data.append('carrierFile', params['carrierFile'] as any);
        }
      }

      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с запасами
   */
  static upload6(
    params: {
      /** siteId */
      siteId: string;
      /** stocksFile */
      stocksFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      if (params['stocksFile']) {
        if (Object.prototype.toString.call(params['stocksFile']) === '[object Array]') {
          for (const item of params['stocksFile']) {
            data.append('stocksFile', item as any);
          }
        } else {
          data.append('stocksFile', params['stocksFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с топологией
   */
  static upload7(
    params: {
      /** placesFile */
      placesFile: any;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['placesFile']) {
        if (Object.prototype.toString.call(params['placesFile']) === '[object Array]') {
          for (const item of params['placesFile']) {
            data.append('placesFile', item as any);
          }
        } else {
          data.append('placesFile', params['placesFile'] as any);
        }
      }

      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла типов мест с характеристиками
   */
  static upload8(
    params: {
      /** placesFile */
      placesFile: any;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes/upload';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['placesFile']) {
        if (Object.prototype.toString.call(params['placesFile']) === '[object Array]') {
          for (const item of params['placesFile']) {
            data.append('placesFile', item as any);
          }
        } else {
          data.append('placesFile', params['placesFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с типами зон
   */
  static upload9(
    params: {
      /** zoneTypeFile */
      zoneTypeFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['zoneTypeFile']) {
        if (Object.prototype.toString.call(params['zoneTypeFile']) === '[object Array]') {
          for (const item of params['zoneTypeFile']) {
            data.append('zoneTypeFile', item as any);
          }
        } else {
          data.append('zoneTypeFile', params['zoneTypeFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с зонами
   */
  static upload10(
    params: {
      /** siteId */
      siteId: string;
      /** zonesFile */
      zonesFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      if (params['zonesFile']) {
        if (Object.prototype.toString.call(params['zonesFile']) === '[object Array]') {
          for (const item of params['zonesFile']) {
            data.append('zonesFile', item as any);
          }
        } else {
          data.append('zonesFile', params['zonesFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с областями комплектования
   */
  static ok(
    params: {
      /** siteId */
      siteId: string;
      /** zonesFile */
      zonesFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/upload/ok';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      if (params['zonesFile']) {
        if (Object.prototype.toString.call(params['zonesFile']) === '[object Array]') {
          for (const item of params['zonesFile']) {
            data.append('zonesFile', item as any);
          }
        } else {
          data.append('zonesFile', params['zonesFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла со складскими участками
   */
  static su(
    params: {
      /** siteId */
      siteId: string;
      /** zonesFile */
      zonesFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/upload/su';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      if (params['zonesFile']) {
        if (Object.prototype.toString.call(params['zonesFile']) === '[object Array]') {
          for (const item of params['zonesFile']) {
            data.append('zonesFile', item as any);
          }
        } else {
          data.append('zonesFile', params['zonesFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с настройками списания
   */
  static upload11(
    params: {
      /** settingsFile */
      settingsFile: any;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/write-off-settings/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['settingsFile']) {
        if (Object.prototype.toString.call(params['settingsFile']) === '[object Array]') {
          for (const item of params['settingsFile']) {
            data.append('settingsFile', item as any);
          }
        } else {
          data.append('settingsFile', params['settingsFile'] as any);
        }
      }

      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с настройкой приоритетов секторов комплектования
   */
  static upload12(
    params: {
      /** siteCode */
      siteCode: string;
      /** zoneCharacteristics */
      zoneCharacteristics: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/zone-characteristics/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteCode']) {
        if (Object.prototype.toString.call(params['siteCode']) === '[object Array]') {
          for (const item of params['siteCode']) {
            data.append('siteCode', item as any);
          }
        } else {
          data.append('siteCode', params['siteCode'] as any);
        }
      }

      if (params['zoneCharacteristics']) {
        if (Object.prototype.toString.call(params['zoneCharacteristics']) === '[object Array]') {
          for (const item of params['zoneCharacteristics']) {
            data.append('zoneCharacteristics', item as any);
          }
        } else {
          data.append('zoneCharacteristics', params['zoneCharacteristics'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла с привязкой стратегии обхода змейки
   */
  static upload13(
    params: {
      /** siteCode */
      siteCode: string;
      /** zoneRouteStrategy */
      zoneRouteStrategy: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/zone-route-strategy/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteCode']) {
        if (Object.prototype.toString.call(params['siteCode']) === '[object Array]') {
          for (const item of params['siteCode']) {
            data.append('siteCode', item as any);
          }
        } else {
          data.append('siteCode', params['siteCode'] as any);
        }
      }

      if (params['zoneRouteStrategy']) {
        if (Object.prototype.toString.call(params['zoneRouteStrategy']) === '[object Array]') {
          for (const item of params['zoneRouteStrategy']) {
            data.append('zoneRouteStrategy', item as any);
          }
        } else {
          data.append('zoneRouteStrategy', params['zoneRouteStrategy'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на загрузку файла правил переходов между местами
   */
  static upload14(
    params: {
      /** siteId */
      siteId: string;
      /** zoneTransferRuleFile */
      zoneTransferRuleFile: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/zone-transfer-rules/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['siteId']) {
        if (Object.prototype.toString.call(params['siteId']) === '[object Array]') {
          for (const item of params['siteId']) {
            data.append('siteId', item as any);
          }
        } else {
          data.append('siteId', params['siteId'] as any);
        }
      }

      if (params['zoneTransferRuleFile']) {
        if (Object.prototype.toString.call(params['zoneTransferRuleFile']) === '[object Array]') {
          for (const item of params['zoneTransferRuleFile']) {
            data.append('zoneTransferRuleFile', item as any);
          }
        } else {
          data.append('zoneTransferRuleFile', params['zoneTransferRuleFile'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class HazelcastGatewayService {
  /**
   * Удаление мест из кэша для МРТ
   */
  static delete(
    params: {
      /** cmd */
      cmd: DeletePlacesFromHazelcastCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/hazelcast/delete';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Начальное заполнение кэша мест для МРТ
   */
  static init(
    params: {
      /** cmd */
      cmd: InitialPopulateHazelcastCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/hazelcast/init';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class MarksGatewayService {
  /**
   * Удаление марок
   */
  static marks(
    params: {
      /** request */
      request: DeleteMarksCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DeleteMarksResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/marks';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение статусов марок
   */
  static changeStatus(
    params: {
      /** request */
      request: ChangeMarksStatusCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ChangeMarksStatusResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/marks/changeStatus';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Диагностический эндпоинт, предназначен для сохранения марок
   */
  static create(
    params: {
      /** request */
      request: CreateMarksCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CreateMarksResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/marks/create';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Создание отсканированной марки ЧЗ при комплектовании, инвентаризации и т.п.
   */
  static createScannedChzMark(
    params: {
      /** request */
      request: CreateScannedChzMarkCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CreateScannedChzMarkResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/marks/createScannedChzMark';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Поиск марок по параметрам, для внешних модулей
   */
  static search(
    params: {
      /** request */
      request: MarkSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MarkSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/marks/search';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Диагностический эндпоинт, предназначен для изменения свойств марок
   */
  static update(
    params: {
      /** request */
      request: UpdateMarksCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UpdateMarksResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/marks/update';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceReservationSearchGatewayService {
  /**
   * Поиск носителей, резевов под носители и резервов мест под процесс в определенных местах
   */
  static search(
    params: {
      /** placeReservationSearchRequest */
      placeReservationSearchRequest: PlaceReservationSearchRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceReservationSearchResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/places/reservation/search';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['placeReservationSearchRequest'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProductBatchGatewayService {
  /**
   * getAll
   */
  static productBatch(
    params: {
      /** inboundId */
      inboundId: string;
      /** manufactureTime */
      manufactureTime?: number;
      /** productId */
      productId: string;
      /** siteCode */
      siteCode: string;
      /** type */
      type?: string;
      /** vendorCode */
      vendorCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductBatchDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        inboundId: params['inboundId'],
        manufactureTime: params['manufactureTime'],
        productId: params['productId'],
        siteCode: params['siteCode'],
        type: params['type'],
        vendorCode: params['vendorCode']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * assignBatch
   */
  static productBatch1(
    params: {
      /** cmd */
      cmd: AssignProductBatchCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductBatchDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getBatchBalanceDifference
   */
  static difference(
    params: {
      /** productBatchId */
      productBatchId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductBatchBalanceDifferenceDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/balance/{productBatchId}/difference';
      url = url.replace('{productBatchId}', params['productBatchId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * fixBatchBalanceDifference
   */
  static fix(
    params: {
      /** productBatchId */
      productBatchId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/balance/{productBatchId}/fix';
      url = url.replace('{productBatchId}', params['productBatchId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * "Обеление" вида запаса
   */
  static confirm(
    params: {
      /** confirmProductBatchCmd */
      confirmProductBatchCmd: ConfirmProductBatchCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/confirm';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['confirmProductBatchCmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * synchronizeBatch
   */
  static sync(
    params: {
      /** productBatchId */
      productBatchId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/sync/{productBatchId}';
      url = url.replace('{productBatchId}', params['productBatchId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getById
   */
  static productBatch2(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductBatchDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProductBatchSearchGatewayService {
  /**
   * Search Batches
   */
  static search(
    params: {
      /** query */
      query: ProductBatchesSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductBatchesSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/product-batch/search';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ReplenishmentGatewayService {
  /**
   * Перемещение запаса между носителями с рокировкой входящего резерва в месте
   */
  static moveStockBetweenCarriers(
    params: {
      /** request */
      request: MoveStockBetweenReplenishmentCarriersCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MoveStockBetweenReplenishmentCarriersResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/replenishment/moveStockBetweenCarriers';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Размещение товара
   */
  static replenish(
    params: {
      /** cmd */
      cmd: ReplenishCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/replenishment/replenish';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на построение и резервирование маршрута для перехода между областями действий
   */
  static build(
    params: {
      /** cmd */
      cmd: AllocationZoneTransferReq;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<Route> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/replenishment/transfer/build';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Проверка места перед ручным перемещением сырья
   */
  static tryAllocateRaw(
    params: {
      /** cmd */
      cmd: TryAllocateSpaceForReplenishmentCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TryAllocateSpaceForReplenishmentResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/replenishment/tryAllocateRaw';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Проверка места перед размещением товара
   */
  static tryAllocateSpace(
    params: {
      /** cmd */
      cmd: TryAllocateSpaceForReplenishmentCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TryAllocateSpaceForReplenishmentResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/replenishment/tryAllocateSpace';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Проверка места перед ручным перемещением товара
   */
  static tryAllocateSpaceManual(
    params: {
      /** cmd */
      cmd: TryAllocateSpaceForReplenishmentCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TryAllocateSpaceForReplenishmentResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/replenishment/tryAllocateSpaceManual';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class DownloadGatewayService {
  /**
   * Запрос на выгрузку змейки по буферам отгрузки в файл
   */
  static download(
    params: {
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/reservation/route/download/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на выгрузку файла с приоритетом обхода секций для стратегии обхода
   */
  static download1(
    params: {
      /** strategyId */
      strategyId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/download/{strategyId}';
      url = url.replace('{strategyId}', params['strategyId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на выгрузку стоков
   */
  static download2(
    params: {
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/download/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на выгрузку файла с типами мест
   */
  static download3(
    params: {
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeTypes/download/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на выгрузку файла с топологией
   */
  static download4(
    params: {
      /** address */
      address?: string;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/download/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { address: params['address'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на выгрузку файла с типами зон
   */
  static download5(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zoneTypes/download';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RouteStrategyGatewayService {
  /**
   * Запрос на создание стратегии обхода на площадке
   */
  static routeStrategy(
    params: {
      /** cmd */
      cmd: CreateRouteStrategyCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteStrategyDTO> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на получение всех стратегий обхода в рамках площадки
   */
  static strategies(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteStrategyDTO[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/strategies';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { siteId: params['siteId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на получение змейки с порядком обхода секций для стратегии обхода в рамках площадки
   */
  static strategies1(
    params: {
      /** routeStrategyId */
      routeStrategyId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteStrategySteps> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/strategies/{routeStrategyId}';
      url = url.replace('{routeStrategyId}', params['routeStrategyId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на обновление стратегии обхода
   */
  static routeStrategy1(
    params: {
      /** cmd */
      cmd: UpdateRouteStrategyCmd;
      /** routeStrategyId */
      routeStrategyId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteStrategyDTO> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/{routeStrategyId}';
      url = url.replace('{routeStrategyId}', params['routeStrategyId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на удаление стратегии обхода
   */
  static routeStrategy2(
    params: {
      /** routeStrategyId */
      routeStrategyId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/route-strategy/{routeStrategyId}';
      url = url.replace('{routeStrategyId}', params['routeStrategyId'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RouteGatewayService {
  /**
   * Запрос змейки для подбора мест в отгрузке
   */
  static reservation(
    params: {
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ClusterDeliveryReservationRoute> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/routes/reservation/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос маршрута слотчика по области действий
   */
  static routes(
    params: {
      /** currentPlaceId */
      currentPlaceId?: string;
      /** startPlaceId */
      startPlaceId: string;
      /** transferId */
      transferId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SlottingRoute> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/routes/{transferId}';
      url = url.replace('{transferId}', params['transferId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { currentPlaceId: params['currentPlaceId'], startPlaceId: params['startPlaceId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class SnapshotGatewayService {
  /**
   * triggerSnapshot
   */
  static snapshot(
    params: {
      /** entity */
      entity: string;
      /** entityId */
      entityId: string;
      /** type */
      type: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/snapshot/{entityId}';
      url = url.replace('{entityId}', params['entityId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);
      configs.params = { entity: params['entity'], type: params['type'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class StockGatewayService {
  /**
   * getAll
   */
  static stock(
    params: {
      /** carrierId */
      carrierId?: string;
      /** placeBarcode */
      placeBarcode?: string;
      /** placeId */
      placeId?: string;
      /** productBatchId */
      productBatchId?: string;
      /** productId */
      productId?: string;
      /** siteCode */
      siteCode?: string;
      /** stockTypeCode */
      stockTypeCode?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        carrierId: params['carrierId'],
        placeBarcode: params['placeBarcode'],
        placeId: params['placeId'],
        productBatchId: params['productBatchId'],
        productId: params['productId'],
        siteCode: params['siteCode'],
        stockTypeCode: params['stockTypeCode']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Уничтожение запаса без проводок в ERP
   */
  static annihilate(
    params: {
      /** cmd */
      cmd: StockAnnihilationCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockAnnihilationResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/annihilate';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Расчет кол-ва в ПЕИ
   */
  static calculatePeiQuantity(
    params: {
      /** cmd */
      cmd: CalculateNewProductPeiQuantityCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CalculateNewProductPeiQuantityResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/calculatePeiQuantity';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Добавление запаса в носитель
   */
  static carriers(
    params: {
      /** carrierId */
      carrierId: string;
      /** cmd */
      cmd: AddStockToCarrierCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/{carrierId}';
      url = url.replace('{carrierId}', params['carrierId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение productId в существующем запасе
   */
  static changeProduct(
    params: {
      /** cmd */
      cmd: ChangeStockProductCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ChangeStockProductResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/changeProduct';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Отпуск сырья
   */
  static changeRawStockType(
    params: {
      /** cmd */
      cmd: ChangeRawStockTypeCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/changeRawStockType';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Проверка, может ли сток лежать в его месте
   */
  static checkPlaceForStock(
    params: {
      /** cmd */
      cmd: CheckPlaceForStockCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CheckPlaceForStockResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/checkPlaceForStock';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Детальный поиск по стокам
   */
  static findAll(
    params: {
      /** query */
      query: DetailedStockSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DetailedStocksSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/findAll';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Перемещение запаса в носитель с корректировкой резерва
   */
  static moveAllTo(
    params: {
      /** cmd */
      cmd: MoveAllStocksToCarrierCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MoveAllStocksToCarrierResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/moveAllTo';

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Перемещение запаса (без проверок, без корректировки резервов)
   */
  static moveStock(
    params: {
      /** cmd */
      cmd: MoveStockCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MoveStockCmdResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/moveStock';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Отгрузка заказов
   */
  static outboundOrder(
    params: {
      /** cmd */
      cmd: OutboundOrderCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/outboundOrder';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Добавление найденного товара в место или носитель без проверок
   */
  static stockIn(
    params: {
      /** cmd */
      cmd: StockInCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockInResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-in';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос на попытку взять товар из ячейки
   */
  static tryTake(
    params: {
      /** tryTakeRequest */
      tryTakeRequest: TryTakeRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TryTakeResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/try-take';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['tryTakeRequest'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Перевод всех товаров с истекающим сроком реализации в SS
   */
  static updateExpired(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/updateExpired';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Перевод товаров пекарни с истекающим сроком реализации в SS
   */
  static updateExpiredBakery(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/updateExpiredBakery';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение вида запаса инвентаризацией
   */
  static updateStockTypeByInventory(
    params: {
      /** updateStockTypeCmd */
      updateStockTypeCmd: UpdateStockTypeByInventoryCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UpdateStockTypeEvent> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/updateStockTypeByInventory';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['updateStockTypeCmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение вида запаса комплектацией
   */
  static updateStockTypeByPicking(
    params: {
      /** updateStockTypeCmd */
      updateStockTypeCmd: UpdateStockTypeByPickingCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UpdateStockTypeEvent> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/updateStockTypeByPicking';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['updateStockTypeCmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение вида запаса через UI
   */
  static updateStockTypeByUi(
    params: {
      /** cmd */
      cmd: UpdateStockTypeInStockByUICmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/updateStockTypeByUI';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение вида запаса для партии. Вызывается после обеления.
   */
  static updateStockTypeForBatch(
    params: {
      /** updateStockTypeForBatchCmd */
      updateStockTypeForBatchCmd: UpdateStockTypeForBatchCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/updateStockTypeForBatch';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['updateStockTypeForBatchCmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Списание запаса
   */
  static writeOff(
    params: {
      /** cmd */
      cmd: WriteOffStockCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WriteOffStockResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/writeOff';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Списание запаса
   */
  static writeOffLost(
    params: {
      /** cmd */
      cmd: WriteOffStockByUICmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WriteOffStockResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/writeOffLost';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Отпуск сырья с доинвентаризацией выбывшего запаса
   */
  static writeOffRaw(
    params: {
      /** cmd */
      cmd: WriteOffRawCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WriteOffRawResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/writeOffRaw';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Отпуск сырья
   */
  static writeOffRawStock(
    params: {
      /** cmd */
      cmd: WriteOffRawStocksCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/writeOffRawStock';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getById
   */
  static stock1(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Перемещение запаса в носитель с корректировкой резерва
   */
  static moveTo(
    params: {
      /** carrierId */
      carrierId: string;
      /** cmd */
      cmd: MoveStockToCarrierCmd;
      /** stockId */
      stockId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/{stockId}/moveTo/{carrierId}';
      url = url.replace('{carrierId}', params['carrierId'] + '');
      url = url.replace('{stockId}', params['stockId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class StockSearchGatewayService {
  /**
   * Search Stocks by Addresses and Product Ids
   */
  static addressAndProductIdsSearch(
    params: {
      /** query */
      query: StockAddressesAndProductIdsSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StocksSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/addressAndProductIdsSearch';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Поиск для грида запасов
   */
  static findAllUi(
    params: {
      /** query */
      query: UIStockSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UIStockSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/findAllUI';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Search Stocks
   */
  static search(
    params: {
      /** query */
      query: StockHierarchySearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StocksSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/search';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CarrierTypeGatewayService {
  /**
   * getCarrierTypes
   */
  static carrierTypes(
    params: {
      /** code */
      code?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carrier-types';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { code: params['code'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * create
   */
  static carrierTypes1(
    params: {
      /** cmd */
      cmd: CreateCarrierTypeCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carrier-types';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * codeExists
   */
  static codeExists(
    params: {
      /** query */
      query: CarrierTypeCodeExistsQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CodeExists> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carrier-types/codeExists';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * update
   */
  static carrierTypes2(
    params: {
      /** carrierTypeId */
      carrierTypeId: string;
      /** cmd */
      cmd: UpdateCarrierTypeCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carrier-types/{carrierTypeId}';
      url = url.replace('{carrierTypeId}', params['carrierTypeId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getCarrierType
   */
  static carrierTypes3(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carrier-types/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CarrierGatewayService {
  /**
   * getAll
   */
  static carriers(
    params: {
      /** barcode */
      barcode?: string;
      /** placeId */
      placeId?: string;
      /** rootOnly */
      rootOnly?: boolean;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { barcode: params['barcode'], placeId: params['placeId'], rootOnly: params['rootOnly'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * create a Carrier
   */
  static carriers1(
    params: {
      /** cmd */
      cmd: CreateCarrierCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getAll
   */
  static find(
    params: {
      /** detailedCarriersSearchQuery */
      detailedCarriersSearchQuery: DetailedCarriersSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/find';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['detailedCarriersSearchQuery'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Отгрузка носителя
   */
  static ship(
    params: {
      /** cmd */
      cmd: ShipCarrierCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierShippedResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/ship';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * moveCarrierIntoCarrier
   */
  static moveInto(
    params: {
      /** carrierId */
      carrierId: string;
      /** parentCarrierId */
      parentCarrierId: string;
      /** parentProcessId */
      parentProcessId?: string;
      /** processId */
      processId?: string;
      /** processType */
      processType?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/{carrierId}/moveInto/{parentCarrierId}';
      url = url.replace('{carrierId}', params['carrierId'] + '');
      url = url.replace('{parentCarrierId}', params['parentCarrierId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);
      configs.params = {
        parentProcessId: params['parentProcessId'],
        processId: params['processId'],
        processType: params['processType']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getById
   */
  static carriers2(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * delete a Carrier
   */
  static carriers3(
    params: {
      /** id */
      id: string;
      /** processId */
      processId?: string;
      /** processType */
      processType?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);
      configs.params = { processId: params['processId'], processType: params['processType'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CarrierViewGatewayService {
  /**
   * Get carriers hierarchy in place for grid representation
   */
  static carriersView(
    params: {
      /** placeId */
      placeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CarrierGridView[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers-view';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { placeId: params['placeId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ReservationCarrierGatewayService {
  /**
   * Резервирование носителя под процесс
   */
  static forProcess(
    params: {
      /** cmd */
      cmd: ReserveCarrierForProcessCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReserveCarrierForProcessResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/reservation/for-process';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Сброс резерва носителя под процесс
   */
  static forProcess1(
    params: {
      /** cmd */
      cmd: DropCarrierReservationForProcessCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DropCarrierReservationForProcessResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/carriers/reservation/for-process';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ReservationStockGatewayService {
  /**
   * Уменьшение входящего резерва запаса
   */
  static reduceIncomingQuantity(
    params: {
      /** request */
      request: ReduceIncomingQuantityCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reduceIncomingQuantity';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['request'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Точечная отмена резерва запаса
   */
  static cancelByStock(
    params: {
      /** cmd */
      cmd: CancelStockReservationCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservationCancelledResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/cancelByStock';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Получение списка резервов запаса по списку transferId
   */
  static reservations(
    params: {
      /** transferId */
      transferId?: string[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservationDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservations';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { transferId: params['transferId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Создание исходящего резерва запаса по id запаса
   */
  static reserveOutgoingQuantity(
    params: {
      /** cmd */
      cmd: ReserveStockOutgoingQuantityCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReserveStockOutgoingQuantityResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reserveOutgoingQuantity';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Отмена резервов запаса
   */
  static drop(
    params: {
      /** cmd */
      cmd: DropTransferCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TransferDroppedResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/transfers/drop';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ReservationPlaceGatewayService {
  /**
   * Резервирование мест под носители
   */
  static forCarriers(
    params: {
      /** cmd */
      cmd: ReservePlacesForCarriersCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservedPlacesForCarriersResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Сброс резерва места под носитель
   */
  static forCarriers1(
    params: {
      /** cmd */
      cmd: DropPlaceReservationsForTransferCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Резервирование мест в шутах сортера
   */
  static chute(
    params: {
      /** cmd */
      cmd: ReserveChutePlacesForCarriersCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservedChutePlacesForCarriersResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers/chute';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Замена резерва под носители в месте с sourceTransferId на targetTransferId
   */
  static exchange(
    params: {
      /** cmd */
      cmd: ExchangeChuteReservationCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ExchangeChuteReservationResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers/chute/exchange';

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Резервирование мест под носители для кластерной доставки
   */
  static clusterDelivery(
    params: {
      /** cmd */
      cmd: ReservePlacesForCarriersByClusterDeliveryCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservedPlacesForCarriersByClusterDeliveryResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers/cluster-delivery';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Резервирование мест под носители для нескольких процессов
   */
  static multiProcess(
    params: {
      /** cmd */
      cmd: ReservePlacesForCarriersByMultyProcessesCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ReservedPlacesForCarriersByMultiProcessesResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers/multi-process';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Уменьшить резерв под носители в месте
   */
  static reduce(
    params: {
      /** cmd */
      cmd: ReducePlaceReservationForTransferCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-carriers/reduce';

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Резервирование места под процесс
   */
  static forProcess(
    params: {
      /** cmd */
      cmd: ReservePlaceForProcessCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-process';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Сброс резервов всех мест под конкретный процесс
   */
  static allPlaces(
    params: {
      /** cmd */
      cmd: DropAllProcessReservationsCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-process/all-places';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Сброс резерва конкретного места под процесс
   */
  static onePlace(
    params: {
      /** cmd */
      cmd: DropPlaceReservationForProcessCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/places/reservation/for-process/one-place';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class StockTypeGatewayService {
  /**
   * Запрос списка видов запаса
   */
  static stockTypes(options: IRequestOptions = {}): Promise<StockTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Создание нового справочника вида запаса
   */
  static stockTypes1(
    params: {
      /** cmd */
      cmd: ModifyStockTypeCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос вида запаса по коду
   */
  static code(
    params: {
      /** code */
      code: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types/code/{code}';
      url = url.replace('{code}', params['code'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Проверка существует ли вид запаса с указанным кодом
   */
  static codeExists(
    params: {
      /** code */
      code: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CodeExists> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types/codeExists/{code}';
      url = url.replace('{code}', params['code'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Запрос вида запаса по идентификатору
   */
  static stockTypes2(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Обновление вида запаса
   */
  static stockTypes3(
    params: {
      /** cmd */
      cmd: ModifyStockTypeCmd;
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StockTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Установка признака заархивирован
   */
  static stockTypes4(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/stock/stock-types/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ToolGatewayService {
  /**
   * Отгрузка заказа
   */
  static pushOrder(
    params: {
      /** cmd */
      cmd: OutboundOrderCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/tool/pushOrder';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ZoneDictionaryGatewayService {
  /**
   * getZoneTypes
   */
  static zones(
    params: {
      /** siteId */
      siteId: string;
      /** zoneTypeId */
      zoneTypeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/dictionary/site/{siteId}/zoneType/{zoneTypeId}/zones';
      url = url.replace('{siteId}', params['siteId'] + '');
      url = url.replace('{zoneTypeId}', params['zoneTypeId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceTypeDictionaryGatewayService {
  /**
   * placeTypes
   */
  static placeTypes(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/dictionary/sites/{siteId}/placeTypes';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ZoneTypeDictionaryGatewayService {
  /**
   * getZoneTypes
   */
  static zoneTypes(options: IRequestOptions = {}): Promise<ZoneTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/dictionary/zoneTypes';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceStatusGatewayService {
  /**
   * getPlaceStatuses
   */
  static placeStatuses(options: IRequestOptions = {}): Promise<PlaceStatusDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeStatuses';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceTypeCharacteristicGatewayService {
  /**
   * getPlaceTypeCharacteristics
   */
  static characteristics(
    params: {
      /** placeTypeId */
      placeTypeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeCharacteristicDTO[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeTypes/{placeTypeId}/characteristics';
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * add
   */
  static characteristics1(
    params: {
      /** placeTypeId */
      placeTypeId: string;
      /** req */
      req: AddPlaceTypeCommonCharacteristicDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeCharacteristicDTO> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeTypes/{placeTypeId}/characteristics';
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['req'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getPlaceTypeCharacteristic
   */
  static characteristics2(
    params: {
      /** characteristicId */
      characteristicId: string;
      /** placeTypeId */
      placeTypeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeCharacteristicDTO> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
      url = url.replace('{characteristicId}', params['characteristicId'] + '');
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * update
   */
  static characteristics3(
    params: {
      /** characteristicId */
      characteristicId: string;
      /** placeTypeId */
      placeTypeId: string;
      /** req */
      req: UpdatePlaceTypeCommonCharacteristicDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeCharacteristicDTO> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
      url = url.replace('{characteristicId}', params['characteristicId'] + '');
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['req'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * remove
   */
  static characteristics4(
    params: {
      /** characteristicId */
      characteristicId: string;
      /** placeTypeId */
      placeTypeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/placeTypes/{placeTypeId}/characteristics/{characteristicId}';
      url = url.replace('{characteristicId}', params['characteristicId'] + '');
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceGatewayService {
  /**
   * getPlace
   */
  static places(
    params: {
      /** barcode */
      barcode?: string;
      /** placeId */
      placeId?: string;
      /** showChildren */
      showChildren?: boolean;
      /** siteCode */
      siteCode?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        barcode: params['barcode'],
        placeId: params['placeId'],
        showChildren: params['showChildren'],
        siteCode: params['siteCode']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * create
   */
  static places1(
    params: {
      /** cmd */
      cmd: CreatePlaceCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * findPlacesByProcessId
   */
  static findByProcessId(
    params: {
      /** placeTypeCode */
      placeTypeCode: string;
      /** processId */
      processId: string;
      /** showChildren */
      showChildren?: boolean;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/findByProcessId';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        placeTypeCode: params['placeTypeCode'],
        processId: params['processId'],
        showChildren: params['showChildren'],
        siteCode: params['siteCode']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getHierarchy
   */
  static hierarchy(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceNodeWithPosition> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/hierarchy/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getHierarchyForParentId
   */
  static hierarchy1(
    params: {
      /** parentId */
      parentId: string;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceNodeWithPosition> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/hierarchy/{siteId}/{parentId}';
      url = url.replace('{parentId}', params['parentId'] + '');
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getHierarchyByTypeCode
   */
  static hierarchyTree(
    params: {
      /** siteId */
      siteId: string;
      /** typeCode */
      typeCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceHierarchyTreeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/hierarchyTree/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { typeCode: params['typeCode'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getPlaceByCode
   */
  static shortView(
    params: {
      /** placeBarcode */
      placeBarcode: string;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceShortView[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/shortView';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { placeBarcode: params['placeBarcode'], siteCode: params['siteCode'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getTransferPlace
   */
  static transfer(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceShortView> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/transfer/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Изменение статуса места с удалением входящих резервов
   */
  static updatePlaceStatus(
    params: {
      /** cmd */
      cmd: UpdatePlaceStatusCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UpdatePlaceStatusResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/updatePlaceStatus';

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getPlaceById
   */
  static places2(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceWithPosition> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * update
   */
  static places3(
    params: {
      /** cmd */
      cmd: UpdatePlaceCmd;
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * delete
   */
  static places4(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceBulkOperationsGatewayService {
  /**
   * startBulkDeletePlaces
   */
  static deletePlaces(
    params: {
      /** cmd */
      cmd: BulkPlacesDeleteCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BulkOperationDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/bulk/delete-places';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * startBulkUpdatePlaceStatus
   */
  static updatePlaceStatus(
    params: {
      /** cmd */
      cmd: BulkPlaceUpdateStatusCmd;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BulkOperationDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/bulk/update-place-status';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);
      configs.params = { siteCode: params['siteCode'] };
      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * startBulkUpdatePlaceType
   */
  static updatePlaceType(
    params: {
      /** cmd */
      cmd: BulkPlaceUpdateTypeCmd;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BulkOperationDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/bulk/update-place-type';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);
      configs.params = { siteCode: params['siteCode'] };
      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * startBulkUpdateZone
   */
  static updateZone(
    params: {
      /** cmd */
      cmd: BulkPlaceUpdateZoneCmd;
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BulkOperationDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/bulk/update-zone';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);
      configs.params = { siteCode: params['siteCode'] };
      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getBulkUpdateOperationData
   */
  static bulk(
    params: {
      /** operationId */
      operationId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BulkOperationDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/bulk/{operationId}';
      url = url.replace('{operationId}', params['operationId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceSearchGatewayService {
  /**
   * Search Places
   */
  static search(
    params: {
      /** query */
      query: PlacesSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlacesSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/search';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Универсальный поиск мест с иерархией
   */
  static detailed(
    params: {
      /** searchQuery */
      searchQuery: DetailedPlaceSearchQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DetailedPlaceSearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/search/detailed';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['searchQuery'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getPlaceTypesBySite
   */
  static placeTypes(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/search/dicts/placeTypes/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getZoneTypesWithZones
   */
  static zoneTypes(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneTypeWithZones[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/search/dicts/zoneTypes/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceCharacteristicGatewayService {
  /**
   * getPlaceCharacteristics
   */
  static characteristics(
    params: {
      /** placeId */
      placeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceCharacteristicDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/{placeId}/characteristics';
      url = url.replace('{placeId}', params['placeId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getPlaceCharacteristic
   */
  static characteristics1(
    params: {
      /** characteristicId */
      characteristicId: string;
      /** placeId */
      placeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceCharacteristicDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/{placeId}/characteristics/{characteristicId}';
      url = url.replace('{characteristicId}', params['characteristicId'] + '');
      url = url.replace('{placeId}', params['placeId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * override
   */
  static characteristics2(
    params: {
      /** characteristicId */
      characteristicId: string;
      /** cmd */
      cmd: OverridePlaceCommonCharacteristicCmd;
      /** placeId */
      placeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceCharacteristicDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/places/{placeId}/characteristics/{characteristicId}';
      url = url.replace('{characteristicId}', params['characteristicId'] + '');
      url = url.replace('{placeId}', params['placeId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class SiteGatewayService {
  /**
   * getSites
   */
  static sites(options: IRequestOptions = {}): Promise<SiteDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * createSite
   */
  static sites1(
    params: {
      /** cmd */
      cmd: CreateSiteCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SiteDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getSiteByCode
   */
  static byCode(
    params: {
      /** code */
      code: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SiteDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/byCode/{code}';
      url = url.replace('{code}', params['code'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * codeExists
   */
  static codeExists(
    params: {
      /** query */
      query: SiteCodeExistsQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CodeExists> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/codeExists';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getSite
   */
  static sites2(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SiteDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * updateSite
   */
  static sites3(
    params: {
      /** cmd */
      cmd: UpdateSiteCmd;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SiteDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class SiteSettingsGatewayService {
  /**
   * getSettingsForSite
   */
  static settings(
    params: {
      /** siteCode */
      siteCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SiteSettingDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/settings/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * updateSettingsForSite
   */
  static settings1(
    params: {
      /** siteCode */
      siteCode: string;
      /** siteSettingDtos */
      siteSettingDtos: SiteSettingDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SiteSettingDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/settings/{siteCode}';
      url = url.replace('{siteCode}', params['siteCode'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['siteSettingDtos'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PlaceTypeGatewayService {
  /**
   * Запрос на получение типов мест для площадки с количеством их экземпляров
   */
  static placeTypes(
    params: {
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * createPlaceType
   */
  static placeTypes1(
    params: {
      /** cmd */
      cmd: CreatePlaceTypeCmd;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getAvailablePlaceTypesByParentPlaceId
   */
  static availableByParentPlaceId(
    params: {
      /** parentPlaceId */
      parentPlaceId?: string;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes/availableByParentPlaceId';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { parentPlaceId: params['parentPlaceId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * codeExists
   */
  static codeExists(
    params: {
      /** query */
      query: PlaceTypeCodeExistsQuery;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CodeExists> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes/codeExists';
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getPlaceType
   */
  static placeTypes2(
    params: {
      /** placeTypeId */
      placeTypeId: string;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * updatePlaceType
   */
  static placeTypes3(
    params: {
      /** cmd */
      cmd: UpdatePlaceTypeCmd;
      /** placeTypeId */
      placeTypeId: string;
      /** siteId */
      siteId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/sites/{siteId}/placeTypes/{placeTypeId}';
      url = url.replace('{placeTypeId}', params['placeTypeId'] + '');
      url = url.replace('{siteId}', params['siteId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ZoneTypeGatewayService {
  /**
   * getZoneTypes
   */
  static zoneTypes(options: IRequestOptions = {}): Promise<ZoneTypeDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * create
   */
  static zoneTypes1(
    params: {
      /** cmd */
      cmd: CreateZoneTypeCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * codeExists
   */
  static codeExists(
    params: {
      /** query */
      query: ZoneTypeCodeExistsQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CodeExists> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types/codeExists';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getZoneTypesWithZones
   */
  static withZones(
    params: {
      /** siteId */
      siteId?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneTypeWithZonesDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types/withZones';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { siteId: params['siteId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getZoneType
   */
  static zoneTypes2(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * delete
   */
  static zoneTypes3(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * update
   */
  static zoneTypes4(
    params: {
      /** cmd */
      cmd: UpdateZoneTypeCmd;
      /** zoneTypeId */
      zoneTypeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneTypeDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zone-types/{zoneTypeId}';
      url = url.replace('{zoneTypeId}', params['zoneTypeId'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ZoneGatewayService {
  /**
   * getZonesByType
   */
  static zones(
    params: {
      /** siteCode */
      siteCode: string;
      /** zoneTypeCode */
      zoneTypeCode: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { siteCode: params['siteCode'], zoneTypeCode: params['zoneTypeCode'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * create a Zone
   */
  static zones1(
    params: {
      /** cmd */
      cmd: CreateZoneCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * codeExists
   */
  static codeExists(
    params: {
      /** query */
      query: ZoneCodeExistsQuery;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CodeExists> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/codeExists';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['query'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getZoneById
   */
  static zones2(
    params: {
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * update the Zone
   */
  static zones3(
    params: {
      /** cmd */
      cmd: UpdateZoneCmd;
      /** id */
      id: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ZoneProjectionGatewayService {
  /**
   * getZonesBySiteIdAndTypeId
   */
  static projections(
    params: {
      /** siteId */
      siteId: string;
      /** typeId */
      typeId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ZoneProjection[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/topology/zones/projections';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { siteId: params['siteId'], typeId: params['typeId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class TransferGatewayService {
  /**
   * Проверка доступности места для трансфера носителя
   */
  static availabilityPlaceForTransferCarrier(
    params: {
      /** cmd */
      cmd: AvailabilityPlaceForTransferCarrierCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<AvailabilityPlaceForTransferCarrierResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/transfer/availabilityPlaceForTransferCarrier';

      const configs: IRequestConfig = getConfigs('put', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * getRoute
   */
  static getRoute(
    params: {
      /** carrierId */
      carrierId: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PlaceShortView[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/transfer/getRoute/{carrierId}';
      url = url.replace('{carrierId}', params['carrierId'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Перенос носителя в место
   */
  static moveCarrierToPlace(
    params: {
      /** cmd */
      cmd: MoveCarrierToPlaceCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MoveCarrierToPlaceResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/transfer/moveCarrierToPlace';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Размещение носителя в месте без проверок
   */
  static setCarrierPlace(
    params: {
      /** cmd */
      cmd: SetCarrierPlaceCmd;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/v1/transfer/setCarrierPlace';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params['cmd'];

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class AbstractWarehouseError {
  /**  */
  'code'?: string;

  /**  */
  'message'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['message'] = data['message'];
  }
}

export class AddPlaceTypeCommonCharacteristicDto {
  /**  */
  'characteristic'?: string;

  /**  */
  'defaultValue'?: DefaultValue;

  /**  */
  'settings'?: CommonCharacteristicSettings;

  constructor(data: undefined | any = {}) {
    this['characteristic'] = data['characteristic'];
    this['defaultValue'] = data['defaultValue'];
    this['settings'] = data['settings'];
  }
}

export class AddStockToCarrierCmd {
  /**  */
  'baseAmount'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumAddStockToCarrierCmdProcessType;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockType'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['stockType'] = data['stockType'];
    this['tabNumber'] = data['tabNumber'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class AllocationError {
  constructor(data: undefined | any = {}) {}
}

export class AllocationZoneTransferReq {
  /**  */
  'carrierId'?: string;

  /**  */
  'destinationPlaceId'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'startPlaceId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['destinationPlaceId'] = data['destinationPlaceId'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['startPlaceId'] = data['startPlaceId'];
    this['transferId'] = data['transferId'];
  }
}

export class AssignProductBatchCmd {
  /**  */
  'createMarksOnPicking'?: boolean;

  /**  */
  'expirationTime'?: Date;

  /**  */
  'inboundId'?: string;

  /**  */
  'inboundTime'?: Date;

  /**  */
  'manufactureTime'?: Date;

  /**  */
  'marksAccountingType'?: EnumAssignProductBatchCmdMarksAccountingType;

  /**  */
  'mercuryExtId'?: string;

  /**  */
  'number'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'productName'?: string;

  /**  */
  'referenceBNumber'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'type'?: EnumAssignProductBatchCmdType;

  /**  */
  'unit'?: string;

  /**  */
  'vendorCode'?: string;

  /**  */
  'vendorName'?: string;

  /**  */
  'weightProduct'?: boolean;

  constructor(data: undefined | any = {}) {
    this['createMarksOnPicking'] = data['createMarksOnPicking'];
    this['expirationTime'] = data['expirationTime'];
    this['inboundId'] = data['inboundId'];
    this['inboundTime'] = data['inboundTime'];
    this['manufactureTime'] = data['manufactureTime'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['mercuryExtId'] = data['mercuryExtId'];
    this['number'] = data['number'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['referenceBNumber'] = data['referenceBNumber'];
    this['siteCode'] = data['siteCode'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
    this['vendorCode'] = data['vendorCode'];
    this['vendorName'] = data['vendorName'];
    this['weightProduct'] = data['weightProduct'];
  }
}

export class AuditCarrierRecordDto {
  /**  */
  'carrierId'?: string;

  /**  */
  'carrierNumber'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'id'?: number;

  /**  */
  'login'?: string;

  /**  */
  'operation'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processType'?: string;

  /**  */
  'sourceCarrierReservationKey'?: string;

  /**  */
  'sourceParentId'?: string;

  /**  */
  'sourceParentNumber'?: string;

  /**  */
  'sourceParentTypeCode'?: string;

  /**  */
  'sourcePlaceAddress'?: string;

  /**  */
  'sourceSiteCode'?: string;

  /**  */
  'sourceTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierReservationKey'?: string;

  /**  */
  'targetParentId'?: string;

  /**  */
  'targetParentNumber'?: string;

  /**  */
  'targetParentTypeCode'?: string;

  /**  */
  'targetPlaceAddress'?: string;

  /**  */
  'targetSiteCode'?: string;

  /**  */
  'targetTypeCode'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['carrierNumber'] = data['carrierNumber'];
    this['createdTime'] = data['createdTime'];
    this['id'] = data['id'];
    this['login'] = data['login'];
    this['operation'] = data['operation'];
    this['operationId'] = data['operationId'];
    this['processType'] = data['processType'];
    this['sourceCarrierReservationKey'] = data['sourceCarrierReservationKey'];
    this['sourceParentId'] = data['sourceParentId'];
    this['sourceParentNumber'] = data['sourceParentNumber'];
    this['sourceParentTypeCode'] = data['sourceParentTypeCode'];
    this['sourcePlaceAddress'] = data['sourcePlaceAddress'];
    this['sourceSiteCode'] = data['sourceSiteCode'];
    this['sourceTypeCode'] = data['sourceTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierReservationKey'] = data['targetCarrierReservationKey'];
    this['targetParentId'] = data['targetParentId'];
    this['targetParentNumber'] = data['targetParentNumber'];
    this['targetParentTypeCode'] = data['targetParentTypeCode'];
    this['targetPlaceAddress'] = data['targetPlaceAddress'];
    this['targetSiteCode'] = data['targetSiteCode'];
    this['targetTypeCode'] = data['targetTypeCode'];
    this['userName'] = data['userName'];
  }
}

export class AuditCarrierRecordSearchQuery {
  /**  */
  'carrierId'?: string;

  /**  */
  'carrierNumber'?: string;

  /**  */
  'createdAtFrom': Date;

  /**  */
  'createdAtTo': Date;

  /**  */
  'login'?: string;

  /**  */
  'maxResponseSize'?: number;

  /**  */
  'operation'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processType'?: string;

  /**  */
  'sourceCarrierReservationKey'?: string;

  /**  */
  'sourceParentId'?: string;

  /**  */
  'sourceParentNumber'?: string;

  /**  */
  'sourceParentTypeCode'?: string;

  /**  */
  'sourcePlaceAddress'?: string;

  /**  */
  'sourceSiteCode'?: string;

  /**  */
  'sourceTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierReservationKey'?: string;

  /**  */
  'targetParentId'?: string;

  /**  */
  'targetParentNumber'?: string;

  /**  */
  'targetParentTypeCode'?: string;

  /**  */
  'targetPlaceAddress'?: string;

  /**  */
  'targetSiteCode'?: string;

  /**  */
  'targetTypeCode'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['carrierNumber'] = data['carrierNumber'];
    this['createdAtFrom'] = data['createdAtFrom'];
    this['createdAtTo'] = data['createdAtTo'];
    this['login'] = data['login'];
    this['maxResponseSize'] = data['maxResponseSize'];
    this['operation'] = data['operation'];
    this['operationId'] = data['operationId'];
    this['processType'] = data['processType'];
    this['sourceCarrierReservationKey'] = data['sourceCarrierReservationKey'];
    this['sourceParentId'] = data['sourceParentId'];
    this['sourceParentNumber'] = data['sourceParentNumber'];
    this['sourceParentTypeCode'] = data['sourceParentTypeCode'];
    this['sourcePlaceAddress'] = data['sourcePlaceAddress'];
    this['sourceSiteCode'] = data['sourceSiteCode'];
    this['sourceTypeCode'] = data['sourceTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierReservationKey'] = data['targetCarrierReservationKey'];
    this['targetParentId'] = data['targetParentId'];
    this['targetParentNumber'] = data['targetParentNumber'];
    this['targetParentTypeCode'] = data['targetParentTypeCode'];
    this['targetPlaceAddress'] = data['targetPlaceAddress'];
    this['targetSiteCode'] = data['targetSiteCode'];
    this['targetTypeCode'] = data['targetTypeCode'];
    this['userName'] = data['userName'];
  }
}

export class AuditCarrierRecordSearchResponse {
  /**  */
  'records'?: AuditCarrierRecordDto[];

  /**  */
  'recordsCount'?: number;

  constructor(data: undefined | any = {}) {
    this['records'] = data['records'];
    this['recordsCount'] = data['recordsCount'];
  }
}

export class AuditMarkRecordDto {
  /**  */
  'createdTime': number;

  /**  */
  'id'?: number;

  /**  */
  'inboundDeliveryId'?: string;

  /**  */
  'inboundDeliveryPosition'?: number;

  /**  */
  'level': EnumAuditMarkRecordDtoLevel;

  /**  */
  'login'?: string;

  /**  */
  'markCode': string;

  /**  */
  'operation': string;

  /**  */
  'parentCode'?: string;

  /**  */
  'process': string;

  /**  */
  'productId': string;

  /**  */
  'productName'?: string;

  /**  */
  'siteCode': string;

  /**  */
  'sourceOrderNumber'?: string;

  /**  */
  'sourceOrderPosition'?: number;

  /**  */
  'sourceProductBatchNumber'?: string;

  /**  */
  'sourceStatus'?: EnumAuditMarkRecordDtoSourceStatus;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetOrderNumber'?: string;

  /**  */
  'targetOrderPosition'?: number;

  /**  */
  'targetProductBatchNumber'?: string;

  /**  */
  'targetStatus'?: EnumAuditMarkRecordDtoTargetStatus;

  /**  */
  'type': EnumAuditMarkRecordDtoType;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['createdTime'] = data['createdTime'];
    this['id'] = data['id'];
    this['inboundDeliveryId'] = data['inboundDeliveryId'];
    this['inboundDeliveryPosition'] = data['inboundDeliveryPosition'];
    this['level'] = data['level'];
    this['login'] = data['login'];
    this['markCode'] = data['markCode'];
    this['operation'] = data['operation'];
    this['parentCode'] = data['parentCode'];
    this['process'] = data['process'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['siteCode'] = data['siteCode'];
    this['sourceOrderNumber'] = data['sourceOrderNumber'];
    this['sourceOrderPosition'] = data['sourceOrderPosition'];
    this['sourceProductBatchNumber'] = data['sourceProductBatchNumber'];
    this['sourceStatus'] = data['sourceStatus'];
    this['tabNumber'] = data['tabNumber'];
    this['targetOrderNumber'] = data['targetOrderNumber'];
    this['targetOrderPosition'] = data['targetOrderPosition'];
    this['targetProductBatchNumber'] = data['targetProductBatchNumber'];
    this['targetStatus'] = data['targetStatus'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class AuditMarkRecordSearchQuery {
  /**  */
  'createdAtFrom': Date;

  /**  */
  'createdAtTo': Date;

  /**  */
  'inboundDeliveryPosition'?: number;

  /**  */
  'level'?: EnumAuditMarkRecordSearchQueryLevel;

  /**  */
  'login'?: string;

  /**  */
  'markCode'?: string;

  /**  */
  'operation'?: string;

  /**  */
  'process'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'siteCode': string;

  /**  */
  'sourceProductBatchNumber'?: string;

  /**  */
  'sourceStatus'?: EnumAuditMarkRecordSearchQuerySourceStatus;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetProductBatchNumber'?: string;

  /**  */
  'targetStatus'?: EnumAuditMarkRecordSearchQueryTargetStatus;

  /**  */
  'type'?: EnumAuditMarkRecordSearchQueryType;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['createdAtFrom'] = data['createdAtFrom'];
    this['createdAtTo'] = data['createdAtTo'];
    this['inboundDeliveryPosition'] = data['inboundDeliveryPosition'];
    this['level'] = data['level'];
    this['login'] = data['login'];
    this['markCode'] = data['markCode'];
    this['operation'] = data['operation'];
    this['process'] = data['process'];
    this['productId'] = data['productId'];
    this['siteCode'] = data['siteCode'];
    this['sourceProductBatchNumber'] = data['sourceProductBatchNumber'];
    this['sourceStatus'] = data['sourceStatus'];
    this['tabNumber'] = data['tabNumber'];
    this['targetProductBatchNumber'] = data['targetProductBatchNumber'];
    this['targetStatus'] = data['targetStatus'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class AuditMarkRecordSearchResponse {
  /**  */
  'moreRecordsLeft'?: boolean;

  /**  */
  'records'?: AuditMarkRecordDto[];

  constructor(data: undefined | any = {}) {
    this['moreRecordsLeft'] = data['moreRecordsLeft'];
    this['records'] = data['records'];
  }
}

export class AuditStockRecordDto {
  /**  */
  'additionalInfo'?: object;

  /**  */
  'baseAmount': number;

  /**  */
  'baseUnit': string;

  /**  */
  'calculatedBaseAmount': boolean;

  /**  */
  'createdTime': number;

  /**  */
  'id'?: number;

  /**  */
  'operation': string;

  /**  */
  'process': string;

  /**  */
  'quantity': number;

  /**  */
  'siteCode': string;

  /**  */
  'sourceCarrierNumber'?: string;

  /**  */
  'sourcePlaceAddress'?: string;

  /**  */
  'sourceProductBatchId'?: string;

  /**  */
  'sourceProductBatchNumber'?: string;

  /**  */
  'sourceProductId'?: string;

  /**  */
  'sourceProductName'?: string;

  /**  */
  'sourceStockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierNumber'?: string;

  /**  */
  'targetPlaceAddress'?: string;

  /**  */
  'targetProductBatchId'?: string;

  /**  */
  'targetProductBatchNumber'?: string;

  /**  */
  'targetProductId'?: string;

  /**  */
  'targetProductName'?: string;

  /**  */
  'targetStockTypeCode'?: string;

  /**  */
  'unit': string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['additionalInfo'] = data['additionalInfo'];
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['calculatedBaseAmount'] = data['calculatedBaseAmount'];
    this['createdTime'] = data['createdTime'];
    this['id'] = data['id'];
    this['operation'] = data['operation'];
    this['process'] = data['process'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['sourceCarrierNumber'] = data['sourceCarrierNumber'];
    this['sourcePlaceAddress'] = data['sourcePlaceAddress'];
    this['sourceProductBatchId'] = data['sourceProductBatchId'];
    this['sourceProductBatchNumber'] = data['sourceProductBatchNumber'];
    this['sourceProductId'] = data['sourceProductId'];
    this['sourceProductName'] = data['sourceProductName'];
    this['sourceStockTypeCode'] = data['sourceStockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierNumber'] = data['targetCarrierNumber'];
    this['targetPlaceAddress'] = data['targetPlaceAddress'];
    this['targetProductBatchId'] = data['targetProductBatchId'];
    this['targetProductBatchNumber'] = data['targetProductBatchNumber'];
    this['targetProductId'] = data['targetProductId'];
    this['targetProductName'] = data['targetProductName'];
    this['targetStockTypeCode'] = data['targetStockTypeCode'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class AuditStockRecordSearchQuery {
  /**  */
  'createdAtFrom': Date;

  /**  */
  'createdAtTo': Date;

  /**  */
  'operation'?: string;

  /**  */
  'process'?: string;

  /**  */
  'siteCode': string;

  /**  */
  'sourceCarrierNumber'?: string;

  /**  */
  'sourcePlaceAddress'?: string;

  /**  */
  'sourceProductBatchNumber'?: string;

  /**  */
  'sourceProductId'?: string;

  /**  */
  'sourceStockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierNumber'?: string;

  /**  */
  'targetPlaceAddress'?: string;

  /**  */
  'targetProductBatchNumber'?: string;

  /**  */
  'targetProductId'?: string;

  /**  */
  'targetStockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['createdAtFrom'] = data['createdAtFrom'];
    this['createdAtTo'] = data['createdAtTo'];
    this['operation'] = data['operation'];
    this['process'] = data['process'];
    this['siteCode'] = data['siteCode'];
    this['sourceCarrierNumber'] = data['sourceCarrierNumber'];
    this['sourcePlaceAddress'] = data['sourcePlaceAddress'];
    this['sourceProductBatchNumber'] = data['sourceProductBatchNumber'];
    this['sourceProductId'] = data['sourceProductId'];
    this['sourceStockTypeCode'] = data['sourceStockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierNumber'] = data['targetCarrierNumber'];
    this['targetPlaceAddress'] = data['targetPlaceAddress'];
    this['targetProductBatchNumber'] = data['targetProductBatchNumber'];
    this['targetProductId'] = data['targetProductId'];
    this['targetStockTypeCode'] = data['targetStockTypeCode'];
  }
}

export class AuditStockRecordSearchResponse {
  /**  */
  'moreRecordsLeft'?: boolean;

  /**  */
  'records'?: AuditStockRecordDto[];

  constructor(data: undefined | any = {}) {
    this['moreRecordsLeft'] = data['moreRecordsLeft'];
    this['records'] = data['records'];
  }
}

export class AvailabilityPlaceForTransferCarrierCmd {
  /**  */
  'carrierTypeCodes': string[];

  /**  */
  'siteCode': string;

  /**  */
  'transferId': string;

  constructor(data: undefined | any = {}) {
    this['carrierTypeCodes'] = data['carrierTypeCodes'];
    this['siteCode'] = data['siteCode'];
    this['transferId'] = data['transferId'];
  }
}

export class AvailabilityPlaceForTransferCarrierResponse {
  /**  */
  'availableCarrierTypes': AvailableCarrierTypeDto[];

  constructor(data: undefined | any = {}) {
    this['availableCarrierTypes'] = data['availableCarrierTypes'];
  }
}

export class AvailableCarrierTypeDto {
  /**  */
  'availabilityStatus': boolean;

  /**  */
  'carrierTypeCode': string;

  constructor(data: undefined | any = {}) {
    this['availabilityStatus'] = data['availabilityStatus'];
    this['carrierTypeCode'] = data['carrierTypeCode'];
  }
}

export class BoxRoute {
  /**  */
  'operationId'?: string;

  /**  */
  'plannedTotalDuration'?: number;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'rejectedItems'?: ProductAmountDto[];

  /**  */
  'steps'?: RouteStep[];

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['plannedTotalDuration'] = data['plannedTotalDuration'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['rejectedItems'] = data['rejectedItems'];
    this['steps'] = data['steps'];
    this['transferId'] = data['transferId'];
  }
}

export class BoxRouteCmd {
  /**  */
  'items'?: ProductAmountItem[];

  /**  */
  'joinedItemByProductId'?: object;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processType'?: EnumBoxRouteCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'useRoute'?: EnumBoxRouteCmdUseRoute;

  /**  */
  'useStartPlace'?: EnumBoxRouteCmdUseStartPlace;

  constructor(data: undefined | any = {}) {
    this['items'] = data['items'];
    this['joinedItemByProductId'] = data['joinedItemByProductId'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['useRoute'] = data['useRoute'];
    this['useStartPlace'] = data['useStartPlace'];
  }
}

export class BoxRouteResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'rejectedItems'?: RejectedProductItemAmount[];

  /**  */
  'transferRoutes'?: TransferRoute[];

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['rejectedItems'] = data['rejectedItems'];
    this['transferRoutes'] = data['transferRoutes'];
  }
}

export class BuildBoxRouteCmd {
  /**  */
  'items'?: ProductAmountDto[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['items'] = data['items'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
    this['transferId'] = data['transferId'];
  }
}

export class BulkOperationDto {
  /**  */
  'operationId'?: string;

  /**  */
  'status'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['status'] = data['status'];
  }
}

export class BulkPlaceUpdateStatusCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeIds'?: string[];

  /**  */
  'statusCode'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeIds'] = data['placeIds'];
    this['statusCode'] = data['statusCode'];
  }
}

export class BulkPlaceUpdateTypeCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeIds'?: string[];

  /**  */
  'targetPlaceTypeId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeIds'] = data['placeIds'];
    this['targetPlaceTypeId'] = data['targetPlaceTypeId'];
  }
}

export class BulkPlaceUpdateZoneCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeIds'?: string[];

  /**  */
  'zoneId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeIds'] = data['placeIds'];
    this['zoneId'] = data['zoneId'];
  }
}

export class BulkPlacesDeleteCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeIds'?: string[];

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeIds'] = data['placeIds'];
    this['siteCode'] = data['siteCode'];
  }
}

export class CalculateNewProductPeiQuantityCmd {
  /**  */
  'productId': string;

  /**  */
  'quantity': number;

  /**  */
  'stockId': string;

  constructor(data: undefined | any = {}) {
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
  }
}

export class CalculateNewProductPeiQuantityResponse {
  /**  */
  'quantity': number;

  /**  */
  'unit': string;

  constructor(data: undefined | any = {}) {
    this['quantity'] = data['quantity'];
    this['unit'] = data['unit'];
  }
}

export class CancelStockReservationCmd {
  /**  */
  'incomingQuantity'?: number;

  /**  */
  'operationId'?: string;

  /**  */
  'outgoingQuantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['incomingQuantity'] = data['incomingQuantity'];
    this['operationId'] = data['operationId'];
    this['outgoingQuantity'] = data['outgoingQuantity'];
    this['siteCode'] = data['siteCode'];
    this['stockId'] = data['stockId'];
    this['transferId'] = data['transferId'];
    this['unit'] = data['unit'];
  }
}

export class CarrierDto {
  /**  */
  'author'?: string;

  /**  */
  'barcode'?: string;

  /**  */
  'carriersTypeId'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'nestedCarriers'?: CarrierDto[];

  /**  */
  'parentId'?: string;

  /**  */
  'parentNumber'?: string;

  /**  */
  'placeAddress'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'type'?: CarrierTypeDto;

  /**  */
  'updatedTime'?: number;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['barcode'] = data['barcode'];
    this['carriersTypeId'] = data['carriersTypeId'];
    this['createdTime'] = data['createdTime'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['nestedCarriers'] = data['nestedCarriers'];
    this['parentId'] = data['parentId'];
    this['parentNumber'] = data['parentNumber'];
    this['placeAddress'] = data['placeAddress'];
    this['placeId'] = data['placeId'];
    this['processId'] = data['processId'];
    this['type'] = data['type'];
    this['updatedTime'] = data['updatedTime'];
  }
}

export class CarrierGridView {
  /**  */
  'barcode'?: string;

  /**  */
  'id'?: string;

  /**  */
  'nestedCarriers'?: CarrierGridView[];

  /**  */
  'status'?: EnumCarrierGridViewStatus;

  /**  */
  'type'?: CarrierTypeGridView;

  constructor(data: undefined | any = {}) {
    this['barcode'] = data['barcode'];
    this['id'] = data['id'];
    this['nestedCarriers'] = data['nestedCarriers'];
    this['status'] = data['status'];
    this['type'] = data['type'];
  }
}

export class CarrierInfo {
  /**  */
  'carrierId'?: string;

  /**  */
  'carrierNumber'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['carrierNumber'] = data['carrierNumber'];
  }
}

export class CarrierShippedResponse {
  /**  */
  'carrierId'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'placeId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['operationId'] = data['operationId'];
    this['placeId'] = data['placeId'];
  }
}

export class CarrierTransferReservationInfo {
  /**  */
  'carrierTypeCode'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['quantity'] = data['quantity'];
    this['transferId'] = data['transferId'];
  }
}

export class CarrierTypeCodeExistsQuery {
  /**  */
  'code'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
  }
}

export class CarrierTypeDto {
  /**  */
  'archived'?: boolean;

  /**  */
  'author'?: string;

  /**  */
  'code'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'description'?: string;

  /**  */
  'editor'?: string;

  /**  */
  'height'?: number;

  /**  */
  'id'?: string;

  /**  */
  'length'?: number;

  /**  */
  'maxVolume'?: number;

  /**  */
  'maxWeight'?: number;

  /**  */
  'name'?: string;

  /**  */
  'updatedTime'?: number;

  /**  */
  'width'?: number;

  constructor(data: undefined | any = {}) {
    this['archived'] = data['archived'];
    this['author'] = data['author'];
    this['code'] = data['code'];
    this['createdTime'] = data['createdTime'];
    this['description'] = data['description'];
    this['editor'] = data['editor'];
    this['height'] = data['height'];
    this['id'] = data['id'];
    this['length'] = data['length'];
    this['maxVolume'] = data['maxVolume'];
    this['maxWeight'] = data['maxWeight'];
    this['name'] = data['name'];
    this['updatedTime'] = data['updatedTime'];
    this['width'] = data['width'];
  }
}

export class CarrierTypeGridView {
  /**  */
  'code'?: string;

  /**  */
  'description'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['description'] = data['description'];
    this['id'] = data['id'];
    this['name'] = data['name'];
  }
}

export class CarrierUploadResponse {
  /**  */
  'reservedCarriersWithProcesses': object;

  constructor(data: undefined | any = {}) {
    this['reservedCarriersWithProcesses'] = data['reservedCarriersWithProcesses'];
  }
}

export class ChangeMarksStatusCmd {
  /**  */
  'marks'?: MarkChangeStatusDTO[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumChangeMarksStatusCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['marks'] = data['marks'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class ChangeMarksStatusResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'marks'?: ResponseMarkDTO[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['marks'] = data['marks'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ChangeRawStock {
  /**  */
  'position'?: number;

  /**  */
  'stockId'?: string;

  constructor(data: undefined | any = {}) {
    this['position'] = data['position'];
    this['stockId'] = data['stockId'];
  }
}

export class ChangeRawStockTypeCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'orderVersion'?: string;

  /**  */
  'outboundDeliveryNumber'?: string;

  /**  */
  'outboundTime'?: Date;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumChangeRawStockTypeCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'stocks'?: ChangeRawStock[];

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['orderVersion'] = data['orderVersion'];
    this['outboundDeliveryNumber'] = data['outboundDeliveryNumber'];
    this['outboundTime'] = data['outboundTime'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['stocks'] = data['stocks'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class ChangeStockProductCmd {
  /**  */
  'operationId': string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumChangeStockProductCmdProcessType;

  /**  */
  'productId': string;

  /**  */
  'quantity': number;

  /**  */
  'stockId': string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class ChangeStockProductResponse {
  /**  */
  'newStockId'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumChangeStockProductResponseProcessType;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['newStockId'] = data['newStockId'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class CharacteristicDto {
  /**  */
  'type'?: string;

  /**  */
  'value'?: ValueDto;

  constructor(data: undefined | any = {}) {
    this['type'] = data['type'];
    this['value'] = data['value'];
  }
}

export class CheckPlaceForStockCmd {
  /**  */
  'processBusinessKey'?: string;

  /**  */
  'quantity': number;

  /**  */
  'siteCode': string;

  /**  */
  'stockId': string;

  /**  */
  'unit': string;

  constructor(data: undefined | any = {}) {
    this['processBusinessKey'] = data['processBusinessKey'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['stockId'] = data['stockId'];
    this['unit'] = data['unit'];
  }
}

export class CheckPlaceForStockResponse {
  /**  */
  'approvedQuantity'?: number;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'rejectedInfo'?: RejectedInfo;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['approvedQuantity'] = data['approvedQuantity'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['rejectedInfo'] = data['rejectedInfo'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ClusterDeliveryReservationRoute {
  /**  */
  'author'?: string;

  /**  */
  'createdTime': number;

  /**  */
  'editor'?: string;

  /**  */
  'steps': RouteStep[];

  /**  */
  'updatedTime': number;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['createdTime'] = data['createdTime'];
    this['editor'] = data['editor'];
    this['steps'] = data['steps'];
    this['updatedTime'] = data['updatedTime'];
  }
}

export class CodeExists {
  /**  */
  'exists'?: boolean;

  constructor(data: undefined | any = {}) {
    this['exists'] = data['exists'];
  }
}

export class CommonCharacteristicSettings {
  /**  */
  'editable'?: boolean;

  /**  */
  'mandatory'?: boolean;

  /**  */
  'parentInheritance'?: EnumCommonCharacteristicSettingsParentInheritance;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['editable'] = data['editable'];
    this['mandatory'] = data['mandatory'];
    this['parentInheritance'] = data['parentInheritance'];
    this['unit'] = data['unit'];
  }
}

export class ConfirmProductBatchCmd {
  /**  */
  'batchId'?: string;

  /**  */
  'targetStockType'?: string;

  constructor(data: undefined | any = {}) {
    this['batchId'] = data['batchId'];
    this['targetStockType'] = data['targetStockType'];
  }
}

export class Coordinates3D {
  /**  */
  'x'?: number;

  /**  */
  'y'?: number;

  /**  */
  'z'?: number;

  constructor(data: undefined | any = {}) {
    this['x'] = data['x'];
    this['y'] = data['y'];
    this['z'] = data['z'];
  }
}

export class CoordinatesDto {
  /**  */
  'x'?: number;

  /**  */
  'y'?: number;

  /**  */
  'z'?: number;

  constructor(data: undefined | any = {}) {
    this['x'] = data['x'];
    this['y'] = data['y'];
    this['z'] = data['z'];
  }
}

export class CreateCarrierCmd {
  /**  */
  'carrierBarcode'?: string;

  /**  */
  'carrierId'?: string;

  /**  */
  'carrierReservationKey'?: string;

  /**  */
  'carrierTypeCode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'placeBarcode'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumCreateCarrierCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierBarcode'] = data['carrierBarcode'];
    this['carrierId'] = data['carrierId'];
    this['carrierReservationKey'] = data['carrierReservationKey'];
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['operationId'] = data['operationId'];
    this['placeBarcode'] = data['placeBarcode'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class CreateCarrierTypeCmd {
  /**  */
  'code'?: string;

  /**  */
  'description'?: string;

  /**  */
  'height'?: number;

  /**  */
  'id'?: string;

  /**  */
  'length'?: number;

  /**  */
  'maxVolume'?: number;

  /**  */
  'maxWeight'?: number;

  /**  */
  'name'?: string;

  /**  */
  'width'?: number;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['description'] = data['description'];
    this['height'] = data['height'];
    this['id'] = data['id'];
    this['length'] = data['length'];
    this['maxVolume'] = data['maxVolume'];
    this['maxWeight'] = data['maxWeight'];
    this['name'] = data['name'];
    this['width'] = data['width'];
  }
}

export class CreateMarksCmd {
  /**  */
  'marks'?: MarkCreateDTO[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumCreateMarksCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['marks'] = data['marks'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class CreateMarksResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class CreatePlaceCmd {
  /**  */
  'address'?: string;

  /**  */
  'addressTypeId'?: string;

  /**  */
  'coordinates'?: Coordinates3D;

  /**  */
  'description'?: string;

  /**  */
  'id'?: string;

  /**  */
  'number'?: number;

  /**  */
  'parentId'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'status'?: PlaceStatusDto;

  /**  */
  'statusReason'?: string;

  /**  */
  'typeId'?: string;

  /**  */
  'zoneIds'?: string[];

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['addressTypeId'] = data['addressTypeId'];
    this['coordinates'] = data['coordinates'];
    this['description'] = data['description'];
    this['id'] = data['id'];
    this['number'] = data['number'];
    this['parentId'] = data['parentId'];
    this['siteId'] = data['siteId'];
    this['status'] = data['status'];
    this['statusReason'] = data['statusReason'];
    this['typeId'] = data['typeId'];
    this['zoneIds'] = data['zoneIds'];
  }
}

export class CreatePlaceTypeCmd {
  /**  */
  'code'?: string;

  /**  */
  'coordinatesRequired'?: boolean;

  /**  */
  'description'?: string;

  /**  */
  'maxMixBatches'?: number;

  /**  */
  'maxMixProducts'?: number;

  /**  */
  'name'?: string;

  /**  */
  'numerationRule'?: EnumCreatePlaceTypeCmdNumerationRule;

  /**  */
  'placeTypeId'?: string;

  /**  */
  'storagePlace'?: boolean;

  /**  */
  'virtualPlace'?: boolean;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['coordinatesRequired'] = data['coordinatesRequired'];
    this['description'] = data['description'];
    this['maxMixBatches'] = data['maxMixBatches'];
    this['maxMixProducts'] = data['maxMixProducts'];
    this['name'] = data['name'];
    this['numerationRule'] = data['numerationRule'];
    this['placeTypeId'] = data['placeTypeId'];
    this['storagePlace'] = data['storagePlace'];
    this['virtualPlace'] = data['virtualPlace'];
  }
}

export class CreateRouteStrategyCmd {
  /** Название стратегии (русскоязычное понятное навание стратегии) */
  'name': string;

  /** Код площадки */
  'siteCode': string;

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['siteCode'] = data['siteCode'];
  }
}

export class CreateScannedChzMarkCmd {
  /**  */
  'code'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumCreateScannedChzMarkCmdProcessType;

  /**  */
  'productBatchId'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'status'?: EnumCreateScannedChzMarkCmdStatus;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['productBatchId'] = data['productBatchId'];
    this['siteCode'] = data['siteCode'];
    this['status'] = data['status'];
    this['tabNumber'] = data['tabNumber'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class CreateScannedChzMarkResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class CreateSiteCmd {
  /**  */
  'code'?: string;

  /**  */
  'description'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['description'] = data['description'];
    this['id'] = data['id'];
    this['name'] = data['name'];
  }
}

export class CreateZoneCmd {
  /**  */
  'boundedWarehouseProductGroups'?: WarehouseProductGroupDto[];

  /**  */
  'code'?: string;

  /**  */
  'description'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'typeId'?: string;

  constructor(data: undefined | any = {}) {
    this['boundedWarehouseProductGroups'] = data['boundedWarehouseProductGroups'];
    this['code'] = data['code'];
    this['description'] = data['description'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['siteId'] = data['siteId'];
    this['typeId'] = data['typeId'];
  }
}

export class CreateZoneTypeCmd {
  /**  */
  'code'?: string;

  /**  */
  'description'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['description'] = data['description'];
    this['id'] = data['id'];
    this['name'] = data['name'];
  }
}

export class Data {
  /**  */
  'address'?: string;

  /**  */
  'author'?: string;

  /**  */
  'childrenAmount'?: number;

  /**  */
  'createdTime'?: number;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'status'?: PlaceStatusDto;

  /**  */
  'typeName'?: string;

  /**  */
  'updatedTime'?: number;

  /**  */
  'zoneNames'?: string[];

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['author'] = data['author'];
    this['childrenAmount'] = data['childrenAmount'];
    this['createdTime'] = data['createdTime'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['status'] = data['status'];
    this['typeName'] = data['typeName'];
    this['updatedTime'] = data['updatedTime'];
    this['zoneNames'] = data['zoneNames'];
  }
}

export class DefaultValue {
  /**  */
  'decValue'?: number;

  /**  */
  'intValue'?: number;

  /**  */
  'maxIntValue'?: number;

  /**  */
  'minIntValue'?: number;

  constructor(data: undefined | any = {}) {
    this['decValue'] = data['decValue'];
    this['intValue'] = data['intValue'];
    this['maxIntValue'] = data['maxIntValue'];
    this['minIntValue'] = data['minIntValue'];
  }
}

export class DeleteMarksCmd {
  /**  */
  'marks'?: MarkDeleteDTO[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumDeleteMarksCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['marks'] = data['marks'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class DeleteMarksResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class DeletePlacesFromHazelcastCmd {
  /**  */
  'siteCode'?: string;

  /**  */
  'storageAreaCodes'?: string[];

  constructor(data: undefined | any = {}) {
    this['siteCode'] = data['siteCode'];
    this['storageAreaCodes'] = data['storageAreaCodes'];
  }
}

export class DetailedCarriersSearchQuery {
  /**  */
  'barcodes'?: string[];

  constructor(data: undefined | any = {}) {
    this['barcodes'] = data['barcodes'];
  }
}

export class DetailedPlaceSearchFilter {
  /**  */
  'placeCategory'?: EnumDetailedPlaceSearchFilterPlaceCategory;

  /**  */
  'placeIds'?: string[];

  /**  */
  'placeStatus'?: string;

  /**  */
  'placeTypeCode'?: string;

  /**  */
  'siteCode': string;

  /**  */
  'withHierarchy'?: boolean;

  /**  */
  'zoneCode'?: string;

  /**  */
  'zoneId'?: string;

  /**  */
  'zoneTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['placeCategory'] = data['placeCategory'];
    this['placeIds'] = data['placeIds'];
    this['placeStatus'] = data['placeStatus'];
    this['placeTypeCode'] = data['placeTypeCode'];
    this['siteCode'] = data['siteCode'];
    this['withHierarchy'] = data['withHierarchy'];
    this['zoneCode'] = data['zoneCode'];
    this['zoneId'] = data['zoneId'];
    this['zoneTypeCode'] = data['zoneTypeCode'];
  }
}

export class DetailedPlaceSearchQuery {
  /**  */
  'filter': DetailedPlaceSearchFilter;

  /**  */
  'maxResponseSize'?: number;

  /**  */
  'pageNumber'?: number;

  constructor(data: undefined | any = {}) {
    this['filter'] = data['filter'];
    this['maxResponseSize'] = data['maxResponseSize'];
    this['pageNumber'] = data['pageNumber'];
  }
}

export class DetailedPlaceSearchResult {
  /**  */
  'places'?: PlaceDto[];

  /**  */
  'totalCount'?: number;

  constructor(data: undefined | any = {}) {
    this['places'] = data['places'];
    this['totalCount'] = data['totalCount'];
  }
}

export class DetailedStockSearchQuery {
  /**  */
  'carrierId'?: string;

  /**  */
  'carrierNumber'?: string;

  /**  */
  'inCarrier'?: EnumDetailedStockSearchQueryInCarrier;

  /**  */
  'inVirtualPlace'?: EnumDetailedStockSearchQueryInVirtualPlace;

  /**  */
  'maxResponseSize'?: number;

  /**  */
  'placeAddress'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productBatchNumber'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'productIds'?: string[];

  /**  */
  'siteCode'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'stockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['carrierNumber'] = data['carrierNumber'];
    this['inCarrier'] = data['inCarrier'];
    this['inVirtualPlace'] = data['inVirtualPlace'];
    this['maxResponseSize'] = data['maxResponseSize'];
    this['placeAddress'] = data['placeAddress'];
    this['placeId'] = data['placeId'];
    this['productBatchId'] = data['productBatchId'];
    this['productBatchNumber'] = data['productBatchNumber'];
    this['productId'] = data['productId'];
    this['productIds'] = data['productIds'];
    this['siteCode'] = data['siteCode'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
  }
}

export class DetailedStocksSearchResult {
  /**  */
  'stocks'?: StockDto[];

  /**  */
  'totalCount'?: number;

  constructor(data: undefined | any = {}) {
    this['stocks'] = data['stocks'];
    this['totalCount'] = data['totalCount'];
  }
}

export class DropAllProcessReservationsCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processId'] = data['processId'];
    this['siteCode'] = data['siteCode'];
  }
}

export class DropCarrierReservationForProcessCmd {
  /**  */
  'carrierNumber'?: string;

  /**  */
  'dropForNestedCarriers'?: boolean;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumDropCarrierReservationForProcessCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierNumber'] = data['carrierNumber'];
    this['dropForNestedCarriers'] = data['dropForNestedCarriers'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class DropCarrierReservationForProcessResponse {
  /**  */
  'carrierIds'?: string[];

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierIds'] = data['carrierIds'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class DropPlaceReservationForProcessCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeBarcode'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeBarcode'] = data['placeBarcode'];
    this['processId'] = data['processId'];
    this['siteCode'] = data['siteCode'];
  }
}

export class DropPlaceReservationsForTransferCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['siteCode'] = data['siteCode'];
    this['transferId'] = data['transferId'];
  }
}

export class DropTransferCmd {
  /**  */
  'processBusinessKey'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['processBusinessKey'] = data['processBusinessKey'];
    this['transferId'] = data['transferId'];
  }
}

export class ExchangeChuteReservationCmd {
  /**  */
  'carrierTypeCode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumExchangeChuteReservationCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'sourceTransferId'?: string;

  /**  */
  'targetTransferId'?: string;

  /**  */
  'zoneCodes'?: string[];

  constructor(data: undefined | any = {}) {
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['sourceTransferId'] = data['sourceTransferId'];
    this['targetTransferId'] = data['targetTransferId'];
    this['zoneCodes'] = data['zoneCodes'];
  }
}

export class ExchangeChuteReservationResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'sourceTransferId'?: string;

  /**  */
  'targetReserveInfo'?: ReserveInfo;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
    this['sourceTransferId'] = data['sourceTransferId'];
    this['targetReserveInfo'] = data['targetReserveInfo'];
  }
}

export class FullAmount {
  /**  */
  'quantity'?: number;

  /**  */
  'unit'?: string;

  /**  */
  'volume'?: number;

  /**  */
  'weight'?: number;

  constructor(data: undefined | any = {}) {
    this['quantity'] = data['quantity'];
    this['unit'] = data['unit'];
    this['volume'] = data['volume'];
    this['weight'] = data['weight'];
  }
}

export class GeneralWarehouseError {
  constructor(data: undefined | any = {}) {}
}

export class InitialPopulateHazelcastCmd {
  /**  */
  'siteCode'?: string;

  /**  */
  'storageAreaCodes'?: string[];

  constructor(data: undefined | any = {}) {
    this['siteCode'] = data['siteCode'];
    this['storageAreaCodes'] = data['storageAreaCodes'];
  }
}

export class MarkChangeStatusDTO {
  /**  */
  'code'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'sourceStatus'?: EnumMarkChangeStatusDTOSourceStatus;

  /**  */
  'targetStatus'?: EnumMarkChangeStatusDTOTargetStatus;

  /**  */
  'type'?: EnumMarkChangeStatusDTOType;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['sourceStatus'] = data['sourceStatus'];
    this['targetStatus'] = data['targetStatus'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
  }
}

export class MarkCreateDTO {
  /**  */
  'code'?: string;

  /**  */
  'inboundDeliveryId'?: string;

  /**  */
  'inboundDeliveryPosition'?: number;

  /**  */
  'level'?: EnumMarkCreateDTOLevel;

  /**  */
  'parentCode'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'status'?: EnumMarkCreateDTOStatus;

  /**  */
  'type'?: EnumMarkCreateDTOType;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['inboundDeliveryId'] = data['inboundDeliveryId'];
    this['inboundDeliveryPosition'] = data['inboundDeliveryPosition'];
    this['level'] = data['level'];
    this['parentCode'] = data['parentCode'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['status'] = data['status'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
  }
}

export class MarkDTO {
  /**  */
  'code': string;

  /**  */
  'createdAt': number;

  /**  */
  'inboundDeliveryId'?: string;

  /**  */
  'inboundDeliveryPosition'?: number;

  /**  */
  'level': EnumMarkDTOLevel;

  /**  */
  'markId'?: string;

  /**  */
  'markStatus': EnumMarkDTOMarkStatus;

  /**  */
  'orderNumber'?: string;

  /**  */
  'orderPosition'?: number;

  /**  */
  'parentCode'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productBatchNumber'?: string;

  /**  */
  'productId': string;

  /**  */
  'productName'?: string;

  /**  */
  'type': EnumMarkDTOType;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['createdAt'] = data['createdAt'];
    this['inboundDeliveryId'] = data['inboundDeliveryId'];
    this['inboundDeliveryPosition'] = data['inboundDeliveryPosition'];
    this['level'] = data['level'];
    this['markId'] = data['markId'];
    this['markStatus'] = data['markStatus'];
    this['orderNumber'] = data['orderNumber'];
    this['orderPosition'] = data['orderPosition'];
    this['parentCode'] = data['parentCode'];
    this['productBatchId'] = data['productBatchId'];
    this['productBatchNumber'] = data['productBatchNumber'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
  }
}

export class MarkDeleteDTO {
  /**  */
  'code'?: string;

  /**  */
  'type'?: EnumMarkDeleteDTOType;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['type'] = data['type'];
  }
}

export class MarkError {
  constructor(data: undefined | any = {}) {}
}

export class MarkSearchDTO {
  /**  */
  'code'?: string;

  /**  */
  'type'?: EnumMarkSearchDTOType;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['type'] = data['type'];
  }
}

export class MarkSearchFilter {
  /**  */
  'createdAtFrom'?: Date;

  /**  */
  'createdAtTo'?: Date;

  /**  */
  'inboundDeliveryId'?: string;

  /**  */
  'inboundDeliveryPosition'?: number;

  /**  */
  'level'?: EnumMarkSearchFilterLevel;

  /**  */
  'marks'?: MarkSearchDTO[];

  /**  */
  'parentCode'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productBatchNumber'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'siteCode': string;

  /**  */
  'statuses'?: EnumMarkSearchFilterStatuses[];

  /**  */
  'type'?: EnumMarkSearchFilterType;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['createdAtFrom'] = data['createdAtFrom'];
    this['createdAtTo'] = data['createdAtTo'];
    this['inboundDeliveryId'] = data['inboundDeliveryId'];
    this['inboundDeliveryPosition'] = data['inboundDeliveryPosition'];
    this['level'] = data['level'];
    this['marks'] = data['marks'];
    this['parentCode'] = data['parentCode'];
    this['productBatchId'] = data['productBatchId'];
    this['productBatchNumber'] = data['productBatchNumber'];
    this['productId'] = data['productId'];
    this['siteCode'] = data['siteCode'];
    this['statuses'] = data['statuses'];
    this['type'] = data['type'];
    this['unit'] = data['unit'];
  }
}

export class MarkSearchQuery {
  /**  */
  'filter'?: MarkSearchFilter;

  /**  */
  'maxResponseSize'?: number;

  /**  */
  'orderInfo'?: OrderByCriteria[];

  /**  */
  'pageNumber'?: number;

  constructor(data: undefined | any = {}) {
    this['filter'] = data['filter'];
    this['maxResponseSize'] = data['maxResponseSize'];
    this['orderInfo'] = data['orderInfo'];
    this['pageNumber'] = data['pageNumber'];
  }
}

export class MarkSearchResult {
  /**  */
  'marks'?: MarkDTO[];

  /**  */
  'totalCount'?: number;

  constructor(data: undefined | any = {}) {
    this['marks'] = data['marks'];
    this['totalCount'] = data['totalCount'];
  }
}

export class MarkUpdateDTO {
  /**  */
  'code'?: string;

  /**  */
  'orderNumber'?: string;

  /**  */
  'orderPosition'?: number;

  /**  */
  'productBatchId'?: string;

  /**  */
  'type'?: EnumMarkUpdateDTOType;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['orderNumber'] = data['orderNumber'];
    this['orderPosition'] = data['orderPosition'];
    this['productBatchId'] = data['productBatchId'];
    this['type'] = data['type'];
  }
}

export class ModifyStockTypeCmd {
  /**  */
  'code': string;

  /**  */
  'name'?: string;

  /**  */
  'writeOffAllowed': boolean;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['name'] = data['name'];
    this['writeOffAllowed'] = data['writeOffAllowed'];
  }
}

export class MonoPalletPlacementRequest {
  /**  */
  'incomingCarrierId'?: string;

  /**  */
  'openCarriers'?: string[];

  /**  */
  'operationId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'stockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['incomingCarrierId'] = data['incomingCarrierId'];
    this['openCarriers'] = data['openCarriers'];
    this['operationId'] = data['operationId'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['stockTypeCode'] = data['stockTypeCode'];
  }
}

export class MoveAllStocksToCarrierCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumMoveAllStocksToCarrierCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'sourcePlaceId'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierId'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['sourcePlaceId'] = data['sourcePlaceId'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierId'] = data['targetCarrierId'];
    this['transferId'] = data['transferId'];
    this['userName'] = data['userName'];
  }
}

export class MoveAllStocksToCarrierResponse {
  /**  */
  'operationId'?: string;

  /**  */
  'stockDto'?: StockDto[];

  /**  */
  'targetCarrierId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['stockDto'] = data['stockDto'];
    this['targetCarrierId'] = data['targetCarrierId'];
    this['transferId'] = data['transferId'];
  }
}

export class MoveCarrierToPlaceCmd {
  /**  */
  'carrierId'?: string;

  /**  */
  'carrierReservationKey'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumMoveCarrierToPlaceCmdProcessType;

  /**  */
  'tabNumber'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['carrierReservationKey'] = data['carrierReservationKey'];
    this['operationId'] = data['operationId'];
    this['placeId'] = data['placeId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['tabNumber'] = data['tabNumber'];
    this['transferId'] = data['transferId'];
    this['userName'] = data['userName'];
  }
}

export class MoveCarrierToPlaceResponse {
  /**  */
  'carrierId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'rejected'?: boolean;

  /**  */
  'rejectedReason'?: string;

  /**  */
  'transferComplete'?: boolean;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['rejected'] = data['rejected'];
    this['rejectedReason'] = data['rejectedReason'];
    this['transferComplete'] = data['transferComplete'];
  }
}

export class MoveStockBetweenReplenishmentCarriersCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumMoveStockBetweenReplenishmentCarriersCmdProcessType;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'sourceCarrierReservationKey'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierId'?: string;

  /**  */
  'targetCarrierReservationKey'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['sourceCarrierReservationKey'] = data['sourceCarrierReservationKey'];
    this['stockId'] = data['stockId'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierId'] = data['targetCarrierId'];
    this['targetCarrierReservationKey'] = data['targetCarrierReservationKey'];
    this['userName'] = data['userName'];
  }
}

export class MoveStockBetweenReplenishmentCarriersResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class MoveStockCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumMoveStockCmdProcessType;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierId'?: string;

  /**  */
  'targetPlaceId'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierId'] = data['targetCarrierId'];
    this['targetPlaceId'] = data['targetPlaceId'];
    this['userName'] = data['userName'];
  }
}

export class MoveStockCmdResponse {
  /**  */
  'movedQuantity'?: number;

  /**  */
  'operationId'?: string;

  /**  */
  'originalStockId'?: string;

  /**  */
  'targetStockActualQuantity'?: number;

  /**  */
  'targetStockId'?: string;

  constructor(data: undefined | any = {}) {
    this['movedQuantity'] = data['movedQuantity'];
    this['operationId'] = data['operationId'];
    this['originalStockId'] = data['originalStockId'];
    this['targetStockActualQuantity'] = data['targetStockActualQuantity'];
    this['targetStockId'] = data['targetStockId'];
  }
}

export class MoveStockToCarrierCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumMoveStockToCarrierCmdProcessType;

  /**  */
  'quantity'?: number;

  /**  */
  'sourcePlaceId'?: string;

  /**  */
  'sourceStockId'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetCarrierId'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['sourcePlaceId'] = data['sourcePlaceId'];
    this['sourceStockId'] = data['sourceStockId'];
    this['tabNumber'] = data['tabNumber'];
    this['targetCarrierId'] = data['targetCarrierId'];
    this['transferId'] = data['transferId'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class NamedId {
  /**  */
  'label'?: string;

  /**  */
  'value'?: string;

  constructor(data: undefined | any = {}) {
    this['label'] = data['label'];
    this['value'] = data['value'];
  }
}

export class OrderBoxDto {
  /**  */
  'carrierId'?: string;

  /**  */
  'orderItems'?: OutboundOrderItemDto[];

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['orderItems'] = data['orderItems'];
  }
}

export class OrderByCriteria {
  /**  */
  'field'?: string;

  /**  */
  'order'?: EnumOrderByCriteriaOrder;

  constructor(data: undefined | any = {}) {
    this['field'] = data['field'];
    this['order'] = data['order'];
  }
}

export class OutboundOrderCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'orderBoxes'?: OrderBoxDto[];

  /**  */
  'orderVersion'?: string;

  /**  */
  'outboundDeliveryNumber'?: string;

  /**  */
  'outboundTime'?: Date;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumOutboundOrderCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unshippedOrderItems'?: UnshippedOrderItemDto[];

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['orderBoxes'] = data['orderBoxes'];
    this['orderVersion'] = data['orderVersion'];
    this['outboundDeliveryNumber'] = data['outboundDeliveryNumber'];
    this['outboundTime'] = data['outboundTime'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['unshippedOrderItems'] = data['unshippedOrderItems'];
    this['userName'] = data['userName'];
  }
}

export class OutboundOrderItemDto {
  /**  */
  'baseAmount'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'position'?: number;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['position'] = data['position'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['unit'] = data['unit'];
  }
}

export class OverridePlaceCommonCharacteristicCmd {
  /**  */
  'characteristic'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'value'?: ValueCmd;

  constructor(data: undefined | any = {}) {
    this['characteristic'] = data['characteristic'];
    this['siteId'] = data['siteId'];
    this['value'] = data['value'];
  }
}

export class PageRequestInfo {
  /**  */
  'limit'?: number;

  /**  */
  'offset'?: number;

  constructor(data: undefined | any = {}) {
    this['limit'] = data['limit'];
    this['offset'] = data['offset'];
  }
}

export class PickerRouteReq {
  /**  */
  'pickingZoneId'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'startPlaceId'?: string;

  /**  */
  'transferIds'?: string[];

  constructor(data: undefined | any = {}) {
    this['pickingZoneId'] = data['pickingZoneId'];
    this['siteCode'] = data['siteCode'];
    this['startPlaceId'] = data['startPlaceId'];
    this['transferIds'] = data['transferIds'];
  }
}

export class PickingZoneTransferReq {
  /**  */
  'carrierId'?: string;

  /**  */
  'destinationPlaceId'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'startPlaceId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['destinationPlaceId'] = data['destinationPlaceId'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['startPlaceId'] = data['startPlaceId'];
    this['transferId'] = data['transferId'];
  }
}

export class PlaceCharacteristicDto {
  /**  */
  'author'?: string;

  /**  */
  'characteristicId'?: string;

  /**  */
  'checkParents'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'editable'?: boolean;

  /**  */
  'editor'?: string;

  /**  */
  'mandatory'?: boolean;

  /**  */
  'placeId'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'type'?: string;

  /**  */
  'typeCharacteristicId'?: string;

  /**  */
  'updatedTime'?: number;

  /**  */
  'value'?: ValueDto;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['characteristicId'] = data['characteristicId'];
    this['checkParents'] = data['checkParents'];
    this['createdTime'] = data['createdTime'];
    this['editable'] = data['editable'];
    this['editor'] = data['editor'];
    this['mandatory'] = data['mandatory'];
    this['placeId'] = data['placeId'];
    this['siteId'] = data['siteId'];
    this['type'] = data['type'];
    this['typeCharacteristicId'] = data['typeCharacteristicId'];
    this['updatedTime'] = data['updatedTime'];
    this['value'] = data['value'];
  }
}

export class PlaceCheckError {
  constructor(data: undefined | any = {}) {}
}

export class PlaceDto {
  /**  */
  'address'?: string;

  /**  */
  'addressTypeId'?: string;

  /**  */
  'author'?: string;

  /**  */
  'childPlaces'?: PlaceDto[];

  /**  */
  'coordinates'?: CoordinatesDto;

  /**  */
  'createdTime'?: number;

  /**  */
  'description'?: string;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'number'?: number;

  /**  */
  'parentId'?: string;

  /**  */
  'parentPlaces'?: PlaceDto[];

  /**  */
  'siteId'?: string;

  /**  */
  'status'?: PlaceStatusDto;

  /**  */
  'statusReason'?: string;

  /**  */
  'storagePlace'?: boolean;

  /**  */
  'typeCode'?: string;

  /**  */
  'typeId'?: string;

  /**  */
  'typeName'?: string;

  /**  */
  'updatedTime'?: number;

  /**  */
  'virtualPlace'?: boolean;

  /**  */
  'zones'?: ZoneInfo[];

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['addressTypeId'] = data['addressTypeId'];
    this['author'] = data['author'];
    this['childPlaces'] = data['childPlaces'];
    this['coordinates'] = data['coordinates'];
    this['createdTime'] = data['createdTime'];
    this['description'] = data['description'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['number'] = data['number'];
    this['parentId'] = data['parentId'];
    this['parentPlaces'] = data['parentPlaces'];
    this['siteId'] = data['siteId'];
    this['status'] = data['status'];
    this['statusReason'] = data['statusReason'];
    this['storagePlace'] = data['storagePlace'];
    this['typeCode'] = data['typeCode'];
    this['typeId'] = data['typeId'];
    this['typeName'] = data['typeName'];
    this['updatedTime'] = data['updatedTime'];
    this['virtualPlace'] = data['virtualPlace'];
    this['zones'] = data['zones'];
  }
}

export class PlaceHierarchyFilter {
  /**  */
  'hierarchyElements'?: PlaceHierarchyFilterCondition[];

  /**  */
  'siteId'?: string;

  constructor(data: undefined | any = {}) {
    this['hierarchyElements'] = data['hierarchyElements'];
    this['siteId'] = data['siteId'];
  }
}

export class PlaceHierarchyFilterCondition {
  /**  */
  'criteria'?: string;

  /**  */
  'placeTypeId'?: string;

  constructor(data: undefined | any = {}) {
    this['criteria'] = data['criteria'];
    this['placeTypeId'] = data['placeTypeId'];
  }
}

export class PlaceHierarchyTreeDto {
  /**  */
  'address'?: string;

  /**  */
  'characteristics'?: CharacteristicDto[];

  /**  */
  'children'?: PlaceHierarchyTreeDto[];

  /**  */
  'id'?: string;

  /**  */
  'number'?: number;

  /**  */
  'status'?: string;

  /**  */
  'type'?: Type;

  /**  */
  'zones'?: Zone[];

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['characteristics'] = data['characteristics'];
    this['children'] = data['children'];
    this['id'] = data['id'];
    this['number'] = data['number'];
    this['status'] = data['status'];
    this['type'] = data['type'];
    this['zones'] = data['zones'];
  }
}

export class PlaceInfo {
  /**  */
  'address'?: string;

  /**  */
  'author'?: string;

  /**  */
  'childrenAmount'?: number;

  /**  */
  'createdTime'?: number;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'placeZones'?: PlaceZoneInfo[];

  /**  */
  'siteId'?: string;

  /**  */
  'siteName'?: string;

  /**  */
  'typeName'?: string;

  /**  */
  'updatedTime'?: number;

  /**  */
  'zoneNames'?: string[];

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['author'] = data['author'];
    this['childrenAmount'] = data['childrenAmount'];
    this['createdTime'] = data['createdTime'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['placeZones'] = data['placeZones'];
    this['siteId'] = data['siteId'];
    this['siteName'] = data['siteName'];
    this['typeName'] = data['typeName'];
    this['updatedTime'] = data['updatedTime'];
    this['zoneNames'] = data['zoneNames'];
  }
}

export class PlaceNodeWithPosition {
  /**  */
  'childPlaces'?: Data[];

  /**  */
  'placeNodeChainFromTop'?: NamedId[];

  constructor(data: undefined | any = {}) {
    this['childPlaces'] = data['childPlaces'];
    this['placeNodeChainFromTop'] = data['placeNodeChainFromTop'];
  }
}

export class PlaceReservationForCarriersNotFoundError {
  constructor(data: undefined | any = {}) {}
}

export class PlaceReservationSearchRequest {
  /**  */
  'placeIds'?: string[];

  constructor(data: undefined | any = {}) {
    this['placeIds'] = data['placeIds'];
  }
}

export class PlaceReservationSearchResponse {
  /**  */
  'placeResponses'?: PlaceResponse[];

  constructor(data: undefined | any = {}) {
    this['placeResponses'] = data['placeResponses'];
  }
}

export class PlaceResponse {
  /**  */
  'carrierTransferReservations'?: CarrierTransferReservationInfo[];

  /**  */
  'carriers'?: CarrierInfo[];

  /**  */
  'placeAddress'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'processId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierTransferReservations'] = data['carrierTransferReservations'];
    this['carriers'] = data['carriers'];
    this['placeAddress'] = data['placeAddress'];
    this['placeId'] = data['placeId'];
    this['processId'] = data['processId'];
  }
}

export class PlaceSearchFilter {
  /**  */
  'hierarchy'?: PlaceHierarchyFilter;

  /**  */
  'placeCategory'?: EnumPlaceSearchFilterPlaceCategory;

  /**  */
  'placeTypes'?: string[];

  /**  */
  'zonesType'?: PlaceZonesFilter[];

  constructor(data: undefined | any = {}) {
    this['hierarchy'] = data['hierarchy'];
    this['placeCategory'] = data['placeCategory'];
    this['placeTypes'] = data['placeTypes'];
    this['zonesType'] = data['zonesType'];
  }
}

export class PlaceShortView {
  /**  */
  'barcode'?: string;

  /**  */
  'description'?: string;

  /**  */
  'id'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'typeCode'?: string;

  /**  */
  'typeName'?: string;

  constructor(data: undefined | any = {}) {
    this['barcode'] = data['barcode'];
    this['description'] = data['description'];
    this['id'] = data['id'];
    this['siteCode'] = data['siteCode'];
    this['typeCode'] = data['typeCode'];
    this['typeName'] = data['typeName'];
  }
}

export class PlaceStatusDto {
  /**  */
  'code'?: string;

  /**  */
  'default'?: boolean;

  /**  */
  'title'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['default'] = data['default'];
    this['title'] = data['title'];
  }
}

export class PlaceTypeCharacteristicDTO {
  /**  */
  'author'?: string;

  /**  */
  'characteristicId'?: string;

  /**  */
  'checkParents'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'defaultValue'?: DefaultValue;

  /**  */
  'editable'?: boolean;

  /**  */
  'editor'?: string;

  /**  */
  'mandatory'?: boolean;

  /**  */
  'placeTypeId'?: string;

  /**  */
  'type'?: string;

  /**  */
  'updatedTime'?: number;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['characteristicId'] = data['characteristicId'];
    this['checkParents'] = data['checkParents'];
    this['createdTime'] = data['createdTime'];
    this['defaultValue'] = data['defaultValue'];
    this['editable'] = data['editable'];
    this['editor'] = data['editor'];
    this['mandatory'] = data['mandatory'];
    this['placeTypeId'] = data['placeTypeId'];
    this['type'] = data['type'];
    this['updatedTime'] = data['updatedTime'];
  }
}

export class PlaceTypeCodeExistsQuery {
  /**  */
  'code'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
  }
}

export class PlaceTypeDto {
  /**  */
  'author'?: string;

  /**  */
  'code'?: string;

  /**  */
  'coordinatesRequired'?: boolean;

  /**  */
  'createdTime'?: number;

  /**  */
  'description'?: string;

  /**  */
  'editor'?: string;

  /**  */
  'maxMixBatches'?: number;

  /**  */
  'maxMixProducts'?: number;

  /**  */
  'name'?: string;

  /**  */
  'numberOfImplementation'?: number;

  /**  */
  'numerationRule'?: string;

  /**  */
  'placeTypeId'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'storagePlace'?: boolean;

  /**  */
  'updatedTime'?: number;

  /**  */
  'virtualPlace'?: boolean;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['code'] = data['code'];
    this['coordinatesRequired'] = data['coordinatesRequired'];
    this['createdTime'] = data['createdTime'];
    this['description'] = data['description'];
    this['editor'] = data['editor'];
    this['maxMixBatches'] = data['maxMixBatches'];
    this['maxMixProducts'] = data['maxMixProducts'];
    this['name'] = data['name'];
    this['numberOfImplementation'] = data['numberOfImplementation'];
    this['numerationRule'] = data['numerationRule'];
    this['placeTypeId'] = data['placeTypeId'];
    this['siteId'] = data['siteId'];
    this['storagePlace'] = data['storagePlace'];
    this['updatedTime'] = data['updatedTime'];
    this['virtualPlace'] = data['virtualPlace'];
  }
}

export class PlaceWithPosition {
  /**  */
  'place'?: PlaceDto;

  /**  */
  'placeNodeChainFromTop'?: NamedId[];

  constructor(data: undefined | any = {}) {
    this['place'] = data['place'];
    this['placeNodeChainFromTop'] = data['placeNodeChainFromTop'];
  }
}

export class PlaceWithType {
  /**  */
  'address'?: string;

  /**  */
  'id'?: string;

  /**  */
  'number'?: number;

  /**  */
  'type'?: Type;

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['id'] = data['id'];
    this['number'] = data['number'];
    this['type'] = data['type'];
  }
}

export class PlaceZoneInfo {
  /**  */
  'zoneCode'?: string;

  /**  */
  'zoneName'?: string;

  /**  */
  'zoneTypeCode'?: string;

  /**  */
  'zoneTypeName'?: string;

  constructor(data: undefined | any = {}) {
    this['zoneCode'] = data['zoneCode'];
    this['zoneName'] = data['zoneName'];
    this['zoneTypeCode'] = data['zoneTypeCode'];
    this['zoneTypeName'] = data['zoneTypeName'];
  }
}

export class PlaceZonesFilter {
  /**  */
  'zoneTypeId'?: string;

  /**  */
  'zonesId'?: string[];

  constructor(data: undefined | any = {}) {
    this['zoneTypeId'] = data['zoneTypeId'];
    this['zonesId'] = data['zonesId'];
  }
}

export class PlacesSearchQuery {
  /**  */
  'filter'?: PlaceSearchFilter;

  /**  */
  'orderInfo'?: OrderByCriteria[];

  /**  */
  'pageInfo'?: PageRequestInfo;

  constructor(data: undefined | any = {}) {
    this['filter'] = data['filter'];
    this['orderInfo'] = data['orderInfo'];
    this['pageInfo'] = data['pageInfo'];
  }
}

export class PlacesSearchResult {
  /**  */
  'places'?: PlaceInfo[];

  /**  */
  'totalCount'?: number;

  constructor(data: undefined | any = {}) {
    this['places'] = data['places'];
    this['totalCount'] = data['totalCount'];
  }
}

export class ProcessTransferInfo {
  /**  */
  'carrierTypeCode'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['processId'] = data['processId'];
    this['quantity'] = data['quantity'];
    this['transferId'] = data['transferId'];
  }
}

export class ProductAmountDto {
  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'rejectionReason'?: EnumProductAmountDtoRejectionReason;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['rejectionReason'] = data['rejectionReason'];
    this['unit'] = data['unit'];
  }
}

export class ProductAmountItem {
  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['unit'] = data['unit'];
  }
}

export class ProductBatchBalanceByStockType {
  /**  */
  'baseAmountERP'?: number;

  /**  */
  'baseAmountWMS'?: number;

  /**  */
  'quantityERP'?: number;

  /**  */
  'quantityWMS'?: number;

  /**  */
  'stockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmountERP'] = data['baseAmountERP'];
    this['baseAmountWMS'] = data['baseAmountWMS'];
    this['quantityERP'] = data['quantityERP'];
    this['quantityWMS'] = data['quantityWMS'];
    this['stockTypeCode'] = data['stockTypeCode'];
  }
}

export class ProductBatchBalanceDifferenceDto {
  /**  */
  'canCorrect'?: boolean;

  /**  */
  'needCorrect'?: boolean;

  /**  */
  'stockTypes'?: ProductBatchBalanceByStockType[];

  constructor(data: undefined | any = {}) {
    this['canCorrect'] = data['canCorrect'];
    this['needCorrect'] = data['needCorrect'];
    this['stockTypes'] = data['stockTypes'];
  }
}

export class ProductBatchDto {
  /**  */
  'createMarksOnPicking'?: boolean;

  /**  */
  'expirationTime'?: number;

  /**  */
  'id'?: string;

  /**  */
  'inboundId'?: string;

  /**  */
  'inboundTime'?: number;

  /**  */
  'manufactureTime'?: number;

  /**  */
  'marksAccountingType'?: EnumProductBatchDtoMarksAccountingType;

  /**  */
  'mercuryExtId'?: string;

  /**  */
  'number'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'productName'?: string;

  /**  */
  'referenceBNumber'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'type'?: EnumProductBatchDtoType;

  /**  */
  'vendorCode'?: string;

  /**  */
  'vendorName'?: string;

  /**  */
  'weightProduct'?: boolean;

  constructor(data: undefined | any = {}) {
    this['createMarksOnPicking'] = data['createMarksOnPicking'];
    this['expirationTime'] = data['expirationTime'];
    this['id'] = data['id'];
    this['inboundId'] = data['inboundId'];
    this['inboundTime'] = data['inboundTime'];
    this['manufactureTime'] = data['manufactureTime'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['mercuryExtId'] = data['mercuryExtId'];
    this['number'] = data['number'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['referenceBNumber'] = data['referenceBNumber'];
    this['siteId'] = data['siteId'];
    this['type'] = data['type'];
    this['vendorCode'] = data['vendorCode'];
    this['vendorName'] = data['vendorName'];
    this['weightProduct'] = data['weightProduct'];
  }
}

export class ProductBatchSearchDto {
  /**  */
  'beiUnit'?: string;

  /**  */
  'beiUnitDenominator'?: number;

  /**  */
  'beiUnitNumerator'?: number;

  /**  */
  'confirmed'?: boolean;

  /**  */
  'createdTime'?: number;

  /**  */
  'expirationTime'?: number;

  /**  */
  'height'?: number;

  /**  */
  'id'?: string;

  /**  */
  'inboundTime'?: number;

  /**  */
  'length'?: number;

  /**  */
  'manufactureTime'?: number;

  /**  */
  'marksAccountingType'?: EnumProductBatchSearchDtoMarksAccountingType;

  /**  */
  'mercuryExtId'?: string;

  /**  */
  'peiUnit'?: string;

  /**  */
  'productBatchNumber'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'productName'?: string;

  /**  */
  'sellByTime'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'totalBaseAmountRemainder'?: number;

  /**  */
  'totalQuantityRemainder'?: number;

  /**  */
  'type'?: EnumProductBatchSearchDtoType;

  /**  */
  'vendorCode'?: string;

  /**  */
  'vendorName'?: string;

  /**  */
  'volume'?: number;

  /**  */
  'weightGross'?: number;

  /**  */
  'weightNet'?: number;

  /**  */
  'width'?: number;

  constructor(data: undefined | any = {}) {
    this['beiUnit'] = data['beiUnit'];
    this['beiUnitDenominator'] = data['beiUnitDenominator'];
    this['beiUnitNumerator'] = data['beiUnitNumerator'];
    this['confirmed'] = data['confirmed'];
    this['createdTime'] = data['createdTime'];
    this['expirationTime'] = data['expirationTime'];
    this['height'] = data['height'];
    this['id'] = data['id'];
    this['inboundTime'] = data['inboundTime'];
    this['length'] = data['length'];
    this['manufactureTime'] = data['manufactureTime'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['mercuryExtId'] = data['mercuryExtId'];
    this['peiUnit'] = data['peiUnit'];
    this['productBatchNumber'] = data['productBatchNumber'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['sellByTime'] = data['sellByTime'];
    this['siteCode'] = data['siteCode'];
    this['siteId'] = data['siteId'];
    this['totalBaseAmountRemainder'] = data['totalBaseAmountRemainder'];
    this['totalQuantityRemainder'] = data['totalQuantityRemainder'];
    this['type'] = data['type'];
    this['vendorCode'] = data['vendorCode'];
    this['vendorName'] = data['vendorName'];
    this['volume'] = data['volume'];
    this['weightGross'] = data['weightGross'];
    this['weightNet'] = data['weightNet'];
    this['width'] = data['width'];
  }
}

export class ProductBatchSearchFilter {
  /**  */
  'confirmed'?: boolean;

  /**  */
  'expirationDateFrom'?: Date;

  /**  */
  'expirationDateTo'?: Date;

  /**  */
  'inboundTimeFrom'?: Date;

  /**  */
  'inboundTimeTo'?: Date;

  /**  */
  'manufactureTimeFrom'?: Date;

  /**  */
  'manufactureTimeTo'?: Date;

  /**  */
  'marksAccountingType'?: EnumProductBatchSearchFilterMarksAccountingType;

  /**  */
  'mercuryExtId'?: string;

  /**  */
  'number'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'sellByTimeFrom'?: Date;

  /**  */
  'sellByTimeTo'?: Date;

  /**  */
  'siteCode'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'types'?: EnumProductBatchSearchFilterTypes[];

  constructor(data: undefined | any = {}) {
    this['confirmed'] = data['confirmed'];
    this['expirationDateFrom'] = data['expirationDateFrom'];
    this['expirationDateTo'] = data['expirationDateTo'];
    this['inboundTimeFrom'] = data['inboundTimeFrom'];
    this['inboundTimeTo'] = data['inboundTimeTo'];
    this['manufactureTimeFrom'] = data['manufactureTimeFrom'];
    this['manufactureTimeTo'] = data['manufactureTimeTo'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['mercuryExtId'] = data['mercuryExtId'];
    this['number'] = data['number'];
    this['productId'] = data['productId'];
    this['sellByTimeFrom'] = data['sellByTimeFrom'];
    this['sellByTimeTo'] = data['sellByTimeTo'];
    this['siteCode'] = data['siteCode'];
    this['siteId'] = data['siteId'];
    this['types'] = data['types'];
  }
}

export class ProductBatchesSearchQuery {
  /**  */
  'filter'?: ProductBatchSearchFilter;

  /**  */
  'orderInfo'?: OrderByCriteria[];

  /**  */
  'pageInfo'?: PageRequestInfo;

  constructor(data: undefined | any = {}) {
    this['filter'] = data['filter'];
    this['orderInfo'] = data['orderInfo'];
    this['pageInfo'] = data['pageInfo'];
  }
}

export class ProductBatchesSearchResult {
  /**  */
  'productBatches'?: ProductBatchSearchDto[];

  /**  */
  'totalCount'?: number;

  constructor(data: undefined | any = {}) {
    this['productBatches'] = data['productBatches'];
    this['totalCount'] = data['totalCount'];
  }
}

export class ProductPlacementRequest {
  /**  */
  'openCarriers'?: string[];

  /**  */
  'operationId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'stockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['openCarriers'] = data['openCarriers'];
    this['operationId'] = data['operationId'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['stockTypeCode'] = data['stockTypeCode'];
  }
}

export class ProductPlacementResponse {
  /**  */
  'operationId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'rejectedInfo'?: RejectedInfo;

  /**  */
  'reservedCarriers'?: ReservedCarrier[];

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['rejectedInfo'] = data['rejectedInfo'];
    this['reservedCarriers'] = data['reservedCarriers'];
  }
}

export class RawPlacementRequest {
  /**  */
  'incomingCarrierId'?: string;

  /**  */
  'openCarriers'?: string[];

  /**  */
  'operationId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'stockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['incomingCarrierId'] = data['incomingCarrierId'];
    this['openCarriers'] = data['openCarriers'];
    this['operationId'] = data['operationId'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['stockTypeCode'] = data['stockTypeCode'];
  }
}

export class RawStock {
  /**  */
  'position'?: number;

  /**  */
  'productBatchId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  constructor(data: undefined | any = {}) {
    this['position'] = data['position'];
    this['productBatchId'] = data['productBatchId'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
  }
}

export class ReduceIncomingQuantityCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['productBatchId'] = data['productBatchId'];
    this['quantity'] = data['quantity'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['transferId'] = data['transferId'];
  }
}

export class ReducePlaceReservationForTransferCmd {
  /**  */
  'carrierTypeCode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['operationId'] = data['operationId'];
    this['placeId'] = data['placeId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['transferId'] = data['transferId'];
  }
}

export class RejectedInfo {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'quantity'?: number;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['quantity'] = data['quantity'];
  }
}

export class RejectedProductItemAmount {
  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'rejectionReason'?: EnumRejectedProductItemAmountRejectionReason;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['rejectionReason'] = data['rejectionReason'];
    this['unit'] = data['unit'];
  }
}

export class ReplenishCmd {
  /**  */
  'destPlaceBarcode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumReplenishCmdProcessType;

  /**  */
  'productBatchId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'srcCarrierId'?: string;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'targetStockTypeCode'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['destPlaceBarcode'] = data['destPlaceBarcode'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['productBatchId'] = data['productBatchId'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['srcCarrierId'] = data['srcCarrierId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['targetStockTypeCode'] = data['targetStockTypeCode'];
    this['userName'] = data['userName'];
  }
}

export class ReservationCancelledResponse {
  /**  */
  'incomingQuantity'?: number;

  /**  */
  'operationId'?: string;

  /**  */
  'outgoingQuantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['incomingQuantity'] = data['incomingQuantity'];
    this['operationId'] = data['operationId'];
    this['outgoingQuantity'] = data['outgoingQuantity'];
    this['siteCode'] = data['siteCode'];
    this['stockId'] = data['stockId'];
    this['transferId'] = data['transferId'];
  }
}

export class ReservationDto {
  /**  */
  'batchId'?: string;

  /**  */
  'placeAddress'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'quantityIncoming'?: number;

  /**  */
  'quantityOutgoing'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['batchId'] = data['batchId'];
    this['placeAddress'] = data['placeAddress'];
    this['placeId'] = data['placeId'];
    this['productId'] = data['productId'];
    this['quantityIncoming'] = data['quantityIncoming'];
    this['quantityOutgoing'] = data['quantityOutgoing'];
    this['stockId'] = data['stockId'];
    this['transferId'] = data['transferId'];
  }
}

export class ReservationInfoDto {
  /**  */
  'batchId'?: string;

  /**  */
  'incomingQuantity'?: number;

  /**  */
  'outgoingQuantity'?: number;

  /**  */
  'productId'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['batchId'] = data['batchId'];
    this['incomingQuantity'] = data['incomingQuantity'];
    this['outgoingQuantity'] = data['outgoingQuantity'];
    this['productId'] = data['productId'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['unit'] = data['unit'];
  }
}

export class ReserveCarrierForProcessCmd {
  /**  */
  'carrierNumber'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumReserveCarrierForProcessCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierNumber'] = data['carrierNumber'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class ReserveCarrierForProcessError {
  constructor(data: undefined | any = {}) {}
}

export class ReserveCarrierForProcessResponse {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
  }
}

export class ReserveChutePlacesForCarriersCmd {
  /**  */
  'carrierPlaceTypeCodes'?: string[];

  /**  */
  'carriers'?: TransferDto[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'processPlaceTypeCodes'?: string[];

  /**  */
  'siteCode'?: string;

  /**  */
  'zones'?: ZoneReference[];

  constructor(data: undefined | any = {}) {
    this['carrierPlaceTypeCodes'] = data['carrierPlaceTypeCodes'];
    this['carriers'] = data['carriers'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processName'] = data['processName'];
    this['processPlaceTypeCodes'] = data['processPlaceTypeCodes'];
    this['siteCode'] = data['siteCode'];
    this['zones'] = data['zones'];
  }
}

export class ReserveInfo {
  /**  */
  'placeAddress'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'placeTypeCode'?: string;

  /**  */
  'transfer'?: TransferDto;

  constructor(data: undefined | any = {}) {
    this['placeAddress'] = data['placeAddress'];
    this['placeId'] = data['placeId'];
    this['placeTypeCode'] = data['placeTypeCode'];
    this['transfer'] = data['transfer'];
  }
}

export class ReservePlaceForProcessCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeBarcode'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeBarcode'] = data['placeBarcode'];
    this['processId'] = data['processId'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ReservePlacesForCarriersByClusterDeliveryCmd {
  /**  */
  'carrierPlaceTypeCode'?: string;

  /**  */
  'carriers'?: ProcessTransferInfo[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'processPlaceTypeCode'?: string;

  /**  */
  'reservationMode'?: EnumReservePlacesForCarriersByClusterDeliveryCmdReservationMode;

  /**  */
  'siteCode'?: string;

  /**  */
  'zones'?: ZoneReference[];

  constructor(data: undefined | any = {}) {
    this['carrierPlaceTypeCode'] = data['carrierPlaceTypeCode'];
    this['carriers'] = data['carriers'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processName'] = data['processName'];
    this['processPlaceTypeCode'] = data['processPlaceTypeCode'];
    this['reservationMode'] = data['reservationMode'];
    this['siteCode'] = data['siteCode'];
    this['zones'] = data['zones'];
  }
}

export class ReservePlacesForCarriersByMultyProcessesCmd {
  /**  */
  'carrierPlaceTypeCode'?: string;

  /**  */
  'carriers'?: ProcessTransferInfo[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'processPlaceTypeCode'?: string;

  /**  */
  'siteCode'?: string;

  /**  */
  'zones'?: ZoneReference[];

  constructor(data: undefined | any = {}) {
    this['carrierPlaceTypeCode'] = data['carrierPlaceTypeCode'];
    this['carriers'] = data['carriers'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processName'] = data['processName'];
    this['processPlaceTypeCode'] = data['processPlaceTypeCode'];
    this['siteCode'] = data['siteCode'];
    this['zones'] = data['zones'];
  }
}

export class ReservePlacesForCarriersCmd {
  /**  */
  'carrierPlaceTypeCode'?: string;

  /**  */
  'carriers'?: TransferDto[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'processPlaceTypeCode'?: string;

  /**  */
  'reservationMode'?: EnumReservePlacesForCarriersCmdReservationMode;

  /**  */
  'siteCode'?: string;

  /**  */
  'zones'?: ZoneReference[];

  constructor(data: undefined | any = {}) {
    this['carrierPlaceTypeCode'] = data['carrierPlaceTypeCode'];
    this['carriers'] = data['carriers'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processName'] = data['processName'];
    this['processPlaceTypeCode'] = data['processPlaceTypeCode'];
    this['reservationMode'] = data['reservationMode'];
    this['siteCode'] = data['siteCode'];
    this['zones'] = data['zones'];
  }
}

export class ReservePlacesForCarriersError {
  constructor(data: undefined | any = {}) {}
}

export class ReserveStockOutgoingQuantityCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['transferId'] = data['transferId'];
  }
}

export class ReserveStockOutgoingQuantityResponse {
  /**  */
  'operationId'?: string;

  /**  */
  'reservation'?: ReservationDto;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['reservation'] = data['reservation'];
  }
}

export class ReservedCarrier {
  /**  */
  'openNewCarrier'?: boolean;

  /**  */
  'openedCarrierId'?: string;

  /**  */
  'quantity'?: number;

  constructor(data: undefined | any = {}) {
    this['openNewCarrier'] = data['openNewCarrier'];
    this['openedCarrierId'] = data['openedCarrierId'];
    this['quantity'] = data['quantity'];
  }
}

export class ReservedChutePlacesForCarriersResponse {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'rejectedTransfers'?: TransferDto[];

  /**  */
  'reserves'?: ReserveInfo[];

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processName'] = data['processName'];
    this['rejectedTransfers'] = data['rejectedTransfers'];
    this['reserves'] = data['reserves'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ReservedPlacesForCarriersByClusterDeliveryResponse {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'rejectedTransfers'?: ProcessTransferInfo[];

  /**  */
  'reserves'?: ReserveInfo[];

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processName'] = data['processName'];
    this['rejectedTransfers'] = data['rejectedTransfers'];
    this['reserves'] = data['reserves'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ReservedPlacesForCarriersByMultiProcessesResponse {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'rejectedTransfers'?: ProcessTransferInfo[];

  /**  */
  'reserves'?: ReserveInfo[];

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processName'] = data['processName'];
    this['rejectedTransfers'] = data['rejectedTransfers'];
    this['reserves'] = data['reserves'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ReservedPlacesForCarriersResponse {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processName'?: string;

  /**  */
  'rejectedTransfers'?: TransferDto[];

  /**  */
  'reserves'?: ReserveInfo[];

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processName'] = data['processName'];
    this['rejectedTransfers'] = data['rejectedTransfers'];
    this['reserves'] = data['reserves'];
    this['siteCode'] = data['siteCode'];
  }
}

export class ResponseMarkDTO {
  /**  */
  'actualCode'?: string;

  /**  */
  'newStatus'?: EnumResponseMarkDTONewStatus;

  /**  */
  'requestedCode'?: string;

  constructor(data: undefined | any = {}) {
    this['actualCode'] = data['actualCode'];
    this['newStatus'] = data['newStatus'];
    this['requestedCode'] = data['requestedCode'];
  }
}

export class Route {
  /**  */
  'steps'?: RouteStep[];

  constructor(data: undefined | any = {}) {
    this['steps'] = data['steps'];
  }
}

export class RouteNoEdgeError {
  constructor(data: undefined | any = {}) {}
}

export class RouteNotVacantError {
  constructor(data: undefined | any = {}) {}
}

export class RouteStep {
  /**  */
  'aisleNumber'?: number;

  /**  */
  'cameraNumber'?: number;

  /**  */
  'floorNumber'?: number;

  /**  */
  'orderNumber': number;

  /**  */
  'placeAddress': string;

  /**  */
  'regionNumber'?: number;

  /**  */
  'rowNumber'?: number;

  /**  */
  'sectionNumber'?: number;

  constructor(data: undefined | any = {}) {
    this['aisleNumber'] = data['aisleNumber'];
    this['cameraNumber'] = data['cameraNumber'];
    this['floorNumber'] = data['floorNumber'];
    this['orderNumber'] = data['orderNumber'];
    this['placeAddress'] = data['placeAddress'];
    this['regionNumber'] = data['regionNumber'];
    this['rowNumber'] = data['rowNumber'];
    this['sectionNumber'] = data['sectionNumber'];
  }
}

export class RouteStrategyDTO {
  /** Идентификатор стратегии */
  'id': string;

  /** Название стратегии (русскоязычное понятное навание стратегии) */
  'name': string;

  /** Код площадки */
  'siteCode': string;

  /** коды секторов, к которым будет привязана данная стратегия */
  'zoneCodes': string[];

  constructor(data: undefined | any = {}) {
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['siteCode'] = data['siteCode'];
    this['zoneCodes'] = data['zoneCodes'];
  }
}

export class RouteStrategyStep {
  /** Адрес места/секции */
  'sectionAddress': string;

  /** Признак, является ли данное место - местом старта  */
  'startPlace': boolean;

  /** Порядковый номер секции в стратегии обхода */
  'step': number;

  constructor(data: undefined | any = {}) {
    this['sectionAddress'] = data['sectionAddress'];
    this['startPlace'] = data['startPlace'];
    this['step'] = data['step'];
  }
}

export class RouteStrategySteps {
  /** Последенее время редактирования стратегии */
  'lastUpdateTime': number;

  /** Пользователь, который последний раз редактировал стратегию */
  'lastUpdateUser': string;

  /** Идентификатор стратегии */
  'routeStrategyId': string;

  /** Название стратегии */
  'routeStrategyName': string;

  /** Последовательность секций для обхода */
  'steps': RouteStrategyStep[];

  constructor(data: undefined | any = {}) {
    this['lastUpdateTime'] = data['lastUpdateTime'];
    this['lastUpdateUser'] = data['lastUpdateUser'];
    this['routeStrategyId'] = data['routeStrategyId'];
    this['routeStrategyName'] = data['routeStrategyName'];
    this['steps'] = data['steps'];
  }
}

export class SetCarrierPlaceCmd {
  /**  */
  'actualPlaceAddress'?: string;

  /**  */
  'carrierId'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'planPlaceAddress'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumSetCarrierPlaceCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['actualPlaceAddress'] = data['actualPlaceAddress'];
    this['carrierId'] = data['carrierId'];
    this['operationId'] = data['operationId'];
    this['planPlaceAddress'] = data['planPlaceAddress'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['transferId'] = data['transferId'];
    this['userName'] = data['userName'];
  }
}

export class ShipCarrierCmd {
  /**  */
  'carrierId'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumShipCarrierCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierId'] = data['carrierId'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class SiteCodeExistsQuery {
  /**  */
  'code'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
  }
}

export class SiteDto {
  /**  */
  'archived'?: boolean;

  /**  */
  'author'?: string;

  /**  */
  'code'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'description'?: string;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'updatedTime'?: number;

  constructor(data: undefined | any = {}) {
    this['archived'] = data['archived'];
    this['author'] = data['author'];
    this['code'] = data['code'];
    this['createdTime'] = data['createdTime'];
    this['description'] = data['description'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['updatedTime'] = data['updatedTime'];
  }
}

export class SiteSettingDto {
  /**  */
  'bigDecimalValue'?: number;

  /**  */
  'code'?: EnumSiteSettingDtoCode;

  /**  */
  'intValue'?: number;

  /**  */
  'stringValue'?: string;

  /**  */
  'type'?: EnumSiteSettingDtoType;

  constructor(data: undefined | any = {}) {
    this['bigDecimalValue'] = data['bigDecimalValue'];
    this['code'] = data['code'];
    this['intValue'] = data['intValue'];
    this['stringValue'] = data['stringValue'];
    this['type'] = data['type'];
  }
}

export class SlottingRoute {
  /**  */
  'plannedDuration'?: number;

  /**  */
  'steps'?: SlottingRouteStep[];

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['plannedDuration'] = data['plannedDuration'];
    this['steps'] = data['steps'];
    this['transferId'] = data['transferId'];
  }
}

export class SlottingRouteStep {
  /**  */
  'characteristics'?: CharacteristicDto[];

  /**  */
  'hierarchy'?: PlaceWithType[];

  /**  */
  'place'?: PlaceWithType;

  /**  */
  'plannedDuration'?: number;

  /**  */
  'reservation'?: ReservationInfoDto;

  /**  */
  'transferId'?: string;

  /**  */
  'zones'?: ZoneWithType[];

  constructor(data: undefined | any = {}) {
    this['characteristics'] = data['characteristics'];
    this['hierarchy'] = data['hierarchy'];
    this['place'] = data['place'];
    this['plannedDuration'] = data['plannedDuration'];
    this['reservation'] = data['reservation'];
    this['transferId'] = data['transferId'];
    this['zones'] = data['zones'];
  }
}

export class StockAddressesAndProductIdsSearchFilter {
  /**  */
  'addresses'?: string[];

  /**  */
  'productIds'?: string[];

  /**  */
  'siteId'?: string;

  constructor(data: undefined | any = {}) {
    this['addresses'] = data['addresses'];
    this['productIds'] = data['productIds'];
    this['siteId'] = data['siteId'];
  }
}

export class StockAddressesAndProductIdsSearchQuery {
  /**  */
  'filter'?: StockAddressesAndProductIdsSearchFilter;

  /**  */
  'orderInfo'?: OrderByCriteria[];

  /**  */
  'pageInfo'?: PageRequestInfo;

  constructor(data: undefined | any = {}) {
    this['filter'] = data['filter'];
    this['orderInfo'] = data['orderInfo'];
    this['pageInfo'] = data['pageInfo'];
  }
}

export class StockAmountDto {
  /**  */
  'createMarksOnPicking'?: boolean;

  /**  */
  'marksAccountingType'?: EnumStockAmountDtoMarksAccountingType;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['createMarksOnPicking'] = data['createMarksOnPicking'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['unit'] = data['unit'];
  }
}

export class StockAnnihilationCmd {
  /**  */
  'baseAmount'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'carrierId'?: string;

  /**  */
  'carrierReservationKey'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumStockAnnihilationCmdProcessType;

  /**  */
  'productBatchId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['carrierId'] = data['carrierId'];
    this['carrierReservationKey'] = data['carrierReservationKey'];
    this['operationId'] = data['operationId'];
    this['placeId'] = data['placeId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['productBatchId'] = data['productBatchId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class StockAnnihilationResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class StockDto {
  /**  */
  'actualQuantity'?: number;

  /**  */
  'actualVolume'?: number;

  /**  */
  'actualWeight'?: number;

  /**  */
  'availableQuantity'?: number;

  /**  */
  'availableVolume'?: number;

  /**  */
  'availableWeight'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'batchConfirmed'?: boolean;

  /**  */
  'batchId'?: string;

  /**  */
  'batchNumber'?: string;

  /**  */
  'carrierId'?: string;

  /**  */
  'incomingQuantity'?: number;

  /**  */
  'incomingVolume'?: number;

  /**  */
  'incomingWeight'?: number;

  /**  */
  'outgoingQuantity'?: number;

  /**  */
  'outgoingVolume'?: number;

  /**  */
  'outgoingWeight'?: number;

  /**  */
  'placeId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'productName'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['actualQuantity'] = data['actualQuantity'];
    this['actualVolume'] = data['actualVolume'];
    this['actualWeight'] = data['actualWeight'];
    this['availableQuantity'] = data['availableQuantity'];
    this['availableVolume'] = data['availableVolume'];
    this['availableWeight'] = data['availableWeight'];
    this['baseUnit'] = data['baseUnit'];
    this['batchConfirmed'] = data['batchConfirmed'];
    this['batchId'] = data['batchId'];
    this['batchNumber'] = data['batchNumber'];
    this['carrierId'] = data['carrierId'];
    this['incomingQuantity'] = data['incomingQuantity'];
    this['incomingVolume'] = data['incomingVolume'];
    this['incomingWeight'] = data['incomingWeight'];
    this['outgoingQuantity'] = data['outgoingQuantity'];
    this['outgoingVolume'] = data['outgoingVolume'];
    this['outgoingWeight'] = data['outgoingWeight'];
    this['placeId'] = data['placeId'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['unit'] = data['unit'];
  }
}

export class StockHierarchySearchFilter {
  /**  */
  'hierarchy'?: PlaceHierarchyFilter;

  /**  */
  'placeCategory'?: EnumStockHierarchySearchFilterPlaceCategory;

  /**  */
  'placeTypes'?: string[];

  /**  */
  'productIds'?: string[];

  /**  */
  'zonesType'?: PlaceZonesFilter[];

  constructor(data: undefined | any = {}) {
    this['hierarchy'] = data['hierarchy'];
    this['placeCategory'] = data['placeCategory'];
    this['placeTypes'] = data['placeTypes'];
    this['productIds'] = data['productIds'];
    this['zonesType'] = data['zonesType'];
  }
}

export class StockHierarchySearchQuery {
  /**  */
  'filter'?: StockHierarchySearchFilter;

  /**  */
  'orderInfo'?: OrderByCriteria[];

  /**  */
  'pageInfo'?: PageRequestInfo;

  constructor(data: undefined | any = {}) {
    this['filter'] = data['filter'];
    this['orderInfo'] = data['orderInfo'];
    this['pageInfo'] = data['pageInfo'];
  }
}

export class StockInCmd {
  /**  */
  'baseAmount'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'carrierId'?: string;

  /**  */
  'documentDate'?: Date;

  /**  */
  'documentNumber'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumStockInCmdProcessType;

  /**  */
  'productBatchId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['carrierId'] = data['carrierId'];
    this['documentDate'] = data['documentDate'];
    this['documentNumber'] = data['documentNumber'];
    this['operationId'] = data['operationId'];
    this['placeId'] = data['placeId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['productBatchId'] = data['productBatchId'];
    this['quantity'] = data['quantity'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class StockInResponse {
  /**  */
  'operationId'?: string;

  /**  */
  'stockId'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['stockId'] = data['stockId'];
  }
}

export class StockInfo {
  /**  */
  'actual'?: FullAmount;

  /**  */
  'available'?: FullAmount;

  /**  */
  'batchConfirmed'?: boolean;

  /**  */
  'batchId'?: string;

  /**  */
  'batchNumber'?: string;

  /**  */
  'carrier'?: boolean;

  /**  */
  'expirationTime'?: number;

  /**  */
  'inbound'?: FullAmount;

  /**  */
  'inboundTime'?: number;

  /**  */
  'manufactureTime'?: number;

  /**  */
  'outbound'?: FullAmount;

  /**  */
  'placeAddress'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'productId'?: string;

  /**  */
  'productName'?: string;

  /**  */
  'sellByTime'?: number;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'stockTypeId'?: string;

  constructor(data: undefined | any = {}) {
    this['actual'] = data['actual'];
    this['available'] = data['available'];
    this['batchConfirmed'] = data['batchConfirmed'];
    this['batchId'] = data['batchId'];
    this['batchNumber'] = data['batchNumber'];
    this['carrier'] = data['carrier'];
    this['expirationTime'] = data['expirationTime'];
    this['inbound'] = data['inbound'];
    this['inboundTime'] = data['inboundTime'];
    this['manufactureTime'] = data['manufactureTime'];
    this['outbound'] = data['outbound'];
    this['placeAddress'] = data['placeAddress'];
    this['placeId'] = data['placeId'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['sellByTime'] = data['sellByTime'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['stockTypeId'] = data['stockTypeId'];
  }
}

export class StockReservationsNotFoundError {
  constructor(data: undefined | any = {}) {}
}

export class StockTypeDto {
  /**  */
  'author'?: string;

  /**  */
  'code'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'updatedTime'?: number;

  /**  */
  'writeOffAllowed'?: boolean;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['code'] = data['code'];
    this['createdTime'] = data['createdTime'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['updatedTime'] = data['updatedTime'];
    this['writeOffAllowed'] = data['writeOffAllowed'];
  }
}

export class StocksSearchResult {
  /**  */
  'stocks'?: StockInfo[];

  /**  */
  'totalCount'?: number;

  constructor(data: undefined | any = {}) {
    this['stocks'] = data['stocks'];
    this['totalCount'] = data['totalCount'];
  }
}

export class TransferDroppedResponse {
  /**  */
  'canceledReservations'?: ReservationDto[];

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['canceledReservations'] = data['canceledReservations'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['transferId'] = data['transferId'];
  }
}

export class TransferDto {
  /**  */
  'carrierTypeCode'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['quantity'] = data['quantity'];
    this['transferId'] = data['transferId'];
  }
}

export class TransferRoute {
  /**  */
  'route'?: string[];

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['route'] = data['route'];
    this['transferId'] = data['transferId'];
  }
}

export class TryAllocateSpaceForReplenishmentCmd {
  /**  */
  'destPlaceBarcode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'productBatchId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'siteCode'?: string;

  /**  */
  'srcCarrierId'?: string;

  /**  */
  'stockTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['destPlaceBarcode'] = data['destPlaceBarcode'];
    this['operationId'] = data['operationId'];
    this['processId'] = data['processId'];
    this['productBatchId'] = data['productBatchId'];
    this['quantity'] = data['quantity'];
    this['siteCode'] = data['siteCode'];
    this['srcCarrierId'] = data['srcCarrierId'];
    this['stockTypeCode'] = data['stockTypeCode'];
  }
}

export class TryAllocateSpaceForReplenishmentResponse {
  /**  */
  'approvedQuantity'?: number;

  /**  */
  'operationId'?: string;

  /**  */
  'rejectedInfo'?: RejectedInfo;

  constructor(data: undefined | any = {}) {
    this['approvedQuantity'] = data['approvedQuantity'];
    this['operationId'] = data['operationId'];
    this['rejectedInfo'] = data['rejectedInfo'];
  }
}

export class TryTakeRequest {
  /**  */
  'operationId'?: string;

  /**  */
  'placeId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeId'] = data['placeId'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['transferId'] = data['transferId'];
    this['unit'] = data['unit'];
  }
}

export class TryTakeResponse {
  /**  */
  'acceptedQuantity'?: number;

  /**  */
  'operationId'?: string;

  /**  */
  'rejectReason'?: string;

  /**  */
  'rejected'?: boolean;

  /**  */
  'stockId'?: string;

  /**  */
  'transferId'?: string;

  constructor(data: undefined | any = {}) {
    this['acceptedQuantity'] = data['acceptedQuantity'];
    this['operationId'] = data['operationId'];
    this['rejectReason'] = data['rejectReason'];
    this['rejected'] = data['rejected'];
    this['stockId'] = data['stockId'];
    this['transferId'] = data['transferId'];
  }
}

export class Type {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
  }
}

export class UIStockDto {
  /** Фактический остаток в ПЕИ */
  'actualQuantity': number;

  /** Фактический остаток в мл */
  'actualVolume': number;

  /** Фактический остаток в граммах */
  'actualWeight': number;

  /** Доступный остаток в ПЕИ */
  'availableQuantity': number;

  /** Доступный остаток в мл */
  'availableVolume': number;

  /** Доступный остаток в граммах */
  'availableWeight': number;

  /** Носитель */
  'carrierBarcode'?: string;

  /** Тип носителя */
  'carrierTypeCode'?: string;

  /** Годен до */
  'expirationTime': number;

  /** Дата и время приемки партии запаса */
  'inboundTime': number;

  /** Входящий остаток в ПЕИ */
  'incomingQuantity': number;

  /** Входящий остаток в мл */
  'incomingVolume': number;

  /** Входящий остаток в граммах */
  'incomingWeight': number;

  /** Дата и время производства партии запаса */
  'manufactureTime': number;

  /** Тип помарочного учета */
  'marksAccountingType'?: EnumUIStockDtoMarksAccountingType;

  /** Исходящий остаток в ПЕИ */
  'outgoingQuantity': number;

  /** Исходящий остаток в мл */
  'outgoingVolume': number;

  /** Исходящий остаток в граммах */
  'outgoingWeight': number;

  /** Родительский носитель */
  'parentCarrierBarcode'?: string;

  /** Тип родительского носителя */
  'parentCarrierTypeCode'?: string;

  /** Адрес места */
  'placeAddress': string;

  /** Статус места */
  'placeStatus': string;

  /** Тип места */
  'placeTypeCode': string;

  /** Партия принята к учету */
  'productBatchConfirmed': boolean;

  /** Номер партии */
  'productBatchNumber': string;

  /** Тип партии */
  'productBatchType': EnumUIStockDtoProductBatchType;

  /** Артикул */
  'productId': string;

  /** Название товара */
  'productName': string;

  /** Срок реализации */
  'sellByTime': number;

  /** id запаса */
  'stockId': string;

  /** Код вида запаса */
  'stockTypeCode': string;

  /** ПЕИ */
  'unit': string;

  constructor(data: undefined | any = {}) {
    this['actualQuantity'] = data['actualQuantity'];
    this['actualVolume'] = data['actualVolume'];
    this['actualWeight'] = data['actualWeight'];
    this['availableQuantity'] = data['availableQuantity'];
    this['availableVolume'] = data['availableVolume'];
    this['availableWeight'] = data['availableWeight'];
    this['carrierBarcode'] = data['carrierBarcode'];
    this['carrierTypeCode'] = data['carrierTypeCode'];
    this['expirationTime'] = data['expirationTime'];
    this['inboundTime'] = data['inboundTime'];
    this['incomingQuantity'] = data['incomingQuantity'];
    this['incomingVolume'] = data['incomingVolume'];
    this['incomingWeight'] = data['incomingWeight'];
    this['manufactureTime'] = data['manufactureTime'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['outgoingQuantity'] = data['outgoingQuantity'];
    this['outgoingVolume'] = data['outgoingVolume'];
    this['outgoingWeight'] = data['outgoingWeight'];
    this['parentCarrierBarcode'] = data['parentCarrierBarcode'];
    this['parentCarrierTypeCode'] = data['parentCarrierTypeCode'];
    this['placeAddress'] = data['placeAddress'];
    this['placeStatus'] = data['placeStatus'];
    this['placeTypeCode'] = data['placeTypeCode'];
    this['productBatchConfirmed'] = data['productBatchConfirmed'];
    this['productBatchNumber'] = data['productBatchNumber'];
    this['productBatchType'] = data['productBatchType'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['sellByTime'] = data['sellByTime'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['unit'] = data['unit'];
  }
}

export class UIStockSearchQuery {
  /** Номер носителя */
  'carrierBarcodes'?: string[];

  /** Тип носителя */
  'carrierTypeCodes'?: string[];

  /** Признак "Принято к учету" */
  'confirmed'?: boolean;

  /** Типы зон, исключенные из поиска */
  'excludedZoneTypeIds'?: string[];

  /** Годен до (от) */
  'expirationDateFrom'?: Date;

  /** Годен до (до) */
  'expirationDateTo'?: Date;

  /** Принят (от) */
  'inboundTimeFrom'?: Date;

  /** Принят (до) */
  'inboundTimeTo'?: Date;

  /** Произведен (от) */
  'manufactureTimeFrom'?: Date;

  /** Произведен (до) */
  'manufactureTimeTo'?: Date;

  /** Тип маркировки */
  'markAccountingTypes'?: EnumUIStockSearchQueryMarkAccountingTypes[];

  /**  */
  'maxResponseSize'?: number;

  /** Адреса мест */
  'placeAddresses'?: string[];

  /** Статус места */
  'placeStatusCodes'?: string[];

  /** Тип места */
  'placeTypeCodes'?: string[];

  /** Номер партии */
  'productBatchNumbers'?: string[];

  /** Тип партии */
  'productBatchTypes'?: EnumUIStockSearchQueryProductBatchTypes[];

  /** Артикулы товара */
  'productIds'?: string[];

  /** Срок реализации (от) */
  'sellByTimeFrom'?: Date;

  /** Срок реализации (до) */
  'sellByTimeTo'?: Date;

  /**  */
  'siteCode': string;

  /** Вид запаса */
  'stockTypeCodes'?: string[];

  /** Признак "Виртуальное место" */
  'virtualPlace'?: boolean;

  /** Без маркировки */
  'withoutMarkAccountingType'?: boolean;

  /** Зоны */
  'zoneIds'?: string[];

  constructor(data: undefined | any = {}) {
    this['carrierBarcodes'] = data['carrierBarcodes'];
    this['carrierTypeCodes'] = data['carrierTypeCodes'];
    this['confirmed'] = data['confirmed'];
    this['excludedZoneTypeIds'] = data['excludedZoneTypeIds'];
    this['expirationDateFrom'] = data['expirationDateFrom'];
    this['expirationDateTo'] = data['expirationDateTo'];
    this['inboundTimeFrom'] = data['inboundTimeFrom'];
    this['inboundTimeTo'] = data['inboundTimeTo'];
    this['manufactureTimeFrom'] = data['manufactureTimeFrom'];
    this['manufactureTimeTo'] = data['manufactureTimeTo'];
    this['markAccountingTypes'] = data['markAccountingTypes'];
    this['maxResponseSize'] = data['maxResponseSize'];
    this['placeAddresses'] = data['placeAddresses'];
    this['placeStatusCodes'] = data['placeStatusCodes'];
    this['placeTypeCodes'] = data['placeTypeCodes'];
    this['productBatchNumbers'] = data['productBatchNumbers'];
    this['productBatchTypes'] = data['productBatchTypes'];
    this['productIds'] = data['productIds'];
    this['sellByTimeFrom'] = data['sellByTimeFrom'];
    this['sellByTimeTo'] = data['sellByTimeTo'];
    this['siteCode'] = data['siteCode'];
    this['stockTypeCodes'] = data['stockTypeCodes'];
    this['virtualPlace'] = data['virtualPlace'];
    this['withoutMarkAccountingType'] = data['withoutMarkAccountingType'];
    this['zoneIds'] = data['zoneIds'];
  }
}

export class UIStockSearchResult {
  /**  */
  'stocks': UIStockDto[];

  /**  */
  'totalCount': number;

  constructor(data: undefined | any = {}) {
    this['stocks'] = data['stocks'];
    this['totalCount'] = data['totalCount'];
  }
}

export class UnshippedOrderItemDto {
  /**  */
  'baseAmount'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'position'?: number;

  /**  */
  'productId'?: string;

  /**  */
  'quantity'?: number;

  /**  */
  'unit'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['position'] = data['position'];
    this['productId'] = data['productId'];
    this['quantity'] = data['quantity'];
    this['unit'] = data['unit'];
  }
}

export class UpdateCarrierTypeCmd {
  /**  */
  'description'?: string;

  /**  */
  'height'?: number;

  /**  */
  'length'?: number;

  /**  */
  'maxVolume'?: number;

  /**  */
  'maxWeight'?: number;

  /**  */
  'name'?: string;

  /**  */
  'width'?: number;

  constructor(data: undefined | any = {}) {
    this['description'] = data['description'];
    this['height'] = data['height'];
    this['length'] = data['length'];
    this['maxVolume'] = data['maxVolume'];
    this['maxWeight'] = data['maxWeight'];
    this['name'] = data['name'];
    this['width'] = data['width'];
  }
}

export class UpdateMarksCmd {
  /**  */
  'marks'?: MarkUpdateDTO[];

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumUpdateMarksCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['marks'] = data['marks'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class UpdateMarksResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class UpdatePlaceCmd {
  /**  */
  'address': string;

  /**  */
  'addressTypeId'?: string;

  /**  */
  'coordinates'?: Coordinates3D;

  /**  */
  'description'?: string;

  /**  */
  'number': number;

  /**  */
  'status'?: PlaceStatusDto;

  /**  */
  'statusReason'?: string;

  /**  */
  'typeId': string;

  /**  */
  'zoneIds'?: string[];

  constructor(data: undefined | any = {}) {
    this['address'] = data['address'];
    this['addressTypeId'] = data['addressTypeId'];
    this['coordinates'] = data['coordinates'];
    this['description'] = data['description'];
    this['number'] = data['number'];
    this['status'] = data['status'];
    this['statusReason'] = data['statusReason'];
    this['typeId'] = data['typeId'];
    this['zoneIds'] = data['zoneIds'];
  }
}

export class UpdatePlaceStatusCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'placeAddress'?: string;

  /**  */
  'placeStatusCode'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumUpdatePlaceStatusCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['placeAddress'] = data['placeAddress'];
    this['placeStatusCode'] = data['placeStatusCode'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class UpdatePlaceStatusResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'placeAddress'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['placeAddress'] = data['placeAddress'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class UpdatePlaceTypeCmd {
  /**  */
  'description'?: string;

  /**  */
  'maxMixBatches'?: number;

  /**  */
  'maxMixProducts'?: number;

  /**  */
  'name'?: string;

  /**  */
  'numerationRule'?: EnumUpdatePlaceTypeCmdNumerationRule;

  /**  */
  'storagePlace'?: boolean;

  /**  */
  'virtualPlace'?: boolean;

  constructor(data: undefined | any = {}) {
    this['description'] = data['description'];
    this['maxMixBatches'] = data['maxMixBatches'];
    this['maxMixProducts'] = data['maxMixProducts'];
    this['name'] = data['name'];
    this['numerationRule'] = data['numerationRule'];
    this['storagePlace'] = data['storagePlace'];
    this['virtualPlace'] = data['virtualPlace'];
  }
}

export class UpdatePlaceTypeCommonCharacteristicDto {
  /**  */
  'characteristic'?: string;

  /**  */
  'defaultValue'?: DefaultValue;

  constructor(data: undefined | any = {}) {
    this['characteristic'] = data['characteristic'];
    this['defaultValue'] = data['defaultValue'];
  }
}

export class UpdateRouteStrategyCmd {
  /** Название стратегии (русскоязычное понятное навание стратегии) */
  'name': string;

  /** Код площадки */
  'siteCode': string;

  /** коды секторов, к которым будет привязана данная стратегия. */
  'zoneCodes'?: string[];

  constructor(data: undefined | any = {}) {
    this['name'] = data['name'];
    this['siteCode'] = data['siteCode'];
    this['zoneCodes'] = data['zoneCodes'];
  }
}

export class UpdateSiteCmd {
  /**  */
  'description'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['description'] = data['description'];
    this['name'] = data['name'];
  }
}

export class UpdateStockTypeByInventoryCmd {
  /**  */
  'baseAmount'?: number;

  /**  */
  'docDate'?: Date;

  /**  */
  'documentNumber'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'operationType'?: EnumUpdateStockTypeByInventoryCmdOperationType;

  /**  */
  'orderItem'?: number;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumUpdateStockTypeByInventoryCmdProcessType;

  /**  */
  'saleAmount'?: number;

  /**  */
  'salesOrder'?: string;

  /**  */
  'specStock'?: string;

  /**  */
  'stockId'?: string;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'transferId'?: string;

  /**  */
  'updateReason'?: EnumUpdateStockTypeByInventoryCmdUpdateReason;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['docDate'] = data['docDate'];
    this['documentNumber'] = data['documentNumber'];
    this['operationId'] = data['operationId'];
    this['operationType'] = data['operationType'];
    this['orderItem'] = data['orderItem'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['saleAmount'] = data['saleAmount'];
    this['salesOrder'] = data['salesOrder'];
    this['specStock'] = data['specStock'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['transferId'] = data['transferId'];
    this['updateReason'] = data['updateReason'];
    this['userName'] = data['userName'];
  }
}

export class UpdateStockTypeByPickingCmd {
  /**  */
  'docDate'?: Date;

  /**  */
  'documentNumber'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'operationType'?: EnumUpdateStockTypeByPickingCmdOperationType;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumUpdateStockTypeByPickingCmdProcessType;

  /**  */
  'stockId'?: string;

  /**  */
  'stockTypeCode'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'updateReason'?: EnumUpdateStockTypeByPickingCmdUpdateReason;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['docDate'] = data['docDate'];
    this['documentNumber'] = data['documentNumber'];
    this['operationId'] = data['operationId'];
    this['operationType'] = data['operationType'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['updateReason'] = data['updateReason'];
    this['userName'] = data['userName'];
  }
}

export class UpdateStockTypeEvent {
  /**  */
  'actualQuantity'?: number;

  /**  */
  'batchId'?: string;

  /**  */
  'batchNumber'?: string;

  /**  */
  'beiQuantity'?: number;

  /**  */
  'beiUnit'?: string;

  /**  */
  'carrierId'?: string;

  /**  */
  'documentDate'?: Date;

  /**  */
  'documentNumber'?: string;

  /**  */
  'expirationDate'?: Date;

  /**  */
  'manufactureDate'?: Date;

  /**  */
  'marksAccountingType'?: EnumUpdateStockTypeEventMarksAccountingType;

  /**  */
  'newStockId'?: string;

  /**  */
  'newStockTypeCode'?: string;

  /**  */
  'oldStockId'?: string;

  /**  */
  'oldStockTypeCode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'operationType'?: EnumUpdateStockTypeEventOperationType;

  /**  */
  'orderItemIndex'?: number;

  /**  */
  'placeWithType'?: PlaceWithType;

  /**  */
  'productId'?: string;

  /**  */
  'productName'?: string;

  /**  */
  'salesOrder'?: string;

  /**  */
  'shouldNotifyERP'?: boolean;

  /**  */
  'siteCode'?: string;

  /**  */
  'specialStockCode'?: string;

  /**  */
  'storageDate'?: Date;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'updateReason'?: EnumUpdateStockTypeEventUpdateReason;

  constructor(data: undefined | any = {}) {
    this['actualQuantity'] = data['actualQuantity'];
    this['batchId'] = data['batchId'];
    this['batchNumber'] = data['batchNumber'];
    this['beiQuantity'] = data['beiQuantity'];
    this['beiUnit'] = data['beiUnit'];
    this['carrierId'] = data['carrierId'];
    this['documentDate'] = data['documentDate'];
    this['documentNumber'] = data['documentNumber'];
    this['expirationDate'] = data['expirationDate'];
    this['manufactureDate'] = data['manufactureDate'];
    this['marksAccountingType'] = data['marksAccountingType'];
    this['newStockId'] = data['newStockId'];
    this['newStockTypeCode'] = data['newStockTypeCode'];
    this['oldStockId'] = data['oldStockId'];
    this['oldStockTypeCode'] = data['oldStockTypeCode'];
    this['operationId'] = data['operationId'];
    this['operationType'] = data['operationType'];
    this['orderItemIndex'] = data['orderItemIndex'];
    this['placeWithType'] = data['placeWithType'];
    this['productId'] = data['productId'];
    this['productName'] = data['productName'];
    this['salesOrder'] = data['salesOrder'];
    this['shouldNotifyERP'] = data['shouldNotifyERP'];
    this['siteCode'] = data['siteCode'];
    this['specialStockCode'] = data['specialStockCode'];
    this['storageDate'] = data['storageDate'];
    this['tabNumber'] = data['tabNumber'];
    this['unit'] = data['unit'];
    this['updateReason'] = data['updateReason'];
  }
}

export class UpdateStockTypeForBatchCmd {
  /**  */
  'batchId'?: string;

  /**  */
  'newStockTypeCode'?: string;

  /**  */
  'oldStockTypeCode'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumUpdateStockTypeForBatchCmdProcessType;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['batchId'] = data['batchId'];
    this['newStockTypeCode'] = data['newStockTypeCode'];
    this['oldStockTypeCode'] = data['oldStockTypeCode'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class UpdateStockTypeInStockByUICmd {
  /**  */
  'operationId': string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumUpdateStockTypeInStockByUICmdProcessType;

  /**  */
  'quantity': number;

  /**  */
  'stockId': string;

  /**  */
  'stockTypeCode': string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['stockTypeCode'] = data['stockTypeCode'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class UpdateZoneCmd {
  /**  */
  'boundedWarehouseProductGroups'?: WarehouseProductGroupDto[];

  /**  */
  'code'?: string;

  /**  */
  'description'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['boundedWarehouseProductGroups'] = data['boundedWarehouseProductGroups'];
    this['code'] = data['code'];
    this['description'] = data['description'];
    this['name'] = data['name'];
  }
}

export class UpdateZoneTypeCmd {
  /**  */
  'description'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['description'] = data['description'];
    this['name'] = data['name'];
  }
}

export class ValueCmd {
  /**  */
  'decValue'?: number;

  /**  */
  'intValue'?: number;

  /**  */
  'maxIntValue'?: number;

  /**  */
  'minIntValue'?: number;

  constructor(data: undefined | any = {}) {
    this['decValue'] = data['decValue'];
    this['intValue'] = data['intValue'];
    this['maxIntValue'] = data['maxIntValue'];
    this['minIntValue'] = data['minIntValue'];
  }
}

export class ValueDto {
  /**  */
  'decValue'?: number;

  /**  */
  'intValue'?: number;

  /**  */
  'maxDecValue'?: number;

  /**  */
  'maxIntValue'?: number;

  /**  */
  'minDecValue'?: number;

  /**  */
  'minIntValue'?: number;

  constructor(data: undefined | any = {}) {
    this['decValue'] = data['decValue'];
    this['intValue'] = data['intValue'];
    this['maxDecValue'] = data['maxDecValue'];
    this['maxIntValue'] = data['maxIntValue'];
    this['minDecValue'] = data['minDecValue'];
    this['minIntValue'] = data['minIntValue'];
  }
}

export class WarehouseProductGroupDto {
  /**  */
  'code'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
  }
}

export class WriteOffRawCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'orderVersion'?: string;

  /**  */
  'outboundDeliveryNumber'?: string;

  /**  */
  'outboundTime'?: Date;

  /**  */
  'placeAddress'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumWriteOffRawCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'stocks'?: RawStock[];

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['orderVersion'] = data['orderVersion'];
    this['outboundDeliveryNumber'] = data['outboundDeliveryNumber'];
    this['outboundTime'] = data['outboundTime'];
    this['placeAddress'] = data['placeAddress'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['stocks'] = data['stocks'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class WriteOffRawResponse {
  /**  */
  'error'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'outboundDeliveryNumber'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'siteCode'?: string;

  constructor(data: undefined | any = {}) {
    this['error'] = data['error'];
    this['operationId'] = data['operationId'];
    this['outboundDeliveryNumber'] = data['outboundDeliveryNumber'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['siteCode'] = data['siteCode'];
  }
}

export class WriteOffRawStocksCmd {
  /**  */
  'operationId'?: string;

  /**  */
  'orderVersion'?: string;

  /**  */
  'outboundDeliveryNumber'?: string;

  /**  */
  'outboundTime'?: Date;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumWriteOffRawStocksCmdProcessType;

  /**  */
  'siteCode'?: string;

  /**  */
  'stocks'?: RawStock[];

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['orderVersion'] = data['orderVersion'];
    this['outboundDeliveryNumber'] = data['outboundDeliveryNumber'];
    this['outboundTime'] = data['outboundTime'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['siteCode'] = data['siteCode'];
    this['stocks'] = data['stocks'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class WriteOffStockByUICmd {
  /**  */
  'operationId': string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumWriteOffStockByUICmdProcessType;

  /**  */
  'quantity': number;

  /**  */
  'stockId': string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['tabNumber'] = data['tabNumber'];
    this['userName'] = data['userName'];
  }
}

export class WriteOffStockCmd {
  /**  */
  'baseAmount'?: number;

  /**  */
  'baseUnit'?: string;

  /**  */
  'documentDate'?: Date;

  /**  */
  'documentNumber'?: string;

  /**  */
  'operationId'?: string;

  /**  */
  'processBusinessKey'?: string;

  /**  */
  'processId'?: string;

  /**  */
  'processType'?: EnumWriteOffStockCmdProcessType;

  /**  */
  'quantity'?: number;

  /**  */
  'stockId'?: string;

  /**  */
  'tabNumber'?: string;

  /**  */
  'unit'?: string;

  /**  */
  'userName'?: string;

  constructor(data: undefined | any = {}) {
    this['baseAmount'] = data['baseAmount'];
    this['baseUnit'] = data['baseUnit'];
    this['documentDate'] = data['documentDate'];
    this['documentNumber'] = data['documentNumber'];
    this['operationId'] = data['operationId'];
    this['processBusinessKey'] = data['processBusinessKey'];
    this['processId'] = data['processId'];
    this['processType'] = data['processType'];
    this['quantity'] = data['quantity'];
    this['stockId'] = data['stockId'];
    this['tabNumber'] = data['tabNumber'];
    this['unit'] = data['unit'];
    this['userName'] = data['userName'];
  }
}

export class WriteOffStockResponse {
  /**  */
  'clientError'?: AbstractWarehouseError;

  /**  */
  'operationId'?: string;

  /**  */
  'stockId'?: string;

  constructor(data: undefined | any = {}) {
    this['clientError'] = data['clientError'];
    this['operationId'] = data['operationId'];
    this['stockId'] = data['stockId'];
  }
}

export class Zone {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'type'?: Type;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['type'] = data['type'];
  }
}

export class ZoneCodeExistsQuery {
  /**  */
  'code'?: string;

  /**  */
  'siteId'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['siteId'] = data['siteId'];
  }
}

export class ZoneDto {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
  }
}

export class ZoneInfo {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'typeCode'?: string;

  /**  */
  'typeId'?: string;

  /**  */
  'typeName'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['typeCode'] = data['typeCode'];
    this['typeId'] = data['typeId'];
    this['typeName'] = data['typeName'];
  }
}

export class ZoneProjection {
  /**  */
  'author'?: string;

  /**  */
  'boundedWarehouseProductGroups'?: WarehouseProductGroupDto[];

  /**  */
  'childAmount'?: number;

  /**  */
  'code'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'description'?: string;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'siteId'?: string;

  /**  */
  'typeId'?: string;

  /**  */
  'typeName'?: string;

  /**  */
  'updatedTime'?: number;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['boundedWarehouseProductGroups'] = data['boundedWarehouseProductGroups'];
    this['childAmount'] = data['childAmount'];
    this['code'] = data['code'];
    this['createdTime'] = data['createdTime'];
    this['description'] = data['description'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['siteId'] = data['siteId'];
    this['typeId'] = data['typeId'];
    this['typeName'] = data['typeName'];
    this['updatedTime'] = data['updatedTime'];
  }
}

export class ZoneReference {
  /**  */
  'zoneCode'?: string;

  /**  */
  'zoneTypeCode'?: string;

  constructor(data: undefined | any = {}) {
    this['zoneCode'] = data['zoneCode'];
    this['zoneTypeCode'] = data['zoneTypeCode'];
  }
}

export class ZoneTypeCodeExistsQuery {
  /**  */
  'code'?: string;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
  }
}

export class ZoneTypeDto {
  /**  */
  'author'?: string;

  /**  */
  'code'?: string;

  /**  */
  'createdTime'?: number;

  /**  */
  'description'?: string;

  /**  */
  'editor'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'numberOfImplementation'?: number;

  /**  */
  'updatedTime'?: number;

  /**  */
  'warehouseProductGroupCanBeBounded'?: boolean;

  constructor(data: undefined | any = {}) {
    this['author'] = data['author'];
    this['code'] = data['code'];
    this['createdTime'] = data['createdTime'];
    this['description'] = data['description'];
    this['editor'] = data['editor'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['numberOfImplementation'] = data['numberOfImplementation'];
    this['updatedTime'] = data['updatedTime'];
    this['warehouseProductGroupCanBeBounded'] = data['warehouseProductGroupCanBeBounded'];
  }
}

export class ZoneTypeWithZones {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'zones'?: ZoneDto[];

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['zones'] = data['zones'];
  }
}

export class ZoneTypeWithZonesDto {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'zones'?: ZoneDto[];

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['zones'] = data['zones'];
  }
}

export class ZoneWithType {
  /**  */
  'code'?: string;

  /**  */
  'id'?: string;

  /**  */
  'name'?: string;

  /**  */
  'type'?: Type;

  constructor(data: undefined | any = {}) {
    this['code'] = data['code'];
    this['id'] = data['id'];
    this['name'] = data['name'];
    this['type'] = data['type'];
  }
}
export enum EnumAddStockToCarrierCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumAssignProductBatchCmdMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumAssignProductBatchCmdType {
  'REGULAR' = 'REGULAR',
  'RETURN' = 'RETURN'
}
export enum EnumAuditMarkRecordDtoLevel {
  'TRANSPORT' = 'TRANSPORT',
  'GROUP' = 'GROUP',
  'INDIVIDUAL' = 'INDIVIDUAL'
}
export enum EnumAuditMarkRecordDtoSourceStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumAuditMarkRecordDtoTargetStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumAuditMarkRecordDtoType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumAuditMarkRecordSearchQueryLevel {
  'TRANSPORT' = 'TRANSPORT',
  'GROUP' = 'GROUP',
  'INDIVIDUAL' = 'INDIVIDUAL'
}
export enum EnumAuditMarkRecordSearchQuerySourceStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumAuditMarkRecordSearchQueryTargetStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumAuditMarkRecordSearchQueryType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumBoxRouteCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumBoxRouteCmdUseRoute {
  'NO_ROUTE' = 'NO_ROUTE'
}
export enum EnumBoxRouteCmdUseStartPlace {
  'NO_START_PLACE' = 'NO_START_PLACE'
}
export enum EnumCarrierGridViewStatus {
  'incoming' = 'incoming',
  'outgoing' = 'outgoing',
  'actual' = 'actual'
}
export enum EnumChangeMarksStatusCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumChangeRawStockTypeCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumChangeStockProductCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumChangeStockProductResponseProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumCommonCharacteristicSettingsParentInheritance {
  'check' = 'check',
  'ignore' = 'ignore'
}
export enum EnumCreateCarrierCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumCreateMarksCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumCreatePlaceTypeCmdNumerationRule {
  'LEFT_TO_RIGHT_TOP_TO_BOTTOM' = 'LEFT_TO_RIGHT_TOP_TO_BOTTOM',
  'LEFT_TO_RIGHT_BOTTOM_UP' = 'LEFT_TO_RIGHT_BOTTOM_UP',
  'RIGHT_TO_LEFT_TOP_TO_BOTTOM' = 'RIGHT_TO_LEFT_TOP_TO_BOTTOM',
  'RIGHT_TO_LEFT_BOTTOM_UP' = 'RIGHT_TO_LEFT_BOTTOM_UP'
}
export enum EnumCreateScannedChzMarkCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumCreateScannedChzMarkCmdStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumDeleteMarksCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumDetailedPlaceSearchFilterPlaceCategory {
  'STORAGE_PLACE' = 'STORAGE_PLACE',
  'NOT_STORAGE_PLACE' = 'NOT_STORAGE_PLACE',
  'ALL' = 'ALL'
}
export enum EnumDetailedStockSearchQueryInCarrier {
  'IN_ONLY' = 'IN_ONLY',
  'OUT_ONLY' = 'OUT_ONLY',
  'ALL' = 'ALL'
}
export enum EnumDetailedStockSearchQueryInVirtualPlace {
  'VIRTUAL' = 'VIRTUAL',
  'NOT_VIRTUAL' = 'NOT_VIRTUAL',
  'ALL' = 'ALL'
}
export enum EnumDropCarrierReservationForProcessCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumExchangeChuteReservationCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumMarkChangeStatusDTOSourceStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumMarkChangeStatusDTOTargetStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumMarkChangeStatusDTOType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMarkCreateDTOLevel {
  'TRANSPORT' = 'TRANSPORT',
  'GROUP' = 'GROUP',
  'INDIVIDUAL' = 'INDIVIDUAL'
}
export enum EnumMarkCreateDTOStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumMarkCreateDTOType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMarkDTOLevel {
  'TRANSPORT' = 'TRANSPORT',
  'GROUP' = 'GROUP',
  'INDIVIDUAL' = 'INDIVIDUAL'
}
export enum EnumMarkDTOMarkStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumMarkDTOType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMarkDeleteDTOType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMarkSearchDTOType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMarkSearchFilterLevel {
  'TRANSPORT' = 'TRANSPORT',
  'GROUP' = 'GROUP',
  'INDIVIDUAL' = 'INDIVIDUAL'
}
export enum EnumMarkSearchFilterStatuses {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumMarkSearchFilterType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMarkUpdateDTOType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumMoveAllStocksToCarrierCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumMoveCarrierToPlaceCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumMoveStockBetweenReplenishmentCarriersCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumMoveStockCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumMoveStockToCarrierCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumOrderByCriteriaOrder {
  'ASC' = 'ASC',
  'DESC' = 'DESC'
}
export enum EnumOutboundOrderCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumPlaceSearchFilterPlaceCategory {
  'STORAGE_PLACE' = 'STORAGE_PLACE',
  'NOT_STORAGE_PLACE' = 'NOT_STORAGE_PLACE',
  'ALL' = 'ALL'
}
export enum EnumProductAmountDtoRejectionReason {
  'NO_STOCK' = 'NO_STOCK',
  'NO_ROUTE' = 'NO_ROUTE'
}
export enum EnumProductBatchDtoMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumProductBatchDtoType {
  'REGULAR' = 'REGULAR',
  'RETURN' = 'RETURN'
}
export enum EnumProductBatchSearchDtoMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumProductBatchSearchDtoType {
  'REGULAR' = 'REGULAR',
  'RETURN' = 'RETURN'
}
export enum EnumProductBatchSearchFilterMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumProductBatchSearchFilterTypes {
  'REGULAR' = 'REGULAR',
  'RETURN' = 'RETURN'
}
export enum EnumRejectedProductItemAmountRejectionReason {
  'NO_STOCK' = 'NO_STOCK'
}
export enum EnumReplenishCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumReserveCarrierForProcessCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumReservePlacesForCarriersByClusterDeliveryCmdReservationMode {
  'FULL' = 'FULL',
  'PARTIAL' = 'PARTIAL',
  'SUSTAINED' = 'SUSTAINED'
}
export enum EnumReservePlacesForCarriersCmdReservationMode {
  'FULL' = 'FULL',
  'PARTIAL' = 'PARTIAL',
  'SUSTAINED' = 'SUSTAINED'
}
export enum EnumResponseMarkDTONewStatus {
  'INBOUND_DELIVERY' = 'INBOUND_DELIVERY',
  'REJECTED' = 'REJECTED',
  'INBOUND_SCANNED' = 'INBOUND_SCANNED',
  'INBOUND_EGAIS_CHECK' = 'INBOUND_EGAIS_CHECK',
  'FOR_SALE' = 'FOR_SALE',
  'PICKED' = 'PICKED',
  'SOLD' = 'SOLD',
  'CLIENT_RETURN' = 'CLIENT_RETURN',
  'RETURN_EGAIS_CHECK' = 'RETURN_EGAIS_CHECK',
  'WRITTEN_OFF' = 'WRITTEN_OFF',
  'FOUND' = 'FOUND',
  'CANCELLED' = 'CANCELLED'
}
export enum EnumSetCarrierPlaceCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumShipCarrierCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumSiteSettingDtoCode {
  'MAX_MIX_PRODUCTS' = 'MAX_MIX_PRODUCTS'
}
export enum EnumSiteSettingDtoType {
  'INTEGER' = 'INTEGER',
  'STRING' = 'STRING',
  'BIG_DECIMAL' = 'BIG_DECIMAL'
}
export enum EnumStockAmountDtoMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumStockAnnihilationCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumStockHierarchySearchFilterPlaceCategory {
  'STORAGE_PLACE' = 'STORAGE_PLACE',
  'NOT_STORAGE_PLACE' = 'NOT_STORAGE_PLACE',
  'ALL' = 'ALL'
}
export enum EnumStockInCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumUIStockDtoMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumUIStockDtoProductBatchType {
  'REGULAR' = 'REGULAR',
  'RETURN' = 'RETURN'
}
export enum EnumUIStockSearchQueryMarkAccountingTypes {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumUIStockSearchQueryProductBatchTypes {
  'REGULAR' = 'REGULAR',
  'RETURN' = 'RETURN'
}
export enum EnumUpdateMarksCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumUpdatePlaceStatusCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumUpdatePlaceTypeCmdNumerationRule {
  'LEFT_TO_RIGHT_TOP_TO_BOTTOM' = 'LEFT_TO_RIGHT_TOP_TO_BOTTOM',
  'LEFT_TO_RIGHT_BOTTOM_UP' = 'LEFT_TO_RIGHT_BOTTOM_UP',
  'RIGHT_TO_LEFT_TOP_TO_BOTTOM' = 'RIGHT_TO_LEFT_TOP_TO_BOTTOM',
  'RIGHT_TO_LEFT_BOTTOM_UP' = 'RIGHT_TO_LEFT_BOTTOM_UP'
}
export enum EnumUpdateStockTypeByInventoryCmdOperationType {
  'MOVE' = 'MOVE'
}
export enum EnumUpdateStockTypeByInventoryCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumUpdateStockTypeByInventoryCmdUpdateReason {
  'UNKNOWN' = 'UNKNOWN',
  'PRODUCT_BATCH' = 'PRODUCT_BATCH',
  'CORRUPTED_BARCODE' = 'CORRUPTED_BARCODE',
  'DEFECT' = 'DEFECT',
  'NO_PRODUCT' = 'NO_PRODUCT',
  'EXPIRED' = 'EXPIRED'
}
export enum EnumUpdateStockTypeByPickingCmdOperationType {
  'MOVE' = 'MOVE'
}
export enum EnumUpdateStockTypeByPickingCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumUpdateStockTypeByPickingCmdUpdateReason {
  'UNKNOWN' = 'UNKNOWN',
  'PRODUCT_BATCH' = 'PRODUCT_BATCH',
  'CORRUPTED_BARCODE' = 'CORRUPTED_BARCODE',
  'DEFECT' = 'DEFECT',
  'NO_PRODUCT' = 'NO_PRODUCT',
  'EXPIRED' = 'EXPIRED'
}
export enum EnumUpdateStockTypeEventMarksAccountingType {
  'EGAIS' = 'EGAIS',
  'CHZ' = 'CHZ'
}
export enum EnumUpdateStockTypeEventOperationType {
  'MOVE' = 'MOVE'
}
export enum EnumUpdateStockTypeEventUpdateReason {
  'UNKNOWN' = 'UNKNOWN',
  'PRODUCT_BATCH' = 'PRODUCT_BATCH',
  'CORRUPTED_BARCODE' = 'CORRUPTED_BARCODE',
  'DEFECT' = 'DEFECT',
  'NO_PRODUCT' = 'NO_PRODUCT',
  'EXPIRED' = 'EXPIRED'
}
export enum EnumUpdateStockTypeForBatchCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumUpdateStockTypeInStockByUICmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumWriteOffRawCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumWriteOffRawStocksCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumWriteOffStockByUICmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
export enum EnumWriteOffStockCmdProcessType {
  'INBOUND' = 'INBOUND',
  'REPLENISHMENT' = 'REPLENISHMENT',
  'MANUFACTURING' = 'MANUFACTURING',
  'OUTBOUND' = 'OUTBOUND',
  'PICKING' = 'PICKING',
  'CONSOLIDATION' = 'CONSOLIDATION',
  'CLIENT_RETURNS' = 'CLIENT_RETURNS',
  'INVENTORY' = 'INVENTORY',
  'UI' = 'UI',
  'TSD_MANUAL_MOVEMENT' = 'TSD_MANUAL_MOVEMENT',
  'WMS_SUPPORT' = 'WMS_SUPPORT',
  'NOT_SPECIFIED' = 'NOT_SPECIFIED'
}
