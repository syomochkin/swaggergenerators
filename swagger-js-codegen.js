var fs = require("fs");
var path = require("path");
var CodeGen = require("swagger-js-codegen").CodeGen;

var file = "./swagger.json";
var swagger = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, file), "UTF-8")
);
var tsSourceCode = CodeGen.getTypescriptCode({
  className: "Test",
  swagger: swagger,
});

fs.writeFileSync(
  path.resolve(__dirname, "./src/dto/swaggerJsCodegen.ts"),
  tsSourceCode
);
