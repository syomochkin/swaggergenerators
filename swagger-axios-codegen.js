const { codegen } = require("swagger-axios-codegen");

codegen({
  remoteUrl: "https://wms.utkonos.dev/api/warehouse/v2/api-docs",
  outputDir: "./src/dto",
  fileName: "swaggerAxiosCodegen.ts",
  methodNameMode: "path",
  modelMode: "class",
});
