const converter = require("swagger2openapi");

converter.convertUrl(
  "https://wms.utkonos.dev/api/warehouse/v2/api-docs",
  {
    warnOnly: true,
    refSiblings: "preserve",
    rbname: "requestBodyName",
  },
  function (err, options) {
    if (err) {
      throw new Error(err);
    }
    console.log(options);
  }
);
